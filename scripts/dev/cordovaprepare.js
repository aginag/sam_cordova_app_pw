const fs = require('fs');
const path = require("path");
var program = require('commander');

program
    .version('0.1.0')
    .option('--path <path>', 'Add the path to watch')
    .option('--platform <platform>', 'Add the platforms to be prepared', ['ios', 'android', 'windows'])
    .action(function(file) {

        if (!program.path) {
            console.error("No path defined!");
            return;
        }

        if (!program.platform) {
            console.error("No platform defined!");
            return;
        }

        const watchPath = program.path;
        const platform = program.platform;

        const util = require('util');
        const exec = util.promisify(require('child_process').exec);

        async function cordovaPrepare() {
            console.log(`cordova prepare ${platform}`);
            const { stdout, stderr } = await exec(`cordova prepare ${platform}`);

            if (stdout !== "") {
                console.log('stdout:', stdout);
            }

            if (stderr !== "") {
                console.log('stderr:', stderr);
            }
        }

        console.log(`Watching for file changes on ${watchPath}`);

        fs.watch(watchPath, {
            interval: 1000,
            recursive: true
        }, (event, fileName) => {
            if (fileName) {
                try {
                    if (fs.statSync(path.join(watchPath, fileName)).isFile()) {
                        console.log(`${fileName} file Changed`);
                        cordovaPrepare();
                    }
                } catch (err) {}
            }
        });
    })
    .parse(process.argv);