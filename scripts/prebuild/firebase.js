const fs = require("fs");
const cheerio = require("cheerio");
const moment = require ("moment");

var args = process.argv.slice(2);

if (args.length == 0) {
    throw new Error("no arguments passed");
}

console.log(args);

const firebasePath = args[0];
const firebaseInfoId = args[1];
const firebaseVersionId = args[2];
const appName = args[3];
const version = args[4];
const buildNumber = args[5];

if (!fs.lstatSync(firebasePath).isFile()) {
    throw new Error("Build path is no file");
}

const $ = cheerio.load(fs.readFileSync(firebasePath));

$(firebaseInfoId).html(appName + " (Last Changes: " + moment().format("YYYY-MM-DD HH:mm") + " - Version <span id='" + firebaseVersionId + "'>" + version.toString() + " (" + buildNumber + ")</span>)");

// Save updated html
fs.writeFile(firebasePath, $.html(), function(err) {
    if(err) {
        throw err;
    }
});