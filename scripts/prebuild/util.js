const fs = require("fs");
const fsExtra = require("fs-extra");
const path = require("path");
const util = require('util');
const execSync = require('child_process').execSync;
const rimraf = util.promisify(require("rimraf"));
const stat = util.promisify(fs.stat);

const PROJECT_ROOT_PATH = path.join(__dirname, "../../");
const WWW_FOLDER_PATH = path.join(__dirname, "../../www");
const WWW_RESOURCES_PATH = path.join(WWW_FOLDER_PATH, "./resources");
const DIST_RESOURCES_PATH = path.join(__dirname, "../../dist_container/resources");

const BUILD_UI5_DIST_COMMAND = "node node_modules/@ui5/cli/bin/ui5.js build self-contained --all --config ui5_samcontainer.yaml --dest ./dist_container --exclude-task=createDebugFiles --include-task=generateComponentPreload --clean-dest";

async function updateUI5Resources() {
    let wwwResourcesExist = null;
    try {
        wwwResourcesExist = await stat(WWW_RESOURCES_PATH);
    } catch(err) {
        console.error("FETCH UI5 RESOURCES - ERROR - Checking for resources available: " + err.message);
    }

    try {
        // 2. Execute 'ui5 build self-contained -a --exclude-task=createDebugFiles --clean-dest' in root directory ../../
        const stdout = execSync(BUILD_UI5_DIST_COMMAND, {cwd: PROJECT_ROOT_PATH, stdio: 'inherit'});

        if (wwwResourcesExist) {
            // Delete resources folder
            const result = await rimraf(WWW_RESOURCES_PATH);
        }

        // 3. Move entire ../../dist/resources folder to ../../www/resources
        const copy = await fsExtra.copy(DIST_RESOURCES_PATH, WWW_RESOURCES_PATH)
    } catch(err) {
        console.error("FETCH UI5 RESOURCES - ERROR - During building and moving the resources: " + err.message);
    }
}

module.exports = {
    updateUI5Resources: updateUI5Resources
};