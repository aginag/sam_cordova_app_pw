const fs = require("fs");
const fsExtra = require("fs-extra");
const path = require("path");

// Deletable files/folders
const filesFoldersToDelete = ["../../www/components", "../../www/control", "../../www/controller", "../../www/helpers", "../../www/i18n", "../../www/models", "../../www/test", "../../www/view", "../../www/manifest.json", "../../www/Component.js"];

// Delete files/folders
filesFoldersToDelete.forEach(fileOrFolderPath => {
    const fullPath = path.join(__dirname, fileOrFolderPath);

    if (!fs.existsSync(fullPath)) {
        console.info("prepareUI5App - Delete files/folders - File/Folder does not exist: " + fullPath);
        return;
    }

    fsExtra.removeSync(fullPath);
});

// Move Component-Preload from ../../dist -> ../../www
const componentPreloadPath = path.join(__dirname, "../../dist_container/Component-preload.js");

move(componentPreloadPath, path.join(__dirname, "../../www/Component-preload.js")).then(_ => {
    console.info("prepareUI5App - Component preload file moved to www.");
}).catch(err => {
    console.error("prepareUI5App - Component preload file move failed: " + err.message);
});

function move(fromPath, toPath) {
    return new Promise(function(resolve, reject) {
        fsExtra.copy(fromPath, toPath, function(err) {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
}