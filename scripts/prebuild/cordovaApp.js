const fs = require("fs");
const path = require("path");
const cheerio = require("cheerio");

var args = process.argv.slice(2);

if (args.length == 0) {
    throw new Error("no arguments passed");
}

console.log(args);

const version = args[0];
const buildNumber = args[1];
const host = args[2];
const port = args[3];
const protocol = args[4];
const useLocalDevSources = args[5].toLowerCase() == "true";

// 1. Update the ui5 manifest
const manifestPath = path.join(__dirname, "../../www/manifest.json");
const manifestJSON = JSON.parse(fs.readFileSync(manifestPath, "utf8"));

manifestJSON["sap.app"].applicationVersion.version = version.toString();
manifestJSON["sap.app"].applicationVersion.build = buildNumber;
manifestJSON["sap.app"].applicationVersion.env = "";

manifestJSON["sap.app"].MLSetup.hostname = host;
manifestJSON["sap.app"].MLSetup.port = port;
manifestJSON["sap.app"].MLSetup.protocol = protocol;

manifestJSON["sap.app"].SAMConfig.useLocalDevSources = useLocalDevSources;

// 2. Change cordova config.xml
const configPath = path.join(__dirname, "../../config.xml");
const $xml = cheerio.load(fs.readFileSync(configPath), { xmlMode: true });

// Windows and Android Only
if (args[6]) {
    // App name
    $xml("widget").find("name").text(args[6]);
}

// Windows and Android Only
if (args[7]) {
    // App "bundle" identifier
    $xml("widget").attr("id", args[7]);
}

// App version
$xml("widget").attr("version", version);

// Save updated manifest
fs.writeFile(manifestPath, JSON.stringify(manifestJSON), function(err) {
    if(err) {
        throw err;
    }
});
// Save updated config.xml
fs.writeFile(configPath, $xml.xml(), function(err) {
    if(err) {
        throw err;
    }
});
