const fs = require("fs");
const fsExtra = require("fs-extra");
const path = require("path");

const ANDROID_RESOURCES_PATH = path.join(__dirname, "../../platforms/android/app/src/main/res");
const PLUGIN_PATH = path.join(__dirname, "../../plugins/cordova-plugin-audio-recorder");

isDirectory(ANDROID_RESOURCES_PATH);
isDirectory(PLUGIN_PATH);

const pathMap = [
    [path.join(PLUGIN_PATH, "src/android/res/hdpi"), path.join(ANDROID_RESOURCES_PATH, "drawable-hdpi")],
    [path.join(PLUGIN_PATH, "src/android/res/mdpi"), path.join(ANDROID_RESOURCES_PATH, "drawable-mdpi")],
    [path.join(PLUGIN_PATH, "src/android/res/xhdpi"), path.join(ANDROID_RESOURCES_PATH, "drawable-xhdpi")],
    [path.join(PLUGIN_PATH, "src/android/res/xxhdpi"), path.join(ANDROID_RESOURCES_PATH, "drawable-xxhdpi")]
];

var promises = pathMap.map(function(pathPair) {
    return moveDir(pathPair[0], pathPair[1]);
});

Promise.all(promises).then(function(result) {
    console.log("Cordova afterPlatform add hook done");
}).catch(function(err) {
    throw err;
});


function isDirectory(path) {
    if (!fs.lstatSync(path).isDirectory()) {
        throw new Error("Path not found: " + path);
    }
}

function moveDir(fromPath, toPath) {
    return new Promise(function(resolve, reject) {

        if (!fs.existsSync(toPath)) {
            fs.mkdirSync(toPath);
        }

        fsExtra.copy(fromPath, toPath, function(err) {
           if (err) {
               reject(err);
               return;
           }

           resolve();
        });
    });
}