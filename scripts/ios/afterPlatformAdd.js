var xcode = require('xcode');
var fs = require('fs');
var path = require('path');
var q = require('q');

module.exports = function(ctx) {
  if (ctx.opts.platforms.indexOf('ios') < 0) {
    return;
  }
  var projectRoot = ctx.opts.projectRoot;
  var deferral = q.defer();
  var common = ctx.requireCordovaModule('cordova-common');
  var util = ctx.requireCordovaModule('cordova-lib/src/cordova/util');

  var projectName = new common.ConfigParser(util.projectConfig(util.isCordova())).name();
  var projectPath = './platforms/ios/' + projectName + '.xcodeproj/project.pbxproj';
  var project = xcode.project(projectPath);
  function unquote(str) {
    if (str) return str.replace(/^"(.*)"$/, "$1");
  }
  project.parse(function(err) {
    if (err) {
      console.error(err);
      deferral.reject('xcode could not parse project');
    } else{
      project.updateBuildProperty('SWIFT_VERSION', '4.2');
      project.updateBuildProperty('SWIFT_SWIFT3_OBJC_INFERENCE', '"On"');
      project.updateBuildProperty('CLANG_CXX_LIBRARY', '"libc++"');
      project.updateBuildProperty('LD_RUNPATH_SEARCH_PATHS', '"@executable_path/Frameworks"');
      
      fs.writeFileSync(projectPath, project.writeSync());
      deferral.resolve();
    }
  });
  project.parseSync();

  var bridgingHeaderPath = path.join(projectRoot, 'platforms', 'ios', projectName, 'Bridging-Header.h');;
  var content = fs.readFileSync(bridgingHeaderPath, 'utf-8');
  content += '\n';
  content += '#import "UltraLiteDB.h"\n';
  content += '#import "ZipArchive.h"\n';
  content += '#import <CoreLocation/CoreLocation.h>\n';
  console.log('Importing headers');
  fs.writeFileSync(bridgingHeaderPath, content, 'utf-8');
  return deferral.promise;
};