// Karma configuration
// Generated on Tue Oct 01 2019 16:20:09 GMT+0200 (Central European Summer Time)

module.exports = function (config) {
    config.set({
        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['ui5'],

        ui5: {
            url: "www/resources",
            mode: "html",
            testpage: "www/test/unit/unitTests.qunit.html",
            type: "application",
            paths: {
                webapp: "www"
            }

        },

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['ChromeHeadless'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    })
}
