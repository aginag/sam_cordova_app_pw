sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/udb/ColumnStore"],

    function (JSONModel, ColumnStore) {
        "use strict";

        // constructor
        function RequestHelperCore(tableColumnExtensions, queryExtensions) {
            try {
                this.oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance() ? sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle() : null;
            } catch(err) {

            }

            this.tableColumnJSONModel = null;
            this.queryStringJSONModel = null;
            this.columnStore = new ColumnStore();

            this.columnDefinitionsLsKey = "SAM_CORE_COLUMN_DEFINITIONS";

            this.extendColumnsAndQueries(tableColumnExtensions, queryExtensions);
        }

        RequestHelperCore.prototype = {
            extendColumnsAndQueries: function(tableColumnExtensions, queryExtensions) {
                var extendedTableColumns = Object.assign(tableColumnExtensions, this.tableColumns);
                var extendedQueries = Object.assign(queryExtensions, this.queries);

                this.tableColumnJSONModel = new JSONModel(extendedTableColumns);
                this.queryStringJSONModel = new JSONModel(extendedQueries);
            },

            getData: function (modelEntityName, selCriteria, successCallBack, failureCallBack, isAsync, actionName) {
                var queryStr, success_method, isNewObject = false;
                if (actionName == null) {
                    actionName = 'get';
                }

                if (selCriteria.isNewObject) {
                    isNewObject = selCriteria.isNewObject;
                }
                if (jQuery.isArray(modelEntityName)) {
                    queryStr = this.fetchQueriesForEntity(modelEntityName, actionName, selCriteria, isNewObject);
                    success_method = function (data) {
                        // var data = JSON.parse(result);
                        console.log(data.length);
                        successCallBack(data);
                    };
                } else {
                    queryStr = this.fetchQueryForEntity(modelEntityName, actionName, selCriteria);
                    success_method = function (data) {
                        // var data = JSON.parse(result);
                        console.log(data.length);
                        successCallBack(data);
                    };
                }

                var sync_error = function (result) {
                    failureCallBack(result);
                };

                if (jQuery.isArray(modelEntityName)) {
                    if (isAsync) {
                        executeQueriesBackground(queryStr, success_method, sync_error);
                    } else {
                        executeQueries(queryStr, success_method, sync_error);
                    }

                } else {
                    executeQuery(queryStr, success_method, sync_error);

                }
            },

            fetch: function (queries, params) {
                var queryObjects = [];

                var mapParamsToQuery = function (query, paramObject) {
                    for (var key in paramObject) {
                        query.statement = query.statement.replace(new RegExp("@" + key, 'g'), paramObject[key]);
                    }

                    return {
                        tableName: query.id,
                        statement: query.statement
                    };
                };

                if (queries instanceof Array) {
                    queryObjects = queries.map(function (query) {
                        return mapParamsToQuery(query, params);
                    });
                } else {
                    queryObjects = [mapParamsToQuery(queries, params)];
                }

                return new Promise(function (resolve, reject) {
                    executeQueriesBackground(queryObjects, resolve, reject);
                });
            },

            getCount: function (modelEntityName, selCriteria, successCallBack, failureCallBack) {
                var queryStr = this.fetchQueryForEntity(modelEntityName, 'count', selCriteria);
                var success_method = function (data) {
                    // var data = JSON.parse(result);
                    console.log(data.length);
                    successCallBack(data);
                };

                var sync_error = function (result) {
                    failureCallBack(result);
                };

                executeQuery(queryStr, success_method, sync_error);
            },
            setField: function (actionName, modelEntityName, selCriteria, successCallBack, failureCallBack) {
                var updateStr = this.fetchQueryForEntity(modelEntityName, actionName, selCriteria);
                var success_method = function (result) {
                    // var data = JSON.parse(result);
                    console.log(result.length);
                    successCallBack(result);
                };

                var sync_error = function (result) {
                    failureCallBack(result);
                };

                executeUpdate(updateStr, success_method, sync_error);
            },
            searchData: function (modelEntityName, selCriteria, successCallBack, failureCallBack) {
                var isNewObject = false;
                if (selCriteria.isNewObject) {
                    isNewObject = selCriteria.isNewObject;
                }
                if (selCriteria.searchString) {
                    selCriteria.searchString = selCriteria.searchString.replace(/'/g, "''");
                }

                if (isNewObject) {
                    var queryStr = this.fetchQueryForEntity(modelEntityName, 'searchNew', selCriteria);
                } else if (selCriteria.hasOwnProperty("action")) {
                    var queryStr = this.fetchQueryForEntity(modelEntityName, selCriteria.action, selCriteria);
                } else {
                    var queryStr = this.fetchQueryForEntity(modelEntityName, 'search', selCriteria);
                }

                var success_method = function (data) {
                    // var data = JSON.parse(result);
                    console.log(data.length);
                    successCallBack(data);
                };

                var sync_error = function (result) {
                    failureCallBack(result);
                };

                executeQuery(queryStr, success_method, sync_error);
            },
            executeStatementsAndCommit: function (sqlStatements, successCallBack, failureCallBack) {
                var that = this;
                var success_method = function (result) {
                    // console.log(result.length);
                    successCallBack(result);
                };
                var sync_error = function (result) {
                    failureCallBack(result);
                };
                executeStatementsAndCommit(sqlStatements, success_method, sync_error);

            },
            deleteData: function (selCriteria, successCallBack, failureCallBack) {
                var queryStr = this.fetchQueryForEntity('globalDelete', 'delete', selCriteria);
                var success_method = function (result) {
                    // var data = JSON.parse(result);
                    console.log(result.length);
                    successCallBack(result);
                };
                var sync_error = function (result) {
                    failureCallBack(result);
                };
                executeUpdate(queryStr, success_method, sync_error);

            },

            getColumnNames: function (objectKey, entityNames, successCallBack) {
                var that = this;
                var queryStrings = [];

                var success_method = function (data) {
                    // var data = JSON.parse(result);
                    entityNames.forEach(function (entityName) {
                        if (that.tableColumnJSONModel.getProperty('/' + entityName)) {
                            var tableColumns = that.tableColumnJSONModel.getProperty('/' + entityName);
                            data[entityName] = data[entityName].filter(function (column) {
                                return (jQuery.inArray(column.column_name, tableColumns) > -1);
                            });
                        }
                    });

                    console.log(data.length);
                    var columnSQLStrings = that.buildColumnSQLString(entityNames, data);

                    localStorage.setItem(objectKey, JSON.stringify({
                        data: data,
                        columnSQLStrings: columnSQLStrings
                    }));

                    successCallBack(entityNames, data, columnSQLStrings);

                };

                var sync_error = function (result) {
                    MessageBox.alert(result);
                    // Something really went wrong?!!
                };

                if (localStorage.getItem(objectKey) === null) {

                    entityNames.forEach(function (entityName) {
                        var queryStr = that.fetchQueryForEntity("getColumns", 'get', {
                            tableName: entityName
                        });
                        queryStrings.push({
                            tableName: entityName,
                            statement: queryStr
                        });
                    });

                    executeQueries(queryStrings, success_method, sync_error);
                } else {
                    var cachedColumns = JSON.parse(localStorage.getItem(objectKey));
                    ;
                    successCallBack(entityNames, cachedColumns.data, cachedColumns.columnSQLStrings);
                }

            },

            loadColumnDefinitions: function(tables, bHardReset, successCb, errorCb) {
                var that = this;
                var queryStrings = [];

                var load_success = function(data) {
                    that.columnStore.setTables(data);
                    if(successCb !== undefined) {
                        successCb();
                    }
                };

                if (localStorage.getItem(this.columnDefinitionsLsKey) === null || bHardReset) {

                    tables.forEach(function(tableName) {
                        var queryStr = that.fetchQueryForEntity("getColumns", 'get', {
                            tableName: tableName
                        });
                        queryStrings.push({
                            tableName: tableName,
                            statement: queryStr
                        });
                    });

                    executeQueries(queryStrings, function(data) {
                        var columnObject = {};

                        for (var key in data) {
                            columnObject[key] = data[key];
                        }

                        localStorage.setItem(that.columnDefinitionsLsKey, JSON.stringify(columnObject));
                        load_success(columnObject);
                    }, errorCb.bind(null, new Error(that.oBundle.getText("ERROR_LOADING_COLUMN_DEFINITIONS"))));

                } else {
                    var cachedColumnDefinitions = JSON.parse(localStorage.getItem(this.columnDefinitionsLsKey));
                    load_success(cachedColumnDefinitions);
                }
            },

            buildColumnSQLString: function (entityNames, data) {
                var that = this;
                var columnSQLString = "";
                var columnSQLData = {};
                entityNames.forEach(function (entity) {
                    var entityData = data[entity];
                    var tableColumns = that.tableColumnJSONModel.getProperty('/' + entity);

                    if (tableColumns) {
                        entityData = entityData.filter(function (column) {
                            return (jQuery.inArray(column.column_name, tableColumns) > -1);
                        });
                    }
                    entityData.forEach(function (column, index) {
                        if (column.column_name != "MOBILE_ID") {
                            columnSQLString = columnSQLString + column.column_name + ',';
                        }
                    });
                    columnSQLString = columnSQLString.substr(0, columnSQLString.length - 1);
                    columnSQLData[entity] = columnSQLString;
                });

                return columnSQLData;
            },
            fetchQueryForEntity: function (entityName, actionName, selCriteria) {
                var query = this.queryStringJSONModel.getProperty('/' + entityName + '/' + actionName);
                if (!query) {
                    alert(this.oBundle.getText("MISSING_QUERY_INFO_FOR_QUERY") + entityName);
                }
                var queryString = query.query, replaceString;

                var columnsString = '';
                if (query.columns) {
                    if (!this.tableColumnJSONModel.getProperty('/' + query.columns)) {
                        alert(this.oBundle.getText("MISSING_COLUMN_INFO_FOR") + query.columns);
                    }
                    this.tableColumnJSONModel.getProperty('/' + query.columns).forEach(function (column) {
                        columnsString = columnsString + column + ",";
                    });
                    columnsString = columnsString.substr(0, columnsString.length - 1);
                    queryString = queryString.replace('$COLUMNS', columnsString);
                }

                query.params.forEach(function (criteria) {
                    // This whole replace thing is so that '$' in
                    // the replace string can be replaced by '$$',
                    // so that it actually replaces each individual
                    // '$' in queryString by '$'
                    // because '$' is already a special character
                    // while using replace function
                    var replaceString = selCriteria[criteria.value];
                    if (replaceString !== undefined) {
                        replaceString = replaceString.toString().replace('$', '$$$');
                    }

                    queryString = queryString.replace(criteria.parameter, replaceString);
                });

                return queryString;
            },
            fetchQueriesForEntity: function (entityNames, actionName, selCriteria, isNewObject) {
                var queries = [], that = this, query;
                entityNames.forEach(function (entity) {
                    if (!isNewObject) {
                        query = that.fetchQueryForEntity(entity, actionName, selCriteria);
                    } else if (entity === selCriteria.parentTable) {
                        query = that.fetchQueryForEntity(entity, actionName, selCriteria);
                    } else {
                        selCriteria["childTable"] = entity;
                        query = that.fetchQueryForEntity("newObjectData", actionName, selCriteria);
                    }
                    queries.push({
                        tableName: entity,
                        statement: query
                    });
                });
                return queries;
            }
        };

        RequestHelperCore.prototype.tableColumns = {};

        RequestHelperCore.prototype.queries = {
            "getColumns": {
                "get": {
                    "query": "SELECT b.column_name as column_name, b.domain as domain, b.domain_info as count, b.nulls as nullable FROM systable AS a LEFT JOIN syscolumn AS b ON a.object_id=b.table_id WHERE UPPER(a.table_name) = UPPER('$1')",
                    "params": [{
                        "parameter": "$1",
                        "value": "tableName"
                    }]
                }
            },
            "getUUID": {
                "get": {
                    "query": "SELECT NEWID() AS new_uuid",
                    "params": []
                }
            },
            "SAMUsers": {
                "get": {
                    "query": "SELECT * FROM SAM_USERS",
                    "params": []
                },
        		"search" : {
                    "query": "SELECT SU.ID, SU.UDB_DLED, SU.SCENARIO_ID, SU.EMPLOYEE_NUMBER, SU.WORKCENTER_ID, SU.PLANT, SU.STORAGE_LOCATION, SU.FIRST_NAME, SU.LAST_NAME, SU.USER_NAME, DB_PROPERTY('ml_remote_id') AS REMOTE_ID, ZT.RES_FLAG_RESP, ZT.TEAM_GUID " +
                        "FROM SAM_USERS as SU LEFT JOIN ZMRSTEAM_RES AS ZT ON SU.EMPLOYEE_NUMBER = ZT.RES_PERNR WHERE UPPER(user_name) = UPPER('$1')",
        		    "params" : [{
                        "parameter" : "$1",
                        "value" : "userName"
        		    }]
        		},
                "updateDBDownloaded": {
                    "query": "UPDATE SAM_USERS SET udb_dled=1 WHERE UPPER(user_name) = UPPER('$1')",
                    "params": [{
                        "parameter": "$1",
                        "value": "userName"
                    }]
                }
            },
            "SAMScenarioVersions": {
                "get": {
                    "query": "SELECT scv.UI5_FILE_NAME, scv.UI5_COMPONENT_NAME, scv.VERSION, scv.SCENARIO_ID FROM SAM_USERS as su " +
                        "JOIN SAM_USER_UDB as udb on udb.USER_ID = su.ID " +
                        "JOIN SAM_SCENARIO_VERSIONS as scv on scv.ID = udb.SCENARIO_VERSION_ID " +
                        "WHERE UPPER(user_name) = UPPER('$1')",
                    "params": [{
                        "parameter": "$1",
                        "value": "userName"
                    }]
                }
            },
            "SAMSyncMessages": {
                "get": {
                    "query": "SELECT * FROM SAMSyncMessages WHERE ACTION_FLAG!='D' AND ACTION_FLAG!='M'",
                    "params": []
                },
                "delete": {
                    "query": "UPDATE SAMSyncMessages SET ACTION_FLAG='D' WHERE mobile_id='$1'",
                    "params": [{
                        "parameter": "$1",
                        "value": "mobileId"
                    }]
                },
                "deleteAll": {
                    "query": "UPDATE SAMSyncMessages SET ACTION_FLAG='D'",
                    "params": []
                },
                "unreadCount": {
                    "query": "SELECT count(MOBILE_ID) FROM SAMSyncMessages WHERE ACTION_FLAG!='D' AND ACTION_FLAG!='M' AND READ_FLAG != 'X'",
                    "params": []
                },
                "markReadAll": {
                    "query": "UPDATE SAMSyncMessages SET READ_FLAG='X' WHERE READ_FLAG != 'X' AND ACTION_FLAG !='D' AND ACTION_FLAG !='M'",
                    "params": []
                }
            },

            StatusFlow: {
                get: {
                    query: "SELECT sf.ID, sf.ACT_TYPE, sf.ASYNC, sf.CHILD, sf.ICON, sf.STATUS, sf.STATUS_PROFILE, sf.STOPWATCH, sf.AUTOSYNC, ml.USER_STATUS_DESC FROM STATUSFLOW as sf LEFT JOIN USERSTATUSML as ml on ml.STATUS_PROFILE = sf.STATUS_PROFILE AND ml.USER_STATUS_CODE = sf.STATUS and ml.SPRAS = '$1'",
                    params: [{
                        parameter: "$1",
                        value: "language"
                    }]
                }
            },

            Scenario: {
                get: {
                    query: "SELECT sc.NAME, sc.LIVE_TRACK_ENABLED, sc.LIVE_TRACK_FREQUENCY FROM SAM_USERS as su \n" +
                        "JOIN SAM_SCENARIOS as sc on sc.id = su.SCENARIO_ID  \n" +
                        "WHERE su.EMPLOYEE_NUMBER = '$1'",
                    params: [{
                        parameter: "$1",
                        value: "persNo"
                    }]
                }
            },

            SAMUpgradeNeeded: {
                get: {
                    query: "SELECT sudb.UPGRADE_NEEDED FROM SAM_USERS as su " +
                        "JOIN SAM_USER_UDB as sudb on sudb.USER_ID = su.ID " +
                        "WHERE su.EMPLOYEE_NUMBER = '$1'",
                    params: [{
                        parameter: "$1",
                        value: "persNo"
                    }]
                }
            },

            SAMDeltaDone: {
                get: {
                    query: "SELECT sudb.INITIAL_DELTA_DONE FROM SAM_USERS as su " +
                        "JOIN SAM_USER_UDB as sudb on sudb.USER_ID = su.ID " +
                        "WHERE su.EMPLOYEE_NUMBER = '$1'",
                    params: [{
                        parameter: "$1",
                        value: "persNo"
                    }]
                }
            }
        };

        return RequestHelperCore;
    });
