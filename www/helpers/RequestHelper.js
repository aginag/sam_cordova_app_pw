sap.ui.define([ "SAMContainer/helpers/RequestHelperCore", "sap/ui/model/json/JSONModel", "sap/m/MessageBox" ],

function(RequestHelperCore, JSONModel, MessageBox) {
    "use strict";
    var tableColumns = {};
    var queries = {
	    "getColumns" : {
		"get" : {
		    "query" : "SELECT b.column_name as column_name, b.domain as domain FROM systable AS a LEFT JOIN syscolumn AS b ON a.object_id=b.table_id WHERE UPPER(a.table_name) = UPPER('$1')",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "tableName"
		    } ]
		}
	    },
	    "getUUID" : {
		"get" : {
		    "query" : "SELECT NEWID() AS new_uuid",
		    "params" : []
		}
	    },
	    "SAMUsers" : {
		"get" : {
		    "query" : "SELECT * FROM SAM_USERS",
		    "params" : []
		},
		"search" : {
			"query": "SELECT SU.APPL_LOG_LEVEL, SU.CREATED_ON, SU.EMPLOYEE_NUMBER, SU.FIRST_NAME, SU.FULL_NAME, SU.ID, SU.IS_DOING_FIRST_SYNCHRONIZATION, SU.LAST_NAME, " +
				"SU.PLANT, SU.SCENARIO_ID, SU.STORAGE_LOCATION, SU.TRANSAC_LOG_ON, SU.UDB_CREATION_STATUS, SU.UDB_DLED, SU.USER_NAME, SU.WORKCENTER_ID, ZT.RES_FLAG_RESP, ZT.TEAM_GUID " +
				"FROM SAM_USERS as SU LEFT JOIN ZMRSTEAM_RES AS ZT ON SU.EMPLOYEE_NUMBER = ZT.RES_PERNR WHERE UPPER(user_name) = UPPER('$1')",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "userName"
		    } ]
		},
		"updateDBDownloaded" : {
		    "query" : "UPDATE SAM_USERS SET udb_dled=1 WHERE UPPER(user_name) = UPPER('$1')",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "userName"
		    } ]
		}
	    },
	    "SAMScenarioVersions" : {
		"get" : {
		    "query" : "SELECT * FROM SAM_SCENARIO_VERSIONS where ID=(SELECT SCENARIO_VERSION_ID FROM SAM_USERS where UPPER(user_name) = UPPER('$1'))",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "userName"
		    } ]
		}
	    },
	    "SAMSyncMessages" : {
		"get" : {
		    "query" : "SELECT * FROM SAMSyncMessages WHERE ACTION_FLAG!='D' AND ACTION_FLAG!='M'",
		    "params" : []
		},
		"delete" : {
		    "query" : "UPDATE SAMSyncMessages SET ACTION_FLAG='D' WHERE mobile_id='$1'",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "mobileId"
		    } ]
		},
		"deleteAll" : {
		    "query" : "UPDATE SAMSyncMessages SET ACTION_FLAG='D'",
		    "params" : []
		},
		"unreadCount" : {
		    "query" : "SELECT count(MOBILE_ID) FROM SAMSyncMessages WHERE ACTION_FLAG!='D' AND ACTION_FLAG!='M' AND READ_FLAG != 'X'",
		    "params" : []
		},
		"markReadAll" : {
		    "query" : "UPDATE SAMSyncMessages SET READ_FLAG='X' WHERE READ_FLAG != 'X' AND ACTION_FLAG !='D' AND ACTION_FLAG !='M'",
		    "params" : []
		}
	    },
	    "globalDelete" : {
		"delete" : {
		    "query" : "DELETE FROM $1 WHERE mobile_id ='$2'",
		    "params" : [ {
			"parameter" : "$1",
			"value" : "tableName"
		    }, {
			"parameter" : "$2",
			"value" : "mobile_id"
		    } ]
		}
	    }
    };
    var me = new RequestHelperCore(tableColumns, queries);
    return me;
});
