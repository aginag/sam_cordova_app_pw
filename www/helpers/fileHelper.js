sap.ui.define(["sap/m/MessageToast"],

   function(MessageToast) {
      "use strict";
      return {
         b64toBlob: function(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
               var slice = byteCharacters.slice(offset, offset + sliceSize);

               var byteNumbers = new Array(slice.length);
               for (var i = 0; i < slice.length; i++) {
                  byteNumbers[i] = slice.charCodeAt(i);
               }

               var byteArray = new Uint8Array(byteNumbers);

               byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, {
               type: contentType
            });
            return blob;
         },

         blobExtToFileExtension: function (blob) {
            var fileExt = blob.type.split("/")[1];

            switch (fileExt.toLowerCase()) {
               case "msword":
                  return "doc";
               case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                  return "docx";
               case "vnd.ms-excel":
                  return "xls";
               case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                  return "xlsx";
               default:
                  return fileExt;
            }
         },

         downloadFile: function(fileName, attachment) {
            var that = this;
            var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
            this.viewModel.setProperty("/piTitle", oBundle.getText('downloading') + " " + fileName);
            this.viewModel.setProperty("/piPercentValue", 0);
            this.viewModel.setProperty("/piDisplayText", "");
            this.viewModel.setProperty("/piState", "");

            this.getOwnerComponent().setBusyOn();
            var progressHandler = function(result) {
               var isSyncing = that.viewModel.oData.isSyncing;
               //started
               if (!isSyncing) {
                  //open busy Dialog
                  that._busyDialog.open();
                  that.viewModel.setProperty("/isSyncing", true);
               }

               var percent = Math.round((result.bytesTransferred / result.fileSize) * 100);

               that.viewModel.setProperty("/piPercentValue", percent);
               that.viewModel.setProperty("/piDisplayText", percent + "%");
               that.viewModel.setProperty("/piState", result.status);

            };

            var successDownload = function(result) {
               var isSyncing = that.viewModel.oData.isSyncing;
               console.log(result);

               if (result === 1000) {
                  //finished
                  if (isSyncing) {
                     that.viewModel.setProperty("/isSyncing", false);
                  }
               }

               //close busy Dialog
               that._busyDialog.close();
               MessageToast.show(oBundle.getText('downloadedSuccessful'));

               that.getOwnerComponent().setBusyOff();
               that.onAttachmentItemSelection(null, attachment);

            };

            var errorDownload = function() {
               that.viewModel.setProperty("/isSynycing", false);
               //close busyDialog
               that._busyDialog.close();
               that.getOwnerComponent().setBusyOff();
               MessageToast.show(oBundle.getText('downloadError'));
            };

            var cancelDownload = function() {
               cancelFileTransfer(fileName, cancelSuccess, errorDownload);
            };

            var cancelSuccess = function() {
               that.viewModel.setProperty("/isSynycing", false);
               //close busyDialog
               that._busyDialog.close();
               that.getOwnerComponent().setBusyOff();
            };

            this._busyDialog.getEndButton().mEventRegistry.press = [];
            this._busyDialog.getEndButton().attachPress(cancelDownload);

            downloadFile(fileName, successDownload, errorDownload, progressHandler);
         },

      };
   });