sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "sap/m/MessageToast",
        "sap/m/MessageBox",
        "sap/ui/model/Context",
        "SAMMobile/helpers/MediaCapture",
        "SAMMobile/helpers/FileSystem",
        "sap/m/LightBox",
        "sap/m/LightBoxItem",
        "sap/m/Dialog",
        "sap/m/Button",
        "sap/m/Text",
        "SAMMobile/helpers/fileHelper",
        "sap/ui/core/HTML"],

    function (Controller, JSONModel, MessageToast, MessageBox, Context, MediaCapture, FileSystem, LightBox, LightBoxItem, Dialog, Button, Text, fileHelper, HTML) {
        "use strict";
        //var me = null;
        var me = Controller.extend("SAMMobile.controller.common.MediaAttachments", {
            onNewAttachment: {successCb: null, errorCb: null},
            onEditAttachment: {successCb: null, errorCb: null},
            onDeleteAttachment: {successCb: null, errorCb: null},
            fileHelper: fileHelper,
            noCommitMode: false,
            formatAttachmentTypes: null,
            minWidth: 1280,
            minHeight: 1280,
            maxSize: 10000048576, //10MB
            component: null,
            oBundle: null,
            fileSystem: null,
            mediaCapture: null,
            inAppExtensions: ["jpg", "jpeg", "png", "bmp", "gif", "tiff"],
            audioExtensions: ["wav", "amr", "mp3", "m4a"],

            initialize: function (component) {
                me = this;
                this.component = component;
                this.oBundle = this.getOwnerComponent().rootComponent.getModel("i18n").getResourceBundle();
                this.fileSystem = new FileSystem();
                this.mediaCapture = new MediaCapture();
                this._busyDialog = sap.ui.xmlfragment("SAMMobile.view.home.BusyDialog");
                this.viewModel = new JSONModel({
                    //file transfer
                    isSyncing: false,
                    piTitle: "",
                    piPercentValue: 0,
                    piDisplayText: "",
                    piState: ""
                });
                this._busyDialog.setModel(this.viewModel, 'viewModel');
                this._busyDialog.setModel(this.component.getModel("i18n"), 'i18n');
            },
            getOwnerComponent: function(){
                return this.component;
            },
            _onCapturePhotoPress: function (oEvent, oBindingContext, storagePath, successCb) {
                storagePath = storagePath == undefined ? me.fileSystem.getRootDataStorage() : storagePath;
                var that = this;
                var bCtx = oEvent ? oEvent.getSource().getBindingContext() : oBindingContext;
                //var taskObject = bCtx.getObject();

                me.component.setBusyOn();
                me.mediaCapture.capturePhoto(
                    function onSuccess(data) {
                        me.component.setBusyOff();
                        var photo = data[0];
                        that._resizeImage(photo.localURL, photo.name, function (filePath, fileSize) {
                            var device = sap.ui.Device.os;
                            var os = device.name;
                            var tempName = new Date().getTime().toString() + "." + getFileExtension(filePath);
                            switch (os) {
                                case device.OS.ANDROID:
                                case device.OS.IOS:
                                    me.fileSystem.moveFile2(filePath, storagePath,
                                        tempName, onPhotoMoved, onCapturePhotoError);
                                    break;
                                default:
                                    //windows
                                    window.resolveLocalFileSystemURL(filePath, onPhotoMoved, onCapturePhotoError);
                                    break;
                            }


                            function onPhotoMoved(fileEntry) {

                                var filePath = fileEntry.nativeURL;
                                that._renameAttachment(filePath, bCtx,
                                    function onSuccess(fileEntry) {
                                        if(device.name === device.OS.IOS){
                                            fileEntry.nativeURL = storagePath + fileEntry.name;
                                        }
                                        that.onNewAttachment.successCb(bCtx, fileEntry, fileSize, photo.type);
                                        successCb();
                                    },
                                    onCapturePhotoError
                                );

                            }
                        }, onCapturePhotoError);
                    },
                    function onError(error) {
                        //user probably cancelled/closed the capture
                        if (error && error.code === CaptureError.CAPTURE_NO_MEDIA_FILES) {
                            me.component.setBusyOff();
                            MessageToast.show(that.component.i18n_core.getText("MC_NO_IMAGE_CAPTURED"));
                        }
                        else {
                            onCapturePhotoError(error);
                        }
                    }
                );

                function onCapturePhotoError(error) {
                    if (that.onNewAttachment.errorCb instanceof Function) {
                        that.onNewAttachment.errorCb(error);
                    }
                    else {
                        me.component.setBusyOff();
                        var msg = that.component.i18n_core.getText("ERROR_PICTURE_TAKE") + error;
                        MessageBox.alert(msg);
                    }
                }
            },

            _onCaptureVideoPress: function (oEvent, oBindingContext, successCb) {
                var that = this;
                var bCtx = oEvent ? oEvent.getSource().getBindingContext() : oBindingContext;

                var onCaptureError = function (err) {
                    if (that.onNewAttachment.errorCb instanceof Function) {
                        that.onNewAttachment.errorCb(err);
                    }
                    else {
                        me.component.setBusyOff();
                        MessageToast.show(that.component.i18n_core.getText("MC_NO_VIDEO_CAPTURED"));
                    }
                };

                me.component.setBusyOn();
                me.mediaCapture.captureVideo(function (metadata) {
                    var fileData = metadata[0];
                    that._moveFile(fileData.localURL, me.fileSystem.getRootDataStorage(), function (fileEntry) {
                        that._renameAttachment(fileEntry.nativeURL, bCtx, function onSuccess(fileEntry) {
                            that.onNewAttachment.successCb(bCtx, fileEntry, fileData.size);
                            successCb();
                        }, onCaptureError);
                    }, onCaptureError);
                }, onCaptureError);
            },

            _onCaptureAudioPress: function (oEvent, oBindingContext, successCb) {
                var that = this;
                var bCtx = oEvent ? oEvent.getSource().getBindingContext() : oBindingContext;

                var onCaptureError = function (err) {
                    // Ignoring when error is regarding cancel capture
                    if (err.error_code === 1) {
                        me.component.setBusyOff();
                        return;
                    }

                    if (that.onNewAttachment.errorCb instanceof Function) {
                        that.onNewAttachment.errorCb(err);
                    }
                    else {
                        me.component.setBusyOff();
                        MessageToast.show(that.component.i18n_core.getText("ERROR_AUDIO_TAKE"));
                    }
                };

                me.component.setBusyOn();
                me.mediaCapture.captureAudio(function (metadata) {
                    that._moveFile(metadata.full_path, me.fileSystem.getRootDataStorage(), function (fileEntry) {
                        that._renameAttachment(fileEntry.nativeURL, bCtx, function onSuccess(fileEntry) {
                            that.onNewAttachment.successCb(bCtx, fileEntry, 0);
                            successCb();
                        }, onCaptureError);
                    }, onCaptureError);
                }, onCaptureError);
            },

            _formatAttachmentType: function (fileExt) {
                var that = this;
                return that.formatAttachmentTypes(fileExt);
            },

            _moveFile: function(fromPath, toPath, successCb, errorCb) {
                var device = sap.ui.Device.os;
                var os = device.name;
                var tempName = new Date().getTime().toString() + "." + getFileExtension(fromPath);
                switch (os) {
                    case device.OS.ANDROID:
                    case device.OS.IOS:
                        me.fileSystem.moveFile2(fromPath, toPath,
                            tempName, successCb, errorCb);
                        break;
                    default:
                        //windows
                        window.resolveLocalFileSystemURL(fromPath, successCb, errorCb);
                        break;
                }
            },

            _renameAttachment: function (filePath, bCtx, successCb, errorCb) {
                var that = this;

                var dialog = me.mediaCapture.getEditAttachmentDialog();
                dialog.getEndButton()
                    .attachPress(onNewFileNameEntered.bind(that, filePath, dialog, "photo", false));
                dialog.getBeginButton()
                    .attachPress(onNewFileNameEntered.bind(that, filePath, dialog, "photo", true));
                dialog.open();

                function onNewFileNameEntered(filePath, dialog, type, cancelled) {
                    me.component.setBusyOff();
                    if (cancelled) {
                        dialog.close();
                        window.resolveLocalFileSystemURL(filePath, successCb, errorCb);
                        return;
                    }

                    var dialogModel = dialog.getModel("attachment").oData;
                    var newFileName = dialogModel.fileName;

                    newFileName = that.replaceUmlaute(newFileName);
                    
                    //var attPath = oAttachment.localURL;
                    if (dialogModel.fileName === "") {
                        newFileName = type + "_" + new Date().getTime().toString();
                    }
                    // Remove whitespaces from file name as this causes issues on sync
                    newFileName = newFileName.replace(/\s/g,"");
                    var fileExt = getFileExtension(filePath);
                    var newFullFileName = newFileName + "." + fileExt;
                    var patternFileName = new RegExp(/[\\?*.%,\/:<>\"]/g);
                    if (patternFileName.test(newFileName)) {
                        MessageToast.show(me.getOwnerComponent().rootComponent.i18n.getText("fileNameInvalid"));
                        return;
                    }

                    //TODO: refactor
                    //check if entered fileName already exists,
                    //first in the model
                    var attachmentsPath = bCtx.getPath().substr(0, bCtx.getPath().lastIndexOf('/'));

                    var matchAttachments = [];
                    if (attachmentsPath !== '' && bCtx.getModel() !== undefined) {
                        var attachments = bCtx.getModel().getProperty(attachmentsPath);

                        if (attachments != null) {
                            matchAttachments = attachments.filter(function (file) {
                                return file.FILENAME === newFileName && file.FILEEXT === fileExt;
                            });
                        }
                    }
                    if (matchAttachments.length > 0) {
                        MessageToast.show(me.getOwnerComponent().rootComponent.i18n.getText("fileNameExists"));
                    } else {
                        var newFilePath = getFolderPath(filePath) + "/" + newFullFileName;
                        me.fileSystem.exists2(newFilePath,
                            function () {
                                MessageToast.show(me.getOwnerComponent().rootComponent.i18n.getText("fileNameExists"));
                            },
                            renameFileAndContinue
                        );
                    }

                    //rename file
                    function renameFileAndContinue() {
                        if (me.noCommitMode) {
                            //directly close and call callback
                            dialog.close();
                            successCb({name: newFullFileName});
                            return;
                        }
                        //Rename and then call callback
                        me.fileSystem.renameFile2(filePath, newFullFileName,
                            function onSuccess(fileEntry) {
                                dialog.close();
                                successCb(fileEntry);
                            },
                            onRenameError
                        );

                        function onRenameError(error) {
                            dialog.close();
                            var errorMsg = that._getErrorMsg(error);
                            var msg = "Error renaming file " + filePath + " to " + newFullFileName + ": " + errorMsg;
                            MessageBox.alert(msg);
                        }
                    }
                }
            },

            redirectToCorrectUrl: function(url){
                var correctUrl;
                if(url.includes("http://") || url.includes("https://")) {
                    correctUrl = url;
                } else {
                    correctUrl = "https://" + url;
                }
                sap.m.URLHelper.redirect(correctUrl, true);
            },

            openAttachment: function (filePath, fileName, errorCb) {
                var fileExt = getFileExtension(filePath);

                if (me.inAppExtensions.indexOf(fileExt.toLowerCase()) != -1) {
                    //workaround for image caching
                    filePath += "?i=" + Date.now();

                    var lBox = new LightBox();
                    var lBoxItem = new LightBoxItem({
                        imageSrc: filePath,
                        title: fileName
                    });
                    lBox.addImageContent(lBoxItem);
                    lBox.open();
                } else if (me.audioExtensions.indexOf(fileExt.toLowerCase()) != -1) {
                    me._getTaskAttachmentAudioDialog(filePath, fileName, fileExt).open();
                } else {
                    openLocalFile(
                        filePath,
                        function onSuccess() {
                        },
                        function onError(error) {
                            if (errorCb instanceof Function) {
                                errorCb(error);
                            }
                            else {
                                var errorMsg = me._getErrorMsg(error);
                                var msg = "Error opening file: " + fileName + ": " + errorMsg;
                                MessageBox.alert(msg);
                            }
                        }
                    );
                }
            },
            onAttachmentItemSelection: function (oEvent, attachment) {
                var fileName = "";
                var filePath = "";

                if (attachment.ACTION_FLAG !== 'N' && attachment.ACTION_FLAG !== 'O' && attachment.ACTION_FLAG !== 'L') {
                    fileName = (attachment.MOBILE_ID.toUpperCase() + "." + attachment.FILEEXT).replace(/-/g, '');
                }
                else {
                    fileName += attachment.FILENAME + "." + attachment.FILEEXT;
                }

                filePath = attachment.folderPath + fileName;

                me.openAttachment(filePath);
            },

            _onOpenAttachment: function (oEvent, oBindingContext, property) {
                var bCtx = oEvent ? oEvent.getSource().getSelectedItem().getBindingContext() : oBindingContext;
                var attachment = bCtx.getObject();
                var fileName = "";

                if(property){
                    attachment = bCtx.getProperty();
                }

                if (attachment.ACTION_FLAG == "N") {
                    this.onAttachmentItemSelection(oEvent, attachment);
                    return;
                }

                if (attachment.ACTION_FLAG !== 'N' && attachment.ACTION_FLAG !== 'O' && attachment.ACTION_FLAG !== 'L') {
                    fileName = (attachment.MOBILE_ID.toUpperCase() + "." + attachment.FILEEXT).replace(/-/g, '');
                }
                else {
                    fileName += attachment.FILENAME + "." + attachment.FILEEXT;
                }

                //Doc directory
                var filePath = this.fileSystem.getRootDataStorage() + fileName;

                this.fileSystem.exists2(filePath, function() {
                    // File exists
                    this.onAttachmentItemSelection(oEvent, attachment);
                }.bind(this), function() {
                    // File does not exist
                    this.fileHelper.downloadFile.apply(this, [fileName, attachment]);
                }.bind(this));
            },

            _onEditAttachment: function (oEvent, oBindingContext) {
                var that = this;
                var bCtx = oEvent ? oEvent.getSource().getParent().getParent().getBindingContext() : oBindingContext;
                var attachment = bCtx.getObject();
                var filePath = attachment.folderPath + attachment.FILENAME + "." + attachment.FILEEXT;

                me.fileSystem.exists2(filePath,
                    function onSuccess(fileExists) {
                        that._renameAttachment(filePath, bCtx,
                            function onSuccess(fileEntry) {
                                that.onEditAttachment.successCb(bCtx, fileEntry);
                            },
                            onError
                        );
                    },
                    onError
                );

                function onError(error) {
                    if (that.onEditAttachment.errorCb instanceof Function) {
                        that.onEditAttachment.errorCb(error);
                    }
                    else {
                        var errorMsg = that._getErrorMsg(error);
                        var msg = "Error editing file: " + filePath + ": " + errorMsg;
                        MessageBox.alert(msg);
                    }
                }
            },

            _onDeleteAttachment: function (oEvent, oBindingContext, successCb) {
                var bCtx = oEvent ? oEvent.getSource().getParent().getParent().getBindingContext() : oBindingContext;
                var attachment = bCtx.getObject();
                var fileName = "";
                var filePath = "";
                var that = this;
                if (attachment.LINKID) {
                    fileName = attachment.LINKID + ".";
                }

                fileName += attachment.FILENAME + "." + attachment.FILEEXT;
                filePath = attachment.folderPath + fileName;
                if (me.noCommitMode) {
                    that.onDeleteAttachment.successCb(bCtx);
                    return;
                }
                me.fileSystem.deleteByPath(
                    filePath,
                    function onSuccess() {
                        that.onDeleteAttachment.successCb(bCtx);
                        if (successCb) {
                            successCb();
                        }
                    },
                    function onError(error) {
                        if (error === "File not found") {
                            //file no longer present -> call successcallback to remove it from model
                            that.onDeleteAttachment.successCb(bCtx);
                        }
                        else {
                            if (that.onDeleteAttachment.errorCb instanceof Function) {
                                that.onDeleteAttachment.errorCb(error);
                            }
                            else {
                                var errorMsg = that._getErrorMsg(error);
                                var msg = "Error deleting file: " + fileName + ": " + errorMsg;
                                MessageBox.alert(msg);
                            }
                        }
                    }
                );
            },
            _onCloseTaskAttachmentsDialog: function (oEvent) {
                oEvent.getSource().getParent().close();
            },

            _getErrorMsg: function (error) {
                if (error && error.code) {
                    return error.code;
                }
                return error;
            },

            _getTaskAttachmentAudioDialog: function(filePath, fileName, fileExt) {
                var that = this;
                var fullPath = filePath;
                var mime = this.getMimeTypeForFileExt(fileExt);

                var htmlString = "<audio style='width:100%;' controls><source src='" + fullPath + "' type='" + mime + "'>Browser does not supperot</audio>";

                var dialog = new Dialog({
                    title: fileName,
                    content: new HTML({
                        content: htmlString
                    }),
                    beginButton: new Button({
                        text: that.oBundle.getText("close"),
                        press: function() {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function() {
                        this.destroy();
                    }
                });

                return dialog;
            },

            getMimeTypeForFileExt: function(extension) {
                switch (extension.toLowerCase()) {
                    case "mov":
                        return "video/quicktime";
                    case "mp4":
                        return "video/mp4";
                    case "3gp":
                        return "video/3gpp";
                    case "wav":
                        return "audio/wav";
                    case "mp3":
                        return "audio/mpeg3";
                    case "amr":
                        return "audio/amr";
                    default:
                        return "";
                }
            },

            getRandomIntegerUUID: function (length) {
                var result = '';
                var chars = '0123456789';
                for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
                return result;
            },

            getConvertedFilename: function (fileName){
                if(fileName.length > 44){
                    fileName = fileName.slice(0,43);
                }

                fileName = this.replaceUmlaute(fileName);

                return fileName + "_" + this.getRandomIntegerUUID(5);
            },

            replaceUmlaute: function(fileName){

                fileName = JSON.parse(JSON.stringify(fileName));

                const umlautMap = {
                    '\u00dc': 'U',
                    '\u00c4': 'A',
                    '\u00d6': 'O',
                    '\u00fc': 'u',
                    '\u00e4': 'a',
                    '\u00f6': 'o',
                    '\u00df': 's',
                    ' ': '_'
                };

                return fileName
                    .replace(/[\u00dc|\u00c4|\u00d6][a-z]/g, (a) => {
                        const big = umlautMap[a.slice(0, 1)];
                        return big.charAt(0) + big.charAt(1).toLowerCase() + a.slice(1);
                    })
                    .replace(new RegExp('['+Object.keys(umlautMap).join('|')+']',"g"),
                        (a) => umlautMap[a]
                    );
            },

            _resizeImage(filePath, fileName, successCallback, errorCallback) {
                me.fileSystem.getFileSize(filePath, function(fileSize) {
                    successCallback(filePath, fileSize);
                }, function(err) {
                    successCallback(filePath, 0);
                });

                // window.ImageResizer.resize(options,
                //     function(imagePath) {
                //         me.fileSystem.deleteByPath(filePath, function () {
                //             // call resize again
                //             me.fileSystem.getFileSize(imagePath, function(fileSize) {
                //                 successCallback(imagePath, fileSize);
                //             }, errorCallback);
                //         }, errorCallback);
                //     }, function() {
                //         errorCallback('image resize failed');
                //     });
            }
        });
        return me;
    });
