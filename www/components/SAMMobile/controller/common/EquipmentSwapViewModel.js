sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/models/dataObjects/Equipment",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "sap/m/MessageToast"],

    function (JSONModel, ViewModel, Validator, Equipment, Formatter, GenericSelectDialog, MessageToast) {
        "use strict";

        // constructor
        function EquipmentSwapViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.newEquipmentObject = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EquipmentSwapViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        equipment: {
                            id: "",
                            description: "",
                            newId: "",
                            newDescription: ""
                        },
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOldEquipment: {
                value: function (id, description) {
                    this.setProperty("/equipment/id", id);
                    this.setProperty("/equipment/description", description);
                }
            },

            getNewEquipmentObject: {
                value: function () {
                    return this.newEquipmentObject;
                }
            }

        });

        EquipmentSwapViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EquipmentSwapViewModel.prototype.displayEquipmentDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oEquipmentDialog) {
                this._oEquipmentDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.EquipmentSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oEquipmentDialog.display(oEvent, {
                tableName: "EquipmentsInstalled",
                params: {
                    spras: that.getLanguage(),
                    tplnr: that.getProperty("/equipment/TPLNR")
                },
                actionName: "installedAtFuncLoc"
            });

            this._oEquipmentDialog.setTitle(this._component.i18n.getText("SELECT_EQUIPMENT"));
        };

        EquipmentSwapViewModel.prototype.handleEquipmentSelect = function (oEvent) {

            this._oEquipmentDialog.select(oEvent, 'EQUNR', [{
                'newId': 'EQUNR'
            }, {
                'newDescription': 'SHTXT'
            }], this, "/equipment");

        };

        EquipmentSwapViewModel.prototype.handleEquipmentSearch = function (oEvent) {
            this._oEquipmentDialog.search(oEvent, ["id", "description"]);
        };


        EquipmentSwapViewModel.prototype._fetchEquipment = function(EQUNR) {
            return this._requestHelper.fetch({
                id: "equipment",
                statement: "SELECT e.MOBILE_ID, e.EQUNR, e.TPLNR, e.TPLMA, e.TPLMA_SHTXT, e.TPLNR_SHTXT, e.FUNCLOC_DISP, e.FUNCLOC_DISPLAY, e.EQUIPMENT_DISPLAY, e.EQTYP, e.EQART, e.OBJNR, e.MATNR, e.WERK, e.LAGER, e.SHTXT FROM Equipment AS e " +
                    "LEFT JOIN EquipmentMl as eml ON eml.EQUNR = e.EQUNR AND eml.SPRAS='@spras' " +
                    "LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = e.EQTYP AND ec.SPRAS = '@spras' " +
                    "WHERE e.EQUNR = '@equnr' ",
            }, {
                equnr: EQUNR,
                spras: this.getLanguage()
            });
        };

        EquipmentSwapViewModel.prototype.loadEquipment = function (EQUNR, successCb) {
            var that = this;

            var onDataFetched = function (data) {
                var object = new Equipment(data[0].equipment[0], that._requestHelper, this);

                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(object);
                }
            };

            var onError = function(err) {
                this.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_FUNCTIONAL_LOCATIONS_FETCHING")));
            };

            Promise.all([this._fetchEquipment(EQUNR)]).then(onDataFetched.bind(this)).catch(onError.bind(this));

        };

        EquipmentSwapViewModel.prototype.validate = function (equipment) {
            var validator = new Validator(equipment, this.getValueStateModel());

            validator.check("", this._component.i18n.getText("SWAP_EQUIPMENT_EMPTY")).custom(function (value) {
                if (value.newId) {
                    return true;
                } else {
                    return false;
                }
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EquipmentSwapViewModel.prototype.updateEquipment = function (equipment) {
            var that = this;

            try {
                equipment.update(false, function () {
                    that.refresh();
                }, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_UPDATE")));
                });
            } catch(err) {
                this.executeErrorCallback(err);
            }
        };

        EquipmentSwapViewModel.prototype.loadNewEquipmentObject = function (EQUNR) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadEquipment(EQUNR, resolve, reject);
            });
        };

        EquipmentSwapViewModel.prototype.swapEquipment = function (oldEquipmentObject, newEquipmentObject) {
            var equipmentData = this.getData().equipment;
            var switchFuncLocVar = oldEquipmentObject.TPLNR;
            var switchFuncLocDescVar = oldEquipmentObject.TPLNR_SHTXT;
            var switchFuncLocDispVar = oldEquipmentObject.FUNCLOC_DISP;
            var switchFuncLocDisplayVar = oldEquipmentObject.FUNCLOC_DISPLAY;
            var switchSupFunclocVar = oldEquipmentObject.TPLMA;
            var switchSupFunclocDescVar = oldEquipmentObject.TPLMA_SHTXT;

            oldEquipmentObject.TPLNR = newEquipmentObject.TPLNR;
            oldEquipmentObject.TPLNR_SHTXT = newEquipmentObject.TPLNR_SHTXT;
            oldEquipmentObject.FUNCLOC_DISP = newEquipmentObject.FUNCLOC_DISP;
            oldEquipmentObject.FUNCLOC_DISPLAY = newEquipmentObject.FUNCLOC_DISPLAY;
            oldEquipmentObject.TPLMA = newEquipmentObject.TPLMA;
            oldEquipmentObject.TPLMA_SHTXT = newEquipmentObject.TPLMA_SHTXT;
            oldEquipmentObject.HEQUI = "";

            newEquipmentObject.TPLNR = switchFuncLocVar;
            newEquipmentObject.TPLNR_SHTXT = switchFuncLocDescVar;
            newEquipmentObject.FUNCLOC_DISP = switchFuncLocDispVar;
            newEquipmentObject.FUNCLOC_DISPLAY = switchFuncLocDisplayVar;
            newEquipmentObject.TPLMA = switchSupFunclocVar;
            newEquipmentObject.TPLMA_SHTXT = switchSupFunclocDescVar;
            newEquipmentObject.HEQUI = "";

            this.updateEquipment(oldEquipmentObject);
            this.updateEquipment(newEquipmentObject);

            equipmentData.newId = null;
            equipmentData.newDescription = null;

            MessageToast.show(this._component.i18n.getText("SWAP_SUCCESSFULL"));
        };

        EquipmentSwapViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    newId: sap.ui.core.ValueState.None,
                    id: sap.ui.core.ValueState.None,
                    description: sap.ui.core.ValueState.None,
                    newDescription: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EquipmentSwapViewModel.prototype.constructor = EquipmentSwapViewModel;

        return EquipmentSwapViewModel;
    });
