sap.ui.define(["sap/ui/model/json/JSONModel",
        'sap/m/Button',
        'sap/m/Dialog',
        "SAMMobile/helpers/MediaCapture",
        "SAMContainer/helpers/FileSystem",
        "SAMMobile/helpers/formatter",
        "SAMMobile/controller/common/MediaAttachments.controller",
        "sap/ui/core/HTML",
        "SAMMobile/helpers/SignaturePad",
        "SAMMobile/helpers/fileHelper"],

    function (JSONModel, Button, Dialog, MediaCapture, FileSystem, formatter, MediaAttachmentsController, HTML, SignaturePad, fileHelper) {
        "use strict";

        function ChecklistPDFForm(htmlString, title, prefillInfo, savedForm) {
            this.htmlString = htmlString;
            this.prefillInfo = prefillInfo;
            this.savedForm = savedForm;

            this.htmlContent = new HTML({
                content: "<div style='width:100%;height:100%;' id='checklistFrame'></div>"
            });

            this.title = title;
            this.mediaCapture = new MediaCapture();
            this.fileSystem = new FileSystem();
            this.component = sap.ui.getCore().byId("samComponent").getComponentInstance();
            this.oBundle = this.component.getModel("i18n").getResourceBundle();
            //this.oBundle_core = this.component.getModel("i18n_core").getResourceBundle();

            var footerButtons = [new Button({
                text: this.oBundle.getText("close"),
                press: function () {
                    this.pdfDialog.close();
                }.bind(this)
            }), new Button({
                text: this.oBundle.getText("SAVE_PDF"),
                press: function () {
                    //save PDF
                    this.savePDF();
                    this.pdfDialog.close();
                }.bind(this)
            }), new Button({
                text: this.oBundle.getText("GENERATE_PDF"),
                press: function () {
                    //need to generate PDF
                    this.generatePDF();
                    this.pdfDialog.close();
                }.bind(this)
            })
            ];
            this.pdfDialog = new Dialog({
                stretch: true,
                title: title,
                contentWidth: "100%",
                contentHeight: "100%",
                verticalScrolling: sap.ui.Device.os.name === 'iOS' ? true : false,
                content: this.htmlContent,
                afterOpen: this.afterOpen.bind(this),
                afterClose: function() {
                    $("#checklistFrame").remove();
                    this.destroy();
                },
                buttons: footerButtons
            });
            //this.pdfDialog.setModel(oModel);
        }

        ChecklistPDFForm.prototype = {

            display: function (oEvent) {
                // var that = this;
                // var vc = this.viewContext;
                // this.pdfDialog.setBusy(true);
                // this.pdfDialog.parentInput = oEvent.getSource === undefined ? oEvent : oEvent.getSource();
                this.openDialog();
            },

            openDialog: function () {
                var that = this;
                var INTERVAL = 20;
                var ESCAPE_TIME = 1500;
                var windowIsOffset = function () {
                    return window.scrollY >= 20 || window.scrollY <= -20 ? true : false;
                };

                if (windowIsOffset()) {
                    // If the window is offset, we wait until the offset is back to normal before opening the dialog
                    var elapsed = 0;
                    var interval = setInterval(function () {
                        if (!windowIsOffset() || elapsed >= ESCAPE_TIME) {
                            that.pdfDialog.open();
                            clearInterval(interval);
                        }

                        elapsed += INTERVAL;
                    }, INTERVAL);
                } else {
                    this.pdfDialog.open();
                }
            },
            addSignature: function () {
                var dialog = this.getSignatureDialog();
                dialog.open();
                this.signaturePad = new SignaturePad($("canvas")[0]);
                this.onResetSignature();
            },
            generatePDF: function () {
                var me = this;
                var iframeId = "#checklistFrame";
                var iframeBody = $(iframeId);

                iframeBody.find('input[type=checkbox]:checked').each(function () {
                    this.setAttribute('checked', "checked");
                });
                iframeBody.find('input[type=radio]:checked').each(function () {
                    this.setAttribute('checked', "checked");
                });
                iframeBody.find('input[type=date]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=time]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=number]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=tel]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=email]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=text]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.value + '</span>';
                });
                iframeBody.find('input[type=datetime-local]').replaceWith(function () {
                    return '<span class=' + this.className + '>' + moment(this.value).format("DD.MM.YYYY, HH:mm") + '</span>';
                });
                iframeBody.find('select').replaceWith(function () {
                    return '<span class=' + this.className + '>' + this.selectedOptions[0].text + '</span>';
                });

                iframeBody.find("canvas").each(function (a, b, c, d) {
                    var base64String = this.toDataURL().replace(/\r?\n|\r/, "");
                    var image = $(new Image());
                    image.attr("src", base64String);
                    image.attr("width", this.width);
                    image.attr("height", this.height);

                    $(this).replaceWith(image);
                });

                var fileName = this.title + "_" + new Date().getTime();
                var success = function (status) {
                    me.component.setBusyOff();
                    // Do Nothing
                    if (me.successCb !== null && me.successCb !== undefined) {
                        me.successCb(fileName, me.savedForm);
                    }
                };

                var error = function (status) {
                    me.component.setBusyOff();

                    if (me.errorCb !== null && me.errorCb !== undefined) {
                        me.errorCb(new Error(me._component.i18n.getText("ERROR_CHECKLIST_PDF_CREATE")));
                    }
                };

                var device = sap.ui.Device.os;
                var os = device.name;
                var filePath = "";
                var user = me.component.globalViewModel.getSAMUser();

                //fileName =  "test";
                if (os == device.OS.IOS) {
                    filePath = "~/Documents/" + user.getUserName() + "/attachments/" + fileName + ".pdf";
                } else {
                    filePath = user.getUserName() + "/attachments/" + fileName + ".pdf";
                }

                var documentAsString = document.getElementById('checklistFrame').innerHTML;
                
                createPDF(documentAsString, filePath, success, error);
            },

            prefillChecklist: function () {
                const that = this;

                if (this.prefillInfo == undefined || this.prefillInfo.mapping == undefined || !this.prefillInfo.mapping || this.prefillInfo.mapping.length == 0) {
                    return;
                }

                var state = {};
                var prefillFunctionCalls = this.prefillInfo.mapping.filter(function (mapping) {
                    return mapping.id == null;
                });

                this.prefillInfo.mapping
                    .filter(function (mapping) {
                        return mapping.id != null;
                    })
                    .forEach(function (mapping) {
                        var value = mapping.source.map(function (source) {
                            return Object.byString(that.prefillInfo.orderInfo, source);
                        }).join(" ");

                        value = formatter.removeLeadingZeros.apply(this, [value]);

                        state[mapping.id] = value;
                    });

                try {
                    prefillFunctionCalls.forEach(function(functionCall) {
                        var data = Object.byString(that.prefillInfo.orderInfo, functionCall.source[0]);
                        document.getElementById("checklistFrame").contentWindow[functionCall.function](data);
                    });
                } catch (err) {
                    console.error(that._component.i18n.getText("ERROR_FUNCTION_FORM_DOCUMENT_EXECUTE") + err);
                }

                this.restoreSavedState(state);
            },

            restoreSavedState: function (state) {
                var iframeId = "#checklistFrame";
                var iframeBody = $(iframeId);

                for (var id in state) {
                    if (id == undefined) {
                        continue;
                    }

                    const element = iframeBody.find("#" + id);

                    if (element.is("div")) {
                        element.html(state[id]);
                    } else if (element.is("input") && element.attr('type') == "checkbox") {
                        element.prop("checked", state[id]);
                    } else if (element.is("input") && element.attr('type') == "radio") {
                        element.val(state[id].value);
                        element.prop("checked", state[id].checked);
                    }else if(element.is("canvas")){
                        var canvas = element[0];
                        var ctx = canvas.getContext("2d");
                        var image = new Image();
                        image.src = state[id].src;
                        var width = state[id].width;
                        var height = state[id].height;
                        this.drawImg(ctx, image, width, height);
                    }else {
                        element.val(state[id]);
                    }
                }
            },

            async drawImg(ctx, image, width, height){
                return new Promise(resolve => {
                      image.onload = function () {
                            ctx.drawImage(image, 0, 0, width, height);
                            resolve();
                         }
                });
              },

            savePDF: function () {
                var iframeId = "#checklistFrame";
                var iframeBody = $(iframeId);
                var savedState = {};

                var saveInputState = function () {
                    const id = this.getAttribute("id");
                    if (id && id !== undefined && id !== "") {
                        savedState[id] = this.value;
                    }
                };

                var saveDivState = function () {
                    const id = this.getAttribute("id");
                    if (id && id !== undefined && id !== "") {
                        savedState[id] = this.innerHTML;
                    }
                };

                var saveCheckboxRadioButtonState = function () {
                    const id = this.getAttribute("id");
                    if (id && id !== undefined && id !== "") {
                        savedState[id] = this.type == "checkbox" ? this.checked : {
                            value: this.value,
                            checked: this.checked
                        };
                    }
                };

                var saveSignature = function () {
                    const id = this.getAttribute("id");
                    if (id && id !== undefined && id !== "") {
                            var base64String = this.toDataURL().replace(/\r?\n|\r/, "");
                        savedState[id] = {
                            src: this.toDataURL(),
                            width: this.width,
                            height: this.height
                        };
                    }
                };


                iframeBody.find('input[type=checkbox]:checked').each(saveCheckboxRadioButtonState);
                iframeBody.find('input[type=radio]').each(saveCheckboxRadioButtonState);
                iframeBody.find('input[type=date], input[type=time], input[type=number], input[type=tel], input[type=email], input[type=text], input[type=datetime-local], select').each(saveInputState);
                iframeBody.find('div[contenteditable=true]').each(saveDivState);
                iframeBody.find('select').each(saveInputState);

                iframeBody.find("canvas").each(saveSignature);

                var documentAsString = document.getElementById('checklistFrame').innerHTML;

                if (this.saveCb !== null && this.saveCb !== undefined) {
                    if (this.savedForm && this.savedForm !== undefined) {
                        this.savedForm.setState(savedState);
                        this.savedForm.setHTML(documentAsString);

                        this.saveCb(this.savedForm);
                    } else {
                        this.saveCb({
                            formName: this.title,
                            document: documentAsString,
                            state: savedState
                        });
                    }
                }
            },

            afterOpen: function () {
                //  var jsonContent = this.getModel().oData;
                // $(".clFormPrefill").each(function(){
                //   this.value = jsonContent[this.getAttribute('data-mapJSON')];
                // });
                if (this.savedForm && this.savedForm !== undefined) {
                    try {
                    $('#checklistFrame').html(this.savedForm.getHTML());
                    } catch (error) {
                    }finally{
                        this.restoreSavedState(this.savedForm.getState());
                    }
                } else {
                    //document.getElementById('checklistFrame').innerHTML = this.htmlString;
                    $('#checklistFrame').html(this.htmlString);
                    this.prefillChecklist();
                }
            },
            saveSignature: function () {
                var signatureType = "image/jpeg";

                if (this._sDialog.getContent()[1].getValue() !== "") {
                    if (!this.signaturePad.isEmpty()) {
                        var signatureB64 = this.signaturePad.toDataURL(signatureType);
                        // var signatureBlob = fileHelper.b64toBlob(signatureB64.split(",")[1], signatureType, 512);
                        // var mediaObject = this.mediaCapture.getMediaFileObject();
                        // mediaObject.fullPath = signatureB64;

                        // this.signatureModel.oData.push({
                        //     NAME: this._sDialog.getContent()[1].getValue(),
                        //     PATH: mediaObject.fullPath
                        // });
                        this._sDialog.close();
                        this.linkSignatures(signatureB64);
                    } else {
                        MessageToast.show(this.oBundle.getText("WOAddSignatureMsg"));
                    }
                } else {
                    MessageToast.show(this.oBundle.getText("enterValidName"));
                }
            },
            linkSignatures: function (fullPath) {
                $(".clFormSignature").each(function () {
                    this.src = fullPath;
                });
            },
            onResetSignature: function (oEvent) {
                var canvas = $("canvas")[0];
                // // Make it visually fill the positioned parent
                canvas.style.width = '100%';
                canvas.style.height = '90%';
                // ...then set the internal size to match
                canvas.width = canvas.offsetWidth;
                canvas.height = canvas.offsetHeight > 0 ? canvas.offsetHeight : 300;
                this.signaturePad.clear();
            },

            getSignatureDialog: function () {
                var that = this;
                var htmlString = '<canvas></canvas>';
                this._sDialog != null ? this._sDialog.destroy() : null;


                var contentHeight = sap.ui.Device.system.phone ? "80%" : "50%";
                this._sDialog = new Dialog({
                    title: that.oBundle.getText("WOAddSignature"),
                    contentWidth: "90%",
                    contentHeight: contentHeight,
                    content: [
                        new HTML({
                            content: htmlString
                        }),
                        new sap.m.Input({
                            width: '100%',
                            placeholder: that.oBundle.getText("enterName"),
                            value: ''
                        })],
                    buttons: [new Button({
                        text: that.oBundle.getText("reset"),
                        press: that.onResetSignature.bind(that)
                    }), new Button({
                        text: that.oBundle.getText("close"),
                        press: function () {
                            this.getParent().close();
                        }
                    }), new Button({
                        text: that.oBundle.getText("add"),
                        press: function () {
                            that.saveSignature();
                        }
                    })
                    ],
                    afterClose: function () {
                        // this.destroy();
                    }
                });

                this._sDialog.getContent()[1].addStyleClass("signatureInput");
                this._sDialog.setVerticalScrolling(false);
                //this.getView().addDependent(this._sDialog);

                return this._sDialog;
            },

            setSuccessCb: function (successCb) {
                this.successCb = successCb;
            },

            setSaveCb: function (saveCb) {
                this.saveCb = saveCb;
            },

            setErrorCb: function (errorCb) {
                this.errorCb = errorCb;
            }
        };

        ChecklistPDFForm.mode = {
            WITH_SIGNATURE: 1,
            WITHOUT_SIGNATURE: 2
        };

        return ChecklistPDFForm;
    });
