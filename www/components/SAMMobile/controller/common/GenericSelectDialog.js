sap.ui.define(["sap/ui/model/json/JSONModel", "sap/ui/model/Filter"],

   function(JSONModel, Filter) {
      "use strict";

      function GenericSelectDialog(fragmentName, RequestHelper, viewContext, dataModel, mode, multiSelect) {
         this.fragmentName = fragmentName;
         this.viewContext = viewContext;
         this.dataModel = dataModel;
         this.requestHelper = RequestHelper;
         this.mode = mode === undefined ? GenericSelectDialog.mode.CUST_MODEL : mode;

         var growingListConfig = viewContext.getOwnerComponent().getGrowingListSettings("selectDialogs");
         this.growingThreshold = parseInt(growingListConfig.growingThreshold);
         this.loadingChunk = parseInt(growingListConfig.loadChunk);

         this.dialog = sap.ui.xmlfragment(fragmentName, viewContext);
         this.dialog._list.attachUpdateStarted(this.onListGrowing, this);
         this.searchField = this.dialog._searchField;
         this.multiSelect = multiSelect ? multiSelect : false;
         this.dialogModel = new JSONModel({growingThreshold: this.growingThreshold, loadingChunk: this.loadingChunk});
         this.dialog.setModel(this.dialogModel, "selectDialogModel");
      }

      GenericSelectDialog.prototype = {

         display: function(oEvent, modelTable, filterFunction, customModel) {
            var that = this;
            if(oEvent && oEvent.getSource().focus){
        	oEvent.getSource().focus();
            }
            var vc = this.viewContext;
            this.dialog.setBusy(true);

            this.filterFunction = typeof filterFunction === "function" ? filterFunction : null;

            if (customModel !== undefined) {
               
               this.dialog.setModel(customModel);
               this.dialog.setBusy(false);

            } else if (this.mode === GenericSelectDialog.mode.CUST_MODEL) {

               var customizingModel = vc.getOwnerComponent().customizingModel;

               customizingModel.getModel(modelTable, function(model) {
                  model = that._filterData(model);
                  that.dialog.setModel(new JSONModel(model));
                  that.dialog.parentInput = oEvent;
                  that.dialog.setBusy(false);
               });

            } else if (this.mode === GenericSelectDialog.mode.DATABASE) {
               this.query = modelTable;
               this.query.params.startAt = 1;
               this.query.params.searchString = "";

               this._querySearch(function(data) {
                  that.dialog.setModel(new JSONModel(data));
                  that.dialog.setBusy(false);
               });
            }

            this.dialog.parentInput = oEvent.getSource === undefined ? oEvent : oEvent.getSource();
            this.dialog.setMultiSelect(this.multiSelect);
            this.openDialog();
         },

         openDialog: function() {
            var that = this;
            var INTERVAL = 20;
            var ESCAPE_TIME = 1500;
            var windowIsOffset = function() {
               return window.scrollY >= 20 || window.scrollY <= -20 ? true : false;
            };

            if (windowIsOffset()) {
               // If the window is offset, we wait until the offset is back to normal before opening the dialog
               var elapsed = 0;
               var interval = setInterval(function() {
                  if (!windowIsOffset() || elapsed >= ESCAPE_TIME) {
                     that.dialog.open();
                     clearInterval(interval);
                  }

                  elapsed += INTERVAL;
               }, INTERVAL);
            } else {
               this.dialog.open();
            }
         },

         select: function(oEvent, property, modelField, model, sPath, oSelectFn) {
            var that = this, oModel;
            oModel = model !== undefined ? model : this.dataModel;
            sPath = sPath !== undefined ? sPath : "";

            if(this.dialog.getMultiSelect()){
        	var selectedItems = oEvent.getParameter("selectedItems");
        	var selectedContexts = oEvent.getParameter("selectedContexts");
        	if(typeof oSelectFn == "function"){
        	    oSelectFn(selectedItems, selectedContexts);
        	}
            }else{
        	if (modelField === undefined) {
                    modelField = property;
                 }
                 var selectedRow = oEvent.getParameter("selectedItem").getBindingContext().getObject();

                 //oEvent.getSource().parentInput.setValue(selectedRow[property]);
                 
                 if (modelField instanceof Array) {
                    modelField.forEach(function(field) {
                       var key, value;

                       if (typeof field === "object") {
                          for (var key in field) {
                             oModel.setProperty(sPath + "/" + key, selectedRow[field[key]])
                          }
                       } else if (typeof field === "string") {
                          oModel.setProperty(sPath + "/" + field, selectedRow[field])
                       } else {
                          console.log("GENERIC_SELECT_DIALOG (select): modelField array only allows String and Objects");
                       }

                    });
                 } else {
                    oModel.setProperty(sPath + "/" + modelField, selectedRow[property])
                 }
            }
            
            

         },

         search: function(oEvent, searchFields) {
            var sValue = oEvent.getParameter("value");

            if (this.mode === GenericSelectDialog.mode.CUST_MODEL) {

               var aFilters = [];

               if (sValue && sValue.length > 0) {
                  for (var i in searchFields) {
                     aFilters.push(new Filter(searchFields[i], sap.ui.model.FilterOperator.Contains, sValue));
                  }
                  var allFilter = new Filter(aFilters);
               }

               var oBinding = oEvent.getSource().getBinding("items");
               oBinding.filter(allFilter);

            } else if (this.mode === GenericSelectDialog.mode.DATABASE) {
               var that = this;
               this.query.params.searchString = sValue;
               this.query.params.startAt = 1;

               clearTimeout(this.delayedSearch);
               this.delayedSearch = setTimeout(this._querySearch.bind(this, function(data) {
                  that.dialog.getModel().setData(data);
               }), 400);

            }

         },

         setTitle: function(title){
               this.dialog.setTitle(title);
         },

         onListGrowing: function(oEvent) {
            // Based on category select query name
            if (oEvent.getParameter("reason") !== "Growing") {
               return;
            }

            var currentCount = oEvent.getParameter("actual");
            var dialogListModel = this.dialog.getModel();
            var databaseMode = this.mode === GenericSelectDialog.mode.DATABASE;

            if ((currentCount + this.growingThreshold) >= dialogListModel.oData.length && databaseMode) {

               var that = this;
               //load new chunk
               this.dialog.setBusy(true);

               var sQuery = this.searchField.getValue();
               this.query.params.startAt = dialogListModel.oData.length + 1;
               this.query.params.searchString = sQuery;

               this._querySearch(function(data) {
                  that._appendQueryData(data);
                  that.dialog.setBusy(false);
               });

            }
         },

         get: function() {
            return this.dialog;
         },

         getParentInputBindingContext: function(modelIdentifier) {
            return this.dialog.parentInput.getBindingContext(modelIdentifier);
         },

         _querySearch: function(successCb) {
            var that = this;
            this.query.params.noOfRows = this.loadingChunk;

            var actionName = this.query.hasOwnProperty("actionName") ? this.query.actionName : "search";

            this.requestHelper.getData(this.query.tableName, this.query.params, function(data) {
               if (typeof successCb === "function") {
                  data = that._filterData(data);
                  successCb(data);
               }
            }, this._error.bind(this), true, actionName);

         },

         _appendQueryData: function(data) {
            var dialogListModel = this.dialog.getModel();
            var newData = dialogListModel.getProperty("/").concat(data);
            dialogListModel.setProperty("/", newData);
         },

         _error: function(error) {
            this.dialog.setBusy(false);
            this.vc.onSelectDialogError(error) || console.log("ERROR in GenericSelectDialog: " + error);
         },

         _filterData: function(data) {
            if (this.filterFunction) {
               return data.filter(this.filterFunction);
            }

            return data;
         }

      };

      GenericSelectDialog.mode = {
         DATABASE: 1,
         CUST_MODEL: 2
      };

      return GenericSelectDialog;
   });
