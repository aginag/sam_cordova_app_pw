sap.ui.define(["SAMContainer/models/ViewModel",
		"sap/ui/model/Filter",
		"SAMMobile/helpers/formatter"
	],

    function (ViewModel, Filter, formatter) {
        "use strict";

        // constructor
        function FuncLocHirarchyViewModel(requestHelper, globalViewModel, component, displayFunc) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

			this.formatter = formatter;

            this._GROWING_DATA_PATH = "/tree";

            this._oList = null;
            this._oButtons = null;
			this._delayedSearch = null;
			this.displayFunc = displayFunc;
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        FuncLocHirarchyViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                    	toolbarVisible: true,   // set as default at TRUE
                    	PMHierarchy: true,      // set as default at TRUE
                    	FunclocParent: null,
                    	orderInfo: {},
                        tree: [],
                        allFl: [],
                        searchString: "",
                        selectedFuncLoc: null,
						_workCenterId: this._globalViewModel.model.getProperty("/workCenterId"),
                        view: {
                            busy: true,
                            growingThreshold: this.getGrowingListSettings().growingThreshold,
							gasButtonVisible: false
                        }
                    };
                }
            },
            
            getSelectedFuncloc: {
                value: function () {
                    return this.getProperty("/selectedFuncLoc");
                }
            },
            
            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },
            
            getHierarchy: {
            	value: function (){
            		return this.getProperty("/PMHierarchy");
            	}
            },
            
            getFLParent: {
            	value: function (){
            		return this.getProperty("/FunclocParent");
            	}
            },
            
            setTree: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getTree: {
                value: function () {
                    return this._oList;
                }
            },

			setButtons: {
            	value: function (oButtons) {
            		this._oButtons = oButtons;
				}
			},

			getButtons: {
            	value: function () {
					return this._oButtons;
				}
			},
            
            getOrderInfo: {
                value: function () {
                	return this.getProperty("/orderInfo");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },
            
            setHierarchy: {
                value: function (state) {
                    this.setProperty("/PMHierarchy", state);
                }
            },
            
            setToolbar: {
                value: function (state) {
                    this.setProperty("/toolbarVisible", state);
                }
            },
            
            setFLParent: {
                value: function (funcloc) {
                    this.setProperty("/FunclocParent", funcloc);
                }
            },

			setGasButtonVisible: {
				value: function (bVisible) {
					this.setProperty("/view/gasButtonVisible", bVisible);
				}
			},

            
            setOrderInfo: {
                value: function (orderId, orderOper) {
                    this.setProperty("/orderInfo", {orderId : orderId, orderOper : orderOper});
                }
            }
        });

        FuncLocHirarchyViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        FuncLocHirarchyViewModel.prototype.setSelectedFuncloc = function (funcloc) {
            this.setProperty("/selectedFuncLoc", funcloc);

        };        
        
        FuncLocHirarchyViewModel.prototype.setNewData = function (data) {
          
 			var nodes = [];		// 'deep' object structure
    		var nodeMap = {};	// 'map', each node is an attribute

    		if (data) {
				var nodesIn = data;
    			var parentId;

            	for (var i = 0; i < nodesIn.length; i++) {
    				var nodeIn = nodesIn[i];
    				var nodeOut = {};
    				
    				nodeOut = nodeMap[nodeIn.TPLNR];
    				
    				if (nodeOut !== undefined){
    					// the node was already set so we have just to update the values
    					nodeOut.text =	nodeIn.SHTXT;
    					nodeOut.catalogProfile = nodeIn.RBNR;
    				}else{
        				nodeOut = {
        					id: nodeIn.TPLNR,
							text: nodeIn.SHTXT,
							icon: "sap-icon://functional-location",
							type: "F",
							catalogProfile: nodeIn.RBNR
        				};
    				}
         
    				parentId = nodeIn.TPLMA;
    		
    			   if (parentId && parentId.length > 0) {
   					// we have a parent, add the node there
    					// NB because object references are used, changing the
						// node
    					// in the nodeMap changes it in the nodes array too
    					// (we rely on parents always appearing before their
						// children)
    					var parent = nodeMap[nodeIn.TPLMA];

    					if (parent) {
    						if (parent.nodes === undefined){
    							parent.nodes = [];
    						}
    						parent.nodes.push(nodeOut);
    					}else{
    						parent = {
    							id: parentId,
								text:	"",
								catalogProfile: "",
								icon: "sap-icon://functional-location",
								type : "F",
								nodes: []
    						};
    						
    						parent.nodes.push(nodeOut);	
    						nodeMap[parentId] = parent;
    					}
    				} else {
				// there is no parent, must be top level
    					nodes.push(nodeOut);
    				}

    			// add the node to the node map, which is a simple 1-level list
				// of all nodes
    				nodeMap[nodeOut.id] = nodeOut;

    			}
    		}

			this.setProperty("/allFl", nodeMap);
						
			if (this.getProperty("/toolbarVisible")) {
    			// need to simulate onPress for the first button
				var sgButton = this.getButtons();
				if (sgButton) {

					sgButton.removeAllItems();
					var flag = false;
					if(this.getProperty("/_workCenterId") !== 'N42' && this.getProperty("/_workCenterId") !== 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter1}',
							key: 'btn1'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn1");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") !== 'N41' && this.getProperty("/_workCenterId") !== 'N42' && this.getProperty("/_workCenterId") !== 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter2}',
							key: 'btn2'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn2");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") !== 'N41'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter3}',
							key: 'btn3'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn3");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") === 'N42' || this.getProperty("/_workCenterId") === 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter4}',
							key: 'btn4'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn4");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/view/gasButtonVisible") && (this.getProperty("/_workCenterId") === 'N44' || this.getProperty("/_workCenterId") === 'N45' || this.getProperty("/_workCenterId") === 'N46'  || this.getProperty("/_workCenterId") === 'N47')){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter5}',
							key: 'btn5'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn5");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}
				}
    		}
        };

        FuncLocHirarchyViewModel.prototype.setNewDataHierarchy = function (data) {
            
 			var nodes = [];		// 'deep' object structure
    		var nodeMap = {};	// 'map', each node is an attribute

    		// Build Funcloc relashionship
    		if (data.FunclocHierarchy) {
				var nodesIn = data.FunclocHierarchy;		
    			var parentId;

            	for (var i = 0; i < nodesIn.length; i++) {
    				var nodeIn = nodesIn[i];
    				var nodeOut = {};
    				
    				nodeOut = nodeMap[nodeIn.TPLNR];
    				
    				if (nodeOut !== undefined){
    					// the node was already set so we have just to update the values
    					nodeOut.text =	nodeIn.SHTXT;
    					nodeOut.catalogProfile = nodeIn.RBNR;
    				}else{
	    				nodeOut = { id: 	nodeIn.TPLNR,
	    		            		text:	nodeIn.SHTXT,
									catalogProfile: nodeIn.RBNR,
	    		            		icon:   "sap-icon://functional-location",
	    		            		type:   "F"
	    		            	};
    				}
    				// with the sort issue regarding the FL Number
    				// we are not sure that the parent is already 
    				
    				
    				parentId = nodeIn.TPLMA;
    		
    			   if  ((parentId && parentId.length > 0) && (nodeIn.TPLNR !== this.getFLParent())) {
   					// we have a parent, add the node there
    					// NB because object references are used, changing the
						// node
    					// in the nodeMap changes it in the nodes array too
    					// (we rely on parents always appearing before their
						// children)
    					var parent = nodeMap[nodeIn.TPLMA];

    					if (parent) {
    						if (parent.nodes === undefined){
    							parent.nodes = [];
    						}
    						parent.nodes.push(nodeOut);	
    					}else{
    						parent = { id: 	parentId,
        		            		text:	"",
    								catalogProfile: "",
        		            		icon:   "sap-icon://functional-location",
        		            		type:   "F",
        		            		nodes: [] };
    						
    						parent.nodes.push(nodeOut);	
    						nodeMap[parentId] = parent;
    					};
    						
    				} else {
				// there is no parent, must be top level
    					nodes.push(nodeOut);
    				}

    			// add the node to the node map, which is a simple 1-level list
				// of all nodes
    				nodeMap[nodeOut.id] = nodeOut;

    			}
    		}

    		// Add equipment in FL tree
    		if (data.EquipmentHierarchy) {
				var nodesIn = data.EquipmentHierarchy;
    			var nodeOut;
    			var parentId;

            	for (var i = 0; i < nodesIn.length; i++) {
    				var nodeIn = nodesIn[i];
    				nodeOut = { id: 	nodeIn.EQUNR,
    		            		text:	nodeIn.SHTXT,
    		            		catalogProfile: nodeIn.RBNR,
    		            		icon:   "sap-icon://wrench",
    		            		type:   "E" };

    				if (nodeIn.HEQUI && nodeIn.HEQUI.length  > 0){
    					parentId = nodeIn.HEQUI;
    				} else{
    					parentId = nodeIn.TPLNR;
    				}
    		
    			   if  (parentId && parentId.length > 0) {
   					// we have a parent, add the node there
    					// NB because object references are used, changing the
						// node
    					// in the nodeMap changes it in the nodes array too
    					// (we rely on parents always appearing before their
						// children)
    					var parent = nodeMap[parentId];

    					if (parent) {
    						if (parent.nodes === undefined){
    							parent.nodes = [];
    						}
    						parent.nodes.push(nodeOut);
    					}
    				} else {
				// there is no parent, must be top level
    					nodes.push(nodeOut);
    				}

    			// add the node to the node map, which is a simple 1-level list
				// of all nodes
    				nodeMap[nodeOut.id] = nodeOut;
    			}
    		}
    		
    		if (this.getProperty("/toolbarVisible")) {
    			this.setProperty("/allFl", nodeMap);
    			// need to simulate onPress for the first button
				var sgButton = this.getButtons();
				if (sgButton) {

					sgButton.removeAllItems();
					var flag = false;
					if(this.getProperty("/_workCenterId") !== 'N42' && this.getProperty("/_workCenterId") !== 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter1}',
							key: 'btn1'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn1");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") !== 'N41' && this.getProperty("/_workCenterId") !== 'N42' && this.getProperty("/_workCenterId") !== 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter2}',
							key: 'btn2'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn2");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") !== 'N41'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter3}',
							key: 'btn3'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn3");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/_workCenterId") === 'N42' || this.getProperty("/_workCenterId") === 'N43'){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter4}',
							key: 'btn4'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn4");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}

					if(this.getProperty("/view/gasButtonVisible") && (this.getProperty("/_workCenterId") === 'N44' || this.getProperty("/_workCenterId") === 'N45' || this.getProperty("/_workCenterId") === 'N46'  || this.getProperty("/_workCenterId") === 'N47')){
						sgButton.addItem(new sap.m.SegmentedButtonItem({
							text: '{i18n_core>FLBtFilter5}',
							key: 'btn5'
						}));

						if(!flag){
							sgButton.setSelectedKey("btn5");
							sgButton.fireSelectionChange();
							flag = true;
						}
					}
				}
    		} else {
    			this.setProperty("/tree", nodes);
    		}
        };
        
        FuncLocHirarchyViewModel.prototype.onTreeSelection = function(oEvent) {

			 var bindingContext = oEvent.getParameter("listItem").getBindingContext("funcLocTreeViewModel");
			 var funcloc = bindingContext.getObject();

			 this.setSelectedFuncloc(funcloc);
           
        };
        
        FuncLocHirarchyViewModel.prototype.handleNavPress = function(oEvent){
        	
			 var bindingContext = oEvent.getSource().getBindingContext("funcLocTreeViewModel");
			 var funcloc = bindingContext.getObject();
			 var orderInfo = this.getOrderInfo();
			 if ( funcloc.type === "F" ){
		            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
		                route: "workOrdersCustomRoute",
		                params: {
		                    query: {
		                        routeName: "workOrderDetailRoute",
		                        params: JSON.stringify({
		                            id: orderInfo.orderId,
		                            operId: orderInfo.orderOper,
		                            persNo: this.getUserInformation().personnelNumber ? this.getUserInformation().personnelNumber : '00000000'
		                        })
		                    }
		                }
		            }, {
		                route: "technicalObjectsCustomRoute",
		                params: {
		                    query: {
		                        routeName: "technicalObjectsDetailRoute",
		                        params: JSON.stringify({
		                            id: funcloc.id,
		                            objectType: "1"
		                        })
		                    }
		                }
		            });
			 }else if ( funcloc.type === "E" ){
		            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
		                route: "workOrdersCustomRoute",
		                params: {
		                    query: {
		                        routeName: "workOrderDetailRoute",
		                        params: JSON.stringify({
		                            id: orderInfo.orderId,
		                            operId: orderInfo.orderOper,
		                            persNo: this.getUserInformation().personnelNumber ? this.getUserInformation().personnelNumber : '00000000'
		                        })
		                    }
		                }
		            }, {
		                route: "technicalObjectsCustomRoute",
		                params: {
		                    query: {
		                        routeName: "technicalObjectsDetailRoute",
		                        params: JSON.stringify({
		                            id: funcloc.id,
		                            objectType: "0"
		                        })
		                    }
		                }
		            });				 
			 }
        };

        FuncLocHirarchyViewModel.prototype._createTreeForBtn1 = function (data) {
			var filterNodes= [];
			var nodes = this.getProperty("/allFl");
			var nodeOut;

			for (var i = 0; i < data.FunctionalLocations.length; i++) {
				nodeOut = nodes[data.FunctionalLocations[i].TPLNR];

				if (nodeOut){
					if (!this.getHierarchy()){
						if (nodeOut.nodes) {
							// we need to keep only one level below the main FL
							nodeOut.nodes.forEach(function(subNode) {
								subNode.nodes = [];
							});
						}
					}
					filterNodes.push(nodeOut);
				}
			}

			return filterNodes;
		};

        FuncLocHirarchyViewModel.prototype._createTreeForBtn2 = function (data) {
			var filterNodes= [];
			var nodes = this.getProperty("/allFl");
			var nodeOut;

			for (var i = 0; i < data.FunctionalLocations.length; i++) {
				nodeOut = nodes[data.FunctionalLocations[i].TPLNR];

				if (nodeOut) {
					if (nodeOut.nodes) {
						// Filter out the children that matches the pattern "O-___-S-AN"
						nodeOut.nodes = nodeOut.nodes.filter(function(subNode) {
							return !subNode.id.match(/O-\d\d\d-S-AN/);
						});
					}
					filterNodes.push(nodeOut);
				}
			}

			return filterNodes;
		};

		FuncLocHirarchyViewModel.prototype._createTreeForBtn3 = function (data) {
			var nodes = this.getProperty("/allFl");
			var filterNodes= [];
			var nodeOut;
			var filteredFunctionLocations;
			
			if(this.displayFunc){
				filteredFunctionLocations = data.FunctionalLocations.filter(function(currentFuncLoc){
					// Filtering out for 'Leitungen': defined in PFAL-1025
					if(currentFuncLoc.TPLNR.includes('S-L-')) {
						if(currentFuncLoc.TPLNR.includes('-PO-')) {
							if(!currentFuncLoc.TPLNR.includes('-IH-KA-EN-')){
								return currentFuncLoc;
							}
						}else{
							return currentFuncLoc;
						}
					}else{
						return currentFuncLoc;
					}
				});
			}else{
				filteredFunctionLocations = data.FunctionalLocations;
			}
			for (var i = 0; i < filteredFunctionLocations.length; i++) {
				nodeOut = nodes[filteredFunctionLocations[i].TPLNR];

				if (nodeOut){
					if (filteredFunctionLocations[i].TPLMA) {
						var parent = filterNodes.find( parent => parent.id === filteredFunctionLocations[i].TPLMA);
						if (!parent){
							if (nodeOut.nodes !== undefined){
								nodeOut.nodes = [];
								if (nodeOut.id.match(/S-L-\d\d\d-PO-\d\d\d-IH$/)) {
									filterNodes.push(nodeOut);
								}
							}
						} else {
							if(this.displayFunc){
								var nodeFiltered = nodeOut.nodes.filter(function(currentFuncLoc){
									if(currentFuncLoc.id.includes('S-L-')) {
										if(currentFuncLoc.id.includes('-PO-')) {
											if(!currentFuncLoc.id.includes('-IH-KA-EN')){
												return currentFuncLoc;
											}else{
												currentFuncLoc.nodes = [];
												return currentFuncLoc;
											}
										}else{
											return currentFuncLoc;
										}
									}else{
										return currentFuncLoc;
									}
								});
								nodeOut.nodes = nodeFiltered;
							}
							parent.nodes.push(nodeOut);
						}
					} else {
						if (nodeOut.nodes !== undefined){
							nodeOut.nodes = [];
						}
						filterNodes.push(nodeOut);
					}
				}
			};

			return filterNodes;
		};

		FuncLocHirarchyViewModel.prototype._createTreeForBtn4 = function (data) {
			var filterNodes= [];
			var nodes = this.getProperty("/allFl");
			var nodeOut;

			for (var i = 0; i < data.FunctionalLocations.length; i++) {
				nodeOut = nodes[data.FunctionalLocations[i].TPLNR];

				if (nodeOut){
					filterNodes.push(nodeOut);
				}
			}

			return filterNodes;
		};

        FuncLocHirarchyViewModel.prototype.onButtonPress = function(oEvent) {
			this.setBusy(true);

        	var that = this;
        	var statement = null;
        	var id = oEvent.getSource().getSelectedKey();
        	var wrkCtr = this.getUserInformation().workCenterId.trimEnd();

        	if(id === "btn5") {
        		if(oEvent.getSource().getParent().getParent().getParent().getModel("notificationModel")) {
					oEvent.getSource().getParent().getParent().getParent().getModel("notificationModel").setProperty("/notification/FUNCT_LOC", "D-" + wrkCtr);
				}
				if(oEvent.getSource().getParent().getParent().getParent() instanceof sap.m.Dialog) {
					oEvent.getSource().getParent().getParent().getParent().close();
				}
			}

            var onDataFetched = function(data) {
				this.setSizeLimit(data.FunctionalLocations.length);
            	switch (id) {
					case "btn1":
						that.setProperty("/tree", this._createTreeForBtn1(data));
						break;
					case "btn2":
						that.setProperty("/tree", this._createTreeForBtn2(data));
						break;
					case "btn3":
						that.setProperty("/tree", this._createTreeForBtn3(data));
						break;
					case "btn4":
						that.setProperty("/tree", this._createTreeForBtn4(data));
						break;
				}
            	this.getTree().removeSelections();
            	this.getTree().collapseAll();
				that.setBusy(false);
            };

            var onError = function(err) {
                this.executeErrorCallback(new Error("Error while fetching Functional Locations"));
            };

        	if (id==="btn1") {
        		switch (wrkCtr){
        		case "N44":
        		case "N45":
        		case "N46":
        		case "N47":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
                    + "FROM FUNCTIONLOC AS FL "
					+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                    + "WHERE UPPER(FL.TPLNR) LIKE UPPER('O-___-S-AN') "
					+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                    + "ORDER BY FL.TPLNR ASC ";
        			break;
        		case "N41":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
                        + "FROM FUNCTIONLOC AS FL "
						+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                        + "WHERE UPPER(FL.TPLNR) LIKE UPPER('S-A-_____-IH') "
						+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                        + "ORDER BY FL.TPLNR ASC ";
        			break;
        		}
        	} else if (id==="btn2") {
        		switch (wrkCtr){
        		case "N44":
        		case "N45":
        		case "N46":
        		case "N47":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
                    + "FROM FUNCTIONLOC AS FL "
					+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                    + "WHERE UPPER(FL.TPLNR) LIKE UPPER('O-___-S') "
					+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                    + "ORDER BY FL.TPLNR ASC ";
        			break;
        		}
        	} else if (id==="btn3") {
        		switch (wrkCtr){
        		case "N42":
        		case "N43":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
                    + "FROM FUNCTIONLOC AS FL "
					+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                    + "WHERE ( UPPER(FL.TPLNR) LIKE UPPER('S-L-110-PO-___-IH') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-110-PO-___-IH-SK') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-110-PO-___-IH-TR') ) "
					+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                    + "ORDER BY FL.TPLNR ASC ";
        			break;
        		case "N44":
        		case "N45":
        		case "N46":
        		case "N47":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
					+ "FROM FUNCTIONLOC AS FL "
					+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                    + "WHERE ( UPPER(FL.TPLNR) LIKE UPPER('S-L-020-PO-___-IH') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-020-PO-___-IH-FL') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-020-PO-___-IH-KA') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-005-PO-___-IH') "
                    + "OR UPPER(FL.TPLNR) LIKE UPPER('S-L-005-PO-___-IH-KA') ) "
					+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                    + "ORDER BY FL.TPLNR ASC ";
        			break;
        		}
        	} else if (id==="btn4") {
        		switch (wrkCtr){
        		case "N42":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
						+ "FROM FUNCTIONLOC AS FL "
						+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                        + "WHERE UPPER(FL.TPLNR) LIKE UPPER('O-___-L') "
						+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
                        + "ORDER BY FL.TPLNR ASC ";
            			break;
        		case "N43":
        			statement = "SELECT FL.TPLNR, FL.TPLMA, FL.RBNR "
						+ "FROM FUNCTIONLOC AS FL "
						+ "LEFT JOIN WorkCenterMl AS WC ON FL.WKCTR = WC.OBJID "
                        + "WHERE UPPER(FL.TPLNR) LIKE UPPER('O-___-L') "
						+ "AND WC.ARBPL = '" + wrkCtr + "' AND WC.SPRAS = '" + this.getLanguage() + "' "
						+ "ORDER BY FL.TPLNR ASC ";
            			break;
        		}
        	}


        	if (statement){
        		this._fetchFuncLocs(statement).then(onDataFetched.bind(this)).catch(onError.bind(this));
        	} else {
        		this.setProperty("/tree", [] );
				this.setBusy(false);
        	}

       };

        FuncLocHirarchyViewModel.prototype.onTreeSearch = function(oEvent) {
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this._search.bind(this, oEvent.getSource().getValue()), 400);
        };

        FuncLocHirarchyViewModel.prototype._search = function(sValue) {
            // MODEL SEARCH
            var aFilters = [];

            if (sValue && sValue.length > 0) {
                aFilters.push(new Filter("id", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("text", sap.ui.model.FilterOperator.Contains, sValue));
                var allFilter = new Filter(aFilters);
            }

            var oBinding = this.getTree().getBinding("items");
            oBinding.filter(allFilter);
            
            if (aFilters.length > 0){ 
            	this.getTree().expandToLevel(5);
            }
            else{
            	this.getTree().collapseAll();
            }
        };

        FuncLocHirarchyViewModel.prototype.loadData = function (successCb) {
            var that = this;
        	// we need to know when you are on hierarchy view (funcloc/equipments) or only on funcloc tree
        	var Hierarchy = this.getHierarchy();
        	
            var onDataFetched = function(data) {
            	if (Hierarchy){
            		that.setNewDataHierarchy(data);
            	}else{
            		that.setNewData(data.FunctionalLocations);
            	}
				that._needsReload = false;
            	if (successCb){
            		successCb();
            	}
                
            };

            var onError = function(err) {
            	if (Hierarchy){
            		that.executeErrorCallback(new Error("Error while fetching Hierarchy"));
            	}else{
            		this.executeErrorCallback(new Error("Error while fetching Functional Locations"));
            	}
            };

            if (Hierarchy){
                this._requestHelper.getData(["FunclocHierarchy", "EquipmentHierarchy"], {
                	funcLoc: this.getFLParent(),                   
                }, onDataFetched, onError, true);
            }else{
            // Fetch Funclocations
            	this._fetchFuncLocs().then(onDataFetched.bind(this)).catch(onError.bind(this));
            }
        };
          
        
        FuncLocHirarchyViewModel.prototype._fetchFuncLocs = function(statement) {
        	
        	var sqlStatement = "";
			  if (statement){
				  sqlStatement = statement;
			  } else{
				  sqlStatement = "SELECT TPLNR, SHTXT, TPLMA, RBNR "
			          + "FROM FUNCTIONLOC "
			          + "WHERE UPPER(TPLNR) LIKE UPPER('%@searchString%') OR UPPER(SHTXT) LIKE UPPER('%@searchString%') " 
			          + "ORDER BY TPLNR ASC "
			 }   	
        	
	            return this._requestHelper.fetch({
	                id: "FunctionalLocations",
	                statement: sqlStatement
	            }, {
	                searchString: this.getSearchString()
	            });
        	
        };
        
        FuncLocHirarchyViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.loadData(successCb);
        };

        FuncLocHirarchyViewModel.prototype.resetModel = function () {
            this._needsReload = true;
            this.setBusy(true);
        };

        FuncLocHirarchyViewModel.prototype.constructor = FuncLocHirarchyViewModel;

        return FuncLocHirarchyViewModel;
    });
