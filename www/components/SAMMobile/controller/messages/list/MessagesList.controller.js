sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/RequestHelper",
        "SAMMobile/helpers/formatter",
        "sap/ui/model/Sorter",
        "sap/ui/model/Filter",
        "sap/m/MessageBox"
    ],

    function (Controller, JSONModel, RequestHelper, formatter, Sorter, Filter, MessageBox) {
        "use strict";

        return Controller.extend("SAMMobile.controller.messages.list.MessagesList", {
            requestHelper: RequestHelper,
            formatter: formatter,

            onInit: function () {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("messagesRoute").attachPatternMatched(this._onRouteMatched, this);
                sap.ui.getCore().getEventBus().subscribe("messageListView", "refreshRoute", this.onRefreshRoute, this);
                this.messagesModel = this.getOwnerComponent().getModel("messagesModel");
                this.messagesModel.needsReload = true;

                this.viewModel = new JSONModel({
                    selectedTabKey: "all"
                });
                this.globalViewModel = this.getOwnerComponent().globalViewModel;

                this.getView().setModel(this.viewModel, "viewModel");
                this.oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                this.oTable = this.byId("messagesTable");
                this.oTabBar = this.byId("filterMessagesTabBar");
            },

            _onRouteMatched: function (oEvent) {
                this.globalViewModel.setHeaderText(this.oBundle.getText('Messages'));
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));
                this.oTabBar.setSelectedKey("all");
                if (this.messagesModel.needsReload == true) {
                    this.reloadData();
                }

                this.markAllRead();
            },
            onExit: function () {
                sap.ui.getCore().getEventBus().unsubscribe("messageListView", "refreshRoute", this.onRefreshRoute, this);
            },
            onRefreshRoute: function () {
                this.reloadData();
            },
            reloadData: function () {
                var that = this;
                this.getOwnerComponent().setBusyOn();

                var spras = this.globalViewModel.getActiveUILanguage();

                this.requestHelper.getData("SAMSyncMessages", {}, function (data) {

                    data.forEach(function (message) {
                        if (message.CREATED_ON !== "") {
                            if (spras === "D") {
                                message.CREATED_ON = moment(message.CREATED_ON).format("DD.MM.YYYY, HH:mm");
                            } else {
                                message.CREATED_ON = moment(message.CREATED_ON).format("MM/DD/YYYY, HH:mm");
                            }
                        }
                    });

                    that.messagesModel.setProperty('/', data);
                    that.getOwnerComponent().setBusyOff();
                }, function (error) {
                    MessageBox.alert(error);
                });
            },
            onItemSelection: function (oEvent) {
                var message = oEvent.getParameter('listItem').getBindingContext('messagesModel').getObject();
                //this.oTable.getSelectedItem().getBindingContext("messagesModel").getObject();
                var routeName, objectType, query;
                switch (message.TABLE_NAME) {
                    case 'Orders':
                        this.oRouter.navTo("workOrderDetailRoute", {
                            id: message.OBJECT_KEY
                        });
                        break;
                    case 'Equipment':
                    case 'FuncLoc':
                        var objectId = message.OBJECT_KEY;
                        objectType = (message.TABLE_NAME === 'Equipment') ? 'equipments' : 'funcLocations';
                        if (objectId == "") {
                            query = {
                                mobile_id: message.OBJECT_MOBILE_ID,
                                forceReload: false
                            };
                            objectId = " ";
                        }

                        this.oRouter.navTo("technicalObjectDetailRoute", {
                            objectType: objectType,
                            objectId: encodeURIComponent(objectId),
                            query: query
                        });
                        break;
                    case 'NotificationHeader':
                        this.oRouter.navTo("notificationDetailRoute", {
                            id: message.OBJECT_KEY
                        });
                        break;
                    default:

                        var item = oEvent.getSource().getSelectedItem();
                        if (item.getHighlight() !== sap.ui.core.MessageType.None) {
                            item.setHighlight(sap.ui.core.MessageType.None);
                            var unreadMessagesCount = this.globalViewModel.model.getProperty("/unreadMessages");
                            this.globalViewModel.model.setProperty("/unreadMessages", unreadMessagesCount - 1);
                        }

                        routeName = "";
                        break;
                }

            },

            markAllRead: function () {
                this.requestHelper.setField("markReadAll", "SAMSyncMessages", {}, function () {
                }, function (error) {
                    MessageBox.alert(error);
                });
            },

            onDelete: function (oEvent) {
                var message = oEvent.getParameter('listItem').getBindingContext('messagesModel').getObject(),
                    that = this;
                this.getOwnerComponent().setBusyOn();
                this.requestHelper.setField("delete", "SAMSyncMessages", {mobileId: message.MOBILE_ID}, function (data) {
                    that.reloadData();
                    sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                }, function (error) {
                    MessageBox.alert(error);
                });
            },
            onTabSelect: function (oEvent) {
                var selectedKey = oEvent.getParameter("selectedKey");
                var oBinding = this.oTable.getBinding("items");
                var aFilter = [];

                switch (selectedKey) {
                    case "all":
                        aFilter = [];
                        break;
                    case "error":
                        var filter = new Filter("STATUS", sap.ui.model.FilterOperator.EQ, 'E');
                        aFilter.push(filter);
                        break;
                    case "warning":
                        var filter = new Filter("STATUS", sap.ui.model.FilterOperator.EQ, 'W');
                        aFilter.push(filter);
                        break;
                    case "success":
                        var filter = new Filter("STATUS", sap.ui.model.FilterOperator.EQ, 'S');
                        aFilter.push(filter);
                        break;
                    default:
                        break;
                }

                oBinding.filter(aFilter, "Application");
            },

            onTableSearch: function (oEvt) {
                var sQuery = oEvt.getSource().getValue();
                var aFilters = [];
                if (sQuery && sQuery.length > 0) {
                    var filter1 = new Filter("STATUS", sap.ui.model.FilterOperator.Contains, sQuery);
                    var filter2 = new Filter("TEXT", sap.ui.model.FilterOperator.Contains, sQuery);
                    var allFilter = new Filter([filter1, filter2]);
                }
                var binding = this.oTable.getBinding("items");
                binding.filter(allFilter, "Application");
            },
            handleDeleteAll: function (oEvent) {
                var that = this;
                this.getOwnerComponent().setBusyOn();
                this.requestHelper.setField("deleteAll", "SAMSyncMessages", {}, function () {
                    that.reloadData();
                    sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                }, function (error) {
                    MessageBox.alert(error);
                });
            }

        });
    });
