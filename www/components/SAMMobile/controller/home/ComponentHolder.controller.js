sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
    "use strict";

    return Controller.extend("SAMMobile.controller.home.ComponentHolder", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrders").attachPatternMatched(this._onWorkOrdersRouteMatched, this);
            this.oRouter.getRoute("workOrderOperations").attachPatternMatched(this._onWorkOrdersRouteMatched, this);
            this.oRouter.getRoute("notifications").attachPatternMatched(this._onNotificationsRouteMatched, this);
            this.oRouter.getRoute("technicalObjectsList").attachPatternMatched(this._onTechnicalObjectsRouteMatched, this);
            this.oRouter.getRoute("inventoryList").attachPatternMatched(this._onInventoryRouteMatched, this);
            this.oRouter.getRoute("partnersList").attachPatternMatched(this._onPartnersRouteMatched, this);
            this.oRouter.getRoute("timesheetCalendar").attachPatternMatched(this._onTimesheetRouteMatched, this);
            this.oRouter.getRoute("equipmentCreation").attachPatternMatched(this._onEquipmentCreationRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.components = [];

            this.isInitialized = false;
        },

        onExit: function (oEvent) {
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onLogout: function () {
            this.isInitialized = false;
            var samConfig = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig;

            this.getOwnerComponent().scenarioComponent.destroy();
            this.getOwnerComponent().publication = this.getOwnerComponent().getManifestObject().getEntry("sap.app").MLSetup.publication;
            //deregister path
            if (samConfig.loadScenarioFromUDB) {
                jQuery.sap.registerModulePath(this.getOwnerComponent().getModel("globalViewModel").getProperty('/scenarioComponentName'), {url: null});
            }

            this.components.forEach(function(component) {
                component.destroy();
            });
            this.components = [];
        },

        _onWorkOrdersRouteMatched: function (oEvent) {
            const workOrdersComponent = {
                id: "woCC",
                name: "WorkOrders"
            };

            this._loadComponent(workOrdersComponent);
        },

        _onNotificationsRouteMatched: function (oEvent) {
            const notificationsComponent = {
                id: "noCC",
                name: "Notifications"
            };

            this._loadComponent(notificationsComponent);
        },

        _onTechnicalObjectsRouteMatched: function (oEvent) {
            const technicalObjectComponent = {
                id: "toCC",
                name: "TechnicalObjects"
            };

            this._loadComponent(technicalObjectComponent);
        },

        _onInventoryRouteMatched: function (oEvent) {
            const inventoryComponent = {
                id: "inCC",
                name: "Inventory"
            };

            this._loadComponent(inventoryComponent);
        },

        _onPartnersRouteMatched: function (oEvent) {
            const partnerComponent = {
                id: "partnerCC",
                name: "Partners"
            };

            this._loadComponent(partnerComponent);
        },

        _onTimesheetRouteMatched: function (oEvent) {
            const timesheetComponent = {
                id: "timesheetCC",
                name: "Timesheet"
            };

            this._loadComponent(timesheetComponent);
        },

        _onEquipmentCreationRouteMatched: function (oEvent) {
            const equipmentCreationComponent = {
                id: "equipmentCreationCC",
                name: "Equipment"
            };

            this._loadComponent(equipmentCreationComponent);
        },

        _loadComponent: function(component) {
            const currentVisibleComponent = this._getVisibleComponent();

            if (currentVisibleComponent && currentVisibleComponent.getId() !== component.id) {
                currentVisibleComponent.setVisible(false);
            }

            if (!this._componentExists(component.id)) {
                const toComponent = new sap.ui.core.ComponentContainer(component.id, {
                    name: "SAMMobile.components." + component.name,
                    height: "100%",
                    manifest: true,
                    async: true
                });

                toComponent.placeAt(this.getView().getId() + "--componentHolder");

                this.components.push(toComponent);
            }

            const toComponent = this._getComponentById(component.id);

            if (toComponent) {
                toComponent.setVisible(true);
            }
        },

        _componentExists: function (componentId) {
            const foundComponent = this.components.filter(function (component) {
                return component.getId() == componentId;
            });

            return foundComponent.length > 0 ? true : false;
        },

        _getComponentById: function (componentId) {
            const foundComponent = this.components.filter(function (component) {
                return component.getId() === componentId;
            });

            return foundComponent.length > 0 ? foundComponent[0] : null;
        },

        _getVisibleComponent: function () {
            const visibleComponent = this.components.filter(function (component) {
                return component.getVisible();
            });

            return visibleComponent.length > 0 ? visibleComponent[0] : null;
        },

        _setComponentVisible: function (bVisible) {

        }
    });

});