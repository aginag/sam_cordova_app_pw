sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
    "use strict";

    return Controller.extend("SAMMobile.controller.home.Delta", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("delta").attachPatternMatched(this._onRouteMatched, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.hasAutomaticallySynchronized = false;

            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);
            sap.ui.getCore().getEventBus().subscribe("sync", "onAfterSync", this.onAfterSync, this);
        },

        _onRouteMatched: function (oEvent) {
            this.globalViewModel.setComponentBackNavEnabled(false);
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.hasAutomaticallySynchronized) {
                this.onSynchronizePressed();
            }
        },

        onExit: function (oEvent) {
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
            sap.ui.getCore().getEventBus().unsubscribe("sync", "onAfterSync", this.onAfterSync, this);
        },

        onLogout: function() {
            this.hasAutomaticallySynchronized = false;
        },

        onAfterSync: function() {
            if (this.globalViewModel.getCurrentRoute() != "delta") {
                return;
            }

            this.oRouter.navTo("home");
        },

        onSynchronizePressed: function() {
            sap.ui.getCore().getEventBus().publish("rootComponent", "onSyncRequest", {
                syncMode: "",
                publication: "",
                completeCb: function() {
                    this.hasAutomaticallySynchronized = true;
                }.bind(this)
            });
        }
    });

});