sap.ui.define(["sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "sap/m/MessageToast",
        'sap/m/Button',
        'sap/m/Dialog',
        'sap/m/Text',
        "sap/m/MessageBox",
        "SAMMobile/helpers/formatter",
        "SAMMobile/helpers/RequestHelper",
        'sap/m/MessagePopover',
        'sap/m/MessagePopoverItem',
        "SAMMobile/controller/home/HomeToolpageViewModel",
        "SAMMobile/models/dataObjects/ZPWAttachment",
        "SAMMobile/controller/common/MediaAttachments.controller"],

    function (Controller,
              JSONModel,
              MessageToast,
              Button,
              Dialog,
              Text,
              MessageBox,
              formatter,
              RequestHelper,
              MessagePopover,
              MessagePopoverItem,
              HomeToolpageViewModel,
              ZPWAttachment,
              MediaAttachmentsController) {
        "use strict";

        return Controller.extend("SAMMobile.controller.home.HomeToolpage", {
            requestHelper: RequestHelper,
            formatter: formatter,

            onInit: function () {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("home").attachPatternMatched(this._onRouteMatched, this);

                this.oRouter.attachRoutePatternMatched(function(oEvent) {
                    var routeName = oEvent.getParameter("name");
                    switch(routeName){
                        case "workOrderOperations":
                        case "workOrders":
                            this._onOrdersRouteMatched();
                            break;
                        case "notifications":
                            this._onNotificationsRouteMatched();
                            break;
                        case "technicalObjectsList":
                            this._onTechnicalObjectsRouteMatched();
                            break;
                        case "partnersList":
                            this._onPartnersRouteMatched();
                            break;
                        case "inventoryList":
                            this._onInventoryRouteMatched();
                            break;
                        case "timesheetList":
                            this._onTimesheetRouteMatched();
                            break;
                        case "equipmentCreation":
                            this._onEquipmentCreationRouteMatched();
                            break;
                        case "timesheetCalendar":
                            this._onTimesheetCalendarRouteMatched();
                            break;
                        default:
                            this._onOrdersRouteMatched();
                    }
                }, this);

                sap.ui.getCore().getEventBus().subscribe("home", "refreshCounters", this.onRefreshCounterRequest, this);
                sap.ui.getCore().getEventBus().subscribe("rootComponent", "onSyncRequest", this.onSyncRequestReceived, this);
                sap.ui.getCore().getEventBus().subscribe("sideNavButtonPress", "onSideNavButtonPress", this.onSideNavigation, this);

                this.globalViewModel = this.getOwnerComponent().globalViewModel;
                this.viewModel = new JSONModel({
                    isSyncing: false,
                    piTitle: "",
                    piPercentValue: 0,
                    piDisplayText: "",
                    piState: ""
                });

                this.oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();

                this.toolPage = this.byId("toolPage");
                this.sideNavigation = this.byId("sideNavigation");
                this.sideNavigationList = this.byId("upperSideNavigationList");
                this.lowerSideNavigationList = this.byId("lowerSideNavigationList");
                this.toolPage.setSideExpanded(false);
                this.component = this.getOwnerComponent();
                this.initialized = false;

                this.getView().setModel(this.viewModel, "viewModel");

                this._oDialog = sap.ui.xmlfragment("SAMMobile.view.home.FileTransferBusyDialog", this);
                this.getView().addDependent(this._oDialog);

                this.homeToolpageViewModel = new HomeToolpageViewModel(this.requestHelper, this.globalViewModel, this.component);

                this.getView().setModel(this.homeToolpageViewModel, "homeToolpageViewModel");
            },

            _onOrdersRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("workOrdersRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onNotificationsRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("notificationsRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onTechnicalObjectsRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("technicalObjectsRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onPartnersRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("partnersRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onInventoryRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("inventoryListRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onTimesheetRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("timesheetRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onTimesheetCalendarRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("timesheetCalendar");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onEquipmentCreationRouteMatched: function() {
                this.selectedNavListItem = this.getNavListItem("equipmentCreationRoute");
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },

            _onRouteMatched: function (oEvent) {
                const that = this;
                this.globalViewModel.model.setProperty("/editMode", false);
                this.getOwnerComponent().checkDeltaDone().then(function (bDone) {
                    if (!bDone) {
                        that.oRouter.navTo("delta");
                        return;
                    }

                    if (that.component.appUrl || window.SAM_APP_URL) {

                        var pieces = window.SAM_APP_URL.split("?");
                        var paramValue = {};

                        if (pieces[1]) {

                            var params = pieces[1].split("&");

                            params.forEach(function (param) {

                                var parameterArray = param.split("=");

                                switch (parameterArray[0]) {
                                    case "order":
                                        paramValue.order = parameterArray[1];
                                        break;
                                    case "operation":
                                        paramValue.operation = parameterArray[1];
                                        break;
                                    case "notification":
                                        paramValue.notification = parameterArray[1];
                                        break;
                                    case "equipment":
                                        paramValue.equipment = parameterArray[1];
                                        break;
                                    case "funcloc":
                                        paramValue.funcloc = parameterArray[1];
                                        break;
                                    default:
                                        paramValue.wrong = true;
                                        break;
                                }

                            });

                            if (paramValue.wrong) {
                                MessageBox.error(that.i18n.getText("ERROR_WRONG_PARAMETERS"));
                                this.appUrl = null;
                                this.appUrlParams = null;
                                return;
                            }
                        } else {
                            // if no parameters in url, start with workOrders List
                            paramValue.startWorkOrders = true;
                        }

                        if (that.component.appUrlParams) {
                            paramValue = that.component.appUrlParams;
                        }

                        if (paramValue.order && paramValue.operation) {
                            that.component.getRouter().navTo("workOrdersCustomRoute", {
                                query: {
                                    routeName: "workOrderDetailRoute",
                                    params: JSON.stringify({
                                        id: paramValue.order,
                                        opId: paramValue.operation,
                                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                    })
                                }
                            });
                        } else if (paramValue.notification) {
                            that.oRouter.navTo("notificationsCustomRoute", {
                                query: {
                                    routeName: "notificationDetailRoute",
                                    params: JSON.stringify({
                                        id: paramValue.notification,
                                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                    })
                                }
                            });
                        } else if (paramValue.equipment) {
                            that.oRouter.navTo("technicalObjectsCustomRoute", {
                                query: {
                                    routeName: "technicalObjectsDetailRoute",
                                    params: JSON.stringify({
                                        id: paramValue.equipment,
                                        objectType: "0",
                                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                    })
                                }
                            });

                        } else if (paramValue.funcloc) {
                            that.oRouter.navTo("technicalObjectsCustomRoute", {
                                query: {
                                    routeName: "technicalObjectsDetailRoute",
                                    params: JSON.stringify({
                                        id: paramValue.funcloc,
                                        objectType: "1",
                                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                    })
                                }
                            });
                        } else if (paramValue.startWorkOrders) {
                            that.onGoToWorkOrders();
                        }

                        that.component.appUrl = null;
                        that.component.appUrlParams = null;
                        window.SAM_APP_URL = null;
                    } else {
                        that.onGoToWorkOrders();
                    }

                    that.globalViewModel.updateUnreadSyncMessageCount();
                    that.onRefreshCounterRequest();
                    that.getOwnerComponent().onAfterSync();
                }).catch(function (err) {
                    MessageBox.error(that.component.i18n.getText("ERROR_DATABASE_DELTA_STATUS_DETERMINE"));
                });

            },

            onExit: function () {
                sap.ui.getCore().getEventBus().unsubscribe("home", "refreshCounters", this.onRefreshCounterRequest, this);
                sap.ui.getCore().getEventBus().unsubscribe("rootComponent", "onSyncRequest", this.onSyncRequestReceived, this);
            },

            onSyncRequestReceived: function(channel, eventId, data) {
                var syncMode = "";
                var publication = null;
                var completeCb = null;

                if (data) {
                    syncMode = data.syncMode ? data.syncMode : syncMode;
                    publication = data.publication ? data.publication : publication;
                    completeCb = data.completeCb ? data.completeCb : completeCb;
                }

                this.onSync(null, syncMode, publication, completeCb);
            },

            onRefreshCounterRequest: function () {
                this.homeToolpageViewModel.loadCounters(this.requestHelper);
            },

            goToScenario: function (oEvent) {
                var navFn = function () {
                    this.selectedRoute = "componentHolderRoute";
                    this.toggleSideNavigation();
                    this.globalViewModel.setBackNavEnabled(false);
                    this.selectedNavListItem = this.getNavListItem("componentHolderRoute");
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    this.oRouter.navTo("componentHolderRoute");

                    sap.ui.getCore().getEventBus().publish("home", "onNavToScenario");
                };

                if (this.globalViewModel.hasChanges()) {
                    this.setSideNavItem();
                    this.getOwnerComponent().openLeaveScreenConfirmationDialog(navFn.bind(this));
                    return;
                }

                navFn.apply(this, []);
            },

            onGoToWorkOrders: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();

                    var orderVariant = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.orderListVariant;

                    if (orderVariant == "ORDER") {
                        this.oRouter.navTo("workOrders");
                        this.globalViewModel.model.setProperty("/orderListVariant", "orders");
                    } else if (orderVariant == "OPERATION") {
                        this.oRouter.navTo("workOrderOperations");
                        this.globalViewModel.model.setProperty("/orderListVariant", "operations");
                    }

                    this.selectedNavListItem = this.getNavListItem("workOrdersRoute");
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }

                this.toggleSideNavigation();

            },

            onGoToNotifications: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();

                    this.oRouter.navTo("notifications");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },

            onGoToTechnicalObjects: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();

                    this.oRouter.navTo("technicalObjectsList");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },

            onGoToInventory: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();

                    this.oRouter.navTo("inventoryList");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },

            onGoToPartners: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();
                    this.oRouter.navTo("partnersList");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },
            onGoToEquipments: function () {
                var that = this;
                var onActionConfirmed = function () {
                    that.globalViewModel.model.setProperty("/editMode", false);
                    that.getOwnerComponent().resetComponentNavigation();
                    that.oRouter.navTo("equipmentCreation");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },
            onGoToTimesheet: function () {
                var onActionConfirmed = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.getOwnerComponent().resetComponentNavigation();
                    this.oRouter.navTo("timesheetCalendar");
                };

                if (this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
                this.toggleSideNavigation();
            },

            onOpenCustomAttachmentsDialog: function () {
                if(!this.mediaAttachmentsController){
                    this.component.rootComponent = sap.ui.getCore().byId("samComponent").getComponentInstance();
                    
                    this.mediaAttachmentsController = new MediaAttachmentsController();
                    this.mediaAttachmentsController.initialize(this.getOwnerComponent());
                }

                this.homeToolpageViewModel.loadCustomAttachments(this.requestHelper).then(function (data) {

                    var attachments = data.CustomAttachments.map(function(attachment) {
                        return new ZPWAttachment(attachment, this.requestHelper, this);
                    }.bind(this));
                    this.getCustomAttachmentDialog(attachments).open();
                }.bind(this));
            },
    
            getCustomAttachmentDialog: function (data) {
                if (!this._oCustomDialog) {
                    this._oCustomDialog = sap.ui.xmlfragment("attachmentDialog", "SAMMobile.view.common.customAttachments", this);
                    this.getView().addDependent(this._oCustomDialog);
                }
    
                if (data) {
                    this._oCustomDialog.setModel(new JSONModel({
                        attachments: data
                    }), "customAttachmentsModel");
                }
    
                return this._oCustomDialog;
            },
    
    
            onCloseCustomAttachmentsDialog: function () {
                this.getCustomAttachmentDialog().close();
                this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
            },
    
            _onOpenCustomAttachment: function (oEvent) {
                var bContext = oEvent.getParameter("listItem").getBindingContext("customAttachmentsModel");
                this.mediaAttachmentsController._onOpenAttachment(null, bContext);
            },

            goToMessages: function (oEvent) {
                var navFn = function (oEvent) {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.selectedRoute = "messagesRoute";
                    this.toggleSideNavigation();
                    this.globalViewModel.setBackNavEnabled(false);
                    this.selectedNavListItem = this.getNavListItem("messagesRoute");
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    this.oRouter.navTo(this.selectedRoute);
                };

                if (this.globalViewModel.hasChanges() || this.globalViewModel.model.getProperty("/editMode")) {
                    this.setSideNavItem();
                    this.getOwnerComponent().openLeaveScreenConfirmationDialog(navFn.bind(this));
                    return;
                }

                this.getOwnerComponent().resetComponentNavigation();
                navFn.apply(this, []);

            },

            // runTest: function(oEvent){
            // var success = function(){};
            // var error = function(){};
            // var queryStr = "SELECT * FROM Orders";
            // RunTestPlugin(queryStr, success, error);
            // },

            onNavBackPress: function (oEvent) {
                var navFn = function () {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.globalViewModel.setBackNavEnabled(false);
                    var oHistory = sap.ui.core.routing.History.getInstance();
                    var sPreviousHash = oHistory.getPreviousHash();

                    this.selectedNavListItem = this.getNavListItem(this.selectedRoute);
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    this.lowerSideNavigationList.setSelectedItem(null);

                    // The history contains a previous entry
                    if (sPreviousHash !== undefined) {
                        window.history.go(-1);
                    } else {
                        this.oRouter.navTo(this.selectedRoute);
                    }

                };

                var navFromGlobalViewNavFunction = function (fn) {
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.globalViewModel.setBackNavEnabled(false);
                    this.selectedNavListItem = this.getNavListItem(this.selectedRoute);
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    this.lowerSideNavigationList.setSelectedItem(null);

                    if (this.getOwnerComponent().componentNavigationActive) {
                        this.getOwnerComponent().navToPreviousComponent();
                    } else {
                        fn();
                    }

                };

                var navFunctionToExecute;

                if (this.globalViewModel.getComponentBackNavFunction() != null) {
                    navFunctionToExecute = navFromGlobalViewNavFunction.bind(this, this.globalViewModel.getComponentBackNavFunction());
                } else {
                    navFunctionToExecute = navFn.bind(this);
                }

                if (this.globalViewModel.hasChanges() || this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent().openLeaveScreenConfirmationDialog(navFunctionToExecute);
                    return;
                }

                navFunctionToExecute();
            },

            toggleSideNavigation: function () {
                if (this.toolPage.getSideExpanded()) {
                    this.toolPage.setSideExpanded(false);
                }
            },

            onSideNavButtonPress: function () {
                var viewId = this.getView().getId();
                var toolPage = sap.ui.getCore().byId(viewId + "--toolPage");
                var sideExpanded = toolPage.getSideExpanded();

                // this._setToggleButtonTooltip(sideExpanded);

                toolPage.setSideExpanded(!toolPage.getSideExpanded());
            },

            onSideNavigation: function () {
                var viewId = this.getView().getId();
                var toolPage = sap.ui.getCore().byId(viewId + "--toolPage");

                if(toolPage){
                    toolPage.setSideExpanded(false);
                }
            },

            goToSync: function (oEvent) {
                // hasChange check
                this.oRouter.navTo("syncRoute");
                this.toggleSideNavigation();
            },
            onSyncPressed: function (oEvent) {
                this.onSync(oEvent, "", this.getOwnerComponent().publication);
            },
            onSync: function (oEvent, syncMode, publication, completeCb) {
                completeCb = completeCb == null || completeCb == undefined || typeof completeCb != "function" ? function () {
                } : completeCb;
                var that = this;
                var model = publication;

                var success_method = function (result) {
                    that.getOwnerComponent().setBusyOff();
                    that.performSync(oEvent, syncMode, publication, completeCb);
                };
                var error_method = function (result) {
                    // TODO Error messages here
                    that.getOwnerComponent().setBusyOff();
                    MessageBox.warning(that.getOwnerComponent().getSyncErrorMessageForCode(result));
                    completeCb(false);
                };

                var onActionConfirmed = function () {
                    that.getOwnerComponent().setBusyOn();
                    if(this.globalViewModel.getCurrentRoute() == "notificationsCreationRoute" || this.globalViewModel.getCurrentRoute() == "notificationsEditRoute" ){
                        this.globalViewModel.model.setProperty("/editMode", false);
                        this.oRouter.navTo("notifications");
                    }

                    checkConnectivity(model, success_method, error_method);
                };

                if (this.globalViewModel.hasChanges() || this.globalViewModel.model.getProperty("/editMode")) {
                    this.getOwnerComponent()._getConfirmActionDialog(this.oBundle.getText("unsavedData"), onActionConfirmed.bind(this)).open();
                } else {
                    onActionConfirmed.apply(this);
                }
            },
            performSync: function (oEvent, syncMode, publication, completeCb) {
                const samUser = this.globalViewModel.getSAMUser();
                this.setSideNavItem();
                this.timeStart = Date.now();
                this.lastSyncModel = publication;
                console.log(new Date());

                var that = this;
                this.viewModel.setProperty("/piTitle", this.oBundle.getText("syncInProgress"));
                this.viewModel.setProperty("/piPercentValue", 0);
                this.viewModel.setProperty("/piDisplayText", "");
                this.viewModel.setProperty("/piState", "");

                // open busy Dialog
                if (!this._oDialog.isOpen()) {
                    this._oDialog.open();
                }

                var progressHandler = function (result) {

                    that.handleSyncProgress(result);
                };

                var success_method = function () {
                    // close busy Dialog
                    that._oDialog.close();

                    that.viewModel.setProperty("/isSyncing", false);
                    that.timeEnd = Date.now();

                    var elapsedMs = that.timeEnd - that.timeStart;
                    var timeElapsed = that.formatter.formatMsToTimeText(elapsedMs);

                    MessageToast.show(that.oBundle.getText("syncedInSeconds", [timeElapsed]), {
                        duration: 3000,
                        onClose: null,
                        animationDuration: 300
                    });

                    that.getOwnerComponent().setBusyOn();
                    $.when.apply(null, promises).done(function () {
                        that.getOwnerComponent().setBusyOff();
                        console.log("After-sync done!");
                        that.globalViewModel.updateUnreadSyncMessageCount();
                        completeCb(true);
                    });

                };

                var afterProgressDialogClosed = function () {
                    if (!this.syncOk) {
                        return;
                    }

                    var promises = [$.Deferred().resolve()];
                    sap.ui.getCore().getEventBus().publish("sync", "onAfterSync", {syncModel: this.lastSyncModel});
                    that.onRefreshCounterRequest();
                };

                var sync_error = function (result) {
                    // TODO Error messages here
                    that.viewModel.setProperty("/isSynycing", false);
                    // close busyDialog
                    that.syncOk = false;
                    that._oDialog.close();
                    MessageBox.alert(that.getOwnerComponent().getSyncErrorMessageForCode(result));
                    that.getOwnerComponent().setBusyOff();
                    completeCb(false);
                };

                //	    this._oDialog.open();
                var promises = [$.Deferred().resolve()];
                sap.ui.getCore().getEventBus().publish("sync", "onBeforeSync", function (promise) {
                    promises.push(promise);
                });

                if (!this._oDialog.hasListeners("afterClose")) {
                    this._oDialog.attachEvent("afterClose", null, afterProgressDialogClosed.bind(this));
                }

                this.syncOk = true;

                var fileCompressionQuality = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.fileCompressionQuality;

                this.getOwnerComponent().setBusyOn();
                $.when.apply(null, promises).done(function () {
                    that.getOwnerComponent().setBusyOff();
                    console.log("Pre-sync done!");
                    samUser.sync(progressHandler, publication, syncMode, fileCompressionQuality)
                        .then(success_method.bind(that))
                        .catch(sync_error.bind(that));
                });

            },
            handleSyncProgress: function (syncProgressInfo) {
                var that = this;
                var isSyncing = that.viewModel.oData.isSyncing;
                console.log(syncProgressInfo);
                if (typeof syncProgressInfo === "object") {
                    that.viewModel.setProperty('/syncProgress', syncProgressInfo);

                }
            },
            handleCancelSync: function (oEvent) {
                var that = this;
                var success_method = function (data) {
                    that.viewModel.setProperty("/isSynycing", false);
                    // close busyDialog
                    that._oDialog.close();
                    MessageToast.show(that.oBundle.getText("syncCancelled"));
                };
                var sync_error = function () {
                    MessageToast.show(that.oBundle.getText("cancelSyncHasErrors"));
                };


                this._getConfirmActionDialog(that.oBundle.getText("CANCEL_SYNC_TEXT"), function () {
                    cancelSync(success_method, sync_error);
                }).open();
            },

            onSideNavigationItemSelect: function () {
                this.globalViewModel.updateUnreadSyncMessageCount();
            },

            setSideNavItem: function () {
                var that = this;
                var item = that.lowerSideNavigationList.getSelectedItem();

                setTimeout(function () {
                    that.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    that.lowerSideNavigationList.setSelectedItem(item);
                }, 20);
            },

            getSyncMessages: function (callback) {
                var that = this;
            },

            getSyncCompleteDialog: function (title, type, state, content) {
                var that = this;
                return new Dialog({
                    title: title,
                    type: type,
                    state: state,
                    content: content,
                    beginButton: new Button({
                        text: that.oBundle.getText("ok"),
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        this.destroy();
                    }
                });
            },

            getNavListItem: function (key) {
                var items = this.sideNavigationList.getItems();
                var lowerItems = this.lowerSideNavigationList.getItems();
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (item.getKey() === key) {
                        return item;
                    }
                }

                for (var i = 0; i < lowerItems.length; i++) {
                    var item = lowerItems[i];
                    if (item.getKey() === key) {
                        return item;
                    }
                }
            },

            goToSettings: function (oEvent) {
                var navFn = function (oEvent) {
                    // this.selectedRoute = "settings";
                    this.globalViewModel.model.setProperty("/editMode", false);
                    this.toggleSideNavigation();
                    this.globalViewModel.setBackNavEnabled(false);
                    this.selectedNavListItem = this.getNavListItem("settings");
                    this.sideNavigationList.setSelectedItem(this.selectedNavListItem);
                    this.oRouter.navTo("settings");
                };

                if (this.globalViewModel.hasChanges() || this.globalViewModel.model.getProperty("/editMode")) {
                    this.setSideNavItem();
                    this.getOwnerComponent().openLeaveScreenConfirmationDialog(navFn.bind(this));
                    return;
                }

                this.getOwnerComponent().resetComponentNavigation();
                navFn.apply(this, []);
            },

            onLogoutPressed: function (oEvent) {
                var that = this;
                this.setSideNavItem();
                this._getConfirmActionDialog(this.oBundle.getText("logoutConfirmation"), function () {
                    sap.ui.getCore().getEventBus().publish("home", "onLogout");
                }).open();
            },

            _getConfirmActionDialog: function (message, executeCb) {
                var that = this;

                var _oConfirmationDialog = new Dialog({
                    title: this.oBundle.getText("Warning"),
                    type: 'Message',
                    state: 'Warning',
                    content: new Text({
                        text: message,
                        textAlign: 'Center',
                        width: '100%'
                    }),
                    beginButton: new Button({
                        text: this.oBundle.getText("yes"),
                        press: function () {
                            executeCb();
                            this.getParent().close();
                        }
                    }),
                    endButton: new Button({
                        text: this.oBundle.getText("no"),
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        this.destroy();
                    }
                });

                return _oConfirmationDialog;
            },

            formatSyncStateToText: function (state) {
                return this.oBundle.getText(state);
            }
        });
    });
