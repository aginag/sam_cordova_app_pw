sap.ui.define(["sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel", "sap/m/MessageToast", "sap/m/MessageBox", "SAMMobile/helpers/formatter",
        "sap/m/Button", "sap/m/Dialog", "sap/m/Text", "SAMMobile/helpers/RequestHelper"],

    function (Controller, JSONModel, MessageToast, MessageBox, formatter, Button, Dialog, Text, RequestHelper) {
        "use strict";

        return Controller.extend("SAMMobile.controller.home.Settings", {
            requestHelper: RequestHelper,
            formatter: formatter,

            onInit: function () {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                // this.oRouter.getRoute("homeMainContent").attachPatternMatched(this._onRouteMatched,
                // this);
                this.oRouter.getRoute("settings").attachPatternMatched(this._onRouteMatched, this);

                this.globalViewModel = this.getOwnerComponent().globalViewModel;
                this.pwdResetModel = new JSONModel({});
                this.viewModel = new JSONModel({
                    isSyncing: false,
                    piTitle: "",
                    piPercentValue: 0,
                    piDisplayText: "",
                    piState: ""
                });
                this.getView().setModel(this.viewModel, "viewModel");

                this.oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();

                this.oldPasswordInput;
                this.newPasswordInput;
                this.confirmNewPasswordInput;

                this._busyDialog = sap.ui.xmlfragment("SAMMobile.view.home.BusyDialog");
                this._busyDialog.setModel(this.getOwnerComponent().getModel("i18n"), 'i18n');
                this.getView().addDependent(this._busyDialog);
            },

            _onRouteMatched: function (oEvent) {
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));
                this.globalViewModel.setBackNavEnabled(false);

                this.globalViewModel.setHeaderText(this.oBundle.getText("settings"));
                this.resetPwdResetModel();
                this.initPwdChangeDialog();
                console.log("settingsLoaded");
            },

            onUploadDbPress: function (oEvent) {
                var that = this;
                that.getOwnerComponent().setBusyOn();
                var success_method = function () {
                    that.getOwnerComponent().setBusyOff();
                    that.uploadDb(oEvent);
                };
                var error_method = function () {
                    that.getOwnerComponent().setBusyOff();
                    MessageToast.show(that.oBundle.getText("noConnectionToMl"));
                };
                checkConnectivity(this.getOwnerComponent().publication, success_method, error_method);
            },
            uploadDb: function () {
                // Upload the sam.udb file to the documents upload folder
                var that = this;
                var dbFileName = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.dbFileName + Date.now() + ".zip";
//	    var connectionValid = checkConnection();
//	    if (connectionValid == false) {
//		MessageToast.show(this.oBundle.getText("noInternet"));
//		return;
//	    }

                this.viewModel.setProperty("/piTitle", that.oBundle.getText("UPLOADING_DB_DIALOG_TITLE"));
                this.viewModel.setProperty("/piPercentValue", 0);
                this.viewModel.setProperty("/piDisplayText", "");
                this.viewModel.setProperty("/piState", "");

                this.getOwnerComponent().setBusyOn();
                var progressHandler = function (result) {
                    that.handleUploadProgress(result);
                };
                var success_method = function (result) {

                    that.handleUploadProgress(result);

                    if (!that.viewModel.oData.isSyncing) {
                        that._busyDialog.close();
                        that.getOwnerComponent().setBusyOff();
                        MessageToast.show(that.oBundle.getText("dbUploaded"), {
                            duration: 3000,
                            onClose: null,
                            animationDuration: 300
                        });
                    }

                };
                var sync_error = function (result) {
                    that.viewModel.setProperty("/isSynycing", false);
                    // close busyDialog
                    that._busyDialog.close();
                    MessageBox.alert(that.oBundle.getText("syncHasErrors"));
                    that.getOwnerComponent().setBusyOff();
                };

                var cancelSuccess = function() {
                    that.viewModel.setProperty("/isSynycing", false);
                    //close busyDialog
                    that._busyDialog.close();
                    that.getOwnerComponent().setBusyOff();
                };

                var cancelUpload = function() {
                    cancelFileTransfer(dbFileName, cancelSuccess, sync_error);
                };

                this._busyDialog.getEndButton().mEventRegistry.press = [];
                this._busyDialog.getEndButton().attachPress(cancelUpload);

                uploadDBFile(dbFileName, success_method, sync_error, progressHandler);
            },

            onUploadLogfilePress: function (oEvent) {
                var that = this;
                that.getOwnerComponent().setBusyOn();
                var success_method = function () {
                    that.getOwnerComponent().setBusyOff();
                    that.uploadLogfile(oEvent);
                };
                var error_method = function () {
                    that.getOwnerComponent().setBusyOff();
                    MessageToast.show(that.oBundle.getText("noConnectionToMl"));
                };
                checkConnectivity(this.getOwnerComponent().publication, success_method, error_method);
            },
            uploadLogfile: function () {
                // Upload the log files to the documents upload folder
                var that = this;
                var logFileName = "Log" + Date.now() + ".zip";
//	    var connectionValid = checkConnection();
//	    if (connectionValid == false) {
//		MessageToast.show(this.oBundle.getText("noInternet"));
//		return;
//	    }

                this.viewModel.setProperty("/piTitle", "Uploading Log File");
                this.viewModel.setProperty("/piPercentValue", 0);
                this.viewModel.setProperty("/piDisplayText", "");
                this.viewModel.setProperty("/piState", "");

                this.getOwnerComponent().setBusyOn();
                var progressHandler = function (result) {
                    that.handleUploadProgress(result);
                };
                var success_method = function (result) {

                    that.handleUploadProgress(result);

                    if (!that.viewModel.oData.isSyncing) {
                        that._busyDialog.close();
                        that.getOwnerComponent().setBusyOff();
                        MessageToast.show(that.oBundle.getText("logFilesUploaded"), {
                            duration: 3000,
                            onClose: null,
                            animationDuration: 300
                        });
                    }

                };
                var sync_error = function (result) {
                    that.viewModel.setProperty("/isSynycing", false);
                    // close busyDialog
                    that._busyDialog.close();
                    MessageBox.alert(that.oBundle.getText("uploadHasErrors"));
                    that.getOwnerComponent().setBusyOff();
                };

                uploadLogFiles(logFileName, success_method, sync_error, progressHandler);
            },

            handleUploadProgress: function (result) {
                var that = this;
                var isSyncing = that.viewModel.oData.isSyncing;
                console.log(result);

                if (typeof result === "object") {
                    // started
                    if (!isSyncing) {
                        // open busy Dialog
                        that._busyDialog.open();
                        that.viewModel.setProperty("/isSyncing", true);
                    }

                    // FileTransfer
                    var percent = Math.round((result.bytesTransferred / result.fileSize) * 100);

                    this.viewModel.setProperty("/piPercentValue", percent);
                    this.viewModel.setProperty("/piDisplayText", percent + "%");
                    this.viewModel.setProperty("/piState", result.status);

                } else {
                    // finished
                    if (isSyncing) {
                        that.viewModel.setProperty("/isSyncing", false);
                    }
                }
            },
            onResetLogFiles: function () {
                this.getResetLogFilesConfDialog().open();
            },
            getResetLogFilesConfDialog: function () {
                var that = this;
                if (!this._oResetLogFilesConfDialog) {
                    var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                    that._oResetLogFilesConfDialog = new Dialog({
                        title: oBundle.getText("Warning"),
                        type: 'Message',
                        state: 'Warning',
                        content: new Text({
                            text: oBundle.getText("resetLogsConfirmation"),
                            textAlign: 'Center',
                            width: '100%'
                        }),
                        beginButton: new Button({
                            text: oBundle.getText("yes"),
                            press: function () {
                                that.resetLogs();
                            }
                        }),
                        endButton: new Button({
                            text: oBundle.getText("no"),
                            press: function () {
                                this.getParent().close();
                            }
                        }),
                        afterClose: function () {
                            //			this.destroy();
                        }
                    });
                }
                return this._oResetLogFilesConfDialog;
            },
            resetLogs: function () {
                //Remove the sam.udb file from the documents folder and nav to login
                var that = this;
                this.getOwnerComponent().setBusyOn();
                var success_method = function (result) {
                    MessageToast.show(that.oBundle.getText("logsReset"), {
                        duration: 3000,
                        onClose: null,
                        animationDuration: 300
                    });
                    that.getOwnerComponent().setBusyOff();
                    that._oResetLogFilesConfDialog.close();
                };
                var sync_error = function (result) {
                    MessageBox.alert(that.oBundle.getText("logResetFailed"));
                    that.getOwnerComponent().setBusyOff();
                    that._oResetLogFilesConfDialog.close();
                };

                resetLogFiles(success_method, sync_error);
            },
            onResetPress: function () {
                this.getResetConfirmationDialog().open();
            },
            getResetConfirmationDialog: function () {
                var that = this;
                if (!this._oResetConfirmationDialog) {
                    var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                    that._oResetConfirmationDialog = new Dialog({
                        title: oBundle.getText("Warning"),
                        type: 'Message',
                        state: 'Warning',
                        content: new Text({
                            text: oBundle.getText("resetDBConfirmation"),
                            textAlign: 'Center',
                            width: '100%'
                        }),
                        beginButton: new Button({
                            text: oBundle.getText("yes"),
                            press: function () {
                                that.performReset();
                            }
                        }),
                        endButton: new Button({
                            text: oBundle.getText("no"),
                            press: function () {
                                this.getParent().close();
                            }
                        }),
                        afterClose: function () {
                            // this.destroy();
                        }
                    });
                }
                return this._oResetConfirmationDialog;
            },
            performReset: function () {
                // Remove the sam.udb file from the documents folder and nav to
                // login
                var that = this;
                this.getOwnerComponent().setBusyOn();
                var user = that.getOwnerComponent().getModel("globalViewModel").getProperty('/userName');

                var success_method = function (result) {
                    MessageToast.show(that.oBundle.getText("dbReset"), {
                        duration: 3000,
                        onClose: null,
                        animationDuration: 300
                    });
                    that.getOwnerComponent().setBusyOff();
                    sap.ui.getCore().getEventBus().publish("home", "onLogout");
                    if (that.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.loadScenarioFromUDB) {
                        var filePath = user + '/' + that.getOwnerComponent().getModel("globalViewModel").getProperty('/scenarioComponentName') + '_' +
                            that.getOwnerComponent().getModel("globalViewModel").getProperty('/scenarioComponentVersion') + '/Component-preload.js';
                        deleteFile(filePath, function (result) {
                            // navigate to login screen
                            sap.ui.getCore().getEventBus().publish("home", "onLogout");
                        }, sync_error);
                    } else {
                        sap.ui.getCore().getEventBus().publish("home", "onLogout");
                    }
                };
                var sync_error = function (result) {
                    MessageBox.alert(that.oBundle.getText("syncHasErrors"));
                    that.getOwnerComponent().setBusyOff();
                };

                this.getOwnerComponent().stopLiveTracking().finally(function() {
                    resetDB(user, "", success_method, sync_error);
                });
            },

            onSavePassword: function () {
                var newPwd = this.pwdResetModel.getProperty("/newPassword");
                var oldPwd = this.pwdResetModel.getProperty("/oldPassword");

                var that = this;
                this.getOwnerComponent().setBusyOn();

                var success_method = function (result) {
                    that.getOwnerComponent().setBusyOff();
                    MessageToast.show(that.oBundle.getText("newPwdSaved"), {
                        duration: 3000,
                        onClose: null,
                        animationDuration: 300
                    });

                    that.onCloseChangePwdDialog();

                    localStorage.removeItem("user");
                    localStorage.removeItem("password");
                };
                var sync_error = function (result) {
                    MessageBox.alert(that.oBundle.getText("newPwdNotSaved"));
                    that.getOwnerComponent().setBusyOff();
                };
                setNewPassword(newPwd, oldPwd, success_method, sync_error)
            },

            onChangePwdPress: function () {
                this._settingsDialog.open();
            },

            onCloseChangePwdDialog: function () {
                // also reset pwdResetModel
                this._settingsDialog.close();
                this.resetPwdResetModel();
                this.oldPasswordInput.removeStyleClass("success");
                this.newPasswordInput.removeStyleClass("success");
                this.confirmNewPasswordInput.removeStyleClass("success");
            },

            resetPwdResetModel: function () {
                this.pwdResetModel.setData({
                    minimumPwdLength: 1,
                    oldPassword: "",
                    newPassword: "",
                    passwordConfirm: ""
                });
            },

            initPwdChangeDialog: function () {
                if (!this._settingsDialog) {
                    this._settingsDialog = new sap.ui.xmlfragment("changePwdDialog", "SAMMobile.view.home.ChangePasswordDialog", this);
                    this._settingsDialog.setStretch(sap.ui.Device.system.phone);

                    this._settingsDialog.setModel(this.pwdResetModel, "pwdResetModel");
                    this._settingsDialog.setModel(this.getOwnerComponent().getModel("i18n"), "i18n");

                    this.getView().addDependent(this._settingsDialog);

                    this.oldPasswordInput = sap.ui.core.Fragment.byId("changePwdDialog", "oldPasswordInput");
                    this.newPasswordInput = sap.ui.core.Fragment.byId("changePwdDialog", "newPasswordInput");
                    this.confirmNewPasswordInput = sap.ui.core.Fragment.byId("changePwdDialog", "confirmNewPasswordInput");
                }
            },

            validatePassword: function (oEvent) {
                var minimumPwdLength = this.pwdResetModel.getProperty("/minimumPwdLength");

                var oldPwd = this.pwdResetModel.getProperty("/oldPassword");
                var newPwd = this.pwdResetModel.getProperty("/newPassword");
                var confPwd = this.pwdResetModel.getProperty("/passwordConfirm");

                if (oldPwd.length >= 1) {
                    this.oldPasswordInput.setValueState(sap.ui.core.ValueState.Success);
                } else {
                    this.oldPasswordInput.setValueState(sap.ui.core.ValueState.None);
                }

                if (newPwd.length >= minimumPwdLength) {
                    this.newPasswordInput.setValueState(sap.ui.core.ValueState.Success);
                } else {
                    this.newPasswordInput.setValueState(sap.ui.core.ValueState.None);
                }

                if (newPwd == oldPwd) {
                    this.newPasswordInput.setValueState(sap.ui.core.ValueState.Error);
                    return;
                }

                if (newPwd.length == 0) {
                    this.confirmNewPasswordInput.setValueState(sap.ui.core.ValueState.None);
                    return;
                }

                if (newPwd === confPwd && newPwd.length >= minimumPwdLength) {
                    this.confirmNewPasswordInput.setValueState(sap.ui.core.ValueState.Success);
                } else {
                    this.confirmNewPasswordInput.setValueState(sap.ui.core.ValueState.Error);
                }
            },

            onThemeChanged: function(oEvent) {
                var theme = oEvent.getParameter("selectedItem").getKey();
                this.getOwnerComponent().changeTheme(theme);
            }

        });
    });
