sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function HomeToolpageViewModel(requestHelper, globalViewModel, component) {

            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        HomeToolpageViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        HomeToolpageViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        HomeToolpageViewModel.prototype.loadCounters = function (requestHelper, successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/operationCounter", data[0].OperationCounter[0].COUNT.toString());
                that.setProperty("/notificationCounter", data[1].NotificationCounter[0].COUNT.toString());
                that.setProperty("/orderCounter", data[2].OrderCounter[0].COUNT.toString());
                that.setProperty("/messageCounter", data[3].MessageCounter[0].COUNT.toString());
            };

            var fetchCounterQueue = [
                this._fetchOperationsCount(requestHelper),
                this._fetchNotificationsCount(requestHelper),
                this._fetchOrdersCount(requestHelper),
                this._fetchMessagesCount(requestHelper),
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);
        };

        HomeToolpageViewModel.prototype._fetchOperationsCount = function(requestHelper){
            return requestHelper.fetch({
                id: "OperationCounter",
                statement: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM ORDEROPERATIONS as a " 
                    + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID " 
                    + "WHERE ( a.PERS_NO = '@persNo' OR (a.TEAM_GUID IN (@teamGuids) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '@persNo' ) " 
                    + "AND ( a.START_CONS <= '@startCons' or t.TEAM_BEG_DATUM <= '@startCons' ) "
                    + "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '@yesterday' ) "
                    + "AND NOT EXISTS ( SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = a.OBJECT_NO AND ( ORDERUSERSTATUS.STATUS = 'E0002' OR (ORDERUSERSTATUS.STATUS = 'E0003' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) ) AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='')) ",

            }, {
                persNo: this.getUserInformation().personnelNumber,
                teamGuids: this.getUserInformation().teams.map(function(oTeam) { return "'" + oTeam.teamGuid + "'"; }),
                yesterday: moment().add(-1,'days').format("YYYY-MM-DD"),
                startCons: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS")
            });
        };

        HomeToolpageViewModel.prototype._fetchOrdersCount = function(requestHelper){
            return requestHelper.fetch({
                id: "OrderCounter",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM ORDERS"
            }, {
                persNo: this.getUserInformation().personnelNumber
            });
        };

        HomeToolpageViewModel.prototype._fetchNotificationsCount = function(requestHelper){
            return requestHelper.fetch({
                id: "NotificationCounter",
                statement: "SELECT COUNT(mobile_id) AS COUNT FROM NotificationHeader WHERE ACTION_FLAG = 'N'"
            });
        };

        HomeToolpageViewModel.prototype._fetchMessagesCount = function(requestHelper){
            return requestHelper.fetch({
                id: "MessageCounter",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM SAMSyncMessages WHERE ACTION_FLAG != 'D'"
            });
        };

        HomeToolpageViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        HomeToolpageViewModel.prototype.loadCustomAttachments = function (requestHelper) {

            return requestHelper.fetch({
                id: "CustomAttachments",
                statement: "SELECT * FROM  ZPW_ATTACHMENTS"
            });
            
        };

        HomeToolpageViewModel.prototype.constructor = HomeToolpageViewModel;

        return HomeToolpageViewModel;
    });
