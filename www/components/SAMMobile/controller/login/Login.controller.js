sap.ui.define(["sap/ui/core/mvc/Controller",
        "SAMMobile/helpers/RequestHelper",
        "sap/m/MessageBox",
        'sap/m/Dialog',
        'sap/m/Button',
        'sap/m/Text',
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter",
        "SAMMobile/helpers/Validator",
        "sap/ui/support/supportRules/Storage",
        "SAMMobile/models/sam/SAMUser",
        "SAMContainer/models/ViewModel"],

    function (Controller, RequestHelper, MessageBox, Dialog, Button, Text, JSONModel, Formatter, Validator, Storage, SAMUser, ViewModel) {
        "use strict";

        return Controller.extend("SAMMobile.controller.login.Login", {
            requestHelper: RequestHelper,
            formatter: Formatter,

            onInit: function () {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("login").attachPatternMatched(this._onRouteMatched, this);
                var success_method = function (result) {
                    // alert(' server setup success');
                };
                var setup_error = function (result) {
                    // alert('server setup error');
                };
                var mlSetup = this.getOwnerComponent().getManifestObject().getEntry("sap.app").MLSetup;
                var samConfig = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig;
                this.getOwnerComponent().publication = mlSetup.publication;
                defineMLInfo(mlSetup.protocol, mlSetup.hostname, mlSetup.port, mlSetup.subscription, mlSetup.version, mlSetup.publication, mlSetup.urlSuffix, samConfig.dbFileName, success_method, setup_error);
                this.loginModel = new JSONModel({
                    user: '',
                    password: '',
                    busy: false,
                    fileLoading: false,
                    fileLoadText: '',
                    isDownloading: false,
                    piPercentValue: 0,
                    piDisplayText: "0%",
                    hiddenToolbarCounter: 0
                });

                this.getOwnerComponent().setModel(this.loginModel, "loginModel");

            },

            _onRouteMatched: function (oEvent) {
                this.oBundle = this.getView().getModel("i18n").getResourceBundle();

                var user = localStorage.getItem("user");
                var password = localStorage.getItem("password");

                if (user && password) {
                    this.loginModel.setProperty("/rememberLogin", true);
                }

                this.loginModel.setProperty("/user", user);
                this.loginModel.setProperty("/password", password);

            },
            onAfterRendering: function () {
                const that = this;
                var device = sap.ui.Device.os;
                var os = device.name;

                if (os === device.OS.IOS) {
                    $(".loginDiv").on("touchstart", "textarea, input", function (e) {
                        e.preventDefault();
                        $(e.target).focus();
                    });
                } else if (os === device.OS.ANDROID) {
                    document.addEventListener("backbutton", function (e) {
                        const currentRoute = location.href.split("#")[1];
                        if (currentRoute == "") {
                            e.preventDefault();
                            return;
                        }

                        window.history.go(-1);
                        return false;

                    }, false);
                }

                this.getOwnerComponent().getModel("globalViewModel").setProperty('/applicationVersion', this.getOwnerComponent().getManifestObject().getEntry("sap.app").applicationVersion.version);
                this.getOwnerComponent().getModel("globalViewModel").setProperty('/applicationBuild', this.getOwnerComponent().getManifestObject().getEntry("sap.app").applicationVersion.build);
                this.getOwnerComponent().getModel("globalViewModel").setProperty('/applicationEnvironment', this.getOwnerComponent().getManifestObject().getEntry("sap.app").applicationVersion.env);
            },

            onLogin: function () {
                var that = this;

                if (this.getOwnerComponent().getModel("loginModel").oData.user === "" || this.loginModel.oData.password === "") {
                    MessageBox.alert(this.getOwnerComponent().getModel("i18n").getResourceBundle().getText("noCredentialsMessage"), {
                        icon: MessageBox.Icon.ERROR,
                        title: "",
                    });
                    return;
                }

                this.timeStart = Date.now();
                if (!this._oDialog) {
                    this._oDialog = sap.ui.xmlfragment("SAMMobile.view.login.FileLoading");
                    this.getView().addDependent(this._oDialog);
                    sap.ui.getCore().byId('loadingButton').attachPress(this.navToHome, this);
                }
                this.getOwnerComponent().getModel("loginModel").oData.busy = true;
                this.getOwnerComponent().getModel("loginModel").refresh();
                this.getOwnerComponent().getModel("loginModel").oData.user = this.loginModel.oData.user.toLowerCase();
                var mlSetup = this.getOwnerComponent().getManifestObject().getEntry("sap.app").MLSetup;
                that.getOwnerComponent().getModel("globalViewModel").setProperty('/hostName', mlSetup.hostname);
                that.getOwnerComponent().getModel("globalViewModel").setProperty('/port', mlSetup.port);
                var success = function (newUser) {
                    if (!newUser) {
                        that.validateAndLogin(false);
                    } else {
                        var connectionValid = checkConnection();
                        if (connectionValid) {
                            that.getOwnerComponent().getModel("loginModel").oData.fileLoading = true;
                            that.getOwnerComponent().getModel("loginModel").oData.fileLoadText = that.getOwnerComponent().getModel("i18n").getResourceBundle().getText("loginLoadingFile");
                            that.getOwnerComponent().getModel("loginModel").refresh();
                            that._oDialog.open();
                            that.validateAndLogin(true);
                        } else {
                            MessageBox.alert(that.oBundle.getText("firstLoginNoInternet"));
                            that.getOwnerComponent().getModel("loginModel").setProperty("/busy", false);
                        }
                    }
                };
                var error = function (result) {
                    MessageBox.alert(result);
                    that.getOwnerComponent().getModel("loginModel").setProperty("/busy", false);
                };
                this.checkNewUser(this.getOwnerComponent().getModel("loginModel").oData.user, success, error);

            },

            checkNewUser: function (user, success) {
                var device = sap.ui.Device.os;
                var os = device.name;

                var samConfig = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig;
                var udbPath = '/' + user + '/' + samConfig.dbFileName + '.udb';

                if (os === device.OS.WINDOWS) {
                    udbPath = '/Users' + udbPath;
                }

                fileExists(udbPath, function (exists) {
                    if (exists) {
                        success(false);
                    } else {
                        success(true);
                    }
                });
            },
            onLanguageChange: function (oEvent) {
                var selectedLanguage = oEvent.getSource().getSelectedItem().getBindingContext("globalViewModel").getObject();
                this.getOwnerComponent().setUiLanguage(selectedLanguage.ui5Lang, false);
                this.oBundle = this.getView().getModel("i18n").getResourceBundle();

            },

            load_error: function (result) {
                // show error
                MessageBox.alert(result);
            },

            validateAndLogin: function (newUser) {
                var that = this;
                var pwd = this.loginModel.oData.password;

                var rememberLogin = this.loginModel.getProperty('/rememberLogin');
                if (rememberLogin === true) {
                    localStorage.setItem("user", that.getOwnerComponent().getModel("loginModel").oData.user);
                    localStorage.setItem("password", that.loginModel.oData.password);
                } else {
                    localStorage.setItem("user", '');
                    localStorage.setItem("password", '');
                }

                var isDownloading;
                var progressHandler = null;
                var fileName = "";
                var encryptionKeyFileName = "";
                if (newUser) {
                    if (!isDownloading) {
                        that.loginModel.setProperty("/isDownloading", true);
                    }
                    progressHandler = function (result) {
                        that.handleDownloadProgress(result);
                    }
                    fileName = that.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.dbFileName + ".udb";
                    encryptionKeyFileName = that.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig.encryptionKeyFileName;
                }
                var login_success = function (result) {
                    //Check and download component scenario
                    that.initializeUser(that.getOwnerComponent().getModel("loginModel").oData.user).then(function() {

                        var samUser = that.getOwnerComponent().globalViewModel.getSAMUser();
                        if (samUser.getScenario().getVersion().hasComponent()) {
                            // TODO download component and register and initialize
                        }

                        that.loginModel.setProperty("/isDownloading", false);
                        if (newUser) {
                            that.timeEnd = Date.now();
                            var timeMs = that.timeEnd - that.timeStart;
                            var timeSec = parseInt(timeMs / 1000);
                            var timeMin = parseInt(timeSec / 60);
                            timeSec = parseInt(timeSec % 60);
                            that.getOwnerComponent().getModel("loginModel").oData.fileLoadText = that.getOwnerComponent().getModel("i18n").getResourceBundle().getText("loginLoadedFile", [timeMin.toString(), timeSec.toString()]);
                            that.getOwnerComponent().getModel("loginModel").oData.fileLoading = false;
                            that.getOwnerComponent().getModel("loginModel").oData.busy = false;
                            that.getOwnerComponent().getModel("loginModel").refresh();
                        } else {
                            that.getOwnerComponent().getModel("loginModel").oData.busy = false;
                            that.getOwnerComponent().getModel("loginModel").refresh();
                            that.navToHome();
                        }
                    }).catch(function(err) {
                        MessageBox.error(err.message);
                        that.getOwnerComponent().getModel("loginModel").oData.busy = false;
                        that.getOwnerComponent().getModel("loginModel").refresh();
                        if (newUser) {
                            that._oDialog.close();
                        }
                    });
                };

                var login_error = function (result) {
                    that.getOwnerComponent().getModel("loginModel").oData.busy = false;
                    that.getOwnerComponent().getModel("loginModel").refresh();
                    if (newUser) {
                        that._oDialog.close();
                    }
                    MessageBox.alert(that.getOwnerComponent().getSyncErrorMessageForCode(result));
                };

                var after_login = function (result) {
                    that.requestHelper.setField("updateDBDownloaded", "SAMUsers", {
                        userName: that.getOwnerComponent().getModel("loginModel").oData.user
                    }, login_success, login_error);
                };
                login(this.getOwnerComponent().getModel("loginModel").oData.user, pwd, after_login, login_error, progressHandler, fileName, encryptionKeyFileName);

            },

            initializeUser: function(userName) {
                var that = this;

                return new Promise(function(resolve, reject) {
                    var success = function (data) {
                        if (data.length == 0) {
                            reject(new Error(that.oBundle.getText("ERROR_USER_NOT_FOUND_ON_UDB")));
                            return;
                        }

                        var samUser = new SAMUser(data[0], that.requestHelper, new ViewModel(that.getOwnerComponent(), that.getOwnerComponent().globalViewModel, that.requestHelper, ""));

                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/personnelNumber', samUser.EMPLOYEE_NUMBER);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/workCenterId', samUser.WORKCENTER_ID);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/plant', samUser.PLANT);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/storage_location', samUser.STORAGE_LOCATION);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/firstName', samUser.FIRST_NAME);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/lastName', samUser.LAST_NAME);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/userName', samUser.USER_NAME);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/remoteId', samUser.REMOTE_ID);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/userName', samUser.USER_NAME);
                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/samUser', samUser);

                        var aTeams = [];
                        data.forEach(function (entry) {

                            aTeams = aTeams.concat({
                                teamGuid: entry.TEAM_GUID,
                                isTeamLead: entry.RES_FLAG_RESP
                            });
                        });

                        that.getOwnerComponent().getModel("globalViewModel").setProperty('/teams', aTeams);

                        samUser.init().then(function() {
                            const currentVersion = this.getOwnerComponent().getModel("globalViewModel").getProperty("/applicationVersion");

                            if (!samUser.getScenario().getVersion().isMinimumVersionStatisfied(currentVersion)) {
                                throw new Error(this.oBundle.getText("ERROR_MINIMUM_VERSION_REQUIRED", [samUser.getScenario().getVersion().getRequiredVersionNumber()]));
                            }

                            resolve();
                        }.bind(that)).catch(reject);
                    };

                    that.requestHelper.searchData("SAMUsers", {
                        "userName": userName
                    }, success, reject);
                });
            },

            checkAndLoadScenario: function (user, callbackFn) {
                var that = this;
                var samConfig = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig;

                var getDocumentsDirectoryForPlatform = function () {
                    var device = sap.ui.Device.os;
                    var os = device.name;

                    switch (os) {
                        case device.OS.ANDROID:
                            return cordova.file.dataDirectory + "files";
                        case device.OS.IOS:
                            return cordova.file.documentsDirectory;
                        default:
                            return "";
                    }
                };

                var load_error = function () {
                    alert(that.oBundle.getText("ERROR_COMPONENT_LOAD"));
                };
                var progressHandler = function (result) {
                    that.handleDownloadProgress(result);
                };

                var initComponentPreloadFile = function (data) {
                    var folderName = user + '/' + data[0].UI5_COMPONENT_NAME + '_' + data[0].VERSION;
                    var filePath = folderName + '/Component-preload.js';


                    fileExists(filePath, function (result) {
                        if (result) {
                            //register path
                            jQuery.sap.registerModulePath(data[0].UI5_COMPONENT_NAME, getDocumentsDirectoryForPlatform() + "/" + folderName);
                            callbackFn();
                        } else {
                            // Download encryption key
                            downloadFile(data[0].UI5_FILE_NAME, function () {
                                //create folder
                                createFolderInPersistentStorage(folderName, function () {
                                    //rename and move file
                                    moveFile(data[0].UI5_FILE_NAME, '', 'Component-preload.js', folderName, function () {
                                        //register path
                                        jQuery.sap.registerModulePath(data[0].UI5_COMPONENT_NAME, getDocumentsDirectoryForPlatform() + "/" + folderName);
                                        callbackFn();
                                    }, load_error)
                                }, load_error)
                            }, load_error, progressHandler);
                        }
                    });
                };

                var afterLoadScenarioInformation = function (data) {
                    // Check if component is available
                    that.getOwnerComponent().getModel("globalViewModel").setProperty('/scenarioComponentName', data[0].UI5_COMPONENT_NAME);
                    that.getOwnerComponent().getModel("globalViewModel").setProperty('/scenarioComponentVersion', data[0].VERSION);
                    initComponentPreloadFile(data);
                    //that.setupComponentMLInfo(data[0].NAME, initComponentPreloadFile.bind(this, data), load_error);
                };

                if (samConfig.loadScenarioFromUDB) {
                    this.requestHelper.getData("SAMScenarioVersions", {
                        "userName": user
                    }, afterLoadScenarioInformation, load_error);
                } else {
                    jQuery.sap.registerModulePath("WorkOrders", "./components/WorkOrders");
                    jQuery.sap.registerModulePath("Notifications", "./components/Notifications");
                    jQuery.sap.registerModulePath("TechnicalObjects", "./components/TechnicalObjects");
                    jQuery.sap.registerModulePath("Inventory", "./components/Inventory");
                    jQuery.sap.registerModulePath("Partners", "./components/Partners");
                    jQuery.sap.registerModulePath("Timesheet", "./components/Timesheet");

                    callbackFn();
                }
            },
            setupComponentMLInfo: function (componentSyncModel, successCb, errorCb) {
                //TODO - read scenarios table to find out sync model
                var setup_error = function (result) {
                    MessageBox.alert('server setup error');
                    errorCb();
                };
                var mlSetup = this.getOwnerComponent().getManifestObject().getEntry("sap.app").MLSetup;
                var samConfig = this.getOwnerComponent().getManifestObject().getEntry("sap.app").SAMConfig;
                defineMLInfo(mlSetup.hostname, mlSetup.port, componentSyncModel, componentSyncModel, componentSyncModel, samConfig.dbFileName, successCb, setup_error);

            },
            handleDownloadProgress: function (progress) {
                var percent = Math.round((progress.bytesTransferred / progress.fileSize) * 100);
                var displayText = (isNaN(percent) ? '0' : percent) + "%";

                this.loginModel.setProperty("/piPercentValue", percent);
                this.loginModel.setProperty("/piDisplayText", displayText);
            },

            navToHome: function () {
                var that = this;

                if (this._oDialog) {
                    this._oDialog.close();
                }

                sap.ui.getCore().getEventBus().publish("loginRoute", "onLoginSuccess");
                that.oRouter.navTo("home");
            },
            formatFileLoadText: function (fileLoading) {
                if (fileLoading) {
                    // this.
                }
                var fileLoadTime = this.getOwnerComponent().getModel('loginModel').getProperty('/fileLoadTime');
            },

            onBottomToolbarPressed: function () {
                this.loginModel.setProperty("/hiddenToolbarCounter", this.loginModel.getProperty("/hiddenToolbarCounter") + 1);
            },

            onResetUserDataPressed: function () {
                var that = this;
                var onResetConfirmed = function () {
                    clearUserData(function () {
                        that.loginModel.setProperty("/user", "");
                        that.loginModel.setProperty("/password", "");
                        that.loginModel.setProperty("/rememberLogin", false);
                        MessageBox.success(that.oBundle.getText("USER_DATA_DELETED_SUCCESSFULLY"));
                    }, function () {
                        MessageBox.error(that.oBundle.getText("ERROR_USER_DATA_DELETE"));
                    });
                };

                this.getResetConfirmationDialog(onResetConfirmed.bind(this)).open();
            },

            getResetConfirmationDialog: function (executeCb) {
                var that = this;

                var oBundle = this.getOwnerComponent().getModel("i18n").getResourceBundle();
                that._oResetConfirmationDialog = new Dialog({
                    title: oBundle.getText("Warning"),
                    type: 'Message',
                    state: 'Warning',
                    content: new Text({
                        text: oBundle.getText("ERROR_USER_DATA_DELETE_UNSYNC_DATA_WILL_BE_LOST"),
                        textAlign: 'Center',
                        width: '100%'
                    }),
                    beginButton: new Button({
                        text: oBundle.getText("yes"),
                        press: function () {
                            executeCb();
                            this.getParent().close();
                        }
                    }),
                    endButton: new Button({
                        text: oBundle.getText("no"),
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        this.destroy();
                    }
                });
                // }
                return this._oResetConfirmationDialog;
            },

            onRememberLoginClicked: function (oEvent) {
                var rememberLogin = oEvent.mParameters.selected;

                if (rememberLogin === true) {
                    this.loginModel.setProperty('/rememberLogin', true);
                } else {
                    this.loginModel.setProperty('/rememberLogin', false);
                }
            }
        });
    });
