sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/models/dataObjects/NotificationItem"],

    function (JSONModel, ViewModel, Validator, GenericSelectDialog, NotificationHeader, NotificationItem) {
        "use strict";

        // constructor
        function EditNotificationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this.complaintNotificationInfo = this.scenario.getComplaintNotificationInfo();
            this.setProperty("/view/showWarrantyFlag", this.complaintNotificationInfo.showWarrantyFlag);

            this.valueStateModel = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditNotificationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        notification: null,
                        view: {
                            busy: false,
                            edit: false,
                            onlyOutageTime: false,
                            showWarrantyFlag: false,
                            errorMessages: [],
                            funcLocCatalogProfile: ""
                        }
                    };
                }
            },

            setNotification: {
                value: function (component) {
                    this.setProperty("/notification", component);
                }
            },

            getNotification: {
                value: function () {
                    return this.getProperty("/notification");
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            setOutageOnly: {
                value: function (bOutage) {
                    this.setProperty("/view/onlyOutageTime", bOutage);
                }
            },

            setFuncLocCatalogProfile: {
                value: function (sValue) {
                    this.setProperty("/view/funcLocCatalogProfile", sValue);
                }
            },
            
            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n").getResourceBundle().getText("editNotification") : this._component.getModel("i18n_core").getResourceBundle().getText("NewNotification");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function () {
                    if (this.getProperty("/view/edit")) {
                        return this._component.getModel("i18n").getResourceBundle().getText("editNotification")
                    } else {
                        return this._component.getModel("i18n_core").getResourceBundle().getText("NewNotification");
                    }

                }
            },
        });


        EditNotificationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };
        EditNotificationViewModel.prototype.loadFuncLocCatProfile = function(funcLoc){
            var that = this;
            this._requestHelper.getData("CatProfile", {funclocId : funcLoc.funclocId}, function(data){
                // add the functional location to the model/view
                if(data){
                     var notification = that.getNotification();
                    notification.FUNCT_LOC = funcLoc.funclocId;
                    notification.FUNCLOC_DESC = funcLoc.funclocTxt;
                    that.setFuncLocCatalogProfile(data[0].catalogProfile);
                    that.getValueStateModel().setProperty("/FUNCT_LOC", sap.ui.core.ValueState.None);
                }else{
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("errNoFuncLocFoundthat")));
                }
               

             }, function(error){
                that.executeErrorCallback(new Error(error));
                // leave the functional location empty
             },true,"get");
            

            
        };

        EditNotificationViewModel.prototype.load = function (notificationId) {
            this.setBusy(true);
            const that = this;
            var promises = [];

            promises.push(this._requestHelper.fetch({
                id: "Notification",
                statement: "SELECT @columns FROM NOTIFICATIONHEADER as nh " +
                    "LEFT JOIN NOTIFICATIONTYPE as nt on nt.QMART = nh.NOTIF_TYPE AND nt.SPRAS = '@spras' " +
                    "WHERE nh.NOTIF_NO = '@notifNo'"
            }, {
                notifNo: notificationId,
                spras: this.getLanguage(),
                columns: "nh.MOBILE_ID, nh.NOTIF_NO, nh.NOTIF_NO_DISPLAY, nh.SHORT_TEXT, nh.NOTIF_DATE, nh.NOTIF_TYPE, nh.EQUIPMENT, nh.EQUIPMENT_DESC, nh.SHORT_TEXT, nh.PRIORITY, nh.PRIOTYPE, nh.MATERIAL, nh.XA_STAT_PROF, " +
                    "nh.SERIALNO, nh.FUNCT_LOC, nh.FUNCLOC_DESC, nh.OBJECT_NO, nh.NOTIF_DATE, nh.COMPDATE, nh.COMPTIME, " +
                    "nh.NOTIFTIME, nh.MN_WK_CTR, nh.PLANPLANT, nh.PLANT, nh.CATPROFILE, nh.CODE_GROUP, nh.CODING, nh.ORDERID, nh.BREAKDOWN, nh.ACTION_FLAG, nt.RBNR"
            }));

            Promise.all(promises).then(function(results) {
                if (results[0].Notification.length == 0) {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_NOTIFICATION_NOT_FOUND_TEXT")));
                    return;
                }

                var notification = new NotificationHeader(results[0].Notification[0], that._requestHelper, that);
                var notificationInitQueue = [
                    notification.loadLongText(that.scenario.getComplaintNotificationInfo().longTextObjectType),
                    notification.loadAttachments()
                ];

                Promise.all(notificationInitQueue).then(function (initResults) {
                    notification._longText = initResults[0];
                    notification._CATPROFILE = notification.RBNR;
                    that.setNotification(notification);
                    that.refresh(true);
                    that.setBusy(false);
                }).catch(that.executeErrorCallback.bind(that, new Error(that._component.i18n_core.getText("ERROR_NOTIF_FETCH_LONGTEXT"))));

            }).catch(this.executeErrorCallback.bind(this, new Error(that._component.i18n_core.getText("ERROR_NOTIF_FETCH_DATA"))));
        };

        EditNotificationViewModel.prototype.save = function (successCb) {
            if (!this.validate()) {
            	if (this.getValueStateModel().getProperty("/MALFUNCODE") === sap.ui.core.ValueState.Error ){
            		this.executeErrorCallback(new Error(this._component.getModel("i18n").getResourceBundle().getText("MALFUNCTIONCODE_MISSING")));
            	}
                return;
            }

            if (this.getProperty("/view/edit")) {
                this._update(successCb);
            } else {
                this._insert(successCb);
            }
        };

        EditNotificationViewModel.prototype._update = function (successCb) {
            var notification = this.getNotification();
            var sqlStringsDelete = EditNotificationViewModel._getSubObjectDeletionsStrings(notification.NOTIF_NO);
            const that = this;

            var onAttachmentsUpdated = function() {
                executeUpdates(sqlStringsDelete, function (success) {
                    var sqlStrings = [];

                    /*
                        1. check if _completedFlag is set
                            -> yes. Create NotificationUserStatus 'USCO'
                        2. check if long text has been entered
                            -> yes. Create NotficationLongText rows from _longText
                    */

                    try {

                        sqlStrings.push(notification.update(true));

                        if (notification._completedFlag) {
                            var completeStatus = this.scenario.getComplaintNotificationInfo().completeStatus;
                            var statusMl = that.getUserStatusMl(completeStatus.USER_STATUS, completeStatus.STATUS_PROFILE);

                            if (!statusMl) {
                                that.executeErrorCallback(new Error(that._component.i18n_core.getText("USERSTATUSM1_NOT_FOUND")));
                                return;
                            }

                            sqlStrings = sqlStrings.concat(notification.addUserStatus(statusMl, true));
                        }

                        if (notification._longText.length > 0) {
                            sqlStrings = sqlStrings.concat(notification.addLongtext(true, notification._longText, that.scenario.getComplaintNotificationInfo().longTextObjectType));
                        }

                        if (notification.NOTIF_TYPE !== "A5") {
                            // create notification item
                            sqlStrings.push(that.getNewNotificationItem(notification).insert(true));
                        }


                        that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                            that.executeErrorCallback(new Error(that._component.i18n_core.getText("ERROR_NOTIF_UPDATE")));
                        });
                    } catch (err) {
                        that.executeErrorCallback(err);
                    }
                }, this.executeErrorCallback.bind(this, new Error(that._component.i18n_core.getText("ERROR_NOTIF_UPDATE_PREPARE"))));
            };

            this._updateAttachments().then(onAttachmentsUpdated.bind(this)).catch(function(err) {
                that.executeErrorCallback(err);
            });
        };

        EditNotificationViewModel.prototype._updateAttachments = function() {
            var that = this;
            var sqlStrings = [];
            var notification = this.getNotification();
            var attachments = notification.getAttachments();
            var updateQueue = [];


            // Attachments
            // Updates -> Rename actual attachment
            var updates = attachments.filter(function(attachment) {
                return attachment.MOBILE_ID != "" && attachment.hasChanges();
            });

            // Inserts -> Move from temp to doc directory
            var inserts = attachments.filter(function(attachment) {
                return attachment.MOBILE_ID == "";
            });

            // Deletes -> Delete actual attachment
            var deletions = attachments.filter(function(attachment) {
                return attachment.getDeleteFlag();
            });

            if (updates.length > 0) {
                updateQueue.push(this._renameAttachments(updates));
            }

            if (inserts.length > 0) {
                updateQueue.push(this._moveAttachmentsFromTempToDoc(inserts));
            }

            if (deletions.length > 0) {
                updateQueue.push(this._deleteAttachments(deletions));
            }

            return Promise.all(updateQueue).then(function() {
                return new Promise(function(resolve, reject) {
                    updates.forEach(function(attachment) {
                        sqlStrings.push(attachment.update(true));
                    });

                    inserts.forEach(function(attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    deletions.forEach(function(attachment) {
                        sqlStrings = sqlStrings.concat(attachment.delete(true));
                    });

                    that._requestHelper.executeStatementsAndCommit(sqlStrings, resolve, reject);
                });
            });
        };

        EditNotificationViewModel._getSubObjectDeletionsStrings = function(notifNo) {
            var sqlStringsDelete = [];

            var deleteStatusSql = "DELETE FROM NOTIFICATIONUSRSTAT WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteStatusSql = deleteStatusSql.replace("$1", notifNo);

            var deleteItemSql = "DELETE FROM NOTIFICATIONITEM WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteItemSql = deleteItemSql.replace("$1", notifNo);

            var deleteLongtextSql = "DELETE FROM NOTIFICATIONLONGTEXT WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteLongtextSql = deleteLongtextSql.replace("$1", notifNo);

            sqlStringsDelete.push(deleteStatusSql);
            sqlStringsDelete.push(deleteItemSql);
            sqlStringsDelete.push(deleteLongtextSql);

            return sqlStringsDelete;
        };

        EditNotificationViewModel.prototype._insert = function (successCb) {
            var notification = this.getNotification();

            var that = this;
            var sqlStrings = [];

            /*
                1. check if _completedFlag is set
                    -> yes. Create NotificationUserStatus 'USCO'
                2. check if long text has been entered
                    -> yes. Create NotficationLongText rows from _longText
            */

            var onDocumentsMoved = function() {
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n_core.getText("ERROR_INSERT_NEW_NOTIF")));
                });
            };

            var onDocumentsMovedError = function() {
                that.executeErrorCallback(new Error(that._component.i18n_core.getText("ERROR_ATTACHMENT_MOVE")));
            };


            try {

                sqlStrings.push(notification.insert(true));

                if (notification._completedFlag) {
                    var completeStatus = this.scenario.getComplaintNotificationInfo().completeStatus;
                    var statusMl = this.getUserStatusMl(completeStatus.USER_STATUS, completeStatus.STATUS_PROFILE);

                    if (!statusMl) {
                        this.executeErrorCallback(new Error(that._component.i18n_core.getText("USERSTATUSM1_NOT_FOUND")));
                        return;
                    }

                    sqlStrings = sqlStrings.concat(notification.addUserStatus(statusMl, true));
                }

                if (notification._longText.length > 0) {
                    sqlStrings = sqlStrings.concat(notification.addLongtext(true, notification._longText, this.scenario.getComplaintNotificationInfo().longTextObjectType));
                }

                // create notification item
                sqlStrings.push(this.getNewNotificationItem(notification).insert(true));

                if (notification.getAttachments() != null) {
                    notification.getAttachments().forEach(function(attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    this._moveAttachmentsFromTempToDoc(notification.getAttachments()).then(onDocumentsMoved.bind(this)).catch(onDocumentsMovedError.bind(this));
                    return;
                }

                onDocumentsMoved.apply(this);
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EditNotificationViewModel.prototype.getNewNotificationItem = function (notification) {
            var notificationItem = new NotificationItem(null, this._requestHelper, this);
            notificationItem.NOTIF_NO = notification.NOTIF_NO;
            notificationItem.DL_CAT_TYP = notification.OBJ_CAT_TYP;
            notificationItem.DL_CODEGRP = notification.OBJ_CODE_GROUP;
            notificationItem.DL_CODE = notification.OBJ_CODING;
            notificationItem.ITEM_SORT_NO = "0001";
            notificationItem.ITEM_KEY = '0001';

            return notificationItem;
        };

        EditNotificationViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("notification.NOTIF_TYPE", this._component.i18n_core.getText("NOTIF_TYPE_SELECT")).isEmpty();
            validator.check("notification.PRIORITY", this._component.i18n_core.getText("PRIORITY_SELECT")).isEmpty();
            validator.check("notification.MN_WK_CTR", this._component.i18n_core.getText("WORK_CENTER_SELECT")).isEmpty();
            validator.check("notification.SHORT_TEXT", this._component.i18n_core.getText("SUBJECT_ENTER")).isEmpty();

            if (this.getNotification().NOTIF_TYPE == "Y0" || this.getNotification().NOTIF_TYPE == "Y1") {
                validator.check("notification.CODE_GROUP", this._component.i18n_core.getText("CODE_GROUP_SELECT")).isEmpty();
                validator.check("notification.CODING", this._component.i18n_core.getText("CODE_SELECT")).isEmpty();
                validator.check("notification.EQUIPMENT", this._component.i18n_core.getText("EQUIPMENT_SELECT")).isEmpty();
                validator.check("notification.OBJ_CODE_GROUP", this._component.i18n_core.getText("CODE_GROUP_OBJECT_PART_SELECT")).isEmpty();
                validator.check("notification.OBJ_CODING", this._component.i18n_core.getText("CODE_OBJECT_PART_SELECT")).isEmpty();
            }

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
        EditNotificationViewModel.prototype.displayNotifTypeDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oNotifTypeSelectDialog) {
                this._oNotifTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.NotificationTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            var acceptedNotifTypes = this.scenario.getAcceptedNotificationTypes();

            this._oNotifTypeSelectDialog.display(oEvent, 'NotificationType', function (notifType) {
                var notifTypeAccepted = acceptedNotifTypes.filter(function (type) {
                    return type == notifType.QMART;
                });

                return notifTypeAccepted.length > 0;
            });
        };

        EditNotificationViewModel.prototype.handleNotifTypeSelect = function (oEvent) {
            this._oNotifTypeSelectDialog.select(oEvent, 'QMART', [{
                'NOTIF_TYPE': 'QMART'
            }, {
                'PRIOTYPE': 'ARTPR'
            }, {
                '_CATPROFILE': 'RBNR'
            }], this, "/notification");

            if (this.getNotification().NOTIF_TYPE == "Y0") {
                this.getNotification().CODE_GROUP = "OBSERVAT";
            } else {
                this.getNotification().CODE_GROUP = "";
            }

            this.getNotification().CODING = "";
            this.getNotification().OBJ_CODE_GROUP = "";
            this.getNotification().OBJ_CODING = "";
            this.getNotification().OBJ_CAT_TYP = "";
            this.getNotification()._codingText = "";
            this.getNotification()._objCodingText = "";

            this.refresh();
        };

        EditNotificationViewModel.prototype.handleNotifTypeSearch = function (oEvent) {
            this._oNotifTypeSelectDialog.search(oEvent, ["QMART", "QMARTX"]);
        };

        EditNotificationViewModel.prototype.displayPriorityDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oPrioritySelectDialog) {
                this._oPrioritySelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.PrioritySelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oPrioritySelectDialog.display(oEvent, 'PriorityTypes', function (prio) {
                return prio.PRIOTYPE == that.getProperty("/notification/PRIOTYPE");
            });
        };

        EditNotificationViewModel.prototype.handlePrioritySelect = function (oEvent) {
            this._oPrioritySelectDialog.select(oEvent, 'PRIORITY', [{
                'PRIORITY': 'PRIORITY'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handlePrioritySearch = function (oEvent) {
            this._oPrioritySelectDialog.search(oEvent, ["PRIORITY_DESC", "PRIORITY"]);
        };

        EditNotificationViewModel.prototype.displayWorkCtrDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oWorkCtrDialog) {
                this._oWorkCtrDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.WorkCenterSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oWorkCtrDialog.display(oEvent, 'WorkCenterMl');
        };

        EditNotificationViewModel.prototype.handleWorkCtrSelect = function (oEvent) {
            this._oWorkCtrDialog.select(oEvent, 'ARBPL', [{
                'MN_WK_CTR': 'ARBPL'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleWorkCtrSearch = function (oEvent) {
            this._oWorkCtrDialog.search(oEvent, ["KTEXT", "ARBPL"]);
        };

        EditNotificationViewModel.prototype.displayFuncLocDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oFuncLocDialog) {
                this._oFuncLocDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.FuncLocSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oFuncLocDialog.display(oEvent, {
                tableName: "FunctionLocs",
                params: {
                    spras: this.getLanguage()
                },
                actionName: "get"
            });
        };

        EditNotificationViewModel.prototype.handleFuncLocSelect = function (oEvent) {
            this._oFuncLocDialog.select(oEvent, 'TPLNR', [{
                'FUNCT_LOC': 'TPLNR'
            }, {
                'FUNCLOC_DESC': 'SHTXT'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleFuncLocSearch = function (oEvent) {
            this._oFuncLocDialog.search(oEvent, ["SHTXT", "FUNCLOC_DISPLAY"]);
        };


        EditNotificationViewModel.prototype.displayEquipmentDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oEquipmentDialog) {
                this._oEquipmentDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.EquipmentSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oEquipmentDialog.display(oEvent, {
                tableName: "Equipments",
                params: {
                    spras: this.getLanguage(),
                    tplnr: this.getProperty("/notification/FUNCT_LOC")
                },
                actionName: "installedAtFuncLoc"
            });
        };

        EditNotificationViewModel.prototype.handleEquipmentSelect = function (oEvent) {
            this._oEquipmentDialog.select(oEvent, 'EQUNR', [{
                'EQUIPMENT': 'EQUNR'
            }, {
                '_equipmentText': 'SHTXT'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleEquipmentSearch = function (oEvent) {
            this._oEquipmentDialog.search(oEvent, ["SHTXT", "EQUIPMENT_DISPLAY"]);
        };

        EditNotificationViewModel.prototype.displayCodeGroupDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodeGroupDialog) {
                this._oCodeGroupDialog = new GenericSelectDialog("WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getProperty("/notification/_CATPROFILE"),
                    catalogType: "D"
                },
                actionName: "valueHelp"
            });
        };

        EditNotificationViewModel.prototype.displayObjCodeGroupDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodeGroupDialog) {
                this._oCodeGroupDialog = new GenericSelectDialog("WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getProperty("/notification/_CATPROFILE"),
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditNotificationViewModel.prototype.handleCodeGroupSelect = function (oEvent) {
            this._oCodeGroupDialog.select(oEvent, 'CODE_CAT_GROUP', [{
                'CODE_GROUP': 'CODEGROUP'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleObjCodeGroupSelect = function (oEvent) {
            this._oCodeGroupDialog.select(oEvent, 'CODE_CAT_GROUP', [{
                'OBJ_CODE_GROUP': 'CODEGROUP'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleCodeGroupSearch = function (oEvent) {
            this._oCodeGroupDialog.search(oEvent, ["CODEGROUP_TEXT", "CODE_CAT_GROUP"]);
        };

        EditNotificationViewModel.prototype.displayCodingDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/notification/CODE_GROUP"),
                    catalogType: "D"
                },
                actionName: "valueHelp"
            });
        };

        EditNotificationViewModel.prototype.displayObjCodingDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/notification/OBJ_CODE_GROUP"),
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditNotificationViewModel.prototype.handleCodingSelect = function (oEvent) {
            this._oCodingDialog.select(oEvent, 'CODE', [{
                'CODING': 'CODE'
            }, {
                '_codingText': 'CODE_TEXT'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleObjCodingSelect = function (oEvent) {
            this._oCodingDialog.select(oEvent, 'CODE', [{
                'OBJ_CODING': 'CODE'
            }, {
                'OBJ_CAT_TYP': 'CATALOG_TYPE'
            }, {
                '_objCodingText': 'CODE_TEXT'
            }], this, "/notification");
        };

        EditNotificationViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oCodingDialog.search(oEvent, ["CODEGROUP_TEXT", "CODE_CAT_GROUP"]);
        };

        EditNotificationViewModel.prototype.createNewNotification = function () {
            var notificationObject = new NotificationHeader(null, this._requestHelper, this);

            notificationObject._completedFlag = false;
            notificationObject._warrantyFlag = false;
            notificationObject._orderReferenceFlag = this.scenario.getComplaintNotificationInfo().withReferenceToServiceOrder;
            notificationObject._longText = "";
            notificationObject.OBJ_CODE_GROUP = "";
            notificationObject.OBJ_CODING = "";
            notificationObject.OBJ_CAT_TYP = "";

            return notificationObject;
        };

        EditNotificationViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    SHORT_TEXT: sap.ui.core.ValueState.None,
                    NOTIF_TYPE: sap.ui.core.ValueState.None,
                    PRIORITY: sap.ui.core.ValueState.None,
                    MN_WK_CTR: sap.ui.core.ValueState.None,
                    CODE_GROUP: sap.ui.core.ValueState.None,
                    CODING: sap.ui.core.ValueState.None,
                    OBJ_CODE_GROUP: sap.ui.core.ValueState.None,
                    OBJ_CODING: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditNotificationViewModel.prototype.resetValueStateModel = function () {
            this.valueStateModel.setProperty("/SHORT_TEXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/NOTIF_TYPE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/PRIORITY", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/MN_WK_CTR", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CODE_GROUP", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CODING", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/OBJ_CODE_GROUP", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/OBJ_CODING", sap.ui.core.ValueState.None);
            this.setProperty("/view/errorMessages", []);
        };

        EditNotificationViewModel.prototype.constructor = EditNotificationViewModel;

        return EditNotificationViewModel;
    });
