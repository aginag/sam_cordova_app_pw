sap.ui.define([],
   
   function() {
      "use strict";
      
      // constructor
      function Column(tableName, domain, columnName, count, nullable) {
         this.tableName = tableName;
         this.domain = this._setDomain(domain);
         this.name = columnName;
         this.maxCount = this._formatCount(count);
         this.nullable = this._formatNullable(nullable);
      }
      
      Column.prototype = {
         check: function(value) {
            // Check value corresponds to nullable
            if (this.isNull(value) && !this.nullable) {

               throw this.ERRORS.not_nullable.getError(this);
               return false;
            }
            
            // If value is null, no checks required
            if (this.isNull(value)) {
               return true;
            }
            
            // Check value corresponds to domain
            if (this.domain && !this.domain.validate(value)) {

               throw this.ERRORS.domain.getError(value, this, this.domain);
               return false;
            }
            
            // Check value corresponds to maxCount
            if (!this._checkMaxCount(value)) {

               throw this.ERRORS.column_size.getError(value, this);
               return false;
            }
            
         },
         
         isNull: function(value) {
            if (value == null || undefined) {
               return true;
            }
            
            return value.toString() == "";
         }
      };
      
      Column.prototype.DOMAINS = {
         2: {
            type: "integer",
            validate: function(value) {
               value = parseInt(value);
               return !isNaN(value);
            }
         },
         3: {
            type: "numeric",
            validate: function(value) {
               value = parseFloat(value);
               return !isNaN(value);
            }
         },
         9: {
            type: "varchar",
            validate: function(value) {
               try {
                  value = value.toString();
                  
                  // Replacing all single quotes from string
                  value = value.replace(/'/g, '');
                  return true;
               } catch (err) {
                  return false;
               }
            }
         },
         13: {
            type: "timestamp",
            format: "YYYY-MM-DD HH:mm:ss.SSS",
            validate: function(value) {

               var date = moment(value).format(this.format);
               
               return moment(date, this.format).format(this.format) === date
            }
         },
         14: {
            type: "time",
            format: "HH:mm:ss.SSS",
            validate: function(value) {

               var date = moment(value).format(this.format);
      
               return moment(date, this.format).format(this.format) === date
            }
         },
         19: {
            type: "tinyint",
            validate: function(value) {
               // TODO add check for tinyint
               return true;
            }
         }
      };
      
      Column.prototype.ERRORS = {
        domain: {
            getError: function(value, column, domain) {
               return new Error("COLUMN DATA TYPE Error: The value (" + value.toString() + ") for the column (" + column.tableName + "/" + column.name + ") does not match with the data type (" + domain.type + ")");
            }
        },
        not_nullable: {
            getError: function(column) {
               return new Error("COLUMN NOT_NULLABLE Error: The column (" + column.tableName + "/" + column.name + ") does not allow null entries");
            }
        },
        column_size: {
            getError: function(value, column) {
               return new Error("COLUMN_SIZE Error: The value (" + value.toString() + ") for the column (" + column.tableName + "/" + column.name + ") exceeds the maximum allowed column size (" + column.maxCount + ")");
            }
        }
      };
      
      Column.prototype._checkMaxCount = function(value) {
         if (this.domain && this.domain.type == "varchar") {
            // Only do maxLength check on varchar data type
            return value.toString().length <= this.maxCount;
         }
         
         return true;
      };
   
      Column.prototype._setDomain = function(domainKey) {
         if (this.DOMAINS.hasOwnProperty(domainKey)) {
            return this.DOMAINS[domainKey];
         }
         
         return null;
      };
      
      Column.prototype._formatCount = function(count) {
         count = parseInt(count);
         return count % 4 == 0 ? count / 4 : count;
      };
   
      Column.prototype._formatNullable = function(nullable) {
         return nullable == "Y" ? true : false;
      };
   
      Column.prototype.constructor = Column;
      
      return Column;
   });