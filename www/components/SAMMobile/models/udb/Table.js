sap.ui.define(["SAMMobile/models/udb/Column"],
   
   function(Column) {
      "use strict";
      
      // constructor
      function Table(name, columnsJSON) {
         this.name = name;
         this.columns = this._initColumns(columnsJSON);
      }
      
      Table.prototype = {
         getColumn: function(columnName) {
            var column = this.columns.filter(function(c) {
               return c.name == columnName;
            });
            
            return column.length == 0 ? null : column[0];
         }
      };
      
      Table.prototype._initColumns = function(columnsJSON) {
         var that = this;
         return columnsJSON.map(function(column) {
            return new Column(that.name, column.domain, column.column_name, column.count, column.nullable);
         })
      };
   
      Table.prototype.constructor = Table;
      
      return Table;
   });