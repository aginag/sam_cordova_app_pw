sap.ui.define(["sap/ui/model/json/JSONModel"],
   
   function(JSONModel) {
      "use strict";
      
      // constructor
      function CustomizingModel(manifest, requestHelper, customizingTables, customizingModelLsKey) {
         this.model = new JSONModel();
         this.langId = null;
         this.requestHelper = requestHelper;
         this._customizingTables = customizingTables;
         this.manifest = manifest.getEntry("sap.app");
         this.customizingModelLsKey = customizingModelLsKey;
         this.cacheKeys = this.manifest.SAMConfig.cache;
      }
      
      CustomizingModel.prototype = {
         
         loadData: function(langId, hardReset, successCb, errorCb) {
            hardReset = hardReset === undefined ? false : hardReset;
            this.langId = langId;
            var that = this;

            var load_success = jQuery.proxy(function(data) {
               this.setAllData(data);
               
               if (successCb !== undefined) {
                  successCb();
               }
            }, this);
            
            var load_error = jQuery.proxy(function() {
               if (errorCb !== undefined) {
                  errorCb(new Error("Error loading customizing model"));
               }
            }, this);
            
            // check if localStorage exists
            // Yes -> load
            // No -> get from database
            
            if (localStorage.getItem(this.customizingModelLsKey) === null || hardReset) {
               
               this.requestHelper.getData(this._customizingTables, {
                  "language": langId
               }, load_success, load_error);
               
            } else {
               var cachedCustomizingModel = JSON.parse(localStorage.getItem(this.customizingModelLsKey));
               this.model.setData(cachedCustomizingModel);
               
               if(successCb !== undefined) {
                  successCb();
               }
            }
            
         },
         
         fetchSingleModelData: function(model, successCb) {
            var that = this;
            model = model instanceof Array ? model : [model];
            
            var load_success = jQuery.proxy(function(data) {
               this.setAllData(data);
               successCb(data);
            }, this);
            
            var load_error = jQuery.proxy(this.load_error, this);
            
            this.requestHelper.getData(model, {
               "language": this.langId
            }, load_success, load_error);
         },
         
         load_error: function(result) {
            // do something
         },
         
         setAllData: function(data) {
            for (var key in data) {
               this.model.setProperty("/" + key, data[key]);
            }
            
            //set oData to localStorage
            localStorage.setItem(this.customizingModelLsKey, JSON.stringify(this.model.oData));
            
         },
         
         getModel: function(modelName, successCb) {
            var that = this;
            
            if (modelName instanceof Array) {
               var returnObject = {};
               
               var toBeFetched = modelName.filter(function(name) {
                  if (that.model.oData.hasOwnProperty(name)) {
                     returnObject[name] = that.model.oData[name];
                     
                     return false;
                  }
                  
                  return true;
               });
               
               if (toBeFetched.length > 0) {
                  this.fetchSingleModelData(toBeFetched, function() {
                     
                     toBeFetched.forEach(function(name) {
                        returnObject[name] = that.model.oData[name];
                     });
                     
                     successCb(returnObject);
                     
                  });
               } else {
                  successCb(returnObject);
               }
               
            } else {
               
               if (this.model.oData.hasOwnProperty(modelName)) {
                  successCb(this.model.oData[modelName]);
               } else {
                  //Load that model
                  this.fetchSingleModelData(modelName, function() {
                     successCb(that.model.oData[modelName]);
                  });
               }
               
            }
            
         },
         
         resetCache: function() {
            var that = this;
            Object.keys(this.cacheKeys).forEach(function(key) {
               localStorage.removeItem(that.cacheKeys[key]);
            });
         },
         
         refresh: function() {
            this.model.refresh();
         }
      };
      
      var CustomizingModel = CustomizingModel;
      
      return CustomizingModel;
   });
