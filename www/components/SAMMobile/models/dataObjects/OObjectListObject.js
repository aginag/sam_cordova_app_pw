sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function OObjectListObject(data, requestHelper, model) {
            DataObject.call(this, "OrderObjectList", "OrderObjectList", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "ORDERID", "PROCESSING_IND", "COUNTER", "NOTIF_NO", "EQUIPMENT", "EQUIDESCR", "NOTIF_NO_REF", "FUNCT_LOC", "FUNCLOC_REF", "FUNCLDESCR", "SHORT_TEXT", "ACTIVITY", "SUB_ACTIVITY",
                "ACTION_FLAG"];
            this.setDefaultData();
        }

        OObjectListObject.prototype = Object.create(DataObject.prototype, {
            setProcessingInd: {
                value: function(bActive) {
                    this.PROCESSING_IND = bActive ? "X" : "";
                }
            },
            setNotification: {
                value: function(notificationHeader) {
                    this.notification = notificationHeader;
                }
            },
            getNotification: {
                value: function() {
                    return this.notification;
                }
            }
        });

        OObjectListObject.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "COUNTER") {
                    value = that.getRandomCharUUID(10);
                }

                that[column] = value;
            });
        };

        OObjectListObject.prototype.constructor = OObjectListObject;

        return OObjectListObject;
    }
);