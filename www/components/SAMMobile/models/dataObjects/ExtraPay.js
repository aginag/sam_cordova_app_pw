sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],
    
    function (JSONModel, DataObject) {
        "use strict";
        
        //constructor
        function ExtraPay(data, requestHelper, model) {
            DataObject.call(this, "ZPMC_EXTRAPAY", "ExtraPay", requestHelper, model);

            this.data = data;

            this._VALUE_STATE = sap.ui.core.ValueState.None;

            this.columns = ["ACTION_FLAG", "AMOUNT", "APPROVED", "APR_DAT", "CRE_DAT", "EXEC_DAT", "MOBILE_ID", "PERS_NO", "UNIT", "WGTYPE"];
            this.setDefaultData();
        }

        ExtraPay.prototype = Object.create(DataObject.prototype, {
            setValueState: {
                value: function(valueState) {
                    this._VALUE_STATE = valueState;
                }
            },
        });

        ExtraPay.prototype.initNewObject = function() {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });
        };


        ExtraPay.prototype.constructor = ExtraPay;

        return ExtraPay;

    });