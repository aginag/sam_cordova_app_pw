sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/helpers/FileSystem", "SAMContainer/models/DataObject"],

    function (JSONModel, FileSystem, DataObject) {
        "use strict";

        // constructor
        function ZPWAttachment(data, requestHelper, model) {
            DataObject.call(this, "ZPW_ATTACHMENTS", "ZPW_ATTACHMENTS", requestHelper, model);

            this.data = data;
            this.folderPath = null;
            this.fileSystem = new FileSystem();
            this._DELETE_FLAG = false;
            this._OLD_FILENAME = "";

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.columns = ["MOBILE_ID", "FILE_NAME", "EXTENSION", "MODIFIED_ON", "SIZE", "CREATED_ON"];
            this.setDefaultData();
        }

        ZPWAttachment.prototype = Object.create(DataObject.prototype, {
            setFilename: {
                value: function (fileName) {
                    this.FILE_NAME = fileName;
                }
            },

            setFileExt: {
                value: function (fileExt) {
                    this.EXTENSION = fileExt;
                }
            },

            setFilesize: {
                value: function (fileSize) {
                    this.SIZE = fileSize;
                }
            },

            setFolderPath: {
                value: function (folderPath) {
                    this.folderPath = folderPath;
                }
            },

            getFileName: {
                value: function () {
                    return this.FILE_NAME;
                }
            },

            getFileExt: {
                value: function () {
                    return this.EXTENSION;
                }
            },

            setDeleteFlag: {
                value: function (bDelete) {
                     this._DELETE_FLAG = bDelete;
                }
            },

            getDeleteFlag: {
                value: function () {
                    return this._DELETE_FLAG;
                }
            },

            setRenamedFileName: {
                value: function (newFileName) {
                    if (this._OLD_FILENAME == "") {
                        this._OLD_FILENAME = this.FILE_NAME;
                    }

                    this.FILE_NAME = newFileName;
                }
            }
        });

        ZPWAttachment.prototype.onAfterExistingDataSet = function() {
            this.ON_DATABASE = true;
            this.setFolderPath(this.fileSystem.getRootDataStorage());
            this.FILEEXT = this["EXTENSION"] ? this.EXTENSION.trim() : "";
        };

        ZPWAttachment.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });

            this.ON_DATABASE = false;
        };

        ZPWAttachment.prototype.constructor = ZPWAttachment;

        return ZPWAttachment;
    }
);