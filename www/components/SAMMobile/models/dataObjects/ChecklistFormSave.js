sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function ChecklistFormSave(data, requestHelper, model) {
            DataObject.call(this, "SAM_CHECKLIST_FORM_SAVE", "SAM_CHECKLIST_FORM_SAVE", requestHelper, model);

            this.data = data;

            this.FILENAME = "";
            this.ACTION_FLAG = "N";

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.setDefaultData();
        }

        ChecklistFormSave.prototype = Object.create(DataObject.prototype, {
            setHTML: {
                value: function(html) {
                    this.HTML_CONTENT = html;
                }
            },

            getHTML: {
                value: function() {
                    return this.HTML_CONTENT;
                }
            },

            setState: {
                value: function(state) {
                    this.HTML_STATE = JSON.stringify(state);
                }
            },

            getState: {
                value: function() {
                    return JSON.parse(this.HTML_STATE);
                }
            },

            setFormName: {
                value: function(formName) {
                    this.FORM_NAME = formName;
                    this.FILENAME = formName + "(DRAFT)";
                }
            },

            getFormName: {
                value: function() {
                    return this.FORM_NAME;
                }
            },

            setOrderId: {
                value: function(orderId) {
                    this.ORDERID = orderId;
                }
            }
        });

        ChecklistFormSave.prototype.onAfterExistingDataSet = function() {
            this.FILENAME = this.FORM_NAME + "(DRAFT)";
        };

        ChecklistFormSave.prototype.initNewObject = function() {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "UPDATED_AT") {
                    value = dateNow.dateTime;
                }

                that[column] = value;
            });
        };

        ChecklistFormSave.prototype.constructor = ChecklistFormSave;

        return ChecklistFormSave;
    }
);