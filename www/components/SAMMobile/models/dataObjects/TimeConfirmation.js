sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject",  "SAMMobile/helpers/timeHelper"],

    function(JSONModel, DataObject, timeHelper) {
        "use strict";
        // constructor
        function TimeConfirmation(data, requestHelper, model) {
            DataObject.call(this, "ZPMC_TIMECONF", "TimeConfirmation", requestHelper, model);

            this.data = data;
            this._DEFAULT_HOURS = 0.5;
            this._DEFAULT_MINUTES = 30;
            this._startTime = null;
            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.languageTimeFormat = timeHelper.getDisplayTimeFormatForCurrentLanguage(this.model.getLanguage());

            this.columns = ["MOBILE_ID", "ACTION_FLAG", "ACTIVITY", "ACT_WORK", "B_EXP_FLAG", "B_VALUE_ID", "CONF_CNT", "CONF_NO", "CREATE_DAT", "CREATE_TIM", "END_DATE", "END_TIME",
                "KTEXT", "K_EXP_FLAG", "K_VALUE_ID", "ORDERID", "PERS_NO", "PLANT", "START_DATE", "START_TIME", "T_EXP_FLAG", "T_VALUE_ID", "UN_WORK", "USERID", "WORK_CNTR"];
            this.setDefaultData();
        }

        TimeConfirmation.prototype = Object.create(DataObject.prototype, {

            setMinutes: {
                value: function (newMinutes) {
                    this.ACT_WORK = newMinutes.toFixed(0);
                    this.updateEndTime();
                }
            },

            setHours: {
               value: function (newHours) {
                  this.ACT_WORK = newHours.toFixed(2);
                  this.updateEndTime();
               }
            },

            setStartEndTime: {
              value: function (newStartTime) {
                 var newStartDateMoment = moment(newStartTime);
                 newStartDateMoment.hours(0);
                 newStartDateMoment.minutes(0);
                 newStartDateMoment.seconds(0);
                 newStartDateMoment.milliseconds(0);
                 
                 var newStartTimeMoment = moment(newStartTime);
                  newStartTimeMoment.seconds(0);
                  newStartTimeMoment.milliseconds(0);

                 this._startTime = newStartTime;
                 this._endTime = newStartTime;

                 this.START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                 this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                 this.END_DATE = this.START_DATE;
                 this.END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                 //this.updateEndTime();
              }
           }
        });
        
        TimeConfirmation.prototype.updateEndTime = function() {
            // Add HOURS to _startTime
           var timeHoursFloat = parseFloat(this.ACT_WORK);
           var endDateTime = moment(this._startTime).add(timeHoursFloat, 'minutes');
            endDateTime.seconds(0);
            endDateTime.milliseconds(0);

           this.END_TIME = endDateTime.format("YYYY-MM-DD HH:mm:ss.SSS");
        };

        TimeConfirmation.prototype.onAfterExistingDataSet = function() {
            if (this.hasOwnProperty("START_DATE") && this.hasOwnProperty("START_TIME") &&
                this.hasOwnProperty("END_DATE") && this.hasOwnProperty("END_TIME")) {

                // Set Helper properties
                this._startTime = this.START_TIME;
                this._endTime = this.END_TIME;

                this._START_DATE = moment(this.START_DATE.split(" ").shift() + " " + this.START_TIME.split(" ").pop());
                this._END_DATE = moment(this.END_DATE.split(" ").shift() + " " + this.END_TIME.split(" ").pop());

                if(this.model.getLanguage() === 'D') {
                    this.FORMATTED_START_TIME = moment(this._START_DATE).format("DD.MM.YYYY HH:mm");
                    this.FORMATTED_END_TIME = moment(this._END_DATE).format("DD.MM.YYYY HH:mm");
                } else {
                    this.FORMATTED_START_TIME = moment(this._START_DATE).format("YYYY/MM/DD HH:mm");
                    this.FORMATTED_END_TIME = moment(this._END_DATE).format("YYYY/MM/DD HH:mm");
                }
            }
        };

        TimeConfirmation.prototype.setStartTime = function(startTime) {
            // Set Helper properties
            this._startTime = startTime;
            this.setNewStartTime(startTime);
        };

        TimeConfirmation.prototype.setNewStartTime = function(newStartTime) {
            // Set Helper properties

            var newStartDateMoment = moment(newStartTime);
            newStartDateMoment.hours(0);
            newStartDateMoment.minutes(0);
            newStartDateMoment.seconds(0);
            newStartDateMoment.milliseconds(0);

            var newStartTimeMoment = moment(newStartTime);
            newStartTimeMoment.seconds(0);
            newStartTimeMoment.milliseconds(0);

            this._startTime = newStartTime;

            this.START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
            this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

            var duration = moment.duration(moment(this.END_TIME).diff(moment(this.START_TIME))).asMinutes();
            this.ACT_WORK = duration.toFixed(0);
        };

        TimeConfirmation.prototype.setEndTime = function(newEndTime) {
            // Set Helper properties

            var newEndDateMoment = moment(newEndTime);
            newEndDateMoment.hours(0);
            newEndDateMoment.minutes(0);
            newEndDateMoment.seconds(0);
            newEndDateMoment.milliseconds(0);

            var newEndTimeMoment = moment(newEndTime);
            newEndTimeMoment.seconds(0);
            newEndTimeMoment.milliseconds(0);

            this._endTime = newEndTime;

            this.END_DATE = newEndDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
            this.END_TIME = newEndTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

            var duration = moment.duration(moment(this.END_TIME).diff(moment(this.START_TIME))).asMinutes();
            this.ACT_WORK = duration.toFixed(0);
        };

        TimeConfirmation.prototype.initNewObject = function() {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function(column) {
                var value = "";
               
                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "UN_WORK") {
                    value = "H";
                } else if (column == "ADD_CONF") {
                    value = "X";
                }else if (column == "CONF_CNT") {
                    value = that.getRandomIntegerUUID(8);

                }

                that[column] = value;
            });

            // Helper properties
            this.setStartEndTime(dateNow.dateTime);
            this.setHours(this._DEFAULT_HOURS);
            this.onAfterExistingDataSet();
        };

        TimeConfirmation.prototype.constructor = TimeConfirmation;

        return TimeConfirmation;
    }
);
