sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function MaterialConfirmation(data, requestHelper, model) {
            DataObject.call(this, "MaterialConfirmation", "MaterialConfirmation", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "ORDERID", "ORDER_ITNO", "MATERIAL", "ACTIVITY", "SERIALNO", "ITEM_TEXT", "ENTRY_QNT", "ENTRY_UOM", "MAT_DOC", "DOC_YEAR", "PLANT", "STGE_LOC", "BATCH",
                "CALC_MOTIVE", "ACTION_FLAG"];
            this.setDefaultData();
        }

        MaterialConfirmation.prototype = Object.create(DataObject.prototype, {});

        MaterialConfirmation.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = that.getRandomUUID(12);
                }

                that[column] = value;
            });
        };

        MaterialConfirmation.prototype.constructor = MaterialConfirmation;

        return MaterialConfirmation;
    }
);