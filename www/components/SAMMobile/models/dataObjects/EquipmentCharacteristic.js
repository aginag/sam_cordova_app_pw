sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject", "SAMMobile/models/dataObjects/EquipmentCharacteristic"],

    function (JSONModel, DataObject) {
        "use strict";

        // constructor
        function EquipmentCharacteristic(data, requestHelper, model) {
            DataObject.call(this, "EQUIPMENTCHARACS", "EquipmentCharacteristic", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "ACTION_FLAG", "CHARACT", "CHARACT_DESCR", "CLASS", "CURRENCY_FROM", "CURRENCY_TO", "EQUNR", "TYPE", "UNIT_FROM",
                            "UNIT_TO", "VALUE_DATE", "VALUE_FROM", "VALUE_NEUTRAL_FROM", "VALUE_NEUTRAL_TO", "VALUE_TO", "ATZHL"];
            this.setDefaultData();
        }

        EquipmentCharacteristic.prototype = Object.create(DataObject.prototype, {});

        EquipmentCharacteristic.prototype.constructor = EquipmentCharacteristic;

        return EquipmentCharacteristic;
    }
);