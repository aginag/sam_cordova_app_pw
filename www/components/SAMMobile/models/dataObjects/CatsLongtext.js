sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject", "SAMMobile/models/dataObjects/Longtext"],

    function (JSONModel, DataObject, Longtext) {
        "use strict";

        // constructor
        function CatsLongtext(data, requestHelper, model) {
            Longtext.call(this, "CatsLongtext", "CatsLongtext", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "COUNTER", "OBJTYPE", "LINE_NUMBER", "FORMAT_COL", "TEXT_LINE", "ACTION_FLAG"];
            this.setDefaultData();
        }

        CatsLongtext.prototype = Object.create(Longtext.prototype, {});

        CatsLongtext.prototype._getNewRow = function (string, lineNumber, objKey, objType, catsHeader) {
            var newRow = new CatsLongtext(null, this.requestHelper);

            newRow.TEXT_LINE = string;
            newRow.LINE_NUMBER = lineNumber;
            newRow.OBJTYPE = objType;
            newRow.COUNTER = catsHeader.COUNTER;
            newRow.FORMAT_COL = "*";

            return newRow;
        };

        // Static method
        CatsLongtext.formatToText = Longtext.prototype.formatToText;

        CatsLongtext.prototype.constructor = CatsLongtext;

        return CatsLongtext;
    }
);