sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],
   
   function(JSONModel, DataObject) {
      "use strict";
      // constructor
      function NotificationUserStatus(data, requestHelper, model) {
         DataObject.call(this, "NotificationUsrStat", "NotificationUsrStat", requestHelper, model);
         
         this.data = data;

         this.columns = ["MOBILE_ID", "OBJNR", "STATUS", "CHANGED", "USER_STATUS_CODE", "USER_STATUS_DESC", "INACT", "NOTIF_NO", "ACTION_FLAG"];
         this.setDefaultData();
      }
      
      NotificationUserStatus.prototype = Object.create(DataObject.prototype, {
         setActive: {
            value: function(bActive) {
               this.INACT = bActive ? "" : "X";
               this.CHANGED = "X";
            }
         }
      });
      
      NotificationUserStatus.prototype.initFromMl = function(statusMl) {
         var that = this;
         
         this.columns.forEach(function(column) {
            if (column !== "ACTION_FLAG" && column !== "SPRAS" && column !== "MOBILE_ID" && column !== "STATUS_PROFILE") {
               that[column] = statusMl.hasOwnProperty(column) ? statusMl[column] : "";
            }
            
            if (column == "STATUS") {
               that[column] = statusMl["USER_STATUS"];
            }
         });
      };
      
      NotificationUserStatus.prototype.initNewObject = function() {
         var that = this;
         
         this.columns.forEach(function(column) {
            var value = "";
            
            if (column == "ACTION_FLAG") {
               value = "N";
            } else if (column == "MOBILE_ID") {
               value = "";
            } else if (column == "CHANGED") {
                value = "X";
            }
            
            that[column] = value;
         });
      };
      
      NotificationUserStatus.prototype.constructor = NotificationUserStatus;
      
      return NotificationUserStatus;
   }
);