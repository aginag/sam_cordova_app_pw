sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function OrderComponent(data, requestHelper, model) {

            DataObject.call(this, "OrderComponent", "OrderComponent", requestHelper, model);

            this.data = data;

            this._VALUE_STATE = sap.ui.core.ValueState.None;

            this.columns = ["MOBILE_ID", "ORDERID", "ITEM_TEXT", "ITEM_CAT", "MATERIAL", "ACTIVITY", "PLANT", "STGE_LOC", "REQUIREMENT_QUANTITY", "REQUIREMENT_QUANTITY_UNIT", "RESERV_NO", "RES_ITEM", "ACTION_FLAG"];
            this.setDefaultData();
        }

        OrderComponent.prototype = Object.create(DataObject.prototype, {

            setValueState: {
                value: function(valueState) {
                    this._VALUE_STATE = valueState;
                }
            },

        });

        OrderComponent.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "REQUIREMENT_QUANTITY") {
                    value = 1;
                } else if (column == "ITEM_CAT") {
                    value = 'L';
                }


                that[column] = value;
            });
        };

        OrderComponent.prototype.constructor = OrderComponent;

        return OrderComponent;
    }
);
