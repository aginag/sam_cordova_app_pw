sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject", "SAMMobile/models/dataObjects/FuncLocCharacteristic"],

    function (JSONModel, DataObject) {
        "use strict";

        // constructor
        function FuncLocCharacteristic(data, requestHelper, model) {
            DataObject.call(this, "FUNCTIONLOCCHARACS", "FuncLocCharacteristic", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "ACTION_FLAG", "CHARACT", "CHARACT_DESCR", "CLASS", "CURRENCY_FROM", "CURRENCY_TO", "TPLNR", "TYPE", "UNIT_FROM",
                "UNIT_TO", "VALUE_DATE", "VALUE_FROM", "VALUE_NEUTRAL_FROM", "VALUE_NEUTRAL_TO", "VALUE_TO"];
            this.setDefaultData();
        }

        FuncLocCharacteristic.prototype = Object.create(DataObject.prototype, {});

        FuncLocCharacteristic.prototype.constructor = FuncLocCharacteristic;

        return FuncLocCharacteristic;
    }
);