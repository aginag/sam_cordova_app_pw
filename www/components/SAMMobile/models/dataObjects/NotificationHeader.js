sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/NotificationUserStatus",
        "SAMMobile/models/dataObjects/NotificationLongtext",
        "SAMMobile/models/dataObjects/NotificationTask",
        "SAMMobile/models/dataObjects/NotificationActivity",
        "SAMMobile/models/dataObjects/SAMObjectAttachment",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/helpers/formatter"
    ],

    function (JSONModel, DataObject, NotificationUserStatus, NotificationLongtext, NotificationTask, NotificationActivity, SAMObjectAttachment, DMSAttachment, formatter) {
        "use strict";

        // constructor
        function NotificationHeader(data, requestHelper, model) {
            DataObject.call(this, "NotificationHeader", "NotificationHeader", requestHelper, model);

            this.data = data;

            this.NotificationUsrStat = [];
            this.NotificationLongtext = null;
            this.NotificationTasks = [];
            this.NotificationActivities = [];
            this.NotificationCauses = [];
            this.NotificationAttachments = null;

            this.columns = ["MOBILE_ID", "NOTIF_NO", "NOTIF_NO_DISPLAY", "SHORT_TEXT", "NOTIF_DATE", "NOTIF_TYPE", "EQUIPMENT", "EQUIPMENT_DESC", "SHORT_TEXT", "PRIORITY", "PRIOTYPE", "MATERIAL", "XA_STAT_PROF",
                "SERIALNO", "FUNCT_LOC", "FUNCLOC_DESC", "OBJECT_NO", "NOTIF_DATE", "COMPDATE", "COMPTIME", "NOTIFTIME", "MN_WK_CTR", "PLANPLANT", "PLANT", "CATPROFILE", "CODE_GROUP", "CODING", "ORDERID", "BREAKDOWN", "MALFUNCODE", "ACTION_FLAG"];
            this.setDefaultData();
        }

        NotificationHeader.prototype = Object.create(DataObject.prototype, {
            getNotifNo: {
                value: function() {
                    return this.NOTIF_NO;
                }
            },

            setUserStatus: {
                value: function (statusArray) {
                    this.NotificationUsrStat = statusArray;
                }
            },

            getUserStatus: {
                value: function () {
                    return this.NotificationUsrStat;
                }
            },

            setActivities: {
                value: function (statusArray) {
                    this.NotificationActivities = statusArray;
                }
            },

            getActivities: {
                value: function () {
                    return this.NotificationActivities;
                }
            },

            setCauses: {
                value: function (statusArray) {
                    this.NotificationCauses = statusArray;
                }
            },

            getCauses: {
                value: function () {
                    return this.NotificationCauses;
                }
            },


            setAttachments: {
                value: function (statusArray) {
                    this.NotificationAttachments = statusArray;
                }
            },

            getAttachments: {
                value: function () {
                    return this.NotificationAttachments;
                }
            },
            
            setTasks: {
                value: function (teamGuid, tasksArray) {
                    tasksArray.forEach(function(task){
                	task.TEAM_GUID = teamGuid;
                    });
                    this.NotificationTasks = tasksArray;
                }
            },

            getTasks: {
                value: function () {
                    return this.NotificationTasks;
                }
            },

            setLongText: {
                value: function (longTexts) {
                    if (!this.NotificationLongtext) {
                        this.NotificationLongtext = [];
                    }

                    if(longTexts){
                    this.NotificationLongtext = this.NotificationLongtext.concat(longTexts);
                }
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined || objType == null) {
                        return this.NotificationLongtext;
                    }

                    var filteredByObjectType = this.NotificationLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    return NotificationLongtext.formatToText(filteredByObjectType);
                }
            }
        });

        NotificationHeader.prototype.addUserStatus = function (statusMl, bStrings, successCb, errorCb) {

            var that = this;
            var sqlStrings = [];
            var userStatus = null;

            // 1. Find the current active UserStatus and set it to inactive
            // 2. Check if there is and inactive UserStatus for STATUSML
            //  -> Yes: Set it to active
            //  -> No: Create and insert new UserStatus for STATUSML

            var activeUserStatus = this.getUserStatus().filter(function (userStatus) {
                return userStatus.STATUS == statusMl.USER_STATUS;
            });

            if (activeUserStatus.length > 0) {
                userStatus = activeUserStatus[0];
                userStatus.setActive(false);
                sqlStrings.push(userStatus.update(true));
            }

            var inactiveUserStatusForStatusMl = this.getUserStatus().filter(function (userStatus) {
                return userStatus.INACT == "X" && userStatus.STATUS == statusMl.STATUS_PROFILE;
            });

            if (inactiveUserStatusForStatusMl.length > 0) {
                userStatus = inactiveUserStatusForStatusMl[0];
                userStatus.setActive(true);
                sqlStrings.push(userStatus.update(true));
            } else {
//                userStatus = new NotificationUserStatus(null, this.getRequestHelper());
//
//                if (this.ACTION_FLAG != "N") {
//                    userStatus.setParentTable(this.tableName, this.MOBILE_ID);
//                }
//
//                userStatus.initFromMl(statusMl);
//                userStatus.NOTIF_NO = this.NOTIF_NO;
//                userStatus.OBJNR = this.OBJECT_NO;
//                userStatus.CHANGED = "X";

//                sqlStrings.push(userStatus.insert(true));
        	sqlStrings.push(this.insertNewUserStatus(statusMl));
            }

            if (bStrings) {
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    successCb(e);
                }, errorCb);
            }
        };
        NotificationHeader.prototype.insertNewUserStatus = function(statusMl){
            var userStatus = new NotificationUserStatus(null, this.getRequestHelper());

            userStatus.initFromMl(statusMl);
            userStatus.NOTIF_NO = this.NOTIF_NO;
            userStatus.OBJNR = this.OBJECT_NO;
            userStatus.CHANGED = "X";
            return userStatus.insert(true);
        };
        NotificationHeader.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            // Remark: This function currently adds longtexts only on top of existing longtexgts
            var maxLineNumberForObjType = 0;

            if (this.getLongText()) {
                var longTextsByObjType = this.getLongText().filter(function(longText) {
                    return longText.OBJTYPE == objType;
                });

                maxLineNumberForObjType = longTextsByObjType.map(function(longText) {
                    return parseInt(longText.LINE_NUMBER);
                }).sort(function(a,b) {
                    return a - b;
                }).pop() || 0;
            }


            var notificationLongtext = new NotificationLongtext(null, this.getRequestHelper());

            var newLongTexts = notificationLongtext.createFromString(text, objType, "00000000", maxLineNumberForObjType, this);
            newLongTexts.forEach(function(lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function(e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        NotificationHeader.prototype.addAttachment = function(attachmentObj){
            if (!this.NotificationAttachments) {
                this.NotificationAttachments = [];
            }

            this.NotificationAttachments.push(attachmentObj);
        };

        NotificationHeader.prototype.getNewAttachmentObject = function(fileName, fileExt, fileSize, folderPath) {
            
            var attachmentScenario = this.model._component.getAttachmentScenario();
            var attachment = null;
            var that = this;

            if(attachmentScenario === "DMS"){
                attachment = new DMSAttachment(null, this.getRequestHelper(), this.model);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(this.model._component.i18n_core.getText("NOTIFICATION_TABNAME"));
                attachment.setObjectKey(this.NOTIF_NO);
                attachment.setAttachmentHeader(this.model._component.i18n_core.getText("NOTIFICATION_TABNAME") + ' - ' + this.SHORT_TEXT);
                attachment.setUsername(this.model._globalViewModel.model.getProperty('/userName').toUpperCase());
                attachment.setDocumentPart("000");
                attachment.setDocumentType("DME");
                attachment.setDocumentVersion("00");
                attachment.setDescription(fileName);
            } else {
                attachment = new SAMObjectAttachment(null, this.getRequestHelper(), this.model);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName("NotificationHeader");
                attachment.setObjectKey(this.NOTIF_NO);
                attachment.setAttachmentHeader(this.model._component.i18n_core.getText("NOTIFICATION_TABNAME") + ' - ' + this.SHORT_TEXT);
            }

            return attachment;
        };

        NotificationHeader.prototype.loadLongText = function(objType) {
            var that = this;


            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                	that.NotificationLongtext = [];
                    that.setLongText(that.mapDataToDataObject(NotificationLongtext, data.NotificationLongtext));
                    resolve(that.getLongText(objType));
                };

                if (!that.getLongText()) {
                    that._fetchLongText().then(load_success).catch(reject);
                } else {
                    resolve(that.getLongText(objType));
                }
            });

        };

        NotificationHeader.prototype.loadAttachments = function(bReload) {
            var that = this;

            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setAttachments(that.mapDataToDataObject(DMSAttachment, data.NotificationAttachments));
                    resolve(that.getAttachments());
                };

                if (bReload || !that.getAttachments()) {
                    that._fetchAttachments().then(load_success).catch(reject);
                } else {
                    resolve(that.getAttachments());
                }
            });

        };

        NotificationHeader.prototype.loadActivities = function() {
            var that = this;


            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setActivities(that.mapDataToDataObject(NotificationActivity, data.NotificationActivity));
                    resolve(that.getActivities());
                };

                if (!that.getLongText()) {
                    that._fetchActivities().then(load_success).catch(reject);
                } else {
                    resolve(that.getActivities());
                }
            });

        };

        NotificationHeader.prototype._fetchLongText = function() {
            return this.requestHelper.fetch({
                id: "NotificationLongtext",
                statement: "SELECT * FROM NOTIFICATIONLONGTEXT WHERE NOTIF_NO = '@notifNo' ORDER BY OBJTYPE, OBJKEY, LINE_NUMBER ASC"
            }, {
                 notifNo: this.getNotifNo()
            });
        };

        NotificationHeader.prototype._fetchActivities = function() {
            return this.requestHelper.fetch({
                id: "NotificationActivity",
                statement: "SELECT * FROM NotificationActivity WHERE NOTIF_NO = '@notifNo'"
            }, {
                notifNo: this.getNotifNo()
            });
        };

        NotificationHeader.prototype._fetchCauses = function() {
            return this.requestHelper.fetch({
                id: "NotificationCause",
                statement: "SELECT * FROM NotificationCause WHERE NOTIF_NO = '@notifNo'"
            }, {
                notifNo: this.getNotifNo()
            });
        };

        NotificationHeader.prototype._fetchAttachments = function() {
            return this.requestHelper.fetch({
                id: "NotificationAttachments",
                statement: "SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM SAMObjectAttachments WHERE OBJKEY= '@notifNo' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' " +
                           "UNION ALL SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM DMSATTACHMENTS WHERE OBJKEY= '@notifNo' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' "
            }, {
                notifNo: this.getNotifNo()
            });
        };

        NotificationHeader.prototype._fetchUserStatus = function () {
            return this.requestHelper.fetch({
                id: "NotificationUsrStat",
                statement: "SELECT * FROM NotificationUsrStat WHERE NOTIF_NO = '@notifNo'"
            }, {
                notifNo: this.getNotifNo()
            });
        };

        NotificationHeader.prototype.initNewObject = function () {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "NOTIFTIME") {
                    value = dateNow.time;
                } else if (column == "NOTIF_DATE") {
                    value = dateNow.date;
                }

                that[column] = value;
            });

            this.NOTIF_NO = this.getRandomUUID(12);
            this.OBJECT_NO = "QM" + this.NOTIF_NO;
        };

        NotificationHeader.prototype.constructor = NotificationHeader;

        return NotificationHeader;
    }
);
