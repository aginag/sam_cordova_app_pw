sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject", "SAMMobile/helpers/formatter"],

    function (JSONModel, DataObject, formatter) {
        "use strict";

        // constructor
        function TechnicalObject(tableName, columnsIdentifier, requestHelper, model) {
            DataObject.call(this, tableName, columnsIdentifier, requestHelper, model);

            this._ICON = "";
            this._ID = "";

            this.OrderHistory = null;
            this.NotificationHistory = null;
            this.Characteristics = null;
            this.MeasuringPoints = null;
            this.Partners = null;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
        }

        TechnicalObject.prototype = Object.create(DataObject.prototype, {
            isEquipment: {
                value: function () {
                    if (!this.hasOwnProperty("EQUNR")) {
                        return false;
                    }

                    return this.EQUNR.length > 0;
                }
            },

            getId: {
                value: function () {
                    return this.isEquipment() ? this.EQUNR : this.TPLNR;
                }
            },

            getAddress: {
                value: function() {
                    return formatter.formatAddressToText.apply(this, [this.STREET, this.CITY1, this.POST_CODE1, this.COUNTRY]);
                }
            },

            setOrderHistory: {
                value: function (orderHistory) {
                    this.OrderHistory = orderHistory;
                }
            },

            setAttachments: {
                value: function (attachments) {
                    this.Attachments = attachments;
                }
            },

            getAttachments: {
                value: function () {
                    return this.Attachments;
                }
            },

            getOrderHistory: {
                value: function () {
                    return this.OrderHistory;
                }
            },

            setNotificationHistory: {
                value: function (notificationHistory) {
                    this.NotificationHistory = notificationHistory;
                }
            },

            getNotificationHistory: {
                value: function () {
                    return this.NotificationHistory;
                }
            },

            setCharacteristics: {
                value: function (characteristics) {
                    this.Characteristics = characteristics;
                }
            },

            getCharacteristics: {
                value: function () {
                    return this.Characteristics;
                }
            },

            setMeasuringPoints: {
                value: function (measuringPoints) {
                    this.MeasuringPoints = measuringPoints;
                }
            },

            getMeasuringPoints: {
                value: function () {
                    return this.MeasuringPoints;
                }
            },

            setPartners: {
                value: function (partners) {
                    this.Partners = partners;
                }
            },

            getPartners: {
                value: function () {
                    return this.Partners;
                }
            },

            getCounterList: {
                value: function () {
                    return this.CounterList;
                }
            },

            setCounterList: {
                value: function (list) {
                    this.CounterList = list;
                }
            },
        });

        TechnicalObject.prototype.TYPE = {
            EQUIPMENT: 0,
            FUNC_LOC: 1
        };

        TechnicalObject.prototype.TYPE_ICON = {
            EQUIPMENT: "sap-icon://wrench",
            FUNC_LOC: "sap-icon://functional-location"
        };

        TechnicalObject.prototype.onAfterExistingDataSet = function() {
            this._ICON = this.isEquipment() ? this.TYPE_ICON.EQUIPMENT : this.TYPE_ICON.FUNC_LOC;
            this._ID = this.isEquipment() ? this.EQUIPMENT_DISPLAY : this.FUNCLOC_DISP;
            this._TYPE = this.isEquipment() ? this.TYPE.EQUIPMENT : this.TYPE.FUNC_LOC;
        };

        TechnicalObject.prototype.loadOrderHistory = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setOrderHistory(data);

                successCb(that.getOrderHistory());
            };

            if (!this.getOrderHistory()) {
                if (this.isEquipment()) {
                    this._fetchEquipmentOrderHistory().then(function(data) {
                        load_success(data.EquipmentOrderHistory);
                    }).catch(errorCb);
                } else {
                    this._fetchFunctionLocOrderHistory().then(function(data) {
                        load_success(data.FunctionLocOrderHistory);
                    }).catch(errorCb);
                }
            } else {
                successCb(this.getOrderHistory());
            }
        };

        TechnicalObject.prototype.loadNotificationHistory = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {

                var notificationsP1 = data[0].FunctionLocNotificationHistoryP1 || data[0].EquipmentNotificationHistoryP1 || data[0].FunctionLocNotificationHistory || data[0].EquipmentNotificationHistory,
                    notificationsZ1 = data[1].FunctionLocNotificationHistoryZ1 || data[1].EquipmentNotificationHistoryZ1 || data[1].FunctionLocNotificationHistory || data[1].EquipmentNotificationHistory;

                // Because of a limitation (ORDER BY can only be used for integer values with UNION)
                // the result must be sorted based on NOTIF_DATE and NOTIFTIME
                // before we can take the top 1 and top 3
                var topNotificationsP1 = notificationsP1 && notificationsP1.sort(function (a, b) {
                    var aDateTime = moment(a.NOTIF_DATE.split(" ").shift() + " " + a.NOTIFTIME.split(" ").pop());
                    var bDateTime = moment(b.NOTIF_DATE.split(" ").shift() + " " + b.NOTIFTIME.split(" ").pop());

                    return bDateTime - aDateTime;
                }).slice(0, 3);

                var topNotificationsZ1 = notificationsZ1 && notificationsZ1.sort(function (a, b) {
                    var aDateTime = moment(a.NOTIF_DATE.split(" ").shift() + " " + a.NOTIFTIME.split(" ").pop());
                    var bDateTime = moment(b.NOTIF_DATE.split(" ").shift() + " " + b.NOTIFTIME.split(" ").pop());

                    return bDateTime - aDateTime;
                }).slice(0, 1);


                that.setNotificationHistory(topNotificationsP1.concat(topNotificationsZ1));

                successCb(that.getNotificationHistory());
            };

            if (!this.getNotificationHistory()) {

                var fetchNotificationHistoryQueue = [];

                if (this.isEquipment()) {
                    fetchNotificationHistoryQueue = [
                        this._fetchEquipmentNotificationHistory("P1"),
                        this._fetchEquipmentNotificationHistory("Z1")
                    ];
                } else {
                    fetchNotificationHistoryQueue = [
                        this._fetchFunctionLocNotificationHistory("P1"),
                        this._fetchFunctionLocNotificationHistory("Z1")
                    ];
                }

                Promise.all(fetchNotificationHistoryQueue).then(load_success).catch(errorCb);

            } else {
                successCb(this.getNotificationHistory());
            }
        };

        TechnicalObject.prototype.loadCharacteristics = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setCharacteristics(data);

                successCb(that.getCharacteristics());
            };

            if (!this.getCharacteristics()) {
                if (this.isEquipment()) {
                    this._fetchEquipmentCharacteristics().then(function(data) {
                        load_success(data.EquipmentCharacteristics);
                    }).catch(errorCb);
                } else {
                    this._fetchFunctionLocCharacteristics().then(function(data) {
                        load_success(data.FunctionLocCharacteristics);
                    }).catch(errorCb);
                }
            } else {
                successCb(this.getCharacteristics());
            }
        };

        TechnicalObject.prototype.loadMeasuringPoints = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setMeasuringPoints(data);

                successCb(that.getMeasuringPoints());
            };

            if (!this.getMeasuringPoints()) {
                if (this.isEquipment()) {
                    this._fetchEquipmentMeasuringPoints().then(function(data) {
                        load_success(data.EquipmentMeasuringPoints);
                    }).catch(errorCb);
                } else {
                    this._fetchFunctionLocMeasuringPoints().then(function(data) {
                        load_success(data.FunctionLocMeasuringPoints);
                    }).catch(errorCb);
                }
            } else {
                successCb(this.getMeasuringPoints());
            }
        };

        TechnicalObject.prototype.loadPartners = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setPartners(data);

                successCb(that.getPartners());
            };

            if (!this.getPartners()) {
                if (this.isEquipment()) {
                    this._fetchEquipmentPartner().then(function(data) {
                        load_success(data.EquipmentPartner);
                    }).catch(errorCb);
                } else {
                    this._fetchFunctionLocPartner().then(function(data) {
                        load_success(data.FunctionLocPartner);
                    }).catch(errorCb);
                }
            } else {
                successCb(this.getPartners());
            }
        };

        TechnicalObject.prototype.loadCounters = function(successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setCounterList(data);
                successCb(that.getCounterList());
            };

            var fetchCounterQueue = [];

            if (!this.getOrderHistory()) {
                if (this.isEquipment()) {
                    fetchCounterQueue = [
                        this._fetchCounterEquipmentNotifHistory("P1"),
                        this._fetchCounterEquipmentNotifHistory("Z1"),
                        this._fetchCounterEquipmentCharacteristics(),
                        this._fetchCounterEquipmentOrderHistory(),
                        this._fetchCounterEquipmentMeasuringPoints(),
                        this._fetchCounterEquipmentPartner(),
                        this._fetchCounterEquipmentInstalled()

                    ];
                } else {
                    fetchCounterQueue = [
                        this._fetchCounterFuncLocNotifHistory("P1"),
                        this._fetchCounterFuncLocNotifHistory("Z1"),
                        this._fetchCounterFunctionLocCharacteristics(),
                        this._fetchCounterFunctionLocOrderHistory(),
                        this._fetchCounterFunctionLocMeasuringPoints(),
                        this._fetchCounterFunctionLocPartner(),
                        this._fetchCounterFunctionLocInstalled()

                    ];
                }
            } else {
                successCb(this.getOrderHistory());
            }

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);

        };

        TechnicalObject.prototype._fetchCounterEquipmentNotifHistory = function(sNotificationType) {
            return this.requestHelper.fetch({
                id: sNotificationType ? "CounterNotifHistory" + sNotificationType : "CounterNotifHistory",
                statement: "SELECT COUNT(n.NOTIF_NO) as COUNT " +
                    "FROM NOTIFICATIONHEADER as n " +
                    "LEFT JOIN SAM_NOTIF_HIST as nhist on n.NOTIF_NO = nhist.NOTIF_NO " +
                    "WHERE n.EQUIPMENT = '@equnr' AND n.NOTIF_TYPE = '@notifType' "
            }, {
                equnr: this.EQUNR,
                notifType: sNotificationType
            });
        };

        TechnicalObject.prototype._fetchCounterFuncLocNotifHistory = function(sNotificationType) {
            return this.requestHelper.fetch({
                id: sNotificationType ? "CounterNotifHistory" + sNotificationType : "CounterNotifHistory",
                statement: "SELECT COUNT(n.NOTIF_NO) as COUNT " +
                    "FROM NOTIFICATIONHEADER as n " +
                    "LEFT JOIN SAM_NOTIF_HIST as nhist on n.NOTIF_NO = nhist.NOTIF_NO " +
                    "WHERE n.FUNCT_LOC = '@tplnr' AND n.NOTIF_TYPE = '@notifType' "
            }, {
                tplnr: this.TPLNR,
                notifType: sNotificationType
            });
        };

        TechnicalObject.prototype._fetchCounterEquipmentCharacteristics = function() {
            return this.requestHelper.fetch({
                id: "CounterCharacteristics",
                statement: "SELECT COUNT(ec.MOBILE_ID) as COUNT " +
                    "FROM EquipmentCharacs AS ec " +
                    "WHERE ec.EQUNR='@equnr'"
            }, {
                equnr: this.EQUNR
            });
        };

        TechnicalObject.prototype._fetchCounterFunctionLocCharacteristics = function() {
            return this.requestHelper.fetch({
                id: "CounterCharacteristics",
                statement: "SELECT COUNT(fc.MOBILE_ID) as COUNT " +
                    "FROM FunctionLocCharacs AS fc " +
                    "WHERE fc.TPLNR='@tplnr'"
            }, {
                tplnr: this.TPLNR
            });
        };

        TechnicalObject.prototype._fetchCounterEquipmentOrderHistory = function() {
            return this.requestHelper.fetch({
                id: "CounterOrderHistory",
                statement: "SELECT COUNT(o.ORDERID) as COUNT " +
                    "FROM ORDERS as o " +
                    "LEFT JOIN SAM_ORDER_HIST as oh on o.ORDERID = oh.ORDERID " +
                    "WHERE o.EQUIPMENT = '@equnr' "
            }, {
                equnr: this.EQUNR
            });
        };

        TechnicalObject.prototype._fetchCounterFunctionLocOrderHistory = function() {
            return this.requestHelper.fetch({
                id: "CounterOrderHistory",
                statement: "SELECT COUNT(o.ORDERID) as COUNT " +
                    "FROM ORDERS as o " +
                    "LEFT JOIN SAM_ORDER_HIST as oh on o.ORDERID = oh.ORDERID " +
                    "WHERE o.FUNCT_LOC = '@tplnr'"
            }, {
                tplnr: this.TPLNR
            });
        };

        TechnicalObject.prototype._fetchCounterEquipmentMeasuringPoints = function() {
            return this.requestHelper.fetch({
                id: "CounterMeasuringPoints",
                statement: "SELECT COUNT(emp.MOBILE_ID) as COUNT " +
                    "FROM EquipmentMeasPoint AS emp " +
                    "LEFT JOIN MeasurementUnit AS mu ON emp.MRNGU=mu.MSEHI AND mu.SPRAS='@spras' " +
                    "WHERE emp.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchCounterFunctionLocMeasuringPoints = function() {
            return this.requestHelper.fetch({
                id: "CounterMeasuringPoints",
                statement: "SELECT COUNT(fmp.MOBILE_ID) as COUNT " +
                    "FROM FunctionLocMeasPoint AS fmp " +
                    "LEFT JOIN MeasurementUnit AS mu ON fmp.MRNGU=mu.MSEHI AND mu.SPRAS='@spras' " +
                    "WHERE OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchCounterEquipmentPartner = function() {
            return this.requestHelper.fetch({
                id: "CounterPartner",
                statement: "SELECT COUNT(op.MOBILE_ID) as COUNT " +
                    "FROM EquipmentPartner AS op " +
                    "LEFT JOIN PartnerHeader AS ph ON ph.PARTNER_KEY = op.PARTNER_KEY AND ph.PARTNER_ROLE = op.PARTNER_ROLE " +
                    "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=op.PARTNER_ROLE AND pd.SPRAS = '@spras'  WHERE op.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchCounterFunctionLocPartner = function() {
            return this.requestHelper.fetch({
                id: "CounterPartner",
                statement: "SELECT COUNT(fp.MOBILE_ID) as COUNT " +
                    "FROM FunctionLocPartner AS fp " +
                    "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=fp.PARTNER_ROLE AND pd.SPRAS = '@spras' " +
                    "WHERE fp.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchCounterEquipmentInstalled = function() {
            return this.requestHelper.fetch({
                id: "CounterInstalled",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM EquipmentInstalled WHERE HEQUI='@objNr' AND ( UPPER(HEQUI_SHTXT) LIKE UPPER('%@equNR%') OR EQUNR LIKE '%@equNR%' )"
            }, {
                objNr: this.OBJNR,
                equNR: this.EQUNR
            });
        };

        TechnicalObject.prototype._fetchCounterFunctionLocInstalled = function() {
            return this.requestHelper.fetch({
                id: "CounterInstalled",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM FunctionLocInstalled WHERE TPLNR='@tplnr'"
            }, {
                objNr: this.OBJNR,
                tplnr: this.TPLNR
            });
        };


        TechnicalObject.prototype._fetchEquipmentOrderHistory = function() {
            return this.requestHelper.fetch({
                id: "EquipmentOrderHistory",
                statement: "SELECT o.MOBILE_ID, o.ORDERID, o.ORDER_TYPE, o.SHORT_TEXT, o.START_DATE, o.ACTION_FLAG " +
                    "FROM SAM_ORDER_HIST as oh " +
                    "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                    "WHERE o.EQUIPMENT = '@equnr' " +
                    "UNION SELECT oh1.MOBILE_ID, oh1.ORDERID, oh1.ORDER_TYPE, oh1.SHORT_TEXT, oh1.START_DATE AS ORDER_START, oh1.ACTION_FLAG FROM ORDERS as oh1 WHERE oh1.EQUIPMENT = '@equnr'"
            }, {
                equnr: this.EQUNR
            });
        };

        TechnicalObject.prototype._fetchFunctionLocOrderHistory = function() {
            return this.requestHelper.fetch({
                id: "FunctionLocOrderHistory",
                statement: "SELECT o.MOBILE_ID, o.ORDERID, o.ORDER_TYPE, o.SHORT_TEXT, o.START_DATE, o.ACTION_FLAG " +
                    "FROM SAM_ORDER_HIST as oh " +
                    "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                    "WHERE o.FUNCT_LOC = '@tplnr' " +
                    "UNION SELECT oh1.MOBILE_ID, oh1.ORDERID, oh1.ORDER_TYPE, oh1.SHORT_TEXT, oh1.START_DATE AS ORDER_START, oh1.ACTION_FLAG FROM ORDERS as oh1 WHERE oh1.FUNCT_LOC = '@tplnr'"
            }, {
                tplnr: this.TPLNR
            });
        };

        TechnicalObject.prototype._fetchEquipmentNotificationHistory = function(sNotificationType) {
            return this.requestHelper.fetch({
                id: sNotificationType ? "EquipmentNotificationHistory" + sNotificationType : "EquipmentNotificationHistory",
                statement: "SELECT n.MOBILE_ID, n.NOTIF_NO, n.NOTIF_TYPE, n.SHORT_TEXT, n.NOTIF_DATE, n.NOTIFTIME, n.ACTION_FLAG " +
                    "FROM SAM_NOTIF_HIST as nhist " +
                    "JOIN NOTIFICATIONHEADER as n on n.NOTIF_NO = nhist.NOTIF_NO " +
                    "WHERE n.EQUIPMENT = '@equnr' AND n.NOTIF_TYPE = '@notifType' " +
                    "UNION SELECT n1.MOBILE_ID, n1.NOTIF_NO, n1.NOTIF_TYPE, n1.SHORT_TEXT, n1.NOTIF_DATE, n1.NOTIFTIME, n1.ACTION_FLAG FROM NOTIFICATIONHEADER as n1 WHERE n1.EQUIPMENT = '@equnr' AND n1.NOTIF_TYPE = '@notifType' "
            }, {
                equnr: this.EQUNR,
                notifType: sNotificationType
            });
        };

        TechnicalObject.prototype._fetchFunctionLocNotificationHistory = function(sNotificationType) {
            return this.requestHelper.fetch({
                id: sNotificationType ? "FunctionLocNotificationHistory" + sNotificationType : "FunctionLocNotificationHistory",
                statement: "SELECT n.MOBILE_ID, n.NOTIF_NO, n.NOTIF_TYPE, n.SHORT_TEXT, n.NOTIF_DATE, n.NOTIFTIME, n.ACTION_FLAG " +
                    "FROM SAM_NOTIF_HIST as nhist " +
                    "JOIN NOTIFICATIONHEADER as n on n.NOTIF_NO = nhist.NOTIF_NO " +
                    "WHERE n.FUNCT_LOC = '@tplnr' AND n.NOTIF_TYPE = '@notifType' " +
                    "UNION SELECT n1.MOBILE_ID, n1.NOTIF_NO, n1.NOTIF_TYPE, n1.SHORT_TEXT, n1.NOTIF_DATE, n1.NOTIFTIME, n1.ACTION_FLAG FROM NOTIFICATIONHEADER as n1 WHERE n1.FUNCT_LOC = '@tplnr' AND n1.NOTIF_TYPE = '@notifType' "
            }, {
                tplnr: this.TPLNR,
                notifType: sNotificationType
            });
        };

        TechnicalObject.prototype._fetchEquipmentCharacteristics = function() {
            return this.requestHelper.fetch({
                id: "EquipmentCharacteristics",
                statement: "SELECT ec.MOBILE_ID, ec.CHARACT, cml.ATBEZ AS CHARACT_DESCR, ec.CLASS, ec.EQUNR, ec.VALUE_NEUTRAL_FROM, ec.VALUE_NEUTRAL_TO, ec.TYPE, ec.ACTION_FLAG " +
                    "FROM EquipmentCharacs AS ec " +
                    "LEFT JOIN Characteristics AS c ON c.ATNAM=ec.CHARACT " +
                    "LEFT JOIN CharacteristicsMl AS cml ON cml.ATINN=c.ATINN AND cml.SPRAS='@spras' " +
                    "WHERE ec.EQUNR='@equnr' AND c.MOBILE_ID IS NOT NULL"
        }, {
                equnr: this.EQUNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchFunctionLocCharacteristics = function() {
            return this.requestHelper.fetch({
                id: "FunctionLocCharacteristics",
                statement: "SELECT fc.MOBILE_ID, fc.CHARACT, cml.ATBEZ AS CHARACT_DESCR, fc.CLASS, fc.TPLNR, fc.VALUE_NEUTRAL_FROM, fc.VALUE_NEUTRAL_TO, fc.TYPE, fc.ACTION_FLAG " +
                    "FROM FunctionLocCharacs AS fc " +
                    "LEFT JOIN Characteristics AS c ON c.ATNAM=fc.CHARACT " +
                    "LEFT JOIN CharacteristicsMl AS cml ON cml.ATINN=c.ATINN AND cml.SPRAS='@spras' " +
                    "WHERE fc.TPLNR='@tplnr' AND c.MOBILE_ID IS NOT NULL"
            }, {
                tplnr: this.TPLNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchEquipmentMeasuringPoints = function() {
            return this.requestHelper.fetch({
                id: "EquipmentMeasuringPoints",
                statement: "SELECT emp.MOBILE_ID, emp.OBJNR, emp.MEASUREMENT_POINT, emp.MEASUREMENT_DESC, emp.ATINN, emp.MPTYP, mu.MSEHT, emp.CODGR, emp.INDCT, emp.MRMIN, emp.MRMAX, emp.PYEAR, emp.CODGR, emp.ACTION_FLAG " +
                    "FROM EquipmentMeasPoint AS emp " +
                    "LEFT JOIN MeasurementUnit AS mu ON emp.MRNGU=mu.MSEHI AND mu.SPRAS='@spras' " +
                    "WHERE emp.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchFunctionLocMeasuringPoints = function() {
            return this.requestHelper.fetch({
                id: "FunctionLocMeasuringPoints",
                statement: "SELECT fmp.MOBILE_ID, fmp.OBJNR, fmp.MEASUREMENT_POINT, fmp.MEASUREMENT_DESC, fmp.ATINN, fmp.MPTYP, mu.MSEHT, fmp.CODGR, fmp.INDCT, fmp.MRMIN, fmp.MRMAX, fmp.PYEAR, fmp.CODGR, fmp.ACTION_FLAG  " +
                    "FROM FunctionLocMeasPoint AS fmp " +
                    "LEFT JOIN MeasurementUnit AS mu ON fmp.MRNGU=mu.MSEHI AND mu.SPRAS='@spras' " +
                    "WHERE OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchEquipmentPartner = function() {
            return this.requestHelper.fetch({
                id: "EquipmentPartner",
                statement: "SELECT op.MOBILE_ID, op.NAME_LIST, op.PARTNER_KEY, op.PARTNER_ROLE, pd.DESCRIPTION AS ROLE_DESCRIPTION, op.ACTION_FLAG, ph.STREET, ph.CITY1, ph.POST_CODE1, ph.COUNTRY, ph.TEL_NUMBER, ph.FAX_NUMBER " +
                    "FROM EquipmentPartner AS op " +
                    "LEFT JOIN PartnerHeader AS ph ON ph.PARTNER_KEY = op.PARTNER_KEY AND ph.PARTNER_ROLE = op.PARTNER_ROLE " +
                    "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=op.PARTNER_ROLE AND pd.SPRAS = '@spras' " +
                    "WHERE op.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype._fetchFunctionLocPartner = function() {
            return this.requestHelper.fetch({
                id: "FunctionLocPartner",
                statement: "SELECT fp.MOBILE_ID, fp.OBJNR, fp.NAME_LIST, fp.PARTNER_KEY, fp.PARTNER_ROLE, pd.DESCRIPTION AS ROLE_DESCRIPTION, fp.ACTION_FLAG, ph.STREET, ph.CITY1, ph.POST_CODE1, ph.COUNTRY, ph.TEL_NUMBER, ph.FAX_NUMBER " +
                    "FROM FunctionLocPartner AS fp " +
                    "LEFT JOIN PartnerHeader AS ph ON ph.PARTNER_KEY = fp.PARTNER_KEY AND ph.PARTNER_ROLE = fp.PARTNER_ROLE " +
                    "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=fp.PARTNER_ROLE AND pd.SPRAS = '@spras' " +
                    "WHERE fp.OBJNR='@objNr'"
            }, {
                objNr: this.OBJNR,
                spras: this.model.getLanguage()
            });
        };

        TechnicalObject.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = that.getRandomUUID(12);
                }

                that[column] = value;
            });
        };

        TechnicalObject.prototype.constructor = TechnicalObject;

        return TechnicalObject;
    }
);