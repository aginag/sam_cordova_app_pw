sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function (JSONModel, DataObject) {
        "use strict";

        // constructor
        function NotificationActivity(data, requestHelper, model) {
            DataObject.call(this, "NotificationActivity", "NotificationActivity", requestHelper, model);

            this.data = data;
            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.columns = [ "MOBILE_ID", "NOTIF_NO", "ITEM_KEY", "ITEM_SORT_NO", "ACT_KEY", "ACT_SORT_NO", "ACTTEXT", "ACT_CAT_TYP", "LONG_TEXT", "ACT_CODEGRP", "ACT_CODE", "START_DATE", "START_TIME", "END_DATE", "END_TIME", "ACTION_FLAG", "ADD_FLAG", "TXT_ACTCD", "TXT_ACTGRP" ];

            this.setDefaultData();
        }

        NotificationActivity.prototype = Object.create(DataObject.prototype, {
            setNotifNo: {
                value: function (notifNo) {
                    this.NOTIF_NO = notifNo;
                }
            },

            setStartEndTime: {
                value: function (newStartTime) {
                    var newStartDateMoment = moment(newStartTime);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(newStartTime);

                    this._startTime = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this._endTime = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

                    this.START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.END_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

                    this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    //this.updateEndTime();
                }
            },

            setStartTime: {
                value: function (value) {
                    var newStartDateMoment = moment(value);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(value);

                    this.START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                }
            },

            setEndTime: {
                value: function (value) {
                    var newStartDateMoment = moment(value);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(value);

                    this.END_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                }
            },

        });

        NotificationActivity.prototype.onAfterExistingDataSet = function () {
            // Set Helper properties
            this._startTime = this.START_DATE.split(" ")[0] + " " + this.START_TIME.split(" ")[1];
            this._endTime = this.END_DATE.split(" ")[0] + " " + this.END_TIME.split(" ")[1];
        };

        NotificationActivity.prototype.initNewObject = function () {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });

            this.ITEM_KEY = '0001';
            this.ITEM_SORT_NO = '0001';
            this.ACT_CAT_TYP = 'A';
            this.ADD_FLAG = 'X';
            this.ACT_KEY = this.getRandomCharUUID(4);
            // Helper properties
            this.setStartEndTime(dateNow.dateTime);
        };

        NotificationActivity.prototype.constructor = NotificationActivity;

        return NotificationActivity;
    }
);
