sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/TechnicalObject",
        "SAMMobile/models/dataObjects/DMSAttachment"],

    function (JSONModel, DataObject, TechnicalObject, DMSAttachment) {
        "use strict";

        // constructor
        //function MeasurementPoint(tableName, requestHelper, model) {
        function MeasurementPoint(data, requestHelper, model) {
            //DataObject.call(this, tableName, columnsIdentifier, requestHelper, model);
            TechnicalObject.call(this, "MeasurementPoint", "MeasurementPoint", requestHelper, model);

            this.data = data;

            this.Attachments = null;

            this.columns = ["MOBILE_ID", "OBJNR", "MRMAX", "MRMIN", "MEASUREMENT_DESC", "MEASUREMENT_POINT", "CODGR", "ACTION_FLAG"];
        }

        MeasurementPoint.prototype = Object.create(TechnicalObject.prototype, {
            setAttachments: {
                value: function (attachments) {
                    this.Attachments = attachments;
                }
            },

            getAttachments: {
                value: function () {
                    return this.Attachments;
                }
            },
        });

        MeasurementPoint.prototype.getNewAttachmentObject = function (fileName, fileExt, fileSize, folderPath) {

            var attachmentScenario = this.model._component.getAttachmentScenario();
            var attachment = null;

            if(attachmentScenario === "DMS"){
                attachment = new DMSAttachment(null, this.getRequestHelper(), this.model);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(this.model._component.i18n_core.getText("MEASUREMENTDOCUMENTS_TABNAME"));
                attachment.setObjectKey(this.POINT);
                attachment.setAttachmentHeader(this.model._component.i18n_core.getText("MEASUREMENTDOCUMENTS_TABNAME"));
                attachment.setUsername(this.model._globalViewModel.model.getProperty('/userName').toUpperCase());
                attachment.setDocumentPart("000");
                // TODO check if DOCUMENT TYPE is correct
                attachment.setDocumentType("DMP");
                attachment.setDocumentVersion("00");
                attachment.DISPLAY_TABNAME=this.model._component.i18n_core.getText("MEASUREMENTDOCUMENTS_TABNAME");
                attachment.setDescription(fileName);
            }

            return attachment;
        };

        MeasurementPoint.prototype.loadAttachments = function(bReload) {
            var that = this;

            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setAttachments(that.mapDataToDataObject(DMSAttachment, data.MeasurementsPointAttachments));
                    resolve(that.getAttachments());
                };

                if (bReload || !that.getAttachments()) {
                    that._fetchAttachments().then(load_success).catch(reject);
                } else {
                    resolve(that.getAttachments());
                }
            });

        };

        MeasurementPoint.prototype._fetchAttachments = function() {
            return this.requestHelper.fetch({
                id: "MeasurementsPointAttachments",
                statement:  "SELECT sm.MOBILE_ID, sm.FILEEXT, sm.FILENAME, sm.TABNAME, sm.OBJKEY, sm.DOC_SIZE, sm.ACTION_FLAG, em.MEASUREMENT_DESC as EM_MEASUREMENT_DESC, fm.MEASUREMENT_DESC as FM_MEASUREMENT_DESC "+
                            "FROM SAMObjectAttachments as sm "+
                            "LEFT JOIN EquipmentMeasPoint as em on em.XA_MEASUREMENT_POINT = sm.OBJKEY "+
                            "LEFT JOIN FunctionLocMeasPoint as fm on fm.XA_MEASUREMENT_POINT = sm.OBJKEY "+
                            "WHERE sm.OBJKEY= '@point' AND sm.ACTION_FLAG != 'A' AND sm.ACTION_FLAG != 'B' AND sm.ACTION_FLAG != 'O' " +
                            "UNION ALL SELECT dm.MOBILE_ID, dm.FILEEXT, dm.FILENAME, dm.TABNAME, dm.OBJKEY, dm.DOC_SIZE, dm.ACTION_FLAG, em.MEASUREMENT_DESC as EM_MEASUREMENT_DESC, fm.MEASUREMENT_DESC as FM_MEASUREMENT_DESC "+
                            "FROM DMSATTACHMENTS as dm "+
                            "LEFT JOIN EquipmentMeasPoint as em on em.XA_MEASUREMENT_POINT = dm.OBJKEY "+
                            "LEFT JOIN FunctionLocMeasPoint as fm on fm.XA_MEASUREMENT_POINT = dm.OBJKEY "+
                            "WHERE dm.OBJKEY= '@point' AND dm.ACTION_FLAG != 'A' AND dm.ACTION_FLAG != 'B' AND dm.ACTION_FLAG != 'O' "
            }, {
                point: this.POINT
            });
        };


        MeasurementPoint.prototype.addAttachment = function(attachmentObj){
            if (!this.Attachments) {
                this.Attachments = [];
            }
            this.Attachments.push(attachmentObj);
        };


        MeasurementPoint.prototype.constructor = MeasurementPoint;

        return MeasurementPoint;
    }
);