sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/CatsLongtext",
        "SAMMobile/helpers/timeHelper"],

    function (JSONModel, DataObject, CatsLongtext, timeHelper) {
        "use strict";

        // constructor
        function CatsHeader(data, requestHelper, model) {
            DataObject.call(this, "CatsHeader", "CatsHeader", requestHelper, model);

            this.data = data;
            this._DEFAULT_HOURS = 0.5;
            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.CatsLongtext = null;

            this.languageTimeFormat = timeHelper.getDisplayTimeFormatForCurrentLanguage(model.getLanguage());

            this.columns = ["MOBILE_ID", "ACTIVITY", "AWART", "SUB_ACTIVITY", "BEMOT", "CATSHOURS", "COUNTER", "LSTAR", "MEINH", "PERNR", "RAUFNR", "SKOSTL", "WERKS", "WORKDATE", "WORK_CNTR", "PROFILE", "TEXT_FORMAT_IMP", "ACTION_FLAG", "BEGUZ", "ENDUZ", "AUERU"];
            this.setDefaultData();
        }

        CatsHeader.prototype = Object.create(DataObject.prototype, {
            setHours: {
                value: function (newHours) {
                    // toFixed() is rounding up the decimals which could lead to issues when autosync is active
                    // there is at least a theoretically chance of having dates in the future - SAP will complain about this
                    // parseInt is cutting of the digits which however is resulting into a "loss" of working hours
                    this.ACT_WORK = (parseInt('' + (newHours * 100)) / 100).toString();
                    this.CATSHOURS = (parseInt('' + (newHours * 100)) / 100).toString();

                    this.updateEndTime();
                }
            },

            setStartEndTime: {
                value: function (newStartTime) {
                    var newStartDateMoment = moment(newStartTime);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(newStartTime);

                    this._startTime = newStartTime;
                    this._endTime = newStartTime;
                    this.WORKDATE = newStartDateMoment.format("YYYY-MM-DD HH:mm");
                    this.BEGUZ = newStartTimeMoment.format("YYYY-MM-DD HH:mm");
                    this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm");
                    this.ENDUZ = newStartTimeMoment.format("YYYY-MM-DD HH:mm");
                    this.END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm");
                    //this.updateEndTime();
                }
            },

            setLongText: {
                value: function (longTexts) {
                    if (!this.CatsLongtext) {
                        this.CatsLongtext = [];
                    }

                    this.CatsLongtext = this.CatsLongtext.concat(longTexts);
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined) {
                        return this.CatsLongtext;
                    }

                    var filteredByObjectType = this.CatsLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    return CatsLongtext.formatToText(filteredByObjectType);
                }
            }
        });

        CatsHeader.prototype.setStartTime = function(startTime) {
            // Set Helper properties
            this._startTime = startTime;
            this.setNewStartTime(startTime);

        };

        CatsHeader.prototype.setNewStartTime = function(newStartTime) {
            // Set Helper properties

            var newStartDateMoment = moment(newStartTime);
            newStartDateMoment.hours(0);
            newStartDateMoment.minutes(0);
            newStartDateMoment.seconds(0);
            newStartDateMoment.milliseconds(0);

            var newStartTimeMoment = moment(newStartTime);

            this._startTime = newStartTime;

            this.WORKDATE = newStartDateMoment.format("YYYY-MM-DD HH:mm");
            this.BEGUZ = newStartTimeMoment.format("YYYY-MM-DD HH:mm");
            this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm");

            var duration = moment.duration(moment(this.END_TIME).diff(moment(this.START_TIME))).asHours();
            this.CATSHOURS = duration.toFixed(2);
            this.ACT_WORK = duration.toFixed(2);
        };

        CatsHeader.prototype.setFormattedStartTime = function(catsEntry){
            this.FORMATTED_START_TIME = moment(catsEntry).format(this.languageTimeFormat);
        };

        CatsHeader.prototype.setFormattedEndTime = function(catsEntry){
            this.FORMATTED_END_TIME = moment(catsEntry).format(this.languageTimeFormat);
        };

        CatsHeader.prototype.setEndTime = function(newEndTime) {
            // Set Helper properties

            var newEndDateMoment = moment(newEndTime);
            newEndDateMoment.hours(0);
            newEndDateMoment.minutes(0);
            newEndDateMoment.seconds(0);
            newEndDateMoment.milliseconds(0);

            var newEndTimeMoment = moment(newEndTime);

            this._endTime = newEndTime;

            this.ENDUZ = newEndTimeMoment.format("YYYY-MM-DD HH:mm");
            this.END_TIME = newEndTimeMoment.format("YYYY-MM-DD HH:mm");

            var duration = moment.duration(moment(this.END_TIME).diff(moment(this.START_TIME))).asHours();
            this.CATSHOURS = duration.toFixed(2);
            this.ACT_WORK = duration.toFixed(2);
        };


        CatsHeader.prototype.updateEndTime = function () {
            // Add CATSHOURS to _startTime
            var catsHoursFloat = parseFloat(this.CATSHOURS);
            var endDateTime = moment(this._startTime).add(catsHoursFloat, 'hours');

            this.ENDUZ = endDateTime.format("YYYY-MM-DD HH:mm");
            this.END_TIME = endDateTime.format("YYYY-MM-DD HH:mm");
        };

        CatsHeader.prototype.onAfterExistingDataSet = function () {
            if ((this.hasOwnProperty("BEGUZ") && this.hasOwnProperty("ENDUZ")) ||
                (this.hasOwnProperty("START_TIME") && this.hasOwnProperty("END_TIME")) ) {
                // Set Helper properties
                var oStartTime = this.BEGUZ || this.START_TIME;
                var oEndTime = this.ENDUZ ||this.END_TIME;

                this._startTime = oStartTime;
                this._endTime = oEndTime;

                var workDate = this.WORKDATE;

                var fullTimeFormatForLanguage = this.languageTimeFormat;
                var timeArray = fullTimeFormatForLanguage.split(' ');

                this.FORMATTED_START_TIME = moment(workDate).format(timeArray[0]) + " " + moment(oStartTime).format(timeArray[1]);
                this.FORMATTED_END_TIME = moment(workDate).format(timeArray[0]) + " " + moment(oEndTime).format(timeArray[1]);

            }
        };

        CatsHeader.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];


            // Remark: This function currently adds longtexts only on top of existing longtexgts
            var maxLineNumberForObjType = 0;

            if (this.getLongText()) {
                var longTextsByObjType = this.getLongText().filter(function (longText) {
                    return longText.OBJTYPE == objType;
                });

                maxLineNumberForObjType = longTextsByObjType.map(function (longText) {
                    return parseInt(longText.LINE_NUMBER)
                }).sort(function (a, b) {
                    return a - b;
                }).pop() || 0;
            }


            var catsLongtext = new CatsLongtext(null, this.getRequestHelper());

            var newLongTexts = catsLongtext.createFromString(text, "", objType, maxLineNumberForObjType, this);
            newLongTexts.forEach(function (lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        CatsHeader.prototype.updateLongtext = function(bStrings, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            var longTextsByObjType = this.getLongText().filter(function (longText) {
                return longText.OBJTYPE == objType;
            });

            // 1. Delete longText´s == objType
            longTextsByObjType.forEach(function(longText) {
                sqlStrings.push(longText.delete(true));
            });

            var newLongTexts = [];

            if (!/^\s*$/.test(this._longText)) {
                // 2. Create a new one
                var catsLongtext = new CatsLongtext(null, this.getRequestHelper());

                newLongTexts = catsLongtext.createFromString(this._longText, "", objType, 0, this);
                newLongTexts.forEach(function (lt) {
                    sqlStrings.push(lt.insert(true));
                });
            }

            // Flatten array of arrays
            sqlStrings = [].concat.apply([], sqlStrings);

            if (bStrings) {
                this.CatsLongtext = null;
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        CatsHeader.prototype.loadLongText = function (objType, successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setLongText(that.mapDataToDataObject(CatsLongtext, data));
                successCb(that.getLongText(objType));
            };

            if (!this.getLongText()) {
                this._fetchLongText().then(successCb).catch(errorCb);
            } else {
                successCb(this.getLongText(objType));
            }
        };

        CatsHeader.prototype._fetchLongText = function() {
            return this.requestHelper.fetch({
                id: "CatsLongtext",
                statement: "SELECT * FROM CATSLONGTEXT WHERE COUNTER = '@counter'"
            }, {
                counter: this.COUNTER
            });
        };

        CatsHeader.prototype.initNewObject = function () {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "MEINH") {
                    value = "H";
                } else if (column == "PROFILE") {
                    value = that.model._component.rootComponent.getManifestEntry("/sap.app").SAMConfig.timeRecordingConfiguration.catsProfile;
                } else if (column == "COUNTER") {
                    value = that.getRandomCharUUID(12);
                } else if (column == "TEXT_FORMAT_IMP") {
                    value = "ITF";
                }

                that[column] = value;
            });

            // Helper properties
            this.setStartEndTime(dateNow.dateTime);
            this.setHours(this._DEFAULT_HOURS);
            this._longText = "";
        };

        CatsHeader.prototype.constructor = CatsHeader;

        return CatsHeader;
    }
);
