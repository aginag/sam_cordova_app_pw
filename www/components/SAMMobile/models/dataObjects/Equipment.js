sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/TechnicalObject",
        "SAMMobile/models/dataObjects/DMSAttachment"],

    function (JSONModel, DataObject, TechnicalObject, DMSAttachment) {
        "use strict";

        // constructor
        function Equipment(data, requestHelper, model) {
            TechnicalObject.call(this, "Equipment", "Equipment", requestHelper, model);

            this.data = data;

            this.Attachments = null;

            this.columns = ["MOBILE_ID", "EQUNR", "TPLNR", "FUNCLOC_DISP", "EQUIPMENT_DISPLAY", "EQTYP", "EQART", "OBJNR", "MATNR", "WERK", "LAGER", "SHTXT", "TPLNR_SHTXT", "TPLMA", "TPLMA_SHTXT", "TIDNR", "FLTYP", "HERST", "SERNR", "STREET", "SWERK",
                "HEQUI", "HEQUI_SHTXT", "SUBMT", "BEBER", "RBNR", "INGRP", "SPRAS", "POST_CODE1", "CITY1", "COUNTRY", "BAUJJ", "BAUMM", "TYPBZ", "SERGE", "ACTION_FLAG", "WKCTR", "PPSID", "MLANG"];
            this.setDefaultData();
        }

        Equipment.prototype = Object.create(TechnicalObject.prototype, {
            setAttachments: {
                value: function (attachments) {
                    this.Attachments = attachments;
                }
            },

            getAttachments: {
                value: function () {
                    return this.Attachments;
                }
            },

        });

        Equipment.prototype.getNewAttachmentObject = function (fileName, fileExt, fileSize, folderPath) {

            var attachmentScenario = this.model._component.getAttachmentScenario();
            var attachment = null;

            if(attachmentScenario === "DMS"){
                attachment = new DMSAttachment(null, this.getRequestHelper(), this.model);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(this.model._component.i18n_core.getText("EQUIPMENT_TABNAME"));
                attachment.setObjectKey(this.EQUNR);
                attachment.setAttachmentHeader(this.model._component.i18n_core.getText("EQUIPMENT_TABNAME"));
                attachment.setUsername(this.model._globalViewModel.model.getProperty('/userName').toUpperCase());
                attachment.setDocumentPart("000");
                // TODO check if DOCUMENT TYPE is correct
                attachment.setDocumentType("DEQ");
                attachment.setDocumentVersion("00");
                attachment.DISPLAY_TABNAME=this.model._component.i18n_core.getText("EQUIPMENT_TABNAME");
                attachment.setDescription(fileName);
            }

            return attachment;
        };

        Equipment.prototype.loadAttachments = function(bReload) {
            var that = this;

            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setAttachments(that.mapDataToDataObject(DMSAttachment, data.EquipmentAttachments));
                    resolve(that.getAttachments());
                };

                if (bReload || !that.getAttachments()) {
                    that._fetchAttachments().then(load_success).catch(reject);
                } else {
                    resolve(that.getAttachments());
                }
            });

        };

        Equipment.prototype._fetchAttachments = function() {
            return this.requestHelper.fetch({
                id: "EquipmentAttachments",
                statement: "SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM SAMObjectAttachments WHERE OBJKEY= '@equipment' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' " +
                    "UNION ALL SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM DMSATTACHMENTS WHERE OBJKEY= '@equipment' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' "
            }, {
                equipment: this.EQUNR
            });
        };


        Equipment.prototype.addAttachment = function(attachmentObj){
            if (!this.Attachments) {
                this.Attachments = [];
            }
            this.Attachments.push(attachmentObj);
        };


        Equipment.prototype.constructor = Equipment;

        return Equipment;
    }
);