sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],
   
   function(JSONModel, DataObject) {
      "use strict";
      // constructor
      function OrderUserStatus(data, requestHelper, model) {
         DataObject.call(this, "OrderUserStatus", "OrderUserStatus", requestHelper, model);
         
         this.data = data;

         this.columns = ["MOBILE_ID", "USER_STATUS_DESC", "USER_STATUS_CODE", "STATUS", "ORDERID", "OBJNR", "INACT", "CHGNR", "CHANGED", "ACTION_FLAG"];
         this.setDefaultData();
      }
      
      OrderUserStatus.prototype = Object.create(DataObject.prototype, {
          setActive: {
              value: function(bActive) {
                  this.INACT = bActive ? "" : "X";
                  this.CHANGED = "X";
              }
          },

          isActive: {
              value: function() {
                  return this.INACT == "";
              }
          }
      });
   
      OrderUserStatus.prototype.initFromMl = function(statusMl) {
         var that = this;
      
         this.columns.forEach(function(column) {
            if (column !== "ACTION_FLAG" && column !== "SPRAS" && column !== "MOBILE_ID" && column !== "STATUS_PROFILE") {
               that[column] = statusMl.hasOwnProperty(column) ? statusMl[column] : "";
            }
            
            if (column == "STATUS") {
               that[column] = statusMl["USER_STATUS"];
            }
         });
      };
      
      OrderUserStatus.prototype.initNewObject = function() {
         var that = this;
         
         this.columns.forEach(function(column) {
            var value = "";
            
            if (column == "ACTION_FLAG") {
               value = "N";
            } else if (column == "MOBILE_ID") {
               value = "";
            }
            
            that[column] = value;
         });
      };
      
      OrderUserStatus.prototype.constructor = OrderUserStatus;
      
      return OrderUserStatus;
   }
);