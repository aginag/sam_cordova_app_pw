sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function (JSONModel, DataObject) {
        "use strict";

        //constructor
        function Showman(data, requestHelper, model) {
            DataObject.call(this, "ZPMC_SHOWM_BILLING", "Showman", requestHelper, model);
            this.data = data;
            this._LS_KEY = "SAM_PFALZWERKE_SHOWMAN";
            this.showmanProcesses = [];

            this._VALUE_STATE = sap.ui.core.ValueState.None;
            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            if (localStorage.getItem(this._LS_KEY) === null) {
                localStorage.setItem(this._LS_KEY, JSON.stringify(this.showmanProcesses));
            }

            this.installationSelected = false;
            this.removalSelected = false;
            this.disableSelected = false;
            
            this.columns = ["ACTION_FLAG", "CONV_FACTOR", "END_CNT_READ", "INST_DAT", "MOBILE_ID", "OPERATION", "ORDERNO", "REMOVE_DAT", "START_CNT_READ", "CUST_PROPERTY_COUNT"];
            this.setDefaultData();

            this._initShowmanData();
        }

        Showman.prototype = Object.create(DataObject.prototype, {
            setValueState: {
                value: function (valueState) {
                    this._VALUE_STATE = valueState;
                }
            },

            setInstallationRemovalDate: {
                value: function (newStartTime) {
                    var newStartDateMoment = moment(newStartTime);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(newStartTime);

                    this._removeDate = newStartTime;
                    this._instDate = newStartTime;

                    this.REMOVE_DAT = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.INST_DAT = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                }
            }
        });

        Showman.prototype._initShowmanData = function () {
            
            this.showmanProcesses = JSON.parse(localStorage.getItem(this._LS_KEY)).map(function (showman) {
                return showman;
            });
        };

        Showman.prototype._saveLocalStorage = function () {
            localStorage.setItem(this._LS_KEY, JSON.stringify(this.showmanProcesses));
        };

        Showman.prototype.setInstallationDate = function (installationDate) {
            this.INST_DAT = installationDate;
            this._instDate = installationDate;

            this._INST_DATE = moment(this.INST_DAT.split(" ").shift() + " " + this.REMOVE_DAT.split(" ").pop());

            if (this.model.getLanguage() === 'D') {
                this.FORMATTED_REMOVE_DATE = moment(this._INST_DATE).format("DD.MM.YYYY HH:mm");
            } else {
                this.FORMATTED_REMOVE_DATE = moment(this._INST_DATE).format("YYYY/MM/DD HH:mm");
            }
        };

        Showman.prototype.setRemovalDate = function (removalDate) {

            this.REMOVE_DAT = removalDate;
            this._removeDate = removalDate;

            this._REMOVE_DATE = moment(this.REMOVE_DAT.split(" ").shift() + " " + this.REMOVE_DAT.split(" ").pop());


            if (this.model.getLanguage() === 'D') {
                this.FORMATTED_REMOVE_DATE = moment(this._REMOVE_DATE).format("DD.MM.YYYY HH:mm");
            } else {
                this.FORMATTED_REMOVE_DATE = moment(this._REMOVE_DATE).format("YYYY/MM/DD HH:mm");
            }

        };

        Showman.prototype.onAfterExistingDataSet = function () {
            if (this.hasOwnProperty("REMOVE_DAT") && this.hasOwnProperty("REMOVE_DAT") &&
                this.hasOwnProperty("INST_DAT") && this.hasOwnProperty("INST_DAT")) {

                // Set Helper properties
                this._removeDate = this.REMOVE_DAT;
                this._instDate = this.INST_DAT;

                this._REMOVE_DATE = moment(this.REMOVE_DAT.split(" ").shift() + " " + this.REMOVE_DAT.split(" ").pop());
                this._INST_DATE = moment(this.INST_DAT.split(" ").shift() + " " + this.INST_DAT.split(" ").pop());

                if (this.model.getLanguage() === 'D') {
                    this.FORMATTED_REMOVE_DATE = moment(this._REMOVE_DATE).format("DD.MM.YYYY HH:mm");
                    this.FORMATTED_INST_DATE = moment(this._INST_DATE).format("DD.MM.YYYY HH:mm");
                } else {
                    this.FORMATTED_REMOVE_DATE = moment(this._REMOVE_DATE).format("YYYY/MM/DD HH:mm");
                    this.FORMATTED_INST_DATE = moment(this._INST_DATE).format("YYYY/MM/DD HH:mm");
                }
            }
        };

        Showman.prototype.checkProcessFullfiled = function () {

            if(this.INST_DAT == "" && this.REMOVE_DAT == ""){
                return false;
            }

            if(this.INST_DAT && this.REMOVE_DAT ){
                if (this.START_CNT_READ && this.CONV_FACTOR && this.END_CNT_READ) {
                    return true;
                } else {
                    return false;
                }
            }

            if(this.INST_DAT != "" && this.REMOVE_DAT == ""){
                if (this.START_CNT_READ && this.CONV_FACTOR) {
                    return true;
                } else {
                    return false;
                }
            }

            if(this.INST_DAT == "" && this.REMOVE_DAT != ""){
                if (this.END_CNT_READ) {
                    return true;
                } else {
                    return false;
                }
            }

        };

        Showman.prototype.deleteShowmanFromLocalStorage = function (orderId, operation) {
            var showmanArray = this.showmanProcesses.filter(function(showman) {
                return showman.OPERATION !== operation && showman.ORDERNO !== orderId;
            });

            this.showmanProcesses = showmanArray;
            this._saveLocalStorage();
        };

        Showman.prototype.initNewObject = function () {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });
            this.onAfterExistingDataSet();
        };


        Showman.prototype.constructor = Showman;

        return Showman;

    });