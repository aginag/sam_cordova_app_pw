sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function Partner(data, requestHelper, model) {
            DataObject.call(this, "PartnerHeader", "Partner", requestHelper, model);

            this.data = data;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.columns = ["MOBILE_ID", "ACTION_FLAG", "NAME_LIST", "STREET", "PARTNER_ROLE", "CITY1", "COUNTRY", "FAX_NUMBER", "ROLE_DESCRIPTION", "TEL_NUMBER"];
            this.setDefaultData();
        }

        Partner.prototype = Object.create(DataObject.prototype, {});

        Partner.prototype.onAfterExistingDataSet = function() {

        };

        Partner.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = this.getRandomUUID(12);
                }

                that[column] = value;
            });
        };

        Partner.prototype.constructor = Partner;

        return Partner;
    }
);