sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function Inventory(data, requestHelper, model) {
            DataObject.call(this, "InventoryList", "Inventory", requestHelper, model);

            this.data = data;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.columns = ["MOBILE_ID", "MATERIAL", "MATL_DESC", "STGE_LOC", "ENTRY_QNT", "ENTRY_UOM"];
            this.setDefaultData();
        }

        Inventory.prototype = Object.create(DataObject.prototype, {});

        Inventory.prototype.onAfterExistingDataSet = function() {

        };

        Inventory.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = this.getRandomUUID(12);
                }

                that[column] = value;
            });
        };

        Inventory.prototype.constructor = Inventory;

        return Inventory;
    }
);