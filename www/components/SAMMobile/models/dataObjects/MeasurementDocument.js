sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject",  "SAMMobile/models/dataObjects/MeasdocLongtexts"],

    function(JSONModel, DataObject, MeasdocLongtexts) {
        "use strict";
        // constructor
        function MeasurementDocument(data, requestHelper, model) {
            DataObject.call(this, "MeasurementDocument", "MeasurementDocument", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "POINT", "READG_CHAR", "CNTRR_CHAR", "RECDVI", "MDOCM", "ERDAT", "ERUHR", "RECDV", "RECDU", "ERNAM", "MDTXT", "CODGR", "ITIME", "IDATE", "CODCT", "READG", "ACTION_FLAG", "VLCOD", "CVERS", "WOOBJ"];
            
           
            this.setDefaultData();
        }

        MeasurementDocument.prototype = Object.create(DataObject.prototype, {
            setDate: {
                value: function (newStartTime) {
                    var newStartTimeMoment = moment(newStartTime);
                    this.ERDAT = newStartTimeMoment.format("YYYY-MM-DD");
                }
            },
            getMDOCM: {
                value: function () {
                    return this.MDOCM;
                }
            },

            setLongText: {
                value: function (longTexts) {
                    if (!this.MeasdocLongtext) {
                        this.MeasdocLongtext = [];
                    }

                    this.MeasdocLongtext = this.MeasdocLongtext.concat(longTexts);
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined) {
                        return this.MeasDocLongtext;
                    }

                    var filteredByObjectType = this.MeasDocLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    return this.MeasdocLongtext.formatToText(filteredByObjectType);
                }
            }
        });

        MeasurementDocument.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            // Remark: This function currently adds longtexts only on top of existing longtexts
            var maxLineNumberForObjType = 0;
            var objKey = "00000000";

            if (this.getLongText()) {
                var longTextsByObjType = this.getLongText().filter(function (longText) {
                    return longText.OBJTYPE == objType;
                });

                maxLineNumberForObjType = longTextsByObjType.map(function (longText) {
                    return parseInt(longText.LINE_NUMBER);
                }).sort(function (a, b) {
                    return a - b;
                }).pop() || 0;
            }

            var measDocLongtext = new MeasdocLongtexts(null, this.getRequestHelper(), this);

            var newLongTexts = measDocLongtext.createFromString(text, objType, objKey, maxLineNumberForObjType, this);
            newLongTexts.forEach(function (lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        MeasurementDocument.prototype.initNewObject = function() {
            
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MDOCM") {
                    value = that.getRandomCharUUID(20);
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });

            this.setDate(dateNow.dateTime);
        };

        MeasurementDocument.prototype.constructor = MeasurementDocument;

        return MeasurementDocument;
    }
);