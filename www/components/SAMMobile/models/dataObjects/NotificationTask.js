sap.ui.define(["sap/ui/model/json/JSONModel",
                "SAMContainer/models/DataObject",
                "SAMMobile/models/dataObjects/NotificationLongtext",],

    function(JSONModel, DataObject, NotificationLongtext) {
        "use strict";
        // constructor
        function NotificationTask(data, requestHelper, model) {
            DataObject.call(this, "NotificationTask", "NotificationTask", requestHelper, model);
            
            this.data = data;
            this._hasChanged = false;
            this.NotificationLongtext = null;

            this.columns = ["MOBILE_ID", "NOTIF_NO", "ITEM_KEY", "ITEM_SORT_NO", "TASK_KEY", "TASK_SORT_NO", "TASK_TEXT", "OBJECT_NO", "TASK_CAT_TYP", "TASK_CODE", "TASK_CODEGRP", "TXT_TASKGRP", "TXT_TASKCD",
                "PARTNER", "PARTN_ROLE", "DELETE_FLAG", "XA_STAT_PROF", "ACTION_FLAG", "LONG_TEXT"];

            this.setDefaultData();
            this.ITEM_KEY = '0000';
            this.ITEM_SORT_NO = '0000';
            this.TASK_CAT_TYP = '2';
        }

        NotificationTask.prototype = Object.create(DataObject.prototype, {
            setNotifNo: {
                value: function(notifNo) {
                    this.NOTIF_NO = notifNo;
                }
            },

            setLongText: {
                value: function (longTexts) {
                    if (!this.NotificationLongtext) {
                        this.NotificationLongtext = [];
                    }

                    this.NotificationLongtext = this.NotificationLongtext.concat(longTexts);
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined) {
                        return this.NotificationLongtext;
                    }

                    var filteredByObjectType = this.NotificationLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    return NotificationLongtext.formatToText(filteredByObjectType);
                }
            },
            setStartEndTime: {
                value: function (newStartTime) {
                    var newStartDateMoment = moment(newStartTime);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(newStartTime);

                    this._startTime = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this._endTime = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

                    this.PLND_START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.PLND_END_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");

                    this.PLND_START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.PLND_END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    //this.updateEndTime();
                }
            },

            setStartTime: {
                value: function (value) {
                    var newStartDateMoment = moment(value);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(value);

                    this.START_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.START_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                }
            },

            setEndTime: {
                value: function (value) {
                    var newStartDateMoment = moment(value);
                    newStartDateMoment.hours(0);
                    newStartDateMoment.minutes(0);
                    newStartDateMoment.seconds(0);
                    newStartDateMoment.milliseconds(0);

                    var newStartTimeMoment = moment(value);

                    this.END_DATE = newStartDateMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                    this.END_TIME = newStartTimeMoment.format("YYYY-MM-DD HH:mm:ss.SSS");
                }
            },

        });
        
//        NotificationTask.prototype.updateNotifHeader = function(){
//            return {
//                type: "update",
//                mainTable: true,
//                tableName: "NotificationTask",
//                values: [["ACTION_FLAG", "C"]],
//                replacementValues: [],
//                whereConditions: "NOTIF_NO = '" + this.NOTIF_NO + "'",
//                mainObjectValues: []
//            };
//        };
//        
        NotificationTask.prototype.saveTask = function(callbackFn) {
            var sqlStrings = [], that = this;
            //if value is blank, call delete
            if(this.TASK_TEXT === ""){
        	this.deleteTask(callbackFn);
            }
            //else
            else{
        	var error_method = function(error){
        	    alert(error);
        	}
        	var success_method;
        	//if action_flag is null, insert into Notif Activity and update
        	if(this.ACTION_FLAG === ""){
              that.ACTION_FLAG = 'N';
        	    sqlStrings.push(this.insert(true));
        	    success_method = function(mobileId){
        		that.MOBILE_ID = mobileId;
        		that.ACTION_FLAG = 'N';
        		callbackFn();
        	    }
        	}
                //else if action_flag is present, update the value of existing row
        	else{
        	    sqlStrings.push(this.update(true));
        	    success_method = function(mobileId){
        		callbackFn();
        	    }
        	}
        	sqlStrings.push(this.updateNotifHeader());
        	this.getRequestHelper().executeStatementsAndCommit(sqlStrings, success_method, error_method);
            }
            
        };
        
        NotificationTask.prototype.deleteTask = function(callbackFn) {
            var sqlStrings = [], that = this;
//            var error_method = function(error){
//    	    	alert(error);
//    	    };
//    	    var success_method = function(mobileId){
//    		callbackFn();
//	    };
            //just clear the value
	    this.DELETE_FLAG = 'X';
	    this._hasChanges = true;
            //if(this.ACTION_FLAG === 'N' || this.ACTION_FLAG === 'O'){
        	//if action_flag is N or N-like, delete the Notif Activity from db and reload
        	sqlStrings.push(this.delete(true));
//            }else{
//        	//if action_flag is other non-null value, then set the value of the db row to blank and reload
//        	sqlStrings.push(this.update(true));
//            }
            callbackFn(sqlStrings);
        };
        
        NotificationTask.prototype.initNewObject = function() {
            var that = this;
            var dateNow = this.getDateNow();

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } 

                that[column] = value;
            });
            this.setStartEndTime(dateNow.dateTime);
        };

        NotificationTask.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            // Remark: This function currently adds longtexts only on top of existing longtexgts
            var maxLineNumberForObjType = 0;

            if (this.getLongText()) {
                var longTextsByObjType = this.getLongText().filter(function(longText) {
                    return longText.OBJTYPE == objType;
                });

                maxLineNumberForObjType = longTextsByObjType.map(function(longText) {
                    return parseInt(longText.LINE_NUMBER)
                }).sort(function(a,b) {
                    return a - b;
                }).pop() || 0;
            }


            var notificationLongtext = new NotificationLongtext(null, this.getRequestHelper());

            var newLongTexts = notificationLongtext.createFromString(text, objType, "00000000", maxLineNumberForObjType, this);
            newLongTexts.forEach(function(lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function(e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        NotificationTask.prototype.loadLongText = function(objType) {
            var that = this;

            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setLongText(that.mapDataToDataObject(NotificationLongtext, data.NotificationLongtext));
                    resolve(that.getLongText(objType));
                };

                if (!that.getLongText()) {
                    that._fetchLongText().then(load_success).catch(reject);
                } else {
                    resolve(that.getLongText(objType));
                }
            });

        };

        NotificationTask.prototype.constructor = NotificationTask;

        return NotificationTask;
    }
);
