sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/Order",
        "SAMMobile/models/dataObjects/OrderUserStatus",
        "SAMMobile/models/dataObjects/OrderLongtext"
    ],

    function (JSONModel, DataObject, Order, OrderUserStatus, OrderLongtext) {
        "use strict";

        // constructor
        function OrderOperation(data, requestHelper, model) {
            DataObject.call(this, "OrderOperations", "OrderOperations", requestHelper, model);

            this.data = data;

            this.OrderLongText = null;
            this.OrderOperationLongtext = null;

            this.OrderUserStatus = null;

            this.columns = ["MOBILE_ID", "OBJECT_NO", "ORDERID", "ACTIVITY", "SUB_ACTIVITY", "ACTION_FLAG", "ACTTYPE"];


            this.setDefaultData();
        }

        OrderOperation.prototype = Object.create(DataObject.prototype, {
            setUserStatus: {
                value: function (statusArray) {
                    this.OrderUserStatus = statusArray;
                }
            },

            getUserStatus: {
                value: function () {
                    return this.OrderUserStatus;
                }
            },

            getActiveUserStatus: {
                value: function () {
                    var activeStatus = this.OrderUserStatus.filter(function (userStatus) {
                        return userStatus.isActive();
                    });

                    return activeStatus.length == 0 ? null : activeStatus[0];
                }
            },

            getOrderId: {
                value: function () {
                    return this.ORDERID;
                }
            },

            getActivity: {
                value: function () {
                    return this.ACTIVITY;
                }
            },

            setLongText: {
                value: function (longTexts) {
                  this.OrderOperationLongtext = longTexts;
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined || objType == null) {
                        return this.OrderOperationLongtext;
                    }

                    var filteredByObjectType = this.OrderOperationLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    //return OrderLongtext.formatToText(filteredByObjectType);
                    return filteredByObjectType;
                }
            }


        });

        OrderOperation.prototype.setTimeForOperation = function () {
            var startDate;
            var endDate;
            this.STARTDATE = "";
            this.ENDDATE = "";
            if (this.START_CONS &&
                this.STRTTIMCON &&
                this.FIN_CONSTR &&
                this.FINTIMCONS) {

                    var startDate1 = moment(this.START_CONS.split(" ").shift() + " " + this.STRTTIMCON.split(" ").pop());
                    var endDate1 = moment(this.FIN_CONSTR.split(" ").shift() + " " + this.FINTIMCONS.split(" ").pop());

                    if(this.TEAM_BEG_DATUM &&
                        this.TEAM_BEG_UZEIT &&
                        this.TEAM_END_DATUM &&
                        this.TEAM_END_UZEIT){
                        var startDate2 = moment(this.TEAM_BEG_DATUM.split(" ").shift() + " " + this.TEAM_BEG_UZEIT.split(" ").pop());
                        var endDate2 = moment(this.TEAM_END_DATUM.split(" ").shift() + " " + this.TEAM_END_UZEIT.split(" ").pop());

                        if(startDate1.isSame(startDate2) && endDate1.isSame(endDate2)){
                            if (!this.ZFIX_visible) {
                                if(this.TERMIN_START && this.TERMIN_END){
                                    startDate = moment(this.TERMIN_START);
                                    endDate = moment(this.TERMIN_END);
                                }
                            }else{
                                startDate = startDate1;
                                endDate = endDate1;
                            }
                        }else{
                            if(endDate1.diff(startDate1, 'days') == 0){
                                startDate = startDate1;
                                endDate = endDate1;

                                this.TERMIN_START = "";
                                this.TERMIN_END = "";
                            }else if(this.TERMIN_START && this.TERMIN_END){
                                startDate = moment(this.TERMIN_START);
                                endDate = moment(this.TERMIN_END);
                            }
                            }
                }else{
                    if(endDate1.diff(startDate1, 'days') == 0){
                        startDate = startDate1;
                        endDate = endDate1;
                    }else if(this.TERMIN_START && this.TERMIN_END){
                        startDate = moment(this.TERMIN_START);
                        endDate = moment(this.TERMIN_END);
                    }
                }

                }else{
                    var startDate1 = moment(this.START_CONS.split(" ").shift() + " " + this.STRTTIMCON.split(" ").pop());
                    var endDate1 = moment(this.FIN_CONSTR.split(" ").shift() + " " + this.FINTIMCONS.split(" ").pop());

                    if(this.TERMIN_START && this.TERMIN_END){
                        startDate = moment(this.TERMIN_START);
                        endDate = moment(this.TERMIN_END);
                    }else{
                        startDate = startDate1;
                        endDate = endDate1;
                }

                }

                if(this.ZFIX_visible || this.rejectStatusAvailable){
                    this.TERMIN_START = "";
                    this.TERMIN_END = "";
                }

                if(startDate && endDate){
                    if (this.model.getLanguage) {
                        var language = this.model.getLanguage();
                    } else {
                        var language = this.model.component.globalViewModel.getActiveUILanguage();
                    }

                    if (language === 'D') {
                        this.STARTDATE = startDate.isValid() ? startDate.format("DD.MM.YYYY HH:mm") : "";
                        this.ENDDATE = endDate.isValid() ? endDate.format("DD.MM.YYYY HH:mm") : "";
                    } else {
                        this.STARTDATE = startDate.isValid() ? startDate.format("YYYY/MM/DD HH:mm") : "";
                        this.ENDDATE = endDate.isValid() ? endDate.format("YYYY/MM/DD HH:mm") : "";
                    }

                    this._startDate = moment(startDate).format("YYYY-MM-DD HH:mm:ss.SSS");
                    this._endDate = moment(endDate).format("YYYY-MM-DD HH:mm:ss.SSS");
                } else if (startDate1 && startDate2) {
                    this._startDate = moment(startDate1).format("YYYY-MM-DD HH:mm:ss.SSS");
                    this._endDate = moment(endDate1).format("YYYY-MM-DD HH:mm:ss.SSS");
                }
        };

        OrderOperation.prototype.changeUserStatus = function (statusMl, bStrings, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];
            var userStatus = null;

            // Check if there is and inactive UserStatus for STATUSML
            //  -> Yes: Set it to active
            //  -> No: Create and insert new UserStatus for STATUSML

            var inactiveUserStatusForStatusMl = this.OrderUserStatus.filter(function (userStatus) {
                return userStatus.INACT == "X" && userStatus.STATUS == statusMl.USER_STATUS;
            });

            if (inactiveUserStatusForStatusMl.length > 0) {
                userStatus = inactiveUserStatusForStatusMl[0];
                userStatus.setActive(true);

                sqlStrings.push(userStatus.update(true));
            } else {
            	var activeUserStatus = this.OrderUserStatus.filter(function (userStatus) {
            		return userStatus.INACT == "" && userStatus.STATUS == statusMl.USER_STATUS;
                });
            	
            	if (activeUserStatus.length === 0) {
                    userStatus = new OrderUserStatus(null, this.getRequestHelper());
                    userStatus.initFromMl(statusMl);
                    userStatus.ORDERID = this.ORDERID;
                    userStatus.OBJNR = this.OBJECT_NO;
                    userStatus.CHANGED = "X";

                    sqlStrings.push(userStatus.insert(true));
            	}else if(activeUserStatus.length === 1){
                    if(activeUserStatus[0].STATUS == "E0012" || activeUserStatus[0].STATUS == "E0013")
                    {
                        activeUserStatus[0].setActive(false);
                        sqlStrings.push(activeUserStatus[0].update(true));
                    }
                }
            }

            if(userStatus && userStatus.STATUS == "E0013"){
                var getActiveStopStatus = this.OrderUserStatus.filter(function (userStatus) {
                    return userStatus.INACT == "" && userStatus.STATUS == "E0012";
                });

                if (getActiveStopStatus.length > 0) {
                    var userStatus1 = getActiveStopStatus[0];
                    userStatus1.setActive(false);
    
                    sqlStrings.push(userStatus1.update(true));
                }
            }else if(userStatus && userStatus.STATUS == "E0012"){
                var getActiveStartStatus = this.OrderUserStatus.filter(function (userStatus) {
                    return userStatus.INACT == "" && userStatus.STATUS == "E0013";
                });

                if (getActiveStartStatus.length > 0) {
                    var userStatus1 = getActiveStartStatus[0];
                    userStatus1.setActive(false);
    
                    sqlStrings.push(userStatus1.update(true));
                }
            }

            if (bStrings) {
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    successCb(e);
                }, errorCb);
            }

        };

        OrderOperation.prototype.setOperationsUserStatus = function (statusMl, successCb, errorCb) {
            /*
                Mark ORDER´s operation with NEW_USER_STATUS

                1. Delete all order operation user status with action flag 'N' (sqlScript)
                2. Load all operations of ORDER
                3. For each OPERATION (set current active status to inactive) create a new ORDER_USER_STATUS or set an existing (inactive) status to active
                4. Execute sqlStrings
             */
            var that = this;
            var sqlStrings1 = [];
            var sqlStrings2 = [];

            var deleteNewSql = "DELETE FROM OrderUserStatus WHERE OBJNR = '$1' AND ACTION_FLAG = 'N'";
            deleteNewSql = deleteNewSql.replace("$1", this.OBJECT_NO);

            sqlStrings1.push(deleteNewSql);

            var onLoadUserStatusesSuccess = function (data) {
                try {

                    sqlStrings2 = sqlStrings2.concat(this.changeUserStatus(statusMl, true));

                    executeUpdates(sqlStrings1, function (success) {
                        that.getRequestHelper().executeStatementsAndCommit(sqlStrings2, successCb, errorCb);
                    }, errorCb);
                } catch (err) {
                    errorCb(err);
                }
            };

            this.loadUserStatuses(onLoadUserStatusesSuccess.bind(this), errorCb);
        };

        OrderOperation.prototype.setUserStatusToInactive = function (statusMl, bStrings, successCb, errorCb) {
            var sqlStrings = [];
            var userStatusToBeChanged = null;

            var aActiveUserStatusToBeChanged = this.OrderUserStatus.filter(function (userStatus) {
                var sStatus = userStatus.STATUS || userStatus.USER_STATUS;
                return sStatus === statusMl.USER_STATUS && userStatus.isActive();
            });

            if (aActiveUserStatusToBeChanged.length > 0) {

                userStatusToBeChanged = aActiveUserStatusToBeChanged[0];
                userStatusToBeChanged.setActive(false);
                if((statusMl.USER_STATUS == 'E0002' || statusMl.USER_STATUS == 'E0003') && statusMl.ACTION_FLAG == 'N'){
                    sqlStrings.push(userStatusToBeChanged.delete(true));
                }else{
                    sqlStrings.push(userStatusToBeChanged.update(true));    
                }
                

                if (bStrings) {
                    return sqlStrings;
                } else {
                    this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                        successCb(e);
                    }, errorCb);
                }
            } else {
                // Nothing to do...
                successCb();
            }
        };

        OrderOperation.prototype.loadUserStatuses = function (successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var orderOperations = data.OrderUserStatuses.map(function (userStatusData) {
                    return new OrderUserStatus(userStatusData, that.getRequestHelper(), that.model);
                });

                that.setUserStatus(orderOperations);

                successCb(that.getUserStatus());
            };

            // Always fetch the complete UserStatuses as no inactive ones were fetched before
            this._fetchUserStatuses().then(load_success).catch(errorCb);
        };

        OrderOperation.prototype._fetchUserStatuses = function () {
            return this.requestHelper.fetch({
                id: "OrderUserStatuses",
                statement: "SELECT os.MOBILE_ID, os.STATUS, os.ORDERID, os.USER_STATUS_DESC, os.USER_STATUS_CODE, os.OBJNR, os.INACT, os.CHGNR, os.CHANGED, os.ACTION_FLAG " +
                    "FROM ORDEROPERATIONS as o "
                    + "JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO "
                    + "WHERE o.OBJECT_NO = '@objectNo' " 
            }, {
                objectNo: this.OBJECT_NO
            });
        };

        OrderOperation.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });
        };

        OrderOperation.prototype.removeUnsyncLongtext = function (bStrings, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            var aLongText = this.getLongText();
            var allUnSyncLongtext = [];
            var allSyncLongtext = [];

            if (aLongText.length) {

            	allUnSyncLongtext = aLongText.filter(function (longText) {
                    return ( longText.OBJTYPE === objType && longText.ACTION_FLAG === 'N' );
                });

            	allSyncLongtext = aLongText.filter(function (longText) {
                    return ( longText.OBJTYPE === objType && longText.ACTION_FLAG !== 'N' );
                });
            }

            allUnSyncLongtext.forEach(function (lt) {
                sqlStrings.push(lt.delete(true));
            });

            if (bStrings) {
                this.setLongText(allSyncLongtext);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(allSyncLongtext);
                    if (successCb !== undefined) {
                    	successCb(e);
                    }
                }, errorCb);
            }
        };

        OrderOperation.prototype.addLongtext = function (bStrings, text, objType, formatCol, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            // Remark: This function currently adds longtexts only on top of existing longtexts
            var maxLineNumberForObjType = 0;
            var objKey = this.getActivity() + "0000";

            var aLongText = this.getLongText();
            var orderOperationLongText = new OrderLongtext(null, this.getRequestHelper());
            var allLongTextsByObjType = [];

            if (aLongText && aLongText.length > 0) {

                allLongTextsByObjType = aLongText.filter(function (longText) {
                    return longText.OBJTYPE == objType;
                });

                // Set the existing longtext to changed, as otherwise they will get overwritten by the newly added.
                allLongTextsByObjType.forEach(function (longText) {
                    if (longText.ACTION_FLAG !== 'N') {
                        sqlStrings.push(longText.setChanged(true));
                    }
                });

                maxLineNumberForObjType = allLongTextsByObjType.map(function (longText) {
                    return parseInt(longText.LINE_NUMBER);
                }).sort(function (a, b) {
                    return a - b;
                }).pop() || 0;

            }

            var newLongTexts = orderOperationLongText.createFromString(text, objType, objKey, maxLineNumberForObjType, this, formatCol);
            newLongTexts.forEach(function (lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
            	// reset the longtext to force the reload from the DB to get back the Mobile_id
                this.setLongText();
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(allLongTextsByObjType.concat(newLongTexts));
                    successCb(e);
                }, errorCb);
            }
        };

        OrderOperation.prototype.loadLongText = function (objType, successCb, errorCb) {
            var that = this;
            var load_success = function (data) {
                that.setLongText(that.mapDataToDataObject(OrderLongtext, data.OrderOperationLongtext));
                successCb(that.getLongText(objType));
            };

            if (!this.getLongText()) {
                this._fetchOrderOperationLongtext(objType).then(load_success).catch(errorCb);
            } else {
                successCb(this.getLongText(objType));
            }
        };

        OrderOperation.prototype.loadLongTextPromise = function (objType) {
            var that = this;
            return new Promise(function(resolve, reject) {
                that.loadLongText(objType, resolve, reject);
            });
        };

        OrderOperation.prototype._fetchOrderOperationLongtext = function (objType) {
            return this.requestHelper.fetch({
                id: "OrderOperationLongtext",
                statement: "SELECT * FROM ORDERLONGTEXT WHERE OBJTYPE = '@objType' " +
                    "AND ORDERID = '@orderId' AND OBJKEY LIKE '@objKey%' ORDER BY LINE_NUMBER ASC"
            }, {
                orderId: this.getOrderId(),
                objType: objType,
                objKey: this.getActivity()
            });
        };

        OrderOperation.prototype.constructor = OrderOperation;

        return OrderOperation;
    }
);