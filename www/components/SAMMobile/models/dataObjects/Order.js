sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/OrderUserStatus",
        "SAMMobile/models/dataObjects/OrderOperation",
        "SAMMobile/models/dataObjects/MaterialConfirmation",
        "SAMMobile/models/dataObjects/OrderComponent",
        "SAMMobile/models/dataObjects/OObjectListObject",
        "SAMMobile/models/dataObjects/TimeConfirmation",
        "SAMMobile/models/dataObjects/OrderLongtext",
        "SAMMobile/helpers/formatter"
    ],

    function (JSONModel,
              DataObject,
              OrderUserStatus,
              OrderOperation,
              MaterialConfirmation,
              OrderComponent,
              OObjectListObject,
              TimeConfirmation,
              OrderLongtext,
              Formatter) {
        "use strict";

        // constructor
        function Order(orderData, requestHelper, model) {
            DataObject.call(this, "Orders", "Orders", requestHelper, model);

            this.data = orderData;

            this._PERS_NO = null;

            this.formatter = Formatter;

            this.OrderLongtext = null;
            this.OrderOperations = null;
            this.OrderMaterialConfirmations = null;
            this.OrderComponents = null;
            this.NewOrderComponents = null;
            this.OrderObjectList = null;
            this.NotificationHeader = null;
            this.TimeEntries = {
                data: null,
                loaded: false
            };

            this.columns = ["MOBILE_ID", "ORDERID", "ACTION_FLAG"];

            this.setDefaultData();
        }

        Order.prototype = Object.create(DataObject.prototype, {
            setPersNo: {
                value: function (persNo) {
                    this._PERS_NO = persNo;
                }
            },

            getPersNo: {
                value: function () {
                    return this._PERS_NO;
                }
            },

            getOrderId: {
                value: function () {
                    return this.ORDERID;
                }
            },

            setLongText: {
                value: function (longTexts) {
                    this.OrderLongtext = longTexts;
                }
            },

            getLongText: {
                value: function (objType) {
                    if (!objType || objType == undefined || objType == null) {
                        return this.OrderLongtext;
                    }

                    var filteredByObjectType = this.OrderLongtext.filter(function (longText) {
                        return longText.OBJTYPE == objType;
                    });

                    return OrderLongtext.formatToText(filteredByObjectType);
                }
            },

            setNotification: {
                value: function (notification) {
                    this.NotificationHeader = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this.NotificationHeader;
                }
            },

            setOrderOperations: {
                value: function (operations) {
                    this.OrderOperations = operations;
                }
            },

            getOrderOperations: {
                value: function () {
                    return this.OrderOperations;
                }
            },

            setOrderComponents: {
                value: function (components) {
                    this.OrderComponents = components;
                }
            },

            getOperationActivity: {
                value: function () {
                    return this.OPERATION_ACTIVITY;
                }
            },

            getOrderComponents: {
                value: function () {
                    return this.OrderComponents;
                }
            },

            setMaterialConfirmations: {
                value: function (matConfirmations) {
                    this.OrderMaterialConfirmations = matConfirmations;
                }
            },

            getMaterialConfirmations: {
                value: function () {
                    return this.OrderMaterialConfirmations;
                }
            },

            setOrderObjectList: {
                value: function (list) {
                    this.OrderObjectList = list;
                }
            },

            getOrderObjectList: {
                value: function () {
                    return this.OrderObjectList;
                }
            },

            getCounterList: {
                value: function () {
                    return this.CounterList;
                }
            },

            setCounterList: {
                value: function (list) {
                    this.CounterList = list;
                }
            },

            addTimeEntry: {
                value: function (entry) {
                    if (!this.TimeEntries.data) {
                        this.TimeEntries.data = [];
                    }

                    this.TimeEntries.data.push(entry);
                }
            },

            setTimeEntries: {
                value: function (entries) {
                    if (!this.TimeEntries.data) {
                        this.TimeEntries.data = [];
                    }

                    this.TimeEntries.data = this.TimeEntries.data.concat(entries);
                }
            },

            getTimeEntries: {
                value: function () {
                    return this.TimeEntries.data;
                }
            },

            isTimeEntriesLoaded: {
                value: function () {
                    return this.TimeEntries.loaded;
                }
            },

            setTimeEntriesLoaded: {
                value: function (bLoaded) {
                    this.TimeEntries.loaded = bLoaded;
                }
            },

            resetTimeEntries: {
                value: function () {
                    this.TimeEntries.loaded = false;
                    this.TimeEntries.data = null;
                }
            },

            getLocation: {
                value: function () {
                    if (this.ADR_LON == "" || this.ADR_LAT == "") {
                        return null;
                    }

                    return [this.ADR_LAT, this.ADR_LON];
                }
            }
        });

        Order.prototype.setTimeForOperation = function () {
            var startDate;
            var endDate;
            this._START_DATE = "";
            this._END_DATE = "";
            if (this.OPERATION_STARTDATE &&
                this.OPERATION_STARTTIME &&
                this.OPERATION_ENDDATE &&
                this.OPERATION_ENDTIME) {
            
                    var startDate1 = moment(this.OPERATION_STARTDATE.split(" ").shift() + " " + this.OPERATION_STARTTIME.split(" ").pop());
                    var endDate1 = moment(this.OPERATION_ENDDATE.split(" ").shift() + " " + this.OPERATION_ENDTIME.split(" ").pop());
                    
                    startDate = startDate1;
                    endDate = endDate1;

                    if (this.team && this.team.TEAM_BEG_DATUM &&
                        this.team.TEAM_BEG_UZEIT &&
                        this.team.TEAM_END_DATUM &&
                        this.team.TEAM_END_UZEIT){
                        
                            var startDate2 = moment(this.team.TEAM_BEG_DATUM.split(" ").shift() + " " + this.team.TEAM_BEG_UZEIT.split(" ").pop());
                        var endDate2 = moment(this.team.TEAM_END_DATUM.split(" ").shift() + " " + this.team.TEAM_END_UZEIT.split(" ").pop());

                        if(startDate1.isSame(startDate2) && endDate1.isSame(endDate2)){
                            if (!this.ZFIX_visible) {
                                this.OWN_TIME = true;
                            }else{
                                this.OWN_TIME = false;
                            }
                        }else{
                            if(endDate1.diff(startDate1, 'days') == 0){
                                this.OWN_TIME = false;
                            }else{
                                this.OWN_TIME = true;
                            }  
                        }
                    }else{
                        if(endDate1.diff(startDate1, 'days') == 0){
                            this.OWN_TIME = true;
                        }else{
                            this.OWN_TIME = false;
                        } 
                    }
                        
                }else if (this.team && this.team.TEAM_BEG_DATUM &&
                    this.team.TEAM_BEG_UZEIT &&
                    this.team.TEAM_END_DATUM &&
                    this.team.TEAM_END_UZEIT){

                    var startDate1 = moment(this.team.TEAM_BEG_DATUM.split(" ").shift() + " " + this.team.TEAM_BEG_UZEIT.split(" ").pop());
                    var endDate1 = moment(this.team.TEAM_END_DATUM.split(" ").shift() + " " + this.team.TEAM_END_UZEIT.split(" ").pop());
                
                    startDate = startDate1;
                    endDate = endDate1;

                    this.OWN_TIME = true;
                }

                if(startDate && endDate){
                    this._START_DATE_TIMESTAMP = startDate.format("YYYY-MM-DD HH:mm:ss.SSS");
                    if (this.model.getLanguage() === 'D') {
                        this._START_DATE = startDate.isValid() ? startDate.format("DD.MM.YYYY, HH:mm ") : "";
                        this._END_DATE = endDate.isValid() ? endDate.format("DD.MM.YYYY, HH:mm ") : "";
                    } else {
                        this._START_DATE = startDate.isValid() ? startDate.format("YYYY/MM/DD, HH:mm ") : "";
                        this._END_DATE = endDate.isValid() ? endDate.format("YYYY/MM/DD, HH:mm ") : "";
                    }
                }
        };

        Order.prototype.setOperationsUserStatus = function (statusMl, successCb, errorCb) {
            /*
                Mark ORDER´s operation with NEW_USER_STATUS

                1. Delete all order operation user status with action flag 'N' (sqlScript)
                2. Load all operations of ORDER
                3. For each OPERATION (set current active status to inactive) create a new ORDER_USER_STATUS or set an existing (inactive) status to active
                4. Execute sqlStrings
             */
            var that = this;
            var sqlStrings1 = [];
            var sqlStrings2 = [];

            var deleteNewSql = "DELETE FROM OrderUserStatus WHERE OBJNR IN (SELECT OBJECT_NO FROM OrderOperations WHERE ORDERID = '$1' AND PERS_NO = '$2') AND ACTION_FLAG = 'N'";
            deleteNewSql = deleteNewSql.replace("$1", this.getOrderId());
            deleteNewSql = deleteNewSql.replace("$2", this.getPersNo());

            sqlStrings1.push(deleteNewSql);

            var onLoadOperationSuccess = function (data) {
                try {
                    data.forEach(function (operation) {
                        sqlStrings2 = sqlStrings2.concat(operation.changeUserStatus(statusMl, true));
                    });

                    executeUpdates(sqlStrings1, function (success) {
                        that.getRequestHelper().executeStatementsAndCommit(sqlStrings2, successCb, errorCb);
                    }, errorCb);
                } catch (err) {
                    errorCb(err);
                }
            };

            this.loadOperations(onLoadOperationSuccess, errorCb);
        };

        Order.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];

            // Remark: This function currently adds longtexts only on top of existing longtexts
            var maxLineNumberForObjType = 0;

            var aLongText = this.getLongText();
            var oOrderLongtext = new OrderLongtext(null, this.getRequestHelper());
            var allLongTextsByObjType = [];

            if (aLongText.length) {
                allLongTextsByObjType = aLongText.filter(function (longText) {
                    return longText.OBJTYPE == objType;
                });

                // Set the existing longtext to changed, as otherwise they will get overwritten by the newly added.
                allLongTextsByObjType.forEach(function (longText) {
                    if (longText.ACTION_FLAG !== 'N') {
                        sqlStrings.push(longText.setChanged(true));
                    }
                });

                maxLineNumberForObjType = allLongTextsByObjType.map(function (longText) {
                    return parseInt(longText.LINE_NUMBER);
                }).sort(function (a, b) {
                    return a - b;
                }).pop() || 0;
            }

            var newLongTexts = oOrderLongtext.createFromString(text, objType, "00000000", maxLineNumberForObjType, this);
            newLongTexts.forEach(function (lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(allLongTextsByObjType.concat(newLongTexts));
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function (e) {
                    that.setLongText(allLongTextsByObjType.concat(newLongTexts));
                    successCb(e);
                }, errorCb);
            }
        };

        Order.prototype.loadLongText = function (objType, successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setLongText(that.mapDataToDataObject(OrderLongtext, data.OrderLongtext));
                successCb(that.getLongText(objType));
            };

            if (!this.getLongText()) {
                this._fetchOrderLongtext().then(load_success).catch(errorCb);
            } else {
                successCb(this.getLongText(objType));
            }
        };

        Order.prototype.loadOperations = function (successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var orderOperations = data.OrderOperations.map(function (operationData) {
                    var operationObject = new OrderOperation(operationData, that.getRequestHelper(), that.model);

                    var operationUserStatus = data.OrderOperationStatus.filter(function (status) {
                        return status.OBJNR == operationData.OBJECT_NO;
                    });

                    operationObject.setUserStatus(operationUserStatus.map(function (statusData) {
                        return new OrderUserStatus(statusData, that.getRequestHelper(), that.model);
                    }));

                    return operationObject;
                });

                that.setOrderOperations(orderOperations);

                successCb(that.getOrderOperations());
            };

            this._fetchOrderOperations().then(load_success).catch(errorCb);

            if (!this.getOrderOperations()) {
                this._fetchOrderOperations().then(load_success).catch(errorCb);
            } else {
                successCb(this.getOrderOperations());
            }
        };

        /*      Order.prototype.loadMaterialConfirmations = function (successCb, errorCb) {
                  var that = this;

                  var load_success = function (data) {
                      that.setMaterialConfirmations(that.mapDataToDataObject(MaterialConfirmation, data.OrderMaterialConfirmations));

                      successCb(that.getMaterialConfirmations());
                  };

                  if (!this.getMaterialConfirmations()) {
                      this._fetchOrderMaterialConfirmations().then(load_success).catch(errorCb);
                  } else {
                      successCb(this.getMaterialConfirmations());
                  }
              };*/

        Order.prototype.loadOrderComponents = function (successCb, errorCb) {
              
            var that = this;

            var load_success = function (data) {
                that.setOrderComponents(that.mapDataToDataObject(OrderComponent, data.OrderComponents));

                successCb(that.getOrderComponents());
            };

            this._fetchOrderComponents().then(load_success).catch(errorCb);
        };

        Order.prototype.loadMaterialConfirmations = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setMaterialConfirmations(that.mapDataToDataObject(MaterialConfirmation, data.OrderMaterialConfirmations));

                successCb(that.getMaterialConfirmations());
            };

            if (!this.setMaterialConfirmations()) {
                this._fetchMaterialConfirmations().then(load_success).catch(errorCb);
            } else {
                successCb(this.getMaterialConfirmations());
            }
        };

        Order.prototype.loadTimeEntries = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setTimeEntries(that.mapDataToDataObject(TimeConfirmation, data.TimeConfirmationForPW));

                successCb(that.getTimeEntries());
            };

            if (!this.isTimeEntriesLoaded()) {
                this.setTimeEntriesLoaded(true);
                this._fetchTimeConfirmationForPW().then(load_success).catch(errorCb);
            } else {
                successCb(this.getTimeEntries());
            }
        };

        Order.prototype.loadOrderObjectList = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setOrderObjectList(that.mapDataToDataObject(OObjectListObject, data.OrderObjectList));

                successCb(that.getOrderObjectList());
            };

            if (!this.getOrderObjectList()) {
                this._fetchOrderObjectList().then(load_success).catch(errorCb);
            } else {
                successCb(this.getOrderObjectList());
            }
        };

        Order.prototype.loadAll = function (successCb, errorCb) {
            var that = this;
            var getDataQueryArray = [];

            var load_success = function (data) {
                if (data.hasOwnProperty("OrderObjectList")) {
                    that.setOrderObjectList(that.mapDataToDataObject(OObjectListObject, data.OrderObjectList));
                }

                if (data.hasOwnProperty("TimeConfirmation")) {
                    that.setTimeEntriesLoaded(true);
                    that.setTimeEntries(that.mapDataToDataObject(TimeConfirmation, data.TimeConfirmationForPW));
                }

                if (data.hasOwnProperty("OrderComponents")) {
                    that.setOrderComponents(that.mapDataToDataObject(OrderComponent, data.OrderComponents));
                }

                if (data.hasOwnProperty("OrderLongtext")) {
                    that.setLongText(that.mapDataToDataObject(OrderLongtext, data.OrderLongtext));
                }

                successCb();
            };

            if (!this.getCounterList()) {
                getDataQueryArray.push("CounterTimeConfirmation");
                getDataQueryArray.push("CounterMaterialConfirmation");
                getDataQueryArray.push("CounterOrderObjects");
                getDataQueryArray.push("CounterSAMObjects");
                getDataQueryArray.push("CountOrderMaterialConfirmations");
            }

            if (!this.getOrderObjectList()) {
                getDataQueryArray.push("OrderObjectList");
            }

            if (!this.isTimeEntriesLoaded()) {
                getDataQueryArray.push("TimeConfirmation");
            }

            if (!this.getOrderComponents()) {
                getDataQueryArray.push("OrderComponents");
            }

            if (!this.getLongText()) {
                getDataQueryArray.push("OrderLongtext");
            }

            if (getDataQueryArray.length == 0) {
                successCb();
                return;
            }

            this.requestHelper.getData(getDataQueryArray, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                language: this.model.getLanguage(),
                notifNo: this.NOTIF_NO,
                codeGroups: " ('Y0000003', 'Y0000004') "
            }, load_success, errorCb, true);
        };

        Order.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });
        };

        Order.prototype.loadCounters = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setCounterList(data);
                successCb(that.getCounterList());
            };

            var fetchCounterQueue = [
                this._fetchCounterOperations(),
                this._fetchCounterMaterials(),
                this._fetchCounterTimeConfirmation(),
                this._fetchCounterChecklist(),
                this._fetchCounterMinMax(),
                this._fetchCounterSAMObjects(),
                this._fetchCounterObjectsTab()
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);

        };


        Order.prototype.loadNotification = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setNotification(that.mapDataToDataObject(NotificationHeader, data.NotificationHeader)[0]);

                successCb(that.getNotification());
            };

            if (!this.getNotification()) {
                this._fetchOrderNotification().then(load_success).catch(errorCb);
            } else {
                successCb(this.getNotification());
            }
        };

        Order.prototype._fetchOrderObjectList = function () {
            if(this.PMACTTYPE == "N44"){
                return this.requestHelper.fetch({
                    id: "OrderObjectList",
                    statement: "SELECT a.ORDERID, b.OBJECT_NO, a.COUNTER, a.FUNCT_LOC, a.FUNCLDESCR, a.EQUIPMENT, a.EQUIDESCR, c.XA_MEASUREMENT_POINT " +
                        "FROM ORDEROBJECTLIST as a "+
                        "LEFT JOIN OrderOperations as b on b.ORDERID = a.ORDERID and b.ACTIVITY = '@activity' "+
                        "LEFT JOIN EquipmentMeasPoint as c on c.EQUNR = a.EQUIPMENT and c.CODGR = 'SRB-GEN' "+
                        "WHERE a.ORDERID = '@orderId'"
                }, {
                    orderId: this.getOrderId(),
                    activity: this.OPERATION_ACTIVITY
                });
            }else{
                return this.requestHelper.fetch({
                    id: "OrderObjectList",
                    statement: "SELECT a.ORDERID, c.OBJECT_NO, a.COUNTER, a.FUNCT_LOC, a.FUNCLDESCR, a.EQUIPMENT, a.EQUIDESCR, a.PROCESSING_IND, a.MOBILE_ID " +
                        "FROM ORDEROBJECTLIST as a "+
                        "LEFT JOIN OrderOperations as b on b.ORDERID = a.ORDERID and b.ACTIVITY = '@activity' "+
                        "LEFT JOIN Orders as c on c.ORDERID = a.ORDERID  "+
                        "WHERE a.ORDERID = '@orderId'"
                }, {
                    orderId: this.getOrderId(),
                    activity: this.OPERATION_ACTIVITY
                });
            }
        };

        Order.prototype._fetchCounterOrderObjectList = function () {
            return this.requestHelper.fetch({
                id: "CounterOrderObjects",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT " +
                    "FROM ORDEROBJECTLIST WHERE ORDERID = '@orderId'"
            }, {
                orderId: this.getOrderId()
            });
        };

        Order.prototype._fetchTimeConfirmation = function () {
            return this.requestHelper.fetch({
                id: "TimeConfirmation",
                statement: "SELECT t.MOBILE_ID, t.ACTIVITY, t.ACT_TYPE, t.START_TIME, t.END_TIME, t.CONF_TEXT, t.CONF_NO, t.CONF_CNT, t.CALC_MOTIVE, t.ACT_WORK, t.UN_WORK, t.PERS_NO, t.COMPLETE, t.WORK_CNTR, t.ACTION_FLAG "
                    + "FROM ORDEROPERATIONS as op "
                    + "JOIN TimeConfirmation as t on t.ORDER_NO = op.ORDERID AND t.ACTIVITY = op.ACTIVITY "
                    + "WHERE op.ORDERID = '@orderId' AND op.PERS_NO = '@persNo' "
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo()
            });
        };

        Order.prototype._fetchTimeConfirmationForPW = function () {
            return this.requestHelper.fetch({
                id: "TimeConfirmationForPW",
                statement: "SELECT t.MOBILE_ID, t.ACTIVITY, t.START_TIME, t.START_DATE, t.END_TIME, t.END_DATE, t.CONF_NO, t.CONF_CNT, t.ACT_WORK, t.UN_WORK, t.PERS_NO, t.WORK_CNTR, t.ACTION_FLAG, t.B_EXP_FLAG, t.B_VALUE_ID, t.K_EXP_FLAG, t.K_VALUE_ID, t.PLANT, t.KTEXT, t.T_VALUE_ID, t.K_VALUE_ID "
                    + "FROM ORDEROPERATIONS as op "
                    + "JOIN ZPMC_TIMECONF as t on t.ORDERID = op.ORDERID AND t.ACTIVITY = op.ACTIVITY "
                    + "WHERE op.ORDERID = '@orderId' AND t.PERS_NO = '@persNo' AND t.ACTIVITY = '@activity' "
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                activity: this.getOperationActivity()
            });
        };

        Order.prototype._fetchCounterTimeConfirmation = function () {
            return this.requestHelper.fetch({
                id: "CounterTimeConfirmation",
                statement: "SELECT COUNT(t.MOBILE_ID) as COUNT "
                    + "FROM ORDEROPERATIONS as op "
                    + "JOIN ZPMC_TIMECONF as t on t.ORDERID = op.ORDERID AND t.ACTIVITY = op.ACTIVITY "
                    + "WHERE op.ORDERID = '@orderId' AND t.PERS_NO = '@persNo' AND t.ACTIVITY = '@activity' "
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                activity: this.getOperationActivity()
            });
        };

        Order.prototype._fetchCounterOperations = function() {
            return this.requestHelper.fetch({
                id: "CounterOperations",
                statement: "SELECT COUNT(op.MOBILE_ID) as COUNT " +
                    "FROM ORDEROPERATIONS as op " +
                    "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = op.TEAM_GUID " +
                    "WHERE op.ORDERID = '@orderId' " +
                    "AND ( op.PERS_NO = '@persNo' OR (op.TEAM_GUID = '@teamGuid' AND op.TEAM_GUID IS NOT NULL AND op.TEAM_GUID <> '') OR op.TEAM_PERSNO = '@persNo') " +
                    "AND ( op.START_CONS <= '@startCons' OR  t.TEAM_BEG_DATUM <= '@startDat' ) " +
                    "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '@yesterday' ) " +
                    "AND NOT EXISTS ( " +
                    "SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = op.OBJECT_NO " +
                    "AND ( ( ORDERUSERSTATUS.STATUS = '@doneStatus' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) OR ( ORDERUSERSTATUS.STATUS = '@prioStatus' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) ) " +
                    "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ) ",
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                teamGuid: this.TEAM_GUID,
                startCons: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                startDat: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                yesterday: moment().add(-1,'days').format("YYYY-MM-DD"),
                doneStatus: "E0002",
                prioStatus: "E0003"
            });
        };
              
          Order.prototype._fetchCounterMaterials = function() {
              return this.requestHelper.fetch({
                  id: "CounterMaterials",
                  statement: "SELECT COUNT(distinct oc.MOBILE_ID) as COUNT "
                  +"FROM ORDEROPERATIONS as op "
                  +"JOIN OrderComponent as oc on oc.ORDERID = op.ORDERID "
                  +"LEFT JOIN ZMALFUNC_MATERIAL as ml on ml.MATERIAL = oc.MATERIAL "
                  +"WHERE op.ORDERID = '@orderId'"
              }, {
                  orderId: this.getOrderId(),
                  activity: this.OrderOperations.ACTIVITY,
                  language: this.model.getLanguage()
              });
          };

        Order.prototype._fetchOrderComponents = function() {
            return this.requestHelper.fetch({
                id: "OrderComponents",
                statement: "SELECT distinct oc.MOBILE_ID, ml.MATL_DESC, oc.ITEM_TEXT, oc.ITEM_CAT, oc.ACTIVITY, oc.ORDERID, oc.MATERIAL, oc.ENTRY_QNT, oc.ENTRY_UOM, oc.REQUIREMENT_QUANTITY, oc.REQUIREMENT_QUANTITY_UNIT, oc.RESERV_NO, oc.RES_ITEM, oc.PLANT, oc.STGE_LOC, oc.ACTION_FLAG "
                    + "FROM ORDEROPERATIONS as op "
                    + "JOIN OrderComponent as oc on oc.ORDERID = op.ORDERID "
                    + "LEFT JOIN ZMALFUNC_MATERIAL as ml on ml.MATERIAL = oc.MATERIAL "
                    + "WHERE op.ORDERID = '@orderId'"
            }, {
                orderId: this.getOrderId(),
                activity: this.OrderOperations.ACTIVITY,
                language: this.model.getLanguage()
            });
        };

        Order.prototype._fetchCounterMaterialConfirmations = function () {
            return this.requestHelper.fetch({
                id: "CountOrderMaterialConfirmations",
                statement: "SELECT COUNT(mc.MOBILE_ID) as COUNT " +
                    "FROM ORDEROPERATIONS as op " +
                    "LEFT JOIN ORDERCOMPONENT as oc ON oc.ORDERID=op.ORDERID AND oc.ACTIVITY=op.ACTIVITY " +
                    "LEFT JOIN MATERIALCONFIRMATION as mc ON mc.MATERIAL=oc.MATERIAL " +
                    "LEFT JOIN INVENTORYLIST as ii ON ii.MATERIAL=oc.MATERIAL " +
                    "LEFT JOIN MATERIALML as ml ON ml.MATERIAL=oc.MATERIAL AND ml.SPRAS = '@language' " +
                    "WHERE oc.ORDERID = '@orderId' AND op.PERS_NO = '@persNo'"
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                language: this.model.getLanguage()
            });
        };

        Order.prototype._fetchMaterialConfirmations = function () {
            return this.requestHelper.fetch({
                id: "OrderMaterialConfirmations",
                statement: "SELECT mc.MOBILE_ID, oc.ITEM_TEXT, oc.ITEM_CAT, oc.ACTIVITY, oc.REQUIREMENT_QUANTITY, oc.REQUIREMENT_QUANTITY_UNIT, oc.RESERV_NO, oc.RES_ITEM, oc.PLANT, oc.STGE_LOC, oc.ACTION_FLAG, oc.ORDERID, oc.MATERIAL, ml.MATL_DESC, ml.ENTRY_UOM, mc.ENTRY_QNT " +
                    "FROM ORDEROPERATIONS as op " +
                    "LEFT JOIN ORDERCOMPONENT as oc ON oc.ORDERID=op.ORDERID AND oc.ACTIVITY=op.ACTIVITY " +
                    "LEFT JOIN MATERIALCONFIRMATION as mc ON mc.MATERIAL=oc.MATERIAL " +
                    "LEFT JOIN INVENTORYLIST as ii ON ii.MATERIAL=oc.MATERIAL " +
                    "LEFT JOIN MATERIALML as ml ON ml.MATERIAL=oc.MATERIAL AND ml.SPRAS = '@language' " +
                    "WHERE oc.ORDERID = '@orderId' AND op.PERS_NO = '@persNo AND oc.ACTIVITY = '@activity''"
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                activity: this.OrderOperations.ACTIVITY,
                language: this.model.getLanguage()
            });
        };

        /*    Order.prototype._fetchOrderMaterialConfirmations = function() {
                return this.requestHelper.fetch({
                    id: "OrderMaterialConfirmations",
                    statement: "SELECT mc.MOBILE_ID, mc.ORDER_ITNO, mc.ITEM_TEXT, mc.SERIALNO, mc.ACTIVITY, mc.ORDERID, mc.MATERIAL, mc.ENTRY_QNT, mc.ENTRY_UOM, mc.MAT_DOC, mc.DOC_YEAR, mc.PLANT, mc.STGE_LOC, mc.CALC_MOTIVE, mc.ACTION_FLAG "
                        + "FROM ORDEROPERATIONS as op "
                        + "JOIN MATERIALCONFIRMATION as mc on mc.ORDERID = op.ORDERID AND mc.ACTIVITY = op.ACTIVITY "
                        + "WHERE op.ORDERID = '@orderId' AND op.PERS_NO = '@persNo'"
                }, {
                    orderId: this.getOrderId(),
                    persNo: this.getPersNo()
                });
            };*/
          Order.prototype._fetchCounterChecklist = function () {
              return this.requestHelper.fetch({
                  id: "CounterChecklist",
                  statement: "SELECT COUNT(MOBILE_ID) AS COUNT FROM Checklists " +
                  "WHERE ORDERID = '@orderId' "
              }, {
                  orderId: this.getOrderId()
              });
          };

        Order.prototype._fetchCounterMinMax = function () {
            return this.requestHelper.fetch({
                id: "CounterMinMax",
                statement: "SELECT COUNT(MOBILE_ID) AS COUNT FROM CHECKLISTS WHERE ORDERID = '@orderId' AND CHECK_TYPE = '1'"
            }, {
                orderId: this.getOrderId()
            });
        };

        Order.prototype._fetchCounterObjectsTab = function () {
            return this.requestHelper.fetch({
                id: "CounterObjectsTab",
                statement: "SELECT COUNT(a.MOBILE_ID) AS COUNT " +
                    "FROM ORDEROBJECTLIST as a "+
                    "LEFT JOIN OrderOperations as b on b.ORDERID = a.ORDERID and b.ACTIVITY = '@activity' "+
                    "LEFT JOIN EquipmentMeasPoint as c on c.EQUNR = a.EQUIPMENT "+
                    "WHERE a.ORDERID = '@orderId'"
            }, {
                orderId: this.getOrderId(),
                activity: this.OPERATION_ACTIVITY
            });
        };

        Order.prototype._fetchCounterSAMObjects = function () {
            return this.requestHelper.fetch({
                id: "CounterSAMObjects",
                statement: "SELECT SUM(i) AS COUNT FROM (SELECT COUNT(a.MOBILE_ID) as COUNT FROM SAMOBJECTATTACHMENTS as a " +
                    "WHERE (a.OBJKEY = '@objKey' OR a.OBJKEY = '@notificationId' OR a.OBJKEY = '@funcLoc' OR a.OBJKEY = '@equipment') " +
                    "AND a.ACTION_FLAG !='A' AND a.ACTION_FLAG !='B'" +
                    "UNION ALL SELECT COUNT(a1.MOBILE_ID) as COUNT FROM DMSATTACHMENTS as a1 " +
                    "WHERE (a1.OBJKEY = '@objKey' OR a1.OBJKEY = '@notificationId' OR a1.OBJKEY = '@funcLoc' OR a1.OBJKEY = '@equipment') " +
                    "AND a1.ACTION_FLAG !='A' AND a1.ACTION_FLAG !='B') AS d(i) "
            }, {
                objKey: this.getOrderId(),
                notificationId: this.NOTIF_NO,
                funcLoc: this.FUNCT_LOC,
                equipment: this.EQUIPMENT,
                persNo: this.getPersNo()
            });
        };

        Order.prototype._fetchCounterMaterialConfirmation = function () {
            return this.requestHelper.fetch({
                id: "CounterMaterialConfirmation",
                statement: "SELECT COUNT(oc.MOBILE_ID) as COUNT "
                    + "FROM ORDEROPERATIONS as op "
                    + "JOIN OrderComponent as oc on oc.ORDERID = op.ORDERID AND oc.ACTIVITY = op.ACTIVITY "
                    + "LEFT JOIN MATERIALML as ml on ml.MATERIAL = oc.MATERIAL AND ml.SPRAS = '@language' "
                    + "WHERE op.ORDERID = '@orderId' AND op.PERS_NO = '@persNo'"
            }, {
                orderId: this.getOrderId(),
                persNo: this.getPersNo()
            });
        };

        Order.prototype._fetchOrderNotification = function () {
            return this.requestHelper.fetch({
                id: "NotificationHeader",
                statement: "SELECT MOBILE_ID, NOTIF_NO, NOTIF_NO_DISPLAY, SHORT_TEXT, NOTIF_DATE, NOTIF_TYPE, EQUIPMENT, EQUIPMENT_DESC, SHORT_TEXT, PRIORITY, PRIOTYPE, MATERIAL, XA_STAT_PROF, SERIALNO, FUNCT_LOC, FUNCLOC_DESC, OBJECT_NO, NOTIF_DATE, COMPDATE, COMPTIME, NOTIFTIME, MN_WK_CTR, PLANPLANT, PLANT, CATPROFILE, CODE_GROUP, CODING, ORDERID, BREAKDOWN, ACTION_FLAG "
                    + "from NotificationHeader "
                    + "WHERE NOTIF_NO ='@notifNo' "
            }, {
                notifNo: this.NOTIF_NO
            });
        };

        Order.prototype._fetchOrderOperations = function () {
            var operationsQuery = {
                id: "OrderOperations",
                statement: "SELECT DISTINCT op.MOBILE_ID, op.OBJECT_NO, op.ORDERID, op.ACTTYPE, op.WORK_CNTR, op.ACTIVITY, op.SUB_ACTIVITY, op.START_CONS, op.STRTTIMCON, op.FIN_CONSTR, op.FINTIMCONS, op.DESCRIPTION, op.PERS_NO, op.DURATION_NORMAL, op.DURATION_NORMAL_UNIT, op.ACTION_FLAG, zt.RES_NAME " +
                    "FROM ORDEROPERATIONS as op " +
                    "LEFT JOIN ZMRSTEAM_RES as zt on op.PERS_NO = zt.RES_PERNR " +
                    "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = op.TEAM_GUID " +
                    "WHERE ORDERID = '@orderId' " +
                    "AND ( op.PERS_NO = '@persNo' OR (op.TEAM_GUID = '@teamGuid' AND op.TEAM_GUID IS NOT NULL AND op.TEAM_GUID <> '') OR op.TEAM_PERSNO = '@persNo') " +
                    "AND ( op.START_CONS <= '@startCons' OR  t.TEAM_BEG_DATUM <= '@startDat' ) " +
                    "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '@yesterday' ) " +
                    "AND NOT EXISTS ( " +
                    "SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = op.OBJECT_NO " +
                    "AND ( ( ORDERUSERSTATUS.STATUS = '@doneStatus' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) OR ( ORDERUSERSTATUS.STATUS = '@prioStatus' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) ) " +
                    "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ) ",
            };

            var operationsUserStatusQuery = {
                id: "OrderOperationStatus",
                statement: "SELECT os.MOBILE_ID, os.STATUS, os.ORDERID, os.USER_STATUS_DESC, os.USER_STATUS_CODE, os.OBJNR, os.INACT, os.CHGNR, os.CHANGED, os.ACTION_FLAG " +
                    "FROM ORDEROPERATIONS as o "
                    + "JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO "
                    + "WHERE o.ORDERID = '@orderId' AND o.PERS_NO = '@persNo' AND os.ACTION_FLAG != 'N'"
            };


            return this.requestHelper.fetch([operationsQuery, operationsUserStatusQuery], {
                orderId: this.getOrderId(),
                persNo: this.getPersNo(),
                teamGuid: this.TEAM_GUID,
                startCons: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                startDat: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                yesterday: moment().add(-1,'days').format("YYYY-MM-DD"),
                doneStatus: "E0002",
                prioStatus: "E0003"
            });
        };

        Order.prototype._fetchOrderLongtext = function () {
            return this.requestHelper.fetch({
                id: "OrderLongtext",
                statement: "SELECT * FROM ORDERLONGTEXT WHERE ORDERID = '@orderId' ORDER BY LINE_NUMBER ASC"
            }, {
                orderId: this.getOrderId()
            });
        };

        Order.prototype.constructor = Order;

        return Order;
    }
);
