sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function (JSONModel, DataObject) {
        "use strict";

        // constructor
        function ZOPTime(data, requestHelper, model) {
            DataObject.call(this, "ZPW_OP_TIME", "ZOPTime", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "OBJNR", "TERMIN_END", "TERMIN_START"];
            this.setDefaultData();
        }

        ZOPTime.prototype = Object.create(DataObject.prototype, {});

        ZOPTime.prototype.constructor = ZOPTime;

        return ZOPTime;
    }
);