sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],
   
   function(JSONModel, DataObject) {
      "use strict";
      // constructor
      function Longtext(tableName, columnsIdentifier, requestHelper, model) {
         DataObject.call(this, tableName, columnsIdentifier, requestHelper, model);
      }
      
      Longtext.prototype = Object.create(DataObject.prototype, {
      
      });
      
      Longtext.prototype.FORMAT_COLS = {
         "*": "\n",
         "EN": "",
         ">X": "\n",
         "=": "",
         "/:": "",
         "/(": "",
         "$" : "\n"
      };
      
      Longtext.prototype.createFromString = function(string, objType, objKey, startingLineNumber, mainObject, formatCol) {
         var longTexts = [];
         var newTexts = string.match(/.{1,40}/g);
         
         for (var i in newTexts) {
            var lineNumber = this.addLeadingZeroes('000', (parseInt(i) + 1 + startingLineNumber).toString());
            
            longTexts.push(this._getNewRow(newTexts[i], lineNumber, objType, objKey, mainObject, formatCol));
         }
         
         return longTexts;
      };
      
      Longtext.prototype.addLeadingZeroes = function(template, number) {
         return template.substring(0, template.length - number.length) + number;
      };
      
      Longtext.prototype.formatToText = function(longTexts) {
         var that = this;
         var formattedData = longTexts && longTexts.map(function(longText) {
            var text = longText.TEXT_LINE;
            var format = that.prototype.FORMAT_COLS[longText.FORMAT_COL] == undefined ? " " : that.prototype.FORMAT_COLS[longText.FORMAT_COL];
            
            return text + format;
         });
         
         return formattedData && formattedData.length > 0 ? formattedData.join("") : "";
      };
      
      Longtext.prototype.initNewObject = function() {
         var that = this;
         
         this.columns.forEach(function(column) {
            var value = "";
            
            if (column == "ACTION_FLAG") {
               value = "N";
            } else if (column == "MOBILE_ID") {
               value = "";
            }
            
            that[column] = value;
         });
      };
      
      Longtext.prototype.constructor = Longtext;
      
      return Longtext;
   }
);