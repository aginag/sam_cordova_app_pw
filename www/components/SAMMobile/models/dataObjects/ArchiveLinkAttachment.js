sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/FileSystem",
        "SAMContainer/models/DataObject"],

    function (JSONModel, FileSystem, DataObject) {
        "use strict";

        // constructor
        function ArchiveLinkAttachment(data, requestHelper, model) {
            DataObject.call(this, "ArchiveLink", "ArchiveLink", requestHelper, model);

            this.data = data;
            this.folderPath = null;
            this.fileSystem = new FileSystem();
            this._DELETE_FLAG = false;
            this._OLD_FILENAME = "";

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.columns = ["MOBILE_ID", "TABNAME", "OBJECT_ID", "NEW_MOBILE_ID", "FILENAME", "FILEEXT", "USER_FIELD", "DOC_SIZE", "ARCHIV_ID", "ARC_DOC_ID", "AR_OBJECT", "SAP_OBJECT", "ACTION_FLAG"];
            this.setDefaultData();
        }

        ArchiveLinkAttachment.prototype = Object.create(DataObject.prototype, {
            setFilename: {
                value: function (fileName) {
                    this.FILENAME = fileName;
                }
            },

            setAttachmentHeader: {
                value: function (attachmentHeader) {
                    this.ATTACHMENTHEADER = attachmentHeader;
                }
            },

            setArchivID: {
                value: function (archivId) {
                    this.ARCHIV_ID = archivId;
                }
            },

            setArcDocId: {
                value: function (arcDocId) {
                    this.ARC_DOC_ID = arcDocId;
                }
            },

            setArObject: {
                value: function (arObject) {
                    this.AR_OBJECT = arObject;
                }
            },

            setFileId: {
                value: function (fileID) {
                    this.FILE_ID = fileID;
                }
            },

            setSAPObject: {
                value: function (sapObject) {
                    this.SAP_OBJECT = sapObject;
                }
            },

            setFileExt: {
                value: function (fileExt) {
                    this.FILEEXT = fileExt;
                }
            },

            setFilesize: {
                value: function (fileSize) {
                    this.DOC_SIZE = fileSize;
                }
            },

            setUsername: {
                value: function (username) {
                    this.USER_FIELD = username;
                }
            },

            setFolderPath: {
                value: function (folderPath) {
                    this.folderPath = folderPath;
                }
            },

            setTabName: {
                value: function (tabName) {
                    this.TABNAME = tabName;
                }
            },

            setObjectID: {
                value: function (objKey) {
                    this.OBJECT_ID = objKey;
                }
            },

            getFileName: {
                value: function () {
                    return this.FILENAME;
                }
            },

            getFileExt: {
                value: function () {
                    return this.FILEEXT;
                }
            },

            setDeleteFlag: {
                value: function (bDelete) {
                    this._DELETE_FLAG = bDelete;
                }
            },

            getDeleteFlag: {
                value: function () {
                    return this._DELETE_FLAG;
                }
            },

            setRenamedFileName: {
                value: function (newFileName) {
                    if (this._OLD_FILENAME == "") {
                        this._OLD_FILENAME = this.FILENAME;
                    }

                    this.FILENAME = newFileName;
                }
            }
        });

        ArchiveLinkAttachment.prototype.onAfterExistingDataSet = function() {
            this.ON_DATABASE = true;
            this.setFolderPath(this.fileSystem.getRootDataStorage());
        };

        ArchiveLinkAttachment.prototype.rename = function(newFileName, successCb, errorCb) {
            const that = this;
            var filePath = "";
            var newFileNameWithExtension = newFileName + "." + this.getFileExt();

            if (this.MOBILE_ID == "") {
                //Temp directory
                filePath = this.fileSystem.filePathTmp + "/" + newFileNameWithExtension;
            } else {
                //Doc directory
                filePath = this.fileSystem.getRootDataStorage() + "/" + newFileNameWithExtension;
            }

            this.fileSystem.exists2(filePath, errorCb.bind(null, new Error("File already exists!")), function() {
                if (that.MOBILE_ID == "") {
                    //Rename directly
                    var oldFilePath = that.fileSystem.filePathTmp + "/" + that.getFileName() + "." + that.getFileExt();
                    that.fileSystem.renameFile2(oldFilePath, newFileNameWithExtension, function() {
                        that.setFilename(newFileName);
                        successCb();
                    }, errorCb.bind(null, new Error(that._component.i18n.getText("RENAMING_FAILED"))));
                    return;
                }

                that.setRenamedFileName(newFileName);
                successCb();
            });
        };

        ArchiveLinkAttachment.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "ARC_DOC_ID"){
                    value = that.getRandomUUID(32);
                }
                that[column] = value;
            });

            this.ON_DATABASE = false;
        };

        ArchiveLinkAttachment.prototype.constructor = ArchiveLinkAttachment;

        return ArchiveLinkAttachment;
    }
);