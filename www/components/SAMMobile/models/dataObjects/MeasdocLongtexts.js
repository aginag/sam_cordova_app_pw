sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/Longtext"],

    function(JSONModel, DataObject, Longtext) {
        "use strict";
        // constructor
        function MeasDocLongtext(data, requestHelper, model) {
            Longtext.call(this, "MeasDocLongtext", "MeasDocLongtext", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "OBJTYPE", "OBJKEY", "LINE_NUMBER", "FORMAT_COL", "TEXT_LINE", "ACTION_FLAG", "MDOCM"];
            this.setDefaultData();
        }

        MeasDocLongtext.prototype = Object.create(Longtext.prototype, {

        });

        MeasDocLongtext.prototype._getNewRow = function(string, lineNumber, objType, objKey, measurementDoc, formatCol) {
            var newRow = new MeasDocLongtext(null, this.requestHelper);

            newRow.TEXT_LINE = string;
            newRow.LINE_NUMBER = lineNumber;
            newRow.OBJTYPE = objType;
            newRow.MDOCM = measurementDoc.getMDOCM();
            newRow.FORMAT_COL = formatCol ? formatCol : "*";
            newRow.OBJKEY = objKey;

            return newRow;
        };

        // Static method
        MeasDocLongtext.formatToText = Longtext.prototype.formatToText;

        MeasDocLongtext.prototype.constructor = MeasDocLongtext;

        return MeasDocLongtext;
    }
);