sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/Longtext"],

    function (JSONModel, DataObject, Longtext) {
        "use strict";

        // constructor
        function NotificationLongtext(data, requestHelper, model) {
            Longtext.call(this, "NotificationLongtext", "NotificationLongtext", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "NOTIF_NO", "OBJTYPE", "OBJKEY", "LINE_NUMBER", "FORMAT_COL", "TEXT_LINE", "ACTION_FLAG"];
            this.setDefaultData();
        }

        NotificationLongtext.prototype = Object.create(Longtext.prototype, {});

        NotificationLongtext.prototype._getNewRow = function (string, lineNumber, objType, objKey, notification) {
            var newRow = new NotificationLongtext(null, this.requestHelper);

            newRow.TEXT_LINE = string;
            newRow.LINE_NUMBER = lineNumber;
            newRow.OBJTYPE = objType;
            newRow.NOTIF_NO = notification.NOTIF_NO;
            newRow.FORMAT_COL = "*";
            newRow.OBJKEY = objKey;

            return newRow;
        };

        // Static method
        NotificationLongtext.formatToText = Longtext.prototype.formatToText;

        NotificationLongtext.prototype.constructor = NotificationLongtext;

        return NotificationLongtext;
    }
);