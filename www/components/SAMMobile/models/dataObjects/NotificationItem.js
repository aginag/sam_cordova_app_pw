sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function NotificationItem(data, requestHelper, model) {
            DataObject.call(this, "NotificationItem", "NotificationItem", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "NOTIF_NO", "DL_CAT_TYP", "DL_CODEGRP", "DL_CODE", "ITEM_KEY", "ITEM_SORT_NO", "ACTION_FLAG", "D_CODE", "D_CODEGRP", "DESCRIPT", "LONG_TEXT", "TXT_GRPCD","TXT_OBJPTCD", "TXT_PROBCD", "D_CAT_TYP", "ASSEMBLY"];
            this.setDefaultData();
        }

        NotificationItem.prototype = Object.create(DataObject.prototype, {

        });

        NotificationItem.prototype.initFromNotification = function(statusMl) {

        };

        NotificationItem.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });
        };

        NotificationItem.prototype.constructor = NotificationItem;

        return NotificationItem;
    }
);