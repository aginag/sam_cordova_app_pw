sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject","sap/ui/core/format/NumberFormat",
"SAMMobile/models/dataObjects/NotificationLongtext"],
   
   function(JSONModel, DataObject, NumberFormat, NotificationLongtext) {
      "use strict";
      // constructor
      function SAMChecklistPoint(data, requestHelper, model) {
         DataObject.call(this, "CHECKLISTS", "Checklists", requestHelper, model);
   
         this._ICON = "";
         this._CHECK_TYPE = null;
         this._HAS_TEXT = "";
         this._MEAS_RESULT = "";
         this._TITLE = "TITLE NOT FOUND";
         this._VALUE_STATE = sap.ui.core.ValueState.None;
         this._CHARACT_BASED = false;
         this.data = data;
         this.userStatuses = [];
         this.attachments = [];
         this._ATTACHMENT_COUNT = 0;
         this._HAS_ATTACHMENTS = "false";
         
         this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
         this.columns = ["MOBILE_ID", "ORDERID", "NOTIF_NO", "SORT_NO", "SPRAS", "CHECK_TYPE", "CHECK_KEY", "TASK_TEXT", "DESCRIPTION", "ZRESULT_READING", "ZRESULT_CODE", "MSEHT", "DECIM", "MRMIN", "MRMAX", "USER_TEXT", "ACTION_FLAG", "RECDV", "MEAS_POINT", "USER_FIELD"  ];
         this.setDefaultData();
      }
      
      SAMChecklistPoint.prototype = Object.create(DataObject.prototype, {
         isTask: {
            value: function() {
               return this._CHECK_TYPE === this.TYPE.TASK;
            }
         },

          addAttachment : {
            value: function(attachment) {
               this.attachments.push(attachment);
            }
          },

          getAttachments: {
              value: function() {
                  return this.attachments;
              }
          },

         setUserStatuses: {
            value: function(userStatuses) {
               this.userStatuses = userStatuses;
            }
         },
         
         getStatusProfile: {
            value: function() {
               return this.CHECK_KEY;
            }
         },
         
         setUserText: {
            value: function(text) {
                if (this.isTask()) {
                    this.TASK_TEXT = text;
                } else {
                    this.USER_TEXT = text;
                }

               this._updateHasText();
            }
         },
         
         getUserText: {
            value: function() {
               return this.isTask() ? this.DESCRIPTION : this.USER_TEXT;
            }
         },
         
         setResultCode: {
            value: function(result) {
               this.ZRESULT_CODE = result;
               this.ZRESULT_READING = "";
               this._setMeasResult(this.ZRESULT_CODE);
            }
         },
   
         setResultReading: {
            value: function(result) {
               this.ZRESULT_READING = result;
               this.ZRESULT_CODE = "";
               this._setMeasResult(this.ZRESULT_READING);
            }
         },
         
         updateIcon: {
            value: function() {
               if (this.isTask()) {
                  this._ICON = this.ZRESULT_CODE_DISPLAY === "" || this.ZRESULT_CODE_DISPLAY === "E0001" ? this.TYPE_ICON.TASK.EMPTY : this.TYPE_ICON.TASK.FILLED;
               } else {
                  this._ICON = this.ZRESULT_CODE === "" || this.ZRESULT_CODE === "E0001" ? this.TYPE_ICON.MEAS.EMPTY : this.TYPE_ICON.MEAS.FILLED;
               }
            }
         },

          isReadingResultInRange: {
             value: function() {

                 var enteredReading = parseFloat(this.ZRESULT_READING);
                 var minAcceptedValue = parseFloat(this.MRMIN);
                 var maxAcceptedValue = parseFloat(this.MRMAX) == 0 ? Infinity : parseFloat(this.MRMAX);

                 if (minAcceptedValue == 0 && maxAcceptedValue == 0) {
                     return true;
                 }

                 if (enteredReading > maxAcceptedValue || enteredReading < minAcceptedValue) {
                     return false;
                 }

                 return true;
             }
          },

          getValidReadingRange: {
              value: function() {
                  var enteredReading = parseFloat(this.ZRESULT_READING);
                  var minAcceptedValue = parseFloat(this.MRMIN);
                  var maxAcceptedValue = parseFloat(this.MRMAX) == 0 ? Infinity : parseFloat(this.MRMAX);

                  if (maxAcceptedValue == Infinity) {
                      // must be greater than minimum
                      return " > " + minAcceptedValue;
                  }

                  if (minAcceptedValue == 0 && maxAcceptedValue != 0) {
                      // must be lesser than maximum
                      return " < " + maxAcceptedValue;
                  }

                  // must be in between range
                  return minAcceptedValue + " - " + maxAcceptedValue;
              }
          },

          setValueState: {
             value: function(valueState) {
                 this._VALUE_STATE = valueState;
             }
          },

          updateAttachmentCount: {
            value: function (count) {
                this._ATTACHMENT_COUNT = count;
                this._HAS_ATTACHMENTS = this._ATTACHMENT_COUNT > 0 ? "true" : "false";
            }
        },

        getAttachmentCount: {
         value: function () {
             return this._ATTACHMENT_COUNT;
         }
     },
        
         isTaskCodeAvailable: {
            value: function () {
               return this.TASK_CODEGRP !== "" && this.TASK_CODE !== "";
            }
      },

      setLongText: {
         value: function (longTexts) {
             if (!this.NotificationLongtext) {
                 this.NotificationLongtext = [];
             }

             this.NotificationLongtext = this.NotificationLongtext.concat(longTexts);
         }
     },

     getLongText: {
      value: function () {
         return this.isTask() ? NotificationLongtext.formatToText(this.NotificationLongtext) : "";
      }
  }
      });
      
      SAMChecklistPoint.prototype.TYPE = {
         TASK: 0,
         MEAS: 1
      };
   
      SAMChecklistPoint.prototype.TYPE_ICON = {
         TASK: {
            EMPTY: "sap-icon://checklist-item",
            FILLED: "sap-icon://checklist-item-2"
         },
         MEAS: {
            EMPTY: "sap-icon://measurement-document",
            FILLED: "sap-icon://measurement-document"
         }
      };

      SAMChecklistPoint.prototype.onAfterExistingDataSet = function() {
         // Set Helper properties
         this._CHECK_TYPE = parseInt(this.CHECK_TYPE);
         this._CHARACT_BASED = this._getCharacteristicBased();

         this._updateTitle();
         this._setMeasResult(this._getMeasResult());
      };

      SAMChecklistPoint.prototype._getCharacteristicBased = function (result) {
         if (!this.ATINN || this.ATINN == "" || this.ATINN == "0000000000") {
             return false;
         }

         return true;
     };

     SAMChecklistPoint.prototype._setMeasResult = function (result) {
      if (parseFloat(result) == 0) {
          this._MEAS_RESULT = "";
      } else {
          this._MEAS_RESULT = result;
      }
  };

     SAMChecklistPoint.prototype._getMeasResult = function() {
      if (this.isTask()) {
          return this.ZRESULT_READING == "" ? this.ZRESULT_CODE : this.ZRESULT_READING;
      }

      if (!this._CHARACT_BASED && this.CHECK_KEY !== "") {
          //Scenario 2
          return this.ZRESULT_CODE;
      } else {
          //Scenario 1
          return this.ZRESULT_READING;
      }
  };
      
      SAMChecklistPoint.prototype.initNewObject = function() {
         var that = this;
         
         this.columns.forEach(function(column) {
            var value = "";
            
            if (column == "ACTION_FLAG") {
               value = "N";
            } else if (column == "MOBILE_ID") {
               value = "";
            }
            
            that[column] = value;
         });
      };
      
      SAMChecklistPoint.prototype._updateHasText = function() {
          if (this.isTask()) {
              this._HAS_TEXT = (this.getLongText() || this.DESCRIPTION || this.TASK_TEXT) ? "true" : "false";
          } else {
              this._HAS_TEXT = this.USER_TEXT ? "true" : "false";
          }
      };

       SAMChecklistPoint.prototype._updateTitle = function() {
           this._TITLE = this.isTask() ? this.TXT_TASKCD : this.TASK_TEXT;
       };

       SAMChecklistPoint.prototype.formatMeasurmentPointResultToText = function () {
          var decim = this.DECIM;
          var result = this._MEAS_RESULT;

         if(this.USER_FIELD){
               return this.USER_FIELD;
         }

         if ((result == "" || result.charAt(result.length-1)==".")) {
            return "";
         }

         if (!this._CHARACT_BASED && this.CHECK_KEY !== "") {
                 //Scenario 2
            return result;
         }

         var numFormatter = NumberFormat.getFloatInstance({maxFractionDigits:decim});

         if(numFormatter.format(result) == "0.00"){
             return "";
         }
         return numFormatter.format(result).replace(/\,/g,'');
     };

     SAMChecklistPoint.prototype.resetChecklist = function (bStrings) {
      var that = this;
      this.ACTION_FLAG = "U";
      this.ZRESULT_READING = "0.0000000000";
      this.USER_FIELD = "";

      var valuesForUpdate = this.getChangedSqlValues(this.getSqlValues());

      // Update the current state with the updated values
      this.columns.forEach(function(column) {
         that.data[column] = that[column] || "";
      });

      var sqlString = {
         type: "update",
         mainTable: this.mainTable,
         tableName: this.tableName,
         values: valuesForUpdate,
         replacementValues: [],
         whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
         mainObjectValues: []
      };

     if (bStrings) {
         return sqlString;
   }
  };
      
      // Static methods
      SAMChecklistPoint.TYPE = SAMChecklistPoint.prototype.TYPE;
      
      SAMChecklistPoint.prototype.constructor = SAMChecklistPoint;
      
      return SAMChecklistPoint;
   }
);