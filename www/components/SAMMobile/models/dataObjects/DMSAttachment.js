sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/FileSystem",
        "SAMContainer/models/DataObject"],

    function (JSONModel, FileSystem, DataObject) {
        "use strict";

        // constructor
        function DMSAttachment(data, requestHelper, model) {
            DataObject.call(this, "DMSAttachments", "DMSAttachments", requestHelper, model);

            this.data = data;
            this.folderPath = null;
            this.fileSystem = new FileSystem();
            this._DELETE_FLAG = false;
            this._OLD_FILENAME = "";

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.columns = ["MOBILE_ID", "TABNAME", "OBJKEY", "NEW_MOBILE_ID", "FILENAME", "FILEEXT", "USERNAME", "DOC_SIZE", "FILE_ID", "ACTION_FLAG", "APPLICATION_ID", "DESCRIPTION", "DOCUMENTNUMBER", "DOCUMENTPART", "DOCUMENTTYPE", "DOCUMENTVERSION"];
            this.setDefaultData();
        }

        DMSAttachment.prototype = Object.create(DataObject.prototype, {
            setFilename: {
                value: function (fileName) {
                    this.FILENAME = fileName;
                }
            },

            setAttachmentHeader: {
                value: function (attachmentHeader) {
                    this.ATTACHMENTHEADER = attachmentHeader;
                }
            },

            setDocumentNumber: {
                value: function (documentNumber) {
                    this.DOCUMENTNUMBER = documentNumber;
                }
            },
            setDocumentPart: {
                value: function (documentPart) {
                    this.DOCUMENTPART = documentPart;
                }
            },
            setDocumentType: {
                value: function (documentType) {
                    this.DOCUMENTTYPE = documentType;
                }
            },
            setDocumentVersion: {
                value: function (documentVersion) {
                    this.DOCUMENTVERSION = documentVersion;
                }
            },

            setApplicationID: {
                value: function (applicationID) {
                    this.APPLICATION_ID = applicationID;
                }
            },

            setFileId: {
                value: function (fileID) {
                    this.FILE_ID = fileID;
                }
            },

            setDescription: {
                value: function (description) {
                    if(description.length > 40){
                        description = description.slice(0,40);
                    }
                    this.DESCRIPTION = description;
                }
            },

            setFileExt: {
                value: function (fileExt) {
                    this.FILEEXT = fileExt;
                }
            },

            setFilesize: {
                value: function (fileSize) {
                    this.DOC_SIZE = fileSize;
                }
            },

            setUsername: {
                value: function (username) {
                    this.USERNAME = username;
                }
            },

            setFolderPath: {
                value: function (folderPath) {
                    this.folderPath = folderPath;
                }
            },

            setTabName: {
                value: function (tabName) {
                    this.TABNAME = tabName;
                }
            },

            setObjectKey: {
                value: function (objKey) {
                    this.OBJKEY = objKey;
                }
            },

            getFileName: {
                value: function () {
                    return this.FILENAME;
                }
            },

            getFileExt: {
                value: function () {
                    return this.FILEEXT;
                }
            },

            setDeleteFlag: {
                value: function (bDelete) {
                    this._DELETE_FLAG = bDelete;
                }
            },

            getDeleteFlag: {
                value: function () {
                    return this._DELETE_FLAG;
                }
            },

            setRenamedFileName: {
                value: function (newFileName) {
                    if (this._OLD_FILENAME == "") {
                        this._OLD_FILENAME = this.FILENAME;
                    }

                    this.FILENAME = newFileName;
                }
            }
        });

        DMSAttachment.prototype.onAfterExistingDataSet = function() {
            this.ON_DATABASE = true;
            this.setFolderPath(this.fileSystem.getRootDataStorage());
        };

        DMSAttachment.prototype.rename = function(newFileName, successCb, errorCb) {
            const that = this;
            var filePath = "";
            var newFileNameWithExtension = newFileName + "." + this.getFileExt();
            var checkQueue = [];

            if (this.MOBILE_ID == "") {
                //Temp directory
                filePath = this.fileSystem.filePathTmp + "/" + newFileNameWithExtension;
                checkQueue.push(this.fileSystem.existsPromise(filePath));
                checkQueue.push(this.fileSystem.existsPromise(this.fileSystem.getRootDataStorage() + "/" + newFileNameWithExtension));
            } else {
                //Doc directory
                filePath = this.fileSystem.getRootDataStorage() + "/" + newFileNameWithExtension;
                checkQueue.push(this.fileSystem.existsPromise(filePath));
            }

            Promise.all(checkQueue).then(function() {
                if (that.MOBILE_ID == "") {
                    //Rename directly
                    var oldFilePath = that.fileSystem.filePathTmp + "/" + that.getFileName() + "." + that.getFileExt();
                    that.fileSystem.renameFile2(oldFilePath, newFileNameWithExtension, function() {
                        that.setFilename(newFileName);
                        successCb();
                    }, errorCb.bind(null, new Error(that.model._component.i18n_core.getText("RENAMING_FAILED"))));
                    return;
                }

                that.setRenamedFileName(newFileName);
                successCb();
            }).catch(errorCb.bind(null, new Error(that.model._component.i18n_core.getText("FILE_EXIST"))));
        };

        DMSAttachment.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                } else if (column == "APPLICATION_ID"){
                    value = that.getRandomUUID(32);
                } else if(column == "FILE_ID"){
                    value = that.getRandomCharUUID(32);
                }

                that[column] = value;
            });

            this.ON_DATABASE = false;
        };

        DMSAttachment.prototype.constructor = DMSAttachment;

        return DMSAttachment;
    }
);