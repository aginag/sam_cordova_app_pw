sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject", "SAMMobile/models/dataObjects/TechnicalObject"],

    function (JSONModel, DataObject, TechnicalObject) {
        "use strict";

        // constructor
        function FunctionalLocation(data, requestHelper, model) {
            TechnicalObject.call(this, "FunctionalLocation", "FunctionalLocation", requestHelper, model);

            this.data = data;

            this.columns = ["MOBILE_ID", "EQUNR", "TPLNR", "FUNCLOC_DISP", "EQTYP", "EQART", "OBJNR", "MATNR", "WERK", "LAGER", "SHTXT", "TPLNR_SHTXT", "TPLMA_SHTXT", "FLTYP", "SERNR", "STREET", "POST_CODE1", "SWERK",
                "HERLD", "TPLMA", "CITY1", "COUNTRY", "WKCTR", "ACTION_FLAG"];
            this.setDefaultData();
        }

        FunctionalLocation.prototype = Object.create(TechnicalObject.prototype, {});

        FunctionalLocation.prototype.constructor = FunctionalLocation;

        return FunctionalLocation;
    }
);