sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/helpers/FileSystem", "SAMContainer/models/DataObject"],

    function (JSONModel, FileSystem, DataObject) {
        "use strict";

        // constructor
        function SAMObjectAttachment(data, requestHelper, model) {
            DataObject.call(this, "SAMObjectAttachments", "SAMObjectAttachments", requestHelper, model);

            this.data = data;
            this.folderPath = null;
            this.fileSystem = new FileSystem();
            this._DELETE_FLAG = false;
            this._OLD_FILENAME = "";

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;
            this.columns = ["MOBILE_ID", "TABNAME", "OBJKEY", "NEW_MOBILE_ID", "LINKID", "FILENAME", "FILEEXT", "DOC_SIZE", "ACTION_FLAG"];
            this.setDefaultData();
        }

        SAMObjectAttachment.prototype = Object.create(DataObject.prototype, {
            setFilename: {
                value: function (fileName) {
                    this.FILENAME = fileName;
                }
            },

            setAttachmentHeader: {
                value: function (attachmentHeader) {
                    this.ATTACHMENTHEADER = attachmentHeader;
                }
            },

            setFileExt: {
                value: function (fileExt) {
                    this.FILEEXT = fileExt;
                }
            },

            setFilesize: {
                value: function (fileSize) {
                    this.DOC_SIZE = fileSize;
                }
            },

            setFolderPath: {
                value: function (folderPath) {
                    this.folderPath = folderPath;
                }
            },

            setTabName: {
                value: function (tabName) {
                    this.TABNAME = tabName;
                }
            },

            setObjectKey: {
                value: function (objKey) {
                    this.OBJKEY = objKey;
                }
            },

            getFileName: {
                value: function () {
                    return this.FILENAME;
                }
            },

            getFileExt: {
                value: function () {
                    return this.FILEEXT;
                }
            },

            setDeleteFlag: {
                value: function (bDelete) {
                     this._DELETE_FLAG = bDelete;
                }
            },

            getDeleteFlag: {
                value: function () {
                    return this._DELETE_FLAG;
                }
            },

            setRenamedFileName: {
                value: function (newFileName) {
                    if (this._OLD_FILENAME == "") {
                        this._OLD_FILENAME = this.FILENAME;
                    }

                    this.FILENAME = newFileName;
                }
            }
        });

        SAMObjectAttachment.prototype.onAfterExistingDataSet = function() {
            this.ON_DATABASE = true;
            this.setFolderPath(this.fileSystem.getRootDataStorage())
        };

        SAMObjectAttachment.prototype.rename = function(newFileName, successCb, errorCb) {
            const that = this;
            var filePath = "";
            var newFileNameWithExtension = newFileName + "." + this.getFileExt();
            if (this.MOBILE_ID == "") {
                //Temp directory
                filePath = this.fileSystem.filePathTmp + "/" + newFileNameWithExtension;
            } else {
                //Doc directory
                filePath = this.fileSystem.getRootDataStorage() + "/" + newFileNameWithExtension;
            }

            this.fileSystem.exists2(filePath, errorCb.bind(null, new Error("File already exists!")), function() {
                if (that.MOBILE_ID == "") {
                    //Rename directly
                    var oldFilePath = that.fileSystem.filePathTmp + "/" + that.getFileName() + "." + that.getFileExt();
                    that.fileSystem.renameFile2(oldFilePath, newFileNameWithExtension, function() {
                        that.setFilename(newFileName);
                        successCb();
                    }, errorCb.bind(null, new Error("Renaming of the file failed!")));
                    return;
                }

                that.setRenamedFileName(newFileName);
                successCb();
            });
        };

        SAMObjectAttachment.prototype.initNewObject = function () {
            var that = this;

            this.columns.forEach(function (column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });

            this.ON_DATABASE = false;
        };

        SAMObjectAttachment.prototype.constructor = SAMObjectAttachment;

        return SAMObjectAttachment;
    }
);