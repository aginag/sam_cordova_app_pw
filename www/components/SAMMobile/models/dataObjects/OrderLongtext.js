sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/Longtext"],
   
   function(JSONModel, DataObject, Longtext) {
      "use strict";
      // constructor
      function OrderLongtext(data, requestHelper, model) {
         Longtext.call(this, "OrderLongtext", "OrderLongtext", requestHelper, model);
         
         this.data = data;

         this.columns = ["MOBILE_ID", "ORDERID", "OBJTYPE", "OBJKEY", "LINE_NUMBER", "FORMAT_COL", "TEXT_LINE", "ACTION_FLAG"];
         this.setDefaultData();
      }
      
      OrderLongtext.prototype = Object.create(Longtext.prototype, {
      
      });
      
      OrderLongtext.prototype._getNewRow = function(string, lineNumber, objType, objKey, order, formatCol) {
         var newRow = new OrderLongtext(null, this.requestHelper);
         
         newRow.TEXT_LINE = string;
         newRow.LINE_NUMBER = lineNumber;
         newRow.OBJTYPE = objType;
         newRow.ORDERID = order.getOrderId();
         newRow.FORMAT_COL = formatCol ? formatCol : "*";
         newRow.OBJKEY = objKey;
         
         return newRow;
      };
   
      // Static method
      OrderLongtext.formatToText = Longtext.prototype.formatToText;
      
      OrderLongtext.prototype.constructor = OrderLongtext;
      
      return OrderLongtext;
   }
);