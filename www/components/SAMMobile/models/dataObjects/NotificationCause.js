sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMMobile/models/dataObjects/NotificationLongtext"],

    function(JSONModel, DataObject, NotificationLongtext) {
        "use strict";
        // constructor
        function NotificationCause(data, requestHelper, model) {
            DataObject.call(this, "NotificationCause", "NotificationCause", requestHelper, model);

            this.data = data;
            this._hasChanged = false;
            this.NotificationLongtext = null;

            this.columns = ["MOBILE_ID", "NOTIF_NO", "ITEM_KEY", "ITEM_SORT_NO", "CAUSE_KEY", "CAUSE_SORT_NO", "CAUSETEXT", "REFOBJECTKEY", "CAUSE_CAT_TYP", "CAUSE_CODE", "CAUSE_CODEGRP", "TXT_CAUSEGRP", "TXT_CAUSECD",
                "PRILANG", "DELETE_FLAG", "ACTION_FLAG", "LONG_TEXT", "ADD_FLAG"];

            this.setDefaultData();
            this.ITEM_KEY = '0001';
            this.ITEM_SORT_NO = '0001';
            this.CAUSE_KEY = this.getRandomCharUUID(4);
            this.CAUSE_CAT_TYP = 'U';
        }

        NotificationCause.prototype = Object.create(DataObject.prototype, {
            setNotifNo: {
                value: function(notifNo) {
                    this.NOTIF_NO = notifNo;
                }
            },
        });

       NotificationCause.prototype.updateNotifHeader = function(){
           return {
               type: "update",
               mainTable: true,
               tableName: "NotificationCause",
               values: [["ACTION_FLAG", "C"]],
               replacementValues: [],
               whereConditions: "NOTIF_NO = '" + this.NOTIF_NO + "'",
               mainObjectValues: []
           };
       };

        NotificationCause.prototype.saveCause = function(callbackFn) {
            var sqlStrings = [], that = this;
            //if value is blank, call delete
            if(this.CAUSE_TEXT === ""){
                this.deleteCAUSE(callbackFn);
            }
            //else
            else{
                var error_method = function(error){
                    alert(error);
                }
                var success_method;
                //if action_flag is null, insert into Notif Activity and update
                if(this.ACTION_FLAG === ""){
                    that.ACTION_FLAG = 'N';
                    sqlStrings.push(this.insert(true));
                    success_method = function(mobileId){
                        that.MOBILE_ID = mobileId;
                        that.ACTION_FLAG = 'N';
                        callbackFn();
                    }
                }
                //else if action_flag is present, update the value of existing row
                else{
                    sqlStrings.push(this.update(true));
                    success_method = function(mobileId){
                        callbackFn();
                    }
                }
                sqlStrings.push(this.updateNotifHeader());
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, success_method, error_method);
            }

        };

        NotificationCause.prototype.deleteCause = function(callbackFn) {
            var sqlStrings = [], that = this;
//            var error_method = function(error){
//    	    	alert(error);
//    	    };
//    	    var success_method = function(mobileId){
//    		callbackFn();
//	    };
            //just clear the value
            this.DELETE_FLAG = 'X';
            this._hasChanges = true;
            //if(this.ACTION_FLAG === 'N' || this.ACTION_FLAG === 'O'){
            //if action_flag is N or N-like, delete the Notif Activity from db and reload
            sqlStrings.push(this.delete(true));
//            }else{
//        	//if action_flag is other non-null value, then set the value of the db row to blank and reload
//        	sqlStrings.push(this.update(true));
//            }
            callbackFn(sqlStrings);
        };

        NotificationCause.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = "";
                }

                that[column] = value;
            });

            this.ADD_FLAG = 'X';
        };

   /*     NotificationCause.prototype.addLongtext = function (bStrings, text, objType, successCb, errorCb) {
            var that = this;
            var sqlStrings = [];


            // Remark: This function currently adds longtexts only on top of existing longtexgts
            var maxLineNumberForObjType = 0;

            if (this.getLongText()) {
                var longTextsByObjType = this.getLongText().filter(function(longText) {
                    return longText.OBJTYPE == objType;
                });

                maxLineNumberForObjType = longTextsByObjType.map(function(longText) {
                    return parseInt(longText.LINE_NUMBER)
                }).sort(function(a,b) {
                    return a - b;
                }).pop() || 0;
            }


            var notificationLongtext = new NotificationLongtext(null, this.getRequestHelper());

            var newLongTexts = notificationLongtext.createFromString(text, objType, "00000000", maxLineNumberForObjType, this);
            newLongTexts.forEach(function(lt) {
                sqlStrings.push(lt.insert(true));
            });

            if (bStrings) {
                this.setLongText(newLongTexts);
                return sqlStrings;
            } else {
                this.getRequestHelper().executeStatementsAndCommit(sqlStrings, function(e) {
                    that.setLongText(newLongTexts);
                    successCb(e);
                }, errorCb);
            }
        };

        NotificationCause.prototype.loadLongText = function(objType) {
            var that = this;


            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setLongText(that.mapDataToDataObject(NotificationLongtext, data.NotificationLongtext));
                    resolve(that.getLongText(objType));
                };

                if (!that.getLongText()) {
                    that._fetchLongText().then(load_success).catch(reject);
                } else {
                    resolve(that.getLongText(objType));
                }
            });

        };*/

        NotificationCause.prototype.constructor = NotificationCause;

        return NotificationCause;
    }
);
