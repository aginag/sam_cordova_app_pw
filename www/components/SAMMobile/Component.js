sap.ui.define(["sap/ui/core/UIComponent", "SAMMobile/helpers/RequestHelperCore", "SAMMobile/helpers/RequestHelper",
        "SAMMobile/helpers/Navigator", "SAMMobile/helpers/formatter", "sap/ui/model/json/JSONModel", "sap/m/ResponsivePopover",
        "sap/m/Button", "sap/m/Dialog", 'sap/m/Text', "sap/ui/Device", "sap/m/MessageBox"
    ],

    function (UIComponent,
              RequestHelperCore,
              RequestHelper,
              Navigator,
              formatter,
              JSONModel,
              ResponsivePopover,
              Button,
              Dialog,
              Text,
              Device,
              MessageBox) {
        "use strict";

        return UIComponent.extend("SAMMobile.Component", {
            requestHelper: RequestHelper,
            formatter: formatter,
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup
             * of the app and calls the init method once.
             *
             * @public
             * @override
             */
            init: function () {
                // call the base component'ss init function
                UIComponent.prototype.init.apply(this, arguments);

                sap.ui.getCore().getEventBus().subscribe("loginRoute", "onLoginSuccess", this.onAfterFirstLogin, this);
                sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);
                sap.ui.getCore().getEventBus().subscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().subscribe("appUrl", "newRequest", this.onAppUrlReceived, this);
                sap.ui.getCore().getEventBus().subscribe("locationTracking", "newResponse", this.onLocationTrackingResponseReceived, this);

                this.globalViewModel = sap.ui.getCore().byId("samContainerComponent").getComponentInstance().globalViewModel;

                this.i18n = this.getModel("i18n").getResourceBundle();

                this.messagesModel = new JSONModel({});
                this.requestHelper = new RequestHelperCore({}, {});

                var deviceModel = new JSONModel(Device);
                deviceModel.setDefaultBindingMode("OneWay");

                this.navigator = new Navigator();
                this.publication = this.getManifestObject().getEntry("sap.app").MLSetup.publication;
                this.publicationAutomatedSync = this.getManifestObject().getEntry("sap.app").MLSetup.publication_automatedSync;
                this.setModel(this.globalViewModel.model, "globalViewModel");
                this.setModel(deviceModel, "device");
                this.setModel(this.messagesModel, "messagesModel");

                this.isTracking = false;

                this.componentNavigationActive = false;
                this.componentNavigationHistory = [];

                this.appUrl = null;
                this.appUrlParams = null;

                this.getRouter().initialize();
                this.getRouter().navTo("home");
            },

            onAppUrlReceived: function (channel, event, objectUrl) {
                var url = objectUrl.url;
                this.appUrl = url;
                var pieces = url.split("?");
                var that = this;
                var paramValue = {};

                if (pieces[1]) {

                    var params = pieces[1].split("&");

                    params.forEach(function (param) {

                        var parameterArray = param.split("=");

                        switch (parameterArray[0]) {
                            case "order":
                                paramValue.order = parameterArray[1];
                                break;
                            case "operation":
                                paramValue.operation = parameterArray[1];
                                break;
                            case "notification":
                                paramValue.notification = parameterArray[1];
                                break;
                            case "equipment":
                                paramValue.equipment = parameterArray[1];
                                break;
                            case "funcloc":
                                paramValue.funcloc = parameterArray[1];
                                break;
                            default:
                                paramValue.wrong = true;
                                break;
                        }

                    });

                    if (paramValue.wrong) {
                        MessageBox.error(that.i18n.getText("ERROR_WRONG_PARAMETERS"));
                        this.appUrl = null;
                        this.appUrlParams = null;
                        return;
                    }
                } else {
                    // if no parameters in url, start with workOrders List
                    paramValue.startWorkOrders = true;
                }

                this.appUrlParams = paramValue;

                var userName = this.getModel("globalViewModel").getProperty('/userName');
                if (!userName) {
                    that.getRouter().navTo("login");
                } else {
                    if (paramValue.order && paramValue.operation) {
                        that.getRouter().navTo("workOrdersCustomRoute", {
                            query: {
                                routeName: "workOrderDetailRoute",
                                params: JSON.stringify({
                                    id: paramValue.order,
                                    opId: paramValue.operation,
                                    persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                })
                            }
                        });
                    } else if (paramValue.notification) {
                        that.getRouter().navTo("notificationsCustomRoute", {
                            query: {
                                routeName: "notificationDetailRoute",
                                params: JSON.stringify({
                                    id: paramValue.notification,
                                    persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                })
                            }
                        });
                    } else if (paramValue.equipment) {
                        that.getRouter().navTo("technicalObjectsCustomRoute", {
                            query: {
                                routeName: "technicalObjectsDetailRoute",
                                params: JSON.stringify({
                                    id: paramValue.equipment,
                                    objectType: "0",
                                    persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                })
                            }
                        });

                    } else if (paramValue.funcloc) {
                        that.getRouter().navTo("technicalObjectsCustomRoute", {
                            query: {
                                routeName: "technicalObjectsDetailRoute",
                                params: JSON.stringify({
                                    id: "APPURL_" + paramValue.funcloc,
                                    objectType: "1",
                                    persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                                })
                            }
                        });
                    }
                }

            },

            navToComponent: function (from, to) {
                this.componentNavigationHistory.push({
                    from: from,
                    to: to
                });

                this.componentNavigationActive = true;
                this.getRouter().navTo(to.route, to.params);
            },

            navToPreviousComponent: function () {
                var previousComponent = this.componentNavigationHistory.pop();

                if (this.componentNavigationHistory.length == 0) {
                    this.componentNavigationActive = false;
                }

                this.getRouter().navTo(previousComponent.from.route, previousComponent.from.params);
            },

            resetComponentNavigation: function () {
                this.componentNavigationHistory = [];
                this.componentNavigationActive = false;
            },

            exit: function () {
                sap.ui.getCore().getEventBus().unsubscribe("loginRoute", "onLoginSuccess", this.onAfterFirstLogin, this);
                sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
                sap.ui.getCore().getEventBus().unsubscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().unsubscribe("appUrl", "newRequest", this.onAppUrlReceived, this);
                sap.ui.getCore().getEventBus().unsubscribe("locationTracking", "newResponse", this.onLocationTrackingResponseReceived, this);
            },

            onAfterFirstLogin: function () {
                const samUser = this.globalViewModel.getSAMUser();

                if (this.isTracking) {
                    return;
                }

                // Check if live tracking is enabled
                // -> Yes start it with interval
                if (samUser.getScenario().isLiveTrackEnabled()) {
                    this.startLiveTracking(samUser.getScenario().getLiveTrackingFrequency());
                }
            },

            onAfterSync: function () {
                const samUser = this.globalViewModel.getSAMUser();

                var onSAMUserUpdated = function() {
                    if (this.isTracking) {
                        return;
                    }

                    // Check if live tracking is enabled
                    // -> Yes start it with interval
                    if (samUser.getScenario().isLiveTrackEnabled()) {
                        this.startLiveTracking(samUser.getScenario().getLiveTrackingFrequency());
                    }
                };

                var onSAMUserUpdateFailed = function(err) {
                    MessageBox.alert(err.message);
                };

                //Update SAMUser
                samUser.init().then(onSAMUserUpdated.bind(this)).catch(onSAMUserUpdateFailed.bind(this));
            },

            onLogout: function (channel, eventId, data) {
                this.resetComponentNavigation();
                this.stopLiveTracking().finally(function() {
                    sap.ui.getCore().getEventBus().publish("core", "onLoggedOut", data);
                }.bind(this));
            },

            startLiveTracking: function (interval) {
                var that = this;

                this.isTracking = true;

                startLocationTracking(interval, function (result) {
                    if (result.EVENT == 3) {
                        that.showNewObjectDialog(result.OBJECTS);
                    }
                }, function (err) {
                    that.isTracking = false;
                    console.error(that.i18n.getText("ERROR_LOCATION_LISTEN"));
                });
            },

            stopLiveTracking: function () {
                var that = this;

                return new Promise(function(resolve, reject) {
                    if (!this.isTracking) {
                        resolve();
                        return;
                    }

                    stopLocationTracking(function (result) {
                        that.isTracking = false;
                        resolve();
                    }, function (err) {
                        MessageBox.error(that.i18n.getText("ERROR_LOCATION_TRACKING_STOP"));
                        reject();
                    });
                }.bind(this));
            },

            onLocationTrackingResponseReceived: function(channel, event, data) {
                this.showNewObjectDialog(JSON.parse(data.OBJECTS));
            },

            showNewObjectDialog: function (newObjects) {
                if (this._oNewObjectDialog && this._oNewObjectDialog.isOpen()) {
                    this._oNewObjectDialog.close();
                }

                this.getNewObjectDialog(newObjects).open();
            },

            checkDeltaDone: function () {
                const that = this;
                const samUser = this.globalViewModel.getSAMUser();

                return samUser.fetchInitialDeltaDone().then(function(data) {
                    if (data.SAMInitialDeltaDone.length > 0 && data.SAMInitialDeltaDone[0].INITIAL_DELTA_DONE == "1") {
                        that.globalViewModel.model.setProperty("/deltaDone", true);
                    } else {
                        that.globalViewModel.model.setProperty("/deltaDone", false);
                    }

                    return that.globalViewModel.model.getProperty("/deltaDone");
                });
            },

            getSyncErrorMessageForCode: function (code) {
                return sap.ui.getCore().byId("samContainerComponent").getComponentInstance().getSyncErrorMessageForCode(code);
            },

            sortDatesOnTable: function (sorter) {
                sorter.fnCompare = $.proxy(function (a, b) {
                    // parse to Date object
                    var aDate = new Date(a.split(" ")[0]);
                    var bDate = new Date(b.split(" ")[0]);

                    if (bDate == null) {
                        return -1;
                    }
                    if (aDate == null) {
                        return 1;
                    }
                    if (aDate.getTime() < bDate.getTime()) {
                        return -1;
                    }
                    if (aDate.getTime() > bDate.getTime()) {
                        return 1;
                    }
                    return 0;
                }, this);
            },

            setUiLanguage: function (langCode, reloadModels) {
                // set preferred language on localStorage
                localStorage.setItem("SAM_lang", langCode);
                sap.ui.getCore().getConfiguration().setLanguage(langCode);
                if (localStorage.hasOwnProperty('SAM_CUSTOMIZING_MODEL')) {
                    localStorage.removeItem('SAM_CUSTOMIZING_MODEL');
                }
                if (reloadModels) {
                    sap.ui.getCore().getEventBus().publish("home", "onModelsReload");
                }
            },

            getBarcodeScannerSettings: function () {
                var samConfig = this.getManifestObject().getEntry("sap.app").SAMConfig;
                return samConfig.barcodeScanner;
            },

            sync: function (syncMode) {
                //TODO WORKAROUND, calling sync need to be available in the rootComponent
                var view = this.getRouter().getView("SAMMobile.view.home.HomeToolpage");
                var controller = view.getController();
                var syncModel = this.getManifestObject().getEntry("sap.app").SAMConfig.publication;

                controller.onSync(controller, syncMode || "", syncModel);
            },

            setBusyOn: function () {
                this.globalViewModel.model.setProperty('/busy', true);
            },

            setBusyOff: function () {
                this.globalViewModel.model.setProperty('/busy', false);
            },

            getLeaveScreenConfirmationDialog: function () {
                var globalViewModel = this.globalViewModel.model;
                var that = this;

                var oBundle = this.getModel("i18n").getResourceBundle();
                that._oConfirmationDialog = new Dialog({
                    title: oBundle.getText("Warning"),
                    type: 'Message',
                    state: 'Warning',
                    content: new Text({
                        text: oBundle.getText("loseChanges"),
                        textAlign: 'Center',
                        width: '100%'
                    }),
                    beginButton: new Button({
                        text: oBundle.getText("yes"),
                        press: function () {
                            globalViewModel.oData.hasChanges = false;
                            globalViewModel.refresh();
                        }
                    }),
                    endButton: new Button({
                        text: oBundle.getText("no"),
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        this.destroy();
                    }
                });

                return this._oConfirmationDialog;
            },

            _getConfirmActionDialog: function (message, executeCb) {
                var that = this;
                var oBundle = this.getModel("i18n").getResourceBundle();

                var _oConfirmationDialog = new Dialog({
                    title: oBundle.getText("Warning"),
                    type: 'Message',
                    state: 'Warning',
                    content: new Text({
                        text: message,
                        textAlign: 'Center',
                        width: '100%'
                    }),
                    beginButton: new Button({
                        text: oBundle.getText("yes"),
                        press: function () {
                            executeCb();
                            this.getParent().close();
                        }
                    }),
                    endButton: new Button({
                        text: oBundle.getText("no"),
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        this.destroy();
                    }
                });

                return _oConfirmationDialog;
            },

            getNewObjectDialog: function (newObjects) {
                var oBundle = this.getModel("i18n").getResourceBundle();

                const formatObjTypeToText = function(objectType) {
                    if (objectType == "ORDERS") {
                        return oBundle.getText("Orders");
                    } else if (objectType == "ORDEROPERATIONS") {
                        return oBundle.getText("Operations");
                    } else if (objectType == "FUNCTIONLOC") {
                        return oBundle.getText("TOListFuncLocations");
                    } else if (objectType == "EQUIPMENT") {
                        return oBundle.getText("TOListEquipments");
                    } else if (objectType == "NOTIFICATIONHEADER") {
                        return oBundle.getText("TNotifications");
                    }

                    return objectType;
                };

                // Group new objects in [{OBJECT_TYPE: "..", _COUNT: x}, ... ]
                const groupBy = function (key, array) {
                    var groupedArrayByKey = array.reduce((objectsByKeyValue, obj) => {
                        const value = obj[key];
                        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
                        return objectsByKeyValue;
                    }, {});

                    var groupedArrayWithCount = [];
                    for (var objectKey in groupedArrayByKey) {
                        var object = {};
                        object[key] = formatObjTypeToText(objectKey);
                        object["_COUNT"] = groupedArrayByKey[objectKey].length;
                        groupedArrayWithCount.push(object);
                    }
                    return groupedArrayWithCount;
                };

                var newObjectsViewModel = new JSONModel({objects: groupBy("OBJECT_TYPE", newObjects)});

                if (!this._oNewObjectDialog) {
                    if (!this._oNewObjectPopover) {
                        this._oNewObjectPopover = sap.ui.xmlfragment("newObjectPopover", "SAMMobile.view.home.NewObjectDialog", this);
                    }

                    this._oNewObjectDialog = new Dialog({
                        title: oBundle.getText("OBJECTS_ARRIVED"),
                        content: this._oNewObjectPopover,
                        beginButton: new Button({
                            text: oBundle.getText("syncNow"),
                            type: "Accept",
                            press: function () {
                                sap.ui.getCore().getEventBus().publish("rootComponent", "onSyncRequest");
                                this.getParent().close();
                            }
                        }),
                        endButton: new Button({
                            text: oBundle.getText("close"),
                            press: function () {
                                this.getParent().close();
                            }
                        })
                    });
                }

                this._oNewObjectDialog.setModel(newObjectsViewModel, "newObjectsViewModel");

                return this._oNewObjectDialog;
            },

            openLeaveScreenConfirmationDialog: function (navFn) {
                var confirmDialog = this.getLeaveScreenConfirmationDialog();
                confirmDialog.getBeginButton().attachPress(navFn);
                confirmDialog.open();
            },

            initTheme: function () {
                var theme = localStorage.getItem("SAM_theme");

                if (theme == null) {
                    theme = "sap_belize";
                    localStorage.setItem("SAM_theme", theme);
                }


                this.globalViewModel.model.setProperty("/selectedTheme", theme);
                this.changeTheme(theme);
            },

            changeTheme: function (theme) {
                localStorage.setItem("SAM_theme", theme);

                if (theme !== "sam") {
                    // Remove SAM custom style
                    $('link[rel=stylesheet][href="css/samCustom.css"]').remove();
                } else {
                    // Add SAM custom style
                    $('head').append('<link rel="stylesheet" type="text/css" href="css/samCustom.css">');
                    theme = "sap_fiori_3";
                }

                sap.ui.getCore().applyTheme(theme);
            }
        });

    });