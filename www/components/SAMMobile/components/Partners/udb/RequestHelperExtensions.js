sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                PartnerList: ["MOBILE_ID", "ACTION_FLAG", "NAME_LIST", "STREET", "PARTNER_ROLE"]
            },
            queries: {

                Partners: {
                    get : {
                        query : "SELECT TOP $1 START AT $2 $COLUMNS FROM PartnerHeader " + 
                                " WHERE UPPER(NAME_LIST) LIKE UPPER('%$3%') " +
                                " OR UPPER(STREET) LIKE UPPER('%$4%') " +
                                " OR UPPER(PARTNER_ROLE) LIKE UPPER('%$5%')",
                        params : [ {
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter : "$3",
                            value : "searchString"
                        }, {
                            parameter : "$4",
                            value : "searchString"
                        }, {
                            parameter : "$5",
                            value : "searchString"
                        } ],
                        columns : "PartnerList"
                    },

                    count : {
                        query : "SELECT COUNT(MOBILE_ID) AS count FROM PartnerHeader",
                        params : []
                    }
                },
            }
        };
    });
