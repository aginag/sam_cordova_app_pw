sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Partners/controller/list/PartnerListViewModel",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "sap/ui/model/Filter"
], function(Controller, PartnerListViewModel, MessageBox, formatter, Filter) {
    "use strict";

    return Controller.extend("SAMMobile.components.Partners.controller.list.List", {
        formatter: formatter,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("partnersList").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("partnersList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.partnerListViewModel = new PartnerListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.partnerListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.partnerListViewModel.setList(this.byId("partnerList"));

            this.initialized = false;
            this.getView().setModel(this.partnerListViewModel, "partnerListViewModel");

        },

        _onRouteMatched: function (oEvent) {
            var that = this;
    
            var onMasterDataLoaded = function(routeName) {

                if (this.partnerListViewModel.needsReload()) {
                    this.loadObjects();
                }

                if (!this.initialized) {
                    this.initialized = true;
                }
            };

            this.partnerListViewModel.fetchCounts();
            this.globalViewModel.setComponentHeaderText(this.partnerListViewModel.getHeaderText());
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            var onReloadSuccess = function() {

            };

           this.partnerListViewModel.fetchCounts();
           this.globalViewModel.setComponentHeaderText(this.partnerListViewModel.getHeaderText());

            if (bSync) {
                this.partnerListViewModel.refreshData();
            } else {
                this.partnerListViewModel.fetchData(onReloadSuccess.bind(this));
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {

        },

        onLogout: function() {
            this.initialized = false;
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("partnersList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onTableSearch: function (oEvt) {
            this.partnerListViewModel.onSearch(oEvt);

            /*
            // add filter for search
            var aFilters = [];
            var sQuery = oEvt.getSource().getValue();
            if (sQuery && sQuery.length > 0) {
                var filter = new Filter("Name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters.push(filter);
            }

            // update list binding
            var list = this.byId("partnerList");
            var binding = list.getBinding("items");
            binding.filter(aFilters, "Application");
            //
            */
        },

        onListGrowing: function (oEvt) {
            this.partnerListViewModel.onListGrowing(oEvt);
        }
    });

});