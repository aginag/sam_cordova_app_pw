sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "SAMMobile/models/dataObjects/Partner"],

    function (ViewModel, Filter, Partner) {
        "use strict";

        // constructor
        function PartnerListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/partners";

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        PartnerListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        partners: [],
                        searchString: "",
                        counts: {
                            partners: 0
                        },
                        view: {
                            busy: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold
                        }
                    };
                }
            },

            getInventoryQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        searchString: this.getSearchString()
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function() {
                    return "PARTNERS ( " + this.getProperty("/partnerCounter") + " )";
                }
            }
        });

        PartnerListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        PartnerListViewModel.prototype.onSearch = function (oEvt) {
            // DATABASE SEARCH
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);
        
           
        };

        PartnerListViewModel.prototype.setNewData = function (data, bGrowing) {
            var that = this;
            

            var newPartners = [];
            data.forEach(function (partnerData) {
                newPartners.push(new Partner(partnerData, that._requestHelper, that));
            });

            // If growing, append new data to current data on the model
            // If !growing, just set new data on to the model
            if (bGrowing) {

            } else {
                this.setProperty("/partners", newPartners);
            }

        };

        PartnerListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;
            var that = this;

            var load_success = function (data) {
                that._needsReload = false;

                that.setNewData(data, bGrowing);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching partners"));
            };

            this.setBusy(true);
            this._requestHelper.getData("Partners", this.getInventoryQueryParams(bGrowing ? this.getProperty("/partners").length + 1 : 1), load_success, load_error, true);
        };

        PartnerListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        PartnerListViewModel.prototype.resetModel = function () {
            this._needsReload = true;
        };

        PartnerListViewModel.prototype.fetchCounts = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/partnerCounter", data[0].PartnerCounter[0].COUNT.toString());
            };

            var fetchCounterQueue = [
                this._fetchPartnerCount()
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);
        };

        PartnerListViewModel.prototype._fetchPartnerCount = function(){
            return this._requestHelper.fetch({
                id: "PartnerCounter",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM PartnerHeader "
            });
        };

        PartnerListViewModel.prototype.constructor = PartnerListViewModel;

        return PartnerListViewModel;
    });
