sap.ui.define(["SAMMobile/models/dataObjects/OrderUserStatus", "SAMMobile/models/dataObjects/OrderOperation", "SAMMobile/models/dataObjects/OrderUserStatus"],

    function (Order, OrderOperation, OrderUserStatus) {
        "use strict";

        // constructor
        function AfterSyncOperations(component) {
            this.component = component;
            this.requestHelper = this.component.requestHelper;
            this._errorCallback = null;
        }

        AfterSyncOperations.prototype = {

        };

        AfterSyncOperations.prototype.changeOrderUserStatuses = function(fromStatus, toStatus, successCb) {
            var sqlStrings = [];


            var errorCb = function(err) {
                throw err;
            };

            var onOrderOperationsLoaded = function(operations) {
                // 2. For each orderOperation
                // -> change user status to toStatus where current active status == fromStatus

                var operationUserStatusToBeChanged = operations.filter(function(operation) {
                    var activeStatus = operation.getActiveUserStatus();

                    return activeStatus && activeStatus.STATUS == fromStatus;
                });

                if (operationUserStatusToBeChanged.length == 0) {
                    // Nothing needs to be done
                    successCb(false);
                    return;
                }

                operationUserStatusToBeChanged.forEach(function(operation) {
                    sqlStrings = sqlStrings.concat(operation.changeUserStatus(toStatus, true));
                });

                // 3. For each operation in operationUserStatusToBeChanged
                // -> set parent order to action flag = 'C'
                var sqlOrderChangeStrings = this._getOrderChangedSqlStringsByOrderIds(operationUserStatusToBeChanged.map(function(operation) {
                    return operation.ORDERID;
                }));

                sqlStrings = sqlStrings.concat(sqlOrderChangeStrings);

                this.requestHelper.executeStatementsAndCommit(sqlStrings, successCb.bind(null, true), errorCb.bind(this, new Error("AfterSyncOperations: Error while inserting User Status changes!")));
            };

            var toStatusMl = this._getUserStatusMl(toStatus.STATUS, toStatus.STATUS_PROFILE);

            if (!toStatusMl) {
                throw new Error("Could not change user status: User status not found");
                return;
            }

            toStatus = toStatusMl;

            // 1. Fetch all orderUserStatuses
            this._fetchOrderOperations(onOrderOperationsLoaded.bind(this), errorCb.bind(this, new Error("AfterSyncOperations: Error while fetching Order Operations")))
        };

        AfterSyncOperations.prototype.handleOrderUserStatusErrors = function(successCb) {
            var sqlStrings = [];


            var errorCb = function(err) {
                throw err;
            };

            var onOrderUserStatusLoaded = function(orderUserStatus) {
                var orderUserStatusToBeChanged = orderUserStatus;

                if (orderUserStatusToBeChanged.length == 0) {
                    // Nothing needs to be done
                    successCb(false);
                    return;
                }

                // 2. For each orderUserSTatus
                // -> change status to inactive and action flag to 'C'
                orderUserStatusToBeChanged.forEach(function(status) {
                    if (status.ACTION_FLAG == "P") {
                        status.ACTION_FLAG = "C";
                        status.CHANGED = "X";
                    } else if (status.ACTION_FLAG == "O") {
                        status.ACTION_FLAG = "N";
                        status.CHANGED = "X";
                    }

                    sqlStrings.push(status.update2(true));
                });

                // 3. For each status in orderUserStatusToBeChanged
                // -> set parent order to action flag = 'C'
                var sqlOrderChangeStrings = this._getOrderChangedSqlStringsByOrderIds(orderUserStatusToBeChanged.map(function(status) {
                    return status.ORDERID;
                }));

                sqlStrings = sqlStrings.concat(sqlOrderChangeStrings);

                this.requestHelper.executeStatementsAndCommit(sqlStrings, successCb.bind(null, true), errorCb.bind(this, new Error("AfterSyncOperations: Error while inserting User Status changes!")));
            };

            // 1. Fetch all orderUserStatus in error
            this._fetchOrderUserStatusInError(onOrderUserStatusLoaded.bind(this), errorCb.bind(this, new Error("AfterSyncOperations: Error while fetching Order User Status")));
        };

        AfterSyncOperations.prototype._fetchOrderOperations = function(successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var orderOperations = data.OrderOperations.map(function (operationData) {
                    var operationObject = new OrderOperation(operationData, that.requestHelper);

                    var operationUserStatus = data.OrderOperationStatus.filter(function (status) {
                        return status.OBJNR == operationData.OBJECT_NO;
                    });

                    operationObject.setUserStatus(operationUserStatus.map(function (statusData) {
                        return new OrderUserStatus(statusData, that.requestHelper);
                    }));

                    return operationObject;
                });

                successCb(orderOperations);
            };

            this.requestHelper.getData(["OrderOperations", "OrderOperationStatus"], {persNo: this.component.globalViewModel.model.getProperty("/personnelNumber")}, load_success, errorCb, true, "all");
        };

        AfterSyncOperations.prototype._fetchOrderUserStatusInError = function(successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var orderUserStatus = data.OrderOperationStatus.map(function (statusData) {
                    return new OrderUserStatus(statusData, that.requestHelper);
                });

                successCb(orderUserStatus);
            };

            this.requestHelper.getData(["OrderOperationStatus"], {persNo: this.component.globalViewModel.model.getProperty("/personnelNumber")}, load_success, errorCb, true, "inError");
        };

        AfterSyncOperations.prototype._getOrderChangedSqlStringsByOrderIds = function(orderIds) {
            var sqlChangeStrings = orderIds.map(function(id) {
                return {
                    type: "update",
                    mainTable: true,
                    tableName: "Orders",
                    values: [["ACTION_FLAG", "C"]],
                    replacementValues: [],
                    whereConditions: " ORDERID = '" + id + "'",
                    mainObjectValues: []
                };
            });

            return sqlChangeStrings;
        };

        AfterSyncOperations.prototype._getUserStatusMl = function (status, statusProfile) {
            var data = this.component.customizingModel.model.getProperty("/UserStatusMl");

            for (var i = 0; i < data.length; i++) {
                var stat = data[i];

                if (stat.STATUS_PROFILE == statusProfile && stat.USER_STATUS == status) {
                    return stat;
                }
            }

            return null;
        };

        AfterSyncOperations.prototype.constructor = AfterSyncOperations;

        return AfterSyncOperations;
    });