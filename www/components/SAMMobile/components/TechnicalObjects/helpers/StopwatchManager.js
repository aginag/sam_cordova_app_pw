sap.ui.define(["SAMMobile/components/TechnicalObjects/helpers/Stopwatch", "SAMMobile/models/dataObjects/CatsHeader"],

    function (Stopwatch, CatsHeader) {
        "use strict";

        // constructor
        function StopwatchManager(component) {
            this._LS_KEY = "SAM_STOPWATCHES";

            this.component = component;
            this.requestHelper = this.component.requestHelper;
            this.watches = [];

            if (localStorage.getItem(this._LS_KEY) === null) {
                localStorage.setItem(this._LS_KEY, JSON.stringify(this.watches));
            }

            this._initWatches();
        }

        StopwatchManager.prototype = {
            start: function (order, actType, successCb, errorCb) {
                // Check if there is a stop watch running for order.orderId currently
                // -> Yes? Stop it. -> Start new
                // -> No? Start new
   
                var that = this;
                var runningWatch = this.getStopwatchForOrderId(order.getOrderId());

                if (runningWatch) {
                    this.stop(order, function(catsHeader) {
                        that._start(order, actType);
                        successCb(catsHeader);
                    }, errorCb);
                } else {
                    this._start(order, actType);
                    successCb();
                }
            },

            stop: function (order, successCb, errorCb) {
                var that = this;
                var runningWatch = this.getStopwatchForOrderId(order.getOrderId());
                
                if (runningWatch) {
                    runningWatch.stop();

                    this._insertCatsEntry(order, runningWatch, function(catsHeader) {
                        that._removeWatch(runningWatch);
                        successCb(catsHeader);
                    }, errorCb);
                }
            },

            getStopwatchForOrderId: function (orderId) {
                var foundWatches = this.watches.filter(function(w) {
                    return w.orderId == orderId;
                });

                return foundWatches.length > 0 ? foundWatches[0] : null;
            }
        };

        StopwatchManager.prototype._insertCatsEntry = function(order, watch, successCb, errorCb) {
            var that = this;

            var onSuccess = function(catsHeader) {
                successCb(catsHeader);
            };

            var catsHeader = new CatsHeader(null, this.requestHelper);
            catsHeader.setHours(watch.elapsedHours);

            catsHeader.RAUFNR = watch.orderId;
            catsHeader.PERNR = watch.perNr;
            catsHeader.ACTIVITY = watch.activity;
            catsHeader.LSTAR = watch.activityType;

            try {
                catsHeader.insert(false, function(mobileId) {
                    catsHeader.MOBILE_ID = mobileId;
                    onSuccess(catsHeader);
                }, errorCb.bind(null, new Error("Error while trying to insert new component")));
            } catch(err) {
                errorCb(err);
            }
        };
   
       StopwatchManager.prototype._start = function (order, actType) {
          this.watches.push(this._getNewWatch(order, actType));
          this._saveLocalStorage();
       };
       
        StopwatchManager.prototype._initWatches = function () {
            this.watches = JSON.parse(localStorage.getItem(this._LS_KEY)).map(function(watchData) {
                var watch = new Stopwatch();
                watch.setData(watchData);
                return watch;
            });
        };

        StopwatchManager.prototype._saveLocalStorage = function () {
            localStorage.setItem(this._LS_KEY, JSON.stringify(this.watches));
        };

        StopwatchManager.prototype._getNewWatch = function(order, actType) {
            return new Stopwatch(order.getOrderId(), order.OPERATION_ACTIVITY, actType, order.PERS_NO);
        };

        StopwatchManager.prototype._removeWatch = function(watch) {
            for (var i = 0; i < this.watches.length; i++) {
                if (watch.id == this.watches[i].id) {
                    this.watches.splice(i, 1);
                    break;
                }
            }

            this._saveLocalStorage();
        };

        StopwatchManager.prototype.constructor = StopwatchManager;

        return StopwatchManager;
    }
);