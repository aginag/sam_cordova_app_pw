sap.ui.define([],

    function() {
        "use strict";

        // constructor
        function Formatter(component) {
            this.component = component;

            this.i18n = this.component.getModel("i18n").getResourceBundle();
            //this.i18n_core = this.component.getModel("i18n_core").getResourceBundle();
        }

        Formatter.prototype = {
            formatOperationUserStatusToIcon: function(status) {
                switch(status) {
                    case "E0001":
                        return "sap-icon://accept";
                    case "E0002":
                        return "sap-icon://decline";
                    case "E0006":
                        return "sap-icon://complete";
                    case "E0003":
                        return "sap-icon://begin";
                    case "E0004":
                        return "sap-icon://media-pause";
                    case "E0005":
                        return "sap-icon://factory";
                    case "E0007":
                        return "sap-icon://kpi-managing-my-area";
                    default:
                        return "sap-icon://circle-task";
                }
            },

            formatOperationUserStatusToText: function(status) {
                switch(status) {
                    case "E0001":
                        return "Accepted";
                    case "E0002":
                        return "Rejected";
                    case "E0006":
                        return "Completed";
                    case "E0003":
                        return "Enroute";
                    case "E0004":
                        return "On Hold";
                    case "E0005":
                        return "Onsite";
                    case "E0007":
                        return "Work Finished";
                    default:
                        return "Open";
                }
            }
        };

        return Formatter;
    }
);