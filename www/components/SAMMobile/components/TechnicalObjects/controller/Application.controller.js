sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("SAMMobile.components.TechnicalObjects.controller.Application", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("technicalObjectsCustomRoute").attachPatternMatched(this._onCustomRouteMatched, this);
        },

        _onCustomRouteMatched: function(oEvent) {
            var oArgs = oEvent.getParameter("arguments");
            var query = oArgs["?query"];
            var params;
            if(query.params){
                params = JSON.parse(query.params)
            }
            this.oRouter.navTo(query.routeName, params, true);
		}
	});

});