sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter"
    ],
    function(Controller, JSONModel, formatter) {
        "use strict";
        return Controller.extend("SAMMobile.components.TechnicalObjects.controller.detail.notificationHistory.NotificationHistoryTab", {
            formatter : formatter,

            onInit : function() {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("technicalObjectsNotificationHistory").attachPatternMatched(this._onRouteMatched, this);

                this.component = this.getOwnerComponent();
                this.globalViewModel = this.component.globalViewModel;

                this.technicalObjectDetailViewModel = null;
                this.notificationHistoryTabViewModel = null;
            },

            _onRouteMatched : function(oEvent) {
                var oArgs = oEvent.getParameter("arguments");

                this.globalViewModel.setComponentBackNavEnabled(true);
                this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


                if (!this.technicalObjectDetailViewModel) {
                    this.technicalObjectDetailViewModel = this.getView().getModel("technicalObjectDetailViewModel");
                    this.notificationHistoryTabViewModel = this.technicalObjectDetailViewModel.tabs.notificationHistory.model;

                    this.getView().setModel(this.notificationHistoryTabViewModel, "notificationHistoryTabViewModel");
                }

                // if (this.technicalObjectDetailViewModel.needsReload()) {
                //     return;
                // }

                this.loadOverviewTabData();
            },

            loadOverviewTabData: function() {
            	var that = this;
             //   if (this.notificationHistoryTabViewModel.needsReload()) {
                    this.loadViewModelData(function() {
                        that.globalViewModel.setComponentHeaderText(that.technicalObjectDetailViewModel.getHeaderText());
                      }); 
              //  }
            },

            onExit: function() {

            },

            onNavBack: function() {
                this.oRouter.navTo("technicalObjectsList");
                this.technicalObjectDetailViewModel.resetData();
            },

            onRefreshRoute: function() {
                this.loadViewModelData(true);
            },

            onNavToNotificationPress: function(oEvent) {
            	
   			 var bindingContext = oEvent.getSource().getBindingContext("notificationHistoryTabViewModel");
			 var notification = bindingContext.getProperty(bindingContext.sPath);
			 
                sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                    route: "technicalObjectsCustomRoute",
                    params: {
                        query: {
                            routeName: "technicalObjectsNotificationHistory",
                            params: JSON.stringify({
                                id: this.technicalObjectDetailViewModel.getObjectId(),
                                objectType: this.technicalObjectDetailViewModel.isEquipment() ? "0" : "1"
                            })
                        }
                    }
                }, {
                    route: "notificationsCustomRoute",
                    params: {
                        query: {
                            routeName: "notificationsViewOnlyRoute",
                            params: JSON.stringify({
                                id: notification.NOTIF_NO
                            })
                        }
                    }
                });
                //this.notificationHistoryTabViewModel.resetData();
            },
            
            loadViewModelData: function(bSync) {
                var that = this;
                bSync = bSync == undefined ? false : bSync;

                this.notificationHistoryTabViewModel.loadData(bSync);
            }
        });
    });
