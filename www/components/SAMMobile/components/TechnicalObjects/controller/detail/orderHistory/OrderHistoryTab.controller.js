sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter"
    ],
    function(Controller, JSONModel, formatter) {
        "use strict";
        return Controller.extend("SAMMobile.components.TechnicalObjects.controller.detail.orderHistory.OrderHistoryTab", {
            formatter : formatter,

            onInit : function() {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("technicalObjectsOrderHistory").attachPatternMatched(this._onRouteMatched, this);

                this.component = this.getOwnerComponent();
                this.globalViewModel = this.component.globalViewModel;

                this.technicalObjectDetailViewModel = null;
                this.orderHistoryTabViewModel = null;
            },

            _onRouteMatched : function(oEvent) {
                var oArgs = oEvent.getParameter("arguments");

                this.globalViewModel.setComponentBackNavEnabled(true);
                this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


                if (!this.technicalObjectDetailViewModel) {
                    this.technicalObjectDetailViewModel = this.getView().getModel("technicalObjectDetailViewModel");
                    this.orderHistoryTabViewModel = this.technicalObjectDetailViewModel.tabs.orderHistory.model;

                    this.getView().setModel(this.orderHistoryTabViewModel, "orderHistoryTabViewModel");
                }

                // if (this.technicalObjectDetailViewModel.needsReload()) {
                //     return;
                // }

                this.loadOverviewTabData();
            },

            loadOverviewTabData: function() {
                if (this.orderHistoryTabViewModel.needsReload()) {
                    this.loadViewModelData();
                    this.globalViewModel.setComponentHeaderText(this.technicalObjectDetailViewModel.getHeaderText());
                }
            },

            onExit: function() {

            },

            onNavBack: function() {
                this.oRouter.navTo("technicalObjectsList");
                this.technicalObjectDetailViewModel.resetData();
            },

            onRefreshRoute: function() {
                this.loadViewModelData(true);
            },

            loadViewModelData: function(bSync) {
                var that = this;
                bSync = bSync == undefined ? false : bSync;

                this.orderHistoryTabViewModel.loadData(function() {

                });
            }
        });
    });
