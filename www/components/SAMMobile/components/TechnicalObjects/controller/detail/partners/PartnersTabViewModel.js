sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel"],

    function (JSONModel, ViewModel) {
        "use strict";

        // constructor
        function PartnersTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        PartnersTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        partners: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        PartnersTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        PartnersTabViewModel.prototype.setModelData = function (data) {
            this.setProperty("/partners", data)
        };

        PartnersTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_PARTNER_TAB")));
            };

            if (!this.getObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_ID")));
                return;
            }

            this.setBusy(true);

            this.getObject().loadPartners(load_success, load_error);
        };

        PartnersTabViewModel.prototype.resetData = function () {
            this.setProperty("/partners", []);
            this.setObject(null);
        };

        PartnersTabViewModel.prototype.constructor = PartnersTabViewModel;

        return PartnersTabViewModel;
    });
