sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "SAMMobile/components/TechnicalObjects/controller/detail/TechnicalObjectDetailViewModel"
], function(Controller, MessageBox, TechnicalObjectDetailViewModel) {
    "use strict";

    return Controller.extend("SAMMobile.components.TechnicalObjects.controller.detail.Detail", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("technicalObjectsDetailRoute").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("technicalObjectsDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("technicalObjectMasterList", "onBeforeNavToDetail", this.onBeforeNavToDetail, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.technicalObjectDetailViewModel = new TechnicalObjectDetailViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.technicalObjectDetailViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.getView().setModel(this.technicalObjectDetailViewModel, "technicalObjectDetailViewModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            this.technicalObjectDetailViewModel.resetData();
            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            var technicalObjectId = oArgs.id;
            var objectType = oArgs.objectType;

            this.technicalObjectDetailViewModel.setObjectId(decodeURIComponent(technicalObjectId), objectType);

            if (this.technicalObjectDetailViewModel.needsReload()) {
                this.loadViewModelData(false, this.onAfterInitialLoad.bind(this, oArgs));
            }

        },

        onAfterInitialLoad: function(oArgs) {
            var route;

            if (oArgs["?query"] != undefined) {
                var tab = this.technicalObjectDetailViewModel.getTabByRouteName(oArgs["?query"].directTo);

                if (!tab) {
                    tab = this.technicalObjectDetailViewModel.tabs.overview;
                }

                route = tab.routeName;
                this.technicalObjectDetailViewModel.setSelectedTabKey(tab.actionName);
            } else {
                route = this.technicalObjectDetailViewModel.tabs.overview.routeName;
                this.technicalObjectDetailViewModel.setSelectedTabKey(this.technicalObjectDetailViewModel.tabs.overview.actionName);
            }

            this.oRouter.navTo(route, {
                id: oArgs.id,
                objectType: oArgs.objectType
            }, true);
        },

        onRefreshRoute: function () {
        },

        onLogout: function() {

        },
        onNewNotificationPressed: function(){
            var that = this;
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: that.technicalObjectDetailViewModel.getTechnicalObject()._ID,
                            objectType: that.technicalObjectDetailViewModel.getTechnicalObject()._TYPE 
                        })
                    }
                }
            }, {
                route: "notificationsCustomRoute",
                params: {
                    query: {
                        routeName: "notificationsCreationRoute",
                        params: JSON.stringify({
                            funclocId: that.technicalObjectDetailViewModel.getTechnicalObject()._ID,
                            funclocTxt: that.technicalObjectDetailViewModel.getTechnicalObject().SHTXT
                        })
                    }
                }
            });
        },

        loadViewModelData: function(bSync, successCb) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;


            var onReloadSuccess = function(order) {
                that.globalViewModel.setComponentHeaderText(that.technicalObjectDetailViewModel.getHeaderText());

                that.technicalObjectDetailViewModel.loadAllCounters();

                if (successCb) {
                    successCb();
                }
            };

            if (bSync) {
                this.technicalObjectDetailViewModel.refreshData(onReloadSuccess);
            } else {
                this.technicalObjectDetailViewModel.loadData(onReloadSuccess);
            }
        },

        onNavBack: function() {
            var that = this;

            setTimeout(function() {
                that.technicalObjectDetailViewModel.resetData();
            }, 200);

            window.history.go(-1);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("technicalObjectsDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("technicalObjectMasterList", "onBeforeNavToDetail", this.onBeforeNavToDetail, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onBeforeNavToDetail: function(channel, event, data) {
            if (!this.technicalObjectDetailViewModel) {
                return;
            }

            if (this.technicalObjectDetailViewModel._OBJECT_ID !== data.willNavToId) {
                this.technicalObjectDetailViewModel.resetData();
            }
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);

            if (err.name == "RETURN_TO_LIST") {
                this.onNavBack();
            }
        },

        onTabSelect: function() {

            var routeName = this.technicalObjectDetailViewModel.getTabRouteName();
            var objectId = this.technicalObjectDetailViewModel.getObjectId();
            var objectType = this.technicalObjectDetailViewModel._OBJECT_TYPE;

            this.oRouter.navTo(routeName, {
                id: encodeURIComponent(objectId),
                objectType: objectType
            }, true);
        }
    });

});