sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel"],

    function (JSONModel, ViewModel) {
        "use strict";

        // constructor
        function InstalledTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        InstalledTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        equipments: [],
                        searchString: "",
                        view: {
                            busy: false,
                            errorMessages: [],
                            growingThreshold: this.getGrowingListSettings().growingThreshold
                        }
                    };
                }
            },

            getQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        searchString: this.getSearchString(),
                        objectId: this.getObject().isEquipment() ? this.getObject().EQUNR : this.getObject().TPLNR
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        InstalledTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        InstalledTabViewModel.prototype.setModelData = function (data) {
            this.setProperty("/equipments", data)
        };

        InstalledTabViewModel.prototype.loadData = function (successCb, bGrowing) {
            var that = this;
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var load_success = function (data) {
                that.setBusy(false);
                that._needsReload = false;

                data = bGrowing ? that.getProperty("/equipments").concat(data) : data;

                that.setModelData(data);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_INSTALLED_TAB")));
            };

            if (!this.getObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_ID")));
                return;
            }

            this.setBusy(true);

            if (this.getObject().isEquipment()) {
                this._requestHelper.getData("EquipmentInstalled", this.getQueryParams(bGrowing ? this.getProperty("/equipments").length + 1 : 1), load_success, load_error, true);
            } else {
                this._requestHelper.getData("FunctionLocInstalled", this.getQueryParams(bGrowing ? this.getProperty("/equipments").length + 1 : 1), load_success, load_error, true);
            }
        };

        InstalledTabViewModel.prototype.resetData = function () {
            this.setProperty("/equipments", []);
            this.setObject(null);
        };

        InstalledTabViewModel.prototype.constructor = InstalledTabViewModel;

        return InstalledTabViewModel;
    });
