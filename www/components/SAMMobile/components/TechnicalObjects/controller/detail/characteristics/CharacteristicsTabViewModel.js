sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel"],

    function (JSONModel, ViewModel) {
        "use strict";

        // constructor
        function CharacteristicsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CharacteristicsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        characteristics: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        CharacteristicsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CharacteristicsTabViewModel.prototype.setModelData = function (data) {
            this.setProperty("/characteristics", data)
        };

        CharacteristicsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_CHARACTERISTICS_TAB")));
            };

            if (!this.getObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_ID")));
                return;
            }

            this.setBusy(true);

            this.getObject().loadCharacteristics(load_success, load_error);
        };

        CharacteristicsTabViewModel.prototype.resetData = function () {
            this.setProperty("/characteristics", []);
            this.setObject(null);
        };

        CharacteristicsTabViewModel.prototype.constructor = CharacteristicsTabViewModel;

        return CharacteristicsTabViewModel;
    });
