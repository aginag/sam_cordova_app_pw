sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter",
        "sap/ui/core/routing/History"
    ],
    function(Controller, JSONModel, formatter, History) {
        "use strict";
        return Controller.extend("SAMMobile.components.TechnicalObjects.controller.detail.overview.OverviewTab", {
            formatter : formatter,

            onInit : function() {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("technicalObjectsDetailOverview").attachPatternMatched(this._onRouteMatched, this);

                this.component = this.getOwnerComponent();
                this.globalViewModel = this.component.globalViewModel;

                this.technicalObjectDetailViewModel = null;
                this.overviewTabViewModel = null;
            },

            _onRouteMatched : function(oEvent) {
                var oArgs = oEvent.getParameter("arguments");

                this.globalViewModel.setComponentBackNavEnabled(true);
                this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


                if (!this.technicalObjectDetailViewModel) {
                    this.technicalObjectDetailViewModel = this.getView().getModel("technicalObjectDetailViewModel");
                    this.overviewTabViewModel = this.technicalObjectDetailViewModel.tabs.overview.model;

                    this.getView().setModel(this.overviewTabViewModel, "overviewTabViewModel");
                }

                if (this.technicalObjectDetailViewModel.needsReload()) {
                    return;
                }

                this.loadOverviewTabData();
            },

            loadOverviewTabData: function() {
                if (this.overviewTabViewModel.needsReload()) {
                    this.loadViewModelData();
                    this.globalViewModel.setComponentHeaderText(this.technicalObjectDetailViewModel.getHeaderText());
                }
            },

            onExit: function() {

            },

            onNavBack: function() {
                var that = this;
                var oHistory = History.getInstance();
                var sPreviousHash = oHistory.getPreviousHash();

                if(sPreviousHash.includes("notifications")){
                    this.oRouter.navTo("technicalObjectsList");
                }else{
                    window.history.go(-1);
                }

                setTimeout(function() {
                    that.technicalObjectDetailViewModel.resetData();
                }, 200); 
            },

            onRefreshRoute: function() {
                this.loadViewModelData(true);
            },

            loadViewModelData: function(bSync) {
                var that = this;
                bSync = bSync == undefined ? false : bSync;

                this.overviewTabViewModel.loadData(function() {

                });
            },

            onShowOnMapPressed: function() {
                this.overviewTabViewModel.onShowOnMapPressed();
            }
        });
    });
