sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel"],

    function (JSONModel, ViewModel) {
        "use strict";

        // constructor
        function OverviewTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        OverviewTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        OverviewTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        OverviewTabViewModel.prototype.onShowOnMapPressed = function () {
            var that = this;
            var location = this.getObject().getAddress();

            if (!location) {
                this.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_NO_GPS_COORDINATES_MAINTAINED_TEXT")));
                return;
            }

            if (this._component.rootComponent.navigator.availableApps.length === 1) {
                this._startNavigation(location, this._component.rootComponent.navigator.availableApps[0].app);
            } else {
                var dialog = that._component.rootComponent.navigator.getSelectionDialog(function (oEvent) {
                    var selectedItem = oEvent.getParameter("listItem");
                    var app = selectedItem.getBindingContext("undefined").getObject().app;
                    that._startNavigation(location, app);
                    dialog.close();
                });
            }

            dialog.open();
        };

        OverviewTabViewModel.prototype._startNavigation = function (address, app) {
            this._component.rootComponent.navigator.navTo(address, function () {
            }, function () {
            }, app);
        };

        OverviewTabViewModel.prototype.setModelData = function (data) {

        };

        OverviewTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_OVERVIEW_TAB")));
            };

            if (!this.getObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_ID")));
                return;
            }

            this.setBusy(true);

            load_success();
        };

        OverviewTabViewModel.prototype.resetData = function () {
            this.setObject(null);
        };

        OverviewTabViewModel.prototype.constructor = OverviewTabViewModel;

        return OverviewTabViewModel;
    });
