sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/helpers/formatter",
        "SAMMobile/models/dataObjects/Equipment",
        "SAMMobile/models/dataObjects/FunctionalLocation",
        "SAMMobile/components/TechnicalObjects/controller/detail/overview/OverviewTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/orderHistory/OrderHistoryTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/notificationHistory/NotificationHistoryTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/characteristics/CharacteristicsTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/measuringPoints/MeasuringPointsTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/installed/InstalledTabViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/partners/PartnersTabViewModel"],

    function (JSONModel,
              ViewModel,
              Validator,
              formatter,
              Equipment,
              FunctionalLocation,
              OverviewTabViewModel,
              OrderHistoryTabViewModel,
              NotificationHistoryTabViewModel,
              CharacteristicsTabViewModel,
              MeasuringPointsTabViewModel,
              InstalledTabViewModel,
              PartnersTabViewModel) {
        "use strict";

        // constructor
        function TechnicalObjectDetailViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;

            this._OBJECT_ID = "";
            this._OBJECT_TYPE = "";

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        TechnicalObjectDetailViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        object: null,
                        view: {
                            busy: false,
                            selectedTabKey: "overview",
                            viewOnly: false,
                            isEquipment: false
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId, objectType) {
                    this._OBJECT_ID = objectId;
                    this._OBJECT_TYPE = objectType;
                    this.setProperty("/view/isEquipment", this._OBJECT_TYPE != "0");
                    this.initTabModels(objectId, this._requestHelper, this._globalViewModel, this._component);
                }
            },

            getObjectId: {
                value: function () {
                    return this._OBJECT_ID;
                }
            },

            isEquipment: {
                value: function () {
                    return this._OBJECT_TYPE == "0";
                }
            },

            setTechnicalObject: {
                value: function (object) {
                    this.setProperty("/object", object);
                    this.setObjectOnTabModels(object);
                }
            },

            getTechnicalObject: {
                value: function () {
                    return this.getProperty("/object");
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        objectId: this.getObjectId(),
                        spras: this.getLanguage()
                    };
                }
            },

            getHeaderText: {
                value: function () {
                    return this.getTechnicalObject().isEquipment() ? this.getTechnicalObject().EQUNR : this.getTechnicalObject().TPLNR;
                }
            },

            setSelectedTabKey: {
                value: function (key) {
                    this.setProperty("/view/selectedTabKey", key);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            _getReturnToListError: {
                value: function (message) {
                    var error = new Error(message);
                    error.name = "RETURN_TO_LIST";

                    return error;
                }
            }
        });

        TechnicalObjectDetailViewModel.prototype.tabs = {
            overview: {
                actionName: "overview",
                routeName: "technicalObjectsDetailOverview",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new OverviewTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            characteristics: {
                actionName: "characteristics",
                routeName: "technicalObjectDetailCharacteristics",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new CharacteristicsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            notificationHistory: {
                actionName: "notificationHistory",
                routeName: "technicalObjectsNotificationHistory",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new NotificationHistoryTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            orderHistory: {
                actionName: "orderHistory",
                routeName: "technicalObjectsOrderHistory",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new OrderHistoryTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            measuringPoints: {
                actionName: "measuringPoints",
                routeName: "technicalObjectDetailMeasuringPoints",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new MeasuringPointsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            installed: {
                actionName: "installed",
                routeName: "technicalObjectDetailInstalled",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new InstalledTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            partners: {
                actionName: "partners",
                routeName: "technicalObjectDetailPartners",
                model: null,
                initModel: function (objectId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new PartnersTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setObjectId(objectId);
                }
            },

            attachments: {
                actionName: "attachments",
                routeName: "technicalObjectDetailAttachments",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    // if (!this.model) {
                    //     this.model = new AttachmentTabViewModel(reqHelper, globalViewModel, component);
                    //     this.model.setErrorCallback(errorCallback);
                    // }
                    // this.model.setOrderId(orderId);
                }
            }
        };

        TechnicalObjectDetailViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        TechnicalObjectDetailViewModel.prototype.getTabRouteName = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            return tabInfo.routeName;
        };

        TechnicalObjectDetailViewModel.prototype.setNewObjectData = function (data) {
            var object = null;

            if (this.isEquipment()) {
                object = new Equipment(data, this._requestHelper, this);
            } else {
                object = new FunctionalLocation(data, this._requestHelper, this);
            }

            this.setTechnicalObject(object);

            //this.tabs.overview.model.setDataFromOrder(this.getOrder());
        };

        TechnicalObjectDetailViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                if (data.length == 0) {
                    that.executeErrorCallback(that._getReturnToListError(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_DATA")));
                    return;
                }

                that.setNewObjectData(data[0]);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb(that.getTechnicalObject());
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(that._getReturnToListError(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA")));
            };

            this.setBusy(true);

            if (this.isEquipment()) {
                this._requestHelper.getData("Equipments", this.getQueryParams(), load_success, load_error, true);
            } else {
                this._requestHelper.getData("FunctionalLocations", this.getQueryParams(), load_success, load_error, true);
            }
        };

        TechnicalObjectDetailViewModel.prototype.resetData = function () {
            this.setTechnicalObject(null);
            this._OBJECT_ID = "";
            this._OBJECT_TYPE = "";
            this._needsReload = true;
            this.resetTabModels();
        };

        TechnicalObjectDetailViewModel.prototype.refreshData = function (successCb) {
            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];


            // Mark all other tabs as needsReload = true
               for (var key in this.tabs) {
                if (key !== selectedTabKey) {
                    that.tabs[key].model._needsReload = true;
                }
            }

            //this.orderUserStatus = this.scenario.getOrderUserStatus();
            this.loadData(function () {
                // Publish refresh event for current active tab
                sap.ui.getCore()
                    .getEventBus()
                    .publish(tabInfo.routeName, "refreshRoute");

                successCb();
            });
        };

        TechnicalObjectDetailViewModel.prototype.initTabModels = function () {
            for (var key in this.tabs) {
                this.tabs[key].initModel(this.getObjectId(), this._requestHelper, this._globalViewModel, this._component, this._errorCallback);
            }
        };

        TechnicalObjectDetailViewModel.prototype.setObjectOnTabModels = function (object) {
            for (var key in this.tabs) {
                var tab = this.tabs[key];
                if (tab.model) {
                    tab.model.setObject(object);
                }
            }
        };

        TechnicalObjectDetailViewModel.prototype.resetTabModels = function () {
            for (var key in this.tabs) {
                if (!this.tabs[key].model) {
                    continue;
                }

                this.tabs[key].model._needsReload = true;
                this.tabs[key].model.resetData();
            }
        };

        TechnicalObjectDetailViewModel.prototype.getTabByRouteName = function (routeName) {
            for (var key in this.tabs) {
                if (this.tabs[key].routeName == routeName) {
                    return this.tabs[key];
                }
            }

            return null;
        };

        TechnicalObjectDetailViewModel.prototype.loadAllCounters = function (successCb) {
            var that = this;
            var load_success = function (data) {

                var iCounterNotifHistoryP1 = Number(data[0].CounterNotifHistoryP1[0].COUNT) > 3 ? 3 : Number(data[0].CounterNotifHistoryP1[0].COUNT),
                    iCounterNotifHistoryZ1 = Number(data[1].CounterNotifHistoryZ1[0].COUNT) > 1 ? 1 : Number(data[1].CounterNotifHistoryZ1[0].COUNT);

                that.setProperty("/counterNotifHistory", iCounterNotifHistoryP1 + iCounterNotifHistoryZ1);
                that.setProperty("/counterCharacteristics", data[2].CounterCharacteristics[0].COUNT);
                that.setProperty("/counterOrderHistory", data[3].CounterOrderHistory[0].COUNT);
                that.setProperty("/counterMeasuringPoints", data[4].CounterMeasuringPoints[0].COUNT);
                that.setProperty("/counterPartner", data[5].CounterPartner[0].COUNT);
                that.setProperty("/counterInstalled", data[6].CounterInstalled[0].COUNT);

                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_COUNTER")));
            };

            if (!this.getTechnicalObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_DATA")));
            }

            this.setBusy(true);
            this.getTechnicalObject().loadCounters(load_success);
        };

        TechnicalObjectDetailViewModel.prototype.constructor = TechnicalObjectDetailViewModel;

        return TechnicalObjectDetailViewModel;
    });
