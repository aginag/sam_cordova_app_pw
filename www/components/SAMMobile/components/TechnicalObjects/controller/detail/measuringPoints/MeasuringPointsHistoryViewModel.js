sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel"],

    function (JSONModel, ViewModel) {
        "use strict";

        // constructor
        function MeasuringPointsHistoryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MeasuringPointsHistoryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        points: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setDocuments: {
                value: function(documents){
                    this._DOCUMENTS = documents;
                }
            },

            getDocuments: {
                value: function () {
                    return this._DOCUMENTS;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        MeasuringPointsHistoryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MeasuringPointsHistoryViewModel.prototype.setModelData = function (data){
            //this.setDocuments(data);
            this.setProperty("/documents", data);
        };

        MeasuringPointsHistoryViewModel.prototype.loadData = function (point, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_MEASUREMENT_POINTS_TAB")));
            };

            this.setBusy(true);
            this._requestHelper.getData("MeasurementDocuments", {
                point: point
            }, load_success, load_error, true);
        };

        MeasuringPointsHistoryViewModel.prototype.constructor = MeasuringPointsHistoryViewModel;

        return MeasuringPointsHistoryViewModel;
    });
