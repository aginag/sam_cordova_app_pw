sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/models/dataObjects/MeasurementDocument"
        ],

    function (JSONModel, ViewModel, MeasurementDocument) {
        "use strict";

        // constructor
        function MeasuringPointsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;
            this._DOCUMENTS = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MeasuringPointsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        points: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        MeasuringPointsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MeasuringPointsTabViewModel.prototype.setModelData = function (data) {
            this.setProperty("/points", data);
        };

        MeasuringPointsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_MEASUREMENT_POINTS_TAB")));
            };

            if (!this.getObject()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_GENERIC_LOAD_DATA_NO_ID")));
                return;
            }

            this.setBusy(true);

            this.getObject().loadMeasuringPoints(load_success, load_error);
        };

        MeasuringPointsTabViewModel.prototype.resetData = function () {
            this.setProperty("/points", []);
            this.setObject(null);
        };

        MeasuringPointsTabViewModel.prototype.insertNewMeasurementDocument = function (document, point, successCb) {
            var that = this;
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            try {
                document.CODGR = point.CODGR;
                document.RECDV = point.OWN_MEASUREMENTPOINT;
                document.MDTXT = point.MEASUREMENT_DESC;
            } catch(error){

            }

           var onSuccess = function (asd) {
                successCb();
            };

            try {
                document.insert(false, function (mobileId) {
                    document.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_INSERT_NEW_MEASUREMENT_POINT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MeasuringPointsTabViewModel.prototype.getNewMeasurementDocument = function (point) {

            var newMeasurementDocumentData = new MeasurementDocument(null, this._requestHelper, this);

            newMeasurementDocumentData.POINT = point.MEASUREMENT_POINT;
            newMeasurementDocumentData.RECDV = null;
            newMeasurementDocumentData.ERDAT = moment(new Date()).format("YYYY-MM-DD HH:mm:ss.SSS");
            newMeasurementDocumentData.CODGR = point.CODGR;
            newMeasurementDocumentData.RECDU = point.MSEHT;
            newMeasurementDocumentData.MDTXT = null;
            newMeasurementDocumentData.ERNAM = "" + this.getUserInformation().firstName + " " + this.getUserInformation().lastName ;

            return newMeasurementDocumentData;
        };

        MeasuringPointsTabViewModel.prototype.constructor = MeasuringPointsTabViewModel;

        return MeasuringPointsTabViewModel;
    });
