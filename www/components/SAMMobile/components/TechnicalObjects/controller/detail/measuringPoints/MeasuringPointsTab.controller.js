sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "SAMMobile/components/TechnicalObjects/controller/detail/measuringPoints/EditMeasuringPointsViewModel",
        "SAMMobile/components/TechnicalObjects/controller/detail/measuringPoints/MeasuringPointsHistoryViewModel",
        "sap/m/Dialog",
        "sap/m/Button",
        "sap/m/Text",
        "sap/m/TextArea",
        "sap/m/MessageToast",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter"
    ],
    function (Controller, EditMeasuringPointsViewModel, MeasuringPointsHistoryViewModel, Dialog, Button, Text, TextArea, MessageToast, JSONModel, formatter) {
        "use strict";
        return Controller.extend("SAMMobile.components.TechnicalObjects.controller.detail.measuringPoints.MeasuringPointsTab", {
            formatter: formatter,

            onInit: function () {
                this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                this.oRouter.getRoute("technicalObjectDetailMeasuringPoints").attachPatternMatched(this._onRouteMatched, this);

                this.component = this.getOwnerComponent();
                this.globalViewModel = this.component.globalViewModel;
                this.editMeasuringPointsViewModel = new EditMeasuringPointsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
                this.measuringPointsHistoryViewModel = new MeasuringPointsHistoryViewModel(this.component.requestHelper, this.globalViewModel, this.component);

                this.technicalObjectDetailViewModel = null;
                this.measuringPointsTabViewModel = null;
            },

            _onRouteMatched: function (oEvent) {
                var oArgs = oEvent.getParameter("arguments");

                this.globalViewModel.setComponentBackNavEnabled(true);
                this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
                this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

                if (!this.technicalObjectDetailViewModel) {
                    this.technicalObjectDetailViewModel = this.getView().getModel("technicalObjectDetailViewModel");
                    this.measuringPointsTabViewModel = this.technicalObjectDetailViewModel.tabs.measuringPoints.model;

                    this.getView().setModel(this.measuringPointsTabViewModel, "measuringPointsTabViewModel");
                }

                this.loadOverviewTabData();

                if (this.technicalObjectDetailViewModel.needsReload()) {
                    this.loadViewModelData();
                    //return;
                }
            },

            loadOverviewTabData: function () {
                if (this.measuringPointsTabViewModel.needsReload()) {
                    this.loadViewModelData();
                    this.globalViewModel.setComponentHeaderText(this.technicalObjectDetailViewModel.getHeaderText());
                }
            },

            onExit: function () {

            },

            onNavBack: function () {
                this.oRouter.navTo("technicalObjectsList");
                this.technicalObjectDetailViewModel.resetData();
            },

            onRefreshRoute: function () {
                this.loadViewModelData(true);
            },

            loadViewModelData: function (bSync) {
                var that = this;
                bSync = bSync == undefined ? false : bSync;

                this.measuringPointsTabViewModel.loadData(function () {

                });
            },

            onCreateNewMeasurementDocument: function (oEvent) {

                var that = this;
                var point;

                var sPath = oEvent.getSource().getBindingContext("measuringPointsTabViewModel").sPath;

                try {
                    point = oEvent.getSource().getBindingContext("measuringPointsTabViewModel").getObject().MeasuringPoints[sPath.split("/").pop()];
                } catch (err) {
                }

                var onAcceptPressed = function (document, dialog) {
                    that.measuringPointsTabViewModel.insertNewMeasurementDocument(document, point, function () {
                        dialog.close();
                        MessageToast.show("Inserted new Measurement Document successfully");
                    });
                };
                this._getPointInsertDialog(that.measuringPointsTabViewModel.getNewMeasurementDocument(point), point, onAcceptPressed).open();

            },

            _getPointInsertDialog: function (document, point, acceptCb) {
                var that = this;
                var onAcceptPressed = function () {
                    var measuringPointsTabViewModel = this.getParent().getModel("measuringModel");
                    if (measuringPointsTabViewModel.validate()) {
                        acceptCb(measuringPointsTabViewModel.getDocument(), this.getParent());
                    }
                };

                var onCancelPressed = function () {
                    var dialog = this.getParent();
                    var measuringPointsTabViewModel = dialog.getModel("measuringModel");
                    measuringPointsTabViewModel.rollbackChanges();
                    dialog.close();
                };

                var object = this.measuringPointsTabViewModel.getObject();

                this.editMeasuringPointsViewModel = new EditMeasuringPointsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
                this.editMeasuringPointsViewModel.setObject(object);
                this.editMeasuringPointsViewModel.setPoint(point);
                this.editMeasuringPointsViewModel.setDocument(document);

                if (!this._pointInsertPopover) {

                    if (!this._pointInsertPopover) {
                        this._pointInsertPopover = sap.ui.xmlfragment("pointInsertPopover", "SAMMobile.components.TechnicalObjects.view.detail.measuringPoints.MeasuringInsertPopover", this);
                    }

                    this._pointInsertPopover = new Dialog({
                        stretch: sap.ui.Device.system.phone,
                        title: "New MeasurementDocument",
                        contentWidth: "60%",
                        content: this._pointInsertPopover,
                        beginButton: new Button({
                            text: '{i18n_core>ok}',
                            press: onAcceptPressed
                        }),
                        endButton: new Button({
                            text: '{i18n_core>cancel}',
                            press: onCancelPressed
                        }),
                        afterClose: function () {
                        }
                    });
                    this.getView().addDependent(this._pointInsertPopover);
                }

                this._pointInsertPopover.getBeginButton().mEventRegistry.press = [];
                this._pointInsertPopover.getBeginButton().attachPress(onAcceptPressed);

                this._pointInsertPopover.setModel(this.editMeasuringPointsViewModel, "measuringModel");
                return this._pointInsertPopover;
            },

            onShowMeasurementDocumentHistory: function (oEvent) {

                var that = this;
                var point;
                var sPath = oEvent.getSource().getBindingContext("measuringPointsTabViewModel").sPath;

                try {
                    point = oEvent.getSource().getBindingContext("measuringPointsTabViewModel").getObject().MeasuringPoints[sPath.split("/").pop()];
                    this.measuringPointsHistoryViewModel.loadData(point.MEASUREMENT_POINT);
                } catch (err) {

                }

                var onCancelPressed = function () {
                    var dialog = this.getParent();
                    dialog.close();
                };

                this.pressDialog = null;

                if (!this.pressDialog) {

                    this.pressDialog = sap.ui.xmlfragment("measuringDocHistory", "SAMMobile.components.TechnicalObjects.view.detail.measuringPoints.MeasuringDocumentsHistoryPopup", this);

                    this.pressDialog = new Dialog({
                        stretch: sap.ui.Device.system.phone,
                        title: "{i18n_core>TOMeasDocHistory}",
                        contentWidth: "80%",
                        contentHeight: "80%",
                        content: this.pressDialog,
                        endButton: new Button({
                            text: '{i18n_core>close}',
                            press: onCancelPressed
                        }),
                        afterClose: function () {
                        }
                    });
                    this.getView().addDependent(this.pressDialog);
                }

                this.measuringPointsHistoryViewModel.loadData(point.MEASUREMENT_POINT, function () {
                    that.pressDialog.setModel(that.measuringPointsHistoryViewModel, "documentModel");
                    that.pressDialog.open();
                });

            },

            displayCodeGroupSelectDialog: function (oEvent) {
                this.editMeasuringPointsViewModel.displayCodeGroupDialog(oEvent, this);
            },

            handleCodeGroupSelect: function (oEvent) {
                this.editMeasuringPointsViewModel.handleCodeGroupSelect(oEvent);
            },

            handleCodeGroupSearch: function (oEvent) {
                this.editMeasuringPointsViewModel.handleCodeGroupSearch(oEvent);
            }

        });
    });
