sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/MeasurementDocument"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, MeasurementDocument) {
        "use strict";

        // constructor
        function EditMeasuringViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditMeasuringViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        document: null,
                        point: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setDocument : {
                value: function (document) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = document.getCopy();

                    }

                    this.setProperty("/document", document);
                }
            },

            getDocument: {
                value: function () {
                    return this.getProperty("/document");
                }
            },

            setPoint: {
                value: function (point) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = point.getCopy();

                    }

                    this.setProperty("/point", point);
                }
            },

            getPoint: {
                value: function () {
                    return this.getProperty("/point");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getDocument().setData(this._objectDataCopy);
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditMeasuringViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };


        EditMeasuringViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation of the measurementpoint
            validator.check("point", "Measurepoint have to be between the limits!").custom(function(value) {
                var ownMeasurementPoint = parseFloat(value.OWN_MEASUREMENTPOINT);
                var minAcceptedValue = parseFloat(value.MRMIN);
                var maxAcceptedValue = parseFloat(value.MRMAX);

                if (minAcceptedValue == 0 && maxAcceptedValue == 0) {
                    return true;
                }

                if (ownMeasurementPoint > maxAcceptedValue || ownMeasurementPoint < minAcceptedValue) {
                    return false;
                }

                return true;

            });
            validator.check("point.OWN_MEASUREMENTPOINT", "Reading has to be a Number!").isNumber();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };


        EditMeasuringViewModel.prototype.displayCodeGroupDialog = function(oEvent, viewContext){
            if (!this._oMeasuringCodeGroupDialog) {
                this._oMeasuringCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oMeasuringCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: "QM0000001",
                    catalogType: "D"
                },
                actionName: "valueHelp"
            });
        };

        EditMeasuringViewModel.prototype.handleCodeGroupSelect = function (oEvent) {
            this._oMeasuringCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'CODGR': 'CODEGROUP'
            }], this, "/point");
        };

        EditMeasuringViewModel.prototype.handleCodeGroupSearch = function (oEvent) {
            this._oMeasuringCodeGroupDialog.search(oEvent, ["CODGR"]);
        };

        EditMeasuringViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    ACTION_FLAG: sap.ui.core.ValueState.None,
                    MEASUREMENT_DESC: sap.ui.core.ValueState.None,
                    MEASUREMENT_POINT: sap.ui.core.ValueState.None,
                    MPTYP: sap.ui.core.ValueState.None,
                    MSEHT: sap.ui.core.ValueState.None,
                    MRMIN: sap.ui.core.ValueState.None,
                    MRMAX: sap.ui.core.ValueState.None,
                    PYEAR: sap.ui.core.ValueState.None,
                    CODGR: sap.ui.core.ValueState.None,
                    MOBILE_ID: sap.ui.core.ValueState.None,
                });
            }
            return this.valueStateModel;
        };

        EditMeasuringViewModel.prototype.constructor = EditMeasuringViewModel;

        return EditMeasuringViewModel;
    });
