sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
    "SAMMobile/models/dataObjects/Equipment",
    "SAMMobile/models/dataObjects/FunctionalLocation"],

    function (ViewModel, Filter, Equipment, FunctionalLocation) {
        "use strict";

        // constructor
        function TechnicalObjectListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/objects";

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        TechnicalObjectListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        objects: [],
                        selectedObject: null,
                        searchString: "",
                        counts: {
                            equipments: 0,
                            functionalLocations: 0
                        },
                        view: {
                            busy: false,
                            selectedTabKey: "equipments",
                            orderSelected: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold
                        }
                    };
                }
            },

            getSelectedObject: {
                value: function () {
                    return this.getProperty("/selectedObject");
                }
            },

            getOrderQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        searchString: this.getSearchString()
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function() {}, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function() {
                    return "TECHNICAL OBJECTS";
                }
            }
        });

        TechnicalObjectListViewModel.prototype.tabs = {
            equipments: {
                actionName: "equipments",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/equipments", this.getProperty("/counts/equipments") + value);
                }
            },

            functionalLocations: {
                actionName: "functionalLocations",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/functionalLocations", this.getProperty("/counts/functionalLocations") + value);
                }
            }
        };

        TechnicalObjectListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        TechnicalObjectListViewModel.prototype.onTabChanged = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // MODEL SEARCH
            // Reset binding and resetting searchValue when switching tabs
            this.setProperty("/searchString", "");
            this.getList().getBinding("items").filter(null);

            this.resetGrowingListDelegate();

            if (tabInfo.needsReload || tabInfo.lastSearch != this.getSearchString()) {
                this.fetchData();
            } else {
                this.setNewData(tabInfo.cached);
            }
            
        };

        TechnicalObjectListViewModel.prototype.onSearch = function (oEvt) {
            // DATABASE SEARCH
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);

            // MODEL SEARCH
            // var aFilters = [];
            // var sValue = oEvt.getSource().getValue();
            //
            // if (sValue && sValue.length > 0) {
            //     // aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("SHORT_TEXT", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("FUNCLOC_DESC", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("NOTIF_TYPE", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("_START_DATE", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("_END_DATE", sap.ui.model.FilterOperator.Contains, sValue));
            //     // aFilters.push(new Filter("OPERATION_USER_STATUS_TEXT", sap.ui.model.FilterOperator.Contains, sValue)); //OPERATION_USER_STATUS_TEXT
            //     var allFilter = new Filter(aFilters);
            // }
            //
            // var oBinding = this.getList().getBinding("items");
            // oBinding.filter(allFilter);
        };

        TechnicalObjectListViewModel.prototype.setSelectedOrder = function (order) {
            // var selectedTabKey = this.getProperty("/view/selectedTabKey");
            //
            // if (selectedTabKey == this.tabs.complete.actionName || selectedTabKey == this.tabs.rejected.actionName) {
            //     order = null;
            // }
            //
            // this.setProperty("/selectedObject", order);
            // this.setProperty("/view/orderSelected", !order || order == undefined ? false : true);
            //
            // this.setStatusButtonsForStatus(!order ? "" : order.OPERATION_USER_STATUS, !order ? "" : order.OPERATION_STATUS_PROFILE);
            //
            // if (!this.getProperty("/view/orderSelected")) {
            //     this.getList().removeSelections();
            // }
        };

        TechnicalObjectListViewModel.prototype.setNewData = function (data) {
            var that = this;
            var newData = [];

            data.forEach(function (technicalObjectsData) {

                if (that.getProperty("/view/selectedTabKey") == that.tabs.equipments.actionName) {
                    newData.push(new Equipment(technicalObjectsData, that._requestHelper, that));
                } else {
                    newData.push(new FunctionalLocation(technicalObjectsData, that._requestHelper, that));
                }

            });

            this.setProperty("/objects", newData);
            this.setSelectedOrder(null);

        };

        TechnicalObjectListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                tabInfo.needsReload = false;
                tabInfo.lastSearch = that.getSearchString();
                tabInfo.cached = bGrowing ? tabInfo.cached.concat(data) : data;

                that.setNewData(tabInfo.cached);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching orders"));
            };

            this.setBusy(true);
            this._requestHelper.getData("TechnicalObjects", this.getOrderQueryParams(bGrowing ? this.getProperty("/objects").length + 1 : 1), load_success, load_error, true, tabInfo.actionName);
        };

        TechnicalObjectListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        TechnicalObjectListViewModel.prototype.resetModel = function () {
            for (var key in this.tabs) {
                this.tabs[key].needsReload = true;
            }
        };

        TechnicalObjectListViewModel.prototype.fetchCounts = function () {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/counts/equipments", parseInt(data.Equipments[0].count));
                that.setProperty("/counts/functionalLocations", parseInt(data.FunctionalLocations[0].count));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching tab counts"));
            };

            this._requestHelper.getData(["Equipments", "FunctionalLocations"], {}, load_success, load_error, true, "count");
        };

        TechnicalObjectListViewModel.prototype.constructor = TechnicalObjectListViewModel;

        return TechnicalObjectListViewModel;
    });
