sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/TechnicalObjects/controller/list/TechnicalObjectListViewModel",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "SAMMobile/controller/common/FuncLocHirarchyViewModel",
], function(Controller, TechnicalObjectListViewModel, MessageBox, formatter, FuncLocHirarchyViewModel) {
    "use strict";

    return Controller.extend("SAMMobile.components.TechnicalObjects.controller.list.List", {
        formatter: formatter,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("technicalObjectsList").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("technicalObjectsList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            //set model for technical objects            
            this.funcLocTreeViewModel = new FuncLocHirarchyViewModel(this.component.requestHelper, this.globalViewModel, this.component, false );
            this.funcLocTreeViewModel.setFLParent('');
            this.funcLocTreeViewModel.setTree(this.byId("TreeObjects"));
            this.funcLocTreeViewModel.setButtons(this.byId("tree-buttons"));
            this.funcLocTreeViewModel.setHierarchy(true);
            this.funcLocTreeViewModel.setToolbar(true);
            this.initialized = false;
            this.getView().setModel(this.funcLocTreeViewModel, "funcLocTreeViewModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;
            var onMasterDataLoaded = function(routeName) {
                this.globalViewModel.setCurrentRoute(routeName);

                if (this.funcLocTreeViewModel.needsReload()) {
                    this.loadObjects();
                }

                if (!this.initialized) {
                    this.initialized = true;
                }
            };

            this.globalViewModel.setComponentHeaderText(this.component.getModel('i18n_core').getProperty("TObjects"));
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            var onReloadSuccess = function() {

            };

            this.funcLocTreeViewModel.refreshData();

        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {

        },

        onLogout: function() {
            this.initialized = false;
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("workOrdersRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onButtonPress: function (oEvent) {
        	this.funcLocTreeViewModel.onButtonPress(oEvent);
		},
        onNewNotificationPressed: function() {

             sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsList",
                        params: JSON.stringify({

                        })
                    }
                }
            }, {
                route: "notificationsCustomRoute",
                params: {
                    query: {
                        routeName: "notificationsCreationRoute",
                        params: JSON.stringify({
                            funclocId: this.funcLocTreeViewModel.getSelectedFuncloc().id,
                            funclocTxt: this.funcLocTreeViewModel.getSelectedFuncloc().text
                        })
                    }
                }
            });

        },
		
		handleNavPress: function (oEvent) {

          var bindingContext = oEvent.getSource().getBindingContext("funcLocTreeViewModel");
          var technicalObject = bindingContext.getObject();

          sap.ui.getCore().getEventBus().publish("technicalObjectMasterList", "onBeforeNavToDetail", {
              willNavToId: technicalObject.id
          });

          this.oRouter.navTo("technicalObjectsDetailRoute", {
              id: encodeURIComponent(technicalObject.id),
              objectType: technicalObject.type == 'E' ? "0" : "1"
          }, false);
		},
		
		onTreeSelection: function (oEvent) {
        	this.funcLocTreeViewModel.onTreeSelection(oEvent);
		},
		
		onTreeSearch: function (oEvent) {
        	this.funcLocTreeViewModel.onTreeSearch(oEvent);
		},
        
    });

});