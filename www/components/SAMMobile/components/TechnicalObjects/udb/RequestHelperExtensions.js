sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                EquipmentJoinsMl: ["e.MOBILE_ID", "e.EQUNR", "e.TPLNR", "eml.TPLNR_SHTXT AS FUNCLOC_DISP", "e.EQUIPMENT_DISPLAY", "e.EQTYP", "e.EQART", "e.OBJNR", "e.MATNR", "e.WERK", "e.LAGER", "eml.SHTXT", "eml.TPLNR_SHTXT",
                    "e.TPLMA", "e.TPLMA_SHTXT", "e.TIDNR", "e.FLTYP", "e.HERST", "e.SERNR", "e.STREET", "e.SWERK", "e.HEQUI", "e.HEQUI_SHTXT", "e.SUBMT", "e.RBNR", "e.POST_CODE1",
                    "e.CITY1", "e.COUNTRY", "e.BAUJJ", "e.BAUMM", "e.TYPBZ", "e.SERGE", "e.ACTION_FLAG", "ec.TYPTX"],
                EquipmentJoinsEquipmentCategory: ["e.MOBILE_ID", "e.EQUNR", "e.EQUIPMENT_DISPLAY", "e.TPLNR", "e.FUNCLOC_DISP", "e.EQTYP", "e.EQART", "e.OBJNR", "e.MATNR", "e.WERK", "e.LAGER", "eml.SHTXT", "eml.TPLNR_SHTXT", "e.TPLMA_SHTXT", "e.SWERK",
                    "e.TIDNR", "e.FLTYP", "e.SERNR", "e.STREET", "e.POST_CODE1", "e.CITY1", "e.COUNTRY", "e.WKCTR", "e.ACTION_FLAG", "ec.TYPTX"],
                EquipmentInstalled: ["MOBILE_ID", "EQUNR", "SHTXT", "EQTYP", "HEQUI", "HEQUI_SHTXT", "SERNR", "BAUJJ", "BAUMM", "TPLNR", "WERK", "ACTION_FLAG"],
                FunctionLocJoinsMl: ["f.MOBILE_ID", "f.EQUNR", "f.TPLNR", "f.FUNCLOC_DISP", "f.EQTYP", "f.EQART", "f.OBJNR", "f.MATNR", "f.WERK", "f.LAGER", "f.SHTXT", "fml.SHTXT AS TPLNR_SHTXT",
                    "fml.TPLMA_SHTXT", "f.FLTYP", "f.SERNR", "f.STREET", "f.POST_CODE1", "f.SWERK", "f.HERLD", "f.TPLMA", "f.CITY1", "f.COUNTRY", "f.ACTION_FLAG", "ec.TYPTX", "f.HERST", "f.BAUJJ"],
                FunctionLocJoinsEqCategory: ["f.MOBILE_ID", "f.EQUNR", "f.TPLNR", "f.FUNCLOC_DISP", "f.EQTYP", "f.EQART", "f.OBJNR", "f.MATNR", "f.WERK", "f.SWERK", "f.LAGER", "fml.SHTXT", "f.TPLNR_SHTXT", "fml.TPLMA_SHTXT",
                    "f.SWERK", "f.FLTYP", "f.SERNR", "f.STREET", "f.POST_CODE1", "f.CITY1", "f.COUNTRY", "f.WKCTR", "f.ACTION_FLAG", "ec.TYPTX"],
                FunctionLocInstalled: ["MOBILE_ID", "EQUNR", "HEQUI", "HEQUI_SHTXT", "EQTYP", "SERNR", "BAUJJ", "BAUMM", "TPLNR", "OBJNR", "WERK", "ACTION_FLAG"],
                MeasurementDocuments: ["MOBILE_ID", "ERDAT", "ERNAM", "READG","POINT", "RECDV", "RECDU", "MDTXT", "CODGR"],
                EquipmentHierarchy: ["EQUNR", "SHTXT", "TPLNR", "HEQUI"],
                FunclocHierarchy: ["TPLNR", "SHTXT", "TPLMA"],
            },
            queries: {

                TechnicalObjects: {
                    equipments: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM Equipment AS e LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = e.EQTYP AND ec.SPRAS = '$3' " +
                            "LEFT JOIN EquipmentMl AS eml ON eml.EQUNR = e.EQUNR AND eml.SPRAS = '$10' WHERE "
                            + "UPPER(e.SHTXT) LIKE UPPER('%$4%') OR UPPER(e.TPLNR_SHTXT) LIKE UPPER('%$5%') OR UPPER(e.TPLMA_SHTXT) LIKE UPPER('%$6%') OR e.EQUNR LIKE '%$7%' OR UPPER(e.TIDNR) LIKE UPPER('%$8%') "
                            + "OR UPPER(ec.TYPTX) LIKE UPPER('%$9%')  ORDER BY e.EQUNR DESC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "spras"
                        }],
                        columns: "EquipmentJoinsEquipmentCategory"
                    },

                    functionalLocations: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM FunctionLoc AS f LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = f.FLTYP AND ec.SPRAS = '$3' " +
                            "LEFT JOIN FunctionLocMl AS fml ON fml.TPLNR = f.TPLNR AND fml.SPRAS = '$10' WHERE "
                            + "UPPER(f.SHTXT) LIKE UPPER('%$4%') OR UPPER(f.TPLNR_SHTXT) LIKE UPPER('%$5%') OR UPPER(f.TPLMA_SHTXT) LIKE UPPER('%$6%') OR UPPER(f.TPLNR) LIKE UPPER('%$7%')  "
                            + "OR UPPER(f.TIDNR) LIKE UPPER('%$8%') OR UPPER(ec.TYPTX) LIKE UPPER('%$9%')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "spras"
                        }],
                        columns: "FunctionLocJoinsEqCategory"
                    }
                },

                Equipments: {
                    get: {
                        query: "SELECT $COLUMNS FROM Equipment AS e " +
                            "LEFT JOIN EquipmentMl as eml ON eml.EQUNR = e.EQUNR AND eml.SPRAS='$3' " +
                            "LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = e.EQTYP AND ec.SPRAS = '$4' " +
                            "WHERE e.EQUNR='$2'",
                        params: [{
                            parameter: "$2",
                            value: "objectId"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }],
                        columns: "EquipmentJoinsMl"
                    },

                    count: {
                        query: "SELECT COUNT(MOBILE_ID) AS count FROM Equipment",
                        params: []
                    }
                },

                EquipmentInstalled: {
                    get: {
                        query: "SELECT TOP $4 START AT $1  $COLUMNS FROM EquipmentInstalled WHERE HEQUI='$5' AND ( UPPER(HEQUI_SHTXT) LIKE UPPER('%$2%') OR EQUNR LIKE '%$3%' )",
                        params: [{
                            parameter: "$2",
                            value: "searchString"
                        }, {
                            parameter: "$1",
                            value: "startAt"
                        }, {
                            parameter: "$4",
                            value: "noOfRows"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "objectId"
                        }],
                        columns: "EquipmentInstalled"
                    }
                },

                FunctionalLocations: {
                    get: {
                        query: "SELECT $COLUMNS FROM FunctionLoc AS f " +
                            "LEFT JOIN FunctionLocMl as fml ON fml.TPLNR = f.TPLNR AND fml.SPRAS='$3' " +
                            "LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = f.FLTYP AND ec.SPRAS = '$4' " +
                            "WHERE f.TPLNR='$2'",
                        params: [{
                            parameter: "$2",
                            value: "objectId"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "spras"
                        }],
                        columns: "FunctionLocJoinsMl"
                    },

                    count: {
                        query: "SELECT COUNT(MOBILE_ID) AS count FROM FunctionLoc",
                        params: []
                    }
                },

                FunctionLocInstalled: {
                    get: {
                        query: "SELECT TOP $4 START AT $1  $COLUMNS FROM FunctionLocInstalled WHERE TPLNR='$5' AND ( UPPER(HEQUI_SHTXT) LIKE UPPER('%$2%') OR EQUNR LIKE '%$3%'  )",
                        params: [{
                            parameter: "$2",
                            value: "searchString"
                        }, {
                            parameter: "$1",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "noOfRows"
                        }, {
                            parameter: "$5",
                            value: "objectId"
                        }],
                        columns: "FunctionLocInstalled"
                    },
                },

                MeasurementDocuments: {
                    get: {
                        query: "SELECT * FROM MEASUREMENTDOCUMENT WHERE POINT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "point"
                        }],
                        columns: "MeasurementDocuments"
                    },
                },

                CodeGroupMl: {
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 cg.mobile_id, ml.SPRAS, cg.CODE_CAT_GROUP, cg.CODEGROUP, cg.CATALOG_PROFILE, ml.CODEGROUP_TEXT FROM CODEGROUP as cg " +
                            "JOIN CODEGROUPML as ml on ml.CODE_CAT_GROUP = cg.CODE_CAT_GROUP and ml.CATALOG_PROFILE = cg.CATALOG_PROFILE "
                            + "WHERE cg.CATALOG_TYPE = '$7' AND cg.CATALOG_PROFILE = '$4' AND ml.SPRAS = '$3' AND (UPPER(ml.CODEGROUP_TEXT) LIKE UPPER('%$5%') OR UPPER(cg.CODE_CAT_GROUP) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "catalogProfile"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    }
                },
                
                FunclocHierarchy: {
                    get: {
                        query: "SELECT $COLUMNS FROM FUNCTIONLOC WHERE UPPER(TPLNR) LIKE UPPER('$1%')" +
                        		"ORDER BY TPLNR ASC ",
                        params: [{
                            parameter: "$1",
                            value: "funcLoc"
                        }],
                        columns: "FunclocHierarchy"
                    },
                },
                
                EquipmentHierarchy: {
                    get: {
                        query: "SELECT $COLUMNS FROM EQUIPMENT WHERE UPPER(TPLNR) LIKE UPPER('$1%')" +
                        		"ORDER BY HEQUI ASC ",
                        params: [{
                            parameter: "$1",
                            value: "funcLoc"
                        }],
                        columns: "EquipmentHierarchy"
                    },
                },
            }
        };
    });
