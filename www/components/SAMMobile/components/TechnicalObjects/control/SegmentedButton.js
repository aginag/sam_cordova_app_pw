sap.ui.define([
    "sap/m/SegmentedButton"
 ], 
 
function(SegmentedButton) {
    return SegmentedButton.extend("SAMMobile.control.SegmentedButton", {
	metadata : {
	    events : {

		/**
		 * Fires when the user selects a button, which returns the ID and button object.
		 * @overwrite
		 */
		select : {
		    parameters : {

			/**
			 * Reference to the button, that has been selected.
			 */
			button : {
			    type : "sap.m.Button"
			},

			/**
			 * ID of the button, which has been selected.
			 */
			id : {
			    type : "string"
			},

			/**
			 * Key of the button, which has been selected. This property is only filled when the control is initiated with the items aggregation.
			 * @since 1.28.0
			 */
			key : {
			    type : "string"
			},

			/**
			 * Indicates whether the pressed button, has been toggled or untoggled.
			 */
			wasPressed : {
			    type : "boolean"
			}
		    }
		}
	    }
	},

	/** 
	 * @overwrite
	 */
	init : function() {
	    if (sap.m.SegmentedButton.prototype.init) {
		sap.m.SegmentedButton.prototype.init.apply(this, arguments);
	    }
	},
	
	/**
	 * Sets the selectedKey and is usable only when the control is initiated
	 * with the items aggregation.
	 * 
	 * @param {string}
	 *                sKey The key of the button to be selected
	 * @returns {sap.m.SegmentedButton} <code>this</code> this pointer for
	 *          chaining
	 * @override sets selectedButton aggregation to null if sKey equals ""
	 * @since 1.28.0
	 */
	setSelectedKey : function(sKey) {
	    var aButtons = this.getButtons(), aItems = this.getItems(), i = 0;

	    if (aButtons.length === 0 && aItems.length > 0) {
		this.updateItems();

		// Keep buttons in sync
		aButtons = this.getButtons();
	    }

	    if (aItems.length > 0 && aButtons.length > 0) {
		for (; i < aItems.length; i++) {
		    if (aItems[i] && aItems[i].getKey() === sKey) {
			this.setSelectedButton(aButtons[i]);
			break;
		    }
		}
	    }
	    this.setProperty("selectedKey", sKey, true);

	    if (sKey === "") {
		this.setSelectedButton(null);
	    }

	    return this;
	},
	
	/** Event handler for the internal button press events.
	 *  Added functionality to toggle or untoggle the pressed button
	 *  Setting the selectedKey property accordingly
	 * @overwrite
	 */
	_buttonPressed : function(oEvent) {
	    var oButtonPressed = oEvent.getSource();
	    var isPressed = this.getSelectedButton() === oButtonPressed.getId();

	    if (isPressed) {
		 return;
	    }

	    this.getButtons().forEach(function(oButton) {
            oButton.$().removeClass("sapMSegBBtnSel");
            oButton.$().attr("aria-checked", false);
        });
        oButtonPressed.$().addClass("sapMSegBBtnSel");
        oButtonPressed.$().attr("aria-checked", true);

        this.setAssociation('selectedButton', oButtonPressed, true);
        this.setProperty("selectedKey", this.getSelectedKey(), true);

	    this.fireSelect({
		button : oButtonPressed,
		id : oButtonPressed.getId(),
		key : this.getSelectedKey(),
		wasPressed : isPressed
	    });
	},

	/**
	 * Internal helper function that sets the association <code>selectedButton</code> to the first button.
	 * Now if the selectedKey property is empty eg. "", no button of the selectedButton items aggregation 
	 * will be selected
	 * @overwrite
	 */
	_selectDefaultButton : function() {
	    var aButtons = this.getButtons();

	    if (aButtons.length > 0) {
		this.setAssociation('selectedButton', null, true);
	    }
	},

	renderer : {}
    });
});
