sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/controller/edit/NotificationEditViewModel",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button",
    "sap/m/Input",
    "sap/m/Select",
    "sap/m/Label",    
    "sap/ui/core/ListItem",
    "SAMMobile/controller/common/FuncLocHirarchyViewModel",
    "sap/m/MessageToast",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/controller/common/MediaAttachments.controller"
], function (JSONModel, Controller, MessageBox, formatter, NotificationViewModel, formatterComp, Dialog, Text, Button, Input, Select, Label, ListItem, FuncLocHirarchyViewModel, MessageToast, FileSystem, fileHelper, MediaAttachmentsController) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.edit.Edit", {
        fileHelper: fileHelper,
        formatterComp: formatterComp,
        formatter: formatter,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationsCreationRoute").attachPatternMatched(this._onCreationRouteMatched, this);
            this.oRouter.getRoute("notificationsEditRoute").attachPatternMatched(this._onEditRouteMatched, this);
            this.oRouter.getRoute("notificationsViewOnlyRoute").attachPatternMatched(this._onViewOnlyRouteMatched, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
            this.fileSystem = new FileSystem();

            this.funcLocTreeViewModel = null;          
            this.editNotificationViewModel = null;
            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            // Prevent direct user input for DateTimePicker.
            // Unfortunately DATETIMEPICKER object do not offer such a property, that´s why we need to set the input field as readonly manually
            var oStartDateTimePicker = this.getView().byId("notification-start-time");
            var oEndDateTimePicker = this.getView().byId("notification-end-time");

            oStartDateTimePicker.addEventDelegate({
                onAfterRendering: function(){
                    var oDateInner = this.$().find('.sapMInputBaseInner');
                    var oID = oDateInner[0].id;
                    $('#'+oID).attr("readonly", "true");
                }}, oStartDateTimePicker);

            oEndDateTimePicker.addEventDelegate({
                onAfterRendering: function(){
                    var oDateInner = this.$().find('.sapMInputBaseInner');
                    var oID = oDateInner[0].id;
                    $('#'+oID).attr("readonly", "true");
                }}, oEndDateTimePicker);
        },

        _onCreationRouteMatched: function (oEvent) {
            
            var oArgs = oEvent.getParameter("arguments");
            var query = oArgs["?query"];

            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));
            var onAfterLoadMasterData = function () {

                if (!this.editNotificationViewModel){
                    this.editNotificationViewModel = new NotificationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
                    this.editNotificationViewModel.setErrorCallback(this.onViewModelError.bind(this));

                    this.getView().setModel(this.editNotificationViewModel, "notificationModel");
                    this.getView().setModel(this.editNotificationViewModel.getValueStateModel(), "valueStateModel");
                }

                this.editNotificationViewModel.resetValueStateModel();
                this.editNotificationViewModel.setNotification(this.editNotificationViewModel.createNewNotification());
                this.editNotificationViewModel.setEdit(false);
                this.editNotificationViewModel.setOutageOnly(false);
                this.editNotificationViewModel.setViewOnly(false);
	            this.editNotificationViewModel.setIsLongTextMandatory(false);
                this.editNotificationViewModel.setFuncLocCatalogProfile("");
                this.editNotificationViewModel.loadMalCode();

                this.globalViewModel.setComponentHeaderText(this.editNotificationViewModel.getHeaderText());
                this.globalViewModel.model.setProperty("/editMode", true);

                if (oArgs.funclocId){
                    this.editNotificationViewModel.loadFuncLocCatProfile(oArgs);
                }

            }.bind(this);


            var that = this;

            var onMasterDataLoaded = function(routeName) {

                if (!this.initialized) {
                    this.initialized = true;
                }


                onAfterLoadMasterData();
            };

            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }

            this._tryCreateUserAttachmentsControl();
        },


        _onEditRouteMatched: function (oEvent) {
            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            var query = oArgs["?query"];
            var outageOnly  = oArgs["outageOnly"];

            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));
            var onDataLoaded = function() {
	            if (!that.editNotificationViewModel){
	            	that.editNotificationViewModel = new NotificationViewModel(that.component.requestHelper, that.globalViewModel, that.component);
		            that.editNotificationViewModel.setErrorCallback(that.onViewModelError.bind(that));
		
		            that.getView().setModel(that.editNotificationViewModel, "notificationModel");
		            that.getView().setModel(that.editNotificationViewModel.getValueStateModel(), "valueStateModel");
	            }
	            
	            that.editNotificationViewModel.resetValueStateModel();
	            that.editNotificationViewModel.setNotification(that.editNotificationViewModel.createNewNotification());
	            that.editNotificationViewModel.setEdit(true);
	            that.editNotificationViewModel.setOutageOnly(outageOnly);
	            that.editNotificationViewModel.setViewOnly(false);
	            that.editNotificationViewModel.setIsLongTextMandatory(false);
	            
	            that.editNotificationViewModel.loadMalCode();

	            that.editNotificationViewModel.load(oArgs.id, function() {
	                that.globalViewModel.setComponentHeaderText(that.editNotificationViewModel.getHeaderText());
	            });

	            that.globalViewModel.setComponentHeaderText(that.editNotificationViewModel.getHeaderText());
	            that.globalViewModel.model.setProperty("/editMode", true);

            };
            

            if (!this.component.masterDataLoaded){
            	this.component.loadMasterData(true, onDataLoaded.bind(this));
            }else{
            	onDataLoaded();
            }

            this._tryCreateUserAttachmentsControl();
        },

        _onViewOnlyRouteMatched: function(oEvent) {
            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            var query = oArgs["?query"];

            var onDataLoaded = function() {
                if (!that.editNotificationViewModel){
                    that.editNotificationViewModel = new NotificationViewModel(that.component.requestHelper, that.globalViewModel, that.component);
                    that.editNotificationViewModel.setErrorCallback(that.onViewModelError.bind(that));

                    that.getView().setModel(that.editNotificationViewModel, "notificationModel");
                    that.getView().setModel(that.editNotificationViewModel.getValueStateModel(), "valueStateModel");
                }

                that.editNotificationViewModel.resetValueStateModel();
                that.editNotificationViewModel.setNotification(that.editNotificationViewModel.createNewNotification());
                that.editNotificationViewModel.setEdit(false);
                that.editNotificationViewModel.setOutageOnly(false);
                that.editNotificationViewModel.setViewOnly(true);
	            that.editNotificationViewModel.setIsLongTextMandatory(false);
                that.editNotificationViewModel.setFuncLocCatalogProfile("");
                
                that.editNotificationViewModel.load(oArgs.id, function() {
                    that.globalViewModel.setComponentHeaderText(that.editNotificationViewModel.getHeaderText());
                });

                that.globalViewModel.model.setProperty("/editMode", false);
            };

            if (!this.component.masterDataLoaded){
                this.component.loadMasterData(true, onDataLoaded.bind(this));
            }else{
                onDataLoaded();
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },
        
        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        handleMessageViewPress: function () {

        },

        onSubjectChanged: function (oEvent) {
            var value = oEvent.getParameter("value");
            this.editNotificationViewModel.getValueStateModel().setProperty("/SHORT_TEXT", value.length > 0 ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error)
        },

        onAffectedHouseholdChanged: function (oEvent) {
            var value = oEvent.getParameter("value");
            var binding = oEvent.getSource().getBindingInfo("value").binding;
            var path = binding.getContext().getPath();
            this.editNotificationViewModel.setProperty(path + "/_VALUE_STATE_MODEL/" + binding.getPath() + "/state", value.length > 0 && value > 0 && value <= 9999 ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
        },

        onNavBack: function() {
            var that = this;
            if (this.getOwnerComponent().rootComponent.componentNavigationActive) {
                this.getOwnerComponent().rootComponent.navToPreviousComponent();
            } else {
            	window.history.go(-1);
            }
        },
        
        onCancelPressed: function (oEvent) {
            this.globalViewModel.model.setProperty("/editMode", false);
            this.onNavBack();
        },

        onSavePressed: function (oEvent) {
            var that = this;
            this.editNotificationViewModel.save(function () {
                that.globalViewModel.model.setProperty("/editMode", false);
                sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                sap.ui.getCore().getEventBus().publish("notificationsRoute", "refreshRoute");
                that.onNavBack();
                MessageToast.show(that.component.i18n.getText("NOTIFICATION_CREATE_UPDATE_SAVE_MESSAGE_TEXT"));
            });
        },

        onMalfunctionCauseDeletePressed: function () {
            this.editNotificationViewModel.clearMalfunctionCause();
        },

        onActStartTimeChanged: function (oEvent) {
            var value = oEvent.getParameter("value");
            var binding = oEvent.getSource().getBindingInfo("value").binding;
            var activityPath = binding.getContext().getPath();
            var activity = binding.getContext().getObject();

            activity._endTime = activity._startTime;
            activity.setStartTime(value);
            activity.setEndTime(value);
            var isValid = moment(activity._endTime).isAfter(moment(activity._startTime));
            this.editNotificationViewModel.setProperty(activityPath + "/_VALUE_STATE_MODEL/_endTime/state", isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
            this.editNotificationViewModel.setProperty(activityPath + "/_VALUE_STATE_MODEL/_startTime/state", isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
        },

        onActEndTimeChanged: function (oEvent) {
            var value = oEvent.getParameter("value");
            var binding = oEvent.getSource().getBindingInfo("value").binding;
            var activityPath = binding.getContext().getPath();
            var activity = binding.getContext().getObject();

            activity.setEndTime(value);
            var isValid = moment(activity._endTime).isAfter(moment(activity._startTime));
            this.editNotificationViewModel.setProperty(activityPath + "/_VALUE_STATE_MODEL/_endTime/state", isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
            this.editNotificationViewModel.setProperty(activityPath + "/_VALUE_STATE_MODEL/_startTime/state", isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
        },

        onActDeletePressed: function (oEvent) {
            var oActivity = oEvent.getSource().getBindingContext("notificationModel").getObject();
            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("CREATE_NOTIFICATION_REMOVE_OUTAGE_TIME_CONFIRMATION_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("CREATE_NOTIFICATION_REMOVE_OUTAGE_TIME_CONFIRMATION_TITLE"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        this.editNotificationViewModel.deleteActivity(oActivity);
                    }
                }.bind(this)
            });
        },

        onAddOutageTimePressed: function (oEvent) {
            this.editNotificationViewModel.addOutageTime();
        },

        displayNotifTypeSearch: function (oEvent) {
            this.editNotificationViewModel.displayNotifTypeDialog(oEvent, this);
        },

        handleNotifTypeSelect: function (oEvent) {
            this.editNotificationViewModel.handleNotifTypeSelect(oEvent);
        },

        handleNotifTypeSearch: function (oEvent) {
            this.editNotificationViewModel.handleNotifTypeSelect(oEvent);
        },

        // Priority
        displayPrioritySearch: function (oEvent) {
            this.editNotificationViewModel.displayPriorityDialog(oEvent, this);
        },

        handlePrioritySelect: function (oEvent) {
            this.editNotificationViewModel.handlePrioritySelect(oEvent);
        },

        handlePrioritySearch: function (oEvent) {
            this.editNotificationViewModel.handlePrioritySearch(oEvent);
        },

        // WorkCenter
        displayWorkCtrSearch: function (oEvent) {
            this.editNotificationViewModel.displayWorkCtrDialog(oEvent, this);
        },

        handleWorkCtrSelect: function (oEvent) {
            this.editNotificationViewModel.handleWorkCtrSelect(oEvent);
        },

        handleWorkCtrSearch: function (oEvent) {
            this.editNotificationViewModel.handleWorkCtrSearch(oEvent);
        },

        // Function Location
        displayFuncLocSearch: function (oEvent) {
            //this.editNotificationViewModel.displayFuncLocDialog(oEvent, this);

            var onAcceptPressed = function (funcLoc, parent) {

                var notification = this.editNotificationViewModel.getNotification();
                notification.FUNCT_LOC = funcLoc.id;
                notification.FUNCLOC_DESC = funcLoc.text;
                this.editNotificationViewModel.setFuncLocCatalogProfile(funcLoc.catalogProfile);
                this.editNotificationViewModel.getValueStateModel().setProperty("/FUNCT_LOC", sap.ui.core.ValueState.None);

                this.editNotificationViewModel.setNotification(notification);
                parent.close();
            };

            if (!this.funcLocTreeViewModel) {
                this.funcLocTreeViewModel = new FuncLocHirarchyViewModel(this.component.requestHelper, this.globalViewModel, this.component, true);
                this.funcLocTreeViewModel.setGasButtonVisible(true);
            }

            this.openFuncLocTreeDialog(this.funcLocTreeViewModel, onAcceptPressed.bind(this));
        },

        handleFuncLocSelect: function (oEvent) {
            this.editNotificationViewModel.handleFuncLocSelect(oEvent);
        },

        handleFuncLocSearch: function (oEvent) {
            this.editNotificationViewModel.handleFuncLocSearch(oEvent);
        },
        // Equipment
        displayEquipmentSearch: function (oEvent) {
            this.editNotificationViewModel.displayEquipmentDialog(oEvent, this);
        },

        handleEquipmentSelect: function (oEvent) {
            this.editNotificationViewModel.handleEquipmentSelect(oEvent);
        },

        handleEquipmentSearch: function (oEvent) {
            this.editNotificationViewModel.handleEquipmentSearch(oEvent);
        },

        // CodeGroup
        displayCodeGroupSearch: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.getPath().split("/").pop();

            if (path == "CODE_GROUP") {
                this.editNotificationViewModel.displayCodeGroupDialog(oEvent, this);
            } else if (path == "OBJ_CODE_GROUP") {
                this.editNotificationViewModel.displayObjCodeGroupDialog(oEvent, this);
            } else if (path == "TXT_ACTGRP") {
                this.editNotificationViewModel.displayActCodeGroupDialog(oEvent, this);
            }
        },

        handleCodeGroupSelect: function (oEvent) {
            var binding = oEvent.getSource().parentInput.getBindingInfo("value").binding;
            var path = binding.getPath().split("/").pop();

            if (path == "CODE_GROUP") {
                this.editNotificationViewModel.handleCodeGroupSelect(oEvent);
            } else if (path == "OBJ_CODE_GROUP") {
                this.editNotificationViewModel.handleObjCodeGroupSelect(oEvent);
            } else if (path == "TXT_ACTGRP") {
                this.editNotificationViewModel.handleActCodeGroupSelect(oEvent, binding.getContext().getPath());
            }
        },

        handleCodeGroupSearch: function (oEvent) {
            this.editNotificationViewModel.handleCodeGroupSearch(oEvent);
        },

        // Coding
        displayCodingSearch: function (oEvent) {
            var binding = oEvent.getSource().getBindingInfo("value").binding;
            var path = binding.getPath().split("/").pop();

            if (path == "CODING") {
                this.editNotificationViewModel.displayCodingDialog(oEvent, this);
            } else if (path == "OBJ_CODING") {
                this.editNotificationViewModel.displayObjCodingDialog(oEvent, this);
            } else if (path == "TXT_ACTCD") {
                this.editNotificationViewModel.displayActCodingDialog(oEvent, this, binding.getContext().getPath());
            } else if (path = "_MALF_CODE_TEXT") {
                this.editNotificationViewModel.displayMalfCodingDialog(oEvent, this);
            }
        },

        handleCodingSelect: function (oEvent) {
            var binding = oEvent.getSource().parentInput.getBindingInfo("value").binding;
            var path = binding.getPath().split("/").pop();

            if (path == "CODING") {
                this.editNotificationViewModel.handleCodingSelect(oEvent);
            } else if (path == "OBJ_CODING") {
                this.editNotificationViewModel.handleObjCodingSelect(oEvent);
            } else if (path == "TXT_ACTCD") {
                this.editNotificationViewModel.handleActCodingSelect(oEvent, binding.getContext().getPath());
             // Check coding for malfunction
                var isCodeRelevant = this.editNotificationViewModel.checkActCoding(binding.getContext().getPath());            
                if (isCodeRelevant){
                	this._getMalFuncCodeDialog(this.editNotificationViewModel.getProperty(binding.getContext().getPath())).open();
    	            this.editNotificationViewModel.setIsLongTextMandatory(true);
                }
            } else if (path = "_MALF_CODE_TEXT") {
                this.editNotificationViewModel.handleMalfCodingSelect(oEvent);
            }
        },

        onActLongtextPressed: function (oEvent){
            var binding = oEvent.getSource().getBindingInfo("visible").binding;
            var path = binding.getBindings()[0].getContext().getPath();
            this._getMalFuncCodeDialog(this.editNotificationViewModel.getProperty(path)).open();
    	},
    	
        handleCodingSearch: function (oEvent) {
            this.editNotificationViewModel.handleCodingSearch(oEvent);
        },

        // FuncLoc Tree View
        openFuncLocTreeDialog: function (viewModel, acceptCb) {
            var that = this;
            
            var onAcceptPressed = function () {

                var treeViewModel = this.getParent().getModel("funcLocTreeViewModel");
                if (treeViewModel.getSelectedFuncloc()) {
                    acceptCb(treeViewModel.getSelectedFuncloc(), this.getParent());
                }
            };

            var onReloadSuccess = function () {
                that._oFuncLocTreeView.setModel(viewModel, "funcLocTreeViewModel");
                that._oFuncLocTreeView.open();
            };
            
            if (!this._oFuncLocTreeView) {
                

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("funcLocTreeViewPopover-notif", "SAMMobile.view.common.FuncLocHirarchy", viewModel);
                }

                this._oFuncLocTreeView = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "{i18n>funcLocTreeView}",
                    horizontalScrolling: false,
                    verticalScrolling: false,
                    content: this._oMcPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    endButton: new Button({
                        text: '{i18n_core>save}',
                        enabled: '{= ${funcLocTreeViewModel>/selectedFuncLoc} !== null}',
                        icon: "sap-icon://save",
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    beginButton: new Button({
                        text: '{i18n_core>cancel}',
                        icon: "sap-icon://decline",
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        viewModel.getTree().getBinding("items").filter(null);
                        viewModel.getTree().removeSelections();
                        viewModel.getTree().collapseAll();
                        viewModel.setSelectedFuncloc(null);
                        viewModel.setProperty("/searchString", "");
                    }
                });
                this.getView().addDependent(this._oFuncLocTreeView);
                this._oFuncLocTreeView.setModel(this.component.getModel('i18n'), "i18n");
                this._oFuncLocTreeView.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._oFuncLocTreeView.getEndButton().mEventRegistry.press = [];
            this._oFuncLocTreeView.getEndButton().attachPress(onAcceptPressed);


            viewModel.setTree(sap.ui.core.Fragment.byId("funcLocTreeViewPopover-notif", "TreeObjects"));
            viewModel.setButtons(sap.ui.core.Fragment.byId("funcLocTreeViewPopover-notif", "tree-buttons"));
            viewModel.setHierarchy(false);
            viewModel.setToolbar(true);
            viewModel.loadData(onReloadSuccess.bind(this));
        },

        // Attachments

        //region private functions
        _tryCreateUserAttachmentsControl: function () {
            var me = this;
            if (!me.userAttachmentControl) {

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {

                    var notificationObject = me.editNotificationViewModel.getNotification();

                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = notificationObject.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    notificationObject.addAttachment(attachmentObj);
                    me.editNotificationViewModel.refresh(true);
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {

                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));
                    me.editNotificationViewModel.refresh();

                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {

                    var attachmentObj = bCtx.getObject();
                    var attachments = me.editNotificationViewModel.getNotification().attachments;

                    for (var i = 0; i < attachments.length; i++) {
                        if (attachments[i].FILENAME == attachmentObj.FILENAME) {
                            attachments.splice(i, 1);
                            break;
                        }
                    }
                    ;
                    me.editNotificationViewModel.setProperty('/notification/attachments', attachments);
                    me.editNotificationViewModel.refresh();
                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };
            }
        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var fileArr = oEvent.getParameter("files");

            for (let index = 0; index < fileArr.length; index++) {
                var file = fileArr[index];
                let name = file.name;
                var reader = new FileReader();
                reader.onload = function (readerEvt) {
                    binaryString = readerEvt.target.result.split(",")[1];
                    fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);

                    // On windows, we have to get the fileExt based on the file name
                    // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#browser-quirks section "readAsDataURL"
                    if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                        var fileExt = name.split(".").pop();
                    } else {
                        var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                    }

                    var completeFileName = name;
                    var indexOfPoint = completeFileName.lastIndexOf('.');
                    var fileName = completeFileName.slice(0, indexOfPoint);
                    fileName = that.mediaAttachmentsController.getConvertedFilename(fileName);
                    that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                };
                reader.onerror = function (error) {
                    MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                    that.editNotificationViewModel.setBusy(false);
                };

                reader.readAsDataURL(file);
                //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            }
            oEvent.getSource().clear();
        },

        onDeleteAttachment: function (oEvent) {
            var attachment = oEvent.getSource().getBindingContext('notificationModel').getObject();

            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        if (attachment.MOBILE_ID == "") {
                            this.editNotificationViewModel.removeFromModelPath(attachment, "/notification/NotificationAttachments");
                        } else {
                            attachment.setDeleteFlag(true);
                        }
                        this.editNotificationViewModel.refresh(true);
                        MessageToast.show(this.component.i18n.getText("NOTIFICATION_ATTACHMENT_DELETE_SUCCESS_TEXT"));
                    }
                }.bind(this)
            });
        },

        onEditAttachment: function (oEvent) {

            const that = this;
            var bContext = oEvent.getSource().getBindingContext('notificationModel');

            var onNewFileNameAccepted = function (newFileName) {

                var dialog = this;
                var attachment = bContext.getObject();

                // Remove whitespaces from file name as this causes issues on sync
                newFileName = newFileName.replace(/\s/g,"");
                var patternFileName = new RegExp(/[\\?*.%,\/:<>\"]/g);
                if (patternFileName.test(newFileName)) {
                    MessageToast.show(that.component.i18n.getText("fileNameInvalid"));
                    return;
                }
                var replacedFilename = that.mediaAttachmentsController.replaceUmlaute(newFileName);
                attachment.rename(replacedFilename, function () {
                    that.editNotificationViewModel.refresh(true);
                    MessageToast.show(that.component.i18n.getText("NOTIFICATION_ATTACHMENT_UPDATE_SUCCESS_TEXT"));
                    dialog.close();
                }, function (err) {
                    that.onViewModelError(err);
                });
            };

            this._getEditAttachmentDialog(bContext.getObject(), onNewFileNameAccepted).open();
        },

        onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getSelectedItem().getBindingContext('notificationModel');
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

        onCapturePhotoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.editNotificationViewModel, '/notification/NotificationAttachments/'); //oEvent.getSource().getBindingContext('notificationModel');
            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, this.fileSystem.filePathTmp, function(){});
        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;
            var notifObject = this.editNotificationViewModel.getNotification();
            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = notifObject.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.editNotificationViewModel.setBusy(false);
                MessageToast.show(that.component.i18n.getText("NOTIFICATION_ATTACHMENT_INSERT_SUCCESS_TEXT"));
            };

            var onWriteError = function (msg) {
                that.editNotificationViewModel.setBusy(false);
                MessageToast.show(msg);
            };

            this.fileSystem.writeToTemp(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            var notifObject = this.editNotificationViewModel.getNotification();
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];
                    notifObject.addAttachment(attachmentObj);
                }
            } else {
                notifObject.addAttachment(mediaObject);
            }
            that.editNotificationViewModel.refresh(true);
        },

        _getEditAttachmentDialog: function (attachment, acceptCb) {
            var that = this;
            var oBundle = this.getOwnerComponent().rootComponent.getModel("i18n").getResourceBundle();

            var dialog = new Dialog({
                title: oBundle.getText("EnterFileName"),
                contentWidth: "50%",
                content: new Input({
                    maxLength: 50,
                    placeholder: oBundle.getText("EnterFileName"),
                    value: "{attachment>/fileName}",
                    valueLiveUpdate: true
                }),
                beginButton: new Button({
                    text: oBundle.getText("cancel"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: oBundle.getText("save"),
                    enabled: "{= ${attachment>/fileName}.length > 0}",
                    press: function () {
                        acceptCb.apply(this.getParent(), [this.getParent().getModel("attachment").getData().fileName]);
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            dialog.setModel(new JSONModel({
                fileName: ""
            }), "attachment");

            return dialog;
        },
        
        _getMalFuncCodeDialog: function (notifActivity) {
            var that = this;

            var _oMalFuncCodeDialog = new Dialog({
                title: that.component.i18n.getText("TITLE_CODE"),
                contentWidth: "60%",
				contentHeight: "60%",
				stretch: true,		
                content: [ new Text({
                    text: "{/longtext}",
                    textAlign:"Left",
                    width: '100%'
                }),
                new Label({
                	required:true,
                	text:that.component.i18n.getText("LABEL"),
                	textAlign:"Left"                	
                }),
                new Select({
                	forceSelection:false,
                	enabled:"{/enabled}",
                	width:'50%',
				    selectedKey:"{/selectedCode}",
				    autoAdjustWidth:true,
				    items:{
				    	    path:"/confirmCode",
				    	    sorter: "{path:'CODE'}",
					    	template: new ListItem({
					        key: "{CODE}",
					        text: "{TEXT}"
				      })
				    }
                })],
                
                beginButton: new Button({
                    text: this.component.i18n_core.getText("cancel"),
                    icon: "sap-icon://decline",
                    press: function () {
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("save"),
                    enabled: "{= ${/selectedCode} !== ''}",
                    icon: "sap-icon://save",
                    press: function () {
	                     // get the selected value in notification header
	                    var selectedReason = this.getParent().getModel().getData().selectedCode;
	                    var selectedReasonText = '';
	                    
	                    var notification = that.editNotificationViewModel.getNotification();
	                    notification.MALFUNCODE = selectedReason;
	                    for (var i=0;i < this.getParent().getModel().getData().confirmCode.length ; i++){
	                    	var code = this.getParent().getModel().getData().confirmCode[i];
	                    	if (code.CODE === selectedReason ){
	                    		selectedReasonText = code.TEXT;
	                    		break;
	                    	}
	                    }
	                    
	                    notification._longText = selectedReasonText;
	                    that.editNotificationViewModel.setNotification(notification);
	                    this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            _oMalFuncCodeDialog.setModel(new JSONModel({
            	enabled: !that.editNotificationViewModel.getProperty("/view/viewOnly"),
            	selectedCode: that.editNotificationViewModel.getNotification().MALFUNCODE,
                longtext: notifActivity.ACT_CODE_TEXT,
                confirmCode: that.editNotificationViewModel.getConfirmCode()
            }));
            
            _oMalFuncCodeDialog.getContent()[0].addStyleClass("dialogText");
            _oMalFuncCodeDialog.getContent()[1].addStyleClass("dialogText");
            
            return _oMalFuncCodeDialog;
        },
        
    });

});