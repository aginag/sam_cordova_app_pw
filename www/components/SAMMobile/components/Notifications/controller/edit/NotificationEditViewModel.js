sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/helpers/formatter",
        "SAMMobile/models/viewModels/EditNotificationViewModel",
        "SAMMobile/components/Notifications/models/NotificationActivity",
        "SAMMobile/components/Notifications/models/NotificationCause",
        "SAMMobile/components/Notifications/models/NotificationUserStatus",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/components/Notifications/models/NotificationHeader",
        "sap/m/MessageToast"
    ],

    function (JSONModel, Validator, formatter, EditNotificationViewModel, NotificationActivity, NotificationCause, NotificationUserStatus, GenericSelectDialog, NotificationHeader, MessageToast) {
        "use strict";

        // constructor
        function NotificationEditViewModel(requestHelper, globalViewModel, component) {
            EditNotificationViewModel.call(this, requestHelper, globalViewModel, component);

            this._MALF_CODE_GROUP = "NEG00001";
            this._MALF_CATALOG_TYPE = "U";
        }

        NotificationEditViewModel.prototype = Object.create(EditNotificationViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            funcLocCatalogProfile: "",
                            isLongtextMandatory : false,
                            outageCatalogType: "V",
                            malFuncCode: [],
                            confirmCode: []
                        }
                    }
                }
            },

            setFuncLocCatalogProfile: {
                value: function (bValue) {
                    this.setProperty("/view/funcLocCatalogProfile", bValue);
                }
            },

            setMalFuncCode: {
                value: function (bCode) {
                    this.setProperty("/view/malFuncCode", bCode);
                }
            },
            
            setConfirmCode: {
                value: function (bCode) {
                    this.setProperty("/view/confirmCode", bCode);
                }
            },  
            
            getConfirmCode: {
                value: function () {
                    return this.getProperty("/view/confirmCode");
                }
            }, 
            
            setViewOnly: {
                value: function (bViewOnly) {
                    this.setProperty("/view/viewOnly", bViewOnly);
                }
            },
            
            setIsLongTextMandatory: {
                value: function (bMandatory) {
                    this.setProperty("/view/isLongtextMandatory", bMandatory);
                }
            },

            getHeaderText: {
                value: function () {
                    if (this.getProperty("/view/edit")) {
                        return this._component.getModel("i18n").getResourceBundle().getText("editNotification")
                    } else if (this.getProperty("/view/viewOnly")) {
                        return "Meldungsdetails - " + formatter.removeLeadingZeros.apply(this, [this.getNotification().NOTIF_NO]);
                    } else {
                        return this._component.getModel("i18n_core").getResourceBundle().getText("NewNotification");
                    }

                }
            }
        });

        NotificationEditViewModel.prototype._insert = function (successCb) {
            var notification = this.getNotification();
            var that = this;
            var sqlStrings = [];

            var onDocumentsMoved = function() {
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error("Error while trying to insert new notification"));
                });
            };

            var onDocumentsMovedError = function() {
                that.executeErrorCallback(new Error("Error while trying to move attachments"));
            };

            try {

                sqlStrings.push(notification.insert(true));

                if (notification._MALF_CODE) {
                    notification.NotificationCauses.push(this._getNewMalfCause(notification));
                }

                if (notification.getCauses().length > 0) {
                    sqlStrings = sqlStrings.concat(notification.getCauses().map(function (cause) {
                        return cause.insert(true);
                    }));
                }

                if (notification.getActivities().length > 0) {
                    sqlStrings = sqlStrings.concat(notification.getActivities().map(function (activity) {
                        return activity.insert(true);
                    }));
                }

                notification.NotificationUsrStat = this._getNewUserStatuses(notification);
                if (notification.getUserStatus().length > 0) {
                    sqlStrings = sqlStrings.concat(notification.getUserStatus().map(function (userStatus) {
                        return userStatus.insert(true);
                    }));
                }

                if (notification._longText.length > 0) {
                    sqlStrings = sqlStrings.concat(notification.addLongtext(true, notification._longText, this.scenario.getComplaintNotificationInfo().longTextObjectType));
                }

                // create notification item
                sqlStrings = sqlStrings.concat(this.getNewNotificationItem(notification).insert(true));

                if (notification.getAttachments() != null) {
                    notification.getAttachments().forEach(function(attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    this._moveAttachmentsFromTempToDoc(notification.getAttachments()).then(onDocumentsMoved.bind(this)).catch(onDocumentsMovedError.bind(this));
                    return;
                }

                onDocumentsMoved.apply(this);
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        NotificationEditViewModel.prototype._updateOnlyOutageTimes = function (notification, successCb) {
            var sqlStrings = [];
            var that = this;

            try {
                // Activity handling
                var aOutageTimes = notification.getActivities() && notification.getActivities().filter(function (oOutageTime) {
                    return oOutageTime.ACTION_FLAG === 'N';
                });
                if (aOutageTimes && aOutageTimes.length > 0) {
                    sqlStrings = sqlStrings.concat(aOutageTimes.map(function (activity) {
                        return activity.insert(true);
                    }));
                }

                notification.NotificationUsrStat = that._getNewUserStatuses(notification);
                if (notification.getUserStatus().length > 0) {
                    sqlStrings = sqlStrings.concat(notification.getUserStatus().map(function (userStatus) {
                        if (userStatus.ACTION_FLAG === 'N') {
                            return userStatus.insert(true);
                        } else {
                            userStatus.setActive(true);
                            return userStatus.setChanged(true);
                        }
                    }));
                }

                that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("NOTIFICATION_UPDATE_ERROR_TEXT")));
                });
            } catch (err) {
                that.executeErrorCallback(err);
            }

        };

        NotificationEditViewModel.prototype._update = function (successCb) {
            var notification = this.getNotification();
            var sqlStringsDelete = NotificationEditViewModel._getSubObjectDeletionsStrings(notification.NOTIF_NO);
            const that = this;

            var onAttachmentsUpdated = function() {
                executeUpdates(sqlStringsDelete, function (success) {
                    var sqlStrings = [];

                    var outageOnly = that.getProperty("/view/onlyOutageTime");
                    if (outageOnly && outageOnly === "true") {
                        that._updateOnlyOutageTimes(notification, successCb);
                        return;
                    }

                    try {

                        sqlStrings.push(notification.update(true));

                        if (notification._MALF_CODE) {
                            notification.NotificationCauses = [that._getNewMalfCause(notification)]
                        }

                        if (notification.getCauses().length > 0) {
                            sqlStrings = sqlStrings.concat(notification.getCauses().map(function (cause) {
                                return cause.insert(true);
                            }));
                        }

                        // Activity handling
                        if (notification.getActivities().length > 0) {
                            sqlStrings = sqlStrings.concat(notification.getActivities().map(function (activity) {
                                return activity.insert(true);
                            }));
                        }

                        notification.NotificationUsrStat = that._getNewUserStatuses(notification);
                        if (notification.getUserStatus().length > 0) {
                            sqlStrings = sqlStrings.concat(notification.getUserStatus().map(function (userStatus) {
                                if (userStatus.ACTION_FLAG === 'N') {
                                    return userStatus.insert(true);
                                } else {
                                    userStatus.setActive(true);
                                    return userStatus.setChanged(true);
                                }
                            }));
                        }

                        if (notification._longText.length > 0) {
                            sqlStrings = sqlStrings.concat(notification.addLongtext(true, notification._longText, that.scenario.getComplaintNotificationInfo().longTextObjectType));
                        }


                        that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                            that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("NOTIFICATION_UPDATE_ERROR_TEXT")));
                        });
                    } catch (err) {
                        that.executeErrorCallback(err);
                    }
                }, this.executeErrorCallback.bind(this, new Error("Error while preparing the notification update")));
            };

            this._updateAttachments().then(onAttachmentsUpdated.bind(this)).catch(function(err) {
                that.executeErrorCallback(err);
            });
        };

        NotificationEditViewModel.prototype._updateAttachments = function() {
            var that = this;
            var sqlStrings = [];
            var notification = this.getNotification();
            var attachments = notification.getAttachments();
            var updateQueue = [];


            // Attachments
            // Updates -> Rename actual attachment
            var updates = attachments.filter(function(attachment) {
                return attachment.MOBILE_ID != "" && attachment.hasChanges();
            });

            // Inserts -> Move from temp to doc directory
            var inserts = attachments.filter(function(attachment) {
                return attachment.MOBILE_ID == "";
            });

            // Deletes -> Delete actual attachment
            var deletions = attachments.filter(function(attachment) {
               return attachment.getDeleteFlag();
            });

            if (updates.length > 0) {
                updateQueue.push(this._renameAttachments(updates));
            }

            if (inserts.length > 0) {
                updateQueue.push(this._moveAttachmentsFromTempToDoc(inserts));
            }

            if (deletions.length > 0) {
                updateQueue.push(this._deleteAttachments(deletions));
            }

            return Promise.all(updateQueue).then(function() {
                return new Promise(function(resolve, reject) {
                    updates.forEach(function(attachment) {
                       sqlStrings.push(attachment.update(true));
                    });

                    inserts.forEach(function(attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    deletions.forEach(function(attachment) {
                        sqlStrings = sqlStrings.concat(attachment.delete(true));
                    });

                    that._requestHelper.executeStatementsAndCommit(sqlStrings, resolve, reject);
                });
            });
        };

        
        
        NotificationEditViewModel.prototype.load = function (notificationId, successCb) {
            this.setBusy(true);
            const that = this;
            var promises = [];

            promises.push(this._requestHelper.fetch({
                id: "Notification",
                statement: "SELECT @columns FROM NOTIFICATIONHEADER as nh " +
                    "LEFT JOIN NOTIFICATIONTYPE as nt on nt.QMART = nh.NOTIF_TYPE AND nt.SPRAS = '@spras' " +
                    "LEFT JOIN FUNCTIONLOC as fl on nh.FUNCT_LOC = fl.TPLNR " +
                    "WHERE nh.NOTIF_NO = '@notifNo'"
            }, {
                notifNo: notificationId,
                spras: this.getLanguage(),
                columns: "nh.MOBILE_ID, nh.NOTIF_NO, nh.NOTIF_NO_DISPLAY, nh.SHORT_TEXT, nh.NOTIF_DATE, nh.NOTIF_TYPE, nh.EQUIPMENT, nh.EQUIPMENT_DESC, nh.SHORT_TEXT, nh.PRIORITY, nh.PRIOTYPE, nh.MATERIAL, nh.XA_STAT_PROF, " +
                    "nh.SERIALNO, nh.FUNCT_LOC, nh.FUNCLOC_DESC, nh.OBJECT_NO, nh.NOTIF_DATE, nh.COMPDATE, nh.COMPTIME, " +
                    "nh.NOTIFTIME, nh.MN_WK_CTR, nh.MALFUNCODE, nh.PLANPLANT, nh.PLANT, nh.CATPROFILE, nh.CODE_GROUP, nh.CODING, nh.ORDERID, nh.BREAKDOWN, nh.ACTION_FLAG, nt.RBNR, fl.RBNR"
            }));

            Promise.all(promises).then(function (results) {
                if (results[0].Notification.length == 0) {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_NOTIFICATION_NOT_FOUND_TEXT")));
                    return;
                }

                var functionalLocCatalogProfile = results[0].Notification[0].RBNR;
                var notification = new NotificationHeader(results[0].Notification[0], that._requestHelper, that);
                var notificationPromises = [];
                notificationPromises.push(notification.loadLongText(that.scenario.getComplaintNotificationInfo().longTextObjectType));
                notificationPromises.push(notification.loadActivities());
                notificationPromises.push(notification.loadCauses());
                notificationPromises.push(notification.loadAttachments());
                notificationPromises.push(notification.loadUserStatus());
// load CodeML table
                notificationPromises.push(that._requestHelper.fetch({
                    id: "CodeMl",
                    statement: "SELECT * FROM CodeMl WHERE CATALOG_TYPE = '@catalog_type' AND SPRAS = '@spras'" 
                }, {
                    spras: that.getLanguage(),
                    catalog_type: that.getProperty("/view/outageCatalogType")
                }));
                Promise.all(notificationPromises).then(function (results) {

                    notification._longText = results[0];
                    notification._CATPROFILE = notification.RBNR;

                    var activities = results[1];
                    var codes = results[5].CodeMl;
                    if (activities.length > 0 && codes.length > 0){
	                    for  (var i=0;i < activities.length; i++){
	                    	var activity = notification.NotificationActivities[i];
	                    	activity.ACT_CODE_TEXT = '';
	                    	for (var j=0; j < codes.length; j++){
	                    		var code = codes[j];
	                    		if (activity.ACT_CAT_TYP === code.CATALOG_TYPE && activity.ACT_CODEGRP === code.CODE_GROUP && activity.ACT_CODE === code.CODE){
	                    			activity.ACT_CODE_TEXT = code.LONGTEXT;
	                    			break;
	                    		}
	                    	}
	                    }
                    }
                    
                    
                    var malfActivity = results[2];
                    if (malfActivity.length > 0) {
                        notification._MALF_CODE = malfActivity[0].CAUSE_CODE;
                        notification._MALF_CODE_TEXT = malfActivity[0].TXT_CAUSECD;
                        notification._MALF_ACT_CAT_TYP = malfActivity[0].CAUSE_CAT_TYP;
                        notification._MALF_ACTTEXT = malfActivity[0].CAUSETEXT;
                        notification._MALF_ACT_CODEGRP = malfActivity[0].CAUSE_CODEGRP;
                    }


                    that.setNotification(notification);
                    that.setFuncLocCatalogProfile(functionalLocCatalogProfile);
                    that.refresh(true);
                    that.setBusy(false);
                    successCb();
                }).catch(that.executeErrorCallback.bind(that, new Error("Error while fetching notification long text")));

            }).catch(this.executeErrorCallback.bind(this, new Error("Error while trying to fetch notification data")));
        };

        NotificationEditViewModel.prototype.validate = function () {
            const that = this;
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());
            var outageTimes = this.getNotification().getActivities().filter(function(activity) {
                return activity.ACT_CODEGRP !== that._MALF_CODE_GROUP;
            });

            var outageOnly = this.getProperty("/view/onlyOutageTime");
            if (!outageOnly || outageOnly === "false") {
	            validator.check("notification.NOTIF_TYPE", "Please select a Notification Type").isEmpty();
	            validator.check("notification.SHORT_TEXT", "Please enter a Subject").isEmpty();
	            validator.check("notification.FUNCT_LOC", "Please select a Functional Location").isEmpty();

	            if (this.getProperty("/view/funcLocCatalogProfile") && this.getProperty("/view/funcLocCatalogProfile") !== "MAU_DEF") {
                    validator.check("notification._MALF_CODE", "Please enter a malfunction cause").custom(function(code) {
                        return code !== undefined && code !== "";
                    });
                }
            }

            var outageTimeIndexes = outageTimes.map(function(outageTime) {
                return that.getNotification().getActivities().indexOf(outageTime);
            });
    		
    		// Check if one of activities is relevant for malfunction reason
    		var malfReason = false;
    		if(this.getProperty("/view/funcLocCatalogProfile") !== "MAU_DEF") {
                outageTimeIndexes.forEach(function (index) {
                    //Check end date is not greater start date
                    validator.check("notification.NotificationActivities[" + index + "]._startTime", "Outage Time start and end date confusion").custom(function (codeGroup) {
                        var activity = that.getNotification().getActivities()[index];
                        var isValid = moment(activity._endTime).isAfter(moment(activity._startTime));
                        var valueState = isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        activity._VALUE_STATE_MODEL._startTime.text = that._component.i18n.getText("CREATE_NOTIFICATION_VALIDATION_OUTAGE_TIME_START_END_TIME");
                        activity._VALUE_STATE_MODEL._startTime.state = valueState;
                        activity._VALUE_STATE_MODEL._endTime.state = valueState;
                        return isValid;
                    });
                    //Check code group entered
                    validator.check("notification.NotificationActivities[" + index + "].ACT_CODEGRP", "Code group must be entered for outage time").custom(function (codeGroup) {
                        var isValid = codeGroup != undefined && codeGroup != "";
                        var valueState = isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        that.getNotification().getActivities()[index]._VALUE_STATE_MODEL.ACT_CODEGRP.state = valueState;
                        return isValid;
                    });
                    //Check code entered
                    validator.check("notification.NotificationActivities[" + index + "].ACT_CODE", "Code group must be entered for outage time").custom(function (code) {
                        var isValid = code != undefined && code != "";
                        var valueState = isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        that.getNotification().getActivities()[index]._VALUE_STATE_MODEL.ACT_CODE.state = valueState;
                        return isValid;
                    });
                    //Check act text entered
                    validator.check("notification.NotificationActivities[" + index + "].ACTTEXT", "Code group must be entered for outage time").custom(function (actText) {
                        var isValid = actText != undefined && actText != "" && actText > 0 && actText <= 9999;
                        var valueState = isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        that.getNotification().getActivities()[index]._VALUE_STATE_MODEL.ACTTEXT.state = valueState;
                        return isValid;
                    });
                    //Check code existing in ZPMCodeGroupe
                    validator.check("notification.NotificationActivities[" + index + "].ACT_CODE", "Reason Code should be Enter for This Code").custom(function (code) {

                        var isCodeRelevant = false;
                        //check if the code groupe/code are relevant
                        for (var i = 0; i < modelData.view.malFuncCode.length; i++) {
                            var avblCode = modelData.view.malFuncCode[i];
                            if (avblCode.CODEGRUPPE === that.getNotification().getActivities()[index].ACT_CODEGRP && avblCode.CODE === that.getNotification().getActivities()[index].ACT_CODE) {
                                isCodeRelevant = true;
                                break;
                            }
                        }

                        if (!isCodeRelevant) {
                            return true;
                        }

                        malfReason = true;
                        var isValid = modelData.notification.MALFUNCODE != undefined && modelData.notification.MALFUNCODE != "";
                        var type = isValid ? sap.m.ButtonType.Transparent : sap.m.ButtonType.Negative;
                        var valueState = isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        that.getNotification().getActivities()[index]._VALUE_STATE_MODEL.ACT_CODE_TEXT.type = type;
                        that.getValueStateModel().setProperty("/MALFUNCODE", valueState);

                        //state for Longtext
                        var isValidLongText = (modelData.notification._longText != undefined && modelData.notification._longText != "");
                        var valueState = isValidLongText ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error;
                        that.getValueStateModel().setProperty("/LONG_TEXT", valueState);

                        return isValid && isValidLongText;
                    });
                });
            }

            if (!malfReason && this.getProperty("/view/funcLocCatalogProfile") !== "MAU_DEF"){
            	// In case there is no code group/code relevant at save, we have to clear MALFUNCODE field in notification header
            	this.getValueStateModel().setProperty("/MALFUNCODE", sap.ui.core.ValueState.None);
            	modelData.notification.MALFUNCODE = '';
            }
            
            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        NotificationEditViewModel.prototype.addOutageTime = function () {
            var that = this;
            var notification = this.getNotification();

            if (!notification.hasOwnProperty("_CATPROFILE") || notification._CATPROFILE == "") {
                this.executeErrorCallback(new Error(that._component.i18n.getText("CREATE_NOTIFICATION_ERROR_NO_PROFILE_TEXT")));
                return;
            }

            notification.NotificationActivities.unshift(this._getNewActivity());
            this.refresh();
        };

        NotificationEditViewModel.prototype.deleteActivity = function (activity) {
            var notification = this.getNotification();

            var indexOf = notification.getActivities().indexOf(activity);

            if (indexOf == -1) {
                return;
            }

            notification.getActivities().splice(indexOf, 1);
            this.refresh();

            MessageToast.show(this._component.getModel("i18n").getResourceBundle().getText("CREATE_NOTIFICATION_REMOVE_OUTAGE_TIME_CONFIRMATION_SUCCESS_TEXT"));
        };

        NotificationEditViewModel.prototype.clearMalfunctionCause = function () {
            const that = this;
            var malfActivity = this.getNotification().getActivities().filter(function(activity) {
                return activity.ACT_CODEGRP == that._MALF_CODE_GROUP;
            });

            if (malfActivity.length > 0) {
                this.deleteActivity(malfActivity[0]);
            }

            this.setProperty("/notification/_MALF_CODE", "");
            this.setProperty("/notification/_MALF_ACT_CAT_TYP", "");
            this.setProperty("/notification/_MALF_ACTTEXT", "");
            this.setProperty("/notification/_MALF_CODE_TEXT", "");
        };

        NotificationEditViewModel.prototype.displayActCodeGroupDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodeGroupDialog) {
                this._oCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getProperty("/view/funcLocCatalogProfile"),
                    catalogType: this.getProperty("/view/outageCatalogType")
                },
                actionName: "valueHelp"
            });
        };

        NotificationEditViewModel.prototype.handleActCodeGroupSelect = function (oEvent, path) {
            this._oCodeGroupDialog.select(oEvent, 'CODE_CAT_GROUP', [{
                'ACT_CODEGRP': 'CODEGROUP'
            }, {
                'TXT_ACTGRP': 'CODEGROUP_TEXT'
            }], this, path);

            var valueStateModelPath = path + "/_VALUE_STATE_MODEL/ACT_CODEGRP/state";
            this.setProperty(valueStateModelPath, sap.ui.core.ValueState.None);
        };

        NotificationEditViewModel.prototype.displayActCodingDialog = function (oEvent, viewContext, path) {
            var that = this;
            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCodingDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCodingDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty(path + "/ACT_CODEGRP"),
                    catalogType: this.getProperty("/view/outageCatalogType"),
                    catalogProfile: this.getProperty("/view/funcLocCatalogProfile")
                },
                actionName: "valueHelpOutageCodes"
            });
        };

        NotificationEditViewModel.prototype.handleActCodingSelect = function (oEvent, path) {
            this._oCodingDialog.select(oEvent, 'CODE', [{
                'ACT_CODE': 'CODE'
            }, {
                'TXT_ACTCD': 'CODE_TEXT'
            }, {
                'ACT_CAT_TYP': 'CATALOG_TYPE'
            }, {
                'ACT_CODE_TEXT': 'LONGTEXT'
            }], this, path);

            var valueStateModelPath = path + "/_VALUE_STATE_MODEL/ACT_CODE/state";
            this.setProperty(valueStateModelPath, sap.ui.core.ValueState.None);
            this.setProperty(path + "/_VALUE_STATE_MODEL/ACT_CODE_TEXT/type", sap.m.ButtonType.Transparent);
        };

        
        NotificationEditViewModel.prototype.checkActCoding = function (path) {
        	
        	// check if the selected code groupe/Code are relevant in ZPMCCodegroupe table and display the popup if necessary
        	// Get selected value
        	//var codeMl = oEvent.getParameter("selectedItem").getBindingContext().getObject();
        	var activity = this.getProperty(path);
        	var avblCodes = this.getProperty("/view/malFuncCode");
        	
        	var isCodeRelevant = false;
        	//check if longtext code exist
        	if(activity.ACT_CODE_TEXT){        	
        	//check if the code groupe/code are relevant for popup
	        	for (var i=0;i < avblCodes.length;i++){
	        		var avblCode = avblCodes[i];
	        		if (avblCode.CODEGRUPPE === activity.ACT_CODEGRP && avblCode.CODE === activity.ACT_CODE){
	        			isCodeRelevant = true;
	        			break;
	        		}
	        	}
        	}
        	
        	if (!isCodeRelevant){
        		activity.ACT_CODE_TEXT = '';
        		this.setProperty(path,activity);
        	}
        	return isCodeRelevant;
        };
        
        NotificationEditViewModel.prototype.displayMalfCodingDialog = function (oEvent, viewContext, path) {
            var that = this;
            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCodingDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCodingDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            // Pfalzwerke hard coded Malfunction Cause Code group + Catalog Type
            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    catalogType: this._MALF_CATALOG_TYPE,
                    catalogProfile: this.getProperty("/view/funcLocCatalogProfile")
                },
                actionName: "valueHelp"
            });
        };

        NotificationEditViewModel.prototype.handleMalfCodingSelect = function (oEvent, path) {
            this._oCodingDialog.select(oEvent, 'CODE', [{
                '_MALF_CODE': 'CODE'
            }, {
                '_MALF_ACT_CAT_TYP': 'CATALOG_TYPE'
            }, {
                '_MALF_ACT_CODEGRP': 'CODEGROUP'
            }, {
                '_MALF_CODE_TEXT': 'CODE_TEXT'
            }], this, "/notification");

            this.getValueStateModel().setProperty("/_MALF_CODE", sap.ui.core.ValueState.None);
        };

        NotificationEditViewModel.prototype._getNewActivity = function () {
            var activity = new NotificationActivity(null, this._requestHelper, this);
            activity.setNotifNo(this.getNotification().NOTIF_NO);
            activity.ACT_CODE_TEXT = '';

            return activity;
        };

        NotificationEditViewModel.prototype._getNewMalfCause = function (notification) {
            var malfCause = new NotificationCause(null, this._requestHelper, this);
            malfCause.setNotifNo(this.getNotification().NOTIF_NO);
            malfCause.CAUSE_CODEGRP = notification._MALF_ACT_CODEGRP;
            malfCause.CAUSE_CODE = notification._MALF_CODE;
            malfCause.CAUSE_CAT_TYP = this._MALF_CATALOG_TYPE;
            malfCause.CAUSETEXT = notification._MALF_ACTTEXT;
            malfCause.TXT_CAUSECD = notification._MALF_CODE_TEXT;

            return malfCause;
        };

        NotificationEditViewModel.prototype._getNewUserStatuses = function (notification) {
            var aUserStatuses = [],
                aNotifUserStatus = notification.getUserStatus().filter(function (oUserStatus) {
                    return oUserStatus.ACTION_FLAG !== 'N';
                });

            if (!notification.XA_STAT_PROF) {
                return aUserStatuses;
            }

            var aUserStatusMl = this._component.getModel("customizingModel").oData.UserStatusMl.filter(function(oUserStatus) {
                return oUserStatus.STATUS_PROFILE === notification.XA_STAT_PROF && oUserStatus.SPRAS === this.getLanguage();
            }.bind(this));


            if (notification.NOTIF_TYPE !== 'Z1' && notification.NOTIF_TYPE !== 'P3') {

                var oUserStatusSTÖR = aUserStatusMl && aUserStatusMl.find(function (oUserStatus) {
                    return oUserStatus.USER_STATUS_CODE === 'STÖR';
                });

                var oNotificationUserStatusStör = aNotifUserStatus && aNotifUserStatus.find(function (oUserStatus) {
                    return oUserStatus.USER_STATUS_CODE === oUserStatusSTÖR.USER_STATUS_CODE;
                });

                if (oNotificationUserStatusStör) {
                    aUserStatuses = aUserStatuses.concat(oNotificationUserStatusStör);
                } else {
                    // Create "stör" user status which is always required
                    oNotificationUserStatusStör = new NotificationUserStatus(null, this._requestHelper, this);
                    oNotificationUserStatusStör.NOTIF_NO = notification.NOTIF_NO;
                    oNotificationUserStatusStör.OBJNR = "QM" + notification.NOTIF_NO;
                    oNotificationUserStatusStör.STATUS = oUserStatusSTÖR.USER_STATUS;
                    oNotificationUserStatusStör.USER_STATUS_CODE = oUserStatusSTÖR.USER_STATUS_CODE;
                    oNotificationUserStatusStör.USER_STATUS_DESC = oUserStatusSTÖR.USER_STATUS_DESC;

                    aUserStatuses.push(oNotificationUserStatusStör);
                }
            }

            // In case of outage times, create "vdn" user status in addition. Outage times are mandatory in case catalog profile is given.
            if (this.getNotification().getActivities().length > 0) {

                var oUserStatusVDN = aUserStatusMl && aUserStatusMl.find(function (oUserStatus) {
                    return oUserStatus.USER_STATUS_CODE === 'VDN';
                });

                var oNotificationUserStatusVdn = aNotifUserStatus && aNotifUserStatus.find(function (oUserStatus) {
                    return oUserStatus.USER_STATUS_CODE === oUserStatusVDN.USER_STATUS_CODE;
                });

                if (oNotificationUserStatusVdn) {
                    aUserStatuses = aUserStatuses.concat(oNotificationUserStatusVdn);
                } else {
                    oNotificationUserStatusVdn = new NotificationUserStatus(null, this._requestHelper, this);
                    oNotificationUserStatusVdn.NOTIF_NO = notification.NOTIF_NO;
                    oNotificationUserStatusVdn.OBJNR = "QM" + notification.NOTIF_NO;
                    oNotificationUserStatusVdn.STATUS = oUserStatusVDN.USER_STATUS;
                    oNotificationUserStatusVdn.USER_STATUS_CODE = oUserStatusVDN.USER_STATUS_CODE;
                    oNotificationUserStatusVdn.USER_STATUS_DESC = oUserStatusVDN.USER_STATUS_DESC;

                    aUserStatuses = aUserStatuses.concat(oNotificationUserStatusVdn);
                }
            }

            return aUserStatuses;
        };

        NotificationEditViewModel._getSubObjectDeletionsStrings = function (notifNo) {
            var sqlStringsDelete = [];

            var deleteActivitySql = "DELETE FROM NOTIFICATIONACTIVITY WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteActivitySql = deleteActivitySql.replace("$1", notifNo);

            var deleteCauseSql = "DELETE FROM NOTIFICATIONCAUSE WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteCauseSql = deleteCauseSql.replace("$1", notifNo);

            var deleteItemSql = "DELETE FROM NOTIFICATIONITEM WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteItemSql = deleteItemSql.replace("$1", notifNo);

            var deleteStatusSql = "DELETE FROM NotificationUsrStat WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteStatusSql = deleteStatusSql.replace("$1", notifNo);

            var deleteLongtextSql = "DELETE FROM NOTIFICATIONLONGTEXT WHERE NOTIF_NO = '$1' AND ACTION_FLAG = 'N'";
            deleteLongtextSql = deleteLongtextSql.replace("$1", notifNo);

            sqlStringsDelete.push(deleteActivitySql);
            sqlStringsDelete.push(deleteCauseSql);
            sqlStringsDelete.push(deleteItemSql);
            sqlStringsDelete.push(deleteStatusSql);
            sqlStringsDelete.push(deleteLongtextSql);

            return sqlStringsDelete;
        };

        NotificationEditViewModel.prototype.createNewNotification = function () {
            var notificationObject = new NotificationHeader(null, this._requestHelper, this);

            notificationObject._longText = "";
            notificationObject.OBJ_CODE_GROUP = "";
            notificationObject.OBJ_CODING = "";
            notificationObject.OBJ_CAT_TYP = "";
            notificationObject.NOTIF_TYPE = "P1";
            notificationObject._CATPROFILE = "PW01";
            notificationObject.XA_STAT_PROF = "PW_P0001";

            return notificationObject;
        };

        NotificationEditViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    SHORT_TEXT: sap.ui.core.ValueState.None,
                    LONG_TEXT: sap.ui.core.ValueState.None,
                    NOTIF_TYPE: sap.ui.core.ValueState.None,
                    PRIORITY: sap.ui.core.ValueState.None,
                    MN_WK_CTR: sap.ui.core.ValueState.None,
                    CODE_GROUP: sap.ui.core.ValueState.None,
                    CODING: sap.ui.core.ValueState.None,
                    OBJ_CODE_GROUP: sap.ui.core.ValueState.None,
                    OBJ_CODING: sap.ui.core.ValueState.None,
                    _MALF_CODE: sap.ui.core.ValueState.None,
                    MALFUNCODE: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        NotificationEditViewModel.prototype.resetValueStateModel = function () {
            this.valueStateModel.setProperty("/SHORT_TEXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/LONG_TEXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/NOTIF_TYPE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/PRIORITY", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/MN_WK_CTR", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CODE_GROUP", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CODING", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/OBJ_CODE_GROUP", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/FUNCT_LOC", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/_MALF_CODE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/MALFUNCODE", sap.ui.core.ValueState.None);
            this.setProperty("/view/errorMessages", []);
        };
        
        NotificationEditViewModel.prototype.loadMalCode = function() {
            var that = this;
            var onDataLoaded = function(data) {    
            	// add empty line for dropdown list 
				data.ZPMCCode.push({
					TEXT:"",
					CODE:""
				});
				
            	that.setMalFuncCode(data.ZPMCCodegroup);
            	that.setConfirmCode(data.ZPMCCode);	
            };

            this._requestHelper.getData(["ZPMCCodegroup","ZPMCCode"], false, onDataLoaded, function () {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("EROOR_LOAD_CODE")));
            }, true);
        };

        NotificationEditViewModel.prototype.constructor = NotificationEditViewModel;

        return NotificationEditViewModel;
    }
);