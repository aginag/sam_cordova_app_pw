sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "sap/ui/model/Sorter",
        "SAMMobile/models/dataObjects/Order",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/components/Notifications/controller/edit/NotificationEditViewModel"],

    function (ViewModel, Filter, Sorter, Order, Formatter, NotificationHeader, NotificationEditViewModel) {
        "use strict";

        // constructor
        function NotificationListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/notifications";

            this._formatter = new Formatter(this._component);

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        NotificationListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        notifications: [],
                        selectedNotification: null,
                        searchString: "",
                        counts: {
                            local: 0,
                            historic: 0
                        },
                        view: {
                            busy: false,
                            selectedTabKey: "local",
                            notificationSelected: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold,
                            statusButtons: []
                        }
                    };
                }
            },

            getSelectedNotification: {
                value: function () {
                    return this.getProperty("/selectedNotification");
                }
            },

            getNotificationQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber,
                        searchString: this.getSearchString(),
                        userName: this._globalViewModel.model.getProperty("/userName")
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            getScenario: {
                value: function() {
                    return this._component.scenario;
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function() {}, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function() {
                    return this._component.getModel("i18n").getResourceBundle().getText("notifications");
                }
            }
        });

        NotificationListViewModel.prototype.orderUserStatus = null;

        NotificationListViewModel.prototype.tabs = {
            local: {
                actionName: "local",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/local", this.getProperty("/counts/local") + value);
                }
            },

            historic: {
                actionName: "historic",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/historic", this.getProperty("/counts/historic") + value);
                }
            }
        };

        NotificationListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        NotificationListViewModel.prototype.delete = function (notification, successCb) {
            var that = this;
            var sqlStrings = [];
            var sqlDeleteStrings = NotificationEditViewModel._getSubObjectDeletionsStrings(notification.NOTIF_NO);

            if (notification.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error("Can not delete Notifications that are synchronized"));
                return;
            }

            sqlStrings = sqlStrings.concat(notification.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(notification, "/notifications");
                that.tabs.local.changeCountBy.apply(that, [-1]);
                that.tabs.local.needsReload = true;
                that.setSelectedNotification(null);
                successCb();
            };

            var onAttachmentsDeleted = function() {
                executeUpdates(sqlDeleteStrings, function (success) {
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error("Error while trying to delete notification + sub objects"));
                    });
                }, that.executeErrorCallback.bind(that, new Error("Error while preparing the notification deletion")));
            };


            notification.loadAttachments().then(function(attachments) {
                attachments.forEach(function(attachment) {
                    sqlStrings = sqlStrings.concat(attachment.delete(true));
                });

                that._deleteAttachments(attachments)
                    .then(onAttachmentsDeleted)
                    .catch(onAttachmentsDeleted);
            }).catch(onAttachmentsDeleted);
        };

        NotificationListViewModel.prototype.onTabChanged = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // MODEL SEARCH
            // Reset binding and resetting searchValue when switching tabs
            this.setProperty("/searchString", "");
            this.getList().getBinding("items").filter(null);

            this.resetGrowingListDelegate();

            if (tabInfo.needsReload || tabInfo.lastSearch != this.getSearchString()) {
                this.fetchData();
            } else {
                this.setNewData(tabInfo.cached);
            }
        };

        NotificationListViewModel.prototype.onSearch = function (sValue) {
            var aFilters = [];
            var oBinding = this.getList().getBinding("items");

            // do nothing - no filter shall be applied
            aFilters.push(new Filter("NOTIF_NO", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("SHORT_TEXT", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("_START_DATE", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("FL_SHTXT", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("NOTIF_TYPE", sap.ui.model.FilterOperator.Contains, sValue));
            aFilters.push(new Filter("QMARTX", sap.ui.model.FilterOperator.Contains, sValue));
            var allFilter = new Filter(aFilters);

            oBinding.filter(allFilter);
        };

        NotificationListViewModel.prototype.onSort = function (oEvt) {
            var aSorters = [],
                mParams = oEvt.getParameters(),
                oBinding = this.getList().getBinding("items"),
                sPath,
                bDescending;

            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            var oSorter = new Sorter(sPath, bDescending);

            if (sPath === "NOTIF_DATE") {
                this._component.rootComponent.sortDatesOnTable(oSorter);
            }
            aSorters.push(oSorter);

            // apply the selected sort and group settings
            oBinding.sort(aSorters);
        };

        NotificationListViewModel.prototype.setSelectedNotification = function (order) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");

            this.setProperty("/selectedNotification", order);
            this.setProperty("/view/notificationSelected", !order || order == undefined ? false : true);

            if (!this.getProperty("/view/notificationSelected")) {
                this.getList().removeSelections();
            }
        };

        NotificationListViewModel.prototype.setNewData = function (data) {
            var that = this;
            var newData = [];

            data.forEach(function (notificationData) {
                var notification = new NotificationHeader(notificationData, that._requestHelper, that);

                if (notification.hasOwnProperty("NOTIF_DATE") &&
                    notification.hasOwnProperty("NOTIFTIME")) {

                    var startDate = moment(notification.NOTIF_DATE.split(" ").shift() + " " + notification.NOTIFTIME.split(" ").pop());

                    notification._START_DATE = startDate.isValid() ? startDate.format("DD.MM.YYYY") : "";
                }

                newData.push(notification);
            });

            this.setProperty("/notifications", newData);
            this.setSelectedNotification(null);
        };

        NotificationListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                tabInfo.needsReload = false;
                tabInfo.lastSearch = that.getSearchString();
                tabInfo.cached = bGrowing ? tabInfo.cached.concat(data) : data;

                that.setNewData(tabInfo.cached);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching orders"));
            };
            
            this.setBusy(true);
            this._requestHelper.getData("Notifications", this.getNotificationQueryParams(bGrowing ? this.getProperty("/notifications").length + 1 : 1), load_success, load_error, true, tabInfo.actionName);
        };

        NotificationListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        NotificationListViewModel.prototype.resetModel = function () {
            for (var key in this.tabs) {
                this.tabs[key].needsReload = true;
            }
        };

        NotificationListViewModel.prototype.fetchCounts = function () {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/counts/local", parseInt(data.LocalNotificationCount[0].COUNT));
                that.setProperty("/counts/historic", parseInt(data.HistoricNotificationCount[0].COUNT));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching tab counts"));
            };

            this._requestHelper.getData(["LocalNotificationCount", "HistoricNotificationCount"], {
                persNo: this.getUserInformation().personnelNumber,
                userName: this._globalViewModel.model.getProperty("/userName")
            }, load_success, load_error, true);
        };

        NotificationListViewModel.prototype.constructor = NotificationListViewModel;

        return NotificationListViewModel;
    });
