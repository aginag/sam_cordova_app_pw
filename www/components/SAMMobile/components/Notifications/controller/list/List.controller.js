sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/list/NotificationsListViewModel",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "sap/ui/model/Filter",
    "sap/m/MessageToast"
], function(Controller, NotificationsListViewModel, MessageBox, formatter, formatterComp, Filter, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.list.List", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notifications").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("notificationsRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationListViewModel = new NotificationsListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.notificationListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.notificationListViewModel.setList(this.byId("notificationList"));

            this.initialized = false;
            this.getView().setModel(this.notificationListViewModel, "notificationListViewModel");

            this._sortDialogs = {};
        },

        _onRouteMatched: function (oEvent) {
            var that = this;
    
            var onMasterDataLoaded = function(routeName) {
                this.globalViewModel.setCurrentRoute(routeName);

                if (this.notificationListViewModel.needsReload()) {
                    this.loadObjects();
                }

                if (!this.initialized) {
                    this.initialized = true;
                }
            };

            this.globalViewModel.setComponentHeaderText(this.notificationListViewModel.getHeaderText());
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            var onReloadSuccess = function() {

            };

            this.notificationListViewModel.fetchCounts();

            if (bSync) {
                this.notificationListViewModel.refreshData();
            } else {
                this.notificationListViewModel.fetchData(onReloadSuccess.bind(this));
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {
            jQuery.sap.log.info("PriSec - NotificationList Controller - Refresh Event triggered");
            this.loadObjects(true);
        },

        onLogout: function() {
            this.initialized = false;
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("partnersList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onTableSearch: function (oEvt) {
            this.notificationListViewModel.onSearch(oEvt.getSource().getValue());

            /*
            // add filter for search
            var aFilters = [];
            var sQuery = oEvt.getSource().getValue();
            if (sQuery && sQuery.length > 0) {
                var filter = new Filter("Name", sap.ui.model.FilterOperator.Contains, sQuery);
                aFilters.push(filter);
            }

            // update list binding
            var list = this.byId("partnerList");
            var binding = list.getBinding("items");
            binding.filter(aFilters, "Application");
            //
            */
        },

        onListGrowing: function (oEvt) {
            this.notificationListViewModel.onListGrowing(oEvt);
        },

        onTabSelect: function(oEvt) {
           this.notificationListViewModel.onTabChanged();
        },

        onItemPress: function(oEvt) {
            var bindingContext = oEvt.getParameter("listItem").getBindingContext("notificationListViewModel");
            var notification = bindingContext.getObject();

            this.notificationListViewModel.setSelectedNotification(notification);
        },

        onSortTableConfirm: function (oEvt) {
            this.notificationListViewModel.onSort(oEvt);
        },

        onSortButtonPress: function (oEvt) {
            var oDialog = this._sortDialogs["sortFragment"];

            if (!oDialog) {
                oDialog = sap.ui.xmlfragment("SAMMobile.components.Notifications.view.list.SortDialog", this);
                this._sortDialogs["sortFragment"] = oDialog;
                oDialog.setModel(this.component.getModel('i18n'), "i18n");
                oDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            oDialog.open();
        },

        onNewNotificationPressed: function() {
            this.oRouter.navTo("notificationsCreationRoute");
        },

        onDeleteNotificationPressed: function() {
            var onActionConfirmed = function() {
                this.notificationListViewModel.delete(this.notificationListViewModel.getSelectedNotification(), function() {
                    MessageToast.show(this.component.getModel('i18n').getResourceBundle().getText("deleteNotificationConfirmationText"));
                    sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                }.bind(this));
            };

            this.getOwnerComponent().getConfirmActionDialog(this.component.getModel('i18n').getResourceBundle().getText("deleteNotificationConfirmation"), onActionConfirmed.bind(this)).open();

        },

        onNavToDetailPress: function (oEvt) {
            var bindingContext = oEvt.getSource().getBindingContext("notificationListViewModel");
            var notification = bindingContext.getObject();

            if (notification.ACTION_FLAG == "N") {
                this.oRouter.navTo("notificationsEditRoute", {
                    id: notification.NOTIF_NO,
                    outageOnly:false
                });
            } else {
                this.oRouter.navTo("notificationsViewOnlyRoute", {
                    id: notification.NOTIF_NO
                }, false);
            }
        },
    });

});