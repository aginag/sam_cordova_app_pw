sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("SAMMobile.components.Notifications.controller.Application", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationsCustomRoute").attachPatternMatched(this._onCustomRouteMatched, this);
        },

        _onCustomRouteMatched: function(oEvent) {
            var oArgs = oEvent.getParameter("arguments");
            var query = oArgs["?query"];

            this.oRouter.navTo(query.routeName, JSON.parse(query.params), true);
		}
	});

});