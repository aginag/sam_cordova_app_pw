sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/helpers/FileSystem",
        "SAMMobile/models/dataObjects/SAMObjectAttachment",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, FileSystem, SAMObjectAttachment, DMSAttachment, GenericSelectDialog) {
        "use strict";

        // constructor
        function NotificationAttachmentsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._NOTIFICATIONNO = "";
            this._NOTIFICATION = null;

            this.fileSystem = new FileSystem();

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        NotificationAttachmentsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        attachments: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFICATIONNO = notificationNo;
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFICATION = notification;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFICATIONNO;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFICATION;
                }
            },

            canAttachFiles: {
                value: function () {
                    // TODO commented for development purposes
                    return this.getNotificationNo() !== "";
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        NotificationAttachmentsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        NotificationAttachmentsTabViewModel.prototype.getNewAttachmentObject = function (fileName, fileExt, fileSize, folderPath) {
            var notification = this.getNotification();
            var attachmentScenario = this._component.getAttachmentScenario();
            var attachment = null;
            var that = this;

            if(attachmentScenario === "DMS"){
                attachment = new DMSAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(that._component.i18n_core.getText("NOTIFICATION_TABNAME"));
                attachment.setObjectKey(notification.NOTIF_NO);
                attachment.setAttachmentHeader(this._component.i18n_core.getText("NOTIFICATION_TABNAME") + ' - ' + notification.SHORT_TEXT);
                attachment.setUsername(that.getUserInformation().userName.toUpperCase());
                attachment.setDocumentPart("000");
                attachment.setDocumentType("DME");
                attachment.setDocumentVersion("00");
                attachment.setDescription(fileName);
            } else {
                attachment = new SAMObjectAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(that._component.i18n_core.getText("NOTIFICATION_TABNAME"));
                attachment.setObjectKey(notification.NOTIF_NO);
                attachment.setAttachmentHeader(this._component.i18n_core.getText("NOTIFICATION_TABNAME") + ' - ' + notification.SHORT_TEXT);
            }

            return attachment;
        };

        NotificationAttachmentsTabViewModel.prototype.saveUserAttachment = function (attachment, fnCallback) {
            var that = this;
            var sqlStrings = [];
            var notification = this.getNotification();

            var onSuccess = function () {
                that.insertToModelPath(attachment, "/attachments");
                that.refresh();
            };

            if (!this.canAttachFiles()) {
                this.executeErrorCallback(new Error("No reference to the Notification could be found"));
                return;
            }

            try {
                attachment.insert(false, function (mobileId) {
                    attachment.MOBILE_ID = mobileId;
                    onSuccess();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_NOTIF_ATTACHMENT_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        NotificationAttachmentsTabViewModel.prototype.updateUserAttachment = function (attachment) {
            var that = this;

            if (attachment.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error("deletion of attachment is only possible when ACTION_FLAG = 'N'"));
                return;
            }

            try {
                attachment.update(false, function () {
                    that.refresh();
                }, function () {
                    that.executeErrorCallback(new Error("Error while trying to update attachment"));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        NotificationAttachmentsTabViewModel.prototype.deleteUserAttachment = function (attachment) {
            var that = this;

            attachment.delete(false, function () {
                that.removeFromModelPath(attachment, "/attachments");
            }, function () {
                that.executeErrorCallback(new Error("Error while trying to delete attachment"));
            });
        };

        NotificationAttachmentsTabViewModel.prototype.setModelData = function (data) {
            var that = this;

            var arrData = data.map(function (attData) {
                var attObject = new SAMObjectAttachment(attData, that._requestHelper, that);

                if (attObject.ACTION_FLAG == "N") {
                    attObject.setFolderPath(that.fileSystem.filePathDoc);
                } else {
                    attObject.setFolderPath(that.fileSystem.getRootDataStorage());
                }

                return attObject;
            });

            this.setProperty("/attachments", arrData);

        };

        NotificationAttachmentsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false; 

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while loading data for attachments tab"));
            };

            this.setBusy(true);
            this._requestHelper.getData("NotificationAttachment", {
                notificationNo: this.getNotificationNo()
            }, load_success, load_error, true);
        };


        NotificationAttachmentsTabViewModel.prototype.resetData = function () {
            this.setProperty("/attachments", []);
            this.setNotification(null);
        };

        NotificationAttachmentsTabViewModel.prototype.constructor = NotificationAttachmentsTabViewModel;

        return NotificationAttachmentsTabViewModel;
    });
