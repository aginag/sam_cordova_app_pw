sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function(Controller, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.attachments.NotificationAttachmentsTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailAttachments").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailAttachments", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.notificationAttachmantsTabViewModel = null;

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.fileSystem = new FileSystem();
        },

        onRouteMatched: function(oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.notificationAttachmantsTabViewModel = this.notificationDetailViewModel.tabs.attachments.model;

                this.getView().setModel(this.notificationAttachmantsTabViewModel, "notificationAttachmentsTabViewModel");
            }

            if (this.notificationAttachmantsTabViewModel.needsReload()) {
                this.loadViewModelData();
            }

            this._tryCreateUserAttachmentsControl();
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailAttachments", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.notificationAttachmantsTabViewModel.loadData(function() {

            });
        },

        _tryCreateUserAttachmentsControl : function(){
            var me = this;
            if (!me.userAttachmentControl) {
                //                 me.userAttachmentControl = sap.ui.xmlfragment("SAMMobile.view.common.MediaAttachments", me.mediaAttachmentsController);
                //                 me.getView().addDependent(me.userAttachmentControl);

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {
                    var attachmentList = bCtx.getObject();
                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = me.notificationAttachmantsTabViewModel.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    me.notificationAttachmantsTabViewModel.saveUserAttachment(attachmentObj, function(){
                        me.notificationAttachmantsTabViewModel.refresh(true);
                    })
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {
                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));

                    me.notificationAttachmantsTabViewModel.updateUserAttachment(attachment);
                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {
                    var attachment = bCtx.getObject();

                    //in case file could not be deleted on file system we remove it from DB
                    me.notificationAttachmantsTabViewModel.deleteUserAttachment(attachment);
                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };
            }
            this.notificationDetailViewModel.loadAllCounters();
        },

        _onCapturePhotoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.notificationAttachmantsTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("notificationAttachmantsTabViewModel");

            if (!this.notificationAttachmantsTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext);
            this.notificationDetailViewModel.loadAllCounters();

        },

        _onCaptureVideoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.notificationAttachmantsTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("notificationAttachmantsTabViewModel");
            if (!this.notificationAttachmantsTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureVideoPress(null, bContext);
            this.notificationDetailViewModel.loadAllCounters();

        },

        _onCaptureAudioPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.notificationAttachmantsTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("notificationAttachmantsTabViewModel");
            if (!this.notificationAttachmantsTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureAudioPress(null, bContext);
            this.notificationDetailViewModel.loadAllCounters();

        },

        _onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getSelectedItem().getBindingContext("notificationAttachmantsTabViewModel");
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

        _onEditAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("notificationAttachmantsTabViewModel");
            this.mediaAttachmentsController._onEditAttachment(null, bContext);
        },

        _onDeleteAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("notificationAttachmantsTabViewModel");
            this.mediaAttachmentsController._onDeleteAttachment(null, bContext);
            this.workOrderDetailViewModel.loadAllCounters();

        },

        onUserAttachments: function(oEvent){
            var bCtx = oEvent.getSource().getBindingContext();
            var taskObject = bCtx.getObject();
            var me = this;
            var context = new Context(me.checkListTaskModel, bCtx.sPath);
            me.userAttachmentControl.setBindingContext(context);
            me.userAttachmentControl.setModel(me.checkListTaskModel);

            var userAttTable = sap.ui.getCore().byId("checklistUserAttachmentsTable");
            userAttTable.removeSelections(true);

            me.userAttachmentControl.open();
        },

        handleTypeMissmatch: function(oEvent) {
            MessageBox.warning(this.oBundle.getText("fileTypeNotSupported"))
        },

        onFileSelection: function(oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var file = oEvent.getParameter("files")[0];

            if (!this.notificationAttachmantsTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.getOwnerComponent().setBusyOn();
            var reader = new FileReader();
            reader.onload = function(readerEvt) {
                binaryString = readerEvt.target.result.split(",")[1];
                fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);
                var fileExt = fileBlob.type.split("/")[1];
                var completeFileName = file.name;
                var indexOfPoint = completeFileName.lastIndexOf('.');
                var fileName = completeFileName.slice(0, indexOfPoint);
                fileName = that.mediaAttachmentsController.getConvertedFilename(fileName);

                that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
            };
            reader.onerror = function(error){
                MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                that.getOwnerComponent().setBusyOff();
            };
            reader.readAsDataURL(file);
            //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            oEvent.getSource().clear();

        },

        onWriteToFileSystem: function(fileName, fileExt, fileBlob, bContext) {
            var that = this;

            var onWriteSuccess = function(filePath) {
                var attachmentObj = that.notificationAttachmantsTabViewModel.getNewAttachmentObject(fileName, fileExt, fileBlob.size.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.getOwnerComponent().setBusyOff();
            };

            var onWriteError = function(msg) {
                that.getOwnerComponent().setBusyOff();
                MessageToast.show(msg);
            };

            this.fileSystem.write(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function(mediaObject, bContext) {
            var that = this;
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];

                    that.notificationAttachmantsTabViewModel.saveUserAttachment(attachmentObj);

                }
            } else {
                that.notificationAttachmantsTabViewModel.saveUserAttachment(mediaObject);
            }
            this.notificationDetailViewModel.loadAllCounters();
        }

    });

});