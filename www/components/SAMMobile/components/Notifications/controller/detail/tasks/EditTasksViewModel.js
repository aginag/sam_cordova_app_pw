sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationTask"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationTask) {
        "use strict";

        // constructor
        function EditTasksViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditTasksViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        activity: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setTask: {
                value: function (task) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = task.getCopy();

                    }

                    this.setProperty("/task", task);
                }
            },

            getTask: {
                value: function () {
                    return this.getProperty("/task");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getTask().setData(this._objectDataCopy);
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditTask") : this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditTask");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditTasksViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditTasksViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation on new activity
            validator.check("task.TASK_CODEGRP", "Please select a Codegroup").isEmpty();
            validator.check("task.TASK_CODE", "Please select a Codegroup").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditTasksViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MOBILE_ID: sap.ui.core.ValueState.None,
                    NOTIF_NO: sap.ui.core.ValueState.None,
                    TASK_KEY: sap.ui.core.ValueState.None,
                    TASK_SORT_NO: sap.ui.core.ValueState.None,
                    TASK_CAT_TYP: sap.ui.core.ValueState.None,
                    TASK_CODE: sap.ui.core.ValueState.None,
                    TASK_CODEGRP: sap.ui.core.ValueState.None,
                    TXT_TASKCD: sap.ui.core.ValueState.None,
                    TASK_TEXT: sap.ui.core.ValueState.None,
                    LONG_TEXT: sap.ui.core.ValueState.None,
                    PLND_START_DATE: sap.ui.core.ValueState.None,
                    PLND_START_TIME: sap.ui.core.ValueState.None,
                    PLND_END_DATE: sap.ui.core.ValueState.None,
                    PLND_END_TIME: sap.ui.core.ValueState.None,
                });
            }
            return this.valueStateModel;
        };

        // Select Dialogs
        EditTasksViewModel.prototype.displayCodeGroupSelectDialog = function (oEvent, viewContext) {
            if (!this._oTaskCodeGroupDialog) {
                this._oTaskCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oTaskCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oTaskCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oTaskCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "2"
                },
                actionName: "valueHelp"
            });
        };

        EditTasksViewModel.prototype.handleCodeGroupSelect = function (oEvent) {
            this._oTaskCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'TASK_CODEGRP': 'CODEGROUP'
            }, {
                "TXT_TASKGRP": "CODEGROUP_TEXT"
            }], this, "/task");
        };

        EditTasksViewModel.prototype.handleCodeGroupSearch = function (oEvent) {
            this._oTaskCodeGroupDialog.search(oEvent, ["TASK_CODEGRP"]);
        };

        EditTasksViewModel.prototype.displayCodingSelectDialog = function (oEvent, viewContext) {
            var that = this;

            if (!this._oTaskCodeDialog) {
                this._oTaskCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oTaskCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oTaskCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oTaskCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/task/TASK_CODEGRP"),
                    catalogType: "2"
                },
                actionName: "valueHelp"
            });
        };

        EditTasksViewModel.prototype.handleCodingSelect = function (oEvent) {

            this._oTaskCodeDialog.select(oEvent, 'CODE', [{
                'TASK_CODE': 'CODE'
            }, {
                "TXT_TASKCD": "CODE_TEXT"
            }], this, "/task");
        };

        EditTasksViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oTaskCodeDialog.search(oEvent, ["TASK_CODE"]);
        };

        EditTasksViewModel.prototype.constructor = EditTasksViewModel;

        return EditTasksViewModel;
    });
