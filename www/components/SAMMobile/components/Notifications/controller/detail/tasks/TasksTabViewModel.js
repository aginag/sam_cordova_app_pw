sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationTask"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationTask) {
        "use strict";

        // constructor
        function TasksTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._formatter = new Formatter(this._component);
            this._NOTIFIACTIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        TasksTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFIACTIONNO = notificationNo;
                }
            },

            setNotification: {
                value: function (notificationNo) {
                    this._NOTIFIACTION = notificationNo;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        TasksTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        TasksTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var longTextInfo = this.scenario.getComplaintNotificationInfo();
            var arrData = data.map(function (attData) {
                var attObject = new NotificationTask(attData, that._requestHelper, that);
                return new Promise(function (resolve, reject) {
                    var load_success = function (data) {
                        attObject.TEXT_LINE = data;
                        resolve(attObject);
                    };

                    attObject.loadLongText(longTextInfo.longTextTaskObjectType).then(load_success);
                });
            });

            Promise.all(arrData).then(function (result) {
                that.setProperty("/tasks", result);
            }).catch(function (err) {
                that.executeErrorCallback(err);
            });
        };

        TasksTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };


            this.setBusy(true);
            this._requestHelper.getData("NotificationTask", {
                notificationNo: this.getNotificationNo(),
                persNo: this.getUserInformation().personnelNumber,
                objectType: this.scenario.getComplaintNotificationInfo().longTextTaskObjectType
            }, load_success, load_error, true);
        };


        TasksTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        TasksTabViewModel.prototype.insertNewTask = function (task, successCb) {
            var that = this;
            var sqlStrings = [];
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
                that.insertToModelPath(task, "/tasks");
                successCb();
            };

            try {
                task.insert(false, function (mobileId) {
                    task.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.i18n.getText("ERROR_NOTIF_INSERT_COMPONENT"))));

                if (task.TEXT_LINE.length > 0) {

                    sqlStrings = sqlStrings.concat(task.addLongtext(false, true, task.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextTaskObjectType));
                }
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_INSERT")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        TasksTabViewModel.prototype.getNewEditTask = function () {
            var taskModelData = new NotificationTask(null, this._requestHelper, this);

            taskModelData.NOTIF_NO = this.getNotificationNo();
            taskModelData.PERS_NO = this.getUserInformation().personnelNumber;
            taskModelData.TEXT_LINE = "";
            taskModelData.TASK_KEY = taskModelData.getRandomCharUUID(4);
            taskModelData.ITEM_KEY = '0000';
            taskModelData.ITEM_SORT_NO = '0000';

            return taskModelData;
        };

        TasksTabViewModel.prototype.updateTask = function (task, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function () {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(task.update(true));

                var load_success = function (data) {
                    Array.prototype.push.apply(sqlStrings, data);
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_UPDATE")));
                    });
                };
                task.updateLongtext(task.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextTaskObjectType).then(load_success);

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        TasksTabViewModel.prototype.deleteTask = function (task, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(task.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(task, "/tasks");
                task.deleteLongtext(this.scenario.getComplaintNotificationInfo().longTextTaskObjectType).then(successCb)
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_DELETE")));
            });
        };

        TasksTabViewModel.prototype.constructor = TasksTabViewModel;

        return TasksTabViewModel;
    });
