sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/detail/tasks/EditTasksViewModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/TextArea",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function(Controller, EditTasksViewModel, Dialog, Button, Text, TextArea, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.tasks.TasksTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailTasks").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailTasks", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.tasksTabViewModel = null;
        },

        onRouteMatched: function(oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.tasksTabViewModel = this.notificationDetailViewModel.tabs.tasks.model;

                this.getView().setModel(this.tasksTabViewModel, "tasksTabViewModel");
            }

            if (this.tasksTabViewModel.needsReload()) {
                this.loadViewModelData();
            }
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailTasks", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.tasksTabViewModel.loadData(function() {

            });
        },

        onNewTask: function(){
            var that = this;

            var onAcceptPressed = function (task, dialog) {
                that.tasksTabViewModel.insertNewTask(task, function () {
                    dialog.close();
                    that.notificationDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };

            this._getTaskEditDialog(that.tasksTabViewModel.getNewEditTask(), false, onAcceptPressed).open();
        },

        _getTaskEditDialog: function (task, bEdit, acceptCb) {
            var that = this;

            var onAcceptPressed = function () {
                var tasksTabViewModel = this.getParent().getModel("taskModel");
                if (tasksTabViewModel.validate()) {
                    acceptCb(tasksTabViewModel.getTask(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var tasksTabViewModel = dialog.getModel("taskModel");
                tasksTabViewModel.rollbackChanges();
                dialog.close();
            };

            var notification = this.tasksTabViewModel.getNotification();

            this.editTasksViewModel = new EditTasksViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTasksViewModel.setEdit(bEdit);
            this.editTasksViewModel.setNotification(notification);
            this.editTasksViewModel.setTask(task);

            if (!this._taskEditPopover) {

                if (!this._taskEditPopover) {
                    this._taskEditPopover = sap.ui.xmlfragment("taskPopup", "SAMMobile.components.Notifications.view.detail.tasks.TaskEditPopover", this);
                }

                this._taskEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_TASK}' : '{i18n>NOTIFICATIONS_NEW_TASK}',
                    content: this._taskEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this.getView().addDependent(this._taskEditPopover);
            }

            this._taskEditPopover.getBeginButton().mEventRegistry.press = [];
            this._taskEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._taskEditPopover.setModel(this.editTasksViewModel, "taskModel");
            this._taskEditPopover.setModel(this.component.getModel('i18n'), "i18n");
            this._taskEditPopover.setModel(this.component.getModel('i18n_core'), "i18n_core");

            this._taskEditPopover.setModel(this.errorMessageModel, "errorMessageModel");
            return this._taskEditPopover;
        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("DELETE_TASk"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                }
            });

            return _oConfirmationDialog;
        },

        handleEditTask: function(oEvent){
            var that = this;
            var task = oEvent.getSource().getBindingContext("tasksTabViewModel").getObject();
            var onAcceptPressed = function (task, dialog) {
                that.tasksTabViewModel.updateTask(task, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_UPDATED_SUCCESSFULLY"));
                });
            };
            this._getTaskEditDialog(task, true, onAcceptPressed).open();
        },

        handleDeleteTask: function(oEvent){
            var that = this;
            var task = oEvent.getSource().getBindingContext("tasksTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.tasksTabViewModel.deleteTask(task, function () {
                    that.notificationDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.i18n.getText("TASK_DELETED_SUCCESSFULLY"));
                });
            }).open();
        },

        displayCodeGroupSearch: function(oEvent){
            this.editTasksViewModel.displayCodeGroupSelectDialog(oEvent, this);
        },

        handleCodeGroupSelect: function(oEvent){
            this.editTasksViewModel.handleCodeGroupSelect(oEvent);
        },

        handleCodeGroupSearch: function(oEvent){
            this.editTasksViewModel.handleCodeGroupSearch(oEvent);
        },

        displayCodeSearch: function(oEvent){
            this.editTasksViewModel.displayCodingSelectDialog(oEvent, this);
        },

        handleCodingSelect: function(oEvent){
            this.editTasksViewModel.handleCodingSelect(oEvent);
        },

        handleCodingSearch: function(oEvent){
            this.editTasksViewModel.handleCodingSearch(oEvent);
        },

    });

});