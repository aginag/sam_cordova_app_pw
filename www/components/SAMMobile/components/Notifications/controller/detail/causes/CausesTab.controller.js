sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/detail/causes/EditCausesViewModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/TextArea",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function(Controller, EditCausesViewModel, Dialog, Button, Text, TextArea, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.causes.CausesTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailCauses").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailCauses", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.causesTabViewModel = null;
        },

        onRouteMatched: function(oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.causesTabViewModel = this.notificationDetailViewModel.tabs.causes.model;

                this.getView().setModel(this.causesTabViewModel, "causesTabViewModel");
            }

            if (this.causesTabViewModel.needsReload()) {
                this.loadViewModelData();
            }

        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailCauses", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.causesTabViewModel.loadData(function() {

            });
        },

        onNewCause: function(){
            var that = this;

            var onAcceptPressed = function (cause, dialog) {
                that.causesTabViewModel.insertNewCause(cause, function () {
                    dialog.close();
                    that.notificationDetailViewModel.loadAllCounters();
                    that.notificationDetailViewModel.refresh();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };
            this._getCauseEditDialog(that.causesTabViewModel.getNewEditCause(), false, onAcceptPressed).open();
        },

        _getCauseEditDialog: function (cause, bEdit, acceptCb) {
            var that = this;

            var onAcceptPressed = function () {
                var causesTabViewModel = this.getParent().getModel("causeModel");
                if (causesTabViewModel.validate()) {
                    acceptCb(causesTabViewModel.getCause(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var causesTabViewModel = dialog.getModel("causeModel");
                causesTabViewModel.rollbackChanges();
                dialog.close();
            };

            var notification = this.causesTabViewModel.getNotification();

            this.editCausesViewModel = new EditCausesViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editCausesViewModel.setEdit(bEdit);
            this.editCausesViewModel.setNotification(notification);
            this.editCausesViewModel.setCause(cause);

            if (!this._causeEditPopover) {

                if (!this._causeEditPopover) {
                    this._causeEditPopover = sap.ui.xmlfragment("causePopup", "SAMMobile.components.Notifications.view.detail.causes.CauseEditPopover", this);
                }

                this._causeEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_CAUSE}' : '{i18n>NOTIFICATIONS_NEW_CAUSE}',
                    content: this._causeEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this.getView().addDependent(this._causeEditPopover);
            }

            this._causeEditPopover.getBeginButton().mEventRegistry.press = [];
            this._causeEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._causeEditPopover.setModel(this.editCausesViewModel, "causeModel");
            return this._causeEditPopover;
        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("DELETE_CAUSE"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                }
            });

            return _oConfirmationDialog;
        },

        handleEditCause: function(oEvent){
            var that = this;
            var cause = oEvent.getSource().getBindingContext("causesTabViewModel").getObject();
            var onAcceptPressed = function (cause, dialog) {
                that.causesTabViewModel.updateCause(cause, function () {
                    dialog.close();
                    MessageToast.show("Updated successfully");
                });
            };
            this._getCauseEditDialog(cause, true, onAcceptPressed).open();
        },

        handleDeleteCause: function(oEvent){
            var that = this;
            var cause = oEvent.getSource().getBindingContext("causesTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.causesTabViewModel.deleteCause(cause, function () {
                    that.notificationDetailViewModel.loadAllCounters();
                    MessageToast.show("Cause deleted successfully");
                });
            }).open();

            this.notificationDetailViewModel.loadAllCounters();
        },

        displayCodeGroupSearch: function(oEvent){
            this.editCausesViewModel.displayCodeGroupSelectDialog(oEvent, this);
        },

        handleCodeGroupSelect: function(oEvent){
            this.editCausesViewModel.handleCodeGroupSelect(oEvent);
        },

        handleCodeGroupSearch: function(oEvent){
            this.editCausesViewModel.handleCodeGroupSearch(oEvent);
        },

        displayCodeSearch: function(oEvent){
            this.editCausesViewModel.displayCodingSelectDialog(oEvent, this);
        },

        handleCodingSelect: function(oEvent){
            this.editCausesViewModel.handleCodingSelect(oEvent);
        },

        handleCodingSearch: function(oEvent){
            this.editCausesViewModel.handleCodingSearch(oEvent);
        },

    });

});