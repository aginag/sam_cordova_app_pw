sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationCause"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationCause) {
        "use strict";

        // constructor
        function CausesTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._formatter = new Formatter(this._component);
            this._NOTIFIACTIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CausesTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFIACTIONNO = notificationNo;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        CausesTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CausesTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var arrData = data.map(function (attData) {
                var attObject = new NotificationCause(attData, that._requestHelper, that);
                return attObject;
            });
            this.setProperty("/causes", arrData);
        };

        CausesTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {

                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };


            this.setBusy(true);
            this._requestHelper.getData("NotificationCause", {
                notificationNo: this.getNotificationNo(),
                persNo: this.getUserInformation().personnelNumber,
                objectType: this.scenario.getComplaintNotificationInfo().longTextCauseObjectType
            }, load_success, load_error, true);
        };


        CausesTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        CausesTabViewModel.prototype.insertNewCause = function (cause, successCb) {
            var that = this;
            var sqlStrings = [];
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
                that.insertToModelPath(cause, "/causes");
                successCb();
            };

            try {
                cause.insert(false, function (mobileId) {
                    cause.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.i18n.getText("ERROR_CAUSE_INSERT"))));

                if (cause.TEXT_LINE.length > 0) {

                    sqlStrings = sqlStrings.concat(cause.addLongtext(false, true, cause.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextCauseObjectType));
                }
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_INSERT")));
                });

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CausesTabViewModel.prototype.getNewEditCause = function () {
            var causeModelData = new NotificationCause(null, this._requestHelper, this);

            causeModelData.NOTIF_NO = this.getNotificationNo();
            causeModelData.PERS_NO = this.getUserInformation().personnelNumber;
            causeModelData._longText = "";
            causeModelData.CAUSE_KEY = causeModelData.getRandomCharUUID(4);
            causeModelData.ITEM_KEY = '0000';
            causeModelData.ITEM_SORT_NO = '0000';

            return causeModelData;
        };

        CausesTabViewModel.prototype.updateCause = function (cause, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(cause.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CausesTabViewModel.prototype.deleteCause = function (cause, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(cause.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(cause, "/causes");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_DELETE")));
            });
        };

        CausesTabViewModel.prototype.constructor = CausesTabViewModel;

        return CausesTabViewModel;
    });
