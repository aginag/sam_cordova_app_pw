sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationCause"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationCause) {
        "use strict";

        // constructor
        function EditCausesViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditCausesViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        activity: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setCause: {
                value: function (cause) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = cause.getCopy();

                    }

                    this.setProperty("/cause", cause);
                }
            },

            getCause: {
                value: function () {
                    return this.getProperty("/cause");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getCause().setData(this._objectDataCopy);
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditCause") : this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditCause");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditCausesViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditCausesViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation on new activity
            validator.check("cause.CAUSE_CODEGRP", "Please select a Codegroup").isEmpty();
            validator.check("cause.CAUSE_CODE", "Please select a Code").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditCausesViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MOBILE_ID: sap.ui.core.ValueState.None,
                    NOTIF_NO: sap.ui.core.ValueState.None,
                    CAUSE_KEY: sap.ui.core.ValueState.None,
                    CAUSE_SORT_NO: sap.ui.core.ValueState.None,
                    CAUSE_CAT_TYP: sap.ui.core.ValueState.None,
                    CAUSE_CODE: sap.ui.core.ValueState.None,
                    CAUSE_CODEGRP: sap.ui.core.ValueState.None,
                    TXT_CAUSECD: sap.ui.core.ValueState.None,
                    TXT_CAUSEGRP: sap.ui.core.ValueState.None,
                    CAUSE_TEXT: sap.ui.core.ValueState.None,
                    LONG_TEXT: sap.ui.core.ValueState.None,
                });
            }
            return this.valueStateModel;
        };

        // Select Dialogs
        EditCausesViewModel.prototype.displayCodeGroupSelectDialog = function (oEvent, viewContext) {
            if (!this._oCauseCodeGroupDialog) {
                this._oCauseCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCauseCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCauseCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCauseCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "5"
                },
                actionName: "valueHelp"
            });
        };

        EditCausesViewModel.prototype.handleCodeGroupSelect = function (oEvent) {
            this._oCauseCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'CAUSE_CODEGRP': 'CODEGROUP'
            }, {
                "TXT_CAUSEGRP": "CODEGROUP_TEXT"
            }], this, "/cause");
        };

        EditCausesViewModel.prototype.handleCodeGroupSearch = function (oEvent) {
            this._oCauseCodeGroupDialog.search(oEvent, ["CAUSE_CODEGRP"]);
        };

        EditCausesViewModel.prototype.displayCodingSelectDialog = function (oEvent, viewContext) {
            var that = this;

            if (!this._oCauseCodeDialog) {
                this._oCauseCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCauseCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCauseCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCauseCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/cause/CAUSE_CODEGRP"),
                    catalogType: "5"
                },
                actionName: "valueHelp"
            });
        };

        EditCausesViewModel.prototype.handleCodingSelect = function (oEvent) {

            this._oCauseCodeDialog.select(oEvent, 'CODE', [{
                'CAUSE_CODE': 'CODE'
            }, {
                "CAUSE_CAT_TYP": "CATALOG_TYPE"
            },{
                "TXT_CAUSECD": "CODE_TEXT"
            }], this, "/cause");
        };

        EditCausesViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oCauseCodeDialog.search(oEvent, ["CAUSE_CODE"]);
        };

        EditCausesViewModel.prototype.constructor = EditCausesViewModel;

        return EditCausesViewModel;
    });
