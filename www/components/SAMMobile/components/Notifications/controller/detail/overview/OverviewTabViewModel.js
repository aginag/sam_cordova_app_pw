sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/components/Notifications/controller/detail/NotificationDetailViewModel",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, NotificationHeader, NotificationDetailViewModel, GenericSelectDialog) {
        "use strict";

        // constructor
        function OverviewTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._NOTIFIACTIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        OverviewTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFIACTIONNO = notificationNo;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            setNotification: {
                value: function (notification) {
                    this.setProperty("/notification", notification);
                }
            },

            getNotification: {
                value: function () {
                    this.getProperty("/notification");
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setDataFromNotification: {
                value: function (notification) {
                    this.setProperty("/functionalLocation", {
                        id: notification.FUNCT_LOC,
                        description: notification.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: notification.EQUIPMENT,
                        description: notification.EQUIPMENT_DESC
                    });
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        OverviewTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        OverviewTabViewModel.prototype.setNewData = function (data) {
            this.setProperty("/notification", data);
            var notification = new NotificationHeader(data[0], this._requestHelper, this);
            this.setNotification(notification);
        };

        OverviewTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            this.setBusy(true);
            load_success();
        };


        OverviewTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        OverviewTabViewModel.prototype.loadFuncLocHistory = function (successCb) {

            var that = this;

            var load_success = function (data) {
                that.setProperty("/functionalLocation/orderHistory", data.FunctionLocOrderHist);
                that.setProperty("/functionalLocation/notifHistory", data.FunctionLocNotifHist);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb(new JSONModel({
                        orderHistory: that.getProperty("/functionalLocation/orderHistory"),
                        notifHistory: that.getProperty("/functionalLocation/notifHistory")
                    }));
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOCATION_HISTORY_LOADING")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["FunctionLocNotifHist", "FunctionLocOrderHist"], {
                funcLocId: this.getFuncLocId()
            }, load_success, load_error, true);
        };

        OverviewTabViewModel.prototype.loadEquipmentHistory = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/equipment/orderHistory", data.EquipmentOrderHist);
                that.setProperty("/equipment/notifHistory", data.EquipmentNotifHist);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb(new JSONModel({
                        orderHistory: that.getProperty("/equipment/orderHistory"),
                        notifHistory: that.getProperty("/equipment/notifHistory")
                    }));
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_EQUIPMENT_HISTORY_LOADING")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["EquipmentNotifHist", "EquipmentOrderHist"], {
                equipmentNumber: this.getEquipmentId()
            }, load_success, load_error, true);
        };

        OverviewTabViewModel.prototype.constructor = OverviewTabViewModel;

        return OverviewTabViewModel;
    });
