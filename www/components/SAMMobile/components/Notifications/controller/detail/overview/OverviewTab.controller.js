sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/Button",
    "sap/ui/core/routing/History"
], function (Controller,
             JSONModel,
             formatter,
             formatterComp,
             Dialog,
             Text,
             Button,
             History) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.overview.OverviewTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailOverview").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailOverview", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.overviewTabViewModel = null;

            this._oHistoryDialog = sap.ui.xmlfragment("notificationHistoryDialog1", "SAMMobile.components.Notifications.view.detail.overview.HistoryPopover", this);
            this.getView().addDependent(this._oHistoryDialog);
        },

        onRouteMatched: function (oEvent) {
            var oArgs = oEvent.getParameter("arguments");
            var notificationNo = oArgs.id;
            var persNo = oArgs.persNo;

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.overviewTabViewModel = this.notificationDetailViewModel.tabs.overview.model;

                this.getView().setModel(this.overviewTabViewModel, "overviewTabViewModel");
            }

            this.loadOverviewTabData();
        },

        loadOverviewTabData: function () {
            if (this.overviewTabViewModel.needsReload()) {
                this.loadViewModelData();
                this.globalViewModel.setComponentHeaderText(this.notificationDetailViewModel.getHeaderText());
            }
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailOverview", "refreshRoute", this.onRefreshRoute, this);
        },

        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();
            this.oRouter.navTo("notifications");
            this.notificationDetailViewModel.resetData();
            this.getView().getParent().backToTop();
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.overviewTabViewModel.loadData(function () {

            });
        },

        onNavigateToLocationPressed: function (oEvent) {
            //this.notificationDetailViewModel.navigateToLocation(oEvent);
        },

        onGISNavigationPressed: function () {
            //var funcLocId = this.overviewTabViewModel.getFuncLocId();
            //cordova.InAppBrowser.open('https://giswebstg.dewa.gov.ae/mwfgisviewer/?query=MWF_GIS_9311_1,E_EQUIPMENT.FUNCTIONAL_LOCATION,' + funcLocId, '_blank', 'location=yes');
        },

        onNavToFuncLocPress: function (oEvent) {
            var notNo = this.notificationDetailViewModel.getNotificationNo();
            var func = this.overviewTabViewModel.getFuncLocId();

            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "notificationsCustomRoute",
                params: {
                    query: {
                        routeName: "notificationDetailRoute",
                        params: JSON.stringify({
                            id: this.notificationDetailViewModel.getNotificationNo(),
                            persNo: this.notificationDetailViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: this.overviewTabViewModel.getFuncLocId(),
                            objectType: "1"
                        })
                    }
                }
            });

            this.notificationDetailViewModel.resetData();
        },

        onDisplayFuncLocHistory: function (oEvent) {
            var src = oEvent.getSource();
            var that = this;

            var load_success = jQuery.proxy(function (model) {
                that._oHistoryDialog.setModel(model, "historyModel");
                that._oHistoryDialog.openBy(src);
            }, this);

            this.overviewTabViewModel.loadFuncLocHistory(load_success);
        },

        onNavToEquipmentPress: function (oEvent) {
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "notificationsCustomRoute",
                params: {
                    query: {
                        routeName: "notificationDetailRoute",
                        params: JSON.stringify({
                            id: this.notificationDetailViewModel.getNotificationNo(),
                            persNo: this.notificationDetailViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: this.overviewTabViewModel.getEquipmentId(),
                            objectType: "0"
                        })
                    }
                }
            });

            this.notificationDetailViewModel.resetData();
        },

        onDisplayEquipmentHistory: function (oEvent) {
            var src = oEvent.getSource();
            var that = this;

            var load_success = jQuery.proxy(function (model) {
                that._oHistoryDialog.setModel(model, "historyModel");
                that._oHistoryDialog.openBy(src);
            }, this);

            this.overviewTabViewModel.loadEquipmentHistory(load_success);
        },

        goToHistoricalOrder: function (oEvent) {
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                    route: "notificationsCustomRoute",
                    params: {
                        query: {
                            routeName: "notificationDetailRoute",
                            params: JSON.stringify({
                                id: this.notificationDetailViewModel.getNotificationNo(),
                                persNo: this.notificationDetailViewModel.getUserInformation().personnelNumber
                            })
                        }
                    }
                },{
                    route: "workOrdersCustomRoute",
                    params: {
                        query: {
                            routeName: "workOrderDetailRoute",
                            params: JSON.stringify({
                                id: oEvent.getSource().getBindingContext('historyModel').getProperty().ORDERID,
                                opId: oEvent.getSource().getBindingContext('historyModel').getProperty().ACTIVITY,
                                persNo: oEvent.getSource().getBindingContext('historyModel').getProperty().PERS_NO
                            })
                        }
                    }
                });

                this.notificationDetailViewModel.resetData();
        },

        onNavToNotificationPress: function (oEvent) {
            this._oHistoryDialog.close();

            this.notificationDetailViewModel.resetData();
            this.oRouter.navTo("notificationDetailRoute", {
                id: oEvent.getSource().getBindingContext("historyModel").getObject().NOTIF_NO,
                persNo: this.notificationDetailViewModel.getUserInformation().personnelNumber
            }, false);
            },
    });

});