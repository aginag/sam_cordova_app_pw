sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/detail/activities/EditActivitiesViewModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/TextArea",
    "SAMMobile/helpers/formatter",
    "sap/ui/model/json/JSONModel",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function (Controller, EditActivitiesViewModel, Dialog, Button, Text, TextArea, formatter, JSONModel, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.activities.ActivitiesTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailActivities").attachPatternMatched(this.onRouteMatched, this);
            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailActivities", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
            this.editActivitiesViewModel = new EditActivitiesViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.notificationDetailViewModel = null;
            this.activitiesTabViewModel = null;
        },

        onRouteMatched: function (oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.activitiesTabViewModel = this.notificationDetailViewModel.tabs.activities.model;

                this.getView().setModel(this.activitiesTabViewModel, "activitiesTabViewModel");
            }

            if (this.activitiesTabViewModel.needsReload()) {
                this.loadViewModelData();
            }

        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailActivities", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.activitiesTabViewModel.loadData(function () {

            });
        },

        onNewActivity: function () {
            var that = this;

            var onAcceptPressed = function (activity, dialog) {
                that.activitiesTabViewModel.insertNewActivity(activity, function () {
                    dialog.close();
                    MessageToast.show("Inserted successfully");
                });
            };

            this.notificationDetailViewModel.loadAllCounters();

            this._getActivityEditDialog(that.activitiesTabViewModel.getNewEditActivity(), false, onAcceptPressed).open();
        },

        handleEditActivity: function (oEvent) {
            var that = this;
            var activity = oEvent.getSource().getBindingContext("activitiesTabViewModel").getObject();
            var onAcceptPressed = function (activity, dialog) {
                that.activitiesTabViewModel.updateActivity(activity, function () {
                    dialog.close();
                    MessageToast.show("Updated successfully");
                });
            };
            this._getActivityEditDialog(activity, true, onAcceptPressed).open();
        },

        handleDeleteActivity: function (oEvent) {
            var that = this;
            var activity = oEvent.getSource().getBindingContext("activitiesTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.activitiesTabViewModel.deleteActivity(activity, function () {
                    MessageToast.show("Activity deleted successfully");
                });
            }).open();

            this.notificationDetailViewModel.loadAllCounters();
        },

        _getActivityEditDialog: function (activity, bEdit, acceptCb) {
            var that = this;
            var onAcceptPressed = function () {
                var activitiesTabViewModel = this.getParent().getModel("activityModel");
                if (activitiesTabViewModel.validate()) {
                    acceptCb(activitiesTabViewModel.getActivity(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var activitiesTabViewModel = dialog.getModel("activityModel");
                activitiesTabViewModel.rollbackChanges();
                dialog.close();
            };

            var notification = this.activitiesTabViewModel.getNotification();

            this.editActivitiesViewModel = new EditActivitiesViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editActivitiesViewModel.setEdit(bEdit);
            this.editActivitiesViewModel.setNotification(notification);
            this.editActivitiesViewModel.setActivity(activity);

            if (!this._activityEditPopover) {

                if (!this._activityEditPopover) {
                    this._activityEditPopover = sap.ui.xmlfragment("activityEditPopover", "SAMMobile.components.Notifications.view.detail.activities.ActivityEditPopover", this);
                }

                this._activityEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Activity",
                    content: this._activityEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });
                this.getView().addDependent(this._activityEditPopover);
            }

            this._activityEditPopover.getBeginButton().mEventRegistry.press = [];
            this._activityEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._activityEditPopover.setModel(this.editActivitiesViewModel, "activityModel");
            return this._activityEditPopover;
        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("deleteActivity"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("Yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("No"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                }
            });

            return _oConfirmationDialog;
        },

        displayCodeGroupSelectDialog: function (oEvent) {
            this.editActivitiesViewModel.displayCodeGroupSelectDialog(oEvent, this);
        },

        handleCodeGroupSelect: function (oEvent) {
            this.editActivitiesViewModel.handleCodeGroupSelect(oEvent);
        },

        handleCodeGroupSearch: function (oEvent) {
            this.editActivitiesViewModel.handleCodeGroupSearch(oEvent);
        },

        displayCodeSelectDialog: function (oEvent) {
            this.editActivitiesViewModel.displayCodeSelectDialog(oEvent, this);
        },

        handleCodingSelect: function (oEvent) {
            this.editActivitiesViewModel.handleCodeSelect(oEvent);
        },

        handleCodingSearch: function (oEvent) {
            this.editActivitiesViewModel.handleCodeSearch(oEvent);
        },

    });

});