sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationActivity"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationActivity) {
        "use strict";

        // constructor
        function ActivitiesTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._NOTIFIACTIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        ActivitiesTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFIACTIONNO = notificationNo;
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function(){
                    return this._NOTIFIACTION;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            getNotificationSQLChangeString: {
                value: function () {
                    return this.getNotification().setChanged(true);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        ActivitiesTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        ActivitiesTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var arrData = data.map(function (attData) {
                var attObject = new NotificationActivity(attData, that._requestHelper, that);
                return attObject;
            });

            this.setProperty("/activities", arrData);
        };

        ActivitiesTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while loading data for overview tab"));
            };

            this.setBusy(true);
            this._requestHelper.getData("NotificationActivity", {
                notificationNo: this.getNotificationNo(),
                persNo: this.getUserInformation().personnelNumber
            }, load_success, load_error, true);
        };


        ActivitiesTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        ActivitiesTabViewModel.prototype.insertNewActivity = function (activity, successCb) {
            var that = this;
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            try {
                activity.LONG_TEXT = activity.longTextMl;
            } catch (err){

            }

            var onSuccess = function (asd) {
                that.insertToModelPath(activity, "/activities");
                successCb();
            };

            try {
                activity.insert(false, function (mobileId) {
                    activity.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error("Error while trying to insert new component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ActivitiesTabViewModel.prototype.updateActivity = function (activity, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(activity.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error("Error while trying to update activity"));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ActivitiesTabViewModel.prototype.getNewEditActivity = function () {

            var activityModelData = new NotificationActivity(null, this._requestHelper, this);

            activityModelData.NOTIF_NO = this.getNotificationNo();
            activityModelData.PERS_NO = this.getUserInformation().personnelNumber;

            return activityModelData;
        };

        ActivitiesTabViewModel.prototype.deleteActivity = function (activity, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(activity.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(activity, "/activities");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error("Error while trying to delete activity"));
            });
        };

        ActivitiesTabViewModel.prototype.constructor = ActivitiesTabViewModel;

        return ActivitiesTabViewModel;
    });
