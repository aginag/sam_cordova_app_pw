sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationActivity"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationActivity) {
        "use strict";

        // constructor
        function EditActivitiesViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditActivitiesViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        activity: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setActivity: {
                value: function (activity) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = activity.getCopy();

                    }

                    this.setProperty("/activity", activity);
                }
            },

            getActivity: {
                value: function () {
                    return this.getProperty("/activity");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getActivity().setData(this._objectDataCopy);
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("EditActivity") : this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("NewActivity");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditActivitiesViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };


        EditActivitiesViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation on new activity
            validator.check("activity.ACT_CODEGRP", "Please select a Codegroup").isEmpty();
            validator.check("activity.ACT_CODE", "Please select a Code").isEmpty();
            validator.check("activity.START_TIME", "Please select a Starttime").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
        EditActivitiesViewModel.prototype.displayCodeGroupSelectDialog = function (oEvent, viewContext) {
            if (!this._oCodeGroupDialog) {
                this._oCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification().CATPROFILE,
                    catalogType: "D"
                },
                actionName: "valueHelp"
            });
        };

        EditActivitiesViewModel.prototype.handleCodeGroupSelect = function (oEvent) {
            this._oCodeGroupDialog.select(oEvent, 'CODE_CAT_GROUP', [{
                'ACT_CODEGRP': 'CODEGROUP'
            }], this, "/activity");
        };

        EditActivitiesViewModel.prototype.handleCodeGroupSearch = function (oEvent) {
            this._oCodeGroupDialog.search(oEvent, ["ACT_CODEGRP"]);
        };

        EditActivitiesViewModel.prototype.displayCodeSelectDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oCodeDialog) {
                this._oCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/activity/ACT_CODEGRP"),
                    catalogType: "D"
                },
                actionName: "valueHelp"
            });
        };

        EditActivitiesViewModel.prototype.handleCodeSelect = function (oEvent) {

            this._oCodeDialog.select(oEvent, 'CODE', [{
                'ACT_CODE': 'CODE'
            }, {
                'ACTTEXT': 'CODE_TEXT'
            }], this, "/activity");
        };

        EditActivitiesViewModel.prototype.handleCodeSearch = function (oEvent) {
            this._oCodeDialog.search(oEvent, ["ACT_CODE"]);
        };

        EditActivitiesViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    ACTION_FLAG: sap.ui.core.ValueState.None,
                    ACTTEXT: sap.ui.core.ValueState.None,
                    ACT_CAT_TYP: sap.ui.core.ValueState.None,
                    ACT_CODE: sap.ui.core.ValueState.None,
                    ACT_CODEGRP: sap.ui.core.ValueState.None,
                    ACT_KEY: sap.ui.core.ValueState.None,
                    END_DATE: sap.ui.core.ValueState.None,
                    END_TIME: sap.ui.core.ValueState.None,
                    ITEM_KEY: sap.ui.core.ValueState.None,
                    ITEM_SORT_NO: sap.ui.core.ValueState.None,
                    LONG_TEXT: sap.ui.core.ValueState.None,
                    MOBILE_ID: sap.ui.core.ValueState.None,
                    NOTIF_NO: sap.ui.core.ValueState.None,
                    START_DATE: sap.ui.core.ValueState.None,
                    START_TIME: sap.ui.core.ValueState.None
                });
            }
            return this.valueStateModel;
        };

        EditActivitiesViewModel.prototype.constructor = EditActivitiesViewModel;

        return EditActivitiesViewModel;
    });
