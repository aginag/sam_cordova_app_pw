sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function(Controller, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.partners.PartnersTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailPartners").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailPartners", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.partnersTabViewModel = null;
        },

        onRouteMatched: function(oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.partnersTabViewModel = this.notificationDetailViewModel.tabs.partners.model;

                this.getView().setModel(this.partnersTabViewModel, "partnersTabViewModel");
            }

            if (this.partnersTabViewModel.needsReload()) {
                this.loadViewModelData();
            }
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailPartners", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.partnersTabViewModel.loadData(function() {

            });
        },

        onAddToContactsPressed: function(){

        },

        onBpNavigationPress: function(){

        },

        handleBpEdit: function() {

        },

        handleBpDelete: function(){

        },

        onNewBpPressed: function(){

        },

        onAddAllToContactsPressed: function(){

        }

    });

});