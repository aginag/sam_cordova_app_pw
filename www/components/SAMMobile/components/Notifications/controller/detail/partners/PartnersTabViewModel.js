sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/models/dataObjects/Partner"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationHeader, Partner) {
        "use strict";

        // constructor
        function PartnersTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._NOTIFICATIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        PartnersTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFICATIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFICATIONNO = notificationNo;
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFICATION = notification;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFICATIONNO;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFICATION;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        PartnersTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        PartnersTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var arrData = data.map(function (attData) {
                var attObject = new Partner(attData, that._requestHelper, that);
                return attObject;
            });
            this.setProperty("/partners", arrData);
        };

        PartnersTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };

            this.setBusy(true);
            this._requestHelper.getData("NotificationPartner", {
                objNr: this.getNotification().OBJECT_NO,
                persNo: this.getUserInformation().personnelNumber
            }, load_success, load_error, true);
        };


        PartnersTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        PartnersTabViewModel.prototype.constructor = PartnersTabViewModel;

        return PartnersTabViewModel;
    });
