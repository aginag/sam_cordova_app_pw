sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator) {
        "use strict";

        // constructor
        function CompleteNotificationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this.valueStateModel = null;
            this.summaryInfo = this.scenario.getSummaryInfo();
            this.setProperty("/view/custSideVisible", this.summaryInfo.custSideVisible);
            this.setProperty("/view/dewaSideVisible", this.summaryInfo.dewaSideVisible);
            this.setProperty("/view/moveToVisible", this.summaryInfo.moveToVisible);
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CompleteNotificationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        order: null,
                        _longText: "",
                        view: {
                            busy: false,
                            edit: false,
                            custSideVisible: false,
                            dewaSideVisible: false,
                            moveToVisible: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setNotification: {
                value: function (notification) {
                    notification._dewaSide = false;
                    notification._custSide = false;
                    notification._moveTo = null;
                    this.setProperty("/notification", notification);
                }
            },

            getNotification: {
                value: function () {
                    return this.getProperty("/notification");
                }
            },

            getScenario: {
                value: function () {
                    return this._component.scenario;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("CompleteNotification");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        CompleteNotificationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CompleteNotificationViewModel.prototype.saveChanges = function (successCb, errorCb) {
//         //Save remarks (longtext)
//         if (this.getProperty("/_longText").length == 0) {
//            successCb();
//            return;
//         }

            var sqlStrings = [], that = this;
            var notification = this.getNotification();

            var scenarioLongtextDetails = this.getScenario().getDescriptionTabLongtextInformation();

            if (!scenarioLongtextDetails.readOnly) {
                sqlStrings = sqlStrings.concat(notification.addLongtext(true, this.getProperty("/_longText"), this.scenario.getDescriptionTabLongtextInformation().objectType));
            }

            //Set customer and dewa side statuses
            if (notification._dewaSide) {
                sqlStrings.push(notification.insertNewUserStatus(this.getUserStatusMl('E0010', notification.XA_STAT_PROF)));
            }

            if (notification._custSide) {
                sqlStrings.push(notification.insertNewUserStatus(this.getUserStatusMl('E0011', notification.XA_STAT_PROF)));
            }
            //Move to statuses
            var statusCode = '';
            switch (notification._moveTo) {
                case 0:
                    statusCode = 'E0011';
                    break;
                case 1:
                    statusCode = 'E0012';
                    break;
            }
            if (statusCode !== '') {
                var statusMl = this.getUserStatusMl(statusCode, notification.OPERATION_STATUS_PROFILE);
                notification.setOperationsUserStatus(statusMl, function () {
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error(that._component.i18n.getText("ERROR_ORDER_LONGTEXTS_INSERT"))));
                }, errorCb.bind(null, new Error(this._component.i18n.getText("ERROR_STATUS_INSERT"))));
            } else {
                that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error(that._component.i18n.getText("ERROR_ORDER_LONGTEXTS_INSERT"))));
            }
            //
            //this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error("Error while trying to insert order longtexts")));
        };

        CompleteNotificationViewModel.prototype.loadNotificationData = function (errorCb) {
            var that = this;
            this.setBusy(true);

            this.getNotification().loadAll(function () {
                that.setBusy(false);
                that.setProperty("/_longText", that.getNotification().getLongText(that.scenario.getListItemLongtextInformation().objectType));
                that.refresh();
            }, function () {
                that.setBusy(false);
                errorCb(new Error(that._component.i18n.getText("ERROR_NOTIF_DATA_LOAD")));
            });
        };

        CompleteNotificationViewModel.prototype.constructor = CompleteNotificationViewModel;

        return CompleteNotificationViewModel;
    });
