sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/detail/NotificationDetailViewModel",
    "SAMMobile/components/Notifications/controller/detail/CompleteNotificationViewModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "sap/m/Button",
    "sap/m/Dialog",
    'sap/m/Text'
], function (Controller,
             NotificationDetailViewModel,
             CompleteNotificationViewModel,
             MessageBox,
             MessageToast,
             formatter,
             formatterComp,
             Button,
             Dialog,
             Text) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.NotificationDetail", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailRoute").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore()
                .getEventBus()
                .subscribe("home", "onNavToScenario", this.onNavToScenario, this);

            this.iconTabBar = this.byId("idIconTabBarNotifications");
            this.initialized = false;

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.completeNotificationViewModel = null;

        },

        onRouteMatched: function (oEvent) {

            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            var notificationNo = oArgs.id;
            var persNo = oArgs.persNo;

            var onMasterDataLoaded = function (routeName) {
                that.globalViewModel.setComponentBackNavEnabled(true);
                that.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
                that.globalViewModel.setCurrentRoute(routeName);

                if (!that.notificationDetailViewModel) {
                    that.notificationDetailViewModel = new NotificationDetailViewModel(that.component.requestHelper, that.globalViewModel, that.component);
                    that.notificationDetailViewModel.setErrorCallback(that.onViewModelError.bind(that));

                    that.getView().setModel(that.notificationDetailViewModel, "notificationDetailViewModel");
                }

                that.notificationDetailViewModel.setNotificationNo(notificationNo, persNo);

                that.loadViewModelData(false, that.onAfterInitialLoad.bind(that, oArgs));

            };


            if (persNo == undefined || persNo == "" || persNo == null) {
                this.onNavBack();
                return;
            }

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function () {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                that.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("home", "onNavToScenario", this.onNavToScenario, this);
        },

        onRefreshRoute: function () {
            jQuery.sap.log.info("PriSec - Detail Controller - Refresh Event triggered");
            this.loadViewModelData(true);
        },

        loadMasterData: function (successCb) {

            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onNavToTime: function () {
      /*      if (this.notificationDetailViewModel.getTabRouteName() == this.workOrderDetailViewModel.tabs.time.routeName) {
                return;
            }

            var orderId = this.notificationDetailViewModel.getOrderId();
            var persNo = this.notificationDetailViewModel._PERS_NO;

            this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.time.actionName);
            this.oRouter.navTo(this.notificationDetailViewModel.tabs.time.routeName, {
                id: orderId,
                persNo: persNo
            }, true);*/
        },

        onNavToScenario: function () {

            this.notificationDetailViewModel.resetData();
        },

        onAfterInitialLoad: function (oArgs) {
            var route;

            if (oArgs["?query"] != undefined) {
                var tab = this.notificationDetailViewModel.getTabByRouteName(oArgs["?query"].directTo);

                if (!tab) {
                    tab = this.notificationDetailViewModel.tabs.overview;
                }

                route = tab.routeName;
                this.notificationDetailViewModel.setSelectedTabKey(tab.actionName);
            } else {
                route = this.notificationDetailViewModel.tabs.overview.routeName;
                this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.overview.actionName);
            }

            this.oRouter.navTo(route, {
                id: oArgs.id,
                persNo: oArgs.persNo
            }, true);
        },

        loadViewModelData: function (bSync, successCb) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;


            var onReloadSuccess = function (order) {
                that.globalViewModel.setComponentHeaderText(that.notificationDetailViewModel.getHeaderText());

                that.notificationDetailViewModel.loadAllCounters();
                if (successCb) {
                    successCb();
                }
            };

            if (bSync) {
                this.notificationDetailViewModel.refreshData(onReloadSuccess);
            } else {
                this.notificationDetailViewModel.loadData(onReloadSuccess);
            }
        },

        onTabSelect: function (oEvent) {
            var routeName = this.notificationDetailViewModel.getTabRouteName();
            var notificationNo = this.notificationDetailViewModel.getNotificationNo();
            var persNo = this.notificationDetailViewModel._PERS_NO;

            this.oRouter.navTo(routeName, {
                id: notificationNo,
                persNo: persNo
            }, true);
        },

        onNavBack: function () {
            var that = this;

            setTimeout(function () {
                that.notificationDetailViewModel.resetData();
            }, 200);

            window.history.go(-1);
        },

        onViewModelError: function (err) {

            MessageBox.error(err.message);

            if (err.name == "RETURN_TO_LIST") {
                this.onNavBack();
            }
        },

        onStatusButtonPressed: function (oEvt) {
            var that = this;

            var customData = oEvt.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error(that._component.i18n.getText("ERROR_BUTTON_STATUS_READ")));
                return;
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");

            var onActionConfirmed = function () {
                that.notificationDetailViewModel.setOrderStatus(status, statusProfile, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }
                    var syncMode = statusObject.async ? "" : "S";

                    if (bStopwatchStopped) {
                        that._getConfirmActionDialog(that.component.i18n.getText("timeEntryCreateConfirmation"), that.component.sync.bind(that.component, syncMode)).open();
                        return;
                    }

                    that.component.sync(syncMode);
                });
            };


            if (this.notificationDetailViewModel.statusIsReject(status)) {
                this.displayRejectionReasonSearch(oEvt);
            } else if (this.notificationDetailViewModel.statusIsComplete(status)) {
                // get complete dialog
                this._getCompleteOrderDialog(onActionConfirmed).open();
            } else {
                this._getConfirmActionDialog(this.component.i18n.getText("changeOrderStatusConfirmation"), onActionConfirmed).open();
            }
        },

        displayRejectionReasonSearch: function (oEvent) {
            this.rejectionDialogViewModel = new RejectionReasonDialogViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            var operationStatusProfile = this.notificationDetailViewModel.getOperationStatusProfileForOrder();

            this.rejectionDialogViewModel.setStatusProfile(operationStatusProfile);
            this.rejectionDialogViewModel.display(oEvent, this);
        },

        handleUserStatusSearch: function (oEvent) {
            this.rejectionDialogViewModel.handleSearch(oEvent);
        },

        handleUserStatusSelect: function (oEvent) {
            var that = this;
            this.rejectionDialogViewModel.handleSelect(oEvent, function (status, statusProfile) {
                that.notificationDetailViewModel.setOrderStatus(status, statusProfile, function () {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                });
            });
        },

        onCompleteDialogObjectsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.objects.actionName);
            this.onTabSelect();
        },

        onCompleteDialogComponentsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.material.actionName);
            this.onTabSelect();
        },

        onCompleteDialogTimeEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.time.actionName);
            this.onTabSelect();
        },

        onCompleteDialogSiteDetailsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.notificationDetailViewModel.setSelectedTabKey(this.notificationDetailViewModel.tabs.site.actionName);
            this.onTabSelect();
        },

        _getConfirmActionDialog: function (message, executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: message,
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        _getCompleteOrderDialog: function (executeCb) {
            var that = this;

            this.completeOrderViewModel = new CompleteOrderViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.completeOrderViewModel.setOrder(this.notificationDetailViewModel.getOrder());

            if (!this._completeOrderDialog) {

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("completeOrderPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.CompleteOrderPopover", this);
                }

                this._completeOrderDialog = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._oMcPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    busy: "{completeOrderModel>/view/busy}",
                    beginButton: new Button({
                        text: '{i18n_core>complete}',
                        press: function () {
                            var button = this;

                            that.completeOrderViewModel.saveChanges(function () {
                                that.notificationDetailViewModel.setEditable(false);
                                executeCb();
                                button.getParent().close();
                            }, that.onViewModelError);
                        }
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {

                    }
                });

                this._completeOrderDialog.setModel(this.component.getModel('i18n'), "i18n");
                this._completeOrderDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._completeOrderDialog.setTitle(this.completeOrderViewModel.getDialogTitle());
            this._completeOrderDialog.setModel(this.completeOrderViewModel, "completeOrderModel");

            this.completeOrderViewModel.loadOrderData(function (err) {
                that.onViewModelError(err);
            });

            return this._completeOrderDialog;
        },

        formatOperationUserStatusToText: function (status) {
            return this.notificationDetailViewModel.getFormatter().formatOperationUserStatusToText(status);
        }
    });

});