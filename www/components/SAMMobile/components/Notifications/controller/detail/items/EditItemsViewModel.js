sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationItem"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationItem) {
        "use strict";

        // constructor
        function EditItemsViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditItemsViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        activity: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setItem: {
                value: function (item) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = item.getCopy();

                    }

                    this.setProperty("/item", item);
                }
            },

            getItem: {
                value: function () {
                    return this.getProperty("/item");
                }
            },

            setItemActivity: {
                value: function (activity) {
                    this.setProperty("/activity", activity);
                }
            },

            getItemActivity: {
                value: function () {
                    return this.getProperty("/activity");
                }
            },

            setItemCause: {
                value: function (cause) {
                    this.setProperty("/cause", cause);
                }
            },

            getItemCause: {
                value: function () {
                    return this.getProperty("/cause");
                }
            },

            getItemTask: {
                value: function () {
                    return this.getProperty("/task");
                }
            },

            setItemTask: {
                value: function (task) {
                    this.setProperty("/task", task);
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getItem().setData(this._objectDataCopy);
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditItem") : this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditItem");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditItemsViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditItemsViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("item.DL_CODEGRP", this._component.i18n.getText("CODEGROUP_SELECT")).isEmpty();
            validator.check("item.DL_CODEGRP", this._component.i18n.getText("CODE_SELECT")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditItemsViewModel.prototype.validateActivity = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("activity.ACT_CODEGRP", this._component.i18n.getText("CODEGROUP_SELECT")).isEmpty();
            validator.check("activity.ACT_CODE", this._component.i18n.getText("CODE_SELECT")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditItemsViewModel.prototype.validateCause = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("cause.CAUSE_CODEGRP", this._component.i18n.getText("CODEGROUP_SELECT")).isEmpty();
            validator.check("cause.CAUSE_CODE", this._component.i18n.getText("CODE_SELECT")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditItemsViewModel.prototype.validateTask = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("task.TASK_CODEGRP", this._component.i18n.getText("CODEGROUP_SELECT")).isEmpty();
            validator.check("task.TASK_CODE", this._component.i18n.getText("CODE_SELECT")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditItemsViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MOBILE_ID: sap.ui.core.ValueState.None,
                    NOTIF_NO: sap.ui.core.ValueState.None,
                    ITEM_KEY: sap.ui.core.ValueState.None,
                    ITEM_SORT_NO: sap.ui.core.ValueState.None,
                    DESCRIPT: sap.ui.core.ValueState.None,
                    ASSEMBLY: sap.ui.core.ValueState.None,
                    D_CAT_TYP: sap.ui.core.ValueState.None,
                    D_CODE: sap.ui.core.ValueState.None,
                    D_CODEGRP: sap.ui.core.ValueState.None,
                    DL_CODEGRP: sap.ui.core.ValueState.None,
                    DL_CAT_TYP: sap.ui.core.ValueState.None,
                    DL_CODE: sap.ui.core.ValueState.None,
                    TXT_OBJPTCD: sap.ui.core.ValueState.None,
                    TXT_PROBCD: sap.ui.core.ValueState.None,
                    TXT_TASKCD: sap.ui.core.ValueState.None,
                    MATL_DESC: sap.ui.core.ValueState.None,
                    MATERIAL: sap.ui.core.ValueState.None,
                    LONG_TEXT: sap.ui.core.ValueState.None,
                });
            }
            return this.valueStateModel;
        };

        // Select Dialogs
        EditItemsViewModel.prototype.displayMaterialSelectDialog = function (oEvent, viewContext) {
            if (!this._oAssemblyDialog) {
                this._oAssemblyDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.MaterialSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oAssemblyDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oAssemblyDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oAssemblyDialog.display(oEvent, {
                tableName: "NotifItemMaterial",
                params: {
                    spras: this.getLanguage()
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleMaterialSelect = function (oEvent) {
            this._oAssemblyDialog.select(oEvent, 'MATERIAL', [{
                'MATERIAL': 'MATNR',
                'MATL_DESC': 'MATL_DESC'
            }], this, "/item");
        };

        EditItemsViewModel.prototype.handleMaterialSearch = function (oEvent) {
            this._oAssemblyDialog.search(oEvent, ["MATERIAL", "MATL_DESC"]);
        };

        EditItemsViewModel.prototype.displayCodeGroupSelectDialog = function (oEvent, viewContext) {

            if (!this._oItemCodeGroupDialog) {
                this._oItemCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oItemCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oItemCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oItemCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.displayCodeGroupCauseSelectDialog = function (oEvent, viewContext) {
            if (!this._oCauseCodeGroupDialog) {
                this._oCauseCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCauseCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCauseCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCauseCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "5"
                },
                actionName: "valueHelp"
            });

        };

        EditItemsViewModel.prototype.handleCodeGroupCauseSelect = function (oEvent) {
            this._oCauseCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'CAUSE_CODEGRP': 'CODEGROUP'
            }, {
                "TXT_CAUSEGRP": "CODEGROUP_TEXT"
            }], this, "/cause");
        };

        EditItemsViewModel.prototype.handleCodeGroupCauseSearch = function (oEvent) {
            this._oCauseCodeGroupDialog.search(oEvent, ["CAUSE_CODEGRP"], "TXT_CAUSEGRP");
        };

        EditItemsViewModel.prototype.displayObjCodingCauseDialog = function (oEvent, viewContext) {
            if (!this._oCauseCodeDialog) {
                this._oCauseCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCauseCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCauseCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCauseCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/cause/CAUSE_CODEGRP"),
                    catalogType: "5"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleCodeCauseSelect = function (oEvent) {

            this._oCauseCodeDialog.select(oEvent, 'CODE', [{
                'CAUSE_CODE': 'CODE'
            }, {
                "CAUSE_CAT_TYP": "CATALOG_TYPE"
            }, {
                "TXT_CAUSECD": "CODE_TEXT"
            }], this, "/cause");
        };

        EditItemsViewModel.prototype.handleCodeCauseSearch = function (oEvent) {
            this._oCauseCodeDialog.search(oEvent, ["CAUSE_CODE", "TXT_CAUSECD"]);
        };

        EditItemsViewModel.prototype.displayObjCodeGroupActivityDialog = function (oEvent, viewContext) {

            if (!this._oItemCodeGroupDialog) {
                this._oItemCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oItemCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oItemCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oItemCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleObjCodeGroupActivitySelect = function (oEvent) {
            this._oItemCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'ACT_CODEGRP': 'CODEGROUP'
            }, {
                "ACT_CODEGRP_TXT": "CODEGROUP_TEXT"
            }], this, "/activity");
        };


        EditItemsViewModel.prototype.handleObjCodeGroupActivitySearch = function (oEvent) {
            this._oItemCodeGroupDialog.search(oEvent, ["ACT_CODEGRP", "ACT_CODEGRP_TXT"]);
        };

        EditItemsViewModel.prototype.handleCodeGroupSelect = function (oEvent) {

            this._oItemCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'DL_CODEGRP': 'CODEGROUP'
            }, {
                "TXT_GRPCD": "CODEGROUP_TEXT"
            }, {
                'DL_CAT_TYP': 'CATALOG_TYPE'
            }, {
                '_CATPROFILE': 'CATALOG_PROFILE'
            }], this, "/item");
        };

        EditItemsViewModel.prototype.handleCodeGroupSearch = function (oEvent) {

            this._oItemCodeGroupDialog.search(oEvent, ["DL_CODEGRP"]);
        };

        EditItemsViewModel.prototype.displayCodingSelectDialog = function (oEvent, viewContext) {
            if (!this._oItemCodeDialog) {
                this._oItemCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oItemCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oItemCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oItemCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/item/DL_CODEGRP"),
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleCodingSelect = function (oEvent) {
            this._oItemCodeDialog.select(oEvent, 'CODE', [{
                'DL_CODE': 'CODE'
            }, {
                'DL_CAT_TYP': 'CATALOG_TYPE'
            }, {
                'TXT_OBJPTCD': 'CODE_TEXT'
            }], this, "/item");
        };

        EditItemsViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oItemCodeDialog.search(oEvent, ["DL_CODE"]);
        };

        EditItemsViewModel.prototype.displayObjCodeGroupDialog = function (oEvent, viewContext) {
            if (!this._oObjCodeGroupDialog) {
                this._oObjCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oObjCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oObjCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oObjCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "C"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleObjCodeGroupSelect = function (oEvent) {
            this._oObjCodeGroupDialog.select(oEvent, 'CODE_CAT_GROUP', [{
                'D_CODEGRP': 'CODEGROUP'
            }, {
                'TXT_PART': 'CODEGROUP_TEXT'
            }, {
                'D_CAT_TYP': 'CATALOG_TYPE'
            }, {
                '_CATPROFILE': 'CATALOG_PROFILE'
            }], this, "/item");
        };

        EditItemsViewModel.prototype.handleObjCodeGroupSearch = function (oEvent) {
            this._oObjCodeGroupDialog.search(oEvent, ["D_CODEGRP"]);
        };

        EditItemsViewModel.prototype.displayObjCodingDialog = function (oEvent, viewContext) {
            if (!this._oObjCodingDialog) {
                this._oObjCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oObjCodingDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oObjCodingDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oObjCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/item/D_CODEGRP"),
                    catalogType: "C"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleObjCodingSelect = function (oEvent) {
            this._oObjCodingDialog.select(oEvent, 'CODE', [{
                'D_CODE': 'CODE'
            }, {
                'D_CAT_TYP': 'CATALOG_TYPE'
            }, {
                'TXT_PROBCD': 'CODE_TEXT'
            }], this, "/item");
        };

        EditItemsViewModel.prototype.handleObjCodingSearch = function (oEvent) {
            this._oObjCodingDialog.search(oEvent, ["D_CODE"]);
        };

        EditItemsViewModel.prototype.displayObjCodingActivityDialog = function (oEvent, viewContext) {
            if (!this._oCodeDialog) {
                this._oCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/activity/ACT_CODEGRP"),
                    catalogType: "A"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleObjCodingActivitySelect = function (oEvent) {
            this._oCodeDialog.select(oEvent, 'CODE', [{
                'ACT_CODE': 'CODE'
            }, {
                'ACT_CODE_TEXT': 'CODE_TEXT'
            }], this, "/activity");
        };

        EditItemsViewModel.prototype.handleObjCodingActivitySearch = function (oEvent) {
            this._oCodeDialog.search(oEvent, ["ACT_CODE", "ACT_CODE_TEXT"]);
        };

        EditItemsViewModel.prototype.displayCodeGroupTaskSelectDialog = function (oEvent, viewContext) {
            if (!this._oTaskCodeGroupDialog) {
                this._oTaskCodeGroupDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodeGroupSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oTaskCodeGroupDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oTaskCodeGroupDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oTaskCodeGroupDialog.display(oEvent, {
                tableName: "CodeGroupMl",
                params: {
                    spras: this.getLanguage(),
                    catalogProfile: this.getNotification()._CATPROFILE,
                    catalogType: "2"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleCodeGroupTaskSelect = function (oEvent) {
            this._oTaskCodeGroupDialog.select(oEvent, 'CODEGROUP', [{
                'TASK_CODEGRP': 'CODEGROUP'
            }, {
                "TXT_TASKGRP": "CODEGROUP_TEXT"
            }], this, "/task");
        };

        EditItemsViewModel.prototype.handleCodeGroupTaskSearch = function (oEvent) {
            this._oTaskCodeGroupDialog.search(oEvent, ["TASK_CODEGRP", "TXT_TASKGRP"]);
        };

        EditItemsViewModel.prototype.displayCodingTaskSelectDialog = function (oEvent, viewContext) {
            if (!this._oTaskCodeDialog) {
                this._oTaskCodeDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
                this._oTaskCodeDialog.dialog.setModel(this._component.getModel('i18n'), "i18n");
                this._oTaskCodeDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n_core");
            }

            this._oTaskCodeDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: this.getProperty("/task/TASK_CODEGRP"),
                    catalogType: "2"
                },
                actionName: "valueHelp"
            });
        };

        EditItemsViewModel.prototype.handleCodeTaskSelect = function (oEvent) {
            this._oTaskCodeDialog.select(oEvent, 'CODE', [{
                'TASK_CODE': 'CODE'
            }, {
                "TXT_TASKCD": "CODE_TEXT"
            }], this, "/task");
        };

        EditItemsViewModel.prototype.handleCodeTaskSearch = function (oEvent) {
            this._oTaskCodeDialog.search(oEvent, ["TASK_CODE", "TXT_TASKCD"]);
        };

        EditItemsViewModel.prototype.setStartTimeActivity = function (newStartTime) {
            this.getItemActivity().setStartTime(newStartTime);
        };

        EditItemsViewModel.prototype.setEndTimeActivity = function (newEndTime) {
            this.getItemActivity().setEndTime(newEndTime);
        };

        EditItemsViewModel.prototype.setStartTimeTask = function (newStartTime) {
            this.getItemTask().setStartTime(newStartTime);
        };

        EditItemsViewModel.prototype.setEndTimeTask = function (newEndTime) {
            this.getItemTask().setEndTime(newEndTime);
        };

        EditItemsViewModel.prototype.constructor = EditItemsViewModel;

        return EditItemsViewModel;
    });
