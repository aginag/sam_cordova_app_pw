sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationItem",
        "SAMMobile/models/dataObjects/NotificationActivity",
        "SAMMobile/models/dataObjects/NotificationCause",
        "SAMMobile/models/dataObjects/NotificationTask"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, NotificationItem, NotificationActivity, NotificationCause, NotificationTask) {
        "use strict";

        // constructor
        function ItemsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.scenario = this._component.scenario;
            this._NOTIFIACTIONNO = "";
            this._NOTIFICATION = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        ItemsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            editItemAllowed: true,
                            busy: false,
                            selectedTabKey: "itemActivities",
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFIACTIONNO,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            getNotificationSQLChangeString: {
                value: function () {
                    return this.getNotification().setChanged(true);
                }
            },

            setNotificationNo: {
                value: function (notificationNo) {
                    this._NOTIFIACTIONNO = notificationNo;
                }
            },

            setNotification: {
                value: function (notification) {
                    this._NOTIFIACTION = notification;
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            getItemActivity: {
                value: function () {
                    return this.getProperty("/activity");
                }
            },

            getNotification: {
                value: function () {
                    return this._NOTIFIACTION;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        ItemsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        ItemsTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var arrData = data.map(function (attData) {
                return new NotificationItem(attData, that._requestHelper, that);
            });

            arrData.forEach(function (item) {

                that.loadItemData(item.ITEM_KEY).then(function (itemData) {
                    item.itemArray = itemData;
                    var longTextInfo = that.scenario.getComplaintNotificationInfo();
                    var promiseArray = [];

                    if (itemData.NotificationItemActivities) {
                        item.counterItemActivities = itemData.NotificationItemActivities.length;

                        var notifItemActPromise = itemData.NotificationItemActivities.map(function (attObject) {
                            return new Promise(function (resolve, reject) {
                                var load_success = function (data) {
                                    attObject.TEXT_LINE = data;
                                    resolve(attObject);
                                };
                                attObject.loadLongText(longTextInfo.longTextActivityObjectType).then(load_success);
                            });
                        });

                        promiseArray = promiseArray.concat(notifItemActPromise);

                    }

                    if (itemData.NotificationItemCauses) {
                        item.counterItemCauses = itemData.NotificationItemCauses.length;

                        var notifItemCausePromise = itemData.NotificationItemCauses.map(function (attObject) {
                            return new Promise(function (resolve, reject) {
                                var load_success = function (data) {
                                    attObject.TEXT_LINE = data;
                                    resolve(attObject);
                                };

                                attObject.loadLongText(longTextInfo.longTextCauseObjectType).then(load_success);
                            });
                        });

                        promiseArray = promiseArray.concat(notifItemCausePromise);
                    }

                    if (itemData.NotificationItemTasks) {
                        item.counterItemTasks = itemData.NotificationItemTasks.length;

                        var notifItemTaskPromise = itemData.NotificationItemTasks.map(function (attObject) {
                            return new Promise(function (resolve, reject) {
                                var load_success = function (data) {
                                    attObject.TEXT_LINE = data;
                                    resolve(attObject);
                                };

                                attObject.loadLongText(longTextInfo.longTextTaskObjectType).then(load_success);
                            });
                        });

                        promiseArray = promiseArray.concat(notifItemTaskPromise);
                    }

                    Promise.all(promiseArray).then(function (result) {
                        that.setProperty("/items", arrData);
                    }).catch(function (err) {
                        that.executeErrorCallback(err);
                    });

                }).catch(function () {

                });
            });

            this.setProperty("/items", arrData);
        };

        ItemsTabViewModel.prototype.loadItemData = function (itemKey) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadDataForItem(itemKey, resolve, reject);
            });
        };

        ItemsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                data.map(function (item) {
                    item._CATPROFILE = that.getNotification()._CATPROFILE;
                });
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };


            this.setBusy(true);
            this._requestHelper.getData("NotificationItem", {
                notificationNo: this.getNotificationNo(),
                spras: this.getLanguage(),
                persNo: this.getUserInformation().personnelNumber
            }, load_success, load_error, true);
        };

        ItemsTabViewModel.prototype.loadDataForItem = function (itemKey, successCb) {
            var that = this;
            var load_success = function (data) {

                var dataObjects = {};

                var NotificationItemActivities = data.NotificationItemActivities.map(function (notifItemActivity) {
                    return new NotificationActivity(notifItemActivity, that._requestHelper, that);
                });

                var NotificationItemCauses = data.NotificationItemCauses.map(function (notifItemCause) {
                    return new NotificationCause(notifItemCause, that._requestHelper, that);
                });

                var NotificationItemTasks = data.NotificationItemTasks.map(function (notifItemTask) {
                    return new NotificationTask(notifItemTask, that._requestHelper, that);
                });

                dataObjects.NotificationItemActivities = NotificationItemActivities;
                dataObjects.NotificationItemCauses = NotificationItemCauses;
                dataObjects.NotificationItemTasks = NotificationItemTasks;

                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(dataObjects);
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["NotificationItemActivities", "NotificationItemCauses", "NotificationItemTasks"], {
                notificationNo: this.getNotificationNo(),
                itemNo: itemKey,
                spras: this.getLanguage(),
                persNo: this.getUserInformation().personnelNumber
            }, load_success, load_error, true);
        };

        ItemsTabViewModel.prototype.getNewEditItemActivity = function (currentItem, sPath) {

            var activityModelData = new NotificationActivity(null, this._requestHelper, this);

            activityModelData.NOTIF_NO = this.getNotificationNo();
            activityModelData.PERS_NO = this.getUserInformation().personnelNumber;
            activityModelData.ITEM_KEY = currentItem.ITEM_KEY;
            if (parseInt(activityModelData.ITEM_KEY) > 0) {
                activityModelData.ITEM_SORT_NO = currentItem.ITEM_KEY;
            }
            activityModelData.ACT_KEY = activityModelData.getRandomCharUUID(4);
            activityModelData.TEXT_LINE = "";

            return activityModelData;
        };

        ItemsTabViewModel.prototype.getNewEditItemCause = function (currentItem, sPath) {

            var causeModelData = new NotificationCause(null, this._requestHelper, this);

            causeModelData.NOTIF_NO = this.getNotificationNo();
            causeModelData.PERS_NO = this.getUserInformation().personnelNumber;
            causeModelData.ITEM_KEY = currentItem.ITEM_KEY;
            if (parseInt(causeModelData.ITEM_KEY) > 0) {
                causeModelData.ITEM_SORT_NO = currentItem.ITEM_KEY;
            }
            causeModelData._longText = "";
            causeModelData.CAUSE_KEY = causeModelData.getRandomCharUUID(4);

            return causeModelData;
        };

        ItemsTabViewModel.prototype.getNewEditItemTask = function (currentItem, sPath) {

            var taskModelData = new NotificationTask(null, this._requestHelper, this);

            taskModelData.NOTIF_NO = this.getNotificationNo();
            taskModelData.PERS_NO = this.getUserInformation().personnelNumber;
            if (parseInt(taskModelData.ITEM_KEY) > 0) {
                taskModelData.ITEM_SORT_NO = currentItem.ITEM_KEY;
            }
            taskModelData.ITEM_KEY = currentItem.ITEM_KEY;
            taskModelData.TEXT_LINE = "";
            taskModelData.TASK_KEY = taskModelData.getRandomCharUUID(4);

            return taskModelData;
        };

        ItemsTabViewModel.prototype.insertNewItemCause = function (sPath, cause, successCb) {
            var that = this;
            var sqlStrings = [];
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function () {
                that.insertToModelPath(cause, sPath + "/itemArray/NotificationItemCauses");
                that.setProperty(sPath + "/counterItemCauses", that.getProperty(sPath + "/counterItemCauses") + 1);
                successCb();
                that.refresh();
            };

            try {
                cause.insert(false, function (mobileId) {
                    cause.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.i18n.getText("ERROR_CAUSE_INSERT"))));

                if (cause.TEXT_LINE) {
                    sqlStrings = sqlStrings.concat(cause.addLongtext(true, true, cause.TEXT_LINE, that.scenario.getComplaintNotificationInfo().longTextCauseObjectType));
                }
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_INSERT")));
                });

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.insertNewItemTask = function (sPath, task, successCb) {
            var that = this;
            var sqlStrings = [];
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function () {
                that.insertToModelPath(task, sPath + "/itemArray/NotificationItemTasks");
                that.setProperty(sPath + "/counterItemTasks", that.getProperty(sPath + "/counterItemTasks") + 1);
                successCb();
                that.refresh();
            };

            try {
                task.insert(false, function (mobileId) {
                    task.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.i18n.getText("ERROR_NOTIF_INSERT_COMPONENT"))));

                if (task.TEXT_LINE) {
                    sqlStrings = sqlStrings.concat(task.addLongtext(true, true, task.TEXT_LINE, that.scenario.getComplaintNotificationInfo().longTextTaskObjectType));
                }
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_INSERT")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.resetData = function () {
            this.setNotification(null);
        };

        ItemsTabViewModel.prototype.insertNewItem = function (item, successCb) {
            var that = this;

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            try {
                item.LONG_TEXT = item.longTextMl;
                item.ASSEMBLY = item.MATL_DESC;
                item.counterItemActivities = 0;
                item.counterItemCauses = 0;
                item.counterItemTasks = 0;
            } catch (err) {

            }

            var onSuccess = function (asd) {
                that.insertToModelPath(item, "/items");
                successCb();
            };

            try {
                item.insert(false, function (mobileId) {
                    item.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(that._component.i18n.getText("ERROR_NOTIF_INSERT_COMPONENT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.getNewEditItem = function () {
            var itemModelData = new NotificationItem(null, this._requestHelper, this);

            itemModelData.NOTIF_NO = this.getNotificationNo();
            itemModelData.PERS_NO = this.getUserInformation().personnelNumber;
            itemModelData.itemArray = {
                NotificationItemTasks: [],
                NotificationItemActivities: [],
                NotificationItemCauses: [],
            };

            itemModelData.ITEM_KEY = itemModelData.getRandomCharUUID(4);

            return itemModelData;
        };

        ItemsTabViewModel.prototype.updateItem = function (item, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(item.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ITEM_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.deleteItem = function (item, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(item.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(item, "/items");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ITEM_DELETE")));
            });
        };

        ItemsTabViewModel.prototype.insertNewActivity = function (sPath, activity, successCb) {
            var that = this;
            var sqlStrings = [];
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function () {
                that.insertToModelPath(activity, sPath + "/itemArray/NotificationItemActivities");
                that.setProperty(sPath + "/counterItemActivities", that.getProperty(sPath + "/counterItemActivities") + 1);
                successCb();
                that.refresh();
            };

            try {
                activity.insert(false, function (mobileId) {
                    activity.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error(this._component.i18n.getText("ERROR_NOTIF_INSERT_COMPONENT"))));

                if (activity.TEXT_LINE) {
                    sqlStrings = sqlStrings.concat(activity.addLongtext(true, true, activity.TEXT_LINE, that.scenario.getComplaintNotificationInfo().longTextActivityObjectType));
                }
                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ACTIVITY_INSERT")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.deleteItemCause = function (sPath, cause, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(cause.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(cause, sPath.slice(0, sPath.lastIndexOf("/")));
                var updateCounterPath = sPath.slice(0, sPath.indexOf("itemArray")) + 'counterItemCauses';
                that.setProperty(updateCounterPath, that.getProperty(updateCounterPath) - 1);
                cause.deleteLongtext(that.scenario.getComplaintNotificationInfo().longTextCauseObjectType).then(successCb);
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_DELETE")));
            });
        };

        ItemsTabViewModel.prototype.updateItemCause = function (cause, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function () {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(cause.update(true));
                var load_success = function (data) {
                    Array.prototype.push.apply(sqlStrings, data);
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CAUSE_UPDATE")));
                    });
                };
                cause.updateLongtext(cause.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextCauseObjectType).then(load_success);

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.deleteItemActivity = function (sPath, activity, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(activity.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(activity, sPath.slice(0, sPath.lastIndexOf("/")));
                var updateCounterPath = sPath.slice(0, sPath.indexOf("itemArray")) + 'counterItemActivities';
                that.setProperty(updateCounterPath, that.getProperty(updateCounterPath) - 1);
                activity.deleteLongtext(that.scenario.getComplaintNotificationInfo().longTextActivityObjectType).then(successCb);
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ACTIVITY_DELETE")));
            });
        };

        ItemsTabViewModel.prototype.updateItemActivity = function (activity, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function () {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(activity.update(true));
                var load_success = function (data) {
                    Array.prototype.push.apply(sqlStrings, data);
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ACTIVITY_UPDATE")));
                    });
                };
                activity.updateLongtext(activity.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextActivityObjectType).then(load_success);

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };


        ItemsTabViewModel.prototype.deleteItemTask = function (sPath, task, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(task.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(task, sPath.slice(0, sPath.lastIndexOf("/")));
                var updateCounterPath = sPath.slice(0, sPath.indexOf("itemArray")) + 'counterItemTasks';
                that.setProperty(updateCounterPath, that.getProperty(updateCounterPath) - 1);
                task.deleteLongtext(that.scenario.getComplaintNotificationInfo().longTextTaskObjectType).then(successCb)
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_DELETE")));
            });
        };

        ItemsTabViewModel.prototype.updateItemTask = function (task, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function () {
                that.refresh();
                successCb();
            };

            try {
                sqlStrings.push(task.update(true));
                var load_success = function (data) {
                    Array.prototype.push.apply(sqlStrings, data);
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TASK_UPDATE")));
                    });
                };
                task.updateLongtext(task.TEXT_LINE, this.scenario.getComplaintNotificationInfo().longTextTaskObjectType).then(load_success);
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ItemsTabViewModel.prototype.constructor = ItemsTabViewModel;

        return ItemsTabViewModel;
    });