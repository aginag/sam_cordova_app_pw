sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Notifications/controller/detail/items/EditItemsViewModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Text",
    "sap/m/TextArea",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Notifications/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast"
], function (Controller, EditItemsViewModel, Dialog, Button, Text, TextArea, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.Notifications.controller.detail.items.ItemsTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("notificationDetailItems").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("notificationDetailItems", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.notificationDetailViewModel = null;
            this.itemsTabViewModel = null;
        },

        onRouteMatched: function (oEvent) {

            if (!this.notificationDetailViewModel) {
                this.notificationDetailViewModel = this.getView().getModel("notificationDetailViewModel");
                this.itemsTabViewModel = this.notificationDetailViewModel.tabs.items.model;

                this.getView().setModel(this.itemsTabViewModel, "itemsTabViewModel");
            }

            if (this.itemsTabViewModel.needsReload()) {
                this.loadViewModelData();
            }
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("notificationDetailItems", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.itemsTabViewModel.loadData(function () {

            });
        },

        handleEditItem: function (oEvent) {
            var that = this;
            var item = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            var onAcceptPressed = function (item, dialog) {
                that.itemsTabViewModel.updateItem(item, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_UPDATED_SUCCESSFULLY"));
                });
            };
            this._getItemEditDialog(item, true, onAcceptPressed).open();
        },

        handleDeleteItem: function (oEvent) {
            var that = this;
            var item = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.itemsTabViewModel.deleteItem(item, function () {
                    that.notificationDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_DELETED_SUCCESSFULLY"));
                });
            },  that.component.i18n.getText("deleteNotifItem")).open();
        },

        onNewItem: function () {
            var that = this;

            var onAcceptPressed = function (item, dialog) {
                that.itemsTabViewModel.insertNewItem(item, function () {
                    dialog.close();
                    that.notificationDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };

            this._getItemEditDialog(that.itemsTabViewModel.getNewEditItem(), false, onAcceptPressed).open();
        },

        onNewItemActivities: function (oEvent) {
            var that = this;
            var currentItem = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();
            var onAcceptPressed = function (activity, dialog) {
                that.itemsTabViewModel.insertNewActivity(that.sPath, activity, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };
            this._getActivityEditDialog(that.itemsTabViewModel.getNewEditItemActivity(currentItem, this.sPath), false, onAcceptPressed).open();
        },

        _getActivityEditDialog: function (activity, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var editItemsTabViewModel = this.getParent().getModel("activityModel");
                if (editItemsTabViewModel.validateActivity()) {
                    acceptCb(editItemsTabViewModel.getItemActivity(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            var notification = this.itemsTabViewModel.getNotification();

            this.editItemsViewModel = new EditItemsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editItemsViewModel.setEdit(bEdit);
            this.editItemsViewModel.setNotification(notification);
            this.editItemsViewModel.setItemActivity(activity);

            if (!this._activityItemEditPopover) {

                if (!this._activityItemEditPopover) {
                    this._activityItemEditPopover = sap.ui.xmlfragment("activityItemEditPopover", "SAMMobile.components.Notifications.view.detail.activities.ActivityEditPopover", this);
                }

                this._activityItemEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_ACTIVITY}' : '{i18n>NOTIFICATIONS_NEW_ACTIVITY}',
                    content: this._activityItemEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });
                this.getView().addDependent(this._activityItemEditPopover);
            }

            this._activityItemEditPopover.getBeginButton().mEventRegistry.press = [];
            this._activityItemEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._activityItemEditPopover.setModel(this.editItemsViewModel, "activityModel");
            return this._activityItemEditPopover;
        },

        handleStartTimeChange: function (oEvent) {
            this.editItemsViewModel.setStartTimeActivity(oEvent.getParameter("value"));
            this.itemsTabViewModel.refresh();
        },

        handleEndTimeChange: function (oEvent) {
            this.editItemsViewModel.setEndTimeActivity(oEvent.getParameter("value"));
            this.itemsTabViewModel.refresh();
        },

        handleTaskStartTimeChange: function (oEvent) {
            this.editItemsViewModel.setStartTimeTask(oEvent.getParameter("value"));
            this.itemsTabViewModel.refresh();
        },

        handleTaskEndTimeChange: function (oEvent) {
            this.editItemsViewModel.setEndTimeTask(oEvent.getParameter("value"));
            this.itemsTabViewModel.refresh();
        },


        onNewItemCauses: function (oEvent) {
            var that = this;
            var currentItem = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();

            var onAcceptPressed = function (cause, dialog) {
                that.itemsTabViewModel.insertNewItemCause(that.sPath, cause, function () {
                    dialog.close();
                    that.itemsTabViewModel.refresh();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };
            this._getCauseEditDialog(that.itemsTabViewModel.getNewEditItemCause(currentItem, this.sPath), false, onAcceptPressed).open();
        },

        _getCauseEditDialog: function (cause, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var itemCausesTabViewModel = this.getParent().getModel("causeModel");
                if (itemCausesTabViewModel.validateCause()) {
                    acceptCb(itemCausesTabViewModel.getItemCause(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            var notification = this.itemsTabViewModel.getNotification();

            this.editItemsViewModel = new EditItemsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editItemsViewModel.setEdit(bEdit);
            this.editItemsViewModel.setNotification(notification);
            this.editItemsViewModel.setItemCause(cause);

            if (!this._ItemCauseEditPopover) {

                if (!this._ItemCauseEditPopover) {
                    this._ItemCauseEditPopover = sap.ui.xmlfragment("itemCausePopup", "SAMMobile.components.Notifications.view.detail.causes.CauseEditPopover", this);
                }

                this._ItemCauseEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_CAUSE}' : '{i18n>NOTIFICATIONS_NEW_CAUSE}',
                    content: this._ItemCauseEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this.getView().addDependent(this._ItemCauseEditPopover);
            }

            this._ItemCauseEditPopover.getBeginButton().mEventRegistry.press = [];
            this._ItemCauseEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._ItemCauseEditPopover.setModel(this.editItemsViewModel, "causeModel");
            return this._ItemCauseEditPopover;
        },

        onNewItemTasks: function (oEvent) {
            var that = this;
            var currentItem = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();

            var onAcceptPressed = function (task, dialog) {
                that.itemsTabViewModel.insertNewItemTask(that.sPath, task, function () {
                    dialog.close();
                    that.itemsTabViewModel.refresh();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_INSERTED_SUCCESSFULLY"));
                });
            };

            this._getTaskEditDialog(that.itemsTabViewModel.getNewEditItemTask(currentItem, this.sPath), false, onAcceptPressed).open();
        },

        _getTaskEditDialog: function (task, bEdit, acceptCb) {

            var onAcceptPressed = function () {
                var editItemsViewModel = this.getParent().getModel("taskModel");
                if (editItemsViewModel.validateTask()) {
                    acceptCb(editItemsViewModel.getItemTask(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            var notification = this.itemsTabViewModel.getNotification();

            this.editItemsViewModel = new EditItemsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editItemsViewModel.setEdit(bEdit);
            this.editItemsViewModel.setNotification(notification);
            this.editItemsViewModel.setItemTask(task);

            if (!this._ItemTaskEditPopover) {

                if (!this._ItemTaskEditPopover) {
                    this._ItemTaskEditPopover = sap.ui.xmlfragment("ItemTaskPopup", "SAMMobile.components.Notifications.view.detail.tasks.TaskEditPopover", this);
                }

                this._ItemTaskEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_TASK}' : '{i18n>NOTIFICATIONS_NEW_TASK}',
                    content: this._ItemTaskEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this.getView().addDependent(this._ItemTaskEditPopover);
            }

            this._ItemTaskEditPopover.getBeginButton().mEventRegistry.press = [];
            this._ItemTaskEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._ItemTaskEditPopover.setModel(this.editItemsViewModel, "taskModel");
            this._ItemTaskEditPopover.setModel(this.component.getModel('i18n'), "i18n");
            this._ItemTaskEditPopover.setModel(this.component.getModel('i18n_core'), "i18n_core");

            return this._ItemTaskEditPopover;
        },

        _getItemEditDialog: function (item, bEdit, acceptCb) {

            var onAcceptPressed = function () {
                var itemsTabViewModel = this.getParent().getModel("itemModel");
                if (itemsTabViewModel.validate()) {
                    acceptCb(itemsTabViewModel.getItem(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var itemsTabViewModel = dialog.getModel("itemModel");
                itemsTabViewModel.rollbackChanges();
                dialog.close();
            };

            var notification = this.itemsTabViewModel.getNotification();

            this.editItemsViewModel = new EditItemsViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editItemsViewModel.setEdit(bEdit);
            this.editItemsViewModel.setNotification(notification);
            this.editItemsViewModel.setItem(item);

            if (!this._itemEditPopover) {

                if (!this._itemEditPopover) {
                    this._itemEditPopover = sap.ui.xmlfragment("itemPopup", "SAMMobile.components.Notifications.view.detail.items.ItemEditPopover", this);
                }

                this._itemEditPopover = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: bEdit ? '{i18n>NOTIFICATIONS_EDIT_ITEM}' : '{i18n>NOTIFICATIONS_NEW_ITEM}',
                    content: this._itemEditPopover,
                    beginButton: new Button({
                        text: '{i18n_core>ok}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this.getView().addDependent(this._itemEditPopover);
            }

            this._itemEditPopover.getBeginButton().mEventRegistry.press = [];
            this._itemEditPopover.getBeginButton().attachPress(onAcceptPressed);

            this._itemEditPopover.setModel(this.editItemsViewModel, "itemModel");
            return this._itemEditPopover;
        },

        _getConfirmActionDialog: function (executeCb, deleteText) {

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: deleteText,
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                }
            });

            return _oConfirmationDialog;
        },


        displayMaterialSelectDialog: function (oEvent) {
            this.editItemsViewModel.displayMaterialSelectDialog(oEvent, this);
        },

        handleMaterialSelect: function (oEvent) {
            this.editItemsViewModel.handleMaterialSelect(oEvent);
        },

        handleMaterialSearch: function (oEvent) {
            this.editItemsViewModel.handleMaterialSearch(oEvent);
        },

        displayCodeGroupSelectDialog: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();

            if (path == "DL_CODEGRP") {
                this.editItemsViewModel.displayCodeGroupSelectDialog(oEvent, this);
            } else if (path == "D_CODEGRP") {
                this.editItemsViewModel.displayObjCodeGroupDialog(oEvent, this);
            } else if (path === "ACT_CODEGRP") {
                this.editItemsViewModel.displayObjCodeGroupActivityDialog(oEvent, this);
            }
        },

        displayCodeGroupSearch: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();
            if (path === "CAUSE_CODEGRP") {
                this.editItemsViewModel.displayCodeGroupCauseSelectDialog(oEvent, this);
            } else if (path === "TASK_CODEGRP") {
                this.editItemsViewModel.displayCodeGroupTaskSelectDialog(oEvent, this);
            }

        },

        handleCodeGroupSearch: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();

            if (path == "DL_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupSearch(oEvent);
            } else if (path == "D_CODEGRP") {
                this.editItemsViewModel.handleObjCodeGroupSearch(oEvent);
            } else if (path === "ACT_CODEGRP") {
                this.editItemsViewModel.handleObjCodeGroupActivitySearch(oEvent);
            } else if (path === "CAUSE_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupCauseSearch(oEvent);
            } else if (path === "TASK_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupTaskSearch(oEvent);
            }
        },

        handleCodeGroupSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();

            if (path == "DL_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupSelect(oEvent);
            } else if (path == "D_CODEGRP") {
                this.editItemsViewModel.handleObjCodeGroupSelect(oEvent);
            } else if (path === "ACT_CODEGRP") {
                this.editItemsViewModel.handleObjCodeGroupActivitySelect(oEvent);
            } else if (path === "CAUSE_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupCauseSelect(oEvent);
            } else if (path === "TASK_CODEGRP") {
                this.editItemsViewModel.handleCodeGroupTaskSelect(oEvent);
            }
        },

        displayCodingSelectDialog: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();

            if (path == "DL_CODE") {
                this.editItemsViewModel.displayCodingSelectDialog(oEvent, this);
            } else if (path == "D_CODE") {
                this.editItemsViewModel.displayObjCodingDialog(oEvent, this);
            }
        },

        displayCodeSelectDialog: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();
            if (path === "ACT_CODE") {
                this.editItemsViewModel.displayObjCodingActivityDialog(oEvent, this);
            }
        },

        displayCodeSearch: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();
            if (path === "CAUSE_CODE") {
                this.editItemsViewModel.displayObjCodingCauseDialog(oEvent, this);
            } else if (path === "TASK_CODE") {
                this.editItemsViewModel.displayCodingTaskSelectDialog(oEvent, this);
            }
        },

        handleCodingSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();

            if (path == "DL_CODE") {
                this.editItemsViewModel.handleCodingSelect(oEvent);
            } else if (path == "D_CODE") {
                this.editItemsViewModel.handleObjCodingSelect(oEvent);
            } else if (path === "ACT_CODE") {
                this.editItemsViewModel.handleObjCodingActivitySelect(oEvent);
            } else if (path === "CAUSE_CODE") {
                this.editItemsViewModel.handleCodeCauseSelect(oEvent);
            } else if (path === "TASK_CODE") {
                this.editItemsViewModel.handleCodeTaskSelect(oEvent);
            }
        },

        handleCodingSearch: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.aBindings[0].getPath().split("/").pop();
            if (path == "DL_CODE") {
                this.editItemsViewModel.handleCodingSearch(oEvent);
            } else if (path == "D_CODE") {
                this.editItemsViewModel.handleObjCodingSearch(oEvent);
            } else if (path === "ACT_CODE") {
                this.editItemsViewModel.handleObjCodingActivitySearch(oEvent);
            } else if (path === "CAUSE_CODE") {
                this.editItemsViewModel.handleCodeCauseSearch(oEvent);
            } else if (path === "TASK_CODE") {
                this.editItemsViewModel.handleCodeTaskSearch(oEvent);
            }
        },

        handleEditCauseItem: function (oEvent) {
            var that = this;
            var cause = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            var onAcceptPressed = function (cause, dialog) {
                that.itemsTabViewModel.updateItemCause(cause, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_UPDATED_SUCCESSFULLY"));
                });
            };
            this._getCauseEditDialog(cause, true, onAcceptPressed).open();
        },

        handleDeleteCauseItem: function (oEvent) {
            var that = this;
            var cause = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();

            this._getConfirmActionDialog(function () {
                that.itemsTabViewModel.deleteItemCause(that.sPath, cause, function () {
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_CAUSE_DELETED_SUCCESSFULLY"));
                    that.itemsTabViewModel.refresh();
                });
            }, that.component.i18n.getText("DELETE_ITEM_CAUSE")).open();
        },

        handleEditActivityItem: function (oEvent) {
            var that = this;
            var activity = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            var onAcceptPressed = function (activity, dialog) {
                that.itemsTabViewModel.updateItemActivity(activity, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_UPDATED_SUCCESSFULLY"));
                });
            };
            this._getActivityEditDialog(activity, true, onAcceptPressed).open();
        },

        handleDeleteActivityItem: function (oEvent) {
            var that = this;
            var activity = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();
            this._getConfirmActionDialog(function () {
                that.itemsTabViewModel.deleteItemActivity(that.sPath, activity, function () {
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_ACTIVITY_DELETED_SUCCESSFULLY"));
                    that.itemsTabViewModel.refresh();
                });
            }, that.component.i18n.getText("DELETE_ITEM_ACTIVITY")).open();
        },

        handleEditTaskItem: function (oEvent) {
            var that = this;
            var task = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            var onAcceptPressed = function (task, dialog) {
                that.itemsTabViewModel.updateItemTask(task, function () {
                    dialog.close();
                    MessageToast.show(that.component.i18n.getText("NOTIFICATIONS_UPDATED_SUCCESSFULLY"));
                });
            };
            this._getTaskEditDialog(task, true, onAcceptPressed).open();

        },
        handleDeleteTaskItem: function (oEvent) {
            var that = this;
            var task = oEvent.getSource().getBindingContext("itemsTabViewModel").getObject();
            this.sPath = oEvent.getSource().getBindingContext("itemsTabViewModel").getPath();
            this._getConfirmActionDialog(function () {
                that.itemsTabViewModel.deleteItemTask(that.sPath, task, function () {
                    MessageToast.show(that.component.i18n.getText("TASK_DELETED_SUCCESSFULLY"));
                    that.itemsTabViewModel.refresh();
                });
            }, that.component.i18n.getText("DELETE_ITEM_TASK")).open();
        },

    });

});