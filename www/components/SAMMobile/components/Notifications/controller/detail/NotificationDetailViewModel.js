sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/helpers/formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/components/Notifications/controller/detail/overview/OverviewTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/activities/ActivitiesTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/attachments/NotificationAttachmentsTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/causes/CausesTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/items/ItemsTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/partners/PartnersTabViewModel",
        "SAMMobile/components/Notifications/controller/detail/tasks/TasksTabViewModel"],

    function (JSONModel,
              ViewModel,
              Validator,
              formatter,
              GenericSelectDialog,
              NotificationHeader,
              OverviewTabViewModel,
              ActivitiesTabViewModel,
              NotificationAttachmentsTabViewModel,
              CausesTabViewModel,
              ItemsTabViewModel,
              PartnersTabViewModel,
              TasksTabViewModel) {
        "use strict";

        // constructor
        function NotificationDetailViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._NOTIFICATIONNO = "";
            this._PERS_NO = "";
            this._NOTIFICATION = null;

            //this.meterReplacementInfo = this.scenario.getMeterReplacementInfo();
            //this.setProperty("/view/meterReplacementVisible", this.meterReplacementInfo.visible);

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        NotificationDetailViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        notification: null,
                        view: {
                            busy: false,
                            selectedTabKey: "overview",
                            editAllowed: true,
                            meterReplacementVisible: false,
                            statusButtons: []
                        }
                    };
                }
            },

            getNotificationNo: {
                value: function () {
                    return this._NOTIFICATIONNO;
                }
            },

            setNotificationNo: {
                value: function (notificationNo, persNo) {
                    this._NOTIFICATIONNO = notificationNo;
                    this._PERS_NO = persNo;
                    this.initTabModels(notificationNo, this._requestHelper, this._globalViewModel, this._component);
                }
            },

            setNotification: {
                value: function (notification) {
                    if(notification){
                        notification._IS_HISTORIC = notification.HISTORIC != "";
                        this.setProperty("/notification", notification);
                        this.setEditable(notification.ACTION_FLAG !== "L");
                        this.setNotificationOnTabModels(notification);
                    }
                }
            },

            getNotification: {
                value: function () {
                    return this.getProperty("/notification");
                }
            },

            getHeaderText: {
                value: function () {
                    return this.getNotification()._IS_HISTORIC ? this._component.getModel("i18n_core").getResourceBundle().getText("TITLE_HISTORIC") +" - " + this._component.getModel("i18n_core").getResourceBundle().getText("WOOvNotification") : this._component.getModel("i18n_core").getResourceBundle().getText("WOOvNotification");
                }
            },

            setEditable: {
                value: function (bEditable) {
                    this.setProperty("/notification/_IS_EDITABLE", bEditable);
                }
            },

            getStatusHierarchy: {
                value: function () {
                    return this._component.statusHierarchy;
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        notificationNo: this._NOTIFICATIONNO,
                        spras: this.getLanguage(),
                        persNo: this._PERS_NO
                    };
                }
            },

            setSelectedTabKey: {
                value: function (key) {
                    this.setProperty("/view/selectedTabKey", key);
                }
            },

            setStatusButtonsForStatus: {
                value: function (status, statusProfile) {
                    this.setProperty("/view/statusButtons", this.getStatusHierarchy().getOptionsForStatus(status, statusProfile));
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            _getReturnToListError: {
                value: function (message) {
                    var error = new Error(message);
                    error.name = "RETURN_TO_LIST";

                    return error;
                }
            }
        });

        NotificationDetailViewModel.prototype.tabs = {
            overview: {
                actionName: "overview",
                routeName: "notificationDetailOverview",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new OverviewTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            activities: {
                actionName: "activities",
                routeName: "notificationDetailActivities",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new ActivitiesTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            attachments: {
                actionName: "attachments",
                routeName: "notificationDetailAttachments",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new NotificationAttachmentsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            causes: {
                actionName: "causes",
                routeName: "notificationDetailCauses",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new CausesTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            items: {
                actionName: "items",
                routeName: "notificationDetailItems",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new ItemsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            partners: {
                actionName: "partners",
                routeName: "notificationDetailPartners",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new PartnersTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

            tasks: {
                actionName: "tasks",
                routeName: "notificationDetailTasks",
                model: null,
                initModel: function (notificationNo, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new TasksTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setNotificationNo(notificationNo);
                }
            },

        };

        NotificationDetailViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        NotificationDetailViewModel.prototype.getTabRouteName = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];
            return tabInfo.routeName;
        };

        NotificationDetailViewModel.prototype.setNewNotificationData = function (data, successCb) {
            var that = this;
            var longTextInfo = this.scenario.getComplaintNotificationInfo();

            var notification = new NotificationHeader(data, this._requestHelper, this);
            notification._CATPROFILE = data.CATPROFILE ||data.Equi_RBNR || data.FuncLoc_RBNR || data.RBNR;

            var load_success = function (data) {
                // Set the Catalog Profile
                notification.LONG_TEXT = data;

                that.setNotification(notification);

                that.tabs.overview.model.setDataFromNotification(notification);

                 if (successCb !== undefined) {
                    successCb(that.getNotification());
                }
            };

            notification.loadLongText(longTextInfo.longTextObjectType).then(load_success);

        };

        NotificationDetailViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                if (data.length == 0) {
                    that.executeErrorCallback(that._getReturnToListError(that._component.i18n.getText("ERROR_NO_NOTIF_FOR_NOTIF_NO")));
                    return;
                }

                that.setNewNotificationData(data[0],successCb);
                that.setBusy(false);

            };

            var load_error = function (err) {
                that.executeErrorCallback(that._getReturnToListError(that._component.i18n.getText("ERROR_NOTIF_FETCH")));
            };

            this.setBusy(true);
            this._requestHelper.getData("NotificationDetail", this.getQueryParams(), load_success, load_error, true);
        };

        NotificationDetailViewModel.prototype.resetData = function () {
            this.setNotification(null);
            this._NOTIFICATIONNO = "";
            this._needsReload = true;
            this.resetTabModels();
        };

        NotificationDetailViewModel.prototype.refreshData = function (successCb) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // Mark all other tabs as needsReload = true
            for (var key in this.tabs) {
                if (key != selectedTabKey) {
                    this.tabs[key].model._needsReload = true;
                }
            }

            this.loadData(function () {
                // Publish refresh event for current active tab
                sap.ui.getCore()
                    .getEventBus()
                    .publish(tabInfo.routeName, "refreshRoute");

                successCb();
            });
        };

        NotificationDetailViewModel.prototype.initTabModels = function () {
            for (var key in this.tabs) {
                this.tabs[key].initModel(this.getNotificationNo(), this._requestHelper, this._globalViewModel, this._component, this._errorCallback);
            }
        };

        NotificationDetailViewModel.prototype.setNotificationOnTabModels = function (notification) {
            for (var key in this.tabs) {
                var tab = this.tabs[key];
                if (tab.model) {
                    tab.model.setNotification(notification);
                }
            }
        };

        NotificationDetailViewModel.prototype.resetTabModels = function () {
            for (var key in this.tabs) {
                this.tabs[key].model._needsReload = true;
                this.tabs[key].model.resetData();
            }
        };

        NotificationDetailViewModel.prototype.getTabByRouteName = function (routeName) {
            for (var key in this.tabs) {
                if (this.tabs[key].routeName == routeName) {
                    return this.tabs[key];
                }
            }

            return null;
        };

        NotificationDetailViewModel.prototype.loadAllCounters = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/activitiesCounter", data[0].ActivitiesCounter[0].COUNT.toString());
                that.setProperty("/causesCounter", data[1].CausesCounter[0].COUNT.toString());
                that.setProperty("/itemsCounter", data[2].ItemsCounter[0].COUNT.toString());
                that.setProperty("/tasksCounter", data[3].TasksCounter[0].COUNT.toString());
                that.setProperty("/partnersCounter", data[4].PartnerCounter[0].COUNT.toString());
                that.setProperty("/attachmentCounter", data[5].AttachmentCounter[0].COUNT.toString());
            };

            var fetchCounterQueue = [
                this._fetchActivitiesCount(),
                this._fetchCausesCount(),
                this._fetchItemsCount(),
                this._fetchTaskCount(),
                this._fetchPartnerCount(),
                this._fetchAttachmentsCount()
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);
        };

        NotificationDetailViewModel.prototype._fetchActivitiesCount = function(){
            return this._requestHelper.fetch({
                id: "ActivitiesCounter",
                statement: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM NotificationActivity AS a LEFT JOIN NotificationActMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ACT_KEY=b.ACT_KEY AND b.SPRAS = '$3' WHERE a.NOTIF_NO = '@notifNo' AND a.ITEM_KEY = '0000' "
            }, {
                notifNo: this.getNotificationNo()
            });
        };

        NotificationDetailViewModel.prototype._fetchCausesCount = function(){
            return this._requestHelper.fetch({
                id: "CausesCounter",
                statement: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM NotificationCause AS a LEFT JOIN NotificationCauseMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ITEM_KEY=b.ITEM_KEY AND a.CAUSE_KEY=b.CAUSE_KEY "
                    + "AND b.NOTIF_NO = '$2' AND b.SPRAS = '$3' WHERE a.NOTIF_NO = '@notifNo'"
            }, {
                notifNo: this.getNotificationNo()
            });
        };

        NotificationDetailViewModel.prototype._fetchItemsCount = function(){
            return this._requestHelper.fetch({
                id: "ItemsCounter",
                statement: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM NotificationItem AS a LEFT JOIN NotificationItemMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ITEM_KEY=b.ITEM_KEY AND b.NOTIF_NO = '$2' AND b.SPRAS = '$3' "
                    + "WHERE a.NOTIF_NO = '@notifNo'"
            }, {
                notifNo: this.getNotificationNo()
            });
        };

        NotificationDetailViewModel.prototype._fetchTaskCount = function(){
            return this._requestHelper.fetch({
                id: "TasksCounter",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM NotificationTask WHERE NOTIF_NO='@notifNo' and ITEM_KEY = '0000' "
            }, {
                notifNo: this.getNotificationNo()
            });
        };

        NotificationDetailViewModel.prototype._fetchPartnerCount = function(){
            return this._requestHelper.fetch({
                id: "PartnerCounter",
                statement:  "SELECT COUNT(np.MOBILE_ID) as COUNT FROM NotificationPartner AS np LEFT JOIN PartnerHeader AS ph ON ph.PARTNER_KEY = np.PARTNER_KEY AND ph.PARTNER_ROLE = np.PARTNER_ROLE " +
                    "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=np.PARTNER_ROLE AND pd.SPRAS = '@language'  WHERE np.OBJNR='@objNr' " +
                     "AND np.ACTION_FLAG!='D' AND np.ACTION_FLAG!='M'"
            }, {
                objNr: this.getNotification().OBJECT_NO,
                language: this.getLanguage()
            });
        };

        NotificationDetailViewModel.prototype._fetchAttachmentsCount = function(){
            return this._requestHelper.fetch({
                id: "AttachmentCounter",
                statement:  "SELECT SUM(i) AS COUNT FROM (SELECT COUNT(MOBILE_ID) as COUNT FROM SAMObjectAttachments WHERE OBJKEY='@notifNo' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' " +
                            "UNION ALL SELECT COUNT(MOBILE_ID) as COUNT FROM DMSATTACHMENTS WHERE OBJKEY='@notifNo' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' "
            }, {
                notifNo: this.getNotificationNo(),
            });
        };



        NotificationDetailViewModel.prototype.constructor = NotificationDetailViewModel;

        return NotificationDetailViewModel;
    });
