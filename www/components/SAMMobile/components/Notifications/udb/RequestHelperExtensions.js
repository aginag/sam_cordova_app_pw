sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                NotificationList: ["nh.MOBILE_ID", "nh.NOTIF_NO", "nh.SHORT_TEXT", "nh.NOTIF_DATE", "nh.NOTIFTIME", "nh.FUNCT_LOC", "nh.NOTIF_TYPE", "nh.ACTION_FLAG", "pt.PRIORITY_DESC", "nt.QMARTX", "fl.SHTXT as FL_SHTXT"],
                FunctionLocs: ["MOBILE_ID", "EQUNR", "TPLNR", "FUNCLOC_DISP", "EQTYP", "EQART", "OBJNR", "MATNR", "WERK", "LAGER", "SHTXT", "TPLNR_SHTXT", "TPLMA_SHTXT", "FLTYP", "SERNR", "STREET", "POST_CODE1",
                    "SWERK", "HERLD", "TPLMA", "CITY1", "COUNTRY", "ACTION_FLAG"],
                EquipmentsJoinsMl: ["e.MOBILE_ID", "e.TPLNR", "e.HEQUI", "e.EQUIPMENT_DISPLAY", "e.SHTXT", "e.OBJNR", "e.EQUNR", "e.STREET", "e.SWERK", "e.POST_CODE1", "e.CITY1", "e.COUNTRY", "e.RBNR", "e.TPLNR_SHTXT", "e.ACTION_FLAG"],
                NotificationActivityJoinsMl: ["a.MOBILE_ID", "a.NOTIF_NO", "a.ACTION_FLAG", "a.LONG_TEXT", "a.END_DATE", "a.END_TIME", "a.ITEM_KEY", "a.ITEM_SORT_NO", "a.ACT_KEY", "a.ACTTEXT", "a.ACT_CAT_TYP", "a.ACT_CODEGRP", "a.ACT_CODE", "a.START_DATE", "a.START_TIME"],
                NotificationCauseJoinsMl: ["a.MOBILE_ID", "a.NOTIF_NO", "a.ITEM_KEY", "a.CAUSE_CAT_TYP", "a.CAUSE_SORT_NO", "a.CAUSE_KEY", "a.CAUSETEXT", "a.CAUSE_CODE", "a.CAUSE_CODEGRP", "a.LONG_TEXT", "a.TXT_CAUSEGRP",
                    "a.DELETE_FLAG", "a.TXT_CAUSECD", "a.ACTION_FLAG"],
                NotificationItemJoinsMl: ["a.MOBILE_ID", "a.NOTIF_NO", "a.ITEM_KEY", "a.ITEM_SORT_NO", "a.DESCRIPT", "a.ASSEMBLY", "a.D_CAT_TYP", "a.D_CODE", "a.D_CODEGRP", "a.DL_CAT_TYP", "a.DL_CODE", "a.DL_CODEGRP",
                    "a.DELETE_FLAG", "a.TXT_GRPCD", "a.TXT_OBJPTCD", "a.ACTION_FLAG", "a.LONG_TEXT"],
                NotificationTask: ["MOBILE_ID", "NOTIF_NO", "ITEM_KEY", "ITEM_SORT_NO", "TASK_KEY", "TASK_SORT_NO", "TASK_TEXT", "OBJECT_NO", "TASK_CAT_TYP", "TASK_CODE", "TASK_CODEGRP", "TXT_TASKGRP", "TXT_TASKCD", "LONG_TEXT",
                    "XA_STAT_PROF", "DELETE_FLAG", "ACTION_FLAG"],
                NotificationPartnerJoinsPartnerHeader: ["np.MOBILE_ID", "np.NAME_LIST", "np.PARTNER_KEY", "np.PARTNER_ROLE", "pd.DESCRIPTION AS ROLE_DESCRIPTION", "np.ACTION_FLAG", "ph.STREET", "ph.CITY1", "ph.POST_CODE1", "ph.COUNTRY",
                    "ph.TEL_NUMBER", "ph.FAX_NUMBER"],
                NotificationJoinsAttachments: ["MOBILE_ID", "FILEEXT", "FILENAME", "TABNAME", "OBJKEY", "DOC_SIZE", "ACTION_FLAG"],
                NotificationHeader: ["MOBILE_ID", "NOTIF_NO", "NOTIF_NO_DISPLAY", "SHORT_TEXT", "NOTIF_DATE", "OBJECT_NO", "NOTIF_TYPE", "EQUIPMENT", "EQUIPMENT_DESC", "SHORT_TEXT", "PRIORITY", "PRIOTYPE", "MATERIAL", "SERIALNO",
                    "FUNCT_LOC", "FUNCLOC_DESC", "NOTIF_DATE", "COMPDATE", "COMPTIME", "NOTIFTIME", "MN_WK_CTR", "PLANPLANT", "PLANT", "CATPROFILE", "ACTION_FLAG", "REPORTEDBY"],
                EquipmentNotifHist: ["nh.MOBILE_ID", "nh.NOTIF_NO", "nh.NOTIF_TYPE", "nh.SHORT_TEXT", "nh.NOTIF_DATE", "nh.ACTION_FLAG"],
                EquipmentOrderHist: ["o.MOBILE_ID", "o.ORDERID", "o.ORDER_TYPE", "o.SHORT_TEXT", "o.START_DATE", "o.ACTION_FLAG", "op.ACTIVITY", "op.PERS_NO"],
            },
            queries: {
                Notifications: {
                    local: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM NotificationHeader as nh " +
                            "LEFT JOIN FUNCTIONLOC as fl on fl.TPLNR = nh.FUNCT_LOC " +
                            "LEFT JOIN PriorityTypes as pt ON pt.PRIORITY = nh.PRIORITY AND pt.PRIOTYPE = nh.NOTIF_TYPE AND pt.SPRAS = '$3'" +
                            "LEFT JOIN NotificationType AS nt ON nt.QMART = nh.NOTIF_TYPE AND nt.SPRAS = '$4' " +
                            " WHERE nh.ACTION_FLAG = 'N' AND (" +
                            "UPPER(nh.NOTIF_NO) LIKE UPPER('%$5%') OR " +
                            "UPPER(nh.SHORT_TEXT) LIKE UPPER('%$6%') OR " +
                            "UPPER(nh.NOTIF_DATE) LIKE UPPER('%$7%') OR " +
                            "UPPER(nh.FUNCT_LOC) LIKE UPPER('%$8%') OR " +
                            "UPPER(nh.NOTIF_TYPE) LIKE UPPER('%$9%') OR " +
                            "UPPER(nt.QMARTX) LIKE UPPER('%$10%') OR " +
                            "UPPER(FL_SHTXT) LIKE UPPER('%$11%')) ORDER BY nh.NOTIF_DATE DESC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "spras"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "searchString"
                        }, {
                            parameter: "$11",
                            value: "searchString"
                        }],
                        columns: "NotificationList"
                    },

                    historic: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM NotificationHeader as nh " +
                            "LEFT JOIN FUNCTIONLOC as fl on fl.TPLNR = nh.FUNCT_LOC " +
                            "LEFT JOIN PriorityTypes as pt ON pt.PRIORITY = nh.PRIORITY AND pt.PRIOTYPE = nh.NOTIF_TYPE AND pt.SPRAS = '$3' " +
                            "LEFT JOIN NotificationType AS nt ON nt.QMART = nh.NOTIF_TYPE AND nt.SPRAS = '$4' " +
                            "WHERE nh.ACTION_FLAG != 'N' AND " +
                            "UPPER(nh.REPORTEDBY) = UPPER('$12') AND " +
                            "nh.NOTIF_DATE > dateadd(week,-4,getdate()) AND (" +
                            "UPPER(nh.NOTIF_NO) LIKE UPPER('%$5%') OR " +
                            "UPPER(nh.SHORT_TEXT) LIKE UPPER('%$6%') OR " +
                            "UPPER(nh.NOTIF_DATE) LIKE UPPER('%$7%') OR " +
                            "UPPER(nh.FUNCT_LOC) LIKE UPPER('%$8%') OR " +
                            "UPPER(nh.NOTIF_TYPE) LIKE UPPER('%$9%') OR " +
                            "UPPER(nt.QMARTX) LIKE UPPER('%$10%') OR " +
                            "UPPER(FL_SHTXT) LIKE UPPER('%$11%')) ORDER BY nh.NOTIF_DATE DESC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "spras"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "searchString"
                        }, {
                            parameter: "$11",
                            value: "searchString"
                        }, {
                            parameter: "$12",
                            value: "userName"
                        }],
                        columns: "NotificationList"
                    }
                },

                LocalNotificationCount: {
                    get: {
                        query: "SELECT COUNT(mobile_id) AS COUNT FROM NotificationHeader WHERE ACTION_FLAG = 'N'",
                        params: [],
                        columns: ""
                    }
                },

                HistoricNotificationCount: {
                    get: {
                        query: "SELECT COUNT(mobile_id) AS COUNT FROM NotificationHeader WHERE ACTION_FLAG != 'N' AND UPPER(REPORTEDBY) = UPPER('$1') AND NOTIF_DATE > dateadd(week,-4,getdate())",
                        params: [{
                            parameter: "$1",
                            value: "userName"
                        }],
                        columns: ""
                    }
                },

                WorkCenterMl: {
                    get: {
                        query: "SELECT * FROM WorkCenterMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                PriorityTypes: {
                    get: {
                        query: "SELECT * FROM PriorityTypes WHERE SPRAS = '$1' ORDER BY PRIORITY ASC",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                NotificationType: {
                    get: {
                        query: "SELECT * FROM NotificationType WHERE SPRAS='$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                UserStatusMl: {
                    get: {
                        query: "SELECT * FROM UserStatusMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }],
                        columns: ""
                    }
                },

                FunctionLocs: {
                    get: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM FUNCTIONLOC  "
                            + "WHERE (UPPER(SHTXT) LIKE UPPER('%$5') OR FUNCLOC_DISPLAY LIKE '%$6')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "FunctionLocs"
                    }
                },

                Equipments: {
                    installedAtFuncLoc: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM EQUIPMENT as e "
                            + "WHERE e.TPLNR='$4' AND (UPPER(e.SHTXT) LIKE UPPER('%$5') OR e.EQUIPMENT_DISPLAY LIKE '%$6')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "tplnr"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "EquipmentsJoinsMl"
                    }
                },
                CatProfile: {
                        get: {
                            query: "select RBNR as catalogProfile from FunctionLoc where TPLNR = '$1'",
                            params: [{
                                parameter : "$1",
                                value: "funclocId"
                            }]
                        }
                    },

                CodeGroupMl: {
                    get: {
                        query: "SELECT * FROM CodeGroupMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }],
                        columns: ""
                    },
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 cg.mobile_id, ml.SPRAS, cg.CODE_CAT_GROUP, cg.CODEGROUP, cg.CATALOG_PROFILE, ml.CODEGROUP_TEXT FROM CODEGROUP as cg " +
                            "JOIN CODEGROUPML as ml on ml.CODE_CAT_GROUP = cg.CODE_CAT_GROUP and ml.CATALOG_PROFILE = cg.CATALOG_PROFILE "
                            + "WHERE cg.CATALOG_TYPE = '$7' AND cg.CATALOG_PROFILE = '$4' AND ml.SPRAS = '$3' AND (UPPER(ml.CODEGROUP_TEXT) LIKE UPPER('%$5%') OR UPPER(cg.CODE_CAT_GROUP) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "catalogProfile"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    }
                },

                CodeMl: {
                    get: {
                        query: "SELECT * FROM CodeMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }],
                        columns: ""
                    },

                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 CO.ACTION_FLAG, CO.CATALOG_TYPE, CO.CODE, CO.CODE_GROUP, CO.CODE_TEXT, CO.LANGUAGE_KEY, CO.LONGTEXT, CO.MOBILE_ID, CO.SPRAS, CG.CODEGROUP, CG.CODEGROUP_TEXT " +
                            "FROM CodeGroup as CG INNER JOIN CodeMl as CO ON CO.CODE_GROUP = CG.CODEGROUP " +
                            "WHERE CG.CATALOG_TYPE = '$7' AND CO.SPRAS = '$3' AND CG.CATALOG_PROFILE = '$4' AND (UPPER(CO.CODE_TEXT) LIKE UPPER('%$5%') OR UPPER(CO.CODE) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "catalogProfile"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    },
                    valueHelpOutageCodes: {
                        query: "SELECT TOP $1 START AT $2 CO.ACTION_FLAG, CO.CATALOG_TYPE, CO.CODE, CO.CODE_GROUP, CO.CODE_TEXT, CO.LANGUAGE_KEY, CO.LONGTEXT, CO.MOBILE_ID, CO.SPRAS " +
                            "FROM CodeMl as CO INNER JOIN CodeGroup as CG " +
                            "ON CO.CODE_GROUP = CG.CODEGROUP " +
                            "WHERE CG.CATALOG_TYPE = '$7' AND CO.SPRAS = '$3' AND CG.CATALOG_PROFILE = '$4' AND CO.CODE_GROUP = '$8' AND (UPPER(CO.CODE_TEXT) LIKE UPPER('%$5%') OR UPPER(CO.CODE) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "catalogProfile"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }, {
                            parameter: "$8",
                            value: "codeGroup"
                        }]
                    }
                },

                NotificationDetail: {
                    get: {
                        query: "SELECT * FROM NotificationHeader WHERE NOTIF_NO= '$1'",
                        params: [{
                            "parameter": "$1",
                            "value": "notificationNo"
                        }],
                        columns: "NotificationHeader"
                    }
                },

                NotificationAttachment: {
                    get: {
                        query: "SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM SAMObjectAttachments WHERE OBJKEY= '$1' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' "+
                                "UNION ALL SELECT MOBILE_ID, FILEEXT, FILENAME, TABNAME, OBJKEY, DOC_SIZE, ACTION_FLAG FROM DMSATTACHMENTS WHERE OBJKEY= '$2' AND ACTION_FLAG != 'A' AND ACTION_FLAG != 'B' ",
                        params: [{
                            parameter: "$1",
                            value: "notificationNo"
                        },{
                            parameter: "$2",
                            value: "notificationNo"
                        }],
                        columns: "NotificationJoinsAttachments"
                    },
                    preSync: {
                        query: "SELECT $COLUMNS FROM SAMOBJECTATTACHMENTS WHERE ACTION_FLAG='N'",
                        params: [],
                        columns: "NotificationJoinsAttachments"
                    }
                },

                NotificationActivity: {
                    get: {
                        query: "SELECT $COLUMNS FROM NotificationActivity AS a LEFT JOIN NotificationActMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ACT_KEY=b.ACT_KEY AND b.SPRAS = '$3' WHERE a.NOTIF_NO = '$1'" +
                            "ORDER BY a.ACT_KEY",
                        params: [{
                            parameter: "$1",
                            value: "notificationNo"
                        }, {
                            parameter: "$2",
                            value: "notificationNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "NotificationActivityJoinsMl"
                    }
                },

                NotificationItem: {
                    get: {
                        query: "SELECT $COLUMNS FROM NotificationItem AS a LEFT JOIN NotificationItemMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ITEM_KEY=b.ITEM_KEY AND b.NOTIF_NO = '$2' AND b.SPRAS = '$3' "
                            + "WHERE a.NOTIF_NO = '$1' ORDER BY a.ITEM_KEY",
                        params: [{
                            parameter: "$1",
                            value: "notificationNo"
                        }, {
                            parameter: "$2",
                            value: "notificationNo"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }],
                        columns: "NotificationItemJoinsMl"
                    }
                },

                NotificationTask: {
                    get: {
                        query: "SELECT $COLUMNS FROM NotificationTask WHERE NOTIF_NO='$1' ORDER BY TASK_KEY",
                        params: [{
                            parameter: "$1",
                            value: "notificationNo"
                        }],
                        columns: "NotificationTask"
                    }
                },

                NotificationPartner: {
                    get: {
                        query: "SELECT $COLUMNS FROM NotificationPartner AS np LEFT JOIN PartnerHeader AS ph ON ph.PARTNER_KEY = np.PARTNER_KEY AND ph.PARTNER_ROLE = np.PARTNER_ROLE " +
                            "LEFT JOIN PartnerDetermination AS pd ON pd.FUNCTION = 'PM' AND pd.ROLE=np.PARTNER_ROLE AND pd.SPRAS = '$2'  WHERE np.OBJNR='$1' "
                            + "AND np.ACTION_FLAG!='D' AND np.ACTION_FLAG!='M'",
                        params: [{
                            parameter: "$1",
                            value: "objNr"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }],
                        columns: "NotificationPartnerJoinsPartnerHeader"
                    }
                },

                NotificationCause: {
                    get: {
                        query: "SELECT $COLUMNS FROM NotificationCause AS a LEFT JOIN NotificationCauseMl AS b ON a.NOTIF_NO=b.NOTIF_NO AND a.ITEM_KEY=b.ITEM_KEY AND a.CAUSE_KEY=b.CAUSE_KEY "
                            + "AND b.NOTIF_NO = '$2' AND b.SPRAS = '$3' WHERE a.NOTIF_NO = '$1' ORDER BY a.CAUSE_KEY",
                        params: [{
                            parameter: "$1",
                            value: "notificationNo"
                        }, {
                            parameter: "$2",
                            value: "notificationNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "NotificationCauseJoinsMl"
                    }
                },

                FunctionLocOrderHist: {
                    get: {
                        query: "SELECT * FROM SAM_ORDER_HIST as oh " +
                            "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                            "JOIN ORDEROPERATIONS as op on op.ORDERID = o.ORDERID " +
                            "WHERE o.FUNCT_LOC = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "funcLocId"
                        }]
                    }
                },

                FunctionLocNotifHist: {
                    get: {
                        query: "SELECT * FROM SAM_NOTIF_HIST as oh " +
                            "JOIN NOTIFICATIONHEADER as nh on nh.NOTIF_NO = oh.NOTIF_NO " +
                            "WHERE nh.FUNCT_LOC = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "funcLocId"
                        }]
                    }
                },

                EquipmentOrderHist: {
                    get: {
                        query: "SELECT $COLUMNS FROM SAM_ORDER_HIST as oh " +
                            "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                            "JOIN ORDEROPERATIONS as op on op.ORDERID = o.ORDERID " +
                            "WHERE o.EQUIPMENT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "equipmentNumber"
                        }],
                        columns: "EquipmentOrderHist"
                    }
                },

                EquipmentNotifHist: {
                    get: {
                        query: "SELECT $COLUMNS FROM SAM_NOTIF_HIST as oh " +
                            "JOIN NOTIFICATIONHEADER as nh on nh.NOTIF_NO = oh.NOTIF_NO " +
                            "WHERE nh.EQUIPMENT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "equipmentNumber"
                        }],
                        columns: "EquipmentNotifHist"
                    }
                },

                NotifItemMaterial: {
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 i.MATERIAL, i.ENTRY_QNT, i.ENTRY_UOM, m.MATL_DESC FROM INVENTORYLIST as i " +
                            "LEFT JOIN MATERIALML as m ON i.MATERIAL = m.MATERIAL AND m.SPRAS = '$3' AND (UPPER(m.MATL_DESC) LIKE UPPER('%$4%') OR UPPER(i.MATERIAL) LIKE UPPER('%$5%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }]
                    }
                },
                ZPMCCodegroup: {
                    get: {
                        query: "SELECT * FROM ZPMCCodegroup",
                        params: []
                    }
                },
                ZPMCCode: {
                    get: {
                        query: "SELECT CODE, TEXT FROM ZPMC_CODE",
                        params: []
                    }
                },

            }
        };
    });
