sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function Scenario(name, config) {
            this.name = name;
            this.longTexts = config.longTexts;
            this.complaintNotification = config.complaintNotification;
            this.availableNotificationTypes = config.availableNotificationTypes;
            this.afterSync = config.afterSync;
            this.notificationUserStatus = config.notificationUserStatus;
        }

        Scenario.prototype = {
            getListItemLongtextInformation: function () {
                return this.longTexts.list;
            },

            getDescriptionTabLongtextInformation: function() {
                return this.longTexts.descriptionTab;
            },

            getComplaintNotificationInfo: function() {
                return this.complaintNotification;
            },

            getAcceptedNotificationTypes: function () {
                return this.availableNotificationTypes;
            },

            getAfterSyncStatuses: function() {
                return this.afterSync;
            },

            getNotificationUserStatus: function () {
                return this.notificationUserStatus;
            }
        };

        Scenario.prototype.constructor = Scenario;

        return Scenario;
    });