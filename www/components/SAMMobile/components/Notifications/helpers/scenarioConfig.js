sap.ui.define([],
    function () {
        "use strict";
        return {

            SAM: {
                longTexts: {
                    list: {
                        isOrderLongtext: true,
                        objectType: "AUFK",
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: true,
                        objectType: "AUFK",
                        objectName: "Order",
                        readOnly: false
                    }
                },
                complaintNotification: {
                    visible: true,
                    completeStatus: {
                        STATUS_PROFILE: "NORDEX3",
                        USER_STATUS: "E0004"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: true,
                    showWarrantyFlag: true
                },
                availableNotificationTypes: ["P1"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS1"
                    }
                },
                notificationUserStatus: {
                    stör: {
                        STATUS: "E0003",
                        USER_STATUS_CODE: "STÖR",
                        USER_STATUS_DESC: "Störungsmeldung"
                    },
                    vdn: {
                        STATUS: "E0001",
                        USER_STATUS_CODE: "VDN",
                        USER_STATUS_DESC: "VDN zugeordnet"
                    }
                }
            }

        };
    });