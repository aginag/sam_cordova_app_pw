sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/models/dataObjects/NotificationUserStatus"],

    function (JSONModel, NotificationUserStatus) {
        "use strict";

        // constructor
        function NNotificationUserStatus(data, requestHelper, model) {
            NotificationUserStatus.call(this, data, requestHelper, model);
        }

        NNotificationUserStatus.prototype = Object.create(NotificationUserStatus.prototype, {

        });

        NNotificationUserStatus.prototype.constructor = NotificationUserStatus;

        return NNotificationUserStatus;
    }
);
