sap.ui.define([
    "sap/ui/model/json/JSONModel",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/components/Notifications/models/NotificationActivity",
        "SAMMobile/components/Notifications/models/NotificationCause",
        "SAMMobile/components/Notifications/models/NotificationUserStatus"
    ],

    function (JSONModel, NotificationHeader, NotificationActivity, NotificationCause, NotificationUserStatus) {
        "use strict";

        // constructor
        function NNotificationHeader(data, requestHelper, model) {
            NotificationHeader.call(this, data, requestHelper, model);
        }

        NNotificationHeader.prototype = Object.create(NotificationHeader.prototype, {

        });

        NNotificationHeader.prototype.loadActivities = function() {
            var that = this;


            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setActivities(that.mapDataToDataObject(NotificationActivity, data.NotificationActivity));
                    resolve(that.getActivities());
                };

                that._fetchActivities().then(load_success).catch(reject);
            });

        };

        NNotificationHeader.prototype.loadCauses = function () {
            var that = this;

            return new Promise(function(resolve, reject) {
                var load_success = function(data) {
                    that.setCauses(that.mapDataToDataObject(NotificationCause, data.NotificationCause));
                    resolve(that.getCauses());
                };

                that._fetchCauses().then(load_success).catch(reject);
            });
        };

        NNotificationHeader.prototype.loadUserStatus = function () {
            var that = this;

            return new Promise(function (resolve, reject) {
                var load_success = function (data) {
                    that.setUserStatus(that.mapDataToDataObject(NotificationUserStatus, data.NotificationUsrStat));
                    resolve(that.getUserStatus);
                };

                that._fetchUserStatus().then(load_success).catch(reject);
            });
        };

        NNotificationHeader.prototype.constructor = NNotificationHeader;

        return NNotificationHeader;
    }
);
