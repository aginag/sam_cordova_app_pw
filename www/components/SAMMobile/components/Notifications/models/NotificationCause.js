sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/models/dataObjects/NotificationCause"],

    function (JSONModel, NotificationCause) {
        "use strict";

        // constructor
        function NNotificationCause(data, requestHelper, model) {
            NotificationCause.call(this, data, requestHelper, model);

            this._VALUE_STATE_MODEL = {
                _startTime: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                _endTime: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                CAUSE_CODEGRP: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                CAUSE_CODE: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                CAUSETEXT: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                }
            };
        }

        NNotificationCause.prototype = Object.create(NotificationCause.prototype, {

        });

        NNotificationCause.prototype.constructor = NNotificationCause;

        return NNotificationCause;
    }
);
