sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/models/dataObjects/NotificationActivity"],

    function (JSONModel, NotificationActivity) {
        "use strict";

        // constructor
        function NNotificationActivity(data, requestHelper, model) {
            NotificationActivity.call(this, data, requestHelper, model);

            this._VALUE_STATE_MODEL = {
                _startTime: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                _endTime: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                ACT_CODEGRP: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                ACT_CODE: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                },
                ACT_CODE_TEXT: {
                    Type: sap.m.ButtonType.Transparent,
                    text: ""
                },
                ACTTEXT: {
                    state: sap.ui.core.ValueState.None,
                    text: ""
                }
            };
        }

        NNotificationActivity.prototype = Object.create(NotificationActivity.prototype, {

        });

        NNotificationActivity.prototype.constructor = NNotificationActivity;

        return NNotificationActivity;
    }
);
