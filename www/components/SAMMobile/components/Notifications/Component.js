sap.ui.define(["sap/ui/core/UIComponent",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/RequestHelperCore",
        "SAMMobile/components/Notifications/udb/RequestHelperExtensions",
        "SAMMobile/models/CustomizingModel",
        "SAMMobile/components/Notifications/helpers/Scenario",
        "SAMMobile/components/Notifications/helpers/scenarioConfig"
    ],

    function (UIComponent,
              JSONModel,
              RequestHelperCore,
              RequestHelperExtensions,
              CustomizingModel,
              Scenario,
              scenarioConfig) {
        "use strict";

        return UIComponent.extend("SAMMobile.components.Notifications.Component", {
            metadata: {
                manifest: "json"
            },

            customizingTables: ["WorkCenterMl", "PriorityTypes", "NotificationType", "UserStatusMl"],
            columnDefinitions: [],

            /**
             * The component is initialized by UI5 automatically during the startup
             * of the app and calls the init method once.
             *
             * @public
             * @override
             */
            init: function () {
                var that = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                sap.ui.getCore().getEventBus().subscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);
                jQuery.sap.log.setLevel(jQuery.sap.log.Level.INFO);

                this.rootComponent = sap.ui.getCore().byId("samComponent").getComponentInstance();
                this.rootComponent.scenarioComponent = this;

                this.i18n = this.getModel("i18n").getResourceBundle();
                this.i18n_core = this.getModel("i18n_core").getResourceBundle();

                var queryExtensions = this._getRequestHelperExtensions();
                this.requestHelper = new RequestHelperCore(queryExtensions.tableColumns, queryExtensions.queries);
                this.formatter = this.rootComponent.formatter;

                this.globalViewModel = this.rootComponent.globalViewModel;
                this.customizingModel = new CustomizingModel(this.getManifestObject(), this.requestHelper, this.customizingTables, "NOTIFICATIONS_CUSTOMIZING_MODEL");

                this.masterDataLoaded = false;

                this.setModel(this.customizingModel.model, "customizingModel");
                this.setModel(this.rootComponent.getModel("device"), "device");

                this.getRouter().initialize();
            },

            setBusyOn: function () {
                this.rootComponent.globalViewModel.model.setProperty('/busy', true);
            },

            setBusyOff: function () {
                this.rootComponent.globalViewModel.model.setProperty('/busy', false);
            },

            onBeforeRendering: function() {
                var sheet = document.createElement('style');
                sheet.innerHTML = ".dialogText {\n" +
                    "    padding: 30px;\n" +
                    "}\n" +
                    "\n" +
                    ".notificationOutageTimeList .sapMListTblAlternateRowColorsPopin>:nth-child(4n-1), .notificationOutageTimeList .sapMListTblAlternateRowColorsPopin>:nth-child(4n) {\n" +
                    "    background-color: #cecece;\n" +
                    "}\n";
                document.body.appendChild(sheet);
            },
            exit: function(){
                sap.ui.getCore().getEventBus().unsubscribe("sync", "onAfterSync", this.onAfterSync, this);
                },
            onAfterSync: function () {
                this.executingSyncRefresh();
            },

            getBarcodeScannerSettings: function() {
                var samConfig = this.getManifestObject().getEntry("sap.app").SAMConfig;
                return samConfig.barcodeScanner;
            },

            onLogout: function() {
                //Clearing localstorage
                this.customizingModel.resetCache();
                localStorage.removeItem(this.customizingModel.customizingModelLsKey);
                this.masterDataLoaded = false;
            },

            executingSyncRefresh: function() {
                var that = this;

                this.loadMasterData(true, function () {

                    switch (that.globalViewModel.model.getProperty("/currentRoute")) {
                        case "notifications":
                            sap.ui.getCore().getEventBus().publish("notificationsRoute", "refreshRoute");
                            break;
                        default:
                            sap.ui.getCore().getEventBus().publish("notificationsRoute", "refreshRoute");
                            break;
                    }

                });
            },

            sync: function(syncMode) {
                //TODO WORKAROUND, calling sync need to be available in the rootComponent
                var view = this.rootComponent.getRouter().getView("SAMMobile.view.home.HomeToolpage");
                var controller = view.getController();

                controller.onSync(controller, syncMode || "", this.getManifestObject().getEntry("sap.app").SAMConfig.publication);
            },

            loadMasterData: function (hardReset, successCb) {
                var that = this;

                this.setBusyOn();

                Promise.all([this.loadColumnDefinitions(hardReset), this.loadCustomizingModel(hardReset)]).then(function (result) {
                    // Set status profile flow/hierarchy
                    that.scenario = new Scenario("SAM", scenarioConfig["SAM"]);
                    that.setBusyOff();
                    that.masterDataLoaded = true;
                    successCb();
                }).catch(function (err) {

                    alert(err.message+ "\n" + err.stack);
                });
            },

            loadCustomizingModel: function (bHardReset) {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.customizingModel.loadData(that.globalViewModel.getActiveUILanguage(), bHardReset, resolve, reject);
                });
            },

            loadColumnDefinitions: function (bHardReset) {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.requestHelper.loadColumnDefinitions(that.columnDefinitions, bHardReset, resolve, reject);
                });
            },

            loadScenarioName: function () {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.requestHelper.getData("Scenario", {
                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                    }, resolve, reject);
                });
            },

            getGrowingListSettings: function (object) {
                var samConfig = this.getManifestObject().getEntry("sap.app").SAMConfig;
                var listConfig = samConfig.growingLists;

                switch (object) {
                    case "selectDialogs":
                        return listConfig.selectDialogs;
                    default:
                        return {};
                }
            },

            getAttachmentScenario: function () {
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentScenario;
            },

            getAttachmentButtonNotificationNewPhoto: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonNotificationNewPhoto;
            },
            getAttachmentButtonNotificationNewAudio: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonNotificationNewAudio;
            },
            getAttachmentButtonNotificationNewVideo: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonNotificationNewVideo;
            },
            getAttachmentButtonNotificationNewFile: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonNotificationNewFile;
            },

            _getRequestHelperExtensions: function () {
                return {
                    tableColumns: Object.assign(RequestHelperExtensions.tableColumns, {}),
                    queries: Object.assign(RequestHelperExtensions.queries, {})
                };
            },

            getConfirmActionDialog: function(message, executeCb) {
                return this.rootComponent._getConfirmActionDialog(message, executeCb);
            }
        });

    });