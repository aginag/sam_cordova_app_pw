sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/models/dataObjects/EquipmentCharacteristic",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/models/dataObjects/Equipment",
        "SAMMobile/models/dataObjects/MeasdocLongtexts",
        "SAMMobile/models/dataObjects/MeasurementDocument",
        "SAMMobile/controller/common/GenericSelectDialog",
        "sap/m/MessageBox"
    ],
    function (ViewModel, Filter, JSONModel, Validator, EquipmentCharacteristic, DMSAttachment, Equipment, MeasdocLongtexts, MeasurementDocument, GenericSelectDialog, MessageBox) {
        "use strict";

        // constructor
        function EquipmentCreationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/SRBEquipments";

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EquipmentCreationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        searchString: "",
                        view: {
                            busy: false,
                            errorMessages: []
                        },
                        classifications: null,
                        classificationVisible:false,
                        templateAvailable: false
                    };
                }
            },

            getEquipmentQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        searchString: this.getSearchString()
                    };
                }
            },

            setEquipment: {
                value: function (equipment) {
                    this.setProperty("/equipment", equipment);
                }
            },

            getEquipment: {
                value: function () {
                    return this.getProperty("/equipment");
                }
            },

            setMeasPointForEquipment: {
                value: function (measpoint) {
                    this.setProperty("/measpoint", measpoint);
                }
            },

            getMeasPointForEquipment: {
                value: function () {
                    return this.getProperty("/measpoint");
                }
            },

            getEquipmentMeasurementDocument: {
                value: function () {
                    return this.getProperty("/equipment/measurementDocument");
                }
            },

            setEquipmentMeasurementDocument: {
                value: function (equipmentMeasurementDocument) {
                    this.setProperty("/equipment/measurementDocument", equipmentMeasurementDocument);
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getClassifications: {
                value: function () {
                    return this.getProperty("/classifications");
                }
            },

            setClassifications: {
                value: function (classifications) {
                    var that = this;
                    var mappedClassifications = classifications.map(function (characteristic) {
                        if (characteristic.EQUNR) {
                            characteristic.VALUE_NEUTRAL_FROM = "";
                            characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL = "";
                            characteristic.ACTION_FLAG = "N";
                            if(characteristic.ATZHL !== '' && characteristic.ATWRT !== '' && characteristic.ATINN !== '' && characteristic.characteristicCount == 1 && characteristic.ATWRT){
                                characteristic.VALUE_NEUTRAL_FROM = characteristic.ATWRT;
                            }
                            return new EquipmentCharacteristic(characteristic, that._requestHelper, that);
                        }
                    });
                    this.setProperty("/classifications", mappedClassifications);
                }
            },

            getClassification: {
                value: function () {
                    return this.getProperty("/classification");
                }
            },

            setClassification: {
                value: function (classification) {
                    this.setProperty("/classification", classification);
                }
            },

            getHeaderText: {
                value: function() {
                    return this._component.i18n.getText("WORKING_FUNDS");
                }
            },

            setConstructionDate: {
                value: function(constructionDate) {
                    var createdEquipment = this.getEquipment();
                    createdEquipment.BAUJJ = moment(constructionDate).format('YYYY');
                    createdEquipment.BAUMM = moment(constructionDate).format('MM');
                    this.setEquipment(createdEquipment);
                }
            },

            setSaveBetweenFlag: {
                value: function(saveFlag) {
                  this.saveFlag = saveFlag;
                }
            }

        });

        EquipmentCreationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };


        EquipmentCreationViewModel.prototype.onSearch = function (oEvt) {
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);
        };

        EquipmentCreationViewModel.prototype.setNewData = function () {
            var that = this;
            var newSRBEquipment;
            
            if(!this.saveFlag) {
                newSRBEquipment = new Equipment(null, that._requestHelper, that);

                newSRBEquipment.WKCTR = this.getWorkCenterObjectId(this.getUserInformation().workCenterId.trim());
                newSRBEquipment.BEBER = "";
                newSRBEquipment.EQUNR = newSRBEquipment.getRandomUUID(18);
                newSRBEquipment.EQUIPMENT_DISPLAY = newSRBEquipment.EQUNR;
                newSRBEquipment.PERSNR = this.getUserInformation().personnelNumber;
                newSRBEquipment.SWERK = this.getUserInformation().plant.trim();
                newSRBEquipment.APPROVALDATE = newSRBEquipment.getDateNow().dateTime;
                newSRBEquipment.ARCHIVEDATE = newSRBEquipment.getDateNow().dateTime;
                newSRBEquipment.CONSTRUCTIONDATE = moment(newSRBEquipment.getDateNow().dateTime).format("MM.YYYY");
                newSRBEquipment.PLANTLOCATION = "";
                newSRBEquipment.CATEGORY = "";
                newSRBEquipment.CATEGORY_SHTXT = "";
                newSRBEquipment.PLANTLOCATION_SHTXT = "";
                newSRBEquipment.TYP = "";
                newSRBEquipment.EQTYP = "C";
                newSRBEquipment.TYP_SHTXT = "";
                newSRBEquipment.RESULT_CODE = "";
                newSRBEquipment.RESULT_DESCRIPTION = "";
                newSRBEquipment.MLANG = this.getLanguage();
                newSRBEquipment.SPRAS = this.getLanguage();

                this.setProperty("/classificationVisible", false);
                this.setProperty("/templateAvailable", false);
                this.setClassifications([]);

            }else{
                newSRBEquipment = new Equipment(this.getData().equipment, that._requestHelper, that);

                if(newSRBEquipment.TYP == ""){
                    this.setProperty("/classificationVisible", false);
                    this.setProperty("/templateAvailable", false);
                }else{
                    this.setProperty("/classificationVisible", true);
                    this.setProperty("/templateAvailable", true);
                }
            }
            this.setEquipment(newSRBEquipment);

            this.refresh();
        };

        EquipmentCreationViewModel.prototype.getWorkCenterObjectId = function (workCenter) {
            var workCenterMlData = this._component.getModel("customizingModel").oData.WorkCenterMl;
            var currectWKCTR = workCenterMlData.filter(function(wkCtr){
                return wkCtr.ARBPL === workCenter;
            });

            return currectWKCTR[0].OBJID;
        };

        EquipmentCreationViewModel.prototype.getWorkCenterARBPL = function (workCenter) {
            var workCenterMlData = this._component.getModel("customizingModel").oData.WorkCenterMl;
            var currectWKCTR = workCenterMlData.filter(function(wkCtr){
                return wkCtr.OBJID === workCenter;
            });

            return currectWKCTR[0].ARBPL;
        };

        EquipmentCreationViewModel.prototype.fetchData = function () {
            this.setNewData();
        };

        EquipmentCreationViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        EquipmentCreationViewModel.prototype.resetModel = function () {
            this._needsReload = true;
        };

        EquipmentCreationViewModel.prototype.saveEquipment = function(successCb){
            var createdEquipment = this.getEquipment();
            createdEquipment.PPSID = createdEquipment.WKCTR;
            this._insert(successCb);
        };

        EquipmentCreationViewModel.prototype._insert = function (successCb) {
            var createdEquipment = this.getEquipment();
            createdEquipment.PPSID = createdEquipment.WKCTR;
            var dateArr = createdEquipment.CONSTRUCTIONDATE.split(".");
            createdEquipment.BAUMM = dateArr[0];
            createdEquipment.BAUJJ = dateArr[1];
            var that = this;
            var sqlStrings = [];

            that.setBusy(true);

            var onSuccessDocumentsMoved = function () {

                var insert_success = function () {
                    that.setBusy(false);
                    that.refresh();
                    successCb();
                };

                var fetchCounterQueue = [
                    that._updateCharacteristics(that.getClassifications()),
                    that._insertMeasurementDocument(that.getMeasPointForEquipment())
                ];

                Promise.all(fetchCounterQueue).then(insert_success).catch(function (err) {

                });

            };

            var onDocumentsMoved = function () {
                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccessDocumentsMoved, function () {
                    that.executeErrorCallback(new Error(that._component.i18n_core.getText("ERROR_INSERT_NEW_EQU")));
                });
            };
            var onDocumentsMovedError = function () {
                that.executeErrorCallback(new Error(that._component.i18n_core.getText("ERROR_ATTACHMENT_MOVE")));
            };

            try {
                sqlStrings.push(createdEquipment.insert(true));
                if (createdEquipment.getAttachments() != null) {
                    createdEquipment.getAttachments().forEach(function (attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });
                    this._moveAttachmentsFromTempToDoc(createdEquipment.getAttachments()).then(onDocumentsMoved.bind(this)).catch(onDocumentsMovedError.bind(this));
                    return;
                }
                onDocumentsMoved.apply(this);
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EquipmentCreationViewModel.prototype._updateCharacteristics = function (characteristics) {
            var that = this;
            characteristics.forEach(function(characteristic){
                switch (characteristic.ATFOR) {
                    case 'NUM':
                    case 'CHAR':
                        characteristic.VALUE_NEUTRAL_FROM = characteristic.VALUE_NEUTRAL_FROM.replace('.', ',');
                        break;
                    default:
                        break;
                }
            });
            that.updateCharacteristic(characteristics, function(){
            });
        };

        EquipmentCreationViewModel.prototype._insertMeasurementDocument = function (point) {
            var that = this;
            var equipment = this.getEquipment();

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (document) {
                if(equipment.VLCOD === '0202'){
                    that._InsertMeasurementDocLongText(equipment, document);
                }
            };

            if(point && point.length > 0){
                var newMeasurementDocumentData = new MeasurementDocument(null, this._requestHelper, this);
                // TODO check what has to be filled for point
                newMeasurementDocumentData.POINT = equipment.EQUNR.substring(0, 12);
                newMeasurementDocumentData.ERDAT = moment(new Date()).format("YYYY-MM-DD");
                newMeasurementDocumentData.ERUHR = moment(new Date()).format("YYYY-MM-DD HH:mm:ss.SSS");
                newMeasurementDocumentData.CODGR = point[0].CODGR;
                newMeasurementDocumentData.RECDV = equipment.RESULT_CODE;
                newMeasurementDocumentData.MDTXT = equipment.RESULT_DESCRIPTION;
                newMeasurementDocumentData.ERNAM = "" + this.getUserInformation().userName;
                // TODO Not sure if is correct
                newMeasurementDocumentData.CNTRR_CHAR = equipment.EQUNR;
                newMeasurementDocumentData.CVERS = "000001";
                newMeasurementDocumentData.CODCT = equipment.CODCT;
                newMeasurementDocumentData.READG_CHAR = equipment.RESULT_CODE;
                newMeasurementDocumentData.VLCOD = equipment.VLCOD;

                try {
                    newMeasurementDocumentData.insert(false, function (mobileId) {
                        newMeasurementDocumentData.MOBILE_ID = mobileId;
                        onSuccess(newMeasurementDocumentData);
                    }, onError.bind(this, new Error(this._component.i18n.getText("errorInsertMeasDoc"))));
                } catch (err) {
                    this.executeErrorCallback(err);
                }
            }

        };

        EquipmentCreationViewModel.prototype._InsertMeasurementDocLongText = function (equipment, document) {
            var that = this;

            var newMeasurementDocumentLongText = this.getNewMeasurementDocLongText(equipment, document);

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (document) {
            };

            document.addLongtext(false, equipment.RESULT_DESCRIPTION, newMeasurementDocumentLongText.OBJTYPE, onSuccess, onError);

        };

        EquipmentCreationViewModel.prototype.getNewMeasurementDocLongText = function (equipment, document) {
            var newMeasurementDocumentLongText = new MeasdocLongtexts(null, this._requestHelper, this);

            newMeasurementDocumentLongText.MDOCM = document.MDOCM;
            newMeasurementDocumentLongText.FORMAT_COL = "*";
            newMeasurementDocumentLongText.OBJKEY = "00000000";
            newMeasurementDocumentLongText.OBJTYPE = "IMRG";
            newMeasurementDocumentLongText.LINE_NUMER = "1";

            return newMeasurementDocumentLongText;
        };

        EquipmentCreationViewModel.prototype.loadCreatedSRBEquipmentData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.refresh();
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(data);
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("errorLoadEquipmentMeasPoints")));
            };

            this.setBusy(true);

            this._requestHelper.getData("CreatedSRBEquipments", {
            }, load_success, load_error, true);

        };

        EquipmentCreationViewModel.prototype.deleteEquipment = function (equipment, successCb) {
            var that = this;

            var equipmentObject = new Equipment(equipment, that._requestHelper, that);

            var sqlStrings = [];
            var sqlDeleteStrings = that._getDeletionsStrings(equipmentObject.EQUNR);

            if (equipmentObject.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error(that._component.i18n.getText("NOT_ALLOWED_TO_DELETE_SRB")));
                return;
            }

            sqlStrings = sqlStrings.concat(equipmentObject.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(equipmentObject, "/createdSRBEquipments");
                successCb();
            };

            var onAttachmentsDeleted = function() {
                executeUpdates(sqlDeleteStrings, function (success) {
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                        that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_WHILE_PREPARING_SRB_DELETION")));
                    });
                }, that.executeErrorCallback.bind(that, new Error(that._component.i18n.getText("ERROR_WHILE_PREPARING_SRB_DELETION"))));
            };


            equipmentObject.loadAttachments().then(function(attachments) {
                attachments.forEach(function(attachment) {
                    sqlStrings = sqlStrings.concat(attachment.delete(true));
                });

                that._deleteAttachments(attachments)
                    .then(onAttachmentsDeleted)
                    .catch(onAttachmentsDeleted);
            }).catch(onAttachmentsDeleted);

        };

        EquipmentCreationViewModel.prototype._getDeletionsStrings = function (equipment) {

            var sqlStringsDelete = [];

            var deleteMeasdocSql = "DELETE FROM MEASUREMENTDOCUMENT WHERE WOOBJ = '$1'";
            deleteMeasdocSql = deleteMeasdocSql.replace("$1", equipment);

            sqlStringsDelete.push(deleteMeasdocSql);

            return sqlStringsDelete;

        };

        EquipmentCreationViewModel.prototype.displayWorkCtrDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._eWorkCtrDialog) {
                this._eWorkCtrDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.WorkCenterSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._eWorkCtrDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            this._eWorkCtrDialog.display(oEvent, {
                tableName: "WorkCenterMl",
                params: {
                    spras: that.getLanguage(),
                    USER: this.getUserInformation().userName,
                },
                actionName: 'valueHelp'
            });
        };

        EquipmentCreationViewModel.prototype.handleWorkCtrSelect = function (oEvent) {
            this._eWorkCtrDialog.select(oEvent, 'OBJID', [{
                'WKCTR': 'OBJID',
                'PLANTLOCATION_SHTXT': null,
                'CATEGORY_SHTXT': null,
                'TYP_SHTXT': null,
                'RBNR': null,
                'BEBER':null
            }], this, "/equipment");
            this.setProperty("/classificationVisible", false);
            this.setProperty("/templateAvailable", false);
            this.refresh();
        };

        EquipmentCreationViewModel.prototype.handleWorkCtrSearch = function (oEvent) {
            this._eWorkCtrDialog.search(oEvent, ["KTEXT", "ARBPL"]);
        };

        EquipmentCreationViewModel.prototype.displayPlantLocationDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._plantSelectDialog) {
                this._plantSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.PlantLocationSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._plantSelectDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            var currentEquipment = this.getEquipment();
            var workCenter = this.getWorkCenterARBPL(currentEquipment.WKCTR);

            this._plantSelectDialog.display(oEvent, {
                tableName: "FunctionLocation",
                params: {
                    spras: that.getLanguage(),
                    TPLMA: "SRB-" + workCenter,
                },
                actionName: 'valueHelp'
            });
            this.refresh();
        };

        EquipmentCreationViewModel.prototype.handlePlantLocationSelect = function (oEvent) {
            this._plantSelectDialog.select(oEvent, 'TPLNR', [{
                'TPLNR': 'TPLNR',
                'PLANTLOCATION': 'TPLNR',
                'FLTYP': 'FLTYP',
                'FUNCLOC_DISP': 'TPLNR',
                'INGRP': 'INGRP',
                'FUNCLOC_DISPLAY': 'TPLNR',
                'TPLNR_SHTXT': 'TPLNR_SHTXT',
                'BEBER': 'BEBER',
                "SWERK": 'SWERK',
                'PLANTLOCATION_SHTXT': 'TPLNR_SHTXT',
                'CATEGORY_SHTXT': '',
                'TYP_SHTXT': '',
                'RBNR': null,
            }], this, "/equipment");
            this.setProperty("/classificationVisible", false);
            this.setProperty("/templateAvailable", false);
            this.refresh();
        };

        EquipmentCreationViewModel.prototype.handlePlantLocationSearch = function (oEvent) {
            this._plantSelectDialog.search(oEvent, ["TPLNR", "BEBER", "TPLNR_SHTXT"]);
        };

        EquipmentCreationViewModel.prototype.displayCategoryDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._categorySelectDialog) {
                this._categorySelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CategorySelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._categorySelectDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            var currentEquipment = this.getEquipment();

            this._categorySelectDialog.display(oEvent, {
                tableName: "FunctionLocationCategory",
                params: {
                    spras: that.getLanguage(),
                    TPLMA: currentEquipment.PLANTLOCATION,
                },
                actionName: 'valueHelp'
            });
        };

        EquipmentCreationViewModel.prototype.handleCategorySelect = function (oEvent) {
            this._categorySelectDialog.select(oEvent, 'TPLNR', [{
                'TPLNR': 'TPLNR',
                'TPLMA': 'TPLNR',
                'CATEGORY': 'TPLNR',
                'FLTYP': 'FLTYP',
                'INGRP': 'INGRP',
                'FUNCLOC_DISP': 'TPLNR',
                'FUNCLOC_DISPLAY': 'TPLNR',
                'TPLMA_SHTXT': 'TPLNR_SHTXT',
                'CATEGORY_SHTXT': 'TPLNR_SHTXT',
                'TYP_SHTXT': '',
                "RBNR": "RBNR"
            }], this, "/equipment");
            this.setProperty("/classificationVisible", false);
            this.setProperty("/templateAvailable", false);
            this.refresh();
        };

        EquipmentCreationViewModel.prototype.handleCategorySearch = function (oEvent) {
            this._categorySelectDialog.search(oEvent, ["TPLNR", "RBNR", "TPLNR_SHTXT"]);
        };

        EquipmentCreationViewModel.prototype.displayTypSearch = function (oEvent, viewContext) {
            var that = this;
            if (!this._typSelectDialog) {
                this._typSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.EquipmentTypSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._typSelectDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");
            var currentEquipment = this.getEquipment();

            this._typSelectDialog.display(oEvent, {
                tableName: "FunctionLocationTyp",
                params: {
                    spras: that.getLanguage(),
                    TPLMA: currentEquipment.CATEGORY,
                },
                actionName: 'valueHelp'
            });
        };

        EquipmentCreationViewModel.prototype.handleTypSelect = function (oEvent) {
            this._typSelectDialog.select(oEvent, 'TPLNR', [{
                'TPLNR': 'TPLNR',
                'TYP': 'TPLNR',
                'FLTYP': 'FLTYP',
                'INGRP': 'INGRP',
                'FUNCLOC_DISP': 'TPLNR',
                'FUNCLOC_DISPLAY': 'TPLNR',
                'TPLNR_SHTXT': 'TPLNR_SHTXT',
                'TYP_SHTXT': 'TPLNR_SHTXT',
                "EQART": "EQART",
                'SHTXT': "TPLNR_SHTXT",
            }], this, "/equipment");
            this.refresh();
            this.getEquipmentTemplatesForFuncloc(this.getEquipment().TPLNR);
        };

        EquipmentCreationViewModel.prototype.handleTypSearch = function (oEvent) {
            this._typSelectDialog.search(oEvent, ["TPLNR", "EQART", "TPLNR_SHTXT"]);
        };

        EquipmentCreationViewModel.prototype.displayCodingDialog = function (oEvent, viewContext) {

            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodingDialog.dialog.setModel(this._component.getModel("i18n_core"), "i18n_core");
            this._oCodingDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: "SRB-GEN",
                    catalogType: "T"
                },
                actionName: "valueHelp"
            });
        };

        EquipmentCreationViewModel.prototype.handleCodingSelect = function (oEvent) {
            this._oCodingDialog.select(oEvent, 'CODE', [{
                'CODCT': 'CATALOG_TYPE',
                'RESULT_CODE': "CODE_TEXT",
                'VLCOD': 'CODE'
            }], this, "/equipment");

            this.valueStateModel.getData().RESULT_CODE = "None";
            this.valueStateModel.refresh();
        };

        EquipmentCreationViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oCodingDialog.search(oEvent, ["CODING", "CODE_TEXT"]);
        };

        EquipmentCreationViewModel.prototype.handleMaskValueChanged = function(oEvent) {
            var value = oEvent.getParameter("newValue");

            var characteristic = oEvent.getSource().getBindingContext("equipmentCreationViewModel").getObject();
            var sPath = oEvent.getSource().getBindingContext("equipmentCreationViewModel").getPath();

            if (characteristic.ATFOR === 'NUM') {
                characteristic.VALUE_NEUTRAL_FROM = value.replace(/_/g, "");
                characteristic.VALUE_NEUTRAL_FROM = characteristic.VALUE_NEUTRAL_FROM.replace(",", '.');
                if (!isNaN(characteristic.ANZDZ) && !isNaN(characteristic.VALUE_NEUTRAL_FROM)) {
                    characteristic.VALUE_NEUTRAL_FROM = parseFloat(characteristic.VALUE_NEUTRAL_FROM).toFixed(parseInt(characteristic.ANZDZ));
                }
            }

            var valueObject = this.validateCharacteristicValue(characteristic);

            this.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE);
            this.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT);

            this.refresh(true);
        };

        EquipmentCreationViewModel.prototype.validate = function () {
            var modelData = this.getEquipment();
            var validator = new Validator(modelData, this.getValueStateModel());

            this.getProperty("/classifications").forEach(function(characteristic){
                if(characteristic.ATWRT == '' && characteristic.ATFLV !== '' && characteristic.ATFLB !== '' && characteristic.ATWTB){
                    characteristic.VALUE_NEUTRAL_FROM = characteristic.ATWTB;
                }
            });
            // TODO do correct validation on new activity
            validator.check("WKCTR", this._component.i18n.getText("WKCTR_EMPTY")).isEmpty();
            validator.check("PLANTLOCATION_SHTXT", this._component.i18n.getText("PLANTLOCATION_EMPTY")).isEmpty();
            validator.check("CATEGORY_SHTXT", this._component.i18n.getText("CATEGORY_EMPTY")).isEmpty();
            validator.check("CATEGORY_SHTXT", this._component.i18n.getText("CATEGORY_EMPTY")).custom(function (value) {
                if(value){
                    return true;
                } else {
                    return false;
                }
            });
            validator.check("TYP_SHTXT", this._component.i18n.getText("TYP_EMPTY")).isEmpty();
            validator.check("TYP_SHTXT", this._component.i18n.getText("TYP_EMPTY")).custom(function (value) {
                    if(value){
                        return true;
                    } else {
                        return false;
                    }
            });
            validator.check("HERST", this._component.i18n.getText("HERST_EMPTY")).isEmpty();
            validator.check("SERGE", this._component.i18n.getText("SERGE_EMPTY")).isEmpty();
            validator.check("CONSTRUCTIONDATE", this._component.i18n.getText("CONSTRUCTIONDATE_EMPTY")).isEmpty();

            var errroClassfication = false;
            this.getProperty("/classifications").forEach(function(characteristic) {
                characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE = "None";
                characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT = "";
                
                if(characteristic.ATERF == "X" 
                && !((characteristic.VALUE_NEUTRAL_FROM == "" && characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL != "") 
                    || (characteristic.VALUE_NEUTRAL_FROM != "" && characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL == "")
                    || (characteristic.VALUE_NEUTRAL_FROM != "" && characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL != ""))){

                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE = "Error";
                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT = this._component.i18n.getText("SRB_CHARACTERISTIC_EMPTY");
                        errroClassfication = true;
                    }
            }.bind(this));

            this.getProperty("/classifications").filter(function(characteristic) {
                characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE = "None";
                characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT = "";
                if(characteristic.ATERF == "X" && characteristic.ATSON == "X" && characteristic.ATINN !== '' && characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL == ""){
                    characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE = "Error";
                    characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT = this._component.i18n.getText("SRB_CHARACTERISTIC_EMPTY");
                    errroClassfication = true;
                }
            }.bind(this));


            this.getProperty("/classifications").filter(function(characteristic) {
                
                if(characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE != "Error"){
                    if(characteristic.ATERF == "X"  && characteristic.IS_DATE){
                        var valueObject = this.validateCharacteristicDateValue(characteristic);

                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE = valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE;
                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT = valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT;

                        if(characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE == "Error"){
                            errroClassfication = true;
                        }
                    }else{
                        var valueObject = this.validateCharacteristicValue(characteristic);

                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE = valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE;
                        characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT = valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT;

                        if(characteristic.VALUE_NEUTRAL_FROM_VALUE_STATE == "Error"){
                            errroClassfication = true;
                        }
                    }
                
                }

                if(characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE != "Error"){
                    if(characteristic.ATERF == "X" && characteristic.ATSON == "X"  && characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL != ""){
                        var valueObject = this.validateCharacteristicAddtionalValue(characteristic);

                        characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE = valueObject.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE;
                        characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT = valueObject.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT;

                        if(characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE == "Error"){
                            errroClassfication = true;
                        }
                    }
                }
            }.bind(this));
        
            if(this.getMeasPointForEquipment().length > 0){
                validator.check("RESULT_CODE", this._component.i18n.getText("SRB_MEAS_DOC_EMPTY")).isEmpty();

                if(this.getEquipment().VLCOD === '0202'){
                    validator.check("RESULT_DESCRIPTION", this._component.i18n.getText("SRB_MEAS_DOC_TEXT_EMPTY")).isEmpty();
                }
            }
            var errors = validator.checkErrors();
            
            if(errors && errors.length > 0){
                errors.forEach(function(error){
                    this.valueStateModel.getData()[error.property+"_MESSAGE"] = error.errorMessage;
                }.bind(this));
            }

            this.valueStateModel.refresh();
            this.refresh();

            return errors || errroClassfication ? false : true;
        };

        EquipmentCreationViewModel.prototype.setCharacteristicFieldsForType = function (equipmentNr) {
            var that = this;
            if(this.getEquipment().CATEGORY_SHTXT) {
                var load_success = function (data) {
                    var equipmentCharacs = data[0].EquipmentCharacs;
                    that.excludedCharact = [];
                    that.setCharacteristicFieldsForView(equipmentCharacs);
                    that.setBusy(false);
                };

                var fetchCounterQueue = [
                    this._fetchEquipmentCharacs(equipmentNr)
                ];

                that.setBusy(true);

                Promise.all(fetchCounterQueue).then(load_success).catch(function (err) {

                });
            }
            };

        EquipmentCreationViewModel.prototype._fetchEquipmentCharacs = function (equipmentNr) {
            var that = this;
            return this._requestHelper.fetch({
                id: "EquipmentCharacs",
                statement: "SELECT ec.mobile_id, ec.CHARACT, ec.CLASS, cml.ATBEZ as CHARACT_DESCR, ec.EQUNR, ec.VALUE_NEUTRAL_FROM, ec.VALUE_NEUTRAL_TO, " +
                    "ec.VALUE_TO, ec.VALUE_FROM, ec.UNIT_FROM, ec.UNIT_TO," +
                    "ec.VALUE_DATE, ec.TYPE, ec.ACTION_FLAG, cv.ATINN, c.ATFOR, c.ATEIN, c.ATSON, c.ANZDZ, c.ANZST, c.ATSCH, u.MSEHT, cc.POSNR, " +
                    "COUNT(ec.MOBILE_ID) as cv_count, c.ATERF, cv.ATWRT, cv.ATFLV, cv.ATFLB, cv.ATZHL, cvml.ATWTB as ATWTB_DISPLAY FROM EquipmentCharacs AS ec LEFT JOIN Characteristics AS c ON ec.CHARACT = c.ATNAM " +
                    "LEFT JOIN CharacteristicsMl as cml on cml.ATINN = c.ATINN " +
                    "LEFT JOIN MeasurementUnit AS u ON u.MSEH3 = c.MSEHI AND u.SPRAS = '@language' " +
                    "LEFT JOIN Classification as cl ON cl.CLASS=ec.CLASS " +
                    "LEFT JOIN ClassificationChar AS cc ON c.ATINN = cc.IMERK AND cc.CLINT=cl.CLINT " +
                    "LEFT JOIN CharacteristicValues as cv on cv.ATINN = c.ATINN " +
                    "LEFT JOIN CharacteristicValMl as cvml ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cvml.SPRAS='@language' "+
                    "WHERE ec.EQUNR='@equipment' AND ec.ACTION_FLAG!='D' AND ec.ACTION_FLAG!='M' and cml.ATBEZ is not null and ec.ATZHL is not null " +
                    "GROUP BY ec.MOBILE_ID ,ec.CHARACT,ec.CLASS, cml.ATBEZ, ec.EQUNR, ec.VALUE_NEUTRAL_FROM, ec.VALUE_NEUTRAL_TO, " +
                    "ec.VALUE_TO, ec.VALUE_FROM, ec.UNIT_FROM, ec.UNIT_TO,ec.VALUE_DATE, ec.TYPE, ec.ACTION_FLAG, cv.ATINN, c.ATFOR, c.ATEIN, c.ATSON, c.ANZDZ, " +
                    "c.ANZST, c.ATSCH, u.MSEHT, cc.POSNR, c.ATERF, cv.ATWRT, cv.ATFLV, cv.ATFLB, cv.ATZHL, cvml.ATWTB ORDER BY cc.POSNR"
            }, {
                equipment: equipmentNr,
                language: that.getLanguage()
            });
        };

        EquipmentCreationViewModel.prototype.getEquipmentTemplatesForFuncloc = function (funcLoc) {
            var that = this;
            that.setBusy(true);
            if(this.getEquipment().CATEGORY_SHTXT) {
                var load_success = function (data) {
                    var equipmentTemplates = data[0].EquipmentTemplates;
                    var filteredEquipment = equipmentTemplates.filter(function(template){
                        return template.EQART === funcLoc.slice(12,22) && template.TPLNR.startsWith("EV-SRB");
                    });
                    if(filteredEquipment.length > 0){
                        that.getEquipment().SHTXT = filteredEquipment[0].SHTXT;
                        that.getEquipment().EQART = filteredEquipment[0].EQART;
                        that.setCharacteristicFieldsForType(filteredEquipment[0].EQUNR);
                        that.getMeasurementPointForEquipment(filteredEquipment[0].EQUNR).then(function(measPoint){
                            that.setMeasPointForEquipment(measPoint.MeasurementPointForEquipment);
                            that.setProperty("/templateAvailable", measPoint.MeasurementPointForEquipment.length > 0 ? true : false);      
                            that.setBusy(false);
                        }).catch(function(err){

                        });
                    }else{
                        MessageBox.error(that._component.i18n.getText("TEMPLATE_EQUIPMENT_NOT_FOUND"));
                        that.setProperty("/templateAvailable", false);
                        that.setBusy(false);      
                    }
                    that.resetValueStateModel();
                };

                var fetchCounterQueue = [
                    this._fetchEquipmentTemplatesForFuncloc(funcLoc.slice(11,14))
                ];

                that.setBusy(true);
                Promise.all(fetchCounterQueue).then(load_success).catch(function (err) {

                });
            }
        };

        EquipmentCreationViewModel.prototype.getMeasurementPointForEquipment = function (equipment) {
            var that = this;
            return this._requestHelper.fetch({
                id: "MeasurementPointForEquipment",
                statement: "SELECT * FROM EquipmentMeasPoint WHERE CODGR = 'SRB-GEN' AND EQUNR = '@equipment'"
            }, {
                equipment: equipment,
                language: that.getLanguage()
            });
        };

        EquipmentCreationViewModel.prototype._fetchEquipmentTemplatesForFuncloc = function (template) {
            var that = this;
            return this._requestHelper.fetch({
                id: "EquipmentTemplates",
                statement: "SELECT * FROM EQUIPMENT WHERE FUNCLOC_DISP LIKE '%@template%' and ACTION_FLAG != 'N' "
            }, {
                template: template,
                language: that.getLanguage()
            });
        };

        EquipmentCreationViewModel.prototype.setCharacteristicFieldsForView = function (characteristics) {
            var that = this;
            var newCharacteristics = [];
           
            var filteredCharacteristics = characteristics.filter(function(charac){
                charac.ATZHL = "0000";
                if(charac.ATFOR === "DATE"){
                    charac.VALUE_NEUTRAL_FROM = moment(charac.VALUE_NEUTRAL_FROM).format("DD.MM.YYYY");
                }
                var flag = true;
                if (charac.CHARACT.includes("SRB_") && charac.CHARACT.includes("J_PRUEFZYKLUS")) {
                    that.excludedCharact.push(charac);
                    flag = false;
                }
                return flag && charac.CHARACT !== "RFID_TAG_ID" && charac.CHARACT !== "RFID_TAG_LOCKED";
            });
            
            filteredCharacteristics.forEach(function(charac){
                var index = newCharacteristics.findIndex(o => o.CHARACT === charac.CHARACT);

                if(index == "-1"){
                    charac.characteristicCount = 1;
                    newCharacteristics.push(charac);
                }else{
                    newCharacteristics[index].characteristicCount = newCharacteristics[index].characteristicCount + 1;
                }
            });

            newCharacteristics.forEach(function(characteristic){
                if(characteristic.CHARACT === 'PSA_MA'){
                    characteristic.ATINN ='';
                }
                characteristic.EQUNR = that.getEquipment().EQUNR;

                characteristic.VALUE_NEUTRAL_FROM = characteristic.VALUE_NEUTRAL_FROM.trim();
                var mask = characteristic.ATSCH.replaceAll('-','');

                if (characteristic.ATFOR === 'NUM') {
                    characteristic.maskValue = mask.replace(new RegExp('_', 'g'), '9');
                } else {
                    characteristic.maskValue = mask.replace(new RegExp('_', 'g'), '9');
                }
                characteristic.showValueHelp = characteristic.ATINN !== '';

                if(characteristic.ATWRT == '' && characteristic.ATFLV !== '' && characteristic.ATFLB !== '' && parseInt(characteristic.ATFLB) !== 0){
                    characteristic.showValueHelp = false;
                }

                characteristic.LABEL_CHARACT_DESCR = characteristic.MSEHT !== '' ? characteristic.CHARACT_DESCR + " (" + characteristic.MSEHT + ")" : characteristic.CHARACT_DESCR;

                if(characteristic.ATZHL !== '' && characteristic.ATWRT !== '' && characteristic.ATINN !== ''){
                    characteristic.changeValueHelp = true;
                    if(characteristic.characteristicCount == 1){
                        characteristic.VALUE_NEUTRAL_FROM = characteristic.ATWRT;
                        characteristic.ATWTB = characteristic.ATWTB_DISPLAY;
                        characteristic.changeValueHelpEnabled = false;
                        characteristic.showValueHelp = false;
                    }else{
                        characteristic.changeValueHelpEnabled = true;
                        characteristic.showValueHelp = true;
                    }
                }else{
                    characteristic.changeValueHelp = false;
                }
            });

            if(newCharacteristics.length > 0){
                this.setProperty("/classificationVisible", true);
                this.setClassifications(newCharacteristics);
            }

            that.refresh();

        };

        EquipmentCreationViewModel.prototype.validateCharacteristicAddtionalValue = function (characteristic) {
            var valueObject = {
                VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE: "None",
                VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT: ""
            }

            var reading = characteristic.VALUE_NEUTRAL_FROM_ADDITIONAL;
            var readingLength = reading.length;
            if (reading.includes('.')) {
                readingLength = readingLength - 1;
            }

            if (readingLength > parseInt(characteristic.ANZST)) { 
                valueObject = {
                    VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE: "Error",
                    VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_CHARACTERISTIC_LENGTH", [characteristic.CHARACT_DESCR, characteristic.ANZST])
                }
            }

            return valueObject;
        };

        EquipmentCreationViewModel.prototype.validateCharacteristicValue = function (characteristic) {
            var valueObject = {
                VALUE_NEUTRAL_FROM_VALUE_STATE: "None",
                VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: ""
            }

            var validatorFlag = false;
            if(!characteristic.showValueHelp && characteristic.ATWRT == '' && characteristic.ATFLV !== '' && characteristic.ATFLB !== ''){

                var value = parseFloat(characteristic.VALUE_NEUTRAL_FROM);
                var minAcceptedValue = parseFloat(characteristic.ATFLV).toFixed(characteristic.ANZDZ);
                var maxAcceptedValue = parseFloat(characteristic.ATFLB).toFixed(characteristic.ANZDZ);
                if (maxAcceptedValue != 0) {
                
                    if (value > maxAcceptedValue || value < minAcceptedValue) {
                        validatorFlag = true;
                        valueObject = {
                            VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                            VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_CHARACTERISTIC_RANGE", [characteristic.CHARACT_DESCR, parseFloat(characteristic.ATFLB).toFixed(characteristic.ANZDZ)])
                        }
                    }
                }   
            }
            
            var reading = characteristic.VALUE_NEUTRAL_FROM;
            var readingLength = reading.length;

            if(!validatorFlag){
                if(characteristic.ATFOR === 'NUM'){

                    var totalLen = parseInt(characteristic.ANZST)
                    if(characteristic.ANZDZ ){
                        totalLen = totalLen - parseInt(characteristic.ANZDZ);
                        readingLength = reading.split('.')[0].length;
                    }

                    if (readingLength > totalLen) {
                        validatorFlag = true;
                        valueObject = {
                            VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                            VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_CHARACTERISTIC_LENGTH", [characteristic.CHARACT_DESCR, totalLen])
                    };
                }
                }else{

                    if(readingLength > (parseInt(characteristic.ANZST))){
                        validatorFlag = true;
                        valueObject = {
                            VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                            VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_CHARACTERISTIC_LENGTH", [characteristic.CHARACT_DESCR, (parseInt(characteristic.ANZST))])
                        }
                    }
                }
            }


            if(!validatorFlag){

                if (characteristic.ATFOR === 'NUM' && isNaN(reading)) {
                    valueObject = {
                        VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                        VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_READING_IS_NO_VALID_NUMBER")
                    }
                }else{
                    if (reading.includes('.')) {
                        var splitReading = reading.split(".");
                        if (splitReading[1].length > parseInt(characteristic.ANZDZ)) {
                            valueObject = {
                                VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                                VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("SRB_READING_DECIMAL_NO_VALID_NUMBER", [characteristic.ANZDZ])
                            };
                            //Upto 3 decimals are allowed
                        }
                    }
                }
            }

            return valueObject;
        };

        EquipmentCreationViewModel.prototype.validateCharacteristicDateValue = function (characteristic) {
            var valueObject = {
                VALUE_NEUTRAL_FROM_VALUE_STATE: "None",
                VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: ""
            }
           
            var reading = characteristic.VALUE_NEUTRAL_FROM;
            if (moment(reading).unix() < moment(new Date()).unix()) {
                valueObject = {
                    VALUE_NEUTRAL_FROM_VALUE_STATE: "Error",
                    VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT: this._component.i18n.getText("DATE_IN_PAST")
                }
            }

            return valueObject;
        };

        EquipmentCreationViewModel.prototype.updateCharacteristic = function (characObject, acceptCb) {
            var that = this;
            var sqlStrings = [];
            
            var onSuccess = function () {

                acceptCb();
                this.refresh();
            };

            try {

                var additinalChar = characObject.filter(function(characOb){
                if(characOb.VALUE_NEUTRAL_FROM != '' && characOb.VALUE_NEUTRAL_FROM_ADDITIONAL != ''){
                        return characOb;
                    }
                });           

                characObject.forEach(function(characOb){
                    characOb.VALUE_NEUTRAL_FROM = characOb.VALUE_NEUTRAL_FROM != '' ? characOb.VALUE_NEUTRAL_FROM : characOb.VALUE_NEUTRAL_FROM_ADDITIONAL;

                    if(characOb.IS_DATE){
                        characOb.VALUE_NEUTRAL_FROM = characOb.VALUE_NEUTRAL_FROM.replaceAll('-', '');
                    }

                    if(characOb.ATFOR == "NUM"){
                        var newValue;
                        if(characOb.ATSCH && characOb.ATSCH.includes(",")){
                            newValue = characOb.VALUE_NEUTRAL_FROM.replace('.', ',');
                        }else{
                            newValue = characOb.VALUE_NEUTRAL_FROM.replace(',', '.');
                        }
                        characOb.VALUE_NEUTRAL_FROM = newValue;
                    }
                    sqlStrings.push(characOb.insert(true));
                });
                
                this.excludedCharact.forEach(function(characOb){
                    characOb.ACTION_FLAG = "N";
                    characOb.EQUNR = that.getEquipment().EQUNR;
                    var characteristic = new EquipmentCharacteristic(characOb, that._requestHelper, that);
                    sqlStrings.push(characteristic.insert(true));
                });     
                
                additinalChar.forEach(function(characOb){
                    characOb.VALUE_NEUTRAL_FROM = characOb.VALUE_NEUTRAL_FROM_ADDITIONAL;
                    characOb.ATZHL = "0001"; 
                    sqlStrings.push(characOb.insert(true));
                });
                
                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess.bind(this), function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CHARACTERISTIC")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }

        };

        EquipmentCreationViewModel.prototype.validateSelectedCharacteristicValue = function (characteristic) {
            var validator = new Validator(characteristic, this.getValueStateModel());

            validator.check("VALUE_NEUTRAL_FROM", this._component.i18n.getText("readingEmpty")).isEmpty();
            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EquipmentCreationViewModel.prototype.displayCharacValueSearch = function (oEvent, viewContext) {
            this.selectedCharacteristicBC = oEvent.getSource().getBindingContext('equipmentCreationViewModel');
            var selectedCharacteristic = this.selectedCharacteristicBC.getObject();
            if (!this._oCharacteristicValueDialog) {
                this._oCharacteristicValueDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CharacteristicValueSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCharacteristicValueDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n");

            this._oCharacteristicValueDialog.display(oEvent, {
                tableName: 'CharacteristicValues',
                params: {
                    atinn: selectedCharacteristic.ATINN,
                    language: this.getLanguage()
                }
            });
        };

        EquipmentCreationViewModel.prototype.handleCharacValueSearch = function (oEvent) {
            this._oCharacteristicValueDialog.search(oEvent, ['ATWRT', 'ATWTB']);
        };

        EquipmentCreationViewModel.prototype.handleCharacValueSelect = function (oEvent) {
            var sPath = this.selectedCharacteristicBC.getPath();
            var selectedCharacteristic = this.selectedCharacteristicBC.getObject();
            var selectedObject = oEvent.getParameters().selectedItem.getBindingContext().getObject();

            if(selectedObject.ATWRT) {
                this._oCharacteristicValueDialog.select(oEvent, 'ATWRT', [{
                    'VALUE_NEUTRAL_FROM': 'ATWRT',
                    'ATWTB': 'ATWTB'
                },], this, sPath);
            } else if(parseInt(selectedObject.ATFLB) > 0){
                this._oCharacteristicValueDialog.select(oEvent, 'ATFLB', [{
                    'VALUE_NEUTRAL_FROM': 'ATFLB',
                    'ATWTB': 'ATFLB'
                },], this, sPath);
            } else if(selectedObject.ATFLV){
                var number = selectedObject.ATFLV;
                var placeholder = selectedObject.ATSCH;
                if(number){
                    var decimal = 0;
                    if(placeholder && placeholder.includes(".")){
                        decimal = (placeholder.length - 1) - (placeholder.indexOf("."));
                    } else if(placeholder && placeholder.includes(",")){
                        decimal = (placeholder.length - 1) - (placeholder.indexOf(","));
                    }              
                    var newNumber = parseFloat(number).toFixed(decimal);
                    selectedObject.ATFLV = newNumber;
                 }
                this._oCharacteristicValueDialog.select(oEvent, 'ATFLV', [{
                    'VALUE_NEUTRAL_FROM': 'ATFLV',
                    'ATWTB': 'ATFLV'
                },], this, sPath);
            }

            selectedCharacteristic.VALUE_NEUTRAL_FROM = selectedCharacteristic.VALUE_NEUTRAL_FROM.toString();
            selectedCharacteristic.VALUE_NEUTRAL_FROM_VALUE_STATE = "None";
            selectedCharacteristic.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT = "";
            this.refresh();

        };

        EquipmentCreationViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    WKCTR: sap.ui.core.ValueState.None,
                    WKCTR_MESSAGE: "",
                    PLANTLOCATION_SHTXT: sap.ui.core.ValueState.None,
                    PLANTLOCATION_SHTXT_MESSAGE: "",
                    CATEGORY_SHTXT: sap.ui.core.ValueState.None,
                    CATEGORY_SHTXT_MESSAGE: "",
                    TYP_SHTXT: sap.ui.core.ValueState.None,
                    TYP_SHTXT_MESSAGE: "",
                    CONSTRUCTIONDATE: sap.ui.core.ValueState.None,
                    CONSTRUCTIONDATE_MESSAGE: "",
                    HERST: sap.ui.core.ValueState.None,
                    HERST_MESSAGE: "",
                    SERGE: sap.ui.core.ValueState.None,
                    SERGE_MESSAGE: "",
                    RESULT_CODE: sap.ui.core.ValueState.None,
                    RESULT_CODE_MESSAGE: "",
                    RESULT_DESCRIPTION: sap.ui.core.ValueState.None,
                    RESULT_DESCRIPTION_MESSAGE: "",
                });
            }
            return this.valueStateModel;
        };

        EquipmentCreationViewModel.prototype.resetValueStateModel = function () {

            this.valueStateModel.setProperty("/WKCTR", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/PLANTLOCATION_SHTXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CATEGORY_SHTXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/TYP_SHTXT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/CONSTRUCTIONDATE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/HERST", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/SERGE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/RESULT_CODE", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/RESULT_DESCRIPTION", sap.ui.core.ValueState.None);

            this.valueStateModel.setProperty("/WKCTR_MESSAGE", "");
            this.valueStateModel.setProperty("/PLANTLOCATION_SHTXT_MESSAGE", "");
            this.valueStateModel.setProperty("/CATEGORY_SHTXT_MESSAGE", "");
            this.valueStateModel.setProperty("/TYP_SHTXT_MESSAGE", "");
            this.valueStateModel.setProperty("/CONSTRUCTIONDATE_MESSAGE", "");
            this.valueStateModel.setProperty("/HERST_MESSAGE", "");
            this.valueStateModel.setProperty("/SERGE_MESSAGE", "");
            this.valueStateModel.setProperty("/RESULT_CODE_MESSAGE", "");
            this.valueStateModel.setProperty("/RESULT_DESCRIPTION_MESSAGE", "");
        };


        EquipmentCreationViewModel.prototype.constructor = EquipmentCreationViewModel;

        return EquipmentCreationViewModel;
    });
