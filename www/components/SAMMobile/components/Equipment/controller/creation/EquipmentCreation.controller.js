sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "SAMMobile/components/Equipment/controller/creation/EquipmentCreationViewModel",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/Button",
    "sap/m/Input",
    "sap/m/Select",
    "sap/m/Label",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/helpers/formatter",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/components/Equipment/helpers/formatterComp",
    "SAMMobile/controller/common/MediaAttachments.controller"
], function(Controller, JSONModel, EquipmentCreationViewModel, Dialog, Text, Button, Input, Select, Label, MessageBox, MessageToast, formatter, FileSystem, fileHelper,formatterComp,  MediaAttachmentsController) {
    "use strict";

    return Controller.extend("SAMMobile.components.Equipment.controller.list.List", {
        fileHelper: fileHelper,
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("equipmentCreationRoute").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("equipmentCreationRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
            this.fileSystem = new FileSystem();

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.equipmentCreationViewModel = new EquipmentCreationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.equipmentCreationViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.initialized = false;
            this.getView().setModel(this.equipmentCreationViewModel, "equipmentCreationViewModel");
            this.getView().setModel(this.equipmentCreationViewModel.getValueStateModel(), "valueStateModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;

            var onMasterDataLoaded = function(routeName) {
                this.globalViewModel.setCurrentRoute(routeName);

                if (that.equipmentCreationViewModel.needsReload()) {
                    that.loadObjects();
                }

                if (!that.initialized) {
                    that.initialized = true;
                }
            };

            this.globalViewModel.setComponentHeaderText(this.equipmentCreationViewModel.getHeaderText());
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }

            this._tryCreateUserAttachmentsControl();
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            this.globalViewModel.setComponentHeaderText(this.equipmentCreationViewModel.getHeaderText());

            if (bSync) {
                this.equipmentCreationViewModel.refreshData();
            } else {
                this.equipmentCreationViewModel.fetchData();
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {

        },

        onLogout: function() {
            this.initialized = false;
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("equipmentCreation", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onEquipmentSavePress: function(oEvent){
            var that = this;
            if(this.equipmentCreationViewModel.validate()){
                this.equipmentCreationViewModel.saveEquipment(function(equipment){
                    that.globalViewModel.model.setProperty("/editMode", false);
                    sap.ui.getCore().getEventBus().publish("equipments", "equipmentSaved");
                    that.equipmentCreationViewModel.setSaveBetweenFlag(false);
                    that.equipmentCreationViewModel.setNewData();
                    that.equipmentCreationViewModel.setProperty("/view/errorMessages", []);
                    that.equipmentCreationViewModel.refresh();
                    MessageToast.show(that.component.i18n.getText("EQUIPMENT_INSERT_SUCCESSFULLY"));
                });
            }

        },

        onEquipmentClearPress: function(oEvent){
            var that = this;
            //this.equipmentCreationViewModel.setBusy(true);
            that.equipmentCreationViewModel.setSaveBetweenFlag(false);
            that.equipmentCreationViewModel.setClassifications([]);
            that.equipmentCreationViewModel.setNewData();
            that.equipmentCreationViewModel.setBusy(false);
            that.equipmentCreationViewModel.setProperty("/view/errorMessages", []);
            this.equipmentCreationViewModel.refresh();
        },

        onEquipmentSaveBetweenPress:function(oEvent){
            var that = this;
            this.equipmentCreationViewModel.setSaveBetweenFlag(true);
            this.equipmentCreationViewModel.setProperty("/view/errorMessages", []);
            this.equipmentCreationViewModel.refresh();
            MessageToast.show(that.component.i18n.getText("SRB_SAVED_LOCALLY"));
        },

        onDateChanged: function(oEvent){
            this.equipmentCreationViewModel.setConstructionDate(oEvent.getParameter("value"));
        },

        displayWorkCtrSearch: function (oEvent) {
            this.equipmentCreationViewModel.displayWorkCtrDialog(oEvent, this);
        },

        handleWorkCtrSelect: function (oEvent) {
            this.equipmentCreationViewModel.handleWorkCtrSelect(oEvent);
            this.equipmentCreationViewModel.resetValueStateModel();
        },

        handleWorkCtrSearch: function (oEvent) {
            this.equipmentCreationViewModel.handleWorkCtrSearch(oEvent);
        },

        displayPlantLocationSearch: function (oEvent) {
            this.equipmentCreationViewModel.displayPlantLocationDialog(oEvent, this);
        },

        handlePlantLocationSelect: function (oEvent) {
            this.equipmentCreationViewModel.handlePlantLocationSelect(oEvent);
        },

        handlePlantLocationSearch: function (oEvent) {
            this.equipmentCreationViewModel.handlePlantLocationSearch(oEvent);
            this.equipmentCreationViewModel.resetValueStateModel();
        },

        displayCategorySearch: function (oEvent) {
            this.equipmentCreationViewModel.displayCategoryDialog(oEvent, this);
        },

        handleCategorySelect: function (oEvent) {
            this.equipmentCreationViewModel.handleCategorySelect(oEvent);
            this.equipmentCreationViewModel.resetValueStateModel();

        },

        handleCategorySearch: function (oEvent) {
            this.equipmentCreationViewModel.handleCategorySearch(oEvent);
        },

        displayTypSearch: function (oEvent) {
            this.equipmentCreationViewModel.displayTypSearch(oEvent, this);
        },

        handleTypSelect: function (oEvent) {
            this.equipmentCreationViewModel.handleTypSelect(oEvent);
        },

        handleTypSearch: function (oEvent) {
            this.equipmentCreationViewModel.handleTypSearch(oEvent);
        },

        displayCodingDialog: function (oEvent) {
            this.equipmentCreationViewModel.displayCodingDialog(oEvent, this);
        },

        handleCodingSelect: function (oEvent) {
            this.equipmentCreationViewModel.handleCodingSelect(oEvent);
        },

        handleCodingSearch: function (oEvent) {
            this.equipmentCreationViewModel.handleCodingSearch(oEvent);
        },

        onMaskValueChanged: function(oEvent) {
            this.equipmentCreationViewModel.handleMaskValueChanged(oEvent);
        },

        handleCharacAdditionalValueChange: function (oEvent) {
            var selectedCharacteristicBC = oEvent.getSource().getBindingContext('equipmentCreationViewModel');
            var selectedCharacteristic = selectedCharacteristicBC.getObject();
            var sPath = selectedCharacteristicBC.getPath();

            var valueObject = this.equipmentCreationViewModel.validateCharacteristicAddtionalValue(selectedCharacteristic);

            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE', valueObject.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE);
            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT', valueObject.VALUE_NEUTRAL_FROM_ADDITIONAL_VALUE_STATE_TEXT);

            this.equipmentCreationViewModel.refresh();
        },
        handleCharacValueChange: function (oEvent) {
            var selectedCharacteristicBC = oEvent.getSource().getBindingContext('equipmentCreationViewModel');
            var selectedCharacteristic = selectedCharacteristicBC.getObject();
            var sPath = selectedCharacteristicBC.getPath();

            var valueObject = this.equipmentCreationViewModel.validateCharacteristicValue(selectedCharacteristic);

            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE);
            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT);

            this.equipmentCreationViewModel.refresh();
        },

        onClassDateChanged: function(oEvent){
            var selectedCharacteristicBC = oEvent.getSource().getBindingContext('equipmentCreationViewModel');
            var sPath = selectedCharacteristicBC.getPath();

            var selectedCharacteristic = selectedCharacteristicBC.getObject();

            var valueObject = this.equipmentCreationViewModel.validateCharacteristicDateValue(selectedCharacteristic);
            if(valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE == "None"){
                this.equipmentCreationViewModel.setProperty(sPath + '/IS_DATE', selectedCharacteristicBC.VALUE_NEUTRAL_FROM ? true : false);
            }
            
            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE);
            this.equipmentCreationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT', valueObject.VALUE_NEUTRAL_FROM_VALUE_STATE_TEXT);

            this.equipmentCreationViewModel.refresh();
        },

        showCreatedSRB: function(oEvent){
            var that = this;

            this.loadEquipmentData().then(function (createdSRBEquipments) {
                createdSRBEquipments.forEach(function(createdEqui){
                    if(createdEqui.ERUHR) {
                        if(that.equipmentCreationViewModel.getLanguage() === 'D') {
                            createdEqui.FORMATTED_ERDAT = moment(createdEqui.ERUHR).format("DD.MM.YYYY HH:mm");
                        } else {
                            createdEqui.FORMATTED_ERDAT = moment(createdEqui.ERUHR).format("MM/DD/YYYY HH:mm");
                        }
                    } else if(createdEqui.ERDAT){
                        if(that.equipmentCreationViewModel.getLanguage() === 'D') {
                            createdEqui.FORMATTED_ERDAT = moment(createdEqui.ERDAT).format("DD.MM.YYYY");
                        } else {
                            createdEqui.FORMATTED_ERDAT = moment(createdEqui.ERDAT).format("MM/DD/YYYY HH:mm");
                        }
                    }
                });

                var dialog = that._showCreatedSRBDialog(createdSRBEquipments);
                dialog.open();
            }).catch(function () {

            });

        },

        loadEquipmentData: function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.equipmentCreationViewModel.loadCreatedSRBEquipmentData(resolve, reject);
            });
        },

        _showCreatedSRBDialog: function (createdSRBEquipments) {
            var that = this;

            this.equipmentCreationViewModel.setProperty("/createdSRBEquipments", createdSRBEquipments);

            var onAcceptPressed = function () {
                var equipmentCreationViewModel = that.getParent().getModel("equipmentCreationViewModel");
            };

            if (!this._createdEquipmentPopup) {
                if (!this._ocreatedEquipmentPopoverContent) {
                    this._ocreatedEquipmentPopoverContent = sap.ui.xmlfragment(this.createId("equipmentCreationPopover"), "SAMMobile.components.Equipment.view.creation.EquipmentsCreated", this);
                }

                this._createdEquipmentPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._ocreatedEquipmentPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {

                    }
                });

                this._createdEquipmentPopup.setModel(that.component.getModel('i18n'), "i18n");
                this._createdEquipmentPopup.setModel(that.component.getModel('i18n_core'), "i18n_core");
            }

            this._createdEquipmentPopup.setTitle(that.component.i18n.getText("CREATED_SRB"));

            this._createdEquipmentPopup.setModel(that.equipmentCreationViewModel, "createdSRBViewModel");

            return this._createdEquipmentPopup;

        },

        displayCharacValueSearch: function (oEvent) {
            this.equipmentCreationViewModel.displayCharacValueSearch(oEvent, this);
        },

        handleCharacValueSearch: function (oEvent) {
            this.equipmentCreationViewModel.handleCharacValueSearch(oEvent);
        },

        handleCharacValueSelect: function (oEvent) {
            this.equipmentCreationViewModel.handleCharacValueSelect(oEvent);
        },

        _onDeleteSRBEquipment: function(oEvent){
            var that = this;
            var deleteObject = oEvent.getSource().getBindingContext("createdSRBViewModel").getObject();
            this.equipmentCreationViewModel.deleteEquipment(deleteObject, function(){
                MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("SRB_DELETED_SUCCESSFULLY"));
            });
        },

        _onEditSRBEquipment: function(oEvent){
            var that = this;
            var updateObject = oEvent.getSource().getBindingContext("createdSRBViewModel").getObject();
            this.equipmentCreationViewModel.setNewData(updateObject);
            oEvent.getSource().oParent.oParent.oParent.oParent.close();
        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var fileArr = oEvent.getParameter("files");

            this.equipmentCreationViewModel.setBusy(true);
            for (let index = 0; index < fileArr.length; index++) {
                var file = fileArr[index];
                let name = file.name;
                var reader = new FileReader();
                reader.onload = function (readerEvt) {
                    binaryString = readerEvt.target.result.split(",")[1];
                    fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);

                    // On windows, we have to get the fileExt based on the file name
                    // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#browser-quirks section "readAsDataURL"
                    if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                        var fileExt = name.split(".").pop();
                    } else {
                        var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                    }

                    var completeFileName = name;
                    var indexOfPoint = completeFileName.lastIndexOf('.');
                    var fileName = completeFileName.slice(0, indexOfPoint);
                    fileName = that.mediaAttachmentsController.replaceUmlaute(fileName);
                    that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                };
                reader.onerror = function (error) {
                    MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                    that.editNotificationViewModel.setBusy(false);
                };

                reader.readAsDataURL(file);
                //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            }
            oEvent.getSource().clear();
        },

        onDeleteAttachment: function (oEvent) {
            var attachment = oEvent.getSource().getBindingContext('equipmentCreationViewModel').getObject();

            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        if (attachment.MOBILE_ID == "") {
                            this.equipmentCreationViewModel.removeFromModelPath(attachment, "/equipment/Attachments");
                        } else {
                            attachment.setDeleteFlag(true);
                        }
                        this.equipmentCreationViewModel.refresh(true);
                        MessageToast.show(this.component.i18n.getText("EQUIPMENT_ATTACHMENT_DELETE_SUCCESS_TEXT"));
                    }
                }.bind(this)
            });
        },

        onEditAttachment: function (oEvent) {

            const that = this;
            var bContext = oEvent.getSource().getBindingContext('equipmentCreationViewModel');

            var onNewFileNameAccepted = function (newFileName) {

                var dialog = this;
                var attachment = bContext.getObject();

                // Remove whitespaces from file name as this causes issues on sync
                newFileName = newFileName.replace(/\s/g,"");
                var patternFileName = new RegExp(/[\\?*.%,\/:<>\"]/g);
                if (patternFileName.test(newFileName)) {
                    MessageToast.show(that.component.i18n.getText("fileNameInvalid"));
                    return;
                }
                newFileName = that.mediaAttachmentsController.replaceUmlaute(newFileName);
                attachment.rename(newFileName, function () {
                    that.equipmentCreationViewModel.refresh(true);
                    MessageToast.show(that.component.i18n.getText("EQUIPMENT_ATTACHMENT_UPDATE_SUCCESS_TEXT"));
                    dialog.close();
                }, function (err) {
                    that.onViewModelError(err);
                });
            };

            this._getEditAttachmentDialog(bContext.getObject(), onNewFileNameAccepted).open();
        },

        onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getSelectedItem().getBindingContext('equipmentCreationViewModel');
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

        onCapturePhotoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.equipmentCreationViewModel, '/equipment/Attachments/');
            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, this.fileSystem.filePathTmp, function(){});
        },

        handleTypeMissmatch: function(oEvent) {
            MessageBox.warning(this.oBundle.getText("FILETYPE_NOT_SUPPORTED"));
        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;
            var equiObject = this.equipmentCreationViewModel.getEquipment();
            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = equiObject.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.equipmentCreationViewModel.setBusy(false);
                MessageToast.show(that.component.i18n.getText("EQUIPMENT_ATTACHMENT_INSERT_SUCCESS_TEXT"));
            };

            var onWriteError = function (msg) {
                that.equipmentCreationViewModel.setBusy(false);
                MessageToast.show(msg);
            };

            this.fileSystem.writeToTemp(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            var equiObject = this.equipmentCreationViewModel.getEquipment();
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];
                    equiObject.addAttachment(attachmentObj);
                }
            } else {
                equiObject.addAttachment(mediaObject);
            }
            that.equipmentCreationViewModel.refresh(true);
        },

        _getEditAttachmentDialog: function (attachment, acceptCb) {
            var that = this;
            var oBundle = this.getOwnerComponent().rootComponent.getModel("i18n").getResourceBundle();

            var dialog = new Dialog({
                title: oBundle.getText("EnterFileName"),
                contentWidth: "50%",
                content: new Input({
                    maxLength: 50,
                    placeholder: oBundle.getText("EnterFileName"),
                    value: "{attachment>/fileName}",
                    valueLiveUpdate: true
                }),
                beginButton: new Button({
                    text: oBundle.getText("cancel"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: oBundle.getText("save"),
                    enabled: "{= ${attachment>/fileName}.length > 0}",
                    press: function () {
                        acceptCb.apply(this.getParent(), [this.getParent().getModel("attachment").getData().fileName]);
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            dialog.setModel(new JSONModel({
                fileName: ""
            }), "attachment");

            return dialog;
        },

        _tryCreateUserAttachmentsControl: function () {
            var me = this;
            if (!me.userAttachmentControl) {

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {

                    var equipmentObject = me.equipmentCreationViewModel.getEquipment();
                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = equipmentObject.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    equipmentObject.addAttachment(attachmentObj);
                    me.equipmentCreationViewModel.refresh(true);
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {

                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));
                    me.equipmentCreationViewModel.refresh();

                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {

                    var attachmentObj = bCtx.getObject();
                    var attachments = me.equipmentCreationViewModel.getEquipment().Attachments;

                    for (var i = 0; i < attachments.length; i++) {
                        if (attachments[i].FILENAME == attachmentObj.FILENAME) {
                            attachments.splice(i, 1);
                            break;
                        }
                    }

                    me.equipmentCreationViewModel.setProperty('/equipment/Attachments', attachments);
                    me.equipmentCreationViewModel.refresh();
                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };
            }
        },

        onValueChange: function(oEvent){
            this.equipmentCreationViewModel.valueStateModel.setProperty(oEvent.getSource().getBindingPath('valueState'), "None");
            this.equipmentCreationViewModel.valueStateModel.refresh();
        }

    });

});