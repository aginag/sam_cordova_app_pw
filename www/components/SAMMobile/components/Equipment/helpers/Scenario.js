sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function Scenario(name, config) {
            this.name = name;
            this.orderUserStatus = config.orderUserStatus;
            this.longTexts = config.longTexts;
            this.complaintNotification = config.complaintNotification;
            this.availableNotificationTypes = config.availableNotificationTypes;
            this.afterSync = config.afterSync;
        }

        Scenario.prototype = {
            getOrderUserStatus: function () {
                return this.orderUserStatus;
            },

            getListItemLongtextInformation: function () {
                return this.longTexts.list;
            },

            getDescriptionTabLongtextInformation: function() {
                return this.longTexts.descriptionTab;
            },

            getComplaintNotificationInfo: function() {
                return this.complaintNotification;
            },

            getAcceptedNotificationTypes: function () {
                return this.availableNotificationTypes;
            },

            getAfterSyncStatuses: function() {
                return this.afterSync;
            }
        };

        Scenario.prototype.constructor = Scenario;

        return Scenario;
    });