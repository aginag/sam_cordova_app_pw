sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                Equipment: ["MOBILE_ID", "ACTION_FLAG"],
                CharacteristicValuesJoinsMl: ["cv.ADZHL", "cv.ATFLB", "cv.ATFLV", "cv.ATINN", "cv.ATWRT", "cvml.ATWTB", "cv.ATZHL"],
                CreatedEquipments: ["eq.ACTION_FLAG", "eq.SWERK", "eq.WKCTR", "eq.EQUIPMENT_DISPLAY", "eq.MOBILE_ID", "eq.EQUNR", "eq.TPLNR", "eq.SHTXT", "eq.BEBER", "md.WOOBJ", "md.ERDAT", "md.ERUHR","md.READG_CHAR"]
            },
            queries: {

                WorkCenterMl: {
                    get: {
                        query: "SELECT * FROM WorkCenterMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    },
                    valueHelp: {
                        query: "SELECT DISTINCT wkml.ACTION_FLAG, wkml.ARBPL, wkml.KTEXT, wkml.MOBILE_ID, wkml.OBJID, wkml.SPRAS "+
                               "FROM ZMWFM_ADD_WRKCR  as wk "+
                               "LEFT JOIN WorkCenterMl as wkml ON wkml.ARBPL  = wk.WRKCNTR and wkml.SPRAS = '$1' "+
                               "where wk.USERID = '$2' AND (UPPER(wkml.KTEXT) LIKE UPPER('%$3%') OR UPPER(wkml.ARBPL) LIKE UPPER('%$4%')) ORDER BY wkml.ARBPL ",
                        params: [{
                            parameter: "$1",
                            value: "spras"
                        }, {
                            parameter: "$2",
                            value: "USER"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }]
                    },
                },

                FunctionLocation: {
                    get: {
                        query: "SELECT * FROM FunctionLoc ",
                        params: []
                    },
                    valueHelp: {
                        query: "SELECT * FROM FunctionLoc WHERE SPRAS = '$1' AND TPLMA = '$2' AND (UPPER(TPLNR) LIKE UPPER('%$3%') OR UPPER(BEBER) LIKE UPPER('%$4%') OR UPPER(TPLNR_SHTXT) LIKE UPPER('%$5%')) ORDER BY TPLNR_SHTXT",
                        params: [{
                            parameter: "$1",
                            value: "spras"
                        }, {
                            parameter: "$2",
                            value: "TPLMA"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }]
                    },
                },

                FunctionLocationCategory: {
                    get: {
                        query: "SELECT * FROM FunctionLoc ",
                        params: []
                    },
                    valueHelp: {
                        query: "SELECT * FROM FunctionLoc WHERE SPRAS = '$1' AND TPLMA = '$2' AND (UPPER(TPLNR) LIKE UPPER('%$3%') OR UPPER(RBNR) LIKE UPPER('%$4%') OR UPPER(TPLNR_SHTXT) LIKE UPPER('%$5%')) ORDER BY TPLNR_SHTXT",
                        params: [{
                            parameter: "$1",
                            value: "spras"
                        }, {
                            parameter: "$2",
                            value: "TPLMA"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }]
                    },
                },

                FunctionLocationTyp: {
                    get: {
                        query: "SELECT * FROM FunctionLoc ",
                        params: []
                    },
                    valueHelp: {
                        query: "SELECT * FROM FunctionLoc WHERE SPRAS = '$1' AND TPLMA = '$2' AND (UPPER(TPLNR) LIKE UPPER('%$3%') OR UPPER(EQART) LIKE UPPER('%$4%') OR UPPER(TPLNR_SHTXT) LIKE UPPER('%$5%')) ORDER BY TPLNR_SHTXT",
                        params: [{
                            parameter: "$1",
                            value: "spras"
                        }, {
                            parameter: "$2",
                            value: "TPLMA"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }]
                    },
                },

                CodeMl: {
                    get: {
                        query: "SELECT * FROM CodeMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    },
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 * FROM CodeMl "
                            + "WHERE SPRAS = '$3' AND  CODE_GROUP = '$4' AND CATALOG_TYPE = '$7' AND (UPPER(CODE_TEXT) LIKE UPPER('%$5%') OR UPPER(CODE) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "codeGroup"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    },
                    search: {
                        query: "SELECT * FROM CodeMl WHERE SPRAS = '$1' AND CATALOG_TYPE = '$2' AND CODE_GROUP IN $3 ",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }, {
                            parameter: "$2",
                            value: "catalogType"
                        }, {
                            parameter: "$3",
                            value: "codeGroups"
                        }]
                    }
                },

                EquipmentCharacs: {
                    get: {
                        query: "SELECT $COLUMNS, COUNT(ec.MOBILE_ID) as cv_count FROM EquipmentCharacs AS ec LEFT JOIN Characteristics AS c ON ec.CHARACT = c.ATNAM " +
                            "LEFT JOIN MeasurementUnit AS u ON u.MSEH3 = c.MSEHI AND u.SPRAS = '$2' " +
                            "LEFT JOIN Classification as cl ON cl.CLASS=ec.CLASS " +
                            "LEFT JOIN ClassificationChar AS cc ON c.ATINN = cc.IMERK AND cc.CLINT=cl.CLINT " +
                            "LEFT JOIN CharacteristicValues as cv on cv.ATINN = c.ATINN " +
                            "LEFT JOIN CharacteristicValMl as cvml ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cvml.SPRAS='$3' " +
                            "WHERE ec.EQUNR='$1' AND ec.ACTION_FLAG!='D' AND ec.ACTION_FLAG!='M' " +
                            "GROUP BY ec.MOBILE_ID ,ec.CHARACT,ec.CLASS, cl.KSCHL, ec.CHARACT_DESCR,EQUNR, ec.VALUE_NEUTRAL_FROM, ec.VALUE_NEUTRAL_TO, " +
                            "ec.VALUE_TO, ec.VALUE_FROM, ec.UNIT_FROM, ec.UNIT_TO,ec.VALUE_DATE, ec.TYPE, ec.ACTION_FLAG, cv.ATINN, c.ATFOR, c.ATEIN, c.ATSON, c.ANZDZ, " +
                            "c.ANZST, c.ATSCH, u.MSEHT, cc.POSNR ORDER BY cc.POSNR",
                        params: [{
                            parameter: "$1",
                            value: "equipment"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "EquipmentCharacsJoinsCharacteristics"
                    }
                },

                CharacteristicValues: {
                    get: {
                        query: "SELECT $COLUMNS FROM CharacteristicValues AS cv " +
                            "LEFT JOIN CharacteristicValMl as cvml ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cv.ATZHL=cvml.ATZHL AND cvml.SPRAS='$2' " +
                            "WHERE cv.ATINN='$1' ORDER BY cv.ATZHL",
                        params: [{
                            parameter: "$1",
                            value: "atinn"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }],
                        columns: "CharacteristicValuesJoinsMl"
                    },
                    search: {
                        query: "SELECT $COLUMNS, c.ATSCH  FROM CharacteristicValues AS cv "+
                               "LEFT JOIN Characteristics AS c ON c.ATINN = cv.ATINN " +
                               "LEFT JOIN CharacteristicValMl as cvml  ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cv.ATZHL=cvml.ATZHL AND cvml.SPRAS='$6' " +
                               "WHERE ( UPPER(cv.ATWRT) LIKE UPPER('%$1%') OR UPPER(cv.ATFLB) LIKE UPPER('%$2%')  OR UPPER(cv.ATFLV) LIKE UPPER('%$3%') OR UPPER(cvml.ATWTB) LIKE UPPER('%$4%')) AND cv.ATINN='$5' " +
                               "ORDER BY cv.ATZHL",
                        params: [{
                            parameter: "$1",
                            value: "searchString"
                        }, {
                            parameter: "$2",
                            value: "searchString"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "atinn"
                        }, {
                            parameter: "$6",
                            value: "language"
                        }],
                        columns: "CharacteristicValuesJoinsMl"
                    }
                },

                CreatedSRBEquipments: {
                    get: {
                        query: "SELECT $COLUMNS FROM Equipment as eq " +
                            "LEFT JOIN MeasurementDocument as md ON md.POINT = substring(eq.EQUNR, 1, 12) " +
                            "WHERE eq.ACTION_FLAG = 'N'",
                        params: [],
                        columns: "CreatedEquipments"
                    },
                }

            }

        };
    });
