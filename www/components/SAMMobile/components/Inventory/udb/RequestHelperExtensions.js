sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                Inventory: ["MOBILE_ID", "MATERIAL", "MATL_DESC", "STGE_LOC", "PLANT", "ENTRY_QNT", "ENTRY_UOM"],
                InventoryList: ["MOBILE_ID", "MATERIAL", "MATL_DESC", "STGE_LOC", "PLANT", "ENTRY_QNT", "ENTRY_UOM"]
            },
            queries: {

                Inventory: {
                    only_stocks : {
                        query : "SELECT TOP $1 START AT $2 $COLUMNS FROM InventoryList"+
                         " WHERE ENTRY_QNT > 0 "+
                         " AND (UPPER(MATERIAL) LIKE UPPER('%$3%') " +
                         " OR UPPER(MATL_DESC) LIKE UPPER('%$4%') " +
                         " OR UPPER(STGE_LOC) LIKE UPPER('%$5%') " +
                         " OR UPPER(PLANT) LIKE UPPER('%$6%')"  +
                         " OR UPPER(ENTRY_QNT) LIKE UPPER('%$7%') " +
                         " OR UPPER(ENTRY_UOM) LIKE UPPER('%$8%'))",
                        params : [ {
                            "parameter": "$1",
                            "value": "noOfRows"
                        }, {
                            "parameter": "$2",
                            "value": "startAt"
                        }, {
                            parameter : "$3",
                            value : "searchString"
                        }, {
                            parameter : "$4",
                            value : "searchString"
                        }, {
                            parameter : "$5",
                            value : "searchString"
                        }, {
                            parameter : "$6",
                            value : "searchString"
                        }, {
                            parameter : "$7",
                            value : "searchString"
                        }, {
                            parameter : "$8",
                            value : "searchString"
                        } ],
                        columns : "InventoryList"
                    },
                    all : {
                        query : "SELECT TOP $1 START AT $2 $COLUMNS FROM InventoryList "+
                         " WHERE UPPER(MATERIAL) LIKE UPPER('%$3%') " + 
                         " OR UPPER(MATL_DESC) LIKE UPPER('%$4%') " +
                         " OR UPPER(STGE_LOC) LIKE UPPER('%$5%') " +
                         " OR UPPER(PLANT) LIKE UPPER('%$6%')"  +
                         " OR UPPER(ENTRY_QNT) LIKE UPPER('%$7%') " +
                         " OR UPPER(ENTRY_UOM) LIKE UPPER('%$8%')",
                        params : [ {
                            "parameter": "$1",
                            "value": "noOfRows"
                        }, {
                            "parameter": "$2",
                            "value": "startAt"
                        }, {
                            parameter : "$3",
                            value : "searchString"
                        }, {
                            parameter : "$4",
                            value : "searchString"
                        }, {
                            parameter : "$5",
                            value : "searchString"
                        }, {
                            parameter : "$6",
                            value : "searchString"
                        }, {
                            parameter : "$7",
                            value : "searchString"
                        }, {
                            parameter : "$8",
                            value : "searchString"
                        } ],
                        columns : "InventoryList"
                    },
                },
            }
        };
    });
