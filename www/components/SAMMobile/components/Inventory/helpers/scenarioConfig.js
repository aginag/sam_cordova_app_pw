sap.ui.define([],
    function () {
        "use strict";
        return {
            //Primary RCM
            dm_prim_rcm: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0005"
                    },

                    workFinished: {
                        key: "workFinished",
                        status: 'E0009'
                    },

                    complete: {
                        key: "complete",
                        status: 'E0010'
                    }
                },
                longTexts: {
                    list: {
                        isOrderLongtext: true,
                        objectType: "WAPI", // WCA Longtext
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: true,
                        objectType: "WAPI", // WCA Longtext
                        readOnly: true
                    }
                },
                complaintNotification: {
                    completeStatus: {
                        STATUS_PROFILE: "YEL000Y1",
                        USER_STATUS: "E0015"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: true,
                    showWarrantyFlag: true
                },
                availableNotificationTypes: ["Y0", "Y1"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS2"
                    }
                }
            },

            //Primary Upkeeping
            dm_prim_upk: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0005"
                    },

                    workFinished: {
                        key: "workFinished",
                        status: 'E0009'
                    },

                    complete: {
                        key: "complete",
                        status: 'E0010'
                    }
                },
                longTexts: {
                    list: {
                        isOrderLongtext: true,
                        objectType: "AVOT", // Service Order Longtext
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: true,
                        objectType: "AVOT", // Service Order Longtext
                        readOnly: true
                    }
                },
                complaintNotification: {
                    completeStatus: {
                        STATUS_PROFILE: "YEL000Y1",
                        USER_STATUS: "E0015"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: false,
                    showWarrantyFlag: false
                },
                availableNotificationTypes: ["Y0", "Y1"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS2"
                    }
                }
            },

            //Secondary RCM
            dm_sec_rcm: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0006"
                    },

                    workFinished: {
                        key: "workFinished",
                        status: 'E0020'
                    },

                    complete: {
                        key: "complete",
                        status: 'E0021'
                    }
                },
                longTexts: {
                    list: {
                        isOrderLongtext: true,
                        objectType: "WAPI", // WCA Longtext
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: true,
                        objectType: "WAPI", // WCA Longtext
                        readOnly: true
                    }
                },
                complaintNotification: {
                    completeStatus: {
                        STATUS_PROFILE: "YEL000Y1",
                        USER_STATUS: "E0015"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: true,
                    showWarrantyFlag: true
                },
                availableNotificationTypes: ["Y0", "Y1"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS3"
                    }
                }
            },

            //Secondary Upkeeping
            dm_sec_upk: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0006"
                    },

                    workFinished: {
                        key: "workFinished",
                        status: 'E0020'
                    },

                    complete: {
                        key: "complete",
                        status: 'E0021'
                    }
                },
                longTexts: {
                    list: {
                        isOrderLongtext: true,
                        objectType: "AVOT", // Service Order Longtext
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: true,
                        objectType: "AVOT", // Service Order Longtext
                        readOnly: true
                    }
                },
                complaintNotification: {
                    completeStatus: {
                        STATUS_PROFILE: "YEL000Y1",
                        USER_STATUS: "E0015"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: false,
                    showWarrantyFlag: false
                },
                availableNotificationTypes: ["Y1"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS3"
                    }
                }
            },

            //Secondary CMA
            dm_sec_cma: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0006"
                    },

                    workFinished: {
                        key: "workFinished",
                        status: 'E0020'
                    },

                    complete: {
                        key: "complete",
                        status: 'E0021'
                    }
                },
                longTexts: {
                    list: {
                        isOrderLongtext: false,
                        objectType: "QMEL", // Notification Longtext
                        readOnly: true
                    },
                    descriptionTab: {
                        isOrderLongtext: false,
                        objectType: "QMEL", // Notification Longtext
                        readOnly: false
                    }
                },
                complaintNotification: {
                    completeStatus: {
                        STATUS_PROFILE: "YEL000Y1",
                        USER_STATUS: "E0015"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: false,
                    showWarrantyFlag: false
                },
                availableNotificationTypes: ["Y0", "Y1", "A5"],
                afterSync: {
                    fromStatus: "E0002",
                    toStatus: {
                        STATUS: "E0003",
                        STATUS_PROFILE: "ZMWFOPS3"
                    }
                }
            }

        };
    });