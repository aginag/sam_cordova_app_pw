sap.ui.define(["sap/ui/core/format/DateFormat"],

    function (DateFormat) {
        "use strict";
        return {
            formatTimestampToOrderListDate: function (timestamp) {
                return moment(timestamp).format("DD. MMM, HH:mm");
            },

            formatSAPBooleanToJavascriptBoolean: function (sapBool) {
                return sapBool == "X" ? true : false;
            },

            formatOperationUserStatusToIcon: function (status, statusProfile) {
                var statuses = this.getOwnerComponent().getModel("customizingModel").oData.ZDewaUserStatus;

                for (var i = 0; i < statuses.length; i++) {
                    var statusObject = statuses[i];

                    if (statusObject.STATUS == status && statusObject.STATUS_PROFILE == statusProfile) {
                        return statusObject.ICON;
                    }
                }

                var userStatusMl = this.getOwnerComponent().getModel("customizingModel").oData.UserStatusMl;

                for (var i = 0; i < userStatusMl.length; i++) {
                    var userStatus = userStatusMl[i];

                    if (userStatus.USER_STATUS == status && userStatus.STATUS_PROFILE == statusProfile) {
                        if (userStatus.USER_STATUS_CODE.startsWith("RE") && !userStatus.USER_STATUS_CODE.startsWith("REC")) {
                            return "sap-icon://decline";
                        }
                    }
                }

                return "sap-icon://circle-task";
            },

            formatOperationUserStatusToText: function (status, statusProfile) {
                var statuses = this.getOwnerComponent().getModel("customizingModel").oData.ZDewaUserStatus;

                for (var i = 0; i < statuses.length; i++) {
                    var statusObject = statuses[i];

                    if (statusObject.STATUS == status && statusObject.STATUS_PROFILE == statusProfile) {
                        return statusObject.USER_STATUS_DESC;
                    }
                }

                var userStatusMl = this.getOwnerComponent().getModel("customizingModel").oData.UserStatusMl;

                for (var i = 0; i < userStatusMl.length; i++) {
                    var userStatus = userStatusMl[i];

                    if (userStatus.USER_STATUS == status && userStatus.STATUS_PROFILE == statusProfile) {
                        return userStatus.USER_STATUS_DESC;
                    }
                }

                return "TEXT N/A";
            },

            formatPersNoToDIVCONTeamMember: function (persNo, teamGuid) {
                var teamMembers = this.getOwnerComponent().getModel("customizingModel").oData.DIVCONTeams;

                for (var i = 0; i < teamMembers.length; i++) {
                    var member = teamMembers[i];

                    if ((teamGuid !== undefined && teamGuid !== "") && member.T_GUID !== teamGuid) {
                        continue;
                    }

                    if (member.R_GUID.startsWith(persNo)) {
                        return member.FIRTNAME + " " + member.LASTNAME;
                    }
                }

                return persNo;
            },

            formatHoursFromUdb: function (hours) {
                return parseFloat(hours).toFixed(2);
            },

            formatCheckTypeToIcon: function (type) {
                switch (type) {
                    case 0:
                        return "sap-icon://checklist-item";
                    case 1:
                        return "sap-icon://measurement-document";
                    default:
                        return "";
                }
            },

            formatStgLocToText: function (stgeLoc, plant) {
                var storageLocations = this.getOwnerComponent().getModel("customizingModel").oData.StorageLocation;

                for (var i = 0; i < storageLocations.length; i++) {
                    var location = storageLocations[i];

                    if (location.WERKS == plant && location.LGORT == stgeLoc) {
                        return stgeLoc + " - " + location.TEXT;
                    }
                }

                return stgeLoc;
            },

            formatPlantToText: function (plant) {
                return plant;
            },

            formatAttendanceTypeToText: function (awart) {
                var attendanceTypes = this.getOwnerComponent().getModel("customizingModel").oData.AttendanceTypes;

                for (var i = 0; i < attendanceTypes.length; i++) {
                    var type = attendanceTypes[i];

                    if (type.SUBTY == awart) {
                        return awart + " - " + type.ATEXT;
                    }
                }

                return awart;
            },

            formatActivityTypeToText: function (lstar) {
                var activityTypes = this.getOwnerComponent().getModel("customizingModel").oData.ActivityTypeMl;
                if (!activityTypes) {
                    return '';
                }

                for (var i = 0; i < activityTypes.length; i++) {
                    var type = activityTypes[i];
                    if (type.LSTAR === lstar) {
                        return lstar + " - " + type.KTEXT;
                    }
                }

                return lstar;
            },

            formatToNotifTypeText: function (notifType) {
                var notificationTypes = this.getOwnerComponent().getModel("customizingModel").oData.NotificationType;
                if (!notificationTypes) {
                    return '';
                }
                for (var i = 0; i < notificationTypes.length; i++) {
                    var typeObj = notificationTypes[i];
                    if (typeObj.QMART === notifType) {
                        return notifType + " - " + typeObj.QMARTX;
                    }
                }

                return notifType;
            },

            formatWorkCenterToText: function (wkCtr) {
                var workCenter = this.getOwnerComponent().getModel("customizingModel").oData.WorkCenterMl;
                if (!workCenter || wkCtr == "") {
                    return '';
                }
                for (var i = 0; i < workCenter.length; i++) {
                    var center = workCenter[i];
                    if (center.ARBPL === wkCtr) {
                        return wkCtr + " - " + center.KTEXT;
                    }
                }

                return wkCtr;
            },

            formatVehicleText: function (vehicleText) {
                return vehicleText && vehicleText !== '' ? "(Vehicle: " + vehicleText + " )" : "";
            },

            formatPriorityToText: function (priority, prioType) {
                var priorities = this.getOwnerComponent().getModel("customizingModel").oData.PriorityTypes;
                if (!priorities) {
                    return '';
                }

                for (var i = 0; i < priorities.length; i++) {
                    var prioObj = priorities[i];

                    if (prioObj.PRIORITY === priority && prioObj.PRIOTYPE === prioType) {
                        return priority + " - " + prioObj.PRIORITY_DESC;
                    }
                }

                return priority;
            },

            formatBytesToSize: function (bytes) {
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (isNaN(bytes)) {
                    return bytes;
                }
                if (bytes == 0) return '0 Byte';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            },

            formatAttachmentTypeToIconDewa: function (type) {
                if (type && type !== undefined) {
                    switch (type.toLowerCase()) {
                        case "jpg":
                            return "sap-icon://attachment-photo";
                            break;
                        case "jpeg":
                            return "sap-icon://attachment-photo";
                            break;
                        case "png":
                            return "sap-icon://attachment-photo";
                            break;
                        case "pdf":
                            return "sap-icon://pdf-attachment";
                            break;
                        case "txt":
                            return "sap-icon://attachment-text-file";
                            break;
                        case "mov":
                            return "sap-icon://attachment-video";
                            break;
                        case "wav":
                            return "sap-icon://attachment-audio";
                            break;
                        default:
                            return "sap-icon://attachment";
                            break;
                    }
                }
                return "sap-icon://attachment";
            },
        };
    });
