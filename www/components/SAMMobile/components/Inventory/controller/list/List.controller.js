sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Inventory/controller/list/InventoryListViewModel",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
], function(Controller, InventoryListViewModel, MessageBox, formatter) {
    "use strict";

    return Controller.extend("SAMMobile.components.Inventory.controller.list.List", {
        formatter: formatter,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("inventoryList").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("inventoryList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.inventoryListViewModel = new InventoryListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.inventoryListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.inventoryListViewModel.setList(this.byId("inventoryList"));

            this.initialized = false;
            this.getView().setModel(this.inventoryListViewModel, "inventoryListViewModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;
            
            var onMasterDataLoaded = function(routeName) {

                if (this.inventoryListViewModel.needsReload()) {
                    this.loadObjects();
                }

                if (!this.initialized) {
                    this.initialized = true;
                }
            };

            this.inventoryListViewModel.fetchCounts();
            this.globalViewModel.setComponentHeaderText(this.inventoryListViewModel.getHeaderText());
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            var onReloadSuccess = function() {

            };

            this.inventoryListViewModel.fetchCounts();
            this.globalViewModel.setComponentHeaderText(this.inventoryListViewModel.getHeaderText());

            if (bSync) {
                this.inventoryListViewModel.refreshData();
            } else {
                this.inventoryListViewModel.fetchData(onReloadSuccess.bind(this));
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {

        },

        onLogout: function() {
            this.initialized = false;
        },

        onViewModelError: function(err) {
            MessageBox.error(err.message);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("inventoryList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        onListGrowing: function (oEvt) {
            this.inventoryListViewModel.onListGrowing(oEvt);
        },

        onTableSearch: function (oEvt) {
            this.inventoryListViewModel.onSearch(oEvt);
        },
    });

});