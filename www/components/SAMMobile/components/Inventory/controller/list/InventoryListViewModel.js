sap.ui.define(["SAMContainer/models/ViewModel",
    "sap/ui/model/Filter",
    "SAMMobile/models/dataObjects/Inventory"
],
    function (ViewModel, Filter, Inventory) {
        "use strict";

        // constructor
        function InventoryListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/materials";

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        InventoryListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        materials: [],
                        searchString: "",
                        counts: {
                            materials: 0
                        },
                        view: {
                            busy: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold
                        }
                    };
                }
            },

            getInventoryQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        searchString: this.getSearchString()
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function() {
                    return "INVENTORY ( " + this.getProperty("/inventoryCounter") + " )";
                }
            }
        });

        InventoryListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };


        InventoryListViewModel.prototype.onSearch = function (oEvt) {
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);
        };

        InventoryListViewModel.prototype.setNewData = function (data, bGrowing) {
            var that = this;

            var newMaterials = data.map(function (inventoryData) {
                return new Inventory(inventoryData, that._requestHelper, that);
            });

            // If growing, append new data to current data on the model
            // If !growing, just set new data on to the model
            if (bGrowing) {

            } else {
                this.setProperty("/materials", newMaterials);
            }
        };

        InventoryListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;
            var that = this;

            var load_success = function (data) {
                that._needsReload = false;

                that.setNewData(data, bGrowing);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching inventory"));
            };

            this.setBusy(true);
            this._requestHelper.getData("Inventory", this.getInventoryQueryParams(bGrowing ? this.getProperty("/materials").length + 1 : 1), load_success, load_error, true);
        };

        InventoryListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        InventoryListViewModel.prototype.resetModel = function () {
            this._needsReload = true;
        };

        InventoryListViewModel.prototype.fetchCounts = function (successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/inventoryCounter", data[0].InventoryCounter[0].COUNT.toString());
            };

            var fetchCounterQueue = [
                this._fetchInventoryCount()
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(errorCb);
        };

        InventoryListViewModel.prototype._fetchInventoryCount = function(){
            return this._requestHelper.fetch({
                id: "InventoryCounter",
                statement: "SELECT COUNT(MOBILE_ID) as COUNT FROM InventoryList "
            });
        };

        InventoryListViewModel.prototype.constructor = InventoryListViewModel;

        return InventoryListViewModel;
    });
