sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function Timesheet(data, requestHelper, model) {
            DataObject.call(this, "TimesheetHeader", "Timesheet", requestHelper, model);

            this.data = data;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.setDefaultData();
        }

        Timesheet.prototype = Object.create(DataObject.prototype, {});

        Timesheet.prototype.onAfterExistingDataSet = function() {

        };

        Timesheet.prototype.initNewObject = function() {
            var that = this;

            this.columns.forEach(function(column) {
                var value = "";

                if (column == "ACTION_FLAG") {
                    value = "N";
                } else if (column == "MOBILE_ID") {
                    value = this.getRandomUUID(12);
                }

                that[column] = value;
            });
        };

        Timesheet.prototype.constructor = Timesheet;

        return Timesheet;
    }
);