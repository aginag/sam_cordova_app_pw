sap.ui.define([],
    function () {
        "use strict";
        return {

            tableColumns: {
                TimeConfirmation: ["MOBILE_ID", "ACTIVITY", "ACT_TYPE", "CALC_MOTIVE", "ACT_WORK", "CONF_CNT", "CONF_NO", "UN_WORK", "PERS_NO", "ORDER_NO", "CONF_TEXT", "START_DATE", "END_DATE", "WORK_CNTR", "START_TIME", "END_TIME", "ACTION_FLAG"],
                CatsHeader: ["MOBILE_ID", "ACTIVITY", "AWART", "BEGUZ", "BEMOT", "CATSHOURS", "COUNTER", "LSTAR", "MEINH", "PERNR", "RAUFNR", "SKOSTL", "WERKS", "WORKDATE", "WORK_CNTR", "PROFILE", "TEXT_FORMAT_IMP", "ACTION_FLAG"],
                timesheetTC: ["a.ACTIVITY", "a.ORDER_NO", "a.ACT_WORK", "a.ACT_TYPE", "a.UN_WORK", "a.START_TIME", "a.END_TIME", "a.ACTION_FLAG", "a.MOBILE_ID", "b.DESCRIPTION"],
                timesheetCATS: ["a.ACTIVITY as ACTIVITY", "a.RAUFNR as ORDER_NO", "a.CATSHOURS as ACT_WORK", "a.LSTAR as ACT_TYPE", "a.MEINH as UN_WORK", "a.BEGUZ as START_TIME", "a.ACTION_FLAG", "a.MOBILE_ID", "b.DESCRIPTION", "a.ENDUZ as END_TIME"],
                getOrderIds: ["a.ORDERID", "b.ACTIVITY", "b.DESCRIPTION"],
                timesheetCalendar: ["a.ACTIVITY", "a.ORDERID", "a.ACT_WORK", "a.UN_WORK", "a.START_TIME", "a.START_DATE", "a.END_TIME", "a.END_DATE", "a.ACTION_FLAG", "a.MOBILE_ID", "a.KTEXT"]
            },

            queries: {

                getSysTables: {
                    get: {
                        query: "SELECT * FROM systable",
                        params: [{}],
                        columns: ""
                    }
                },

                wagesData: {
                    calendar: {
                        query: "SELECT * FROM ZPMC_EXTRAPAY as w WHERE w.PERS_NO = '$1' ",
                        params: [
                            {
                                parameter: "$1",
                                value: "persNo"
                            }
                        ],
                    },
                },

                tcQuery: {
                    calendar: {
                        query: "SELECT $COLUMNS FROM ZPMC_TIMECONF as a WHERE a.PERS_NO = '$1' ",
                        params: [
                            {
                                parameter: "$1",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetCalendar"
                    },

                    day: {
                        query: "SELECT $COLUMNS FROM TIMECONFIRMATION as a LEFT JOIN ORDEROPERATIONS as b ON a.ORDER_NO = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE DAY(a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND a.PERS_NO = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerDay"
                        },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetTC"
                    },
                    week: {
                        query: "SELECT $COLUMNS FROM ZPMC_TIMECONF as a LEFT JOIN ORDEROPERATIONS as b ON a.ORDER_NO = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE datepart(week, a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND a.PERS_NO = '$3'",
                        params: [
                            {
                                parameter: "$1",
                                value: "timePickerWeek"
                            },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetTC"
                    },
                    month: {
                        query: "SELECT $COLUMNS FROM TIMECONFIRMATION as a LEFT JOIN ORDEROPERATIONS as b ON a.ORDER_NO = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE MONTH(a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND a.PERS_NO = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerMonth"
                        },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetTC"
                    },

                },

                TimesheetDayCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM TIMECONFIRMATION as a WHERE DAY(a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND PERS_NO = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerDay"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: ""
                    }
                },

                TimesheetWeekCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM TIMECONFIRMATION as a WHERE datepart(week, a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND PERS_NO = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerWeek"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }],
                        columns: ""
                    }
                },

                TimesheetMonthCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM TIMECONFIRMATION as a WHERE MONTH(a.END_DATE) = '$1' AND YEAR(a.END_DATE) = '$2' AND PERS_NO = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerMonth"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: ""
                    }
                },

                catsQuery: {
                    day: {
                        query: "SELECT $COLUMNS FROM CATSHEADER as a LEFT JOIN ORDEROPERATIONS as b ON a.RAUFNR = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE DAY(a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerDay"
                        },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetCATS"
                    },
                    week: {
                        query: "SELECT $COLUMNS FROM CATSHEADER as a LEFT JOIN ORDEROPERATIONS as b ON a.RAUFNR = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE datepart(week, a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerWeek"
                        },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetCATS"
                    },
                    month: {
                        query: "SELECT $COLUMNS FROM CATSHEADER as a LEFT JOIN ORDEROPERATIONS as b ON a.RAUFNR = b.ORDERID AND a.ACTIVITY = b.ACTIVITY WHERE MONTH(a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerMonth"
                        },
                            {
                                parameter: "$2",
                                value: "timePickerYear"
                            },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: "timesheetCATS"
                    },
                },

                CatsDayCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM CATSHEADER as a WHERE DAY(a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerDay"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }],
                        columns: ""
                    }
                },

                CatsWeekCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM CATSHEADER as a WHERE datepart(week, a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerWeek"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }],
                        columns: ""
                    }
                },

                CatsMonthCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) AS COUNT FROM CATSHEADER as a WHERE MONTH(a.WORKDATE) = '$1' AND YEAR(a.WORKDATE) = '$2' AND a.PERNR = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "timePickerMonth"
                        }, {
                            parameter: "$2",
                            value: "timePickerYear"
                        },
                            {
                                parameter: "$3",
                                value: "persNo"
                            }
                        ],
                        columns: ""
                    }
                },

                ActivityTypeMl: {
                    get: {
                        query: "SELECT * FROM ActivityTypeMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                OrderIds: {
                    get: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM ORDERS as a JOIN ORDEROPERATIONS as b ON a.ORDERID=b.ORDERID WHERE b.PERS_NO = '$3' " +
                                "AND (a.ORDERID LIKE '%$4%') OR (b.ACTIVITY LIKE '%$5%') OR (b.DESCRIPTION LIKE '%$6%')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "persNo"
                        },{
                            parameter: "$4",
                            value: "searchString"
                        },{
                            parameter: "$5",
                            value: "searchString"
                        },{
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "getOrderIds"
                    }
                }

            }
        };
    });

