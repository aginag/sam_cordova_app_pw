sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/CatsHeader"
    ],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, CatsHeader) {
        "use strict";

        // constructor
        function EditCatsEntryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditCatsEntryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        catsEntry: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setCatsEntry: {
                value: function (entry) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = entry.getCopy();

                    }

                    this.setProperty("/catsEntry", entry);
                }
            },

            getCatsEntry: {
                value: function () {
                    return this.getProperty("/catsEntry");
                }
            },

            setColleagues: {
                value: function (colleagues) {
                    this.setProperty("/colleagues", colleagues);
                }
            },

            getColleagues: {
                value: function () {
                    return this.getProperty("/colleagues");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getCatsEntry().setData(this._objectDataCopy);
                }
            },


            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("EditCatsEntry") : this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("NewCatsEntry");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditCatsEntryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditCatsEntryViewModel.prototype.setHours = function (newHours) {
            this.getCatsEntry().setHours(newHours);
            this.getCatsEntry().ACT_WORK = newHours.toFixed(2);
            this.refresh();
        };

        EditCatsEntryViewModel.prototype.setStartTime = function (newStartTime) {
            this.getCatsEntry().setStartTime(newStartTime);
            this.refresh();
        };

        EditCatsEntryViewModel.prototype.setEndTime = function (newEndTime) {
            this.getCatsEntry().setEndTime(newEndTime);
            this.refresh();
        };

        EditCatsEntryViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation on new cats entry
            validator.check("catsEntry.PERNR", "Please select a User").isEmpty();
            validator.check("catsEntry.WORKDATE", "Please select a Start Time").isEmpty();
            validator.check("catsEntry.ORDERID", "Please select an Order").isEmpty();
            validator.check("catsEntry.ACTIVITY", "Please select an Order").isEmpty();
            validator.check("catsEntry.LSTAR", "Please select an Activity Type").isEmpty();
            validator.check("catsEntry.CATSHOURS", "Please select a Workingtime").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
        EditCatsEntryViewModel.prototype.displayTeamMemberSelectDialog = function (oEvent, viewContext) {
            if (!this._oTeamMemberSelectDialog) {
                this._oTeamMemberSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.TeamMemberSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oTeamMemberSelectDialog.display(oEvent, '', null, new JSONModel(this.getColleagues()));
        };

        EditCatsEntryViewModel.prototype.handleTeamMemberSelect = function (oEvent) {
            this._oTeamMemberSelectDialog.select(oEvent, 'persNo', [{
                'PERNR': 'persNo'
            }], this, "/catsEntry");
        };

        EditCatsEntryViewModel.prototype.handleTeamMemberSearch = function (oEvent) {
            this._oTeamMemberSelectDialog.search(oEvent, ["firstName", "lastName", "persNo"]);
        };

        EditCatsEntryViewModel.prototype.displayOrderIdSelectDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oOrderIdSelectDialog) {
                this._oOrderIdSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.OrderIdSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oOrderIdSelectDialog.display(oEvent, {
                tableName: "OrderIds",
                params: {
                    persNo: this.getUserInformation().personnelNumber
                },
                actionName: "get"
            });
        };

        EditCatsEntryViewModel.prototype.handleOrderIdSelect = function (oEvent) {
            this._oOrderIdSelectDialog.select(oEvent, 'ORDERID', [{
                'ORDERID': 'ORDERID',
                'ACTIVITY': 'ACTIVITY',
                'DESCRIPTION': 'DESCRIPTION'
            }], this, "/catsEntry");
        };

        EditCatsEntryViewModel.prototype.handleOrderIdSearch = function (oEvent) {
            this._oOrderIdSelectDialog.search(oEvent, ["ORDERID", "KTEXT"]);
        };


        EditCatsEntryViewModel.prototype.displayActTypeDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oActTypeSelectDialog) {
                this._oActTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.ActivityTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oActTypeSelectDialog.display(oEvent, 'ActivityTypeMl');
        };

        EditCatsEntryViewModel.prototype.handleActTypeSelect = function (oEvent) {
            this._oActTypeSelectDialog.select(oEvent, 'LSTAR', [{
                'LSTAR': 'LSTAR'
            }], this, "/catsEntry");
        };

        EditCatsEntryViewModel.prototype.handleActTypeSearch = function (oEvent) {
            this._oActTypeSelectDialog.search(oEvent, ["LSTAR", "KTEXT"]);
        };

        EditCatsEntryViewModel.prototype.displayAttendanceTypeDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oAttTypeSelectDialog) {
                this._oAttTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.AttendanceTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oAttTypeSelectDialog.display(oEvent, 'AttendanceTypes');
        };

        EditCatsEntryViewModel.prototype.handleAttendanceTypeSelect = function (oEvent) {
            this._oAttTypeSelectDialog.select(oEvent, 'SUBTY', [{
                'AWART': 'SUBTY'
            }], this, "/catsEntry");


        };

        EditCatsEntryViewModel.prototype.handleAttendanceTypeSearch = function (oEvent) {
            this._oAttTypeSelectDialog.search(oEvent, ["ATEXT", "SUBTY"]);
        };

        EditCatsEntryViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    PERNR: sap.ui.core.ValueState.None,
                    WORKDATE: sap.ui.core.ValueState.None,
                    LSTAR: sap.ui.core.ValueState.None,
                    AWART: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditCatsEntryViewModel.prototype.updateCatsEntry = function (catsEntry, successCb) {
            var that = this;
            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                catsEntry.update(false, onSuccess, that.executeErrorCallback.bind(null, new Error("Error while trying to update component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EditCatsEntryViewModel.prototype.constructor = EditCatsEntryViewModel;

        return EditCatsEntryViewModel;
    });
