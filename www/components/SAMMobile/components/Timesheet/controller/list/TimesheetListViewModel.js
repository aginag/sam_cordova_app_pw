sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "SAMMobile/components/Timesheet/models/Timesheet",
        "SAMMobile/models/dataObjects/TimeConfirmation",
        "SAMMobile/models/dataObjects/CatsHeader"],

    function (ViewModel, Filter, Timesheet, TimeConfirmation, CatsHeader) {
        "use strict";

        // constructor
        function TimesheetListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "timesheet");

            this._GROWING_DATA_PATH = "/timesheet";

            this._oList = null;
            this._oDatePicker = "";

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        TimesheetListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        timesheet: [],
                        tcQuery: [],
                        catsQuery: [],
                        searchString: "",
                        timePickerMonth: moment(new Date()).format("MM"),
                        timePickerYear: moment(new Date()).format("YYYY"),
                        timePickerWeek: moment(new Date()).format("ww"),
                        timePickerDay: moment(new Date()).format("DD"),
                        counts: {
                            day: 0,
                            week: 0,
                            month: 0
                        },
                        view: {
                            busy: false,
                            selectedTabKey: "month",
                            orderSelected: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold,
                            statusButtons: []
                        }
                    };
                }
            },


            getSelectedTimesheet: {
                value: function () {
                    return this.getProperty("/selectedTimesheet");
                }
            },

            getTimesheetQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        searchString: this.getSearchString(),
                        persNo: this.getUserInformation().personnelNumber,
                        timePickerMonth: this.getProperty("/timePickerMonth"),
                        timePickerYear: this.getProperty("/timePickerYear"),
                        timePickerWeek: this.getProperty("/timePickerWeek"),
                        timePickerDay: this.getProperty("/timePickerDay")
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setDatePicker: {
                value: function (oDatePicker) {
                    return this._oDatePicker = oDatePicker;
                }
            },

            getDatePicker: {
                value: function () {
                    return this._oDatePicker;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getHeaderText: {
                value: function () {
                    return "TIMESHEET";
                }
            }
        });

        TimesheetListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        TimesheetListViewModel.prototype.onSearch = function (oEvt) {
            // DATABASE SEARCH
            clearTimeout(this._delayedSearch);
            this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);

            var aFilters = [];
            var sValue = oEvt.getSource().getValue();

            if (sValue && sValue.length > 0) {
                aFilters.push(new Filter("ACTIVITY", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("ORDER_NO", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("START_TIME", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("ACT_WORK", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("UN_WORK", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("ACT_TYPE", sap.ui.model.FilterOperator.Contains, sValue));
                var allFilter = new Filter(aFilters);
            }

            var oBinding = this.getList().getBinding("items");
            oBinding.filter(allFilter);
        };

        TimesheetListViewModel.prototype.setNewData = function (data, bGrowing) {
            var that = this;
            var newTimesheet = [];

            var tcQueryArray = data.tcQuery.map(function(tcData) {
                return new TimeConfirmation(tcData, that._requestHelper, that)
            });

            var catsQueryArray = data.catsQuery.map(function(catsData) {
                return new CatsHeader(catsData, that._requestHelper, that)
            });

            var timeSheetArray = tcQueryArray.concat(catsQueryArray);

            // If growing, append new data to current data on the model
            // If !growing, just set new data on to the model
            if (bGrowing) {

            } else {
                this.setProperty("/timesheet", newTimesheet);
            }

            this.setProperty("/timesheet", timeSheetArray);
            this.setSelectedTimesheet(null);

        };

        TimesheetListViewModel.prototype.tabs = {
            day: {
                actionName: "day",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/day", this.getProperty("/counts/day") + value);
                }
            },

            week: {
                actionName: "week",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/week", this.getProperty("/counts/week") + value);
                }
            },
            month: {
                actionName: "month",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/month", this.getProperty("/counts/month") + value);
                }
            }
        };

        TimesheetListViewModel.prototype.onTabChanged = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // MODEL SEARCH
            // Reset binding and resetting searchValue when switching tabs
            this.setProperty("/searchString", "");
            this.getList().getBinding("items").filter(null);

            this.resetGrowingListDelegate();

            if (tabInfo.needsReload) {
                this.fetchData();
            } else {
                this.setNewData(tabInfo.cached);
            }
        };

        TimesheetListViewModel.prototype.setSelectedTimesheet = function (timesheet) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");

            if (selectedTabKey == this.tabs.day.actionName) {
                timesheet = null;
            }

            this.setProperty("/selectedTimesheet", timesheet);
            this.setProperty("/view/timesheetSelected", !timesheet || timesheet == undefined ? false : true);

            if (!this.getProperty("/view/timesheetSelected")) {
                this.getList().removeSelections();
            }
        };

        TimesheetListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var that = this;
            var load_success = function (data) {
                that._needsReload = true;

                that.setNewData(data, bGrowing);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching timesheet"));
            };

            var selectedTabKey = that.getProperty("/view/selectedTabKey");
            var tabInfo = that.tabs[selectedTabKey];
            tabInfo.needsReload = true;
            this.setBusy(true);

            var sysTables = this._component.customizingModel.model.getProperty("/getSysTables");
            var timesheetQueries = [];

            sysTables.forEach(function (sysTable) {

                if (sysTable.table_name === 'CATSHEADER') {
                    timesheetQueries.push("catsQuery");
                }

                if (sysTable.table_name === 'TIMECONFIRMATION') {
                    timesheetQueries.push("tcQuery");
                }

            });

            that._requestHelper.getData(timesheetQueries,
                that.getTimesheetQueryParams(bGrowing ? this.getProperty("/timesheet").length + 1 : 1),
                load_success,
                load_error,
                true,
                tabInfo.actionName);

        };

        TimesheetListViewModel.prototype.fetchCounts = function () {
            var that = this;
            var load_success = function (data) {

                var dayCounter = parseInt(data.TimesheetDayCount[0].COUNT) + parseInt(data.CatsDayCount[0].COUNT);
                var weekCounter = parseInt(data.TimesheetWeekCount[0].COUNT) + parseInt(data.CatsWeekCount[0].COUNT);
                var monthCounter = parseInt(data.TimesheetMonthCount[0].COUNT) + parseInt(data.CatsMonthCount[0].COUNT);

                that.setProperty("/counts/day", dayCounter);
                that.setProperty("/counts/week", weekCounter);
                that.setProperty("/counts/month", monthCounter);
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching tab counts"));
            };

            this._requestHelper.getData(["TimesheetDayCount", "TimesheetWeekCount", "TimesheetMonthCount", "CatsDayCount", "CatsWeekCount", "CatsMonthCount"], {
                persNo: this.getUserInformation().personnelNumber,
                timePickerMonth: this.getProperty("/timePickerMonth"),
                timePickerYear: this.getProperty("/timePickerYear"),
                timePickerWeek: this.getProperty("/timePickerWeek"),
                timePickerDay: this.getProperty("/timePickerDay")
            }, load_success, load_error, true);
        };

        TimesheetListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        TimesheetListViewModel.prototype.resetModel = function () {
            this._needsReload = true;
        };

        TimesheetListViewModel.prototype.handleDatePickerChange = function (successCb) {

            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var selectedDatePicker = this.getDatePicker()._lastValue;

            var timePickerYear = moment(selectedDatePicker).format("YYYY");
            if (timePickerYear === 'Invalid date') {
                timePickerYear = moment(new Date()).format("YYYY");
            }

            this.setProperty("/timePickerYear", timePickerYear);

            if (selectedTabKey === 'month') {
                var timePickerMonth = moment(selectedDatePicker).format("MM");
                if (timePickerMonth === 'Invalid date') {
                    timePickerMonth = moment(new Date()).format("MM");
                }
                this.setProperty("/timePickerMonth", timePickerMonth);
            } else if (selectedTabKey === 'week') {
                var timePickerWeek = moment(selectedDatePicker).format("ww");
                if (timePickerWeek === 'Invalid date') {
                    timePickerWeek = moment(new Date()).format("ww");
                }
                this.setProperty("/timePickerWeek", timePickerWeek);
            } else {
                var timePickerDay = moment(selectedDatePicker).format("DD");
                if (timePickerDay === 'Invalid date') {
                    timePickerDay = moment(new Date()).format("DD");
                }
                this.setProperty("/timePickerDay", timePickerDay);
            }

            this.refreshData(successCb);
            this.fetchCounts();
        };

        TimesheetListViewModel.prototype.deleteCatsEntry = function (catsEntry, successCb) {
            var that = this;
            var sqlStrings = [];
            sqlStrings = sqlStrings.concat(catsEntry.delete(true));
            //sqlStrings.push(this.getOrderSQLChangeString());

            var onSuccess = function () {
                that.removeFromModelPath(catsEntry, "/timesheet");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error("Error while trying to delete component"));
            });

            this.fetchCounts();
        };

        TimesheetListViewModel.prototype.deleteTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var sqlStrings = [];
            sqlStrings = sqlStrings.concat(timeEntry.delete(true));
            //sqlStrings.push(this.getOrderSQLChangeString());

            var onSuccess = function () {
                that.removeFromModelPath(timeEntry, "/timesheet");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error("Error while trying to delete component"));
            });
        };

        TimesheetListViewModel.prototype.insertNewCatsEntry = function (catsEntry, successCb) {

            var that = this;
            var sqlStrings = [];
            catsEntry.RAUFNR = catsEntry.ORDERID;
            catsEntry.ORDER_NO = catsEntry.ORDERID;
            catsEntry.ACT_TYPE = catsEntry.LSTAR;
            catsEntry.UN_WORK = catsEntry.MEINH;
            catsEntry.ACT_WORK = catsEntry.CATSHOURS;

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
                that.refresh();
                successCb(catsEntry);
            };

            try {
                catsEntry.insert(false, function (mobileId) {
                    catsEntry.MOBILE_ID = mobileId;
                    onSuccess();

                }, onError.bind(this, new Error("Error while trying to insert new component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }

            this.fetchCounts();
        };


        TimesheetListViewModel.prototype.constructor = TimesheetListViewModel;

        return TimesheetListViewModel;
    });
