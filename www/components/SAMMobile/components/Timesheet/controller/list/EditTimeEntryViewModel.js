sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/TimeConfirmation",
        "SAMMobile/models/dataObjects/CatsHeader"],


    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, TimeConfirmation, CatsHeader) {
        "use strict";

        // constructor
        function EditTimeEntryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditTimeEntryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        timeEntry: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setTimeEntry: {
                value: function (entry) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = entry.getCopy();

                    }

                    this.setProperty("/timeEntry", entry);
                }
            },

            getTimeEntry: {
                value: function () {
                    return this.getProperty("/timeEntry");
                }
            },

            setColleagues: {
                value: function (colleagues) {
                    this.setProperty("/colleagues", colleagues);
                }
            },

            getColleagues: {
                value: function () {
                    return this.getProperty("/colleagues");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getTimeEntry().setData(this._objectDataCopy);
                }
            },


            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("EditTimeEntry") : this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("NewTimeEntry");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditTimeEntryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditTimeEntryViewModel.prototype.setHours = function (newHours) {
            this.getTimeEntry().setHours(newHours);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.setStartTime = function (newStartTime) {
            this.getTimeEntry().setStartTime(newStartTime);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.setEndTime = function (newEndTime) {
            this.getTimeEntry().setEndTime(newEndTime);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            // TODO do correct validation on new time entry
            validator.check("timeEntry.PERS_NO", "Please select a User").isEmpty();
            //validator.check("timeEntry.ACT_TYPE", "Please select an Activity Type").isEmpty();
            //validator.check("timeEntry.AWART", "Please select an Activity Type").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditTimeEntryViewModel.prototype.setStartTime = function (newStartTime) {
            this.getTimeEntry().setStartTime(newStartTime);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.setEndTime = function (newEndTime) {
            this.getTimeEntry().setEndTime(newEndTime);
            this.refresh();
        };

        // Select Dialogs
        EditTimeEntryViewModel.prototype.displayTeamMemberSelectDialog = function (oEvent, viewContext) {
            if (!this._oTeamMemberSelectDialog) {
                this._oTeamMemberSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.TeamMemberSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oTeamMemberSelectDialog.display(oEvent, '', null, new JSONModel(this.getColleagues()));
        };

        EditTimeEntryViewModel.prototype.handleTeamMemberSelect = function (oEvent) {
            this._oTeamMemberSelectDialog.select(oEvent, 'persNo', [{
                'PERS_NO': 'persNo'
            }], this, "/timeEntry");
        };

        EditTimeEntryViewModel.prototype.handleTeamMemberSearch = function (oEvent) {
            this._oTeamMemberSelectDialog.search(oEvent, ["firstName", "lastName", "persNo"]);
        };

        EditTimeEntryViewModel.prototype.displayActTypeDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oActTypeSelectDialog) {
                this._oActTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.ActivityTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oActTypeSelectDialog.display(oEvent, 'ActivityTypeMl');
        };

        EditTimeEntryViewModel.prototype.handleActTypeSelect = function (oEvent) {
            this._oActTypeSelectDialog.select(oEvent, 'LSTAR', [{
                'ACT_TYPE': 'LSTAR'
            }], this, "/timeEntry");


        };

        EditTimeEntryViewModel.prototype.handleActTypeSearch = function (oEvent) {
            this._oActTypeSelectDialog.search(oEvent, ["LSTAR", "KTEXT"]);
        };

//      EditTimeEntryViewModel.prototype.displayAttendanceTypeDialog = function(oEvent, viewContext) {
//         var that = this;
//         if (!this._oAttTypeSelectDialog) {
//            this._oAttTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.AttendanceTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
//         }
//      
//         // TODO add correct filtering
//         this._oAttTypeSelectDialog.display(oEvent, 'AttendanceTypes');
//      };
//   
//      EditTimeEntryViewModel.prototype.handleAttendanceTypeSelect = function(oEvent) {
//         this._oAttTypeSelectDialog.select(oEvent, 'SUBTY', [{
//            'AWART': 'SUBTY'
//         }], this, "/timeEntry");
//      
//
//      };
//   
//      EditTimeEntryViewModel.prototype.handleAttendanceTypeSearch = function(oEvent) {
//         this._oAttTypeSelectDialog.search(oEvent, ["ATEXT", "SUBTY"]);
//      };
//      
        EditTimeEntryViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    PERS_NO: sap.ui.core.ValueState.None,
                    START_DATE: sap.ui.core.ValueState.None,
                    START_TIME: sap.ui.core.ValueState.None,
                    END_DATE: sap.ui.core.ValueState.None,
                    END_TIME: sap.ui.core.ValueState.None,
                    ACT_TYPE: sap.ui.core.ValueState.None,
                    AWART: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditTimeEntryViewModel.prototype.updateTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                timeEntry.update(false, onSuccess, that.executeErrorCallback.bind(null, new Error("Error while trying to update component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EditTimeEntryViewModel.prototype.insertNewTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var sqlStrings = [];

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
                that.refresh();
                successCb(timeEntry);
            };


            try {
                timeEntry.insert(false, function (mobileId) {
                    timeEntry.MOBILE_ID = mobileId;
                    onSuccess();

                }, onError.bind(this, new Error("Error while trying to insert new component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };


        EditTimeEntryViewModel.prototype.constructor = EditTimeEntryViewModel;

        return EditTimeEntryViewModel;
    });
