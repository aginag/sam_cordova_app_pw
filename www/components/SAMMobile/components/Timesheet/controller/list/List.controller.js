sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Timesheet/controller/list/TimesheetListViewModel",
    "SAMMobile/components/Timesheet/controller/list/EditTimeEntryViewModel",
    "SAMMobile/components/Timesheet/controller/list/EditCatsEntryViewModel",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "sap/ui/model/Filter",
    "SAMMobile/models/dataObjects/TimeConfirmation",
    "SAMMobile/models/dataObjects/CatsHeader",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/MessageToast",
    "sap/m/Text"
], function (Controller, TimesheetListViewModel, EditTimeEntryViewModel, EditCatsEntryViewModel, MessageBox, formatter, Filter, TimeConfirmation, CatsHeader, Dialog, Button, MessageToast, Text) {
    "use strict";

    return Controller.extend("SAMMobile.components.Timesheet.controller.list.List", {
        formatter: formatter,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("timesheetList").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("timesheetList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.timesheetListViewModel = new TimesheetListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.timesheetListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.timesheetListViewModel.setList(this.byId("timesheetList"));
            this.timesheetListViewModel.setDatePicker(this.byId("datePicker"));

            this.initialized = false;
            this.getView().setModel(this.timesheetListViewModel, "timesheetListViewModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;

            var onMasterDataLoaded = function (routeName) {

                if (this.timesheetListViewModel.needsReload()) {
                    this.loadObjects();
                }

                if (!this.initialized) {
                    this.initialized = true;
                }
            };

            this.globalViewModel.setComponentHeaderText(this.timesheetListViewModel.getHeaderText());
            this.globalViewModel.setComponentBackNavEnabled(false);

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function () {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadObjects: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            var onReloadSuccess = function () {

            };

            this.timesheetListViewModel.fetchCounts();

            if (bSync) {
                this.timesheetListViewModel.refreshData();
            } else {
                this.timesheetListViewModel.fetchData(onReloadSuccess.bind(this));
            }
        },

        loadMasterData: function (successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {

        },

        onLogout: function () {
            this.initialized = false;
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        onTabSelect: function (oEvent) {
            this.timesheetListViewModel.onTabChanged();
        },

        onNavToDetailPress: function (oEvt) {

            var bindingContext = oEvt.getSource().getBindingContext("timesheetListViewModel");
            var timesheet = bindingContext.getObject();

            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "timesheetList",
                params: {}
            }, {
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: timesheet.ORDER_NO,
                            persNo: this.timesheetListViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            });

        },


        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("timesheetList", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        handleDatePickerChange: function () {
            this.timesheetListViewModel.handleDatePickerChange();
        },

        onTableSearch: function (oEvt) {
            this.timesheetListViewModel.onSearch(oEvt);
        },

        onListGrowing: function (oEvt) {
            this.timesheetListViewModel.onListGrowing(oEvt);
        },

        onEditTimeEntryPressed: function (oEvent) {
            var that = this;
            var timeEntry = oEvent.getSource().getBindingContext("timesheetListViewModel").getObject();

            if (timeEntry instanceof TimeConfirmation) {
                this.editTimeConfirmation(timeEntry);
            } else if (timeEntry instanceof CatsHeader) {
                this.editCatsHeader(timeEntry);
            }
        },

        onDeleteTimeEntryPressed: function (oEvent) {
            var that = this;
            var timeEntry = oEvent.getSource().getBindingContext("timesheetListViewModel").getObject();

            if (timeEntry instanceof TimeConfirmation) {
                that._getConfirmActionDialog(function () {
                    that.timesheetListViewModel.deleteTimeEntry(timeEntry, function () {
                        MessageToast.show("Component deleted successfully");
                    });
                }).open();

            } else if (timeEntry instanceof CatsHeader) {
                this._getConfirmActionDialog(function () {
                    that.timesheetListViewModel.deleteCatsEntry(timeEntry, function () {
                        MessageToast.show("Component deleted successfully");
                    });
                }).open();
            }

        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',

                content: new Text({
                    text: this.component.i18n.getText("deleteTimeEntryConfirmation"),
                    textAlign: 'Center',
                    width: '100%'
                }),

                beginButton: new Button({
                    text: this.component.i18n_core.getText("Yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("No"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }

            });

            return _oConfirmationDialog;
        },


        onHoursChanged: function (oEvent) {
            this.editTimeEntryViewModel.setHours(oEvent.getParameter("value"));
        },

        onHoursChangedCats: function (oEvent) {
            this.catsEntryEditViewModel.setHours(oEvent.getParameter("value"));
        },

        editTimeConfirmation: function (timeEntry) {

            var that = this;
            var onAcceptPressed = function (timeEntry, dialog) {
                that.editTimeEntryViewModel.updateTimeEntry(timeEntry, function () {
                    dialog.close();
                    that.timesheetListViewModel.refresh();
                    MessageToast.show("Updated successfully");
                });
            };

            var openDialog = function (colleagues) {
                var dialog = that._getTimeEntryEditDialog(timeEntry, colleagues, true, onAcceptPressed);
                dialog.open();
            };

            openDialog([]);
        },

        onStartTimeCatsChanged: function(oEvent){
            this.catsEntryEditViewModel.setStartTime(oEvent.getParameter("value"));
        },

        onEndTimeCatsChanged: function(oEvent){
            this.catsEntryEditViewModel.setEndTime(oEvent.getParameter("value"));
        },

        onStartTimeentryChanged: function(oEvent){
            this.editTimeEntryViewModel.setStartTime(oEvent.getParameter("value"));
        },

        onEndTimeetryChanged: function(oEvent){
            this.editTimeEntryViewModel.setEndTime(oEvent.getParameter("value"));
        },

        editCatsHeader: function (catsEntry) {

            var that = this;
            var onAcceptPressed = function (catsEntry, dialog) {
                that.catsEntryEditViewModel.updateCatsEntry(catsEntry, function () {
                    dialog.close();
                    that.timesheetListViewModel.refresh();
                    MessageToast.show("Updated successfully");
                });
            };

            var openDialog = function (colleagues) {
                var dialog = that._getCatsEntryEditDialog(catsEntry, colleagues, true, onAcceptPressed);
                dialog.open();
            };

            openDialog([]);
        },


        onAddTimeEntryPressed: function (oEvent) {

            var timeModelData;
            var that = this;

            timeModelData = new CatsHeader(null, this.component.requestHelper, this);
            timeModelData.PERNR = this.timesheetListViewModel.getUserInformation().personnelNumber;

            var onAcceptPressed = function (timeModelData, dialog) {

                that.timesheetListViewModel.insertNewCatsEntry(timeModelData, function (newCatsEntry) {
                    that.timesheetListViewModel.insertToModelPath(newCatsEntry, "/timesheet");
                    dialog.close();
                    that.timesheetListViewModel.refresh();
                    MessageToast.show("Inserted successfully");
                });
            };

            var openDialog = function (colleagues) {
                var dialog = that._getCatsEntryEditDialog(timeModelData, colleagues, false, onAcceptPressed);
                dialog.open();
            };

            openDialog([]);
        },

        _getTimeEntryEditDialog: function (timeEntry, colleagues, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");

                if (editTimeEntryViewModel.validate()) {
                    acceptCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editTimeEntryViewModel = dialog.getModel("timeEntryModel");
                editTimeEntryViewModel.rollbackChanges();
                dialog.close();
            };

            this.editTimeEntryViewModel = new EditTimeEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTimeEntryViewModel.setEdit(bEdit);
            this.editTimeEntryViewModel.setTimeEntry(timeEntry);
            this.editTimeEntryViewModel.setColleagues(colleagues);

            if (!this._timeEntryEditPopup) {

                if (!this._timeEntryPopover) {
                    this._timeEntryPopover = sap.ui.xmlfragment("editTimeEntryPopover1", "SAMMobile.components.Timesheet.view.list.TimeEntryEditPopover", this);
                }

                this._timeEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._timeEntryPopover,
                    beginButton: new Button({
                        text: '{= ${timeEntryModel>/view/edit} ? ${i18n_core>update} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {

                    }
                });

                this._timeEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._timeEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._timeEntryEditPopup.setTitle(this.editTimeEntryViewModel.getDialogTitle());
            this._timeEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._timeEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel, "timeEntryModel");
            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel.getValueStateModel(), "valueStateModel");

            return this._timeEntryEditPopup;
        },

        _getCatsEntryEditDialog: function (catsEntry, colleagues, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var editCatsEntryViewModel = this.getParent().getModel("catsEntryModel");

                if (editCatsEntryViewModel.validate()) {
                    acceptCb(editCatsEntryViewModel.getCatsEntry(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editCatsEntryViewModel = dialog.getModel("catsEntryModel");
                editCatsEntryViewModel.rollbackChanges();
                dialog.close();
            };

            this.catsEntryEditViewModel = new EditCatsEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.catsEntryEditViewModel.setEdit(bEdit);
            this.catsEntryEditViewModel.setCatsEntry(catsEntry);
            this.catsEntryEditViewModel.setColleagues(colleagues);

            if (!this._catsEntryEditPopup) {

                if (!this._catsEntryEditPopover) {
                    this._catsEntryEditPopover = sap.ui.xmlfragment("editCatsEntryPopover", "SAMMobile.components.Timesheet.view.list.CatsEntryEditPopover", this);
                }

                this._catsEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._catsEntryEditPopover,
                    beginButton: new Button({
                        text: '{= ${catsEntryModel>/view/edit} ? ${i18n_core>update} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {

                    }
                });

                this._catsEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._catsEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._catsEntryEditPopup.setTitle(this.catsEntryEditViewModel.getDialogTitle());
            this._catsEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._catsEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._catsEntryEditPopup.setModel(this.catsEntryEditViewModel, "catsEntryModel");
            this._catsEntryEditPopup.setModel(this.catsEntryEditViewModel.getValueStateModel(), "valueStateModel");

            return this._catsEntryEditPopup;
        },

        formatUdbNumericToFloat: function (numeric) {
            return parseFloat(numeric);
        },

        displayOrderIdSelectDialog: function (oEvent) {
            this.catsEntryEditViewModel.displayOrderIdSelectDialog(oEvent, this);
        },

        handleOrderIdSelect: function (oEvent) {
            this.catsEntryEditViewModel.handleOrderIdSelect(oEvent);
        },

        handleOrderIdSearch: function (oEvent) {
            this.catsEntryEditViewModel.handleOrderIdSearch(oEvent);
        },

        displayActTypeSelectDialog: function (oEvent) {
            this.catsEntryEditViewModel.displayActTypeDialog(oEvent, this);
        },

        handleActTypeSelect: function (oEvent) {
            this.catsEntryEditViewModel.handleActTypeSelect(oEvent);
        },

        handleActTypeSearch: function (oEvent) {
            this.catsEntryEditViewModel.handleActTypeSearch(oEvent);
        },


    });

});