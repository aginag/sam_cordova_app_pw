sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/Timesheet/controller/calendar/CalendarViewModel",
    "sap/m/Button",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/Timesheet/helpers/formatterComp",
    "sap/ui/core/Fragment"
], function (Controller, CalendarViewModel, Button, Dialog, Text, MessageBox, MessageToast, formatter, formatterComp, Fragment) {
    "use strict";

    return Controller.extend("SAMMobile.components.TimeSheet.controller.calendar.Calendar", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("timesheetCalendar").attachPatternMatched(this._onCustomRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("timesheetCalendar", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.calendarViewModel = new CalendarViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.calendarViewModel.setErrorCallback(this.onViewModelError.bind(this));
            // per default, the JSON model is limited to 100 entries.
            // Users may have more times recorded in a month than this. See https://msc-mobile.atlassian.net/browse/PFAL-852
            this.calendarViewModel.setSizeLimit(1000);

            this.setCalenderPlanningViews();
            this.calendarStyleElement = this.getView().byId("calendarStyleElement");

            this.getView().setModel(this.calendarViewModel, "calendarViewModel");
            this.getView().setModel(this.calendarViewModel.getValueStateModel(), "valueStateModel");
        },

        onAfterRendering: function(){
            var oCalendarGrid = this.getView().byId("SPC1").getAggregation("_grid");
            oCalendarGrid.addEventDelegate({
                onAfterRendering: function(){
                    var oFullDayRow = this.$().find('.sapMSinglePCBlockersRow');
                    var oGrid = this.getParent().$().find('.sapMSinglePCGrid');

                    if (oFullDayRow[0] && oGrid[0]) {
                        $(oFullDayRow[0]).appendTo(oGrid[0]);
                    }
                }
            }, oCalendarGrid);

            this.loadCalendar();
            this.calendarViewModel.refresh();
        },

        _onCustomRouteMatched: function (oEvent) {
            const routeName = oEvent.getParameter('name');

            this.globalViewModel.setComponentBackNavEnabled(false);
            this.globalViewModel.setCurrentRoute(routeName);
            this.globalViewModel.setComponentHeaderText(this.component.i18n.getText("calendar"));
            this.calendarViewModel.resetValueStateModel();

            if (this.calendarViewModel.needsReload()) {
                this.loadCalendar();
            }

        },

        onRefreshRoute: function () {
            this.loadCalendar(true);
        },

        onExit: function() {
            sap.ui.getCore().getEventBus().unsubscribe("timeSheetCalendar", "refreshRoute", this.onRefreshRoute, this);
            if (this._oPopover) {
                this._oPopover.destroy();
            }
        },

        setCalenderPlanningViews: function(){
            this.calendarControl = this.byId("SPC1");

            var oDayView = new sap.m.SinglePlanningCalendarDayView({
                title: this.component.i18n.getText("CALENDAR_DAY_BUTTON_TEXT"),
                key: "DayView"
            });
            var oWorkWeekView = new sap.m.SinglePlanningCalendarWorkWeekView({
                key: "WorkWeekView",
                title:  this.component.i18n.getText("CALENDAR_WORK_WEEK_BUTTON_TEXT")
            });
            var oWeekView = new sap.m.SinglePlanningCalendarWeekView({
                key: "WeekView",
                title:  this.component.i18n.getText("CALENDAR_WEEK_BUTTON_TEXT")
            });
            if(sap.ui.Device.system.phone){
                this.calendarControl.addView(oDayView);
            } else {
                this.calendarControl.addView(oWeekView);
            }
        },

        loadCalendar: function (bSync) {
            const that = this;
            bSync = bSync == undefined ? false : bSync;

            if (bSync) {
                this.calendarViewModel.refreshData(function (successCb) {
                    that.markExtraPayDays();
                });
            } else {
                this.calendarViewModel.fetchData(function (successCb) {
                    that.markExtraPayDays();
                });
            }
        },

        markExtraPayDays: function() {
            const extraPayDays = this.calendarViewModel.getProperty("/wagesData");
            const calendarStyleDomElement = $("#" + this.calendarStyleElement.getId());
            const isPhone = sap.ui.Device.system.phone;
            const calendarControlId = this.getView().byId("SPC1").getId();

            const styleClasses = extraPayDays.map(function (payDay) {
                var formattedDate = moment(payDay).format("YYYYMMDD");
                return "#" + calendarControlId + " div[data-sap-day='" + formattedDate + "'] {background-color: lightgreen;}";
            });

            if (isPhone) {
                var currentDate = this.getView().byId("SPC1").getStartDate();
                var formattedCurrentDate = moment(currentDate).locale('de').format("DD. MMMM YYYY");
                extraPayDays.forEach(function (payDay) {
                    var formattedDate = moment(payDay).locale('de').format("DD. MMMM YYYY");
                    if (formattedDate === formattedCurrentDate) {
                        styleClasses.push("button[data-sap-ui='" + calendarControlId + "-Header-NavToolbar-PickerBtn']" + "{background-color: lightgreen;}");
                    }
                });
            }
            calendarStyleDomElement[0].textContent = styleClasses.join(" ");
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        toggleFullDay: function () {
            this.calendarViewModel.setProperty("/view/fullDay", !this.calendarViewModel.getProperty("/view/fullDay"));
        },

        handleAppointmentSelect: function (oEvent) {
            var oAppointment = oEvent.getParameter("appointment");
            if (!oAppointment) return;
            
            var appointmentObject = oAppointment.getBindingContext("calendarViewModel").getObject();
            this.calendarViewModel.setProperty("/selectedAppointment", appointmentObject);

            if (oAppointment === undefined) {
                return;
            }

            if (!oAppointment.getSelected()) {
                return;
            }

            var dialog = this._getAppointmnetDetailDialog();
            dialog.open();

        },

        _getAppointmnetDetailDialog: function(){
            const that = this;

            var onCancelPressed = function () {
                var dialog = this.getParent();

                var selectedAppointments = that.getView().byId("SPC1").getSelectedAppointments();
                selectedAppointments.forEach(function(appointment) {
                    appointment.setSelected(false);
                });

                dialog.close();
            };

            if (!this._appointmentDetailPopup) {
                if (!this._appointmentDetailPopover) {
                    this._appointmentDetailPopover = sap.ui.xmlfragment("SAMMobile.components.Timesheet.view.calendar.AppointmentDetails", this);
                }

                this._appointmentDetailPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._appointmentDetailPopover,
                    contentWidth: "30%",
                    endButton: new Button({
                        text: '{i18n_core>close}',
                        press: onCancelPressed
                    })
                });

                this._appointmentDetailPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._appointmentDetailPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }
            this._appointmentDetailPopup.setTitle("Details");
            this._appointmentDetailPopup.setModel(this.calendarViewModel, "calendarViewModel");
            return this._appointmentDetailPopup;
        },

        onShowTimeRecordingDetailsPressed: function (oEvent) {
            var oButton = oEvent.getSource(),
                dStartDate = oEvent.getSource().getParent().getProperty("startDate");
            this.calendarViewModel.calculateTimeConfirmationWeekSummary(dStartDate);
            if (!this._oPopover) {
                Fragment.load({
                    name: "SAMMobile.components.Timesheet.view.calendar.CalendarTimeOverview",
                    controller: this
                }).then(function(oPopover){
                    this._oPopover = oPopover;
                    this.getView().addDependent(this._oPopover);
                    this._oPopover.openBy(oButton);
                }.bind(this));
            } else {
                this._oPopover.openBy(oButton);
            }
        },

        onStartDayChanged: function(oEvent){
            var currentDate = moment(oEvent.getParameters().date).locale('de').format("DD. MMMM YYYY");
            const extraPayDays = this.calendarViewModel.getProperty("/wagesData");
            const calendarStyleDomElement = $("#" + this.calendarStyleElement.getId());
            const isPhone = sap.ui.Device.system.phone;
            const calendarControlId = this.getView().byId("SPC1").getId();

            const styleClasses = extraPayDays.map(function (payDay) {
                var formattedDate = moment(payDay).format("YYYYMMDD");
                return "#" + calendarControlId + " div[data-sap-day='" + formattedDate + "'] {background-color: lightgreen;}";
            });
            if (isPhone) {
                extraPayDays.forEach(function (payDay) {
                    var formattedDate = moment(payDay).locale('de').format("DD. MMMM YYYY");
                    if (formattedDate === currentDate) {
                        styleClasses.push("button[data-sap-ui='" + calendarControlId + "-Header-NavToolbar-PickerBtn']" + "{background-color: lightgreen;}");
                    }
                });
            }
            calendarStyleDomElement[0].textContent = styleClasses.join(" ");
        },

        onWorkingHoursEntered: function (oEvent) {
            var extraPayObject = oEvent.getSource().getBindingContext("calendarViewModel").getObject();
            var amount = oEvent.getParameter("value");
            amount =  amount.replace(',', '.');
            amount = parseFloat(amount).toFixed(2).toString();

            extraPayObject.AMOUNT = amount;
            extraPayObject.changed = true;
            this.calendarViewModel.validate(extraPayObject);
        },

        onTypeEntered: function (oEvent) {
            var typeObject = oEvent.getSource().getBindingContext("calendarViewModel").getObject();
            var type = oEvent.getParameters().selectedItem.mProperties.key;
            typeObject.WGTYPE = type;
            typeObject.changed = true;
            this.calendarViewModel.validateType(typeObject);
        },

        onCaptureSpecialDaysPressed: function (oEvent) {
            var that = this;
            var curr = oEvent.getSource().getParent().getProperty("startDate");
            var wStartDate = this.calendarViewModel.setToMonday(curr);
            this.calendarViewModel.displayWeekDays(wStartDate);
            this.calendarViewModel.typeArray = oEvent.getSource().getParent().getModel("calendarViewModel").getData().extraPayType;
            this.calendarViewModel.hoursArray = oEvent.getSource().getParent().getModel("calendarViewModel").getData().extraPayHours;

            this.calendarViewModel.loadAllExtraPayData().then(function (loadedExtraPayData) {
                var wageTypeData = loadedExtraPayData.filter(function (data) {
                    return data.WGTYPE !== "2340";
                });
                var wageHoursData = loadedExtraPayData.filter(function (data) {
                    return data.WGTYPE === "2340";
                });
                wageTypeData.forEach(function (loadedExtraPayObject) {
                    var loadedDate = moment(loadedExtraPayObject.EXEC_DAT).format("YYYY-MM-DD");
                    that.calendarViewModel.typeArray.forEach(function (currentTypeObject) {
                        var currentDate = moment(currentTypeObject.EXEC_DAT).format("YYYY-MM-DD");
                        if (currentDate === loadedDate) {
                            currentTypeObject.AMOUNT = loadedExtraPayObject.AMOUNT;
                            currentTypeObject.APPROVED = loadedExtraPayObject.APPROVED;
                            currentTypeObject.WGTYPE = loadedExtraPayObject.WGTYPE;
                            currentTypeObject.MOBILE_ID = loadedExtraPayObject.MOBILE_ID;
                            currentTypeObject.ACTION_FLAG = loadedExtraPayObject.ACTION_FLAG;
                            currentTypeObject.APPROVED = loadedExtraPayObject.APPROVED;
                            currentTypeObject.CRE_DAT = loadedExtraPayObject.CRE_DAT;
                            currentTypeObject.PERS_NO = loadedExtraPayObject.PERS_NO;
                            currentTypeObject.UNIT = loadedExtraPayObject.UNIT;
                            currentTypeObject.OLDAMOUNT = loadedExtraPayObject.AMOUNT;
                            currentTypeObject.OLDWGTYPE = loadedExtraPayObject.WGTYPE;
                            currentTypeObject.UPDATE = true;
                        }
                    });
                });
                wageHoursData.forEach(function (loadedExtraPayObject) {
                    var loadedDate = moment(loadedExtraPayObject.EXEC_DAT).format("YYYY-MM-DD");
                    that.calendarViewModel.hoursArray.forEach(function (currentHoursObject) {
                        var currentDate = moment(currentHoursObject.EXEC_DAT).format("YYYY-MM-DD");
                        if (currentDate === loadedDate) {
                            currentHoursObject.AMOUNT = parseFloat(loadedExtraPayObject.AMOUNT).toFixed(2).toString();
                            currentHoursObject.APPROVED = loadedExtraPayObject.APPROVED;
                            currentHoursObject.WGTYPE = loadedExtraPayObject.WGTYPE;
                            currentHoursObject.MOBILE_ID = loadedExtraPayObject.MOBILE_ID;
                            currentHoursObject.ACTION_FLAG = loadedExtraPayObject.ACTION_FLAG;
                            currentHoursObject.APPROVED = loadedExtraPayObject.APPROVED;
                            currentHoursObject.CRE_DAT = loadedExtraPayObject.CRE_DAT;
                            currentHoursObject.PERS_NO = loadedExtraPayObject.PERS_NO;
                            currentHoursObject.UNIT = loadedExtraPayObject.UNIT;
                            currentHoursObject.OLDAMOUNT = loadedExtraPayObject.AMOUNT;
                            currentHoursObject.OLDWGTYPE = loadedExtraPayObject.WGTYPE;
                            currentHoursObject.UPDATE = true;
                        }
                    });
                });
                that.calendarViewModel.refresh();
                that.calendarViewModel.resetValueStateModel();

                var dialog = that._getEditDialog(that.calendarViewModel.typeArray, that.calendarViewModel.hoursArray);
                dialog.open();
                that.calendarViewModel.resolve();
            }.bind(that)).catch(this.calendarViewModel.reject);
        },

        deleteExtraPayTypeEntry: function (oEvent, successCb) {
            var that = this;
            var deleteObject = oEvent.getSource().getParent().getBindingContext("calendarViewModel").getObject();

            if (deleteObject.MOBILE_ID) {
                that.calendarViewModel.deleteExtraPayData(deleteObject, function () {
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("EXTRA_PAY_DELETE_ENTRY_SUCCESS_TEXT"));
                });
            } else  {
                deleteObject.AMOUNT = "";
                deleteObject.WGTYPE = "";
            }
            this.loadCalendar();
            that.calendarViewModel.refresh();
        },

        deleteExtraPayHoursEntry: function (oEvent, successCb) {
            var that = this;
            var deleteObject = oEvent.getSource().getParent().getBindingContext("calendarViewModel").getObject();

            if (deleteObject.MOBILE_ID) {
                that.calendarViewModel.deleteExtraPayData(deleteObject, function () {
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("EXTRA_PAY_DELETE_ENTRY_SUCCESS_TEXT"));
                });
            } else  {
                deleteObject.AMOUNT = "";
                deleteObject.WGTYPE = "";
            }
            this.loadCalendar();
            that.calendarViewModel.refresh();
        },

        _getEditDialog: function (typeArray, hoursArray) {
            var that = this;
            var onSavePressed = function (oEvent) {
                var extraPayType = oEvent.getSource().getParent().getModel("calendarViewModel").getData().extraPayType;
                var extraPayHours = oEvent.getSource().getParent().getModel("calendarViewModel").getData().extraPayHours;
                that.calendarViewModel.errorsExtraPayValidation = [];
                var filteredExtraPayType = extraPayType.filter(function (data) {
                    return data.WGTYPE;
                });
                var filteredExtraPayHours = extraPayHours.filter(function (data) {
                    return data.AMOUNT;
                });
                filteredExtraPayType.forEach(function (typeObject) {
                    typeObject.PERS_NO = that.calendarViewModel.getUserInformation().personnelNumber;
                    typeObject.UNIT = that.component.i18n.getText("WAGE_TYPE_UNIT");
                    typeObject.AMOUNT = "1.00";
                    if (typeObject.UPDATE) {
                        if (parseFloat(typeObject.WGTYPE).toFixed(2) !== typeObject.OLDWGTYPE) {
                            if(typeObject.changed) {
                                that.calendarViewModel.updateExtraPayData(typeObject);
                                MessageToast.show(that.component.i18n.getText("UPDATED_SUCCESSFULLY"));
                            }
                        }
                    } else {
                        that.calendarViewModel.insertExtraPayData(typeObject);
                        MessageToast.show(that.component.i18n.getText("INSERTED_SUCCESSFULLY"));
                    }
                });
                filteredExtraPayHours.forEach(function (hoursObject) {
                    if(that.calendarViewModel.validate(hoursObject)){
                    hoursObject.PERS_NO = that.calendarViewModel.getUserInformation().personnelNumber;
                    hoursObject.UNIT = that.component.i18n.getText("WAGE_HOURS_UNIT");
                    hoursObject.WGTYPE = "2340";
                    if (hoursObject.UPDATE) {
                        if(hoursObject.changed) {
                        if (parseFloat(hoursObject.AMOUNT).toFixed(2) !== hoursObject.OLDAMOUNT) {
                            that.calendarViewModel.updateExtraPayData(hoursObject);
                            MessageToast.show(that.component.i18n.getText("UPDATED_SUCCESSFULLY"));
                        }
                        }
                    } else {
                        that.calendarViewModel.insertExtraPayData(hoursObject);
                        MessageToast.show(that.component.i18n.getText("INSERTED_SUCCESSFULLY"));
                    }
                    }
                });

                that.calendarViewModel.refresh();
                var errorSet = false;
                that.calendarViewModel.errorsExtraPayValidation.forEach(function(isError){
                    if(isError){
                        errorSet = true;
                    }
                });
                if(!errorSet){
                    oEvent.getSource().getParent().close();
                    that.loadCalendar();
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            if (!this._specialCaseEditPopup) {
                if (!this._specialCaseEditPopover) {
                    this._specialCaseEditPopover = sap.ui.xmlfragment("SAMMobile.components.Timesheet.view.calendar.CalenderCaptureDays", this);
                }

                this._specialCaseEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._specialCaseEditPopover,
                    contentWidth: "70%",
                    beginButton: new Button({
                        text: '{i18n_core>save}',
                        press: onSavePressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                        //close
                    }
                });

                this._specialCaseEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._specialCaseEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }
            this._specialCaseEditPopup.setTitle(this.calendarViewModel.getDialogTitle());
            this._specialCaseEditPopup.setModel(this.calendarViewModel, "calendarViewModel");
            this._specialCaseEditPopup.setModel(this.getView().getModel("device"), "device");
            return this._specialCaseEditPopup;
        },
    });
});