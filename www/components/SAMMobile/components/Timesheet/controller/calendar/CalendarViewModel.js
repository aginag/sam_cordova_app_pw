sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/models/dataObjects/ExtraPay",
        "SAMMobile/helpers/timeHelper",
        "sap/m/MessageToast",
    ],

    function (JSONModel, ViewModel, Validator,  ExtraPay, TimeHelper, MessageToast) {
        "use strict";

        // constructor
        function CalendarViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
            this.errorsExtraPayValidation = [];
        }

        CalendarViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        extraPayEntries: [],
                        appointments: [],
                        selectedAppointment: null,
                        weekSummary: {
                            1: 0,   //Monday
                            2: 0,   // Tuesday
                            3: 0,   // Wednesday
                            4: 0,   // Thursday
                            5: 0,   // Friday
                            6: 0,   // Saturday
                            0: 0,   // Sunday
                            week: 0
                        },
                        view: {
                            busy: false,
                            fullDay: true,
                            errorMessages: []
                        },
                        errorsExtraPayValidation: []
                    };
                }
            },
            getCalendarQueryParams: {
                value: function () {
                    return {
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },
            getExtraPayType: {
                value: function () {
                    return this.getProperty("/extraPayType");
                }
            },
            getExtraPayHours: {
                value: function () {
                    return this.getProperty("/extraPayHours");
                }
            },
            setAppointments: {
                value: function (appointments) {
                    this.setProperty("/appointments", appointments);
                }
            },
            getAppointments: {
                value: function (bBusy) {
                    return this.getProperty("/appointments");
                }
            },
            getDialogTitle: {
                value: function () {
                    return this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("CAPTURE_SPECIAL_WORKING_CONDITIONS");
                }
            },
            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },
            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        CalendarViewModel.prototype.orderUserStatus = null;

        CalendarViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CalendarViewModel.prototype.setNewData = function (data) {
            var appointments = data.tcQuery.map(function (timeConf) {
                var startDate = moment(timeConf.START_DATE.split(" ").shift() + " " + timeConf.START_TIME.split(" ").pop());
                var endDate = moment(timeConf.END_DATE.split(" ").shift() + " " + timeConf.END_TIME.split(" ").pop());

                timeConf._START_DATE_TIMESTAMP = startDate.format("DD.MM.YYYY HH:mm:ss.SSS");
                timeConf._START_DATE = startDate.isValid() ? startDate.toDate() : null;
                timeConf._END_DATE = endDate.isValid() ? endDate.toDate() : null;

                return timeConf;
            }).filter(function (timeConf) {
                // Start and End Date can not be null,
                // otherwise UI5 calendar control will complain
                return timeConf._START_DATE && timeConf._END_DATE;
            });

            this.setAppointments(appointments);

            var extraPayExecDates = [];
            data.wagesData.filter(function (day) {
                extraPayExecDates.push(day.EXEC_DAT);
            });
            this.setProperty("/wagesData", extraPayExecDates);
        };

        CalendarViewModel.prototype.fetchData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                if (successCb != undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOAD_CALENDAR_DATA")));
            };

            this.setBusy(true);

            this._requestHelper.getData(["tcQuery", "wagesData"],
                this.getCalendarQueryParams(),
                load_success,
                load_error,
                true,
                "calendar");
        };

        CalendarViewModel.prototype.refreshData = function (successCb) {
            this.fetchData(successCb);
        };

        CalendarViewModel.prototype.calculateTimeConfirmationWeekSummary = function (dStartDate) {
            var oWeekSummary = {
                1: 0,   // Monday
                2: 0,   // Tuesday
                3: 0,   // Wednesday
                4: 0,   // Thursday
                5: 0,   // Friday
                6: 0,   // Saturday
                0: 0,   // Sunday
                week: 0
            };
            var monday = this.setToMonday(dStartDate);
            let curr = new Date(dStartDate);
            let week = []

            for (let i = 1; i <= 7; i++) {
                let first = monday.getDate() - monday.getDay() + i;
                let day = moment(new Date(monday.setDate(first))).format("YYYY-MM-DD");
                week.push(day);
            }

            dStartDate = new Date(week[0]);
            dStartDate.setHours(0);
            var dEndDate = new Date(week[6]);
            dEndDate.setHours(23,59);

            var aTimeConfirmationsInRange = this.getAppointments().filter(function (oAppointment) {
                return moment(oAppointment._START_DATE).isBetween(moment(dStartDate), moment(dEndDate)) || moment(oAppointment._END_DATE).isBetween(moment(dStartDate), moment(dEndDate));
            });
            aTimeConfirmationsInRange.forEach(function (oTimeConfirmation) {
                if (oTimeConfirmation._START_DATE.getDay() === oTimeConfirmation._END_DATE.getDay()) {
                    oWeekSummary[oTimeConfirmation._START_DATE.getDay()] += parseInt(oTimeConfirmation.ACT_WORK);
                } else {
                    var oBeginOfTime = moment("1970-01-01 00:00:00.000");
                    // Case 1
                    // 0            24
                    //           -----
                    if (moment(oTimeConfirmation._START_DATE).isBetween(moment(dStartDate), moment(dEndDate))) {
                        var oDiffTime = oTimeConfirmation.ACTION_FLAG === 'N' ? moment(oTimeConfirmation.START_DATE) : oBeginOfTime;
                        oWeekSummary[oTimeConfirmation._START_DATE.getDay()] += (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(oDiffTime)).asMinutes();
                    }

                    // Case 2
                    // 0            24
                    // -----
                    if (moment(oTimeConfirmation._END_DATE).isBetween(moment(dStartDate), moment(dEndDate))) {
                        var oDiffTime = oTimeConfirmation.ACTION_FLAG === 'N' ? moment(oTimeConfirmation.END_DATE) : oBeginOfTime;
                        oWeekSummary[oTimeConfirmation._END_DATE.getDay()] += moment.duration(moment(oTimeConfirmation.END_TIME).diff(oDiffTime)).asMinutes();
                    }
                }
            });

            oWeekSummary["week"] = oWeekSummary[1] + oWeekSummary[2] + oWeekSummary[3] + oWeekSummary[4] + oWeekSummary[5] + oWeekSummary[6] + oWeekSummary[0];

            this.setProperty("/weekSummary", oWeekSummary);
        };


        CalendarViewModel.prototype.setToMonday = function (date) {
            var day = date.getDay() || 7;
            if( day !== 1 )
                date.setHours(-24 * (day - 1));
            return date;
        };

        CalendarViewModel.prototype.displayWeekDays = function (wStartDate) {
            var oWeekExtraPayType = [];
            var oWeekExtraPayHours = [];

            for(var i=0; i<7; i++) {
                oWeekExtraPayType.push(this.getNewCapturedDataForType(i, wStartDate));
                oWeekExtraPayHours.push(this.getNewCapturedDataForHours(i, wStartDate));
            }

            this.setProperty("/extraPayType", oWeekExtraPayType);
            this.setProperty("/extraPayHours", oWeekExtraPayHours);
        };

        CalendarViewModel.prototype.getNewCapturedDataForType = function (iWeekDay, wStartDate) {
            var capturedDataType = new ExtraPay(null, this._requestHelper, this);

            capturedDataType.PERS_NO = this.getUserInformation().personnelNumber;
            capturedDataType.EXEC_DAT = moment(wStartDate).add(iWeekDay,'days').format("YYYY-MM-DD HH:mm:ss.SSS");
            capturedDataType.DAY_NAME = this.getWeekDay(iWeekDay);
            capturedDataType.UNIT = this._component.i18n.getText("WAGE_TYPE_UNIT");
            capturedDataType.AMOUNT = "1.00";

            if(iWeekDay === 5 || iWeekDay === 6){
                capturedDataType.WEEKEND = "WEEKEND";
            }

            return capturedDataType;
        };

        CalendarViewModel.prototype.getNewCapturedDataForHours = function (iWeekDay, wStartDate) {
            var capturedData = new ExtraPay(null, this._requestHelper, this);

            capturedData.PERS_NO = this.getUserInformation().personnelNumber;
            capturedData.EXEC_DAT = moment(wStartDate).add(iWeekDay,'days').format("YYYY-MM-DD HH:mm:ss.SSS");
            capturedData.DAY_NAME = this.getWeekDay(iWeekDay);
            capturedData.UNIT = this._component.i18n.getText("WAGE_HOURS_UNIT");
            capturedData.WGTYPE = "2340";

            return capturedData;
        };

        CalendarViewModel.prototype.deleteExtraPayData = function (deleteObject, successCb) {
            var that = this;
            var sqlStrings = [];
            var completeArray = [];
            var selectedObject = deleteObject.WGTYPE;
            if (selectedObject === "2340") {
                completeArray = this.getExtraPayHours();
            } else {
                completeArray = this.getExtraPayType();
            }

            var onSuccess = function (test) {
                var indexOfExtraPayObject = completeArray.indexOf(deleteObject);
                var newExtraPay = new ExtraPay(null, that._requestHelper, that);

                newExtraPay.EXEC_DAT = deleteObject.EXEC_DAT;
                newExtraPay.DAY_NAME = deleteObject.DAY_NAME;

                completeArray[indexOfExtraPayObject] = newExtraPay;

                if (successCb !== undefined) {
                    successCb();
                    that.refresh();
                }
            };
            sqlStrings = sqlStrings.concat(deleteObject.delete(true));

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_EXTRA_PAY_DELETE")));
            });
        };

        CalendarViewModel.prototype.updateExtraPayData = function (dateObject) {
            var that = this;
            var sqlStrings = [];
            var currentDate = TimeHelper.getCurrentDateObject(dateObject, this.getProperty("/extraPayEntries"), this.getLanguage());
            dateObject.CRE_DAT = currentDate._dateString;
            sqlStrings.push(dateObject.update(true));

            var onSuccess = function (data) {
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_EXTRA_PAY_UPDATE")));
            });
        };


        CalendarViewModel.prototype.insertExtraPayData = function (dateObject) {
            var that = this;
            var sqlStrings = [];
            var currentDate = TimeHelper.getCurrentDateObject(dateObject, this.getProperty("/extraPayEntries"), this.getLanguage());
            dateObject.CRE_DAT = currentDate._dateString;
            var onSuccess = function (data) {
            };

            sqlStrings.push(dateObject.insert(true));

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_EXTRA_PAY_FETCHING")));
            });
        };

        CalendarViewModel.prototype.validate = function(extraPayObject) {
            var that = this;
            var validator = new Validator(extraPayObject, this.getValueStateModel());

            extraPayObject.AMOUNT = extraPayObject.AMOUNT.replace(',', '.');

            var oMaxDate = moment();
            var oMinDate = moment().subtract(21, 'days');

            // Executed Date in Future
            validator.check("EXEC_DAT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_ERROR_START_TIME_IN_FUTURE")).custom(function (value) {
                return !(moment(value).toDate() >= oMaxDate.toDate());
            });

            // Executed Date more than 21 days in past
            validator.check("EXEC_DAT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_ERROR_START_TIME_IN_PAST")).custom(function (value) {
                return !(moment(value).toDate() < oMinDate.toDate());
            });

            // Today more than 4 day of month and Executed date in last month
            validator.check("EXEC_DAT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_ERROR_START_TIME_IN_LAST_MONTH")).custom(function (value) {
                return !(moment().date() > 4 && moment().month() > moment(value).month());
            });

            validator.check("AMOUNT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_NUMBER_VALUE")).custom(function (value) {
                return parseFloat(value) > 0 && parseFloat(value) <= 9;
            });

            validator.check("AMOUNT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_MUST_BE_QUARTER")).custom(function (value) {
                var decimalPart = "";
                if(value.includes(",")){
                    decimalPart = value.split(",")[1];
                } else if (value.includes(".")){
                    decimalPart = value.split(".")[1];
                }
                return decimalPart === "25" || decimalPart === "50" || decimalPart === "75" || decimalPart === "00";

            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);


            if (errors) {
                that.errorsExtraPayValidation.push(true);
                extraPayObject.setValueState(sap.ui.core.ValueState.Error);
                extraPayObject.AMOUNT = "";
            } else {
                extraPayObject.setValueState(sap.ui.core.ValueState.None);
            }

            that.refresh();
            return errors ? false : true;
        };

        CalendarViewModel.prototype.validateType = function(extraPayObject) {
            var that = this;
            var validator = new Validator(extraPayObject, this.getValueStateModel());


            var oMaxDate = moment();
            var oMinDate = moment().subtract(21, 'days');

            // Executed Date more than 21 days in past
            validator.check("EXEC_DAT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_ERROR_START_TIME_IN_PAST")).custom(function (value) {
                return !(moment(value).toDate() < oMinDate.toDate());
            });

            // Today more than 4 day of month and Executed date in last month
            validator.check("EXEC_DAT", that._component.i18n.getText("EXTRA_PAY_ENTRY_VALIDATION_ERROR_START_TIME_IN_LAST_MONTH")).custom(function (value) {
                return !(moment().date() > 4 && moment().month() > moment(value).month());
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            if (errors) {
            that.errorsExtraPayValidation.push(true);

            //extraPayObject.setValueState(sap.ui.core.ValueState.Error);
            extraPayObject.WGTYPE = "";
            } else {
                extraPayObject.setValueState(sap.ui.core.ValueState.None);
            }
            
            that.refresh();
            return errors ? false : true;
        };

        CalendarViewModel.prototype.loadExtraPayData = function (successCb) {
            var that = this;

            var onDataFetched = function (data) {
                var newExtraPayData = [];
                if(data[0]) {
                    data[0].extraPayData.forEach(function (extraPay) {
                        var object = new ExtraPay(extraPay, that._requestHelper, this);
                        newExtraPayData.push(object);
                    });
                }
                if (successCb !== undefined) {
                    successCb(newExtraPayData);
                }
            };
            var onError = function(err) {
                this.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_FUNCTIONAL_LOCATIONS_FETCHING")));
            };
            Promise.all([this._fetchExtraPayData()]).then(onDataFetched.bind(this)).catch(onError.bind(this));
        };

        CalendarViewModel.prototype.loadAllExtraPayData = function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadExtraPayData(resolve, reject);
            });
        };

        CalendarViewModel.prototype._fetchExtraPayData = function () {
            return this._requestHelper.fetch({
                id: "extraPayData",
                statement: "SELECT * FROM ZPMC_EXTRAPAY as ep " +
                    "WHERE ep.PERS_NO = '@persNo' ",
            }, {
                persNo: this.getUserInformation().personnelNumber
            });
        };

        CalendarViewModel.prototype.getWeekDay = function(iWeekDay) {
            var that = this;

            switch (iWeekDay) {
                case 0:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_MONDAY");
                case 1:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_TUESDAY");
                case 2:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_WEDNESDAY");
                case 3:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_THURSDAY");
                case 4:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_FRIDAY");
                case 5:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_SATURDAY");
                case 6:
                    return that._component.i18n.getText("CALENDAR_TIME_RECORDING_OVERVIEW_SUNDAY");
            }
        };

        CalendarViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    AMOUNT: sap.ui.core.ValueState.None,
                    WGTYPE: sap.ui.core.ValueState.None
                });
            }
            return this.valueStateModel;
        };

        CalendarViewModel.prototype.resetValueStateModel = function() {
            if (!this.valueStateModel) {
                return;
            }
            this.valueStateModel.setProperty("/AMOUNT", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/WGTYPE", sap.ui.core.ValueState.None);

            this.setProperty("/view/errorMessages", []);
        };

        CalendarViewModel.prototype.constructor = CalendarViewModel;

        return CalendarViewModel;
    }
);
