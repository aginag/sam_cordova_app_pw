sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "SAMMobile/models/dataObjects/Order",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/NotificationHeader"],

    function (ViewModel, Filter, Order, Formatter, NotificationHeader) {
        "use strict";

        // constructor
        function WorkOrderListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper, "orders");

            this.orderUserStatus = null;
            this._GROWING_DATA_PATH = "/orders";

            this._formatter = new Formatter(this._component);

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        WorkOrderListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        orders: [],
                        selectedOrder: null,
                        searchString: "",
                        counts: {
                            open: 0,
                            complete: 0,
                            rejected: 0
                        },
                        view: {
                            busy: false,
                            selectedTabKey: "open",
                            orderSelected: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold,
                            statusButtons: []
                        }
                    };
                }
            },

            getSelectedOrder: {
                value: function () {
                    return this.getProperty("/selectedOrder");
                }
            },

            getOrderQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber,
                        searchString: this.getSearchString(),
                        completeStatus: this.orderUserStatus.complete.status,
                        rejectedStatus: this.orderUserStatus.reject.status
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            setStatusButtonsForStatus: {
                value: function (status, statusProfile) {
                    // Check if status needs stop watch action
                    this.setProperty("/view/statusButtons", this.getStatusHierarchy().getOptionsForStatus(status, statusProfile));
                }
            },

            getOperationStatusProfileForSelectedOrder: {
                value: function (status) {
                    return this.getProperty("/selectedOrder").OPERATION_STATUS_PROFILE;
                }
            },

            getStatusHierarchy: {
              value: function() {
                  return this._component.statusHierarchy;
              }
            },

            getScenario: {
                value: function() {
                    return this._component.scenario;
                }
            },

            getStopwatchManager: {
                value: function() {
                    return this._component.stopwatchManager;
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function() {}, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            statusIsReject: {
                value: function (status) {
                    return status == this.orderUserStatus.reject.status;
                }
            },

            statusIsComplete: {
                value: function (status) {
                    return status == this.orderUserStatus.complete.status;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        WorkOrderListViewModel.prototype.orderUserStatus = null;

        WorkOrderListViewModel.prototype.tabs = {
            open: {
                actionName: "open",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/open", this.getProperty("/counts/open") + value);
                }
            },

            complete: {
                actionName: "complete",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/complete", this.getProperty("/counts/complete") + value);
                }
            },

            rejected: {
                actionName: "rejected",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/rejected", this.getProperty("/counts/rejected") + value);
                }
            }
        };

        WorkOrderListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        WorkOrderListViewModel.prototype.setOrderStatus = function (status, statusProfile, successCb) {
            var that = this;
            var load_error = function (err) {
                that.executeErrorCallback(err);
            };

            var order = this.getProperty("/selectedOrder");

            var statusMl = this.getUserStatusMl(status, statusProfile);

            if (!statusMl) {
                load_error(new Error("UserStatusMl not found"));
                return;
            }

            this.setBusy(true);
            order.setOperationsUserStatus(statusMl, function () {
                var selectedTabKey = that.getProperty("/view/selectedTabKey");
                var tabInfo = that.tabs[selectedTabKey];
                tabInfo.needsReload = true;

                // Check if statusMl.USER_STATUS needs stop watch action
                var watchStopped = that.handleStopWatch(statusMl);
                order.OPERATION_USER_STATUS = statusMl.USER_STATUS;
                order.OPERATION_USER_STATUS_TEXT = statusMl.USER_STATUS_DESC;

                if (status == that.orderUserStatus.reject.status || (statusMl.USER_STATUS_CODE.startsWith("RE") && !statusMl.USER_STATUS_CODE.startsWith("REC"))) {
                    // Move order to rejected tab
                    that._moveOrderFromTabToTab(tabInfo, that.tabs.rejected, order);
                } else if (status == that.orderUserStatus.complete.status) {
                    // Move order to completed tab
                    that._moveOrderFromTabToTab(tabInfo, that.tabs.complete, order);
                }

                that.setStatusButtonsForStatus(order.OPERATION_USER_STATUS, order.OPERATION_STATUS_PROFILE);
                that.setBusy(false);
                that.refresh();
                successCb(that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE), watchStopped);
            }, load_error);
        };

        WorkOrderListViewModel.prototype.handleStopWatch = function (statusMl) {
            var that = this;
            var statusObject = this.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE);
            var runningWatch = this.getStopwatchManager().getStopwatchForOrderId(this.getSelectedOrder().getOrderId()) == null ? false : true;

            if (!statusObject) {
                //this.executeErrorCallback(new Error("Could not start stop watch: Status(" + newStatus + ")not found"));
                return false;
            }

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            if (statusObject.startsStopwatch()) {
                // start a stop watch
                // TODO USE APPROPRIATE ACT TYPE HERE
                this.getStopwatchManager().start(this.getProperty("/selectedOrder"), "ACTT", function () {
                    //success
                }, onError);
            } else {
                this.getStopwatchManager().stop(this.getProperty("/selectedOrder"), function () {
                    if (that.orderUserStatus.workFinished.status != statusMl.USER_STATUS) {
                        return;
                    }

                    // Publish refresh event for current active tab
                    sap.ui.getCore()
                        .getEventBus()
                        .publish("workOrdersRoute", "navToTime", that.getProperty("/selectedOrder"));
                }, onError);
            }

            return runningWatch;
        };

        WorkOrderListViewModel.prototype.getLongText = function (order, successCb) {
            var longTextInfo = this.getScenario().getListItemLongtextInformation();

            if (longTextInfo.isOrderLongtext) {
                // Load order long text
                order.loadLongText(longTextInfo.objectType, function (text) {
                    successCb(text);
                });
            } else {
                // Load notification long text
                var notification = order.getNotification();

                if (!notification) {
                    this.executeErrorCallback(new Error("Longtext could not be loaded: Notification could not be retrieved from the order object"));
                    return;
                }

                notification.loadLongText(longTextInfo.objectType).then(successCb);
            }
        };

        WorkOrderListViewModel.prototype.onTabChanged = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // MODEL SEARCH
            // Reset binding and resetting searchValue when switching tabs
            this.setProperty("/searchString", "");
            this.getList().getBinding("items").filter(null);

            this.resetGrowingListDelegate();

            if (tabInfo.needsReload || tabInfo.lastSearch != this.getSearchString()) {
                this.fetchData();
            } else {
                this.setNewData(tabInfo.cached);
            }
        };

        WorkOrderListViewModel.prototype.onSearch = function (oEvt) {
            // DATABASE SEARCH
            // clearTimeout(this._delayedSearch);
            // this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);

            // MODEL SEARCH
            var aFilters = [];
            var sValue = oEvt.getSource().getValue();

            if (sValue && sValue.length > 0) {
                aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("SHORT_TEXT", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("FUNCLOC_DESC", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("NOTIF_TYPE", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("_START_DATE", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("_END_DATE", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("OPERATION_USER_STATUS_TEXT", sap.ui.model.FilterOperator.Contains, sValue)); //OPERATION_USER_STATUS_TEXT
                var allFilter = new Filter(aFilters);
            }

            var oBinding = this.getList().getBinding("items");
            oBinding.filter(allFilter);
        };

        WorkOrderListViewModel.prototype.setSelectedOrder = function (order) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");

            if (selectedTabKey == this.tabs.complete.actionName || selectedTabKey == this.tabs.rejected.actionName) {
                order = null;
            }

            this.setProperty("/selectedOrder", order);
            this.setProperty("/view/orderSelected", !order || order == undefined ? false : true);

            this.setStatusButtonsForStatus(!order ? "" : order.OPERATION_USER_STATUS, !order ? "" : order.OPERATION_STATUS_PROFILE);

            if (!this.getProperty("/view/orderSelected")) {
                this.getList().removeSelections();
            }
        };

        WorkOrderListViewModel.prototype.navigateToLocation = function (oEvent) {
            var that = this;
            var location = this.getSelectedOrder().getLocation();

            if (!location) {
                this.executeErrorCallback(new Error("Could not start navigation: No GPS data maintained."));
                return;
            }

            launchnavigator.isAppAvailable(launchnavigator.APP.HERE_MAPS, function(isAvailable){
                var app;
                if(isAvailable){
                    that.startNavigation(location, launchnavigator.APP.HERE_MAPS);
                }else{
                    var dialog = that._component.rootComponent.navigator.getSelectionDialog(function(oEvent) {
                        var selectedItem = oEvent.getParameter("listItem");
                        var app = selectedItem.getBindingContext("undefined").getObject().app;
                        that.startNavigation(location, app);
                        dialog.close();
                    });

                    dialog.open();
                }
            });
        };

        WorkOrderListViewModel.prototype.startNavigation = function (address, app) {
            this._component.rootComponent.navigator.navTo(address, function () {
            }, function () {
            }, app);
        };

        WorkOrderListViewModel.prototype.setNewData = function (data) {
            var that = this;
            var newData = [];

            data.forEach(function (orderData) {
                var order = new Order(orderData, that._requestHelper, that);
                order.setPersNo(that.getUserInformation().personnelNumber);

                if (orderData.hasOwnProperty("NOTIF_MOBILE_ID") &&
                    orderData.hasOwnProperty("NOTIF_OBJECT_NO") &&
                    orderData.hasOwnProperty("NOTIF_NO") &&
                    orderData.hasOwnProperty("NOTIF_ACTION_FLAG")) {
                    var notification = new NotificationHeader({
                        MOBILE_ID: orderData.NOTIF_MOBILE_ID,
                        OBJECT_NO: orderData.NOTIF_OBJECT_NO,
                        NOTIF_NO: orderData.NOTIF_NO,
                        ACTION_FLAG: orderData.NOTIF_ACTION_FLAG
                    }, that._requestHelper, that);

                    order.setNotification(notification);
                }

                newData.push(order);
            });

            this.setProperty("/orders", newData);
            this.setSelectedOrder(null);
        };

        WorkOrderListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                tabInfo.needsReload = false;
                tabInfo.lastSearch = that.getSearchString();
                tabInfo.cached = bGrowing ? tabInfo.cached.concat(data) : data;

                that.setNewData(tabInfo.cached);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching orders"));
            };

            this.orderUserStatus = this.getScenario().getOrderUserStatus();
            this.setBusy(true);
            this._requestHelper.getData("Orders", this.getOrderQueryParams(bGrowing ? this.getProperty("/orders").length + 1 : 1), load_success, load_error, true, tabInfo.actionName);
        };

        WorkOrderListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.fetchData(successCb);
        };

        WorkOrderListViewModel.prototype.resetModel = function () {
            for (var key in this.tabs) {
                this.tabs[key].needsReload = true;
            }
        };

        WorkOrderListViewModel.prototype.fetchCounts = function () {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/counts/open", parseInt(data.OpenOrderCount[0].COUNT));
                that.setProperty("/counts/complete", parseInt(data.CompleteOrderCount[0].COUNT));
                that.setProperty("/counts/rejected", parseInt(data.RejectedOrderCount[0].COUNT));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching tab counts"));
            };

            this.orderUserStatus = this.getScenario().getOrderUserStatus();
            this._requestHelper.getData(["OpenOrderCount", "CompleteOrderCount", "RejectedOrderCount"], {
                persNo: this.getUserInformation().personnelNumber,
                completeStatus: this.orderUserStatus.complete.status,
                rejectedStatus: this.orderUserStatus.reject.status
            }, load_success, load_error, true);
        };

        WorkOrderListViewModel.prototype._moveOrderFromTabToTab = function(fromTab, toTab, order) {
            this.setSelectedOrder(null);
            this.removeFromModelPath(order, "/orders");
            fromTab.cached = this.getProperty("/orders");
            toTab.needsReload = true;
            toTab.changeCountBy.apply(this, [1]);
            fromTab.changeCountBy.apply(this, [-1]);
        };

        WorkOrderListViewModel.prototype.constructor = WorkOrderListViewModel;

        return WorkOrderListViewModel;
    });
