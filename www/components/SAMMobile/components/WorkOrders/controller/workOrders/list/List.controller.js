sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/list/WorkOrderListViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/list/RejectionReasonDialogViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/CompleteOrderViewModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/m/Button",
    "sap/m/Dialog",
    'sap/m/Text'
], function (Controller,
             WorkOrderListViewModel,
             RejectionReasonDialogViewModel,
             CompleteOrderViewModel,
             formatter,
             formatterComp,
             MessageBox,
             MessageToast,
             Button,
             Dialog,
             Text) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.list.List", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrders").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("workOrdersRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().subscribe("workOrdersRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderListViewModel = new WorkOrderListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.workOrderListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.workOrderListViewModel.setList(this.byId("orderList"));

            this.rejectionDialogViewModel = null;
            this.completeOrderViewModel = null;

            this.initialized = false;

            this.getView().setModel(this.workOrderListViewModel, "workOrderListViewModel");
        },

        _onRouteMatched: function (oEvent) {
            var that = this;
            var onMasterDataLoaded = function(routeName) {
                this.globalViewModel.setComponentBackNavEnabled(false);
                this.globalViewModel.setCurrentRoute(routeName);
                this.globalViewModel.setComponentHeaderText(this.component.i18n.getText("orderListTitle"));
                this.workOrderListViewModel.setSelectedOrder(null);

                if (this.workOrderListViewModel.needsReload()) {
                    this.loadOrders();
                }

                if (!this.initialized) {
                    this.initialized = true;
                    this.component.onAfterSync();
                }
            };

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function() {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }
        },

        loadMasterData: function(successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onRefreshRoute: function () {
            this.loadOrders(true);
        },

        onLogout: function() {
            this.workOrderListViewModel.resetModel();
            this.initialized = false;
        },

        onNavToTime: function (channelId, eventId, order) {
            this.oRouter.navTo("workOrderDetailRoute", {
                id: order.getOrderId(),
                persNo: this.workOrderListViewModel.getUserInformation().personnelNumber,
                query: {
                    directTo: "workOrderDetailTime"
                }
            }, false);
        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("workOrdersRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore().getEventBus().unsubscribe("workOrdersRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore().getEventBus().unsubscribe("home", "onLogout", this.onLogout, this);
        },

        loadOrders: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            this.workOrderListViewModel.fetchCounts();

            if (bSync) {
                this.workOrderListViewModel.refreshData();
            } else {
                this.workOrderListViewModel.fetchData(function () {

                });
            }
        },

        onItemPress: function (oEvt) {
            var bindingContext = oEvt.getParameter("listItem").getBindingContext("workOrderListViewModel");
            var order = bindingContext.getObject();

            this.workOrderListViewModel.setSelectedOrder(order);
        },

        onNavToDetailPress: function (oEvt) {
            var bindingContext = oEvt.getSource().getBindingContext("workOrderListViewModel");
            var order = bindingContext.getObject();

            this.oRouter.navTo("workOrderDetailRoute", {
                id: order.getOrderId(),
                persNo: this.workOrderListViewModel.getUserInformation().personnelNumber
            }, false);
        },

        onTabSelect: function (oEvent) {
            this.workOrderListViewModel.onTabChanged(oEvent);
        },

        onTableSearch: function (oEvt) {
            this.workOrderListViewModel.onSearch(oEvt);
        },

        onListGrowing: function (oEvt) {
            this.workOrderListViewModel.onListGrowing(oEvt);
        },

        onOrderStatusIconPressed: function (oEvt) {
            var that = this;

            var bindingContext = oEvt.getSource().getBindingContext("workOrderListViewModel");
            var order = bindingContext.getObject();

            var onLongTextFetched = function (longText) {
                var dialog = new Dialog({
                    title: 'WCA Long Text',
                    type: 'Message',
                    content: new Text({
                        text: longText
                    }),
                    beginButton: new Button({
                        text: 'OK',
                        press: function () {
                            dialog.close();
                        }
                    }),
                    afterClose: function () {
                        dialog.destroy();
                    }
                });

                dialog.open();
            };

            this.workOrderListViewModel.getLongText(order, onLongTextFetched);
        },

        onStatusButtonPressed: function (oEvt) {
            var that = this;

            var customData = oEvt.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error("Could not read status or status profile for pressed button"));
                return;
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");

            var onActionConfirmed = function () {
                that.workOrderListViewModel.setOrderStatus(status, statusProfile, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }
                    var syncMode = statusObject.async ? "" : "S";

                    if (bStopwatchStopped) {
                        that._getConfirmActionDialog(that.component.i18n.getText("timeEntryCreateConfirmation"), that.component.sync.bind(that.component, syncMode, true)).open();
                        return;
                    }

                    that.component.sync(syncMode, true);
                });
            };

            if (this.workOrderListViewModel.statusIsReject(status)) {
                this.displayRejectionReasonSearch(oEvt);
            } else if (this.workOrderListViewModel.statusIsComplete(status)) {
                // get complete dialog
                this._getCompleteOrderDialog(onActionConfirmed).open();
            } else {
                this._getConfirmActionDialog(this.component.i18n.getText("changeOrderStatusConfirmation"), onActionConfirmed).open();
            }

        },

        onNavigateToLocationPressed: function(oEvent) {
            this.workOrderListViewModel.navigateToLocation();
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        formatOperationUserStatusToText: function (status) {
            return this.workOrderListViewModel.getFormatter().formatOperationUserStatusToText(status);
        },

        formatOperationUserStatusToIcon: function (status) {
            return this.workOrderListViewModel.getFormatter().formatOperationUserStatusToIcon(status);
        },

        displayRejectionReasonSearch: function (oEvent) {
            this.rejectionDialogViewModel = new RejectionReasonDialogViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            var operationStatusProfile = this.workOrderListViewModel.getOperationStatusProfileForSelectedOrder();

            this.rejectionDialogViewModel.setStatusProfile(operationStatusProfile);
            this.rejectionDialogViewModel.display(oEvent, this);
        },

        handleUserStatusSearch: function (oEvent) {
            this.rejectionDialogViewModel.handleSearch(oEvent);
        },

        handleUserStatusSelect: function (oEvent) {
            var that = this;
            this.rejectionDialogViewModel.handleSelect(oEvent, function (status, statusProfile) {
                that.workOrderListViewModel.setOrderStatus(status, statusProfile, function () {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                });
            });
        },

        onCompleteDialogObjectsEdit: function (oEvent) {
            this.oRouter.navTo("workOrderDetailRoute", {
                id: this.workOrderListViewModel.getSelectedOrder().getOrderId(),
                persNo: this.workOrderListViewModel.getUserInformation().personnelNumber,
                query: {
                    directTo: "workOrderDetailObjects"
                }
            }, false);
        },

        onCompleteDialogComponentsEdit: function (oEvent) {
            this.oRouter.navTo("workOrderDetailRoute", {
                id: this.workOrderListViewModel.getSelectedOrder().getOrderId(),
                persNo: this.workOrderListViewModel.getUserInformation().personnelNumber,
                query: {
                    directTo: "workOrderDetailMaterial"
                }
            }, false);
        },

        onCompleteDialogTimeEdit: function (oEvent) {
            this.oRouter.navTo("workOrderDetailRoute", {
                id: this.workOrderListViewModel.getSelectedOrder().getOrderId(),
                persNo: this.workOrderListViewModel.getUserInformation().personnelNumber,
                query: {
                    directTo: "workOrderDetailTime"
                }
            }, false);
        },

        _getCompleteOrderDialog: function (executeCb) {
            var that = this;

            this.completeOrderViewModel = new CompleteOrderViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.completeOrderViewModel.setOrder(this.workOrderListViewModel.getSelectedOrder());

            if (!this._completeOrderDialog) {

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("completeOrderPopoverList", "SAMMobile.components.WorkOrders.view.workOrders.detail.CompleteOrderPopover", this);
                }

                this._completeOrderDialog = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._oMcPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    busy: "{completeOrderModel>/view/busy}",
                    beginButton: new Button({
                        text: '{i18n_core>complete}',
                        press: function () {
                            var button = this;

                            that.completeOrderViewModel.saveChanges(function () {
                                executeCb();
                                button.getParent().close();
                            }, that.onViewModelError);
                        }
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {

                    }
                });

                this._completeOrderDialog.setModel(this.component.getModel('i18n'), "i18n");
                this._completeOrderDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._completeOrderDialog.setTitle(this.completeOrderViewModel.getDialogTitle());
            this._completeOrderDialog.setModel(this.completeOrderViewModel, "completeOrderModel");

            this.completeOrderViewModel.loadOrderData(function (err) {
                that.onViewModelError(err);
            });

            return this._completeOrderDialog;
        },

        _getConfirmActionDialog: function (message, executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: message,
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("Yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("No"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        }

    });

});