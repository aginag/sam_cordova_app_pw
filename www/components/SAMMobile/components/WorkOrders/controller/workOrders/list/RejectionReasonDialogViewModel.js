sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function RejectionReasonDialogViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        RejectionReasonDialogViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        STATUS: '',
                        STATUS_PROFILE: '',
                        STATUS_DESC: '',
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setStatusProfile: {
                value: function (statusProfile) {
                   this.setProperty("/STATUS_PROFILE", statusProfile);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        RejectionReasonDialogViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        RejectionReasonDialogViewModel.prototype.acceptSelection = function(successCb) {
            if (this.validate()) {
                successCb(this.getProperty("/STATUS"), this.getProperty("/STATUS_PROFILE"));
            }
        };

        RejectionReasonDialogViewModel.prototype.validate = function() {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("STATUS", "Please enter a Name").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/messages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialog
        RejectionReasonDialogViewModel.prototype.display = function(oEvent, viewContext) {
            var that = this;

            if (!this._oRejectionReasonSelectDialog) {
                this._oRejectionReasonSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.RejectionReasonSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oRejectionReasonSelectDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");
            this._oRejectionReasonSelectDialog.display(oEvent, 'UserStatusMl', this._filterUserStatus.bind(this));
        };

        RejectionReasonDialogViewModel.prototype.handleSelect = function(oEvent, successCb) {
            this._oRejectionReasonSelectDialog.select(oEvent, 'USER_STATUS_DESC', [{
                'STATUS': 'USER_STATUS'
            }, {
                'STATUS_DESC': 'USER_STATUS_DESC'
            }], this);

            successCb(this.getProperty("/STATUS"), this.getProperty("/STATUS_PROFILE"));
        };

        RejectionReasonDialogViewModel.prototype.handleSearch = function(oEvent) {
            this._oRejectionReasonSelectDialog.search(oEvent, ['USER_STATUS_CODE', 'USER_STATUS_DESC']);
        };

        RejectionReasonDialogViewModel.prototype.getValueStateModel = function() {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    STATUS: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        RejectionReasonDialogViewModel.prototype._filterUserStatus = function(userStatus) {
            return userStatus.STATUS_PROFILE == this.getProperty("/STATUS_PROFILE") && userStatus.USER_STATUS_CODE.startsWith("RE") && !userStatus.USER_STATUS_CODE.startsWith("REC") ;
        };

        RejectionReasonDialogViewModel.prototype.constructor = RejectionReasonDialogViewModel;

        return RejectionReasonDialogViewModel;
    });
