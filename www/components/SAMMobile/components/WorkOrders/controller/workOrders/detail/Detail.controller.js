sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/WorkOrderDetailViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/CompleteOrderViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/list/RejectionReasonDialogViewModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Text",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/reject/RejectOperationViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/EditTimeEntryViewModel",
    "sap/ui/core/routing/History"

], function (Controller,
             WorkOrderDetailViewModel,
             CompleteOrderViewModel,
             RejectionReasonDialogViewModel,
             MessageBox,
             MessageToast,
             formatter,
             formatterComp,
             Button,
             Dialog,
             Text,
             RejectOperationViewModel,
             EditTimeEntryViewModel,
             History) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.Detail", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailRoute").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore()
                .getEventBus()
                .subscribe("home", "onNavToScenario", this.onNavToScenario, this);

            this.iconTabBar = this.byId("idIconTabBarWorkOrders");

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = new WorkOrderDetailViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.workOrderDetailViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.rejectOperationViewModel = new RejectOperationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.rejectOperationViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.completeOrderViewModel = null;

            this.getView().setModel(this.workOrderDetailViewModel, "workOrderDetailViewModel");
        },

        onRouteMatched: function (oEvent) {

            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            var orderId = oArgs.id;
            var operId = oArgs.operId;
            var persNo = oArgs.persNo;

            if (persNo == undefined || persNo == "" || persNo == null) {
                this.onNavBack();
                return;
            }
            this.workOrderDetailViewModel.setOrderId(orderId, operId, persNo);

            this.loadViewModelData(false, this.onAfterInitialLoad.bind(this, oArgs));
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailRoute", "refreshRoute", this.onRefreshRoute, this);
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailRoute", "navToTime", this.onNavToTime, this);
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("home", "onNavToScenario", this.onNavToScenario, this);
        },

        onRefreshRoute: function () {
            jQuery.sap.log.info("PriSec - Detail Controller - Refresh Event triggered");
            this.loadViewModelData(true);
        },

        onNavToTime: function () {
            if (this.workOrderDetailViewModel.getTabRouteName() == this.workOrderDetailViewModel.tabs.time.routeName) {
                return;
            }

            var orderId = this.workOrderDetailViewModel.getOrderId();
            var persNo = this.workOrderDetailViewModel._PERS_NO;

            this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.time.actionName);
            this.oRouter.navTo(this.workOrderDetailViewModel.tabs.time.routeName, {
                id: orderId,
                persNo: persNo
            }, true);
        },

        onNavToScenario: function () {
            this.workOrderDetailViewModel.resetData();
        },

        onAfterInitialLoad: function (oArgs) {
            var route;

            if (oArgs["?query"] != undefined) {
                var tab = this.workOrderDetailViewModel.getTabByRouteName(oArgs["?query"].directTo);

                if (!tab) {
                    tab = this.workOrderDetailViewModel.tabs.overview;
                }

                route = tab.routeName;
                this.workOrderDetailViewModel.setSelectedTabKey(tab.actionName);
            } else {
                route = this.workOrderDetailViewModel.tabs.overview.routeName;
                this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.overview.actionName);
            }

            var notifNo = this.workOrderDetailViewModel.getOrder().NOTIF_NO;

            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if(sPreviousHash.includes("technicalObjects") ||  sPreviousHash.includes("notifications") ){
                
                var routeObj;
                for (var index = oHistory.aHistory.length - 1 ; index >= 0; index--) {
                    var hist = oHistory.aHistory[index];
                    if (sPreviousHash == hist) {
                        routeObj = oHistory.aHistory[index - 1];
                        break;
                    }
                }
                
                if (routeObj && routeObj.includes("workOrders")){
                
                    var routeArr = routeObj.split("/");
                    var tab = this.workOrderDetailViewModel.getTabByActionName(routeArr[routeArr.length - 1]);

                    if (!tab) {
                        tab = this.workOrderDetailViewModel.tabs.overview;
                    }

                    route = tab.routeName;
                    this.workOrderDetailViewModel.setSelectedTabKey(routeArr[routeArr.length - 1]);
                    
                }
            }

            this.oRouter.navTo(route, {
                id: oArgs.id,
                operId: oArgs.operId,
                persNo: oArgs.persNo,
                notifNo: notifNo
            }, true);
        },

        loadViewModelData: function (bSync, successCb) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;


            var onReloadSuccess = function (order) {
                that.globalViewModel.setComponentHeaderText(that.workOrderDetailViewModel.getHeaderText());

                that.workOrderDetailViewModel.loadAllCounters();
                if (successCb) {
                    successCb();
                }
            };

            if (bSync) {
                this.workOrderDetailViewModel.refreshData(onReloadSuccess);
            } else {
                this.workOrderDetailViewModel.loadData(onReloadSuccess);
            }
        },

        onTabSelect: function (oEvent) {
            var routeName = this.workOrderDetailViewModel.getTabRouteName();
            var orderId = this.workOrderDetailViewModel.getOrderId();
            var operId = this.workOrderDetailViewModel._ACTIVITY;
            var persNo = this.workOrderDetailViewModel._PERS_NO;
            var notifNo = this.workOrderDetailViewModel.getOrder().NOTIF_NO;

            this.oRouter.navTo(routeName, {
                id: orderId,
                operId: operId,
                persNo: persNo,
                notifNo: notifNo
            }, true);
        },

        onNavBack: function () {
            var that = this;

            setTimeout(function () {
                that.workOrderDetailViewModel.resetData();
            }, 200);

            if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                this.oRouter.navTo("workOrders");
            } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                this.oRouter.navTo("workOrderOperations");
            }
        },

        onViewModelError: function (err) {

            MessageBox.error(err.message);

            if (err.name == "RETURN_TO_LIST") {
                this.onNavBack();
            }
        },

        //set Operation In progress
        onSetInProgressPress: function (oEvt) {

            var operation = this.workOrderDetailViewModel.getOrder().getOrderOperations();
            var statusProfile = operation.OPERATION_STATUS_PROFILE ? operation.OPERATION_STATUS_PROFILE : this.component.scenario.getOrderUserStatus().down.STATUS_PROFILE;

            var that = this;
            var onActionConfirmed = function () {
                that.workOrderDetailViewModel.setOperationStatus(that.component.scenario.getOrderUserStatus().done.STATUS, statusProfile, false, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                    that.workOrderDetailViewModel.getOrder().SetInProgressVisible = false;
                    that.workOrderDetailViewModel.getOrder().WatchEnabled = false;
                    that.workOrderDetailViewModel.refresh();
                    sap.ui.getCore().getEventBus().publish("home", "refreshCounters");

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }
                });
            };
            this._getConfirmActionDialog(this.component.i18n.getText("CHANGE_ORDER_STATUS_TO_IN_PROGRESS_CONFIRMATION_TEXT"), onActionConfirmed).open();
        },

        //set Operation as Complete
        onSetCompletePress: function (oEvt) {
            var that = this;

            var operation = this.workOrderDetailViewModel.getOrder().getOrderOperations();
            var workCenter;
            if(this.workOrderDetailViewModel.getData().order) {
                workCenter = this.workOrderDetailViewModel.getData().order.OPERATION_WK_CTR;
            }
            var statusProfile = operation.OPERATION_STATUS_PROFILE ? operation.OPERATION_STATUS_PROFILE : this.component.scenario.getOrderUserStatus().down.STATUS_PROFILE;

            var onActionConfirmed = function () {

                var fnCompleteOperation = function () {
                    that.workOrderDetailViewModel.setOperationStatus(that.component.scenario.getOrderUserStatus().done.STATUS, statusProfile, true, function (statusObject, bStopwatchStopped) {
                        MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                        sap.ui.getCore().getEventBus().publish("home", "refreshCounters");

                        that.oRouter.navTo("workOrderOperations");
                    });
                };

                if (workCenter === 'N42' || workCenter=== 'N43') {
                    fnCompleteOperation();
                } else {
                    that.workOrderDetailViewModel.validateOperationCompletion(operation, function (bComplete) {
                        if (bComplete) {
                            fnCompleteOperation();
                        } else {
                            that.workOrderDetailViewModel.executeErrorCallback(new Error(that.component.i18n.getText("CHANGE_ORDER_STATUS_TO_COMPLETE_HAVING_DEPENDENCIES_ERROR_TEXT")));
                        }
                    });
                }
            };
            this._getConfirmActionDialog(this.component.i18n.getText("CHANGE_ORDER_STATUS_TO_COMPLETE_CONFIRMATION_TEXT"), onActionConfirmed).open();
        },

        onStatusButtonPressed: function (oEvt) {
            var that = this;

            var customData = oEvt.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error("Could not read status or status profile for pressed button"));
                return;
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");

            var onActionConfirmed = function () {
                that.workOrderDetailViewModel.setOrderStatus(status, statusProfile, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }
                    var syncMode = statusObject.async ? "" : "S";

                    if (bStopwatchStopped) {
                        that._getConfirmActionDialog(that.component.i18n.getText("timeEntryCreateConfirmation"), that.component.sync.bind(that.component, syncMode)).open();
                        return;
                    }

                    that.component.sync(syncMode);
                });
            };


            if (this.workOrderDetailViewModel.statusIsReject(status)) {
                this.displayRejectionReasonSearch(oEvt);
            } else if (this.workOrderDetailViewModel.statusIsComplete(status)) {
                // get complete dialog
                this._getCompleteOrderDialog(onActionConfirmed).open();
            } else {
                this._getConfirmActionDialog(this.component.i18n.getText("changeOrderStatusConfirmation"), onActionConfirmed).open();
            }
        },

        displayRejectionReasonSearch: function (oEvent) {
            this.rejectionDialogViewModel = new RejectionReasonDialogViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            var operationStatusProfile = this.workOrderDetailViewModel.getOperationStatusProfileForOrder();

            this.rejectionDialogViewModel.setStatusProfile(operationStatusProfile);
            this.rejectionDialogViewModel.display(oEvent, this);
        },

        handleUserStatusSearch: function (oEvent) {
            this.rejectionDialogViewModel.handleSearch(oEvent);
        },

        handleUserStatusSelect: function (oEvent) {
            var that = this;
            this.rejectionDialogViewModel.handleSelect(oEvent, function (status, statusProfile) {
                that.workOrderDetailViewModel.setOrderStatus(status, statusProfile, function () {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                });
            });
        },

        onCompleteDialogObjectsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.objects.actionName);
            this.onTabSelect();
        },

        onCompleteDialogComponentsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.material.actionName);
            this.onTabSelect();
        },

        onCompleteDialogTimeEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.time.actionName);
            this.onTabSelect();
        },

        onCompleteDialogSiteDetailsEdit: function (oEvent) {
            this._completeOrderDialog.close();
            this.workOrderDetailViewModel.setSelectedTabKey(this.workOrderDetailViewModel.tabs.site.actionName);
            this.onTabSelect();
        },

        onOpenRejectOperationDialogPress: function (oEvent) {
            var operation = this.workOrderDetailViewModel.getOrder().getOrderOperations();
            this.rejectOperationViewModel.openDialog(operation, false);
        },

        _getConfirmActionDialog: function (message, executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: message,
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        _getCompleteOrderDialog: function (executeCb) {
            var that = this;

            this.completeOrderViewModel = new CompleteOrderViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.completeOrderViewModel.setOrder(this.workOrderDetailViewModel.getOrder());

            if (!this._completeOrderDialog) {

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("completeOrderPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.CompleteOrderPopover", this);
                }

                this._completeOrderDialog = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._oMcPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    busy: "{completeOrderModel>/view/busy}",
                    beginButton: new Button({
                        text: '{i18n_core>complete}',
                        press: function () {
                            var button = this;

                            that.completeOrderViewModel.saveChanges(function () {
                                that.workOrderDetailViewModel.setEditable(false);
                                executeCb();
                                button.getParent().close();
                            }, that.onViewModelError);
                        }
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {

                    }
                });

                this._completeOrderDialog.setModel(this.component.getModel('i18n'), "i18n");
                this._completeOrderDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._completeOrderDialog.setTitle(this.completeOrderViewModel.getDialogTitle());
            this._completeOrderDialog.setModel(this.completeOrderViewModel, "completeOrderModel");

            this.completeOrderViewModel.loadOrderData(function (err) {
                that.onViewModelError(err);
            });

            return this._completeOrderDialog;
        },

        formatOperationUserStatusToText: function (status) {
            return this.workOrderDetailViewModel.getFormatter().formatOperationUserStatusToText(status);
        },

        onStopWatchPressed: function (oEvent) {
            var that = this;
            
            
            var customData = oEvent.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error(that.component.i18n.getText("BUTTON_PROFILE_COULDNOT_READ")));
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");
            
            var operation = this.workOrderDetailViewModel.getOrder();

            if(operation.whichUser == "1"){
                this.workOrderDetailViewModel.handleStopWatchTeamMember(function(oCreatedTimeEntry, statusObject){
                    that.loadViewModelData(true);
                    if (oCreatedTimeEntry) {
                        that.onTimeConfirmationFinished(oCreatedTimeEntry, statusObject);
                    }
                });
            }
            else{
            that.workOrderDetailViewModel.setOperationStatus(status, statusProfile, true, function (statusObject, oCreatedTimeEntry) {
                MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                that.workOrderDetailViewModel.loadAllCounters();
                if (!statusObject) {
                    return;
                }
                
                that.loadViewModelData(true);

                if (oCreatedTimeEntry) {
                    that.onTimeConfirmationFinished(oCreatedTimeEntry, statusObject);
                    return;
                }

                var syncMode = statusObject.async ? "" : "S";
                if (statusObject.autoSync) {
                    that.component.sync(syncMode, true);
                }
            });
        }

        },

        onTimeConfirmationFinished: function (oCreatedTimeEntry, statObject){
            this.syncMode;
            this.statusObject = statObject;

            if(this.statusObject){
                this.syncMode = this.statusObject.async ? "" : "S";
            }
            var onTimeEntryAdjusted = function (updatedTimeEntry, editDialog) {

                var updateFunction = function (updatedTimeEntry, editDialog) {
                    this.workOrderDetailViewModel.updateTimeConfirmation(updatedTimeEntry, function () {
                        MessageToast.show(this.component.i18n.getText("TIME_CONF_UPDATE_TIME_ENTRY_SUCCESS_TEXT"));
                        editDialog.close();

                        if (this.statusObject && this.statusObject.autoSync) {
                            this.component.sync(this.syncMode, true);
                        }
                    }.bind(this));
                }.bind(this);

                if((!updatedTimeEntry.B_VALUE_ID || updatedTimeEntry.B_VALUE_ID !== "5") && this.workOrderDetailViewModel._hasReachedMaxDailyWork(updatedTimeEntry)) {
                    MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TEXT"), {
                        icon: MessageBox.Icon.INFORMATION,
                        title: this.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TITLE"),
                        actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                        onClose: function(oAction) {
                            if (oAction === "YES") {
                                updateFunction(updatedTimeEntry, editDialog);
                            }
                        }
                    });
                } else {
                    updateFunction(updatedTimeEntry, editDialog);
                }

            };

            var onCancelPressed = function (tmeEntry, dialog) {
                MessageBox.warning(this.component.i18n.getText("DELETE_TIMER_MESSAGE"), {
                    title: this.component.i18n_core.getText("warning"),
                    actions: [this.component.i18n_core.getText("ok"), this.component.i18n_core.getText("cancel")],
                    onClose: function(oAction) {
                        if (oAction === this.component.i18n_core.getText("ok")) {
                                this.workOrderDetailViewModel.loadAllCounters();
                                MessageToast.show(this.component.i18n.getText("TIME_CONF_DELETE_TIME_ENTRY_SUCCESS_TEXT"));
                                dialog.close();
                                if (this.statusObject && this.statusObject.autoSync) {
                                    this.component.sync(this.syncMode, true);
                                }
                        }
                    }.bind(this)
                });
            };

            this._getTimeEntryEditDialog(oCreatedTimeEntry, true, onTimeEntryAdjusted.bind(this), onCancelPressed.bind(this)).open();            
        },

        _getTimeEntryEditDialog: function (timeEntry, bEdit, acceptCb, cancelCb) {
            var onAcceptPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");

                if (editTimeEntryViewModel.validate()) {
                    acceptCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");
                cancelCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
            };

            this.editTimeEntryViewModel = new EditTimeEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTimeEntryViewModel.setEdit(bEdit);
            this.editTimeEntryViewModel.setTimeEntry(timeEntry);

            if (!this._timeEntryEditPopup) {

                if (!this._timeEntryPopover) {
                    this._timeEntryPopover = sap.ui.xmlfragment(this.createId("timeConfirmationPopover"), "SAMMobile.components.WorkOrders.view.common.TimeConfirmationPopover", this);
                    
                    var oStartDateTimePicker = sap.ui.core.Fragment.byId(this.createId("timeConfirmationPopover"), "timeconf-start-time");
                    var oEndDateTimePicker = sap.ui.core.Fragment.byId(this.createId("timeConfirmationPopover"), "timeconf-end-time");

                    oStartDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oStartDateTimePicker);

                    oEndDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oEndDateTimePicker);
                }

                this._timeEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._timeEntryPopover,
                    beginButton: new Button({
                        text: '{= ${timeEntryModel>/view/edit} ? ${i18n_core>STOP_WATCH_BUTTON} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this._timeEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._timeEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._timeEntryEditPopup.setTitle(this.editTimeEntryViewModel.getDialogTitle());
            this._timeEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._timeEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel, "timeEntryModel");
            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel.getValueStateModel(), "valueStateModel");

            return this._timeEntryEditPopup;
        },

        onStartTimeChanged: function (oEvent) {
            this.editTimeEntryViewModel.setStartTime(oEvent.getParameter("value"));
        },

        onEndTimeChanged: function (oEvent){
            this.editTimeEntryViewModel.setEndTime(oEvent.getParameter("value"));
        }
    });

});
