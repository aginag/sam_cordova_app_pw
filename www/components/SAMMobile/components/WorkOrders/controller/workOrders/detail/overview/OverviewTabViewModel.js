sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/FunctionalLocation",
        "SAMMobile/models/dataObjects/ZOPTime"
    ],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, FunctionalLocation, ZOPTime) {
        "use strict";

        // constructor
        function OverviewTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this._FUNCLOC = null;

            this.valueStateModel = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        OverviewTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        functionalLocation: {
                            id: "",
                            description: "",
                            orderHistory: {},
                            notifHistory: {}
                        },
                        equipment: {
                            id: "",
                            description: "",
                            orderHistory: {},
                            notifHistory: {}
                        },
                        notification: {
                            id: "",
                            description: ""
                        },
                        cust_info: {},
                        address: {
                            street: "",
                            postCode: "",
                            city: ""
                        },
                        team: {
                        },
                        ownDates: {
                            TERMIN_START: "",
                            TERMIN_END: "",
                            OBJNR: ""
                        },
                        view: {
                        	teamVisible: false,
                            busy: false,
                            ownDatesEditable: true,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        orderId: this._ORDERID,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            setFunctionalLocationObject: {
                value: function (oFuncLoc) {
                    this._FUNCLOC = oFuncLoc;
                }
            },

            getFunctionalLocationObject: {
                value: function () {
                    return this._FUNCLOC;
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            getOwnDatesObject: {
                value: function () {
                    return this.getProperty("/ownDates");
                }
            },

            setOwnDatesObject: {
                value: function (oOwnDates) {
                    this.setProperty("/ownDates", oOwnDates);
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setDataFromOrder: {
                value: function (order) {
                    this.setProperty("/functionalLocation", {
                        id: order.FUNCT_LOC,
                        description: order.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: order.EQUIPMENT,
                        description: order.EQUIPMENT_DESC
                    });
                    
                    this.setProperty("/notification", {
                        id: order.NOTIF_NO,
                        description: order.NOTIFICATION_DESCRIPTION,
                        exists: order.NOTIF_MOBILE_ID ? true : false
                    });
                    
                    if (order.team !==  undefined ){
	                    this.setProperty("/team", order.team);
	                    if (order.team.name !==  undefined && order.team.name !== ''){
	                    	this.setProperty("/view/teamVisible", true);
	                  	}
                    }
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        OverviewTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        OverviewTabViewModel.prototype.setNewData = function (data) {
            if (data.FunctionalLocation) {
                var oFunctionalLocation = new FunctionalLocation(data.FunctionalLocation[0], this._requestHelper, this);

                // Check for order type and set funcloc description
                // https://msc-mobile.atlassian.net/browse/PFAL-615
                var oOrder = this.getOrder();
                if (oOrder.ORDER_TYPE === "SM03") {
                    oOrder.FUNCLOC_DESC = oFunctionalLocation.CITY1 + " " + oFunctionalLocation.STREET;
                    this.setDataFromOrder(oOrder);
                }

                this.setFunctionalLocationObject(oFunctionalLocation);
            }

            var oZOPTime = new ZOPTime(data.ZOPTime[0],this._requestHelper, this);
            oZOPTime.OBJNR = this.getOrder().getOrderOperations().OBJECT_NO;
            this.setOwnDatesObject(oZOPTime);

            // In case Start Date & End Date is not same -> capacitive operation -> enable functionality
            this.setProperty("/view/ownDatesEditable", this.getOrder().OWN_TIME);
        };

        OverviewTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                   successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_ORDER_DETAIL_FUNCLOC_TEXT")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["FunctionalLocation", "ZOPTime"], {
                objectId: this.getFuncLocId(),
                spras: this.getLanguage(),
                objnr: this.getOrder().getOrderOperations().OBJECT_NO
            }, load_success, load_error, true);
        };

        OverviewTabViewModel.prototype.loadEquipmentHistory = function (successCb) {
            var that = this;

            var load_success = function (data) {
                var newOrderHist = data.EquipmentOrderHist.filter(function (orderHistory) {
                    return orderHistory.ORDERID != that.getOrder().getOrderId();
                });

                that.setProperty("/equipment/orderHistory", newOrderHist);
                that.setProperty("/equipment/notifHistory", data.EquipmentNotifHist);
                that.setBusy(false);

                if (successCb !== undefined) {
                    successCb(new JSONModel({
                        orderHistory: that.getProperty("/equipment/orderHistory"),
                        notifHistory: that.getProperty("/equipment/notifHistory")
                    }));
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_EQUIPMENT_HISTORY")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["EquipmentNotifHist", "EquipmentOrderHist"], {
                equipmentNumber: this.getEquipmentId()
            }, load_success, load_error, true);
        };

        OverviewTabViewModel.prototype.loadFuncLocHistory = function (successCb) {

            var fnLoadError = function (err) {
                this._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_ORDER_DETAIL_FUNCLOC_TEXT");
            }.bind(this);

            var fnLoadSuccess = function (oData) {
                if (successCb !== undefined) {
                    successCb(new JSONModel({
                        notifHistory: oData[0],
                        orderHistory: oData[1],
                    }));
                }
            }.bind(this);

            var oPromiseLoadNotificationHistory = new Promise(function (resolve, reject) {
                this.getFunctionalLocationObject().loadNotificationHistory(resolve, reject);
            }.bind(this));

            var oPromiseLoadOrderHistory = new Promise(function (resolve, reject) {
                this.getFunctionalLocationObject().loadOrderHistory(resolve, reject);
            }.bind(this));

            Promise.all([oPromiseLoadNotificationHistory, oPromiseLoadOrderHistory]).then(fnLoadSuccess).catch(fnLoadError);
        };

        OverviewTabViewModel.prototype.getPersNoForOrderId = function (orderId, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setBusy(false);
                if (data.length == 0 || data[0].PERS_NO == "") {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("NO_PERSONAL_NUMBER_FOUND_FOR_THAT_ORDER")));
                    return;
                }

                successCb(data[0].PERS_NO);
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_ON_LOADING_PERSONAL_NUMBER_FOR_THAT_ORDER")));
            };

            this.setBusy(true);
            this._requestHelper.getData("OrderOperations", {
                orderId: orderId
            }, load_success, load_error, true, "getPersNoByOrderId");
        };

        OverviewTabViewModel.prototype.saveOwnDates = function (successCb) {
            try {
                var oZOPTime = this.getOwnDatesObject();

                if (!oZOPTime.MOBILE_ID) {
                    oZOPTime.insert(false, function (mobileId) {
                        oZOPTime.MOBILE_ID = mobileId;
                        if (successCb) {
                            successCb();
                        }
                    }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_INSERT_OWN_DATES"))));
                } else {
                    if (!oZOPTime.hasChanges()) {
                        return;
                    }
                    oZOPTime.update2(false, successCb, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_INSERT_OWN_DATES"))));
                }

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        OverviewTabViewModel.prototype.deleteOwnDates = function (successCb) {
            try {
                var oZOPTime = this.getOwnDatesObject();

                if (oZOPTime.MOBILE_ID) {
                    oZOPTime.ACTION_FLAG = "N"; // This is required as otherwise the delete sql statement is not executed
                    oZOPTime.delete(false, successCb, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_DELETE_OWN_DATES"))));
                } else {
                    oZOPTime.TERMIN_START = "";
                    oZOPTime.TERMIN_END = "";
                    successCb();
                }

            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        OverviewTabViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("ownDates.TERMIN_START", this._component.getModel("i18n").getResourceBundle().getText("ERROR_INSERT_OWN_DATES_NO_START_TIME")).isEmpty();
            validator.check("ownDates.TERMIN_END", this._component.getModel("i18n").getResourceBundle().getText("ERROR_INSERT_OWN_DATES_NO_END_TIME")).isEmpty();

            validator.check("ownDates", this._component.getModel("i18n").getResourceBundle().getText("ERROR_INSERT_OWN_DATES_END_TIME_LT_START_TIME")).custom(function(oTimeEntry) {
                return moment(oTimeEntry.TERMIN_START).isSameOrBefore(moment(oTimeEntry.TERMIN_END));
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);
            this.valueStateModel.setProperty("/TERMIN_START", errors ? sap.ui.core.ValueState.Error : sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/TERMIN_END", errors ? sap.ui.core.ValueState.Error : sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/VALUE_STATE_TEXT", errors ? errors[0].errorMessage : "");

            return errors ? false : true;
        };

        OverviewTabViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    TERMIN_START: sap.ui.core.ValueState.None,
                    TERMIN_END: sap.ui.core.ValueState.None,
                    VALUE_STATE_TEXT: ""
                });
            }

            return this.valueStateModel;
        };

        OverviewTabViewModel.prototype.resetValueStateModel = function () {
            if (!this.valueStateModel) {
                return;
            }

            this.valueStateModel.setProperty("/TERMIN_START", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/TERMIN_END", sap.ui.core.ValueState.None);
            this.valueStateModel.setProperty("/VALUE_STATE_TEXT", "");
            this.setProperty("/view/errorMessages", []);
        };

        OverviewTabViewModel.prototype.resetData = function () {
            this.setProperty("/functionalLocation/id", "");
            this.setProperty("/functionalLocation/description", "");

            this.setProperty("/equipment/id", "");
            this.setProperty("/equipment/description", "");

            this.setProperty("/notification/id", "");
            this.setProperty("/notification/description", "");
            
            this.setProperty("/team", {
                VEHICLE: "",
                MEMBERS: []
            });

            this.setOrder(null);
            this.setProperty("/view/teamVisible", false);
            this.setProperty("/view/ownDatesEditable", true);

            this.resetValueStateModel();
        };

        OverviewTabViewModel.prototype.constructor = OverviewTabViewModel;

        return OverviewTabViewModel;
    });
