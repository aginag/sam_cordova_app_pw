sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/helpers/FileSystem",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/SAMChecklistPoint",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/SAMObjectAttachment",
        "SAMMobile/models/dataObjects/NotificationLongtext"],

    function (JSONModel, ViewModel, Validator, FileSystem, Formatter, SAMChecklistPoint, GenericSelectDialog, SAMObjectAttachment, NotificationLongtext) {
        "use strict";

        // constructor
        function CheckListViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._formatter = new Formatter(this._component);
            this._CATALOG_TYPE = "S";
            this.taskStatus = {};

            this.fileSystem = new FileSystem();

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CheckListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        checkList: [],
                        checklistCategories: [],
                        checklistGrouped: [],
                        codeMl: [],
                        orderId: "",
                        notifNo: "",
                        persNo: "",
                        view: {
                            busy: false,
                            isExpanded: false,
                            hasChanges: true,
                            errorMessages: []
                        }
                    };
                }
            },

            getCheckList: {
                value: function () {
                    return this.getProperty("/checkList");
                }
            },

            getOrderId: {
                value: function () {
                    return this.getProperty("/orderId");
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this.setProperty("/orderId", orderId);
                }
            },

            getPersNo: {
                value: function () {
                    return this.getProperty("/persNo");
                }
            },

            setPersNo: {
                value: function (persNo) {
                    this.setProperty("/persNo", persNo);
                }
            },

            getNotificationNumber: {
                value: function () {
                    return this.getProperty("/notifNo");
                }
            },

            setNotificationNumber: {
                value: function (notifNo) {
                    this.setProperty("/notifNo", notifNo);
                }
            },

            getHeaderText: {
                value: function () {
                    return "Checklist";
                }
            },

            getObjectAttachmentForCheckListPoint: {
                value: function (checklistPoint) {
                    var attachmentObject = new SAMObjectAttachment(null, this._requestHelper, this);

                    attachmentObject.setTabName("NotificationTask");
                    attachmentObject.setObjectKey(checklistPoint.NOTIF_NO + checklistPoint.TASK_NO);

                    return attachmentObject;
                }
            },

            setHasChanges: {
                value: function (bChanges) {
                    this.setProperty("/view/hasChanges", bChanges);
                    this._globalViewModel.model.setProperty("/hasChanges", bChanges);
                }
            },

            hasChanges: {
                value: function () {
                    return this.getProperty("/view/hasChanges");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            setIsExpanded: {
                value: function (isExpanded) {
                    this.setProperty("/view/isExpanded", isExpanded);
                }
            },

            getIsExpanded: {
                value: function () {
                    return this.getProperty("/view/isExpanded");
                }
            }
        });


        CheckListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData, bForce) {
            var path = changeEvent == undefined || !changeEvent ? "" : changeEvent.getParameter("context").getPath();
        };

        CheckListViewModel.prototype.handleCodeMlSelect = function (oEvent, successCb) {
            var checkListPoint = this._oCodeMlSelectDialog.dialog.parentInput.getBindingContext("checkListViewModel").getObject();
            var selectedCode = oEvent.getParameter("selectedItem").getBindingContext().getObject();

            checkListPoint.setResultCode(selectedCode.CODE);
            this.updateChecklistPoint(checkListPoint, successCb);
        };

        CheckListViewModel.prototype.handleCodeMlSearch = function (oEvent) {
            this._oCodeMlSelectDialog.search(oEvent, ["CODE_TEXT", "CODE"]);
        };

        CheckListViewModel.prototype.updateChecklistPoint = function (checklistPoint, successCb) {
            var that = this;
            try {
                if (!checklistPoint.hasChanges()) {
                    return;
                }

                var onError = function (err) {
                    that.executeErrorCallback(err);
                };

                checklistPoint.update2(false, successCb, onError.bind(this, new Error(this._component.i18n.getText("ERROR_CHECKLIST_UPDATE"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CheckListViewModel.prototype.saveCheckList = function (successCb) {
            var onSaveSuccess = function () {
                this.setHasChanges(false);

                successCb();
            };

            this.setInitialStatusOnUnchangedChecklists();

            try {
                var checkListsToBeUpdate = this.getCheckList().filter(function (checkPoint) {
                    return checkPoint.hasChanges();
                });

                var sqlStrings = checkListsToBeUpdate.map(function (checkPoint) {
                    return checkPoint.update2(true);
                });

                var unsavedAttachments = this.getCheckList().map(function (checkPoint) {
                    return checkPoint.attachments.filter(function (attachment) {
                        return !attachment.ON_DATABASE;
                    })
                });

                // Flatten array of arrays
                unsavedAttachments = [].concat.apply([], unsavedAttachments);


                var sqlStringsAttachments = unsavedAttachments.map(function (attachment) {
                    return attachment.insert(true);
                });

                sqlStrings = sqlStrings.concat(sqlStringsAttachments);

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSaveSuccess.bind(this), this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_CHECKLIST_UPDATE"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CheckListViewModel.prototype.insertUserAttachment = function (attachment, successCb) {
            var that = this;
            var sqlStrings = [];

            try {
                attachment.insert(false, function (mobileId) {
                    attachment.MOBILE_ID = mobileId;
                    successCb();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_ATTACHMENT_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CheckListViewModel.prototype.updateUserAttachment = function (attachment, successCb) {
            var that = this;

            if (attachment.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error(this._component.i18n.getText("ATTACHMENT_DELETE_ONLY_WHEN_ACTION_FLAG_N")));
                return;
            }

            try {
                attachment.update(false, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CheckListViewModel.prototype.deleteUserAttachment = function (attachment, successCb) {
            var that = this;

            attachment.delete(false, successCb, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_DELETE")));
            });
        };

        CheckListViewModel.prototype.validate = function (completeCb) {
            var that = this;
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());


            var checklist = this.getCheckList();
            var measurementPoints = checklist.filter(function (checkPoint) {
                return !checkPoint.isTask() && checkPoint.hasChanges();
            });

            var measurementPointIndexes = measurementPoints.map(function (measPoint) {
                return checklist.indexOf(measPoint);
            });

            measurementPointIndexes.forEach(function (index) {
                var measPoint = checklist[index];
                measPoint.setValueState(sap.ui.core.ValueState.None);
                var readingIsNumber = !isNaN(measPoint.ZRESULT_READING);
                measPoint.setValueState(readingIsNumber ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);

                validator.check("checkList[" + index + "].ZRESULT_READING", that._component.i18n.getText("RESULT_MUST_BE_NUMBER")).isNumber();

                if (readingIsNumber) {

                    var isValid = "";
                    validator.check("checkList[" + index + "].ZRESULT_READING", that._component.i18n.getText("READINGCREATERTHANLAST")).custom(function (value) {
                        var reading = parseFloat(value);
                        var lastReading = measPoint.lastMeasPointReading;
                        isValid = reading >= lastReading;

                        measPoint.setValueState(isValid ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
                        measPoint._VALUE_STATE_TEXT = that._component.i18n.getText("READINGCREATERTHANLAST");
                        return isValid;
                    });

                    var checkWarning = function (value) {
                        var reading = parseFloat(value.ZRESULT_READING);
                        var minAcceptedValue = parseFloat(value.MRMIN);
                        var maxAcceptedValue = parseFloat(value.MRMAX);
                        var warning = [{
                            warningMessage: that._component.i18n.getText("READINGOUTSIDELIMITS")
                        }];

                        if (minAcceptedValue == 0 && maxAcceptedValue == 0) {
                            return "";
                        }

                        if (reading > maxAcceptedValue || reading < minAcceptedValue) {
                            return warning;
                        }

                        return "";
                    };
                    var warning = [];
                    if (isValid) {
                        var warning = checkWarning(measPoint);
                        measPoint.setValueState(warning ? sap.ui.core.ValueState.Warning : sap.ui.core.ValueState.None);
                        measPoint._VALUE_STATE_TEXT = that._component.i18n.getText("READINGOUTSIDELIMITS");
                    }
                    that.setProperty("/view/warningMessages", warning ? warning : []);
                } else {
                    measPoint.setValueState(readingIsNumber ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);
                    measPoint._VALUE_STATE_TEXT = that._component.i18n.getText("RESULT_MUST_BE_NUMBER");
                }
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            completeCb(errors ? false : true);
        };

        CheckListViewModel.prototype.setInitialStatusOnUnchangedChecklists = function () {
            var taskChecklists = this.getCheckList().filter(function (checkPoint) {
                return checkPoint.isTask();
            });

            taskChecklists.forEach(function (checkPoint) {
                if (checkPoint.ZRESULT_CODE == "") {
                    checkPoint.ZRESULT_CODE = checkPoint._initStatus;
                }
            });
        };

        CheckListViewModel.prototype.setNewChecklistData = function (data) {
            var that = this;

            var checkList = data.Checklist.map(function (checkPoint) {
                var newCheckListPoint = new SAMChecklistPoint(checkPoint, that._requestHelper, that);

                newCheckListPoint.setUserStatuses(that._getUserStatusesByStatusProfile(newCheckListPoint.getStatusProfile()));

                var task = newCheckListPoint.userStatuses.allStatus.find(o => o.USER_STATUS === newCheckListPoint.ZRESULT_CODE);
                if (task) {
                    newCheckListPoint.ZRESULT_CODE_DISPLAY = task.USER_STATUS_CODE;
                } else {
                    newCheckListPoint.ZRESULT_CODE_DISPLAY = "";
                }

                if (parseInt(newCheckListPoint.MRMIN) > 0 && parseInt(newCheckListPoint.MRMAX) > 0) {
                    newCheckListPoint.E_MRMIN = parseFloat(newCheckListPoint.MRMIN).toFixed(2);
                    newCheckListPoint.E_MRMAX = parseFloat(newCheckListPoint.MRMAX).toFixed(2);
                    newCheckListPoint.MINMAXUNIT = newCheckListPoint.MSEHT;
                } else {
                    newCheckListPoint.E_MRMIN = "";
                    newCheckListPoint.E_MRMAX = "";
                    newCheckListPoint.MINMAXUNIT = "";
                }
                newCheckListPoint.lastMeasPointReading = newCheckListPoint.ZRESULT_READING;

                newCheckListPoint.updateIcon();
                newCheckListPoint._updateHasText();
                return newCheckListPoint;
            });

            var aCategories = [];
            var EXPANDED = false;
            
            checkList.forEach(function (checkpoint) {
                var checklists = that.getProperty("/checkList");
                var chObj = checklists.filter(function(ch){
                    return ch.TASK_CODEGRP === checkpoint.TASK_CODEGRP;
                });
                if(chObj.length > 0){
                    EXPANDED = chObj[0].EXPANDED;
                }
                
                var oExistingCategory = aCategories.find(el => el.TASK_CODEGRP === checkpoint.TASK_CODEGRP);
                if (oExistingCategory) {
                    oExistingCategory.checklist.push(checkpoint);
                } else {
                    var oCategory = {
                        TASK_CODEGRP: checkpoint.TASK_CODEGRP,
                        TXT_TASKGRP: checkpoint.TXT_TASKGRP,
                        EXPANDED: EXPANDED,
                        checklist: []
                    };
                    oCategory.checklist.push(checkpoint);
                    aCategories.push(oCategory);
                }
            });
            this.setIsExpanded(this.getIsExpanded());
            this.setProperty("/checkList", aCategories);            
        };

        CheckListViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setNewChecklistData(data);
                that.setBusy(false);
                that._needsReload = false;
                that.refresh(true);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CHECKLISTVIEW_DATA_LOAD")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["Checklist"], {
                orderId: this.getOrderId(),
                notifNo: this.getNotificationNumber(),
                language: this.getLanguage(),
            }, load_success, load_error);
        };


        CheckListViewModel.prototype.loadAttachmentsForChecklistPoint = function (checkListPoint, successCb) {
            var that = this;

            var load_success = function (data) {
                checkListPoint.loadAttachments().then(function () {
                    //all attachments loaded
                    successCb();
                    return;
                });
                checkListPoint.attachments = checkListPoint.attachments.concat(attachments);
                checkListPoint._loaded = true;

                that.setBusy(false);
                that.refresh(true);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CHECKLISTVIEW_DATA_LOAD")));
            };

            if (checkListPoint.hasOwnProperty("_loaded") && checkListPoint._loaded) {
                successCb();
                return;
            }

            this.setBusy(true);
            this._requestHelper.getData("SAMObjectAttachments", {
                objKey: checkListPoint.NOTIF_NO + checkListPoint.TASK_NO,
            }, load_success, load_error);
        };

        CheckListViewModel.prototype.refreshData = function (successCb) {
            this.loadData(successCb);
        };

        CheckListViewModel.prototype.resetData = function () {
            this.setProperty("/view/isExpanded", false);
            this.setProperty("/checkList", []);
            this.refresh();
        };

        CheckListViewModel.prototype._getUserStatusesByStatusProfile = function (statusProfile) {
            var userStatus = this.getCustomizingModelData("UserStatusMl");
            var taskStatus = {};

            taskStatus.allStatus = userStatus.filter(function (status) {
                return status.STATUS_PROFILE == statusProfile && status.USER_STATUS_CODE !== "INIT";
            });

            this.taskStatus[statusProfile] = taskStatus;

            return taskStatus;
        };

        CheckListViewModel.prototype._getSubUserStatuses = function (statusCode, userStatuses, statusProfile) {
            var subStatuses = [];
            var subStatusCodeLength = statusCode.length + 1;
            for (var i = 0; i < userStatuses.length; i++) {
                var status = userStatuses[i];
                if (status.STATUS_PROFILE === statusProfile) {
                    if (status.USER_STATUS_CODE.length === subStatusCodeLength && status.USER_STATUS_CODE.startsWith(statusCode)) {
                        subStatuses.push(status);
                    }
                }
            }

            return subStatuses;
        };

        CheckListViewModel.prototype._getCopy = function (checkListPoints) {
            try {
                return checkListPoints.map(function (point) {
                    return point.getSqlValues();
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        CheckListViewModel.prototype._checkListHasNewAttachment = function () {
            var checklist = this.getCheckList();

            for (var i = 0; i < checklist.length; i++) {
                var checkPoint = checklist[i];

                for (var j = 0; j < checkPoint.attachments.length; j++) {
                    if (!checkPoint.attachments[j].ON_DATABASE) {
                        return true;
                    }
                }
            }

            return false;
        };

        CheckListViewModel.prototype._getNotificationChangedSqlStringsByNotifIds = function (notifIds) {
            var sqlChangeStrings = notifIds.map(function (id) {
                return {
                    type: "update",
                    mainTable: true,
                    tableName: "NotificationHeader",
                    values: [["ACTION_FLAG", "C"]],
                    replacementValues: [],
                    whereConditions: " MOBILE_ID = '" + id + "'",
                    mainObjectValues: []
                };
            });

            return sqlChangeStrings;
        };

        CheckListViewModel.prototype.resetValueStateModel = function () {
            if (!this.valueStateModel) {
                return;
            }

            this.valueStateModel.setProperty("/_MEAS_RESULT", sap.ui.core.ValueState.None);
            this.setProperty("/view/errorMessages", []);
        };

        CheckListViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    _MEAS_RESULT: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        CheckListViewModel.prototype.setExpandAllPanels = function (value) {
            var checkLists = this.getProperty("/checkList");
            for (var checkList in checkLists) {
                checkLists[checkList].EXPANDED = value;
            }
            this.setIsExpanded(value);
            this.refresh(true);
        };

        CheckListViewModel.prototype.onSinglePanelExpandCollapse = function () {
            var checkLists = this.getProperty("/checkList");
            var expObj = false;
            var check = checkLists.filter(function(checklist){
                return checklist.TASK_CODEGRP !== '00001PMC';
            });
            for (var checkList in check) {
                if(check[checkList].EXPANDED){
                    expObj = true;
                    break;
                }
            }

            if(expObj){
                this.setIsExpanded(true);
            }else{
                this.setIsExpanded(false);
            }
        };

        CheckListViewModel.prototype.constructor = CheckListViewModel;

        return CheckListViewModel;
    });
