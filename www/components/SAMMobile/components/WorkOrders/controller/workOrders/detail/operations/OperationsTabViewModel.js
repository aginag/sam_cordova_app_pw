sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/models/dataObjects/OrderOperation",
        "SAMMobile/components/WorkOrders/helpers/Formatter"],

    function (JSONModel, ViewModel, Validator, OrderOperation, Formatter) {
        "use strict";

        // constructor
        function OperationsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this.longTextInfo = null;

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        OperationsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        operations: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setOperations: {
                value: function (operations) {
                    this.setProperty("/operations", operations);
                }
            },

            getOperations: {
                value: function () {
                    return this.getProperty("/operations");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        OperationsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        OperationsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                var operations = data.OrderOperations.map(function (operationData) {
                    return new OrderOperation(operationData, that._requestHelper, that);
                });

                that.setOperations(operations);

                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_OPERATIONS_TAB")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_OPERATIONS_TAB_NO_OPERATION")));
                return;
            }

            this.setBusy(true);
            this.getOrder()._fetchOrderOperations().then(load_success).catch(load_error);
        };

        OperationsTabViewModel.prototype.resetData = function () {
            this.setOperations([]);
            this.setOrder(null);
        };

        OperationsTabViewModel.prototype.constructor = OperationsTabViewModel;

        return OperationsTabViewModel;
    });
