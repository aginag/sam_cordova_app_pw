sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/material/MaterialTabViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/material/EditOrderComponentViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/EditTimeEntryViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/TimeTabViewModel",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/Button",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History",
    "SAMMobile/controller/common/FuncLocHirarchyViewModel",
    "sap/m/MessageBox"
], function (Controller,
             JSONModel,
             formatter,
             formatterComp,
             MaterialTabViewModel,
             EditOrderComponentViewModel,
             EditTimeEntryViewModel,
             TimeTabViewModel,
             Dialog,
             Text,
             Button,
             MessageToast,
             History,
             FuncLocHirarchyViewModel,
             MessageBox) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.overview.OverviewTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailOverview").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailOverview", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = null;
            this.overviewTabViewModel = null;
            this.materialTabViewModel = null;
            this.editOrderComponentViewModel = null;
            this.editTimeEntryViewModel = null;
            this.timeTabViewModel = null;
            this.operationLongTextModel = null;
            this.funcLocTreeViewModel = null;

            this._oLongTextDialog = sap.ui.xmlfragment("longTextDialog", "SAMMobile.components.WorkOrders.view.workOrders.detail.overview.LongTextDialog", this);
            this.getView().addDependent(this._oLongTextDialog);

            this._oHistoryDialog = sap.ui.xmlfragment("orderHistoryDialog", "SAMMobile.components.WorkOrders.view.workOrders.detail.overview.HistoryPopover", this);
            this.getView().addDependent(this._oHistoryDialog);

            // Prevent direct user input for DateTimePicker.
            // Unfortunately DATETIMEPICKER object do not offer such a property, that´s why we need to set the input field as readonly manually
            var oStartDateTimePicker = this.getView().byId("own-start-time");
            var oEndDateTimePicker = this.getView().byId("own-end-time");

            oStartDateTimePicker.addEventDelegate({
                onAfterRendering: function(){
                    var oDateInner = this.$().find('.sapMInputBaseInner');
                    var oID = oDateInner[0].id;
                    $('#'+oID).attr("readonly", "true");
                }}, oStartDateTimePicker);

            oEndDateTimePicker.addEventDelegate({
                onAfterRendering: function(){
                    var oDateInner = this.$().find('.sapMInputBaseInner');
                    var oID = oDateInner[0].id;
                    $('#'+oID).attr("readonly", "true");
                }}, oEndDateTimePicker);
        },

        onRouteMatched: function (oEvent) {
            var oArgs = oEvent.getParameter("arguments");
            var orderId = oArgs.id;
            var operIds = oArgs.operId;
            var persNo = oArgs.persNo;

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.overviewTabViewModel = this.workOrderDetailViewModel.tabs.overview.model;
                // this.materialTabViewModel = this.workOrderDetailViewModel.tabs.material.model;
                // this.timeTabViewModel = this.workOrderDetailViewModel.tabs.time.model;

                this.getView().setModel(this.overviewTabViewModel, "overviewTabViewModel");
                // this.getView().setModel(this.materialTabViewModel, "materialTabViewModel");
                // this.getView().setModel(this.editTimeEntryViewModel, "editTimeEntryViewModel");
                // this.getView().setModel(this.timeTabViewModel, "timeTabViewModel");

                this.getView().setModel(this.overviewTabViewModel.getValueStateModel(), "valueStateModel");
            }

            if (this.workOrderDetailViewModel.needsReload()) {
                this.workOrderDetailViewModel.setOrderId(orderId, operIds, persNo);
                this.workOrderDetailViewModel.loadData(this.loadViewModelData.bind(this));
                return;
            }


            this.getView().setModel(this.editOrderComponentViewModel, "editOrderComponentViewModel");

            this.loadViewModelData();
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailOverview", "refreshRoute", this.onRefreshRoute, this);
        },

        onNavBack: function () {
            var that = this;
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();
            if (sPreviousHash.includes("mapsMarker") || sPreviousHash.includes("calendar")) {
                window.history.go(-1);
            }else if(sPreviousHash.includes("operations")){
                if(this.component.routeNumber == 0){
                    this.oRouter.navTo("workOrderOperations");
                }else{
                    this.component.routeNumber--;
                    var oArr = sPreviousHash.split("/");

                    this.workOrderDetailViewModel.resetData();
                    this.oRouter.navTo("workOrderDetailRoute", {
                        id:  oArr[1],
                        operId: oArr[2],
                        persNo: oArr[3]
                    });
                }
            } else {

                if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                    this.oRouter.navTo("workOrders");
                } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                    this.oRouter.navTo("workOrderOperations");
                }
            }
            if(!sPreviousHash.includes("operations")){
                setTimeout(function () {
                    that.workOrderDetailViewModel.resetData();
                }, 200);
            }
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.overviewTabViewModel.loadData(function() {
                var oZOPTime = that.overviewTabViewModel.getOwnDatesObject();
                if(that.overviewTabViewModel.getOrder().ZFIX_visible && oZOPTime.TERMIN_START && oZOPTime.TERMIN_END){
                    that.overviewTabViewModel.deleteOwnDates(function(){
                        sap.ui.getCore().getEventBus().publish("workOrderDetailOverview", "refreshRoute");
                    });
                }else if(!that.overviewTabViewModel.getOrder().OWN_TIME && oZOPTime.TERMIN_START && oZOPTime.TERMIN_END){
                    that.overviewTabViewModel.deleteOwnDates(function(){
                        sap.ui.getCore().getEventBus().publish("workOrderDetailOverview", "refreshRoute");
                    });
                }else if(that.overviewTabViewModel.getOrder().rejectStatusAvailable && oZOPTime.TERMIN_START && oZOPTime.TERMIN_END){
                    that.overviewTabViewModel.deleteOwnDates(function(){
                        sap.ui.getCore().getEventBus().publish("workOrderDetailOverview", "refreshRoute");
                    });
                }
                that.workOrderDetailViewModel.loadAllCounters();
                that.globalViewModel.setComponentHeaderText(that.workOrderDetailViewModel.getHeaderText());
            });
        },

        onNavigateToLocationPressed: function (oEvent) {
            this.workOrderDetailViewModel.navigateToLocation(oEvent);
        },

        onGISNavigationPressed: function () {
            var funcLocId = this.overviewTabViewModel.getFuncLocId();
            cordova.InAppBrowser.open('https://giswebstg.dewa.gov.ae/mwfgisviewer/?query=MWF_GIS_9311_1,E_EQUIPMENT.FUNCTIONAL_LOCATION,' + funcLocId, '_blank', 'location=yes');
        },

/*        onNavToEquipmentPress: function(oEvent) {
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrderId(),
                            persNo: this.workOrderDetailViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: this.overviewTabViewModel.getEquipmentId(),
                            objectType: "0"
                        })
                    }
                }
            });

            this.workOrderDetailViewModel.resetData();
        },*/

        onNavToFuncLocPress: function (oEvent) {

            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrderId(),
                            operId: this.workOrderDetailViewModel.getOrder().getData().OPERATION_ACTIVITY,
                            persNo: this.workOrderDetailViewModel.getUserInformation().personnelNumber ? this.workOrderDetailViewModel.getUserInformation().personnelNumber : '00000000'
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: this.overviewTabViewModel.getFuncLocId(),
                            objectType: "1"
                        })
                    }
                }
            });

            this.workOrderDetailViewModel.resetData();
        },

        onNavToNotificationPress: function(oEvent) {
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrderId(),
                            operId: this.workOrderDetailViewModel.getOrder().getData().OPERATION_ACTIVITY,
                            persNo: this.workOrderDetailViewModel.getUserInformation().personnelNumber ? this.workOrderDetailViewModel.getUserInformation().personnelNumber : '00000000'
                        })
                    }
                }
            }, {
                route: "notificationsCustomRoute",
                params: {
                    query: {
                        routeName: "notificationsEditRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrder().getNotification().getNotifNo(),
                            outageOnly: true
                        })
                    }
                }
            });

            this.workOrderDetailViewModel.resetData();
        },

        onNavToNotifHistPress: function(oEvent) {
            this.oRouter.navTo("notificationDetailRoute", {
                id: oEvent.getSource().getBindingContext("historyModel").getObject().NOTIF_NO
            }, false);

            this.workOrderDetailViewModel.resetData();
        },

        onDisplayFuncLocHistory: function(oEvent) {
            var src = oEvent.getSource();

            var load_success = function (oModel) {
                this._oHistoryDialog.setModel(oModel, "historyModel");
                this._oHistoryDialog.openBy(src);
            }.bind(this);

            this.overviewTabViewModel.loadFuncLocHistory(load_success);
        },

        onDisplayPMHierarchy: function(oEvent){

            var onAcceptPressed = function (funcLoc, parent) {
                parent.close();
            };

            if (!this.funcLocTreeViewModel) {
                this.funcLocTreeViewModel = new FuncLocHirarchyViewModel(this.component.requestHelper, this.globalViewModel, this.component, false );
            }

            this.funcLocTreeViewModel.setFLParent(this.workOrderDetailViewModel.getOrder().getData().FUNCT_LOC);
            this.funcLocTreeViewModel.setOrderInfo(this.workOrderDetailViewModel.getOrderId(), this.workOrderDetailViewModel.getOrder().getData().OPERATION_ACTIVITY);


            this.openHierarchyTreeDialog(this.funcLocTreeViewModel, onAcceptPressed.bind(this));

        },

/*        onDisplayEquipmentHistory: function(oEvent) {
            var src = oEvent.getSource();
            var that = this;

            var load_success = jQuery.proxy(function (model) {
                that._oHistoryDialog.setModel(model, "historyModel");
                that._oHistoryDialog.openBy(src);
            }, this);

            this.overviewTabViewModel.loadEquipmentHistory(load_success);
        },*/

        goToHistoricalOrder: function (oEvent) {
            var orderId = oEvent.getSource().getBindingContext("historyModel").getObject().ORDERID;
            var navFn = jQuery.proxy(function (persNo) {
                this._oHistoryDialog.close();

                // Get personel number for orderId
                this.workOrderDetailViewModel.resetData();
                this.oRouter.navTo("workOrderDetailRoute", {
                    id: orderId,
                    persNo: persNo
                }, false);

            }, this);

            this.overviewTabViewModel.getPersNoForOrderId(orderId, navFn.bind(this));
        },

        onMakaniEdit: function(oEvent){
         // create popover
		if (!this._oMakaniPopover) {
			this._oMakaniPopover = sap.ui.xmlfragment("SAMMobile.components.WorkOrders.view.workOrders.detail.overview.EditMakaniPopover", this);
			this.getView().addDependent(this._oMakaniPopover);
			this._makaniModel = new JSONModel({
			    makani: this.overviewTabViewModel.getProperty('/cust_info/ZZMAKANI')
			});
			this._oMakaniPopover.setModel(this._makaniModel, 'makaniModel');
		}else{
		    this._makaniModel.setProperty('/makani', this.overviewTabViewModel.getProperty('/cust_info/ZZMAKANI'));
		}

		this._oMakaniPopover.openBy(oEvent.getSource());
        },
        navToMakaniAddress: function(oEvent){
            var makaniData = this._makaniModel.oData, that = this;
            getMakaniInfoFromWsdl(makaniData.makani, function (latlong) {
                if (latlong) {
                    that.workOrderDetailViewModel.navigateToLocation(oEvent, latlong);
                }
            });
            //this.workOrderDetailViewModel.navigateToLocation(oEvent, makaniData.makani);
        },
        onNewOrderComponentPressed: function () {

            var that = this;
            var onAcceptPressed = function (component, dialog) {
                that.materialTabViewModel.insertNewComponent(component, function () {
                    dialog.close();
                    MessageToast.show("Inserted successfully");
                });
            };

            var dialog = this._getComponentEditDialog(that.materialTabViewModel.getNewEditComponent(), false, onAcceptPressed);
            dialog.open();
        },

        _getComponentEditDialog: function (component, bEdit, acceptCb) {

            var onAcceptPressed = function () {
                var editOrderComponentViewModel = this.getParent().getModel("componentModel");

                if (editOrderComponentViewModel.validate()) {
                    acceptCb(editOrderComponentViewModel.getOrderComponent(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editOrderComponentViewModel = dialog.getModel("componentModel");
                editOrderComponentViewModel.rollbackChanges();
                dialog.close();
            };

            this.editOrderComponentViewModel = new EditOrderComponentViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editOrderComponentViewModel.setEdit(bEdit);
            this.editOrderComponentViewModel.setOrderComponent(component);
            this.editOrderComponentViewModel.setOrder(this.materialTabViewModel.getOrder());


            if (!this._matConfPopup) {

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("editOrderComponentPopover2", "SAMMobile.components.WorkOrders.view.workOrders.detail.material.OrderComponentEditPopover", this);
                }

                this._matConfPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._oMcPopoverContent,
                    endButton: new Button({
                        text: '{= ${componentModel>/view/edit} ? ${i18n>update} : ${i18n_core>create}}',
                        icon: "sap-icon://save",
                        press: onAcceptPressed
                    }),
                    beginButton: new Button({
                        text: '{i18n_core>cancel}',
                        icon: "sap-icon://decline",
                        press: onCancelPressed
                    }),
                    afterClose: function () {

                    }
                });

                this._matConfPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._matConfPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._matConfPopup.setTitle(this.editOrderComponentViewModel.getDialogTitle());
            this._matConfPopup.getEndButton().mEventRegistry.press = [];
            this._matConfPopup.getEndButton().attachPress(onAcceptPressed);

            this._matConfPopup.setModel(this.editOrderComponentViewModel, "componentModel");
            this._matConfPopup.setModel(this.editOrderComponentViewModel.getValueStateModel(), "valueStateModel");

            return this._matConfPopup;
        },

        onAddTimeEntryPressed: function () {
            var that = this;
            var onAcceptPressed = function (timeEntry, dialog) {
                that.timeTabViewModel.insertNewTimeEntry(timeEntry, function () {
                    dialog.close();
                    MessageToast.show("Inserted successfully");
                });
            };

            var openDialog = function (colleagues) {
                var dialog = that._getTimeEntryEditDialog(that.timeTabViewModel.getNewEditTimeEntry(), colleagues, false, onAcceptPressed);
                dialog.open();
            };

            try {
                this.timeTabViewModel.getColleagues(openDialog);
            } catch (err) {
                openDialog([]);
            }

        },

        onHoursChanged: function (oEvent) {
            this.editTimeEntryViewModel.setHours(oEvent.getParameter("value"));
        },


        onStartTimeChanged: function (oEvent) {
            this.editTimeEntryViewModel.setStartTime(oEvent.getParameter("value"));
        },

        onEndTimeChanged: function (oEvent){
            this.editTimeEntryViewModel.setEndTime(oEvent.getParameter("value"));
        },

        _getTimeEntryEditDialog: function (timeEntry, colleagues, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");

                if (editTimeEntryViewModel.validate()) {
                    acceptCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editTimeEntryViewModel = dialog.getModel("timeEntryModel");
                editTimeEntryViewModel.rollbackChanges();
                dialog.close();
            };

            this.editTimeEntryViewModel = new EditTimeEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTimeEntryViewModel.setEdit(bEdit);
            this.editTimeEntryViewModel.setTimeEntry(timeEntry);
            this.editTimeEntryViewModel.setColleagues(colleagues);

            if (!this._timeEntryEditPopup) {

                if (!this._timeEntryPopover) {
                    this._timeEntryPopover = sap.ui.xmlfragment("editTimeEntryPopover2", "SAMMobile.components.WorkOrders.view.workOrders.detail.time.TimeEntryEditPopover", this);
                }

                this._timeEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._timeEntryPopover,
                    beginButton: new Button({
                        text: '{= ${timeEntryModel>/view/edit} ? ${i18n_core>update} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {

                    }
                });

                this._timeEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._timeEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._timeEntryEditPopup.setTitle(this.editTimeEntryViewModel.getDialogTitle());
            this._timeEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._timeEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel, "timeEntryModel");
            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel.getValueStateModel(), "valueStateModel");

            return this._timeEntryEditPopup;
        },

        openHierarchyTreeDialog: function (viewModel, acceptCb) {
            var that = this;
            var onReloadSuccess = function () {
                this._oFuncLocTreeView.setModel(viewModel, "funcLocTreeViewModel");
                viewModel.getTree().setBusy(false);
                this._oFuncLocTreeView.open();
            };

            if (!this._oFuncLocTreeView) {

                var onNewNotificationPressed = function(){
                    sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                        route: "workOrdersCustomRoute",
                        params: {
                            query: {
                                routeName: "workOrderDetailRoute",
                                params: JSON.stringify({
                                    id: that.workOrderDetailViewModel.getOrder().getOrderId(),
                                    operId: that.workOrderDetailViewModel.getOrder().getOperationActivity(),
                                    persNo: that.workOrderDetailViewModel.getOrder().getPersNo()
                                })
                            }
                        }
                    }, {
                        route: "notificationsCustomRoute",
                        params: {
                            query: {
                                routeName: "notificationsCreationRoute",
                                params: JSON.stringify({
                                    funclocId: that.funcLocTreeViewModel.getSelectedFuncloc() ? that.funcLocTreeViewModel.getSelectedFuncloc().id : that.funcLocTreeViewModel.getSelectedFuncloc(),
                                    funclocTxt: that.funcLocTreeViewModel.getSelectedFuncloc() ? that.funcLocTreeViewModel.getSelectedFuncloc().text : that.funcLocTreeViewModel.getSelectedFuncloc(),
                                })
                            }
                        }
                    });
                };

                if (!this._oMcPopoverContent) {
                    this._oMcPopoverContent = sap.ui.xmlfragment("funcLocTreeViewPopover-operation", "SAMMobile.view.common.FuncLocHirarchy", viewModel);
                }

                this._oFuncLocTreeView = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "{i18n>technObjInfo}",
                    content: this._oMcPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    beginButton: new Button({
                        text: '{i18n_core>cancel}',
                        icon: "sap-icon://decline",
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                        viewModel.getTree().getBinding("items").filter(null);
                        viewModel.getTree().removeSelections();
                        viewModel.getTree().collapseAll();
                        viewModel.setSelectedFuncloc(null);
                        viewModel.setProperty("/searchString", "");
                    }
                });
                this.getView().addDependent(this._oFuncLocTreeView);
                this._oFuncLocTreeView.setModel(this.component.getModel('i18n'), "i18n");
                this._oFuncLocTreeView.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }


            viewModel.setTree(sap.ui.core.Fragment.byId("funcLocTreeViewPopover-operation", "TreeObjects"));
            viewModel.setHierarchy(true);
            viewModel.setToolbar(false);
            viewModel.refreshData(onReloadSuccess.bind(this));
        },
        

        displayActTypeSelectDialog: function (oEvent) {
            this.editTimeEntryViewModel.displayActTypeDialog(oEvent, this);
        },
        handleActTypeSelect: function (oEvent) {
            this.editTimeEntryViewModel.handleActTypeSelect(oEvent);
        },
        handleActTypeSearch: function (oEvent) {
            this.editTimeEntryViewModel.handleActTypeSearch(oEvent);
        },

        displayMaterialSearch: function (oEvent) {
            this.editOrderComponentViewModel.displayMaterialDialog(oEvent, this);
        },

        handleMaterialSearch: function (oEvent) {
            this.editOrderComponentViewModel.handleMaterialSearch(oEvent);
        },

        handleMaterialSelect: function (oEvent) {
            this.editOrderComponentViewModel.handleMaterialSelect(oEvent);
        },

        displayStorageLocSelectDialog: function (oEvent) {
            this.editOrderComponentViewModel.displayStorageLocationDialog(oEvent, this);
        },

        onNavToMapMarkerPress: function () {
            this.oRouter.navTo("mapsMarkerRoute");
        },

        onNavigateToEquipmentPressed: function(){
            this.oRouter.navTo("mapsMarkerRoute", {
                    query: {
                        equipment: this.workOrderDetailViewModel.getOrder().EQUIPMENT,
                        orderid: this.workOrderDetailViewModel.getOrder().ORDERID
                    }
                }
            );
        },

        onAddLongTextPressed: function (oEvent) {
            var oOperation = oEvent.getSource().getBindingContext("overviewTabViewModel").getObject();

            if(!this.operationLongTextModel) {
                this.operationLongTextModel = new JSONModel();
            }

            this.operationLongTextModel.setData(oOperation);
            this._oLongTextDialog.setModel(this.operationLongTextModel, "longTextModel");

            this._oLongTextDialog.open();
        },

        closeAddLongTextDialog: function () {
            this._oLongTextDialog.close();
        },

        onAdditionalLongtextSavePress: function () {
            this.overviewTabViewModel.saveAdditionalOperationLongtext(this.operationLongTextModel.getData(), function() {
                MessageToast.show(this.component.getModel('i18n').getProperty("operationsLongtextAddSuccess"));
                this.closeAddLongTextDialog();
            }.bind(this));
        },

        onSaveOwnDatesPressed: function () {
            if (this.overviewTabViewModel.validate()) {
                this.overviewTabViewModel.saveOwnDates(function () {
                    MessageToast.show(this.component.i18n.getText("FORM_OWN_DATES_SAVE_SUCCESS_TEXT"));
                    sap.ui.getCore().getEventBus().publish("workOrderDetailRoute", "refreshRoute");
                }.bind(this));
            }
        },

        onDeleteOwnDatesPressed: function () {
            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("FORM_OWN_DATES_DELETE_CONFIRMATION_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("FORM_OWN_DATES_DELETE_CONFIRMATION_TITLE"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        this.overviewTabViewModel.deleteOwnDates(function () {
                            MessageToast.show(this.component.i18n.getText("FORM_OWN_DATES_DELETE_SUCCESS_TEXT"));
                            sap.ui.getCore().getEventBus().publish("workOrderDetailRoute", "refreshRoute");
                        }.bind(this));
                    }
                }.bind(this)
            });
        }

    });

});