sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/OrderLongtext"],

    function (JSONModel, ViewModel, Validator, Formatter, OrderLongtext) {
        "use strict";

        // constructor
        function DescriptionTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this.longTextInfo = null;

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this.workOrderDetailViewModel = null;
            this.addInfo = "";

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        DescriptionTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        longText: " ",
                        longTextOp: " ",
                        notifLongText: " ",
                        notifExist: false,
                        //longTextToAdd: "",
                        readOnly: true,
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        DescriptionTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        DescriptionTabViewModel.prototype.saveAdditionalLongtext = function (successCb, workOrderDetailViewModel) {
            var that = this;
            var typeObject;
            this.workOrderDetailViewModel = workOrderDetailViewModel;
            typeObject = this.getOrder().getOrderOperations();

            if (that.addInfo !== undefined) {
                var completeLongTextToAdd = that.addInfo + "\n" + that.oData.longTextToAdd;

                that.oData.longTextToAdd = completeLongTextToAdd;

                that.addInfo = '';
            }


            if (!typeObject) {
                this.executeErrorCallback(new Error("Longtext could not be saved"));
                return;
            }

            var fnSuccess = function () {
                var strings = typeObject.addLongtext(true, that.getProperty("/longTextToAdd"), "AVOT");

                try {
                    that._requestHelper.executeStatementsAndCommit(strings, function () {
                        that.loadData(successCb);
                    }, that.executeErrorCallback.bind(that, new Error("Error while trying to insert OperationsLongtext")));
                } catch (err) {
                    that.executeErrorCallback(err);
                }

                that.refresh();
            }

            // remove unsync longtext already saved
            typeObject.removeUnsyncLongtext(false, "AVOT", fnSuccess, this.executeErrorCallback.bind(this, new Error("Error while trying to delete OperationsLongtext")));


        };

        DescriptionTabViewModel.prototype.saveAdditionalNotifLongtext = function (successCb, workOrderDetailViewModel) {
            var that = this;
            var typeObject;
            this.workOrderDetailViewModel = workOrderDetailViewModel;

            typeObject = this.getOrder().getNotification();

            if (!typeObject) {
                this.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("LONGTEXT_COULD_NOT_BESAVED")));
                return;
            }

            var strings = typeObject.addLongtext(true, this.getProperty("/longTextNotifToAdd"), "QMEL");

            try {
                this._requestHelper.executeStatementsAndCommit(strings, that.loadData.bind(this, successCb), this.executeErrorCallback.bind(this, new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_INSERT_NOTIFLONGTEXT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }

            this.refresh();
        };

        DescriptionTabViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var oQueue = [];
            //this.longTextInfo = this.scenario.getDescriptionTabLongtextInformation();
            this.setProperty("/notifExist", false);

            var load_success = function (data) {
                that.setProperty("/longText", data);
                that.setProperty("/longTextToAdd", "");
                that.setProperty("/longTextNotifToAdd", "");

                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_success_op = function (data) {

                var allUnSyncLongtext = [];
                var allSyncLongtext = [];

                allUnSyncLongtext = data.filter(function (longText) {
                    return (longText.ACTION_FLAG === 'N');
                });

                allSyncLongtext = data.filter(function (longText) {
                    return (longText.ACTION_FLAG !== 'N');
                });

                if (allUnSyncLongtext.length === 0) {
                    that.addInfo = moment().format('DD-MM-YYYY HH:mm:ss') + " " +
                        that._globalViewModel.model.oData.personnelNumber + "\n" +
                        that._globalViewModel.model.oData.firstName + " " +
                        that._globalViewModel.model.oData.lastName;
                }

                that.setProperty("/longTextOp", OrderLongtext.formatToText(allSyncLongtext));
                that.setProperty("/longTextToAdd", OrderLongtext.formatToText(allUnSyncLongtext));

                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_success_nt = function (data) {
                that.setProperty("/notifLongText", data);

                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_DESCRIPTIONTAB")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_DESCRIPTIONTAB_NO_ORDER_SET")));
                return;
            }

            this.setBusy(true);
            oQueue.push(this.getOrder().loadLongText("AUFK", load_success));

            // Load operation long text
            var orderOperation = this.getOrder().getOrderOperations();

            if (orderOperation) {
                this.setBusy(true);
                oQueue.push(orderOperation.loadLongText("AVOT", load_success_op));
            }

            // Load notification long text
            var notification = this.getOrder().getNotification();

            if (notification && notification.NOTIF_NO) {
                this.setBusy(true);
                this.setProperty("/notifExist", true);
                oQueue.push(notification.loadLongText("QMEL").then(load_success_nt));
            }

            Promise.all(oQueue).then(function () {
                if (this.workOrderDetailViewModel) {
                    this.workOrderDetailViewModel.setProperty("/nbLongtext", this.countLongtext());
                }
            }.bind(this));

        };

        DescriptionTabViewModel.prototype.countLongtext = function () {
            var iCounter = 0;

            if (this.getProperty("/notifLongText")) {
                iCounter++;
            }
            if (this.getProperty("/longTextOp")) {
                iCounter++;
            }
            if (this.getProperty("/longText")) {
                iCounter++;
            }

            return iCounter;

        };

        DescriptionTabViewModel.prototype.resetData = function () {
            this.setProperty("/longText", "");
            this.setProperty("/notifLongText", "");
            this.setOrder(null);
        };

        DescriptionTabViewModel.prototype.constructor = DescriptionTabViewModel;

        return DescriptionTabViewModel;
    });
