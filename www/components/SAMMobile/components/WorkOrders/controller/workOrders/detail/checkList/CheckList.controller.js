sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/checkList/CheckListViewModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/components/WorkOrders/helpers/AttachmentHelper",
    "sap/m/Button",
    "sap/m/Dialog",
    'sap/m/Text',
    'sap/m/TextArea',
    "sap/ui/core/format/NumberFormat"
], function (JSONModel,
             Controller,
             CheckListViewModel,
             MessageBox,
             MessageToast,
             formatter,
             formatterComp,
             fileHelper,
             FileSystem,
             MediaAttachmentsController,
             AttachmentHelper,
             Button,
             Dialog,
             Text,
             TextArea,
             NumberFormat) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.checkList.CheckList", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderCheckList").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderCheckListRoute", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
            this.attachmentHelper = new AttachmentHelper(this.component.requestHelper);

            this.attachmentHelper.setErrorCallback(this.onViewModelError.bind(this));

            this.checkListViewModel = new CheckListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.checkListViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.fileSystem = new FileSystem();
            this.delayedMeasurementPointTextSave = null;
            this.workOrderDetailViewModel = null;
        },

        onRouteMatched: function (oEvent) {
            var that = this;
            var oArgs = oEvent.getParameter("arguments");

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            this.checkListViewModel.setOrderId(oArgs.id);
            this.checkListViewModel.setNotificationNumber(oArgs.notifNo);
            this.checkListViewModel.setPersNo(oArgs.persNo);
            this.checkListViewModel.setHasChanges(false);

            this.checkListViewModel.resetValueStateModel();
            this.getView().setModel(this.checkListViewModel, "checkListViewModel");
            this.getView().setModel(this.checkListViewModel.getValueStateModel(), "valueStateModel");

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.getView().setModel(this.workOrderDetailViewModel, "workOrderDetailViewModel");
            }

            this.loadViewModelData(false);
            this._tryCreateUserAttachmentsControl();
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderCheckListRoute", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            jQuery.sap.log.info("PriSec - Checklist Controller - Refresh Event triggered");
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync, successCb) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;


            var onReloadSuccess = function (order) {
                //that.globalViewModel.setComponentHeaderText(that.checkListViewModel.getHeaderText());
                that.workOrderDetailViewModel.loadAllCounters();
                if (successCb) {
                    successCb();
                }
            };

            if (bSync) {
                this.checkListViewModel.refreshData(onReloadSuccess);
            } else {
                this.checkListViewModel.loadData(onReloadSuccess);
            }

        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var fileArr = oEvent.getParameter("files");

            for (let index = 0; index < fileArr.length; index++) {
                var file = fileArr[index];
                let name = file.name;
                var reader = new FileReader();
                reader.onload = function (readerEvt) {
                    binaryString = readerEvt.target.result.split(",")[1];
                    fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);

                    // On windows, we have to get the fileExt based on the file name
                    // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#browser-quirks section "readAsDataURL"
                    if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                        var fileExt = name.split(".").pop();
                    } else {
                        var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                    }

                    var completeFileName = name;
                    var indexOfPoint = completeFileName.lastIndexOf('.');
                    var fileName = completeFileName.slice(0, indexOfPoint);
                    fileName = that.mediaAttachmentsController.getConvertedFilename(fileName);
                    that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                };
                reader.onerror = function (error) {
                    MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                };

                reader.readAsDataURL(file);
                //      var path = (window.URL || window.webkitURL).createObjectURL(file);
            }
            oEvent.getSource().clear();

        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;
            var notifObject = this.workOrderDetailViewModel.getOrder().getNotification();

            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = notifObject.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                setTimeout(function() {
                    that.workOrderDetailViewModel.loadAllCounters();
                }, 200);
            };

            var onWriteError = function (msg) {
                MessageToast.show(msg);
            };

            this.fileSystem.write(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            var notifObject = this.workOrderDetailViewModel.getOrder().getNotification();
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];
                    notifObject.addAttachment(attachmentObj);

                    that.checkListViewModel.insertUserAttachment(attachmentObj, function () {
                        MessageToast.show(that.component.i18n.getText("NOTIFICATION_ATTACHMENT_INSERT_SUCCESS_TEXT"));
                        that.refreshAttachmentsDialogModel();
                    });

                }
            } else {
                that.checkListViewModel.insertUserAttachment(mediaObject, function () {
                    MessageToast.show(that.component.i18n.getText("NOTIFICATION_ATTACHMENT_INSERT_SUCCESS_TEXT"));
                    notifObject.addAttachment(mediaObject);
                    that.refreshAttachmentsDialogModel();
                });
            }

        },

        //region private functions
        _tryCreateUserAttachmentsControl: function () {
            var me = this;

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {
                    var notificationObject = me.workOrderDetailViewModel.getOrder().getNotification();

                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = notificationObject.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    notificationObject.addAttachment(attachmentObj);
                    me.checkListViewModel.insertUserAttachment(attachmentObj, me.refreshAttachmentsDialogModel.bind(me));
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {
                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));

                    me.checkListViewModel.updateUserAttachment(attachment, function () {
                        MessageToast.show(me.component.i18n.getText("NOTIFICATION_ATTACHMENT_UPDATE_SUCCESS_TEXT"));
                        me.refreshAttachmentsDialogModel();
                    });
                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {
                    var attachmentObj = bCtx.getObject();
                    var attachments = me.workOrderDetailViewModel.getOrder().getNotification().getAttachments();


                    me.checkListViewModel.deleteUserAttachment(attachmentObj, function () {
                        for (var i = 0; i < attachments.length; i++) {
                            if (attachments[i].FILENAME == attachmentObj.FILENAME) {
                                attachments.splice(i, 1);
                                break;
                            }
                        }

                        MessageToast.show(me.component.i18n.getText("NOTIFICATION_ATTACHMENT_DELETE_SUCCESS_TEXT"));
                        me.workOrderDetailViewModel.loadAllCounters();
                        me.refreshAttachmentsDialogModel();
                    });

                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };
            
        },

        _onCapturePhotoPress: function (oEvent) {
            var that = this;
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");

            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, null, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureVideoPress: function (oEvent) {
            var that = this;
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");

            this.mediaAttachmentsController._onCaptureVideoPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureAudioPress: function (oEvent) {
            var that = this;
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");

            this.mediaAttachmentsController._onCaptureAudioPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getParameter("listItem").getBindingContext("checkListAttachmentsModel");
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

        _onEditAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("checkListAttachmentsModel");
            this.mediaAttachmentsController._onEditAttachment(null, bContext);
        },

        _onDeleteAttachment: function (oEvent) {
            var that = this;
            var bContext = oEvent.getSource().getBindingContext("checkListAttachmentsModel");

            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        this.mediaAttachmentsController._onDeleteAttachment(null, bContext, function () {
                        });
                    }
                }.bind(this)
            });
        },

        onNavBack: function () {
            var that = this;
            if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                this.oRouter.navTo("workOrders");
            } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                this.oRouter.navTo("workOrderOperations");
            }

            setTimeout(function () {
                that.workOrderDetailViewModel.resetData();
                that.checkListViewModel.resetData();
            }, 200);
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        onCheckListStatusSelect: function (oEvent) {
            var that = this;

            var selectedStatus = oEvent.getSource().getSelectedKey();
            var context = oEvent.getSource().getBindingContext("checkListViewModel");
            var checkListObject = context.getObject();
            var task = checkListObject.userStatuses.allStatus.find(o => o.USER_STATUS === checkListObject.ZRESULT_CODE);
            if(task){
                checkListObject.ZRESULT_CODE_DISPLAY = task.USER_STATUS_CODE;
            }else{
                checkListObject.ZRESULT_CODE_DISPLAY = "";
            }
            
            checkListObject.updateIcon();
            if (!oEvent.getParameter("wasPressed")) {
                // update or insert selected status into options
                checkListObject.selectedUserStatus = selectedStatus;
                if (checkListObject.userStatuses.hasOwnProperty("level2") && checkListObject.userStatuses.level2.length > 0 && checkListObject.userStatuses.level2[0].USER_STATUS_CODE.startsWith(selectedStatus)) {
                    checkListObject.userStatuses.currentLevel = 2;
                    checkListObject.userStatuses.currentLevelSet = checkListObject.userStatuses.level2;
                    this.onOpenSubStatusPopover(context, oEvent.getSource());
                } else {
                    this.resetCheckListObjectFC(checkListObject);
                    this.getCheckListStatusPopover().close();
                }
            } else {
                this.resetCheckListObjectFC(checkListObject);
                this.getCheckListStatusPopover().close();
            }

            // var task = checkListObject.userStatuses.allStatus.find(o => o.USER_STATUS_CODE === checkListObject.ZRESULT_CODE_DISPLAY);

            if (task) {
                checkListObject.ZRESULT_CODE = task.USER_STATUS;

            } else {
                checkListObject.ZRESULT_CODE = "";
            }
            // checkListObject.updateIcon();

            this.checkListViewModel.updateChecklistPoint(checkListObject, function () {
            });
        },

        onOpenSubStatusPopover: function (context, oSource) {
            var popover = this.getCheckListStatusPopover();
            popover.setBindingContext(context, "checkListViewModel");
            popover.openBy(oSource);
        },

        resetCheckListObjectFC: function (checkListObject) {
            checkListObject.TASK_TEXT = "";
            this.checkListViewModel.refresh();
        },

        getCheckListStatusPopover: function (oEvent) {
            // create popover
            if (!this._oPopover) {
                this._oPopover = sap.ui.xmlfragment("checkListStatusPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.checkList.CheckListStatusPopover", this);
                this.getView().addDependent(this._oPopover);
                this._oPopover.setModel(this.getView().getModel("checkListViewModel"), "checkListViewModel");
            }

            return this._oPopover;
        },

        onCheckListSubStatusPress: function (oEvent) {
            var context = oEvent.oSource.getBindingContext("checkListViewModel");
            var checkListObject = context.getObject();
            var popover = this.getCheckListStatusPopover();
            var selectedItem = oEvent.getParameter("listItem").getBindingContext("checkListViewModel").getObject();

            if (checkListObject.userStatuses.currentLevel === 2) {
                if (selectedItem.subStatuses.length > 0) {
                    checkListObject.userStatuses.currentLevel = 3;
                    checkListObject.userStatuses.currentLevelSet = selectedItem.subStatuses;
                    checkListObject.fieldControl.level2Select = selectedItem.USER_STATUS_CODE;
                    // update or insert status object inside options
                } else {
                    // TODO close popover and log level2 selection
                    checkListObject.fieldControl.level2Select = selectedItem.USER_STATUS_CODE;
                    // update or insert status object inside options
                    popover.close();
                }
            } else if (checkListObject.userStatuses.currentLevel === 3) {
                if (checkListObject.userStatuses.hasOwnProperty("level4")) {
                    checkListObject.userStatuses.currentLevel = 4;
                    checkListObject.userStatuses.currentLevelSet = selectedItem.subStatuses;
                    checkListObject.fieldControl.level3Select = selectedItem.USER_STATUS_CODE;
                    // update or insert status object inside options
                } else {
                    // TODO close popover and log level3 selection
                    checkListObject.fieldControl.level3Select = selectedItem.USER_STATUS_CODE;
                    // update or insert status object inside options
                    popover.close();
                }
            } else if (checkListObject.userStatuses.currentLevel === 21) {
                // update status object inside options
                checkListObject.fieldControl.level2Select = selectedItem.USER_STATUS_CODE;
                checkListObject.fieldControl.level3Select = "";
                checkListObject.userStatuses.currentLevel = 3;
                if (selectedItem.subStatuses && selectedItem.subStatuses.length > 0) {
                    checkListObject.userStatuses.currentLevelSet = selectedItem.subStatuses;
                } else {
                    popover.close();
                }
            } else if (checkListObject.userStatuses.currentLevel === 31) {
                // update status object inside options
                checkListObject.fieldControl.level3Select = selectedItem.USER_STATUS_CODE;
                popover.close();
            }

            checkListObject.ZRESULT_CODE = selectedItem.USER_STATUS;
            this.checkListViewModel.updateChecklistPoint(checkListObject, function () {
            });
            this.checkListViewModel.refresh();
        },

        onSubStatusL2EditPress: function (oEvent) {
            var context = oEvent.oSource.getBindingContext("checkListViewModel");
            var checkListObject = context.getObject();

            checkListObject.userStatuses.currentLevel = 21;
            checkListObject.userStatuses.currentLevelSet = checkListObject.userStatuses.level2;

            this.onOpenSubStatusPopover(context, oEvent.getSource());

            this.checkListViewModel.refresh();
        },

        onSubStatusL3EditPress: function (oEvent) {
            var context = oEvent.oSource.getBindingContext("checkListViewModel");
            var checkListObject = context.getObject();
            var level2Select = checkListObject.fieldControl.level2Select;
            checkListObject.userStatuses.currentLevel = 31;

            for (var i = 0; i < checkListObject.userStatuses.level2.length; i++) {
                var status = checkListObject.userStatuses.level2[i];
                if (status.USER_STATUS_CODE === level2Select) {
                    checkListObject.userStatuses.currentLevelSet = status.subStatuses;
                }
            }

            this.onOpenSubStatusPopover(context, oEvent.getSource());

            this.checkListViewModel.refresh();
        },

        onSaveCheckListPressed: function () {
            var that = this;
            this.checkListViewModel.validate(function (bValidated) {

                if (!bValidated) {
                    return;
                }

                that.checkListViewModel.saveCheckList(function () {
                    MessageToast.show(that.component.i18n.getText("SAVED_SUCCESSFULLY"));
                    that.checkListViewModel.resetValueStateModel();
                });
            });

        },

        onEditUserTextPressed: function (oEvent) {
            var that = this;
            var checkListPoint = oEvent.getSource().getBindingContext("checkListViewModel").getObject();

            checkListPoint.setUserText(oEvent.getParameter("newValue"));
            that.checkListViewModel.updateChecklistPoint(checkListPoint, function () {
                that.checkListViewModel.refresh();
            });
        },

        onMeasurementCheckpointTextEntry: function (oEvent) {
            var checkListPoint = oEvent.getSource().getBindingContext("checkListViewModel").getObject();
            var value = oEvent.getParameter("value");

            clearTimeout(this.delayedMeasurementPointTextSave);
            this.delayedMeasurementPointTextSave = setTimeout(this.validateSaveMeasurmentPointText.bind(this, checkListPoint, value), 200);
        },

        validateSaveMeasurmentPointText: function (checklistPoint, value) {
            var that = this;
            checklistPoint.setResultReading(value);
            this.checkListViewModel.validate(function (bValidated) {

                if (!bValidated) {
                    return;
                }

                that.checkListViewModel.updateChecklistPoint(checklistPoint, function () {
                    that.checkListViewModel.refresh();
                });
            });
        },

        onLocalFileSelected: function (oEvent) {
            var that = this;
            var evt = oEvent;
            var checkListPoint = this.getTaskAttachmentDialog().getModel("checkListAttachmentsModel").getData().checkListPoint;
            var file = oEvent.getParameter("files")[0];

            this.checkListViewModel.onFileSelection(evt);

            this.attachmentHelper.addFileFromSystem(file, attachmentObject, function () {
                checkListPoint.addAttachment(attachmentObject);
                that.checkListViewModel.insertUserAttachment(attachmentObject, function () {
                    checkListPoint.updateAttachmentCount(checkListPoint.getAttachmentCount() + 1);
                    that.refreshAttachmentsDialogModel();
                    that.checkListViewModel.refresh();
                });
            });
        },

        onOpenAttachmentsDialog: function (oEvent) {
            var that = this;
            var checkList = this.workOrderDetailViewModel.getOrder().getNotification();

            checkList.loadAttachments(true).then(function () {
                //all attachments loaded
                that.getTaskAttachmentDialog(checkList).open();
            });
        },

        onCloseTaskAttachmentsDialog: function () {
            this.getTaskAttachmentDialog().close();
            this.checkListViewModel.onPropertyChanged(null, null, true);
        },

        // SelectDialogs
        handleCodeMlSearch: function (oEvent) {
            this.checkListViewModel.handleCodeMlSearch(oEvent);
        },

        handleCodeMlSelect: function (oEvent) {
            var that = this;
            this.checkListViewModel.handleCodeMlSelect(oEvent, function () {
                that.checkListViewModel.refresh();
            });
        },

        refreshAttachmentsDialogModel: function () {
            this.getTaskAttachmentDialog().getModel("checkListAttachmentsModel").refresh(true);
        },

        onCloseTaskNotesDialog: function (oEvent) {
            oEvent.getSource().getParent().close();
        },

        getTaskAttachmentDialog: function (checkListPoint) {
            if (!this._oDialog) {
                this._oDialog = sap.ui.xmlfragment("taskAttachmentDialog", "SAMMobile.components.WorkOrders.view.workOrders.detail.checkList.CheckListAttachments", this);
                this.getView().addDependent(this._oDialog);
            }

            if (checkListPoint) {
                this._oDialog.setModel(new JSONModel({
                    checkListPoint: checkListPoint,
                    attachments: checkListPoint.getAttachments()
                }), "checkListAttachmentsModel");
            }

            return this._oDialog;
        },

        handleTypeMissmatch: function (oEvent) {
            var aFileTypes = ["jpeg", "png", "jpg"];
            jQuery.each(aFileTypes, function (key, value) {
                aFileTypes[key] = "*." + value;
            });
            MessageBox.warning(this.oBundle.getText("fileTypeNotSupported"));
        },

        onStandardValuePress: function () {
       
            var that = this;
            var checklistGroup = this.checkListViewModel.getProperty("/checkList");

            checklistGroup.forEach(function(checklistObject){
                if(checklistObject.TASK_CODEGRP != "00001PMC"){
                    checklistObject.checklist.forEach(function(checklist){
                        checklist.ZRESULT_CODE = checklist.userStatuses.allStatus.length > 0 ? checklist.userStatuses.allStatus[0].USER_STATUS : "";
                        checklist.ZRESULT_CODE_DISPLAY = checklist.userStatuses.allStatus.length > 0 ? checklist.userStatuses.allStatus[0].USER_STATUS_CODE : ""; 
                        checklist.updateIcon();
                        that.checkListViewModel.updateChecklistPoint(checklist, function() {});
                    });
                }
            });

            this.checkListViewModel.refresh(true);

        },
        onRemoveStandardValues: function(){

            var that = this;
            var checklistGroup = this.checkListViewModel.getProperty("/checkList");

            checklistGroup.forEach(function(checklistObject){
                if(checklistObject.TASK_CODEGRP != "00001PMC"){
                    checklistObject.checklist.forEach(function(checkpoint){
                        checkpoint.ZRESULT_CODE = "";
                        checkpoint.ZRESULT_CODE_DISPLAY = "";
                        checkpoint.updateIcon();
                        that.resetCheckListObjectFC(checkpoint);
                        that.checkListViewModel.updateChecklistPoint(checkpoint);
                    });
                }
            });

            this.checkListViewModel.refresh(true);
        },

        expandAll: function () {
            //Test
            this.checkListViewModel.setExpandAllPanels(true);
        },

        collapseAll: function () {
            this.checkListViewModel.setExpandAllPanels(false);
        },

        onSinglePanelExpandCollapse: function () {
            this.checkListViewModel.onSinglePanelExpandCollapse();
        },

        onOpenCustomAttachmentsDialog: function (oEvent) {
            var data = [];
            this.workOrderDetailViewModel.loadCustomAttachments().then(function (data) {
                this.getCustomAttachmentDialog(data).open();
            }.bind(this));
        },

        getCustomAttachmentDialog: function (data) {
            if (!this._oCustomDialog) {
                this._oCustomDialog = sap.ui.xmlfragment("attachmentDialog", "SAMMobile.components.WorkOrders.view.common.Attachments", this);
                this.getView().addDependent(this._oCustomDialog);
            }

            if (data) {
                this._oCustomDialog.setModel(new JSONModel({
                    attachments: data
                }), "customAttachmentsModel");
            }

            return this._oCustomDialog;
        },


        onCloseCustomAttachmentsDialog: function () {
            this.getCustomAttachmentDialog().close();
        },

        _onOpenCustomAttachment: function (oEvent) {
            var bContext = oEvent.getParameter("listItem").getBindingContext("customAttachmentsModel");
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },
    });

});
