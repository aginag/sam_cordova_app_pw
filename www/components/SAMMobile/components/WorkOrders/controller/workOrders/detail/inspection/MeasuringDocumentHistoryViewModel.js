sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/models/dataObjects/MeasurementDocument"],

    function (JSONModel, ViewModel, MeasurementDocument) {
        "use strict";

        // constructor
        function MeasuringDocumentHistoryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._OBJECTID = "";
            this._OBJECT = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MeasuringDocumentHistoryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        points: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setDocuments: {
                value: function (documents) {
                    this._DOCUMENTS = documents;
                }
            },

            getDocuments: {
                value: function () {
                    return this._DOCUMENTS;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        MeasuringDocumentHistoryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MeasuringDocumentHistoryViewModel.prototype.setModelData = function (data) {
            this.setProperty("/documents", data);
        };

        MeasuringDocumentHistoryViewModel.prototype.loadData = function (point, successCb) {
            var that = this;

            var load_success = function (data) {

                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while loading data for overview tab"));
            };

            this.setBusy(true);
            this._requestHelper.getData("MeasurementDocuments", {
                point: point
            }, load_success, load_error, true);
        };

        MeasuringDocumentHistoryViewModel.prototype.getNewMeasurementDocument = function (point) {

            var newMeasurementDocumentData = new MeasurementDocument(null, this._requestHelper, this);

            newMeasurementDocumentData.POINT = point.EQUIPMENT_MEASUREMENT_POINT;
            newMeasurementDocumentData.RECDV = null;
            newMeasurementDocumentData.ERDAT = moment(new Date()).format("YYYY-MM-DD HH:mm:ss.SSS");
            //newMeasurementDocumentData.CODGR = point.CODGR;
            newMeasurementDocumentData.RECDU = point.UNIT_EQU;
            newMeasurementDocumentData.MDTXT = null;
            newMeasurementDocumentData.ERNAM = "" + this.getUserInformation().firstName + " " + this.getUserInformation().lastName;

            return newMeasurementDocumentData;
        };

        MeasuringDocumentHistoryViewModel.prototype.insertNewMeasurementDocument = function (document, point) {

            var that = this;
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            try {
                document.RECDV = point.READING;
                document.MDTXT = point.EQU_MEASUREMENT_DESC;
            } catch (error) {

            }

            var onSuccess = function (asd) {
                //successCb();
            };

            try {
                document.insert(false, function (mobileId) {
                    document.MOBILE_ID = mobileId;
                    onSuccess();
                }, onError.bind(this, new Error("Error while trying to insert new measuring point")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MeasuringDocumentHistoryViewModel.prototype.constructor = MeasuringDocumentHistoryViewModel;

        return MeasuringDocumentHistoryViewModel;
    });
