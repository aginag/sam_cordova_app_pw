sap.ui.define([
        "SAMContainer/models/ViewModel",
        "sap/ui/model/Filter"
    ],

    function (ViewModel, Filter) {
        "use strict";

        // constructor
        function ObjectsTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._ORDERID = "";
            this._ORDER = null;
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        ObjectsTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        objectList: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;

                    if(order){

                    this.setProperty("/functionalLocation", {
                        id: order.FUNCT_LOC,
                        description: order.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: order.EQUIPMENT,
                        description: order.EQUIPMENT_DESC
                    });
                }
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        ObjectsTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        ObjectsTabViewModel.prototype.setProcessingIndicator = function (bActive, olObject, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                olObject.setProcessingInd(bActive);

                sqlStrings.push(olObject.update2(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_UPDATING_OBJECTS_ENTRY")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ObjectsTabViewModel.prototype.setSRBData = function (data, successCb) {
            var that = this;
            that.getMeasDoc().then(function(result){

                data.forEach(function(object){

                    var foundDoc = result.find(x => x.POINT == object.XA_MEASUREMENT_POINT && x.WOOBJ == object.OBJECT_NO);

                    object.hasDocument = foundDoc ? "true" : "false";
                    Object.assign(object, foundDoc);

                });

                that.setProperty("/objectList", data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }

            });
        };

        ObjectsTabViewModel.prototype.setAMData = function (data, successCb) {
            var that = this;
            var measPointArr = [];
            data.forEach(function(object){
                            
                if(object.FUNCT_LOC !== "" || object.EQUIPMENT !== ""){
                    object.showAMMenuButton = true;
                } else {
                    object.showAMMenuButton = false;
                }
            });

            that.setProperty("/objectList", data);
            that.setBusy(false);
            that._needsReload = false;

            if (successCb !== undefined) {
                successCb();
            }
        };
        
        ObjectsTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {

                data.forEach(function(object){
                            
                    if(object.FUNCT_LOC !== "" || object.EQUIPMENT !== ""){
                        object.showMenuButton = true;
                    } else {
                        object.showMenuButton = false;
                    }
                });

                if(that.getOrder().PMACTTYPE == "N44"){
                    that.setSRBData(data, successCb);
                }else{
                    that.setAMData(data, successCb);
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_OBJECTSTAB")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_OBJECTSTAB_NO_ORDER")));
            }

            this.setBusy(true);
            this.getOrder().loadOrderObjectList(load_success);
        };

        ObjectsTabViewModel.prototype.getMeasDoc = function () {
            return new Promise(function (resolve, reject) {
                this._requestHelper.getData("SRBMeasDoc", {
                    woobj: this.getOrder().OBJECT_NO
                }, resolve, reject);
            }.bind(this));
        };

        ObjectsTabViewModel.prototype.getEquipmentMeasurementPoints = function (object) {
            return new Promise(function (resolve, reject) {
                this._requestHelper.getData("AllEquipmentMeasurementPoints", {
                    equnr: object.EQUIPMENT,
                    objectNo: object.OBJECT_NO
                }, resolve, reject);
            }.bind(this));
        };

        ObjectsTabViewModel.prototype.getFuncLocMeasurementPoints = function (object) {
            return new Promise(function (resolve, reject) {
                this._requestHelper.getData("AllFuncLocMeasurementPoints", {
                    tplnr: object.FUNCT_LOC,
                    objectNo: object.OBJECT_NO
                }, resolve, reject);
            }.bind(this));
        };

        ObjectsTabViewModel.prototype.resetData = function () {
            if(this.getOrder()){
                this.getOrder().setOrderObjectList(null);
            }
        };

        ObjectsTabViewModel.prototype.onSearch = function (oEvt) {

            var aFilters = [];

            var sValue = oEvt.getSource().getValue();

            if (sValue && sValue.length > 0) {
                aFilters.push(new Filter("TPLNR", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("SHTXT", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("FUNCLDESCR", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("EQUIPMENT", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("TPLNR_SHTXT", sap.ui.model.FilterOperator.Contains, sValue));
                aFilters.push(new Filter("EQUIDESCR", sap.ui.model.FilterOperator.Contains, sValue));

                var allFilter = new Filter(aFilters);
            }

            var oBinding = this.getList().getBinding("items");
            oBinding.filter(allFilter);
        };

        ObjectsTabViewModel.prototype.checkMeasurementPointAvailabilty = function (object, successCb, errorCb) {
            this._requestHelper.getData(["EquipmentMeasurementPoints", "FuncLocMeasurementPoints"], {
                equipment: object.EQUIPMENT,
                funcLocId: object.FUNCT_LOC
            }, successCb, errorCb, true);
        };

        ObjectsTabViewModel.prototype.constructor = ObjectsTabViewModel;

        return ObjectsTabViewModel;
    });
