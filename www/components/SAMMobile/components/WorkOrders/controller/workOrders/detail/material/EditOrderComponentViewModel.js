sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function EditOrderComponentViewModel(requestHelper, globalViewModel, component) {
              
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditOrderComponentViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        component: null,
                        order: null,
                        malFuncMateriallist: [],
                        allMaterials: null,
                        searchstring: '',
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderComponent: {
                value: function (component) {

                   if (this.getProperty("/view/edit")) {
                      this._objectDataCopy = component.getCopy();
                   }

                    this.setProperty("/component", component);
                }
            },

            getOrderComponent: {
                value: function () {
                    return this.getProperty("/component");
                }
            },
   
           rollbackChanges: {
              value: function() {
                 this.getOrderComponent().setData(this._objectDataCopy);
              }
           },

            setOrder: {
                value: function (order) {
                    this.setProperty("/order", order);
                }
            },

            getOrder: {
                value: function () {
                    return this.getProperty("/order");
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n").getResourceBundle().getText("EditComponent") : this._component.getModel("i18n").getResourceBundle().getText("addMalMat");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },
          getMalFuncMatQueryParams: {
              value: function () {
                        return {orderid: this.getOrder().ORDERID};
              }
          },
          getMalFuncMatSearch: {
              value: function (searchstring) {
                  return {orderid: this.getOrder().ORDERID,
                          searchstring: searchstring
                          };
              }
        },
        getSearchstring:{
                value: function(){
                    return {searchstring: this.getProperty("/searchstring")};
                }
        }

        });


        EditOrderComponentViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditOrderComponentViewModel.prototype.validate = function() {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("component.STGE_LOC", "Please select a Storage Location").isEmpty();
            validator.check("component.MATERIAL", "Please select a Material").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
   /*     EditOrderComponentViewModel.prototype.displayStorageLocationDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oStorageLocationSelectDialog) {
                this._oStorageLocationSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.StorageLocSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oStorageLocationSelectDialog.display(oEvent, 'StorageLocation', function(storageLocation) {
    
                //return storageLocation.ILART == that.getOrder().PMACTTYPE;
                return storageLocation;
            });
        };*/

        EditOrderComponentViewModel.prototype.displayStorageLocationDialog = function (oEvent, viewContext) {
            if (!this._oStorageLocationSelectDialog) {
                this._oStorageLocationSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.StorageLocSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            // TODO get correct activity type for current scenario
            this._oStorageLocationSelectDialog.display(oEvent, {
                tableName: 'MaterialStorageLocation',
                params: {
                    spras: this.getLanguage(),
                    plant: this.getProperty("/component/PLANT"),
                },
                actionName: "valueHelp"
            });
        };

        EditOrderComponentViewModel.prototype.handleStorageLocationSelect = function (oEvent) {
            this._oStorageLocationSelectDialog.select(oEvent, 'STGE_LOC', [{
                'STGE_LOC': 'STGE_LOC'
            }, {
                'TEXT': 'TEXT'
            }], this, "/component");


        };

        EditOrderComponentViewModel.prototype.handleStorageLocationSearch = function (oEvent) {
            this._oStorageLocationSelectDialog.search(oEvent, ["TEXT", "STGE_LOC"]);
        };

        EditOrderComponentViewModel.prototype.displayMaterialDialog = function (oEvent, viewContext) {
            if (!this._oMaterialSelectDialog) {
                this._oMaterialSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.MaterialSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            // TODO get correct activity type for current scenario
            this._oMaterialSelectDialog.display(oEvent, {
                tableName: 'ZMALFUNC_MATERIAL', //'InventoryMaterial',
                params: {
//                    spras: this.getLanguage()
                },
                actionName: "get"
            });
        };
        EditOrderComponentViewModel.prototype.loadMalFuncMaterial = function(oEvent){
              var that = this;
              
              var load_success = function(data){
              var unique = {};
              var uniqueEach = [];
              // set the all materials property to false by default
              that.setProperty("/allMaterials", false);
              var filteredData = [];
              var foundResults = false;
//              shrink the list by pattern matched (n.padd) and remove dublicate materialnumbers
              for(var i = 0; i < data.length; i++){
                if(data[i].padd != "0"){
                    filteredData.push(data[i]);
                    foundResults = true;
                }
              }
              if(!foundResults){
                filteredData = $.grep(data, function(n, i){
                                    if(!unique[n.MATERIAL]){
                                        unique[n.MATERIAL] = true;
                                        return n.padd > 0;
                                    }
                                });
              }
              
//              if no result, load all data (= n.padd == 0) from zmalfunc and remove dublicate materialnumbers
              if (filteredData.length == 0){
                      // load all data
                      that._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                                  "",
                                                  function(data){
                                                      $.each(data, function(i, n){
                                                             if(uniqueEach.indexOf(n.MATERIAL) == -1){
                                                                filteredData.push(n);
                                                                uniqueEach.push(n.MATERIAL);
                                                             }
                                                      });
                                                      that.setProperty("/filteredNumb", filteredData.length);
                                                      that.setProperty("/totalNumb", filteredData.length);
                                                      that.setProperty("/malFuncMateriallist", filteredData);
                                                  },
                                                  function(error){
                                                    console.log(error);
                                                  },
                                                  true,
                                                  "getAll"
                                                  );
              
                      that.setProperty("/allMaterials", true);
              

              }else if (filteredData.length > 0){
                    that._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                          "",
                                          function(data){
                                         
                                                that.setProperty("/filteredNumb", filteredData.length);
                                                that.setProperty("/totalNumb", data[0].count);
                                                that.setProperty("/malFuncMateriallist", filteredData);
                                          },
                                          function(error){
                                                console.log(error);
                                          },
                                          true,
                                          "count"
                                          );
              }else{
                  that.setProperty("/filteredNumb", filteredData.length);
                  that.setProperty("/totalNumb", filteredData.length);
                  that.setProperty("/malFuncMateriallist", filteredData);
              }
             
              };
              
              var load_error = function(error){
              
                console.log(error);
              };
             
                  this._requestHelper.getData( "ZMALFUNC_MATERIAL",
                      this.getMalFuncMatSearch( this.getProperty("/searchstring")),
                      load_success,
                      load_error,
                      true,
                      "filtered"
                  );
          };
          EditOrderComponentViewModel.prototype.loadMalFiteredFuncMaterial = function(oEvent){
              var that = this;
              
              var load_success = function(data){
              var unique = {};
              var uniqueEach = [];
              //              shrink the list by pattern matched
              var filteredData = [];
              /**
               if search not all materials but by pattern, give just back the result
               else go through all the results and remove the duplicates
               */
              if(!that.getProperty("/allMaterials")){
                  for(var i = 0; i < data.length; i++){
                      if(data[i].padd != "0"){
                          filteredData.push(data[i]);
                      }
                  }
              
              }else{
                filteredData = $.grep(data, function(n, i){
                                    if(!unique[n.MATERIAL]){
                                      unique[n.MATERIAL] = true;
                                      return n.padd > 0;
                                    }
                                });
                  if (filteredData.length == 0){
                    $.each(data, function(i, n){
                         if(uniqueEach.indexOf(n.MATERIAL) == -1){
                           filteredData.push(n);
                           uniqueEach.push(n.MATERIAL);
                         }
                    });
                  }
              
            
//                  that.setProperty("/allMaterials", true);
              }
              if(typeof that.getProperty("/materialSave") != 'undefined'){
                var savedMat = that.getProperty("/materialSave"); 
                  filteredData.forEach(function(data){ 
                      data.selected = false;
                      for (var i =0; i<savedMat.length; i++){ 
                        if(data.MATERIAL == savedMat[i].MATERIAL){
                          // put this material as selected
                          
                          data.selected = true;
                        } 
                      }  
                  }); 
              }
               that.setProperty("/filteredNumb", filteredData.length);
              that.setProperty("/malFuncMateriallist", filteredData);
              };
              var load_error = function(error){
              console.log(error);
              };
              if( this.getProperty("/allMaterials") ){
                  this._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                              this.getSearchstring(),
                                              load_success,
                                              load_error,
                                              true,
                                              "searchAll");
              }else{
                  this._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                              this.getMalFuncMatSearch(oEvent),
                                              load_success,
                                              load_error,
                                              true,
                                              "filtered");
              }
              
              
          };
          
          EditOrderComponentViewModel.prototype.loadMalFuncMaterialWithoutIHLA = function(oEvent){
                  var that = this;
              
                  var load_success = function(data){

                  if(typeof that.getProperty("/materialSave") != 'undefined'){
                    var savedMat = that.getProperty("/materialSave"); 
                      data.forEach(function(data){ 
                          data.selected = false;
                          for (var i =0; i<savedMat.length; i++){ 
                            if(data.MATERIAL == savedMat[i].MATERIAL){
                              // put this material as selected
                              
                              data.selected = true;
                            } 
                          }  
                    });
                  }   

                      that.setProperty("/filteredNumb", data.length);
                      that.setProperty("/malFuncMateriallist", data);
                  };
              
                  var load_error = function(error){
                        console.log(error);
                  };
                      this._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                                  this.getSearchstring(),
                                                  load_success,
                                                  load_error,
                                                  true,
                                                  "withoutIHLA");
              
              };
              EditOrderComponentViewModel.prototype.loadMalFuncMaterialWithIHLA = function(oEvent){
              var that = this;
              var unique = {};
              
              var load_success = function(data){
              
              var filteredData = [];
            
              // shrink the list by pattern matched (n.padd) and remove dublicate materialnumbers
              for(var i = 0; i < data.length; i++){
                  if(data[i].padd != "0"){
                      filteredData.push(data[i]);
                  }
              }
               if(typeof that.getProperty("/materialSave") != 'undefined'){
                  var savedMat = that.getProperty("/materialSave"); 
                  filteredData.forEach(function(data){ 
                      data.selected = false;
                      for (var i =0; i<savedMat.length; i++){ 
                        if(data.MATERIAL == savedMat[i].MATERIAL){
                          // put this material as selected
                          
                          data.selected = true;
                        } 
                      }  
                  }); 
              }
              that.setProperty("/filteredNumb", filteredData.length);
              
              that.setProperty("/malFuncMateriallist", filteredData);
              };
              
              var load_error = function(error){
              
              console.log(error);
              };
              if(that.oData.allMaterials){
              //              do nothing since IHLA filter is not functional (all materials are loaded)
              }else{
              this._requestHelper.getData( "ZMALFUNC_MATERIAL",
                                          this.getMalFuncMatSearch(this.getSearchstring().searchstring),
                                          load_success,
                                          load_error,
                                          true,
                                          "filtered");
              }
              
              };

        EditOrderComponentViewModel.prototype.handleMaterialSelect = function (oEvent) {
            this._oMaterialSelectDialog.select(oEvent, 'MATERIAL', [{
                'MATERIAL': 'MATERIAL',
                'ITEM_TEXT': 'MATL_DESC',
                'REQUIREMENT_QUANTITY_UNIT': 'ENTRY_UOM',
                'STGE_LOC': 'LGORT',
                'PLANT': 'WERKS',
                'MATL_DESC': 'MATL_DESC'
            }], this, "/component");


        };

        EditOrderComponentViewModel.prototype.handleMaterialSearch = function (oEvent) {
            this._oMaterialSelectDialog.search(oEvent, ["MATL_DESC", "MATERIAL"]);
        };

        EditOrderComponentViewModel.prototype.getValueStateModel = function() {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MATERIAL: sap.ui.core.ValueState.None,
                    ITEM_TEXT: sap.ui.core.ValueState.None,
                    REQUIREMENT_QUANTITY: sap.ui.core.ValueState.None,
                    REQUIREMENT_QUANTITY_UNIT: sap.ui.core.ValueState.None,
                    PLANT: sap.ui.core.ValueState.None,
                    STGE_LOC: sap.ui.core.ValueState.None,
                });
            }

            return this.valueStateModel;
        };

        EditOrderComponentViewModel.prototype.displayPlantSelectDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oPlantSelectDialog) {
                this._oPlantSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.PlantSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oPlantSelectDialog.display(oEvent, {
                tableName: 'PlantMaterial',
                params: {
                    spras: this.getLanguage()
                },
                actionName: "valueHelp"
            });

        };

        EditOrderComponentViewModel.prototype.handlePlantSelect = function (oEvent) {
            this._oPlantSelectDialog.select(oEvent, 'PLANT', [{
                'PLANT': 'PLANT'
            }], this, "/component");
        };

        EditOrderComponentViewModel.prototype.handlePlantSearch = function (oEvent) {
            this._oPlantSelectDialog.search(oEvent, ["PLANT"]);
        };

        EditOrderComponentViewModel.prototype.constructor = EditOrderComponentViewModel;

        return EditOrderComponentViewModel;
    });
