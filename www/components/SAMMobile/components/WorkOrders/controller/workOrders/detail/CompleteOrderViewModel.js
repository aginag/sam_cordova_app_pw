sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter) {
        "use strict";

        // constructor
        function CompleteOrderViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.scenario = this._component.scenario;
            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this.summaryInfo = this.scenario.getSummaryInfo();
            this.setProperty("/view/custSideVisible", this.summaryInfo.custSideVisible);
            this.setProperty("/view/dewaSideVisible", this.summaryInfo.dewaSideVisible);
            this.setProperty("/view/moveToVisible", this.summaryInfo.moveToVisible);
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CompleteOrderViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        order: null,
                        _longText: "",
                        view: {
                            busy: false,
                            edit: false,
                            custSideVisible: false,
                            dewaSideVisible: false,
                            moveToVisible: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrder: {
                value: function (order) {
                    order._dewaSide = false;
                    order._custSide = false;
                    order._moveTo = null;
                    this.setProperty("/order", order);
                }
            },

            getOrder: {
                value: function () {
                    return this.getProperty("/order");
                }
            },

            getScenario: {
                value: function () {
                    return this._component.scenario;
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this._component.getModel("i18n")
                        .getResourceBundle()
                        .getText("CompleteOrder");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        CompleteOrderViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CompleteOrderViewModel.prototype.saveChanges = function (successCb, errorCb) {
//         //Save remarks (longtext)
//         if (this.getProperty("/_longText").length == 0) {
//            successCb();
//            return;
//         }

            var sqlStrings = [], that = this;
            var order = this.getOrder();
            var notification = order.getNotification();

            var scenarioLongtextDetails = this.getScenario().getDescriptionTabLongtextInformation();

            if (!scenarioLongtextDetails.readOnly) {
                sqlStrings = sqlStrings.concat(order.addLongtext(true, this.getProperty("/_longText"), this.scenario.getDescriptionTabLongtextInformation().objectType));
            }

            //Set customer and dewa side statuses
            if (order._dewaSide) {
                sqlStrings.push(notification.insertNewUserStatus(this.getUserStatusMl('E0010', notification.XA_STAT_PROF)));
            }

            if (order._custSide) {
                sqlStrings.push(notification.insertNewUserStatus(this.getUserStatusMl('E0011', notification.XA_STAT_PROF)));
            }
            //Move to statuses
            var statusCode = '';
            switch (order._moveTo) {
                case 0:
                    statusCode = 'E0011';
                    break;
                case 1:
                    statusCode = 'E0012';
                    break;
            }
            if (statusCode !== '') {
                var statusMl = this.getUserStatusMl(statusCode, order.OPERATION_STATUS_PROFILE);
                order.setOperationsUserStatus(statusMl, function () {
                    that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_INSERTING_LONGTEXT"))));
                }, errorCb.bind(null, new Error("Error while trying to insert user status")));
            } else {
                that._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_INSERTING_LONGTEXT"))));
            }
            //
            //this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, errorCb.bind(null, new Error("Error while trying to insert order longtexts")));
        };

        CompleteOrderViewModel.prototype.loadOrderData = function (errorCb) {
            var that = this;
            this.setBusy(true);

            this.getOrder().loadAll(function () {
                that.setBusy(false);
                that.setProperty("/_longText", that.getOrder().getLongText(that.scenario.getListItemLongtextInformation().objectType));
                that.refresh();
            }, function () {
                that.setBusy(false);
                errorCb(new Error("Error loading all order data"));
            })
        };

        CompleteOrderViewModel.prototype.constructor = CompleteOrderViewModel;

        return CompleteOrderViewModel;
    });
