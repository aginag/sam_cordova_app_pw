sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function InspectionTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        InspectionTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        functionalLocation: {
                            id: "",
                            description: "",
                            orderHistory: {},
                            notifHistory: {}
                        },
                        equipment: {
                            id: "",
                            description: "",
                            orderHistory: {},
                            notifHistory: {}
                        },
                        cust_info: {},
                        address: {
                            street: "",
                            postCode: "",
                            city: ""
                        },
                        team: {
                            MEMBERS: []
                        },
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        orderId: this._ORDERID,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setDataFromOrder: {
                value: function (order) {
                    this.setProperty("/functionalLocation", {
                        id: order.FUNCT_LOC,
                        description: order.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: order.EQUIPMENT,
                        description: order.EQUIPMENT_DESC
                    });
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        InspectionTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        InspectionTabViewModel.prototype.setNewData = function (data) {
            var that = this;
            var newArrayData = {};

            data.forEach(function (inspectionData) {
                var EQUIPMENT = inspectionData.EQUIPMENT;
                var ACTIVITY = inspectionData.ACTIVITY;
                var DESCRIPTION = inspectionData.DESCRIPTION;
                var EQUIDESCR = inspectionData.EQUIDESCR;
                var point = inspectionData.EQUIPMENT_MEASUREMENT_POINT;

                var COUNTER = that.loadCounter(point);
                var EXPANDED = false;
                var ICONCOLOR = "gray";
                inspectionData.STATUSICON = 'sap-icon://accept';
                inspectionData.showStatusIcon = false;
                var showErrorMessage = false;
                inspectionData.valueState = "None";

                inspectionData.READING = "";
                inspectionData.EQUIPMENT_MEASPOINT_MIN = parseFloat(inspectionData.EQUIPMENT_MEASPOINT_MIN).toFixed(2);
                inspectionData.EQUIPMENT_MEASPOINT_MAX = parseFloat(inspectionData.EQUIPMENT_MEASPOINT_MAX).toFixed(2);
                if (EQUIPMENT !== "") {
                    if (newArrayData[ACTIVITY]) {
                        newArrayData[ACTIVITY].points.push(inspectionData);
                    } else {
                        newArrayData[ACTIVITY] = {
                            ACTIVITY,
                            EQUIPMENT,
                            DESCRIPTION,
                            EQUIDESCR,
                            EXPANDED,
                            COUNTER,
                            ICONCOLOR,
                            showErrorMessage,
                            points: [inspectionData]
                        };
                    }
                }
            });

            this.setProperty("/inspections", newArrayData);
        };

        InspectionTabViewModel.prototype.loadCounter = function (point, successCb, errorCb) {
            var that = this;

            var load_success = function (data) {
                return data[0].MeasurementCounter[0].COUNT.toString();
            };

            var fetchCounterQueue = [
                this._fetchCountMeasurementDocuments(point)
            ];

            Promise.all(fetchCounterQueue).then(load_success).catch(function (err) {

            });
        };

        InspectionTabViewModel.prototype._fetchCountMeasurementDocuments = function (point) {
            return this._requestHelper.fetch({
                id: "MeasurementCounter",
                statement: "SELECT COUNT(MOBILE_ID) AS COUNT FROM MEASUREMENTDOCUMENT WHERE POINT = '@currentPoint'"
            }, {
                currentPoint: point
            });
        };

        InspectionTabViewModel.prototype.loadData = function (successCb) {

            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_LOADING_DATA_FOR_INSPECTION_TAB")));
            };

            this.setBusy(true);
            this._requestHelper.getData("Inspections", {
                orderId: this.getOrder().getOrderId()
            }, load_success, load_error, true);

        };

        InspectionTabViewModel.prototype.resetData = function () {
            this.setProperty("/inspections", []);
        };

        InspectionTabViewModel.prototype.checkLimits = function (point) {
            var validator = new Validator(point, this.getValueStateModel());

            // TODO do correct validation of the measurementpoint
            validator.check("", "Warning, Reading is outside the limits!").custom(function (value) {
                var reading = parseFloat(value.READING);
                var minAcceptedValue = parseFloat(value.EQUIPMENT_MEASPOINT_MIN);
                var maxAcceptedValue = parseFloat(value.EQUIPMENT_MEASPOINT_MAX);

                if (minAcceptedValue == 0 && maxAcceptedValue == 0) {
                    return true;
                }

                if (reading > maxAcceptedValue || reading < minAcceptedValue) {
                    return false;
                }

                return true;

            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            var operation = point.ACTIVITY;

            var inspections = this.getProperty("/inspections");
            for (var inspection in inspections) {
                if (inspections.hasOwnProperty(inspection)) {
                    if (operation === inspection) {
                        var points = inspections[inspection].points;
                        points.forEach(function (currentPoint) {
                            if (currentPoint.ACTIVITY === operation) {
                                if (errors) {
                                    point.valueState = "Error";
                                    point.STATUSICON = 'sap-icon://error';
                                    point.showStatusIcon = true;
                                } else {
                                    point.valueState = "None";
                                    point.STATUSICON = 'sap-icon://accept';
                                    point.showStatusIcon = true;
                                }
                            }
                        });
                        inspections[inspection].showErrorMessage = true;
                    }
                }
            }

            this.setProperty("/inspections", inspections);

            return errors ? false : true;

        };

        InspectionTabViewModel.prototype.validate = function (point) {

            var validator = new Validator(point, this.getValueStateModel());

            validator.check("READING", "Reading is not a number!").isNumber();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        InspectionTabViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    READING: sap.ui.core.ValueState.None,
                    EQUIPMENT_MEASPOINT_MIN: sap.ui.core.ValueState.None,
                    EQUIPMENT_MEASPOINT_MAX: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        InspectionTabViewModel.prototype.setExpandAllPanels = function () {
            var inspections = this.getProperty("/inspections");
            for (var inspection in inspections) {
                if (inspections.hasOwnProperty(inspection)) {
                    inspections[inspection].EXPANDED = true;
                }
            }
            this.setProperty("/inspections", inspections);
        };

        InspectionTabViewModel.prototype.setCollapseAllPanels = function () {
            var inspections = this.getProperty("/inspections");
            for (var inspection in inspections) {
                if (inspections.hasOwnProperty(inspection)) {
                    inspections[inspection].EXPANDED = false;
                }
            }
            this.setProperty("/inspections", inspections);
        };

        InspectionTabViewModel.prototype.setIconColor = function (oEvent) {
            var operation = oEvent.getSource().getBindingContext("inspectionTabViewModel").getObject().ACTIVITY;
            var that = this;
            var inspections = this.getProperty("/inspections");
            for (var inspection in inspections) {
                if (inspections.hasOwnProperty(inspection)) {
                    if (operation === inspection) {
                        inspections[inspection].ICONCOLOR = "green";
                        var points = inspections[inspection].points;
                        points.forEach(function (point) {
                            if (point.READING === "") {
                                inspections[inspection].ICONCOLOR = "orange";
                            }
                            if (point.valueState === "Error") {
                                inspections[inspection].ICONCOLOR = "red";
                                that.setProperty("/inspections", inspections);
                                that.exit();
                            }
                        });
                    }
                }
            }

            this.setProperty("/inspections", inspections);
        };

        InspectionTabViewModel.prototype.constructor = InspectionTabViewModel;

        return InspectionTabViewModel;
    });
