sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "sap/m/Button",
    "sap/m/Input",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/MessageToast",
    "SAMMobile/models/dataObjects/MeasurementDocument",
    "SAMMobile/models/viewModels/EditNotificationViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/EditMeasurementPointViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/EditClassificationViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/MeasuringDocumentHistoryViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/ObjectsTabViewModel",
    "sap/m/MessageBox",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/helpers/fileHelper",
    "sap/ui/core/Fragment"
], function (JSONModel, Controller, formatter, formatterComp, Button, Input, Dialog, Text, MessageToast, MeasurementDocument, EditNotificationViewModel, EditMeasurementPointViewModel, EditClassificationViewModel, MeasuringDocumentHistoryViewModel, ObjectsTabViewModel, MessageBox, MediaAttachmentsController, FileSystem, fileHelper, Fragment) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.objects.ObjectsTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailObjects").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailObjects", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.measuringDocumentHistoryViewModel = new MeasuringDocumentHistoryViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.fileSystem = new FileSystem();

            this.workOrderDetailViewModel = null;

            this.editNotificationViewModel = null;
            this.editMeasurementPointViewModel = null;
            this.editClassificationViewModel = null;
        },

        onRouteMatched: function (oEvent) {

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            this.editMeasurementPointViewModel = new EditMeasurementPointViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.getView().setModel(this.workOrderDetailViewModel, "workOrderDetailViewModel");
                this.objectsTabViewModel = this.workOrderDetailViewModel.tabs.objects.model;
                this.objectsTabViewModel.setList(this.byId("orderObjectListTable"));
                this.getView().setModel(this.objectsTabViewModel, "objectsTabViewModel");
            }
            this.editMeasurementPointViewModel.setOrder(this.objectsTabViewModel.getOrder());

            this.loadViewModelData();

            this._tryCreateUserAttachmentsControl();
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailObjects", "refreshRoute", this.onRefreshRoute, this);
        },

        onViewModelError: function(error) {
            MessageBox.error(error.message);
        },

        onNavBack: function () {
            var that = this;
            if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                this.oRouter.navTo("workOrders");
            } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                this.oRouter.navTo("workOrderOperations");
            } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION_SPLIT") {
                this.oRouter.navTo("workOrderOperationsSplit");
            }

            setTimeout(function () {
                that.workOrderDetailViewModel.resetData();
            }, 200);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        displayCodeGroupSearch: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.getPath().split("/").pop();

            if (path == "CODE_GROUP") {
                this.editNotificationViewModel.displayCodeGroupDialog(oEvent, this);
            } else if (path == "OBJ_CODE_GROUP") {
                this.editNotificationViewModel.displayObjCodeGroupDialog(oEvent, this);
            }

        },

        handleCodeGroupSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.getPath().split("/").pop();

            if (path == "CODE_GROUP") {
                this.editNotificationViewModel.handleCodeGroupSelect(oEvent);
            } else if (path == "OBJ_CODE_GROUP") {
                this.editNotificationViewModel.handleObjCodeGroupSelect(oEvent);
            }


        },

        handleCodeGroupSearch: function (oEvent) {
            this.editNotificationViewModel.handleCodeGroupSearch(oEvent);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.getView().byId("idObject").setBusy(true);
            this.objectsTabViewModel.loadData(function () {
                that.getView().byId("idObject").setBusy(false);
            });
        },

        onNavToEquipmentPress: function (oEvent) {
            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrderId(),
                            operId: this.workOrderDetailViewModel.getOrder().OPERATION_ACTIVITY,
                            persNo: this.workOrderDetailViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: encodeURIComponent(oEvent.getSource().getBindingContext('objectsTabViewModel').getObject().EQUIPMENT),
                            objectType: "0"
                        })
                    }
                }
            });

            this.workOrderDetailViewModel.resetData();
        },

        onNavToFuncLocPress: function (oEvent) {

            sap.ui.getCore().byId("samComponent").getComponentInstance().navToComponent({
                route: "workOrdersCustomRoute",
                params: {
                    query: {
                        routeName: "workOrderDetailRoute",
                        params: JSON.stringify({
                            id: this.workOrderDetailViewModel.getOrderId(),
                            operId: this.workOrderDetailViewModel.getOrder().OPERATION_ACTIVITY,
                            persNo: this.workOrderDetailViewModel.getUserInformation().personnelNumber
                        })
                    }
                }
            }, {
                route: "technicalObjectsCustomRoute",
                params: {
                    query: {
                        routeName: "technicalObjectsDetailRoute",
                        params: JSON.stringify({
                            id: encodeURIComponent(oEvent.getSource().getBindingContext('objectsTabViewModel').getObject().FUNCT_LOC),
                            objectType: "1"
                        })
                    }
                }
            });

            this.workOrderDetailViewModel.resetData();
        },

        onCreateNewMeasurementDocument: function (oEvent) {

            var that = this;

            var object = oEvent.getSource().getBindingContext("objectsTabViewModel").getObject();

            var onAcceptPressed = function (measurementModel, dialog) {
                try {
                    measurementModel.saveMeasurementPoint(function (document) {
                        object.VLCOD = document.VLCOD;
                        object.IS_LOCAL = document.ACTION_FLAG;
                        object.hasDocument = "true";
                        that.objectsTabViewModel.refresh();
                        MessageToast.show(that.component.i18n.getText("MEAS_DOC_INSERTED_SUCCESS"));
                        dialog.close();
                    });
                } catch (err) {
                    dialog.getContent()[0].scrollTo(0, 0);
                }
            };

            this.editMeasurementPointViewModel.setSelectedObject(object);

            this.getView().byId("idObject").setBusy(true);
            this.loadEquipmentData().then(function () {
                that.getView().byId("idObject").setBusy(false);
                that._getMeasurementPointEditDialog(onAcceptPressed).then(function(dialog) {
                    dialog.open();
                });
            }).catch(function () {

            });
        },

        loadEquipmentData: function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editMeasurementPointViewModel.loadEquipmentData(resolve, reject);
            });
        },

        loadAMEquipmentData: function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editMeasurementPointViewModel.loadAMEquipmentData(resolve, reject);
            });
        },

        _getMeasurementPointEditDialog: function (acceptCb) {
            const that = this;
            var onAcceptPressed = function () {
                var measurementModel = this.getParent().getModel("measurementModel");
                acceptCb(measurementModel, this.getParent());
            };
            var onAfterFragmentLoaded = function() {
                this._oMeasurementPointPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._oMeasurePointPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    beginButton: new Button({
                        text: '{i18n_core>save}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                    }
                });
                this._oMeasurementPointPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._oMeasurementPointPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            };
            var onAfterDialogCreated = function() {
                this._oMeasurementPointPopup.setTitle(this.component.i18n.getText("SRB_COLUMN1"));
                this._oMeasurementPointPopup.getBeginButton().mEventRegistry.press = [];
                this._oMeasurementPointPopup.getBeginButton().attachPress(onAcceptPressed);
                this._oMeasurementPointPopup.setModel(this.editMeasurementPointViewModel, "measurementModel");
                return this._oMeasurementPointPopup;
            };
            var loadFragment = function() {
                return Fragment.load({
                    name: "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.MeasurementPointPopover",
                    controller: that,
                    id: "measurementPointPopover"
                }).then(function(oFragment) {
                    that._oMeasurePointPopoverContent = oFragment;
                });
            };
            if (!this._oMeasurementPointPopup) {
                if (!this._oMeasurePointPopoverContent) {
                    return loadFragment().then(onAfterFragmentLoaded.bind(this)).then(onAfterDialogCreated.bind(this));
                }
                onAfterFragmentLoaded.apply(this);
            }
            onAfterDialogCreated.apply(this);
            return Promise.resolve(this._oMeasurementPointPopup);
        },

        onGoToFuncLocMeasurementPointsPressed: function (functionLocationPara, sPathPara) {
            var that = this;
            var functionLocation = functionLocationPara.data;
            var olObject = functionLocationPara;
            var sPath = sPathPara;

            var onAcceptPressed = function (aMMeasurementModel, dialog) {
                dialog.setBusy(true);
                aMMeasurementModel.saveAMMeasurementPoint(that.objectsTabViewModel.getProperty(sPath + '/OBJECT_NO') ,function (document) {
                    if(that.objectsTabViewModel.getProperty(sPath + '/PROCESSING_IND') == ''){
                        that.objectsTabViewModel.setProcessingIndicator(true, olObject, function () {
                            dialog.setBusy(false);
                            that.objectsTabViewModel.setProperty(sPath + '/PROCESSING_IND', 'X');
                            that.objectsTabViewModel.refresh();
                            that._oAMMeasurementPointPopup.close();
                        });
                    }else{
                        dialog.setBusy(false);
                        that.objectsTabViewModel.refresh();
                        that._oAMMeasurementPointPopup.close();
                    }
                }, function(){
                    dialog.setBusy(false);
                });
            };

            var onCancelPressed = function (aMMeasurementModel, dialog) {
                dialog.setBusy(true);
                if(aMMeasurementModel.validateOnCancelAMMeasurementPoint()){
                    that.objectsTabViewModel.refresh();
                    that._oAMMeasurementPointPopup.close();
                }
                dialog.setBusy(false);
            };

            this.getView().byId("idObject").setBusy(true);
            this.loadFuncLocMeasurementPoints(functionLocation, that.workOrderDetailViewModel).then(function () {
                that.getView().byId("idObject").setBusy(false);
                that._getAMMeasurementPointEditDialog(onAcceptPressed, onCancelPressed).then(function () {
                    var dialog = that._oAMMeasurementPointPopup;
                    dialog.open();
                }).catch(function () {
                });
            });
        },

        onGoToEquipmentMeasurementPointsPressed: function (equipmentPara, sPathPara) {
            var that = this;
            var equipment = equipmentPara.data;
            var olObject = equipmentPara;
            var sPath = sPathPara;

            var onAcceptPressed = function (aMMeasurementModel, dialog) {
                dialog.setBusy(true);
                aMMeasurementModel.saveAMMeasurementPoint(that.objectsTabViewModel.getProperty(sPath + '/OBJECT_NO'), function (document) {
                    if(that.objectsTabViewModel.getProperty(sPath + '/PROCESSING_IND') == ''){
                        that.objectsTabViewModel.setProcessingIndicator(true, olObject, function () {
                            dialog.setBusy(false);
                            that.objectsTabViewModel.setProperty(sPath + '/PROCESSING_IND', 'X');
                            that.objectsTabViewModel.refresh();
                            that._oAMMeasurementPointPopup.close();
                        });
                    }else{
                        dialog.setBusy(false);
                        that.objectsTabViewModel.refresh();
                        that._oAMMeasurementPointPopup.close();
                    }
                }, function(){
                    dialog.setBusy(false);
                });

            };

            var onCancelPressed = function (aMMeasurementModel, dialog) {
                dialog.setBusy(true);
                if(aMMeasurementModel.validateOnCancelAMMeasurementPoint()){
                    that.objectsTabViewModel.refresh();
                    that._oAMMeasurementPointPopup.close();
                }
                dialog.setBusy(false);
            };

            this.getView().byId("idObject").setBusy(true);
            this.loadEquipmentMeasurementPoints(equipment, that.workOrderDetailViewModel).then(function () {
                that.getView().byId("idObject").setBusy(false);
                that._getAMMeasurementPointEditDialog(onAcceptPressed, onCancelPressed).then(function () {
                    var dialog = that._oAMMeasurementPointPopup;
                    dialog.open();
                }).catch(function () {
                });
            });
        },


        _getAMMeasurementPointEditDialog: function (acceptCb, cancelCb) {
            const that = this;
            var onAcceptPressed = function () {
                var aMMeasurementModel = this.getParent().getModel("aMMeasurementModel");
                acceptCb(aMMeasurementModel, this.getParent());
            };

            var onCancelPressed = function () {
                var aMMeasurementModel = this.getParent().getModel("aMMeasurementModel");
                cancelCb(aMMeasurementModel, this.getParent());
            };
            var onAfterFragmentLoaded = function () {
                this._oAMMeasurementPointPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._oAMMeasurePointPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    busyIndicatorDelay: 0,
                    beginButton: new Button({
                        text: '{i18n_core>save}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });
                this._oAMMeasurementPointPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._oAMMeasurementPointPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            };
            var onAfterDialogCreated = function () {

                this._oAMMeasurementPointPopup.setTitle(this.component.i18n.getText("ZUSTANDERFASSUNG"));
                this._oAMMeasurementPointPopup.getBeginButton().mEventRegistry.press = [];
                this._oAMMeasurementPointPopup.getBeginButton().attachPress(onAcceptPressed);
                this._oAMMeasurementPointPopup.setModel(this.editMeasurementPointViewModel, "aMMeasurementModel");
                return this._oAMMeasurementPointPopup;
            };
            var loadFragment = function () {
                return Fragment.load({
                    name: "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.AMMeasurementPointPopover",
                    controller: that
                    //id: "aMMeasurementPointPopover"
                }).then(function (oFragment) {
                    that._oAMMeasurePointPopoverContent = oFragment;
                });
            };
            if (!this._oAMMeasurementPointPopup) {
                if (!this._oAMMeasurePointPopoverContent) {
                    return loadFragment().then(onAfterFragmentLoaded.bind(this)).then(onAfterDialogCreated.bind(this));
                }
                onAfterFragmentLoaded.apply(this);
            }
            onAfterDialogCreated.apply(this);
            return Promise.resolve(this._oMeasurementPointPopup);
        },

        onClassificationSingleAction: function (oEvent) {
            var that = this;
            var object = oEvent.getSource().getBindingContext("objectsTabViewModel").getObject().data;
            var source = oEvent.getSource();
            if(object.FUNCT_LOC !== "" && object.EQUIPMENT !== ""){
                this.oPopover = new sap.m.Popover({
                    placement: "Bottom",
                    busyIndicatorDelay: 0,
                    showHeader: false
                });
                this.getView().addDependent(this.oPopover);
                var oVBox = new sap.m.VBox({width: "100%", height: "100%"});
                var oMenuList = new sap.m.SelectList({
                    itemPress: function (oEvent) {
                        var that = this;
                        var oItem = oEvent.getParameter("item");
                        this.oPopover.setBusy(true);
                        if (oItem.getIcon() === "sap-icon://functional-location") {
                            that.onGoToFuncLocClassificationsPressed(object);
                        } else {
                            that.onGoToEquipmentClassificationsPressed(object);
                        }
                    }.bind(this)
                });

                oMenuList.addStyleClass("sapUiSizeCompact");
                oMenuList.addItem(new sap.ui.core.ListItem({
                    text: "{i18n_core>TOFuncLoc}",
                    icon: "sap-icon://functional-location"
                }));

                oMenuList.addItem(new sap.ui.core.ListItem({
                    text: "{i18n_core>TOEquipment}",
                    icon: "sap-icon://add-equipment"
                }));

                oVBox.addItem(oMenuList);
                this.oPopover.addContent(oVBox);
                this.oPopover.openBy(source);
            }else if(object.EQUIPMENT !== ""){
                that.onGoToEquipmentClassificationsPressed(object);
            }else if(object.FUNCT_LOC !== ""){
                that.onGoToFuncLocClassificationsPressed(object);
            }else{
                MessageToast.show(this.component.i18n.getText("NO_CHARACTERISTIC_AVAILABLE"));
            }
        },

        onGoToEquipmentClassificationsPressed: function (object) {
            var that = this;
            var equipment = object;

            var onAcceptPressed = function (equipment, dialog) {
                dialog.close();
                MessageToast.show("Done");
            };

            this.editClassificationViewModel = new EditClassificationViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            this.getView().byId("idObject").setBusy(true);

            this.loadEquipmentClassifications(equipment).then(function () {
                that.getView().byId("idObject").setBusy(false);
                var classifications = that.editClassificationViewModel.getClassifications();
                var dialog = that._getClassificationEditDialog(classifications, onAcceptPressed);
                dialog.open();
            }).catch(function () {

            });

        },

        onGoToFuncLocClassificationsPressed: function (object) {
            var that = this;
            var functionLocation = object;

            var onAcceptPressed = function (functionLocation, dialog) {
                dialog.close();
                MessageToast.show("Done");
            };

            this.editClassificationViewModel = new EditClassificationViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            this.getView().byId("idObject").setBusy(true);
            this.loadFuncLocClassifications(functionLocation).then(function () {
                that.getView().byId("idObject").setBusy(false);
                var classifications = that.editClassificationViewModel.getClassifications();
                var dialog = that._getClassificationEditDialog(classifications, onAcceptPressed);
                dialog.open();
            }).catch(function () {

            });

        },

        loadFuncLocClassifications: function (functionLocation) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editClassificationViewModel.loadFuncLocData(functionLocation, resolve, reject);
            });
        },

        loadEquipmentClassifications: function (equipment) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editClassificationViewModel.loadEquipmentData(equipment, resolve, reject);
            });
        },

        _getClassificationEditDialog: function (classifications, acceptCb) {
            var that = this;
            var onAcceptPressed = function () {
                that._oClassificationPopup.close();
            };

            this.editClassificationViewModel = new EditClassificationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editClassificationViewModel.setClassifications(classifications);

            if (!this._oClassificationPopup) {

                if (!this._oClassificationPopoverContent) {
                    this._oClassificationPopoverContent = sap.ui.xmlfragment("classificationPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.ClassificationPopover", this);
                }

                this._oClassificationPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._oClassificationPopoverContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    endButton: new Button({
                        text: '{i18n_core>close}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {

                    }
                });

                this._oClassificationPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._oClassificationPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._oClassificationPopup.setTitle(this.component.i18n_core.getText("TODetailCharacteristic"));
            this._oClassificationPopup.getEndButton().mEventRegistry.press = [];
            this._oClassificationPopup.getEndButton().attachPress(onAcceptPressed);

            this._oClassificationPopup.setModel(this.editClassificationViewModel, "classificationModel");
            this._oClassificationPopup.setModel(this.editClassificationViewModel.getValueStateModel(), "valueStateModel");

            return this._oClassificationPopup;
        },

        displayCodingSearch: function (oEvent) {
            this.editMeasurementPointViewModel.displayCodingDialog(oEvent, this);
        },

        displayAMCodingSearch: function (oEvent) {
            this.editMeasurementPointViewModel.displayAMCodingDialog(oEvent, this);
        },

        handleCodingSelect: function (oEvent) {
            this.editMeasurementPointViewModel.handleCodingSelect(oEvent);
        },

        handleCodingSearch: function (oEvent) {
            this.editMeasurementPointViewModel.handleCodingSearch(oEvent);
        },

        handleAMCodingSelect: function (oEvent) {
            this.editMeasurementPointViewModel.handleAMCodingSelect(oEvent);
            this.objectsTabViewModel.refresh();
        },

        handleAMCodingSearch: function (oEvent) {
            this.editMeasurementPointViewModel.handleAMCodingSearch(oEvent);
        },

        onMeasurementSingleAction: function (oEvent) {
            var object = oEvent.getSource().getBindingContext("objectsTabViewModel").getObject();
            var sPath = oEvent.getSource().getBindingContext("objectsTabViewModel").getPath();
            var source = oEvent.getSource();
            
            this.objectsTabViewModel.setBusy(true);
            this.checkMeasurementPointAvailabilty(object).then(function(result){
                if(result.EquipmentMeasurementPoints.length > 0 && result.FuncLocMeasurementPoints.length > 0 ){
                    this.oPopover = new sap.m.Popover({
                        placement: "Bottom",
                        busyIndicatorDelay: 0,
                        showHeader: false
                    });
                    this.getView().addDependent(this.oPopover);
                    var oVBox = new sap.m.VBox({width: "100%", height: "100%"});
                    var oMenuList = new sap.m.SelectList({
                        itemPress: function (oEvent) {
                            var that = this;
                            var oItem = oEvent.getParameter("item");
                            this.oPopover.setBusy(true);
                            if (oItem.getIcon() === "sap-icon://functional-location") {
                                that.onGoToFuncLocMeasurementPointsPressed(object, sPath);
                            } else {
                                that.onGoToEquipmentMeasurementPointsPressed(object, sPath);
                            }
                        }.bind(this)
                    });

                    oMenuList.addStyleClass("sapUiSizeCompact");
                    oMenuList.addItem(new sap.ui.core.ListItem({
                        text: "{i18n_core>TOFuncLoc}",
                        icon: "sap-icon://functional-location"
                    }));

                    oMenuList.addItem(new sap.ui.core.ListItem({
                        text: "{i18n_core>TOEquipment}",
                        icon: "sap-icon://add-equipment"
                    }));

                    oVBox.addItem(oMenuList);
                    this.oPopover.addContent(oVBox);
                    this.oPopover.openBy(source);
                }else if(result.EquipmentMeasurementPoints.length > 0){
                    this.onGoToEquipmentMeasurementPointsPressed(object, sPath);
                }else if(result.FuncLocMeasurementPoints.length > 0){
                    this.onGoToFuncLocMeasurementPointsPressed(object, sPath);
                }else{
                    MessageToast.show(this.component.i18n.getText("NO_ASSET_MANAGEMENT_AVAILABLE"));
                }
                this.objectsTabViewModel.setBusy(false);
            }.bind(this));

        },

        checkMeasurementPointAvailabilty: function (object) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.objectsTabViewModel.checkMeasurementPointAvailabilty(object, resolve, reject);
            });
        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext('aMMeasurementModel');
            var fileArr = oEvent.getParameter("files");
            var fileUploader = oEvent.getSource();

            for (let index = 0; index < fileArr.length; index++) {
                var file = fileArr[index];
                let name = file.name;
                var reader = new FileReader();
                reader.onload = function (readerEvt) {
                    binaryString = readerEvt.target.result.split(",")[1];
                    fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);

                    // On windows, we have to get the fileExt based on the file name
                    // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html#browser-quirks section "readAsDataURL"
                    if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                        var fileExt = name.split(".").pop();
                    } else {
                        var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                    }

                    var completeFileName = name;
                    var indexOfPoint = completeFileName.lastIndexOf('.');
                    var fileName = completeFileName.slice(0, indexOfPoint);
                    fileName = that.mediaAttachmentsController.getConvertedFilename(fileName);
                    if (that._oAMMeasurementPointPopup && that._oAMMeasurementPointPopup.isOpen()) {
                        that.onAMWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                    } else {
                        that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                    }

                    fileUploader.clear();
                };
                reader.onerror = function (error) {
                    MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                    that.editMeasurementPointViewModel.setBusy(false);
                };

                reader.readAsDataURL(file);
                //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            }
        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;

            var equipmentObj = this.editMeasurementPointViewModel.getSRData();
            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = equipmentObj.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.editMeasurementPointViewModel.setBusy(false);
                MessageToast.show(that.component.i18n.getText("NOTIFICATION_ATTACHMENT_INSERT_SUCCESS_TEXT"));
            };

            var onWriteError = function (msg) {
                that.editMeasurementPointViewModel.setBusy(false);
                MessageToast.show(msg);
            };

            this.fileSystem.writeToTemp(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;

            var equipmentObj = this.editMeasurementPointViewModel.getSRData();
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];
                    equipmentObj.addAttachment(attachmentObj);
                }
            } else {
                equipmentObj.addAttachment(mediaObject);
            }
            that.editMeasurementPointViewModel.refresh(true);
        },

        onAMWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;

            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj =  that.editMeasurementPointViewModel.getProperty(bContext.getPath()).getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addAMLocalAttachment(attachmentObj, bContext);
                that.workOrderDetailViewModel.loadAllCounters();
            };

            var onWriteError = function (msg) {
                MessageToast.show(msg);
            };

            this.fileSystem.writeToTemp(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addAMLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            mediaObject.MEASUREMENT_DESC = bContext.getObject().MEASUREMENT_DESC;
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];
                    that.editMeasurementPointViewModel.getProperty(bContext.getPath()).addAttachment(attachmentObj);
                }
            } else {
                that.editMeasurementPointViewModel.getProperty(bContext.getPath()).addAttachment(mediaObject);
            }
            that.editMeasurementPointViewModel.refresh();
        },

        //region private functions
        _tryCreateUserAttachmentsControl: function () {
            var me = this;

            me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {
                var equipment = me.editMeasurementPointViewModel.getSRData();

                var fileName = fileEntry.name.split(".")[0];
                var fileExt = getFileExtension(fileEntry.name);

                if (me._oAMMeasurementPointPopup && me._oAMMeasurementPointPopup.isOpen()) {
                    var aMattachmentObj = me.editMeasurementPointViewModel.getProperty(bCtx.getPath()).getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    aMattachmentObj.MEASUREMENT_DESC = bCtx.getObject().MEASUREMENT_DESC;
                    me.editMeasurementPointViewModel.getProperty(bCtx.getPath()).addAttachment(aMattachmentObj);
                } else {
                    var attachmentObj = equipment.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    equipment.addAttachment(attachmentObj);
                }

                me.editMeasurementPointViewModel.refresh();
            };

            me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {
                var attachment = bCtx.getObject();
                attachment.setFilename(getFileName(fileEntry.name));
                me.editMeasurementPointViewModel.refresh();
            };

            me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {
                var attachmentObj = bCtx.getObject();
                var sPath = bCtx.getPath();
                var index = sPath.lastIndexOf("/");
                sPath = sPath.substring(0,index);

                me.editMeasurementPointViewModel.deleteUserAttachment(attachmentObj, function () {
                    me.editMeasurementPointViewModel.removeFromModelPath(attachmentObj, sPath);

                    me.workOrderDetailViewModel.loadAllCounters();
                    me.refreshAttachmentsDialogModel();
                });

            };

            me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                return me.formatter._formatAttachmentType(fileExt);
            };

        },


        _onCapturePhotoPress: function (oEvent) {
            var that = this;

            if (that._oAMMeasurementPointPopup && that._oAMMeasurementPointPopup.isOpen()) {
                var bContext = new sap.ui.model.Context(this.editMeasurementPointViewModel, oEvent.getSource().getBindingContext('aMMeasurementModel').getPath());
            } else {
                var bContext = new sap.ui.model.Context(this.editMeasurementPointViewModel, '/equipment/Attachments/');
            }

            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, this.fileSystem.filePathTmp, function () {
            });
        },


        handleCharacValueChange: function (oEvent) {
            var that = this;

            var selectedCharacteristicBC = oEvent.getSource().getBindingContext('classificationModel');
            var sPath = selectedCharacteristicBC.getPath();
            var selectedCharacteristic = selectedCharacteristicBC.getObject();
            switch (selectedCharacteristic.ATFOR) {
                case 'NUM':
                    if (that.editClassificationViewModel.validateCharacteristicValue(selectedCharacteristic)) {
                        selectedCharacteristic.VALUE_NEUTRAL_FROM = selectedCharacteristic.VALUE_NEUTRAL_FROM.replace('.', ',');
                        var valueNeutralFrom = selectedCharacteristic.VALUE_NEUTRAL_FROM;
                        that.editClassificationViewModel.updateCharacteristic(selectedCharacteristic, function () {
                        });
                        MessageToast.show(that.component.i18n.getText("SUCCESS_CHARARTERISTIC_UPDATE"));
                        this.editClassificationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM', valueNeutralFrom.toString());
                    } else {
                        MessageToast.show(that.component.i18n.getText("ERROR_NUMBER_DIGITS"));
                    }
                    break;
                default:
                    if (that.editClassificationViewModel.validateSelectedCharacteristicValue(selectedCharacteristic)) {
                        var valueNeutralFrom = selectedCharacteristic.VALUE_NEUTRAL_FROM;
                        that.editClassificationViewModel.updateCharacteristic(selectedCharacteristic, function () {
                        });
                        this.editClassificationViewModel.setProperty(sPath + '/VALUE_NEUTRAL_FROM', valueNeutralFrom.toString());
                        MessageToast.show(that.component.i18n.getText("SUCCESS_CHARARTERISTIC_UPDATE"));
                    }
                    break;
            }

        },

        onDeleteAttachment: function (oEvent) {
            var attachment;
            var bContext;
            var sPath;

            if (this._oAMMeasurementPointPopup && this._oAMMeasurementPointPopup.isOpen()) {
                attachment = oEvent.getSource().getBindingContext('aMMeasurementModel').getObject();
                bContext = oEvent.getSource().getBindingContext('aMMeasurementModel');
                sPath = oEvent.getSource().getBindingContext('aMMeasurementModel').getPath();
            } else {
                attachment = oEvent.getSource().getBindingContext('measurementModel').getObject();
                bContext = oEvent.getSource().getBindingContext('measurementModel');
                sPath = oEvent.getSource().getBindingContext('aMMeasuremeasurementModelmentModel').getPath();
            }
           
            var index = sPath.lastIndexOf("/");
            sPath = sPath.substring(0,index);

            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function (oAction) {
                    if (oAction === "YES") {
                        if (attachment.MOBILE_ID == "") {
                            if (this._oAMMeasurementPointPopup && this._oAMMeasurementPointPopup.isOpen()) {
                                this.editMeasurementPointViewModel.removeFromModelPath(attachment, sPath);
                            } else {
                                this.editMeasurementPointViewModel.removeFromModelPath(attachment, "/equipment/Attachments");
                            }
                        } else {
                            this.mediaAttachmentsController._onDeleteAttachment(null, bContext, function () {
                                this.workOrderDetailViewModel.loadAllCounters();
                                this.editMeasurementPointViewModel.refresh();
                                MessageToast.show(this.component.i18n.getText("EQUIPMENT_ATTACHMENT_DELETE_SUCCESS_TEXT"));
                            }.bind(this));
                        }
                        this.editMeasurementPointViewModel.refresh();
                    }
                }.bind(this)
            });
        },

        onEditAttachment: function (oEvent) {
            const that = this;

            if (that._oAMMeasurementPointPopup && that._oAMMeasurementPointPopup.isOpen()) {
                var bContext = oEvent.getSource().getBindingContext('aMMeasurementModel');
            } else {
                var bContext = oEvent.getSource().getBindingContext('measurementModel');
            }

            var onNewFileNameAccepted = function (newFileName) {

                var dialog = this;
                var attachment = bContext.getObject();

                // Remove whitespaces from file name as this causes issues on sync
                newFileName = newFileName.replace(/\s/g, "");
                var patternFileName = new RegExp(/[\\?*.%,\/:<>\"]/g);
                if (patternFileName.test(newFileName)) {
                    MessageToast.show(that.component.i18n.getText("fileNameInvalid"));
                    return;
                }
                newFileName = that.mediaAttachmentsController.replaceUmlaute(newFileName);
                attachment.rename(newFileName, function () {
                    that.editMeasurementPointViewModel.refresh();
                    MessageToast.show(that.component.i18n.getText("EQUIPMENT_ATTACHMENT_UPDATE_SUCCESS_TEXT"));
                    dialog.close();
                }, function (err) {
                    that.onViewModelError(err);
                });
            };

            this._getEditAttachmentDialog(bContext.getObject(), onNewFileNameAccepted).open();
        },

        onOpenAttachment: function (oEvent) {
            if (this._oAMMeasurementPointPopup && this._oAMMeasurementPointPopup.isOpen()) {
                var bContext = oEvent.getSource().getSelectedItem().getBindingContext('aMMeasurementModel');
            } else {
                var bContext = oEvent.getSource().getSelectedItem().getBindingContext('measurementModel');
            }
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

        onDisplayMeasurementDocumentHistory: function (oEvent) {

            var that = this;

            var object = oEvent.getSource().getBindingContext('aMMeasurementModel').getObject();
            var sPath = oEvent.getSource().getBindingContext('aMMeasurementModel').getPath();
            var source = oEvent.getSource();

            if (!this._oMeasurementDocumentHistory) {

                if (!this._oMeasurementDocumentHistoryContent) {
                    this._oMeasurementDocumentHistoryContent = sap.ui.xmlfragment("measurementHistoryPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.MeasurementHistoryPopover", this);
                }


                this._oMeasurementDocumentHistory = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "Inspection",
                    content: this._oMeasurementDocumentHistoryContent,
                    contentHeight: "80%",
                    contentWidth: "80%",
                    endButton: new Button({
                        text: '{i18n_core>close}',
                        press: function () {
                            this.getParent().close();
                        }
                    }),
                    afterClose: function () {
                    }


                });

                this._oMeasurementDocumentHistory.setModel(this.component.getModel('i18n'), "i18n");
                this._oMeasurementDocumentHistory.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._oMeasurementDocumentHistory.setTitle(this.component.i18n.getText("HISTWERTE"));

            this.editMeasurementPointViewModel.getCodeText(this.editMeasurementPointViewModel.oData.measurePoints);

            var oModel = new sap.ui.model.json.JSONModel();


            var objects = [];

            objects.push(object);

            oModel.setData(objects);

            this._oMeasurementDocumentHistory.setModel(that.editMeasurementPointViewModel, "measurementDocumentHistoryModel");
            this._oMeasurementDocumentHistory.setModel(oModel, "objects");

            this._oMeasurementDocumentHistory.open();
        },

        closeDisplayMeasurementDocumentHistory: function () {
            this._oMeasurementDocumentHistory.close();
        },


        onAddLongTextPressed: function (oEvent) {

            var object = oEvent.getSource().getBindingContext('aMMeasurementModel').getObject();
            this.sPath = oEvent.getSource().getBindingContext('aMMeasurementModel').getPath();
            var source = oEvent.getSource();

            this._oLongTextDialog = sap.ui.xmlfragment("longTextDialog", "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.LongTextDialog", this);

            var measurePoints = oEvent.getSource().getBindingContext('aMMeasurementModel');

            if (!this.measurementLongTextModel) {
                this.measurementLongTextModel = new JSONModel();
            }

            var oModel = new sap.ui.model.json.JSONModel();

            var objects = [];

            objects.push(object);

            oModel.setData(objects);

            this._oLongTextDialog.setModel(oModel, "objects");


            this.measurementLongTextModel.setData(measurePoints);
            this._oLongTextDialog.setModel(this.editMeasurementPointViewModel, "longTextModel");

            this._oLongTextDialog.open();
        },

        closeAddLongTextDialog: function () {
            this._oLongTextDialog.close();
        },

        onHelpPressed: function (oEvent) {

            this._oHelpDialog = sap.ui.xmlfragment("helpDialog", "SAMMobile.components.WorkOrders.view.workOrders.detail.objects.HelpDialog", this);

            this._oHelpDialog.setTitle(this.component.i18n.getText("HELP"));
            this._oHelpDialog.setModel(this.component.getModel('i18n'), "i18n");
            this._oHelpDialog.setModel(this.editMeasurementPointViewModel, "helpModel");

            this._oHelpDialog.open();
        },

        closeHelpDialog: function () {
            this._oHelpDialog.close();
        },

        onAdditionalLongtextSavePress: function (oEvent) {
            this._oAMMeasurementPointPopup.getModel("aMMeasurementModel").getProperty(this.sPath).LONGTEXT = this._oLongTextDialog.oModels.longTextModel.oData.longTextToAdd;
            MessageToast.show(this.component.getModel('i18n').getProperty("operationsLongtextAddSuccess"));
            this._oLongTextDialog.oModels.longTextModel.oData.longTextToAdd = '';
            this.closeAddLongTextDialog();
        },

        onDisplayMeasurementDocLongtext: function (oEvent) {
            var measDocument = oEvent.getSource().getBindingContext("documentModel").getProperty();
            var dialog = this._getDisplayLongtextDialog(measDocument, false);
            dialog.open();
        },

        loadMeasurmentDocumentLongtexts: function (measDocument) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.measuringDocumentHistoryViewModel.loadMeasdocLongtext(measDocument, resolve, reject);
            });
        },

        loadMeasurementDocuments: function (point) {

            var that = this;
            return new Promise(function (resolve, reject) {
                that.measuringDocumentHistoryViewModel.loadData(point, resolve, reject);
            });
        },

        loadFuncLocMeasurementPoints: function (functionLocation, model) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editMeasurementPointViewModel.loadFuncLocData(functionLocation, model, resolve, reject);
            });
        },

        loadEquipmentMeasurementPoints: function (equipment, model) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.editMeasurementPointViewModel.loadAMEquipmentData(equipment, model, resolve, reject);
            });
        },

        refreshAttachmentsDialogModel: function () {
            this.editMeasurementPointViewModel.refresh();
        },

        // Equipment
        displayEquipmentSearch: function (oEvent) {
            this.editNotificationViewModel.displayEquipmentDialog(oEvent, this);
        },

        handleEquipmentSelect: function (oEvent) {
            this.editNotificationViewModel.handleEquipmentSelect(oEvent);
        },

        handleEquipmentSearch: function (oEvent) {
            this.editNotificationViewModel.handleEquipmentSearch(oEvent);
        },

        displayCharacValueSearch: function (oEvent) {
            this.editClassificationViewModel.displayCharacValueSearch(oEvent, this);
        },

        handleCharacValueSearch: function (oEvent) {
            this.editClassificationViewModel.handleCharacValueSearch(oEvent);
        },

        handleCharacValueSelect: function (oEvent) {
            this.editClassificationViewModel.handleCharacValueSelect(oEvent);
        },

        onTableSearch: function (oEvt) {
            this.objectsTabViewModel.onSearch(oEvt);
        },

        _getEditAttachmentDialog: function (attachment, acceptCb) {
            var that = this;
            var oBundle = this.getOwnerComponent().rootComponent.getModel("i18n").getResourceBundle();

            var dialog = new Dialog({
                title: oBundle.getText("EnterFileName"),
                contentWidth: "50%",
                content: new Input({
                    maxLength: 50,
                    placeholder: oBundle.getText("EnterFileName"),
                    value: "{attachment>/fileName}",
                    valueLiveUpdate: true
                }),
                beginButton: new Button({
                    text: oBundle.getText("cancel"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: oBundle.getText("save"),
                    enabled: "{= ${attachment>/fileName}.length > 0}",
                    press: function () {
                        acceptCb.apply(this.getParent(), [this.getParent().getModel("attachment").getData().fileName]);
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            dialog.setModel(new JSONModel({
                fileName: ""
            }), "attachment");

            return dialog;
        },
    });
});