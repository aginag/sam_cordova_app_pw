sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/controller/common/ChecklistPDFForm",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "SAMMobile/models/dataObjects/ChecklistFormSave"
], function (Controller, formatter, formatterComp, fileHelper, FileSystem, MediaAttachmentsController, ChecklistPDFForm, MessageBox, MessageToast, ChecklistFormSave) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.attachments.AttachmentTab", {
        formatter: formatter,
        formatterComp: formatterComp,
        fileHelper: fileHelper,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailAttachments").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailAttachments", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = null;
            this.attachmentTabViewModel = null;

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.fileSystem = new FileSystem();
        },

        onRouteMatched: function (oEvent) {

            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.attachmentTabViewModel = this.workOrderDetailViewModel.tabs.attachments.model;

                this.getView().setModel(this.attachmentTabViewModel, "attachmentTabViewModel");
            }

            this.loadViewModelData();

            this._tryCreateUserAttachmentsControl();
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailDescription", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.attachmentTabViewModel.loadData(function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });
        },

        onOpenChecklistMenuPressed: function (oEvent) {
            var selectedChecklist = oEvent.getParameter("item").getBindingContext("attachmentTabViewModel").getObject();

            if (!this.attachmentTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            var onHtmlLoaded = function (html) {
                var checklistDialog = new ChecklistPDFForm(html, selectedChecklist.FILENAME);
                checklistDialog.setSaveCb(this.onChecklistPdfSaved.bind(this));
                checklistDialog.setSuccessCb(this.onChecklistPdfCreated.bind(this));
                checklistDialog.setErrorCb(this.onCheckListPdfError.bind(this));
                checklistDialog.openDialog();
            };

            // Get html for selectedChecklist
            this.attachmentTabViewModel.getHtmlForChecklist(selectedChecklist.ID, onHtmlLoaded.bind(this), function (err) {
                MessageBox.warning(err.msg);
            });
        },

        onChecklistPdfCreated: function (fileName, savedState) {
            var that = this;
            var extension = "pdf";

            getFileInfo(this.globalViewModel.getSAMUser().getUserName() + "/attachments/"+ fileName + "." + extension, onGetFileInfoCompleted, onGetFileInfoCompleted);

            function onGetFileInfoCompleted(folderPath, fileSize) {
                var attachment = that.attachmentTabViewModel.getNewAttachmentObject(fileName, extension, fileSize.toString(), folderPath);
                that.attachmentTabViewModel.saveUserAttachment(attachment, function () {
                    if (savedState instanceof ChecklistFormSave) {
                        that.attachmentTabViewModel.deleteChecklistState(savedState, function(checklistFormSave) {
                        });
                    }
                });
                that.workOrderDetailViewModel.loadAllCounters();
            };
        },

        onCheckListPdfError: function (err) {
            MessageBox.error(err.msg);
        },

        onChecklistPdfSaved: function(savedState) {
            if (savedState instanceof ChecklistFormSave) {
                this.attachmentTabViewModel.updateChecklistState(savedState, function() {});
            } else {
                this.attachmentTabViewModel.saveChecklistState(savedState, function() {});
            }
        },

        _tryCreateUserAttachmentsControl: function () {
            var me = this;

                //                 me.userAttachmentControl = sap.ui.xmlfragment("SAMMobile.view.common.MediaAttachments", me.mediaAttachmentsController);
                //                 me.getView().addDependent(me.userAttachmentControl);

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {
                    var attachmentList = bCtx.getObject();
                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = me.attachmentTabViewModel.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    me.attachmentTabViewModel.saveUserAttachment(attachmentObj, function () {
                        me.attachmentTabViewModel.refresh(true);
                    });
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {
                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));

                    me.attachmentTabViewModel.updateUserAttachment(attachment);
                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {
                    var attachment = bCtx.getObject();

                    //in case file could not be deleted on file system we remove it from DB
                    me.attachmentTabViewModel.deleteUserAttachment(attachment);
                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };

        },

        _onCapturePhotoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");
            var that = this;
            if (!this.attachmentTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, null, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureVideoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");
            var that = this;
            if (!this.attachmentTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureVideoPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureAudioPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.attachmentTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("attachmentTabViewModel");
            var that = this;
            if (!this.attachmentTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureAudioPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getSelectedItem().getBindingContext("attachmentTabViewModel");
            var attachmentObject = bContext.getObject();
            if(!attachmentObject.FILEEXT){
                this._onOpenChecklistAttachment(oEvent);
            }else{
                if (attachmentObject.USER_FIELD) {
                    this.mediaAttachmentsController.redirectToCorrectUrl(attachmentObject.USER_FIELD);
                } else {
                    this.mediaAttachmentsController._onOpenAttachment(null, bContext);
                }
            }
        },

        _onOpenChecklistAttachment:function(oEvent){

            var bContext = oEvent.getSource().getSelectedItem().getBindingContext("attachmentTabViewModel");
            var checklistSave = bContext.getObject();
            var orderInfoObject = this.attachmentTabViewModel.getChecklistInfoObject(checklistSave.getFormName());
            var checklistDialog = new ChecklistPDFForm("", checklistSave.getFormName(), orderInfoObject, checklistSave);
            checklistDialog.setSuccessCb(this.onChecklistPdfCreated.bind(this));
            checklistDialog.setSaveCb(this.onChecklistPdfSaved.bind(this));
            checklistDialog.setErrorCb(this.onCheckListPdfError.bind(this));
            checklistDialog.openDialog();
        },

        _onEditAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("attachmentTabViewModel");
            this.mediaAttachmentsController._onEditAttachment(null, bContext);
        },

        _onDeleteAttachment: function (oEvent) {
            
            var bContext = oEvent.getSource().getBindingContext("attachmentTabViewModel");
            var that = this;
            MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: this.component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function(oAction) {
                    if (oAction === "YES") {
                        if (bContext.getObject() instanceof ChecklistFormSave) {
                            this.attachmentTabViewModel.deleteChecklistState(bContext.getObject(), function() {
                                MessageToast.show(that.component.i18n.getText("ATTACHMENT_DELETION_SUCCESS_TEXT"));
                            });
                        }else{
                            this.mediaAttachmentsController._onDeleteAttachment(null, bContext, function () {
                                that.workOrderDetailViewModel.loadAllCounters();
                                MessageToast.show(that.component.i18n.getText("ATTACHMENT_DELETION_SUCCESS_TEXT"));
                            });
                        }   
                    }
                }.bind(this)
            });          
        },

        onUserAttachments: function (oEvent) {
            var bCtx = oEvent.getSource().getBindingContext();
            var taskObject = bCtx.getObject();
            var me = this;
            var context = new Context(me.checkListTaskModel, bCtx.sPath);
            me.userAttachmentControl.setBindingContext(context);
            me.userAttachmentControl.setModel(me.checkListTaskModel);

            var userAttTable = sap.ui.getCore().byId("checklistUserAttachmentsTable");
            userAttTable.removeSelections(true);

            me.userAttachmentControl.open();
        },

        handleTypeMissmatch: function (oEvent) {
            var aFileTypes = ["txt", "pdf", "jpeg", "png", "doc", "docx", "xls", "xlsx", "jpg"];
            jQuery.each(aFileTypes, function (key, value) {
                aFileTypes[key] = "*." + value;
            });
            MessageBox.warning(this.oBundle.getText("fileTypeNotSupported"));
        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var fileArr = oEvent.getParameter("files");

            if (!this.attachmentTabViewModel.canAttachFiles()) {
                MessageBox.warning(this.component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.getOwnerComponent().setBusyOn();
           
            for (let index = 0; index < fileArr.length; index++) {
                var file = fileArr[index];
                let name = file.name;
                var reader = new FileReader();
                reader.onload = function (readerEvt) {
                    binaryString = readerEvt.target.result.split(",")[1];
                    fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);
                    // On windows, we have to get the fileExt based on the file name
                    // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html section "readAsDataURL"
                    if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                        var fileExt = name.split(".").pop();
                    } else {
                        var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                    }

                    var completeFileName = name;
                    var indexOfPoint = completeFileName.lastIndexOf('.');
                    var fileName = completeFileName.slice(0, indexOfPoint);
                    fileName = that.mediaAttachmentsController.getConvertedFilename(fileName);

                    that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
                };
                reader.onerror = function (error) {
                    //MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                    that.getOwnerComponent().setBusyOff();
                };
                reader.readAsDataURL(file);
                //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            };
            oEvent.getSource().clear();
        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;

            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = that.attachmentTabViewModel.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.getOwnerComponent().setBusyOff();
                that.workOrderDetailViewModel.loadAllCounters();
            };

            var onWriteError = function (msg) {
                that.getOwnerComponent().setBusyOff();
                MessageToast.show(msg);
            };

            this.fileSystem.write(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];

                    that.attachmentTabViewModel.saveUserAttachment(attachmentObj, function () {
                        that.attachmentTabViewModel.refresh(true);
                    });
                }
            } else {
                that.attachmentTabViewModel.saveUserAttachment(mediaObject, function () {
                    that.attachmentTabViewModel.refresh(true);
                });
            }

        }
    });

});