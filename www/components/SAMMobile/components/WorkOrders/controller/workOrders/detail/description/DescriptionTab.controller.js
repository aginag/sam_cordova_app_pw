sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast"
], function(Controller, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.description.DescriptionTab", {

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailDescription").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailDescription", "refreshRoute", this.onRefreshRoute, this);

            this.workOrderDetailViewModel = null;
            this.descriptionTabViewModel = null;

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
        },

        onRouteMatched: function(oEvent) {

            var that = this;

            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.descriptionTabViewModel = this.workOrderDetailViewModel.tabs.description.model;

                this.getView().setModel(this.descriptionTabViewModel, "descriptionTabViewModel");
            }

            this.loadViewModelData();
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailDescription", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.descriptionTabViewModel.getOrder().setLongText(null);
            this.descriptionTabViewModel.getOrder().getOrderOperations().setLongText(null);
            this.descriptionTabViewModel.getOrder().getNotification().setLongText(null);
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.descriptionTabViewModel.loadData(function() {
                that.workOrderDetailViewModel.loadAllCounters();
            });
        },

        onAdditionalLongtextSavePress: function(oEvent) {
            var that = this;
            var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
            this.descriptionTabViewModel.saveAdditionalLongtext(function() {
                that.workOrderDetailViewModel.setProperty("/nbLongtext", that.descriptionTabViewModel.countLongtext());
                MessageToast.show(oBundle.getText("LONGTEXT_SUCCESSFULLY_SAVED"));
            }, that.workOrderDetailViewModel);
        },
        
        onAdditionalLongtextNotifSavePress: function(oEvent) {
            var that = this;
            var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
            this.descriptionTabViewModel.saveAdditionalNotifLongtext(function() {
                that.workOrderDetailViewModel.setProperty("/nbLongtext", that.descriptionTabViewModel.countLongtext());
                MessageToast.show(oBundle.getText("LONGTEXT_NOTIFICATION_SUCCESSFULLY_SAVED"));
            }, that.workOrderDetailViewModel);
        }
    });

});