sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "sap/m/MessageToast",
        "SAMMobile/models/dataObjects/MeasurementPoint",
        "SAMMobile/models/dataObjects/MeasurementDocument",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/MeasuringDocumentHistoryViewModel",
        "SAMMobile/models/dataObjects/SAMObjectAttachment",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/models/dataObjects/Equipment",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/WorkOrderDetailViewModel"],

    function (JSONModel, ViewModel, Validator, Formatter, MessageToast, MeasurementPoint, MeasurementDocument, GenericSelectDialog, MeasuringDocumentHistoryViewModel, SAMObjectAttachment, DMSAttachment, Equipment, WorkOrderDetailViewModel) {
        "use strict";

        // constructor
        function EditMeasurementPointViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this._FUNCLOC = null;
            this.component = component;
            this.documents = null;

            this.measuringDocumentHistoryViewModel = new MeasuringDocumentHistoryViewModel(requestHelper, globalViewModel, component);

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditMeasurementPointViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        orderId: this._ORDERID,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setFunctionLocation: {
                value: function (funcLoc) {
                    this._FUNCLOC = funcLoc;
                }
            },

            getFunctionLocation: {
                value: function () {
                    return this._FUNCLOC;
                }
            },

            getMeasurementPoints: {
                value: function () {
                    return this.getProperty("/measurePoints");
                }
            },

            setMeasurementPoints: {
                value: function (points) {
                    this.setProperty("/measurePoints", points);
                }
            },

            setMeasurementPoint: {
                value: function (point) {
                    this.setProperty("/measurePoint", point);
                }
            },

            getMeasurementPoint: {
                value: function () {
                    return this.getProperty("/measurePoint");
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            setEquipment: {
                value: function (equipment) {
                    this.setProperty("/equipment", equipment);
                }
            },

            getEquipment: {
                value: function () {
                    return this.getProperty("/equipment");
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setSelectedObject: {
                value: function (object) {
                    this.selectedObject = object;
                }
            },

            getSelectedObject: {
                value: function () {
                    return this.selectedObject;
                }
            },

            setLongtext: {
                value: function (longtext) {
                    this.setProperty("/longtext", longtext);
                }
            },

            getLongtext: {
                value: function () {
                    return this.getProperty("/longtext");
                }
            },

            setDataFromOrder: {
                value: function (order) {
                    this.setProperty("/functionalLocation", {
                        id: order.FUNCT_LOC,
                        description: order.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: order.EQUIPMENT,
                        description: order.EQUIPMENT_DESC
                    });
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        EditMeasurementPointViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };


        EditMeasurementPointViewModel.prototype.setNewData = function (data) {
            var that = this;
            var documents = new MeasurementDocument(null, that._requestHelper, that);

            documents.POINT = data.MEASUREMENT_POINT;
            documents.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
            documents.ITIME = moment(new Date()).format("HH:mm:ss");
            documents.ERNAM = this.getUserInformation().userName;
            documents.CODGR = data.CODGR;
            documents.WOOBJ = this.getOrder().OBJECT_NO;

            this.setProperty("/measureDoc", documents);
            this.setProperty("/view/warningMessages", []);
            this.setProperty("/view/errorMessages", []);
        };

        EditMeasurementPointViewModel.prototype.setOldData = function (data) {
            var that = this;
            var documents = new MeasurementDocument(data, that._requestHelper, that);
            documents.ERDAT = moment(documents.ERDAT).format("YYYY-MM-DD");

            this.setProperty("/measureDoc", documents);
            this.setProperty("/view/warningMessages", []);
            this.setProperty("/view/errorMessages", []);
        };

        EditMeasurementPointViewModel.prototype.setOldData = function (data) {
            var that = this;
            var documents = new MeasurementDocument(data, that._requestHelper, that);
            documents.ERDAT = moment(documents.ERDAT).format("YYYY-MM-DD");

            this.setProperty("/measureDoc", documents);
            this.setProperty("/view/warningMessages", []);
            this.setProperty("/view/errorMessages", []);
        };

        EditMeasurementPointViewModel.prototype.setNewAMData = function (data, model) {
            var that = this;

            var promiseQueue = [];

            if (data.EquipmentMeasurementPoints != undefined) {
                var measurementPoints = data.EquipmentMeasurementPoints.map(function (measPoint) {
                    return new MeasurementPoint(measPoint, that._requestHelper, that);
                });
            } else {
                var measurementPoints = data.FuncLocMeasurementPoints.map(function (measPoint) {
                    return new MeasurementPoint(measPoint, that._requestHelper, that);
                });
            }

            var loadCodeMl = function (measurementPoint) {
                return new Promise(function (resolve, reject) {
                    that.loadCodeML(measurementPoint, resolve, reject);
                });
            };

            var loadCodeMlCodeGroup = function (measurementPoint) {
                return new Promise(function (resolve, reject) {
                    that.loadCodeMLCodeGroup(measurementPoint, resolve, reject);
                });
            };

            var filteredMeasPoints = [];

            for (var i = 0; i < data.AM_MPoint.length; i++) {
                if (data.AM_MPoint[i].ILART === model.getProperty("/order/PMACTTYPE")) {
                    for (var n = 0; n < measurementPoints.length; n++) {
                        if (measurementPoints[n].data.PSORT == data.AM_MPoint[i].MPOS) {
                            filteredMeasPoints.push(measurementPoints[n])
                        }
                    }
                }
            }

            filteredMeasPoints.forEach(function (measurementPoint) {
                if (data.EquipmentMeasurementPoints !== undefined) {
                    for (var i = 0; i < data.EquipmentMeasurementPoints.length; i++) {
                        if (measurementPoint.data.MEASUREMENT_POINT === data.EquipmentMeasurementPoints[i].MEASUREMENT_POINT) {
                                measurementPoint.CODGR = data.EquipmentMeasurementPoints[i].CODGR;
                                measurementPoint.MRMIN = parseFloat(data.EquipmentMeasurementPoints[i].MRMIN).toFixed(2);
                                measurementPoint.MRMAX = parseFloat(data.EquipmentMeasurementPoints[i].MRMAX).toFixed(2);
                                measurementPoint.MINMAXUNIT = data.EquipmentMeasurementPoints[i].MINMAXUNIT;
                                measurementPoint.PSORT = data.EquipmentMeasurementPoints[i].PSORT;
                                measurementPoint.MEASUREMENT_DESC = data.EquipmentMeasurementPoints[i].MEASUREMENT_DESC;
                                measurementPoint.POINT = data.EquipmentMeasurementPoints[i].MEASUREMENT_POINT;
                                if (data.AllEquipmentMeasurementPoints.length !== 0) {

                                    var doc = data.AllEquipmentMeasurementPoints.find(x => x.MEASUREMENT_POINT == measurementPoint.POINT);
                                    if(doc){
                                        measurementPoint.VLCOD = doc.VLCOD;
                                        measurementPoint.READR = doc.READR;
                                        measurementPoint.IDATE = doc.IDATE;
                                        measurementPoint.ITIME = doc.ITIME;
                                        measurementPoint.CODE_TEXT = doc.READG_CHAR;
                                        measurementPoint.LONGTEXT = doc.MDTXT;
                                        measurementPoint.ACTION_FLAG = doc.ACTION_FLAG;
                                    }else{
                                        measurementPoint.ACTION_FLAG = "N";
                                    }
                                } else {
                                    measurementPoint.ACTION_FLAG = "N";
                                }

                                var histMeasPoints = data.AllEquipmentMeasurementPoints.filter(function(histMeasPoint) { return histMeasPoint.ACTION_FLAG !== 'N' && histMeasPoint.ACTION_FLAG !== '' && histMeasPoint.MEASUREMENT_POINT == measurementPoint.POINT; })

                                measurementPoint.MeasurementDocuments = histMeasPoints;

                                measurementPoint.STATUSICON = 'sap-icon://accept';
                                measurementPoint.showStatusIcon = false;
                                measurementPoint.valueState = "None";
                                measurementPoint.showErrorMessage = false;
                                measurementPoint.showWarningMessage = false;

                                if (data.AllEquipmentMeasurementPoints.length !== 0) {
                                    var doc = data.AllEquipmentMeasurementPoints.find(x => x.MEASUREMENT_POINT == measurementPoint.POINT);
                                    if(doc){
                                        var erdatStr = doc.ERDAT.toString();
                                        var erYear = erdatStr.substring(0, 4);

                                        var cuYear = new Date().getFullYear().toString();

                                        if (cuYear === erYear) {
                                            measurementPoint.ACTION_FLAG = doc.ACTION_FLAG;
                                            promiseQueue.push(loadCodeMl(measurementPoint));
                                        } else {
                                            measurementPoint.ACTION_FLAG = "N";
                                        }
                                    }
                                }

                                var helpLongtexts = measurementPoint;

                                promiseQueue.push(loadCodeMlCodeGroup(helpLongtexts));

                                measurementPoint.descriptionIcon = 'sap-icon://add-document';

                                measurementPoint.showCodeValueHelp = measurementPoint.CODGR !== '';

                                promiseQueue.push(that.loadMeasurementDocuments(measurementPoint));
                        }
                        ;
                    }
                    ;
                } else {
                    for (var i = 0; i < data.FuncLocMeasurementPoints.length; i++) {
                        if (measurementPoint.data.MEASUREMENT_POINT === data.FuncLocMeasurementPoints[i].MEASUREMENT_POINT) {
                                    measurementPoint.CODGR = data.FuncLocMeasurementPoints[i].CODGR;
                                    measurementPoint.IDATE = data.FuncLocMeasurementPoints[i].IDATE;
                                    measurementPoint.MRMIN = parseFloat(data.FuncLocMeasurementPoints[i].MRMIN).toFixed(2);
                                    measurementPoint.MRMAX = parseFloat(data.FuncLocMeasurementPoints[i].MRMAX).toFixed(2);
                                    measurementPoint.MINMAXUNIT = data.FuncLocMeasurementPoints[i].MINMAXUNIT;
                                    measurementPoint.PSORT = data.FuncLocMeasurementPoints[i].PSORT;
                                    measurementPoint.MEASUREMENT_DESC = data.FuncLocMeasurementPoints[i].MEASUREMENT_DESC;
                                    measurementPoint.POINT = data.FuncLocMeasurementPoints[i].MEASUREMENT_POINT;
                                    if (data.AllFuncLocMeasurementPoints.length !== 0) {

                                        var doc = data.AllFuncLocMeasurementPoints.find(x => x.MEASUREMENT_POINT == measurementPoint.POINT);
                                        if(doc){
                                            measurementPoint.VLCOD = doc.VLCOD;
                                            measurementPoint.READR = doc.READR;
                                            measurementPoint.IDATE = doc.IDATE;
                                            measurementPoint.ITIME = doc.ITIME;
                                            measurementPoint.CODE_TEXT = doc.READG_CHAR;
                                            measurementPoint.LONGTEXT = doc.MDTXT;
                                            measurementPoint.ACTION_FLAG = doc.ACTION_FLAG;
                                        }else{
                                            measurementPoint.ACTION_FLAG = "N";
                                        }
                                    } else {
                                        measurementPoint.ACTION_FLAG = "N";
                                    }

                                    var histMeasPoints = data.AllFuncLocMeasurementPoints.filter(function(histMeasPoint) { return histMeasPoint.ACTION_FLAG !== 'N' && histMeasPoint.ACTION_FLAG !== '' && histMeasPoint.MEASUREMENT_POINT == measurementPoint.POINT; })

                                    measurementPoint.MeasurementDocuments = histMeasPoints;

                                    measurementPoint.STATUSICON = 'sap-icon://accept';
                                    measurementPoint.showStatusIcon = false;
                                    measurementPoint.valueState = "None";
                                    measurementPoint.showErrorMessage = false;
                                    measurementPoint.showWarningMessage = false;

                                    if (data.AllFuncLocMeasurementPoints.length !== 0) {
                                        var doc = data.AllFuncLocMeasurementPoints.find(x => x.MEASUREMENT_POINT == measurementPoint.POINT);
                                        if(doc){
                                            var erdatStr = doc.ERDAT.toString();
                                            var erYear = erdatStr.substring(0, 4);

                                            var cuYear = new Date().getFullYear().toString();

                                            if (cuYear === erYear) {
                                                measurementPoint.ACTION_FLAG = doc.ACTION_FLAG;
                                                promiseQueue.push(loadCodeMl(measurementPoint));
                                            } else {
                                                measurementPoint.ACTION_FLAG = "N";
                                            }
                                        }
                                    }

                                    var helpLongtexts = measurementPoint;

                                    promiseQueue.push(loadCodeMlCodeGroup(helpLongtexts));

                                    measurementPoint.descriptionIcon = 'sap-icon://add-document';

                                    measurementPoint.showCodeValueHelp = measurementPoint.CODGR !== '';

                                    promiseQueue.push(that.loadMeasurementDocuments(measurementPoint));

                        }
                        ;
                    }
                    ;
                }
            });

            return Promise.all(promiseQueue).then(function () {

                this.setProperty("/measurePoints", filteredMeasPoints);
                this.setProperty("/view/warningMessages", []);
                this.setProperty("/view/errorMessages", []);
            }.bind(this));
        };

        EditMeasurementPointViewModel.prototype.setLastMeasDocData = function (data) {
            var that = this;
            var newData = [];
            data.forEach(function (measDoc) {
                var measurementDocument = new MeasurementDocument(measDoc, that._requestHelper, that);
                measurementDocument.INSPECTIONDATE = moment(measurementDocument.IDATE).format("YYYY-MM-DD") + " " + moment(measurementDocument.ITIME).format("HH:mm:ss");

                newData.push(measurementDocument);
            });


            newData.sort(function (a, b) {
                var x = moment(a.INSPECTIONDATE);
                var y = moment(b.INSPECTIONDATE);
                return y.diff(x)
            });

            this.setProperty("/lastMeasureDoc", newData[0]);
        };

        EditMeasurementPointViewModel.prototype.loadMeasurementDocuments = function (point) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.measuringDocumentHistoryViewModel.loadData(point, resolve, reject);
            });
        };

        EditMeasurementPointViewModel.prototype.loadMeasLongtext = function (point) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadMeasPointLongtext(point, resolve, reject);
            });
        };

        EditMeasurementPointViewModel.prototype.loadCodecatalog = function (point) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadCodeML(point, resolve, reject);
            });
        };

        EditMeasurementPointViewModel.prototype.loadFuncLocData = function (object, model, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setNewAMData(data, model).then(function () {
                    var attachmentArr = [];
                    var allMeasurePoints = that.getProperty("/measurePoints");
                    allMeasurePoints.forEach(function(point){
                        attachmentArr.push(point.loadAttachments());
                    });
                    
                    Promise.all(attachmentArr).then(function () {
                        allMeasurePoints.forEach(function(point){
                            point.Attachments.map(function(attachment){
                                attachment.MEASUREMENT_DESC = attachment.FM_MEASUREMENT_DESC;
                            })
                        });
                        that.refresh();
                        that.setBusy(false);
                        that._needsReload = false;

                        if (successCb !== undefined) {
                            successCb();
                        }
                    }).catch(load_error.bind(that));

                }).catch(load_error.bind(that));
            };



            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadFuncLocMeasPoints")));
            };

            this.setBusy(true);
            var tableName = ["FuncLocMeasurementPoints", "AllFuncLocMeasurementPoints", "AM_MPoint"];
            this._requestHelper.getData(tableName, {
                funcLocId: object.FUNCT_LOC,
                tplnr: object.FUNCT_LOC,
                objectNo: object.OBJECT_NO,
            }, load_success, load_error, true);

        };

        EditMeasurementPointViewModel.prototype.loadMeasPointLongtext = function (object, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setLongtext(data);
                that.refresh();
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(data);
                    that.refresh();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadLongtextMeasPoint")));
            };

            this.setBusy(true);
            this._requestHelper.getData("MeasPointLongtext", {
                point: object.MEASUREMENT_POINT,
                language: this.getLanguage()
            }, load_success, load_error, true);

        };
        

        EditMeasurementPointViewModel.prototype.loadCodeML = function (point, successCb) {

            var that = this;

            var load_success = function (data) {
                that.setBusy(false);
                that._needsReload = false;
                if (data.length !== 0) {
                    point.CODE_TEXT = data[0].CODE_TEXT
                    point.catalogType = data;
                }

                if (successCb !== undefined) {
                    successCb(data);
                    that.refresh();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadLongtextMeasPoint")));
            };

            this.setBusy(true);
            this._requestHelper.getData("CodeMLMassinsp", {
                code: point.VLCOD,
                codgr: point.CODGR,
                spras: this.getLanguage()
            }, load_success, load_error, true);

        };

        EditMeasurementPointViewModel.prototype.loadCodeMLCodeGroup = function (point, successCb) {

            var that = this;

            var load_success = function (data) {
                that.setBusy(false);
                that._needsReload = false;
                if (data.length !== 0) {
                    data.sort(function(a, b) {
                        return a.CODE - b.CODE;
                    });
                    point.HelpTab = data;
                }

                if (successCb !== undefined) {
                    successCb(data);
                    that.refresh();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadLongtextMeasPoint")));
            };

            this._requestHelper.getData("CodeMLCodeGroup", {
                codgr: point.CODGR,
                spras: this.getLanguage()
            }, load_success, load_error, true);

        };

        EditMeasurementPointViewModel.prototype.setSRData = function (data) {
            if (data.BAUMM && data.BAUJJ) {
                data.CONSTRUCTIONDATE = data.BAUMM + "." + data.BAUJJ;
            }
            this.setProperty("/equipment", new Equipment(data, this._requestHelper, this));
        };

        EditMeasurementPointViewModel.prototype.getSRData = function () {
            return this.getProperty("/equipment");
        };

        EditMeasurementPointViewModel.prototype.loadEquipmentData = function (successCb) {
            var that = this;
            var object = this.getSelectedObject();

            var load_success = function (data) {
                if (object.IS_LOCAL == 'N') {
                    that.setOldData(data.OrderEquipmentMeasurementDocuments[0]);
                } else {
                    that.setNewData(data.EquipmentMeasurementPoints[0]);
                }

                that.setLastMeasDocData(data.EquipmentMeasurementDocuments);
                that.setSRData(data.EquipmentDetails[0]);

                var equipment = that.getSRData();
                equipment.loadAttachments().then(function () {
                    that.refresh();
                    that.setBusy(false);
                    that._needsReload = false;

                    if (successCb !== undefined) {
                        successCb();
                    }
                }).catch(load_error.bind(that));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadEquipmentMeasPoints")));
            };

            this.setBusy(true);

            var tableName;
            if (object.IS_LOCAL == 'N') {
                tableName = ["OrderEquipmentMeasurementDocuments", "EquipmentMeasurementDocuments", "EquipmentDetails"]
            } else {
                tableName = ["EquipmentMeasurementPoints", "EquipmentMeasurementDocuments", "EquipmentDetails"];
            }

            this._requestHelper.getData(tableName, {
                equipment: object.EQUIPMENT,
                language: this.getLanguage(),
                obId: object.OBJECT_NO,
                point: object.XA_MEASUREMENT_POINT
            }, load_success, load_error, true);
        };

        EditMeasurementPointViewModel.prototype.resetData = function () {
            this.setProperty("/measurePoints", []);
        };

        EditMeasurementPointViewModel.prototype.loadAMEquipmentData = function (object, model, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setNewAMData(data, model).then(function () {
                    var attachmentArr = [];
                    var allMeasurePoints = that.getProperty("/measurePoints");
                    allMeasurePoints.forEach(function(point){
                        attachmentArr.push(point.loadAttachments());
                    });
                    
                    Promise.all(attachmentArr).then(function () {
                        allMeasurePoints.forEach(function(point){
                            point.Attachments.map(function(attachment){
                                attachment.MEASUREMENT_DESC = attachment.EM_MEASUREMENT_DESC;
                            })
                        });
                        that.refresh();
                        that.setBusy(false);
                        that._needsReload = false;

                        if (successCb !== undefined) {
                            successCb();
                        }
                    }).catch(load_error.bind(that));
                }).catch(load_error.bind(that));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadEquipmentMeasPoints")));
            };

            this.setBusy(true);

            var tableName = ["EquipmentMeasurementPoints", "AllEquipmentMeasurementPoints", "AM_MPoint"];
            this._requestHelper.getData(tableName, {
                equipment: object.EQUIPMENT,
                language: this.getLanguage(),
                equnr: object.EQUIPMENT,
                objectNo: object.OBJECT_NO,
            }, load_success, load_error, true);
            that.refresh();

        };

        EditMeasurementPointViewModel.prototype.resetData = function () {
            this.setProperty("/measurePoints", []);
        };

        EditMeasurementPointViewModel.prototype.getCodeText = function (points) {

            var that = this;

            for (var i = 0; i < points.length; i++) {
                for (var n = 0; n < points[i].MeasurementDocuments.length; n++) {
                    for (var x = 0; x < points[i].HelpTab.length; x++) {
                        if (points[i].MeasurementDocuments[n].VLCOD == points[i].HelpTab[x].CODE ) {
                            points[i].MeasurementDocuments[n].CODE_TEXT = points[i].HelpTab[x].CODE_TEXT;
                            that.oData.measurePoints[i].MeasurementDocuments[n].CODE_TEXT = points[i].HelpTab[n].CODE_TEXT;
                            that.oData.measurePoints[i].MeasurementDocuments[n].IDATE = moment(points[i].MeasurementDocuments[n].IDATE).format("YYYY-MM-DD") + " " + moment(points[i].MeasurementDocuments[n].ITIME).format("HH:mm:ss");
                        }
                    }

                }
            }


        }

        EditMeasurementPointViewModel.prototype.validateWithCounter = function (point, documents) {
            var validator = new Validator(point, this.getValueStateModel());
            var that = this;
            var warning = [];
            var lastDocumentReading;

            if (documents.length > 0) {
                lastDocumentReading = parseFloat(documents[documents.length - 1].RECDV);
            } else {
                lastDocumentReading = 0;
            }

            validator.check("READING", this.component.i18n.getText("readingEmpty")).isEmpty();
            validator.check("", this.component.i18n.getText("READINGCREATERTHANLAST")).custom(function (value) {
                var reading = parseFloat(value.READING);
                return reading >= lastDocumentReading;
            });
            var checkWarning = function (value) {
                var reading = parseFloat(value.READING);
                var warning = [{
                    warningMessage: that.component.i18n.getText("READINGCREATERTHANLAST")
                }];

                return reading == lastDocumentReading ? warning : "";
            };

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            if (!errors) {
                warning = checkWarning(validator.dataModel);
            }
            this.setProperty("/view/warningMessages", warning ? warning : []);

            this.setIconForMeasurePoint(point, errors, warning);

            return errors ? false : true;
        };

        EditMeasurementPointViewModel.prototype.validate = function (point) {

            var validator = new Validator(point, this.getValueStateModel());
            var attachmentCounter = 0;
            var error_text;

            if ((point.READING === undefined || point.READING === '' ) && (point.VLCOD === undefined || point.VLCOD === '' )) {
                validator.check("READING", this.component.i18n.getText("readingEmpty")).isEmpty();
                error_text = this.component.i18n.getText("readingEmpty");
            } else if ((point.READING === '0041' || point.VLCOD === '0041')){
                if(point.Attachments.length == 0){
                    validator.check("FILENAME", this.component.i18n.getText("photoMissing")).custom(function () {
                        return false;
                    });
                    error_text = this.component.i18n.getText("photoMissing");
                }
            }

            if (error_text) {
                var errors = true;
            }

            this.setProperty("/view/errorMessages", errors ? errors : []);
            this.setIconForMeasurePoint(point, errors);
 
            return error_text;
        };

        EditMeasurementPointViewModel.prototype.validateData = function (point) {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("READING", this.component.i18n.getText("readingEmpty")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            this.setIconForMeasurePoint(point, errors);

            return errors ? false : true;
        };

        EditMeasurementPointViewModel.prototype.setIconForMeasurePoint = function (point, errors, warning) {
            var currentMeasurementPoint = point.MEASUREMENT_POINT;
            var allMeasurePoints = this.getProperty("/measurePoints");
            allMeasurePoints.forEach(function (measurePoint) {
                if (measurePoint.MEASUREMENT_POINT === currentMeasurementPoint) {
                    if (errors) {
                        measurePoint.valueState = "Error";
                        measurePoint.STATUSICON = 'sap-icon://error';
                        measurePoint.showStatusIcon = true;
                        measurePoint.showErrorMessage = true;
                        measurePoint.showWarningMessage = false;
                    }
                    if (warning && !errors) {
                        measurePoint.valueState = "Warning";
                        measurePoint.STATUSICON = 'sap-icon://accept';
                        measurePoint.showStatusIcon = true;
                        measurePoint.showWarningMessage = true;
                        measurePoint.showErrorMessage = false;
                    }
                    if (!warning && !errors) {
                        measurePoint.valueState = "None";
                        measurePoint.STATUSICON = 'sap-icon://accept';
                        measurePoint.showStatusIcon = true;
                        measurePoint.showErrorMessage = false;
                        measurePoint.showWarningMessage = false;
                    }
                }
            });
            this.setProperty("/measurePoints", allMeasurePoints);
        };

        EditMeasurementPointViewModel.prototype.checkLimits = function (point) {
            var validator = new Validator(point, this.getValueStateModel());
            var that = this;
            // TODO do correct validation of the measurementpoint
            var checkWarning = function (value) {
                var reading = parseFloat(value.READING);
                var minAcceptedValue = parseFloat(value.MRMIN);
                var maxAcceptedValue = parseFloat(value.MRMAX);
                var warning = [{
                    warningMessage: that.component.i18n.getText("READINGOUTSIDELIMITS")
                }];

                if (minAcceptedValue == 0 && maxAcceptedValue == 0) {
                    return "";
                }

                if (reading > maxAcceptedValue || reading < minAcceptedValue) {
                    return warning;
                }

                return "";
            };

            var warning = checkWarning(validator.dataModel);

            this.setProperty("/view/warningMessages", warning ? warning : []);

            this.setIconForMeasurePoint(point, "", warning);

            return true;

        };

        EditMeasurementPointViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MEASUREMENT_DESC: sap.ui.core.ValueState.None,
                    MEASUREMENT_POINT: sap.ui.core.ValueState.None,
                    TPLNR: sap.ui.core.ValueState.None,
                    EQUIPMENT: sap.ui.core.ValueState.None,
                });
            }

            return this.valueStateModel;
        };

        EditMeasurementPointViewModel.prototype.displayAMCodingDialog = function (oEvent, viewContext) {

            var codeGroup = oEvent.getSource().getBindingContext("aMMeasurementModel").getObject().CODGR;
            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingAMSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodingDialog.dialog.setModel(this._component.getModel("i18n_core"), "i18n");

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: codeGroup,
                    catalogType: "B"
                },
                actionName: "valueHelp"
            });
        };

        EditMeasurementPointViewModel.prototype.displayCodingDialog = function (oEvent, viewContext) {

            if (!this._oCodingDialog) {
                this._oCodingDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CodingSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCodingDialog.dialog.setModel(this._component.getModel("i18n_core"), "i18n_core");
            this._oCodingDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            this._oCodingDialog.display(oEvent, {
                tableName: "CodeMl",
                params: {
                    spras: this.getLanguage(),
                    codeGroup: "SRB-GEN",
                    catalogType: "T"
                },
                actionName: "valueHelp"
            });
        };


        EditMeasurementPointViewModel.prototype.handleAMCodingSelect = function (oEvent) {
            //var path = oEvent.getSource().parentInput.getBindingContext("measurementModel").getPath();
            var path = oEvent.getSource().parentInput.getBindingContext("aMMeasurementModel").getPath();

            this._oCodingDialog.select(oEvent, 'CODE', [{
                'READING': 'CODE'
            }, {
                'CODE_TEXT': 'CODE_TEXT',
            },{
                'VLCOD': 'CODE'
            }], this, path);
        };

        EditMeasurementPointViewModel.prototype.handleCodingSelect = function (oEvent) {

            this._oCodingDialog.select(oEvent, 'CODE', [{
                'CODCT': 'CATALOG_TYPE'
            }, {
                'VLCOD': 'CODE'
            }], this, "/measureDoc");


        };

        EditMeasurementPointViewModel.prototype.resetData = function () {
            this.setProperty("/measureDoc", []);
        };

        EditMeasurementPointViewModel.prototype.handleCodingSearch = function (oEvent) {
            this._oCodingDialog.search(oEvent, ["CODING", "_codingText"]);
        };


        EditMeasurementPointViewModel.prototype.insertNewMeasurementDocument = function (document, successCb) {
            var that = this;
            var equipment = this.getSRData();
            var attachments = equipment.getAttachments();

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            document.CVERS = "000001";

            var onSuccess = function (document) {
                successCb(document);
            };

            var onAttachmentsUpdated = function () {
                try {
                    document.insert(false, function (mobileId) {
                        document.MOBILE_ID = mobileId;
                        onSuccess(document);
                    }, onError.bind(this, new Error(this.component.i18n.getText("MEAS_DOC_INSERT_FAIL"))));
                } catch (err) {
                    this.executeErrorCallback(err);
                }
            };

            this._updateAttachments(attachments).then(onAttachmentsUpdated.bind(this)).catch(onError.bind(this));
        };

        EditMeasurementPointViewModel.prototype.updateNewMeasurementDocument = function (document, successCb) {
            var that = this;
            var equipment = this.getSRData();
            var attachments = equipment.getAttachments();

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (document) {
                successCb(document);
            };

            var onAttachmentsUpdated = function () {
                try {
                    document.update(false, function (mobileId) {
                        onSuccess(document);
                    }, onError.bind(this, new Error(this.component.i18n.getText("MEAS_DOC_INSERT_FAIL"))));
                } catch (err) {
                    this.executeErrorCallback(err);
                }
            };

            this._updateAttachments(attachments).then(onAttachmentsUpdated.bind(this)).catch(onError.bind(this));
        };

        EditMeasurementPointViewModel.prototype.validateMeasurementDocumentData = function () {

            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("measureDoc.VLCOD", this.component.i18n.getText("READING_EMPTY_ERROR")).isEmpty();
            validator.check("measureDoc.ERDAT", this.component.i18n.getText("READING_DATE_EMPTY_ERROR")).isEmpty();

            if (this.getProperty("/measureDoc").VLCOD == "0202") {
                validator.check("measureDoc.MDTXT", this.component.i18n.getText("READING_EMPTY_MEAS_DOC_COLUMN3")).isEmpty();
            }

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditMeasurementPointViewModel.prototype.insertUserAttachment = function (attachment, successCb) {
            var that = this;
            var sqlStrings = [];

            try {
                attachment.insert(false, function (mobileId) {
                    attachment.MOBILE_ID = mobileId;
                    successCb();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_ATTACHMENT_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EditMeasurementPointViewModel.prototype.updateUserAttachment = function (attachment, successCb) {
            var that = this;

            if (attachment.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error(this._component.i18n.getText("ATTACHMENT_DELETE_ONLY_WHEN_ACTION_FLAG_N")));
                return;
            }

            try {
                attachment.update(false, successCb, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        EditMeasurementPointViewModel.prototype.deleteUserAttachment = function (attachment, successCb) {
            var that = this;

            attachment.delete(false, successCb, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_DELETE")));
            });
        };

        EditMeasurementPointViewModel.prototype.saveAMMeasurementPoint = function (objNo, successCb, errorCb) {
            var that = this;
            var docArr = this.getProperty("/measurePoints");
            var loadDocArr = [];
            docArr.forEach(function(point){
                point.MEASUREMENT_POINT = point.POINT;
                loadDocArr.push(that.loadMeasurementDocuments(point));
            });

            Promise.all(loadDocArr).then(function(result){
                var insertUpdateArr = [];

                for(var i = 0 ; i < result.length; i++){
                    var measDocs = result[i];
                    var point = docArr[i];
                    insertUpdateArr.push(that.insertUpdatePromise(point, measDocs, objNo));
                }
                
                Promise.all(insertUpdateArr).then(function(){
                    successCb();
                }).catch(function(err){
                    errorCb();
                });
            });
            
        };

        EditMeasurementPointViewModel.prototype.validateOnCancelAMMeasurementPoint = function () {
            var docArr = this.getProperty("/measurePoints");
            var error_text;
            docArr.forEach(function(point){
                point.MEASUREMENT_POINT = point.POINT;
                var validator = new Validator(point, this.getValueStateModel());
                if ((point.READING === '0041' || point.VLCOD === '0041')){
                    if(point.Attachments.length == 0){
                        validator.check("FILENAME", this.component.i18n.getText("photoMissing")).custom(function () {
                            return false;
                        });
                        error_text = this.component.i18n.getText("photoMissing");
            
                        this.setIconForMeasurePoint(point, true);
                    }
                }
            }.bind(this));
            if(error_text !== undefined){
                MessageToast.show(error_text);
            }
            return error_text === undefined;
        };

        EditMeasurementPointViewModel.prototype.insertUpdatePromise = function (point, measDocs, objNo) {
            var that = this;
            return new Promise(function (resolve, reject) {
                if(measDocs.length > 0){
                    var documents = measDocs;
                    var lastDocument = documents[documents.length - 1];

                    if (point.CODGR !== "" && point.READING !== "") {
                        if (lastDocument.ACTION_FLAG === "N") {
                            that.updateDocument(point, lastDocument, resolve, reject);
                        } else {
                            that.insertDocument(point, objNo, resolve, reject);
                        }
                    }
                }else{
                    that.insertDocument(point, objNo, resolve, reject);
                }
            });
        };

        EditMeasurementPointViewModel.prototype.saveMeasurementPoint = function (successCb) {
            if (this.validateMeasurementDocumentData()) {
                var document = this.getProperty("/measureDoc");
                if (this.getSelectedObject().IS_LOCAL == 'N') {
                    this.updateNewMeasurementDocument(document, successCb);
                } else {
                    this.insertNewMeasurementDocument(document, successCb);
                }
            } else {
                throw new Error("VALIDATION_FAILED");
            }
        };

        EditMeasurementPointViewModel.prototype._updateAttachments = function (attachments) {
            var that = this;
            var sqlStrings = [];
            var updateQueue = [];

            // Attachments
            // Updates -> Rename actual attachment
            var updates = attachments.filter(function (attachment) {
                return attachment.MOBILE_ID != "" && attachment.hasChanges();
            });

            // Inserts -> Move from temp to doc directory
            var inserts = attachments.filter(function (attachment) {
                return attachment.MOBILE_ID == "";
            });

            if (updates.length > 0) {
                updateQueue.push(this._renameAttachments(updates));
            }

            if (inserts.length > 0) {
                updateQueue.push(this._moveAttachmentsFromTempToDoc(inserts));
            }

            return Promise.all(updateQueue).then(function () {
                return new Promise(function (resolve, reject) {
                    updates.forEach(function (attachment) {
                        sqlStrings.push(attachment.update(true));
                    });

                    inserts.forEach(function (attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    that._requestHelper.executeStatementsAndCommit(sqlStrings, resolve, reject);
                });
            });
        };

        EditMeasurementPointViewModel.prototype._updateAMAttachments = function (point) {
            var that = this;
            var sqlStrings = [];
            var updateQueue = [];
            var attachments = point.Attachments;
            // Attachments
            // Updates -> Rename actual attachment
            var updates = attachments.filter(function (attachment) {
                return attachment.MOBILE_ID != "" && attachment.hasChanges();
            });

            // Inserts -> Move from temp to doc directory
            var inserts = attachments.filter(function (attachment) {
                return attachment.MOBILE_ID == "";
            });

            if (updates.length > 0) {
                updateQueue.push(this._renameAttachments(updates));
            }

            if (inserts.length > 0) {
                updateQueue.push(this._moveAttachmentsFromTempToDoc(inserts));
            }

            return Promise.all(updateQueue).then(function () {
                return new Promise(function (resolve, reject) {
                    updates.forEach(function (attachment) {
                        sqlStrings.push(attachment.update(true));
                    });

                    inserts.forEach(function (attachment) {
                        sqlStrings.push(attachment.insert(true));
                    });

                    if(sqlStrings.length > 0){
                        that._requestHelper.executeStatementsAndCommit(sqlStrings, function(){
                            point.loadAttachments(true).then(function(attachments){
                                attachments.map(function(attachment){
                                    if(attachment.EM_MEASUREMENT_DESC != ""){
                                        attachment.MEASUREMENT_DESC = attachment.EM_MEASUREMENT_DESC;
                                    }else if(attachment.FM_MEASUREMENT_DESC != ""){
                                        attachment.MEASUREMENT_DESC = attachment.FM_MEASUREMENT_DESC;
                                    }
                                })
                                resolve();
                            });
                        }, reject);
                    }else{
                        resolve();
                    }
                });
            });
        };

        EditMeasurementPointViewModel.prototype.insertDocument = function (point, objNo, successCb, errorCb) {
            var that = this;

            var document = that.measuringDocumentHistoryViewModel.getNewMeasurementDocument(point);
            if (point.READING === undefined) {
                point.READING = point.VLCOD;
            }
            document.POINT = point.POINT;
            document.VLCOD = point.READING;
            document.WOOBJ = objNo;
            document.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
            document.ITIME = moment(new Date()).format("HH:mm:ss");
            document.MDTXT = point.LONGTEXT;
            document.READG_CHAR = point.CODE_TEXT;
            document.RECDV = point.CODE_TEXT;
            document.RECDV_CHAR = point.CODE_TEXT;
            point.VLCOD = point.READING;
            var errorMessage = that.validate(point);
            if (errorMessage === undefined) {
                var onAttachmentsUpdated = function () {
                        that.measuringDocumentHistoryViewModel.insertNewMeasurementDocument(document, point, point.catalogType, successCb);
                };

                var allMeasurePoints = that.getProperty("/measurePoints");  
                var pointObj = allMeasurePoints.find(x => x.POINT == point.POINT);                      
                                        
                if (pointObj.Attachments && pointObj.Attachments.length > 0) {
                    this._updateAMAttachments(pointObj).then(onAttachmentsUpdated.bind(this)).catch(function(err){
                    });
                } else {
                    onAttachmentsUpdated();
                }
            }else {
                MessageToast.show(errorMessage);
                errorCb();
            }
        };

        EditMeasurementPointViewModel.prototype.updateDocument = function (point, lastDocument, successCb, errorCb) {
            var that = this;
            var errorMessage = that.validate(point);
            if (errorMessage=== undefined) {
                var onAttachmentsUpdated = function () {
                        that.measuringDocumentHistoryViewModel.updateMeasurementDocument(lastDocument, point, point.catalogType, successCb);
                };

                var allMeasurePoints = that.getProperty("/measurePoints");  
                var pointObj = allMeasurePoints.find(x => x.POINT == point.POINT);                      
                                        
                if (pointObj.Attachments && pointObj.Attachments.length > 0) {
                    this._updateAMAttachments(pointObj).then(onAttachmentsUpdated.bind(this)).catch(function(err){
                    });
                } else {
                    onAttachmentsUpdated();
                }
            }else {
                MessageToast.show(errorMessage);
                errorCb();
            }
        };

        EditMeasurementPointViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    ERDAT: sap.ui.core.ValueState.None,
                    CODE_TEXT: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditMeasurementPointViewModel.prototype.constructor = EditMeasurementPointViewModel;

        return EditMeasurementPointViewModel;
    });
