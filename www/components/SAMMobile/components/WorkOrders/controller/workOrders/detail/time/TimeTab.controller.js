sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/EditTimeEntryViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/CopyTimeEntriesViewModel",
    "sap/m/Button",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp"
], function (Controller, EditTimeEntryViewModel, CopyTimeEntriesViewModel, Button, Dialog, Text, MessageToast, MessageBox, formatter, formatterComp) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.time.TimeTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailTime").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailTime", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = null;
            this.timeTabViewModel = null;

            this.timeEntriesTable = this.byId("timeConfirmationsTable");
        },

        onRouteMatched: function (oEvent) {
            
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.timeTabViewModel = this.workOrderDetailViewModel.tabs.time.model;

                this.getView().setModel(this.timeTabViewModel, "timeTabViewModel");
            }

            this.loadViewModelData();

            this.timeEntriesTable.removeSelections();
            if (this.timeTabViewModel.getOrder()._IS_EDITABLE || this.timeTabViewModel.getOrder().OPERATION_USER_STATUS === this.component.scenario.getOrderUserStatus().done.STATUS ||  this.timeTabViewModel.getOrder().ACTION_FLAG === 'U'){
            	this.timeTabViewModel.setEditable(true);
            }else{
            	this.timeTabViewModel.setEditable(false);
            }
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailTime", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.timeTabViewModel.getOrder().resetTimeEntries();
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.timeTabViewModel.loadData(function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });
        },

        onTableSelectionChanged: function (oEvent) {
            this.timeTabViewModel.setSelectedTimeEntries(oEvent.getSource().getSelectedItems());
        },

        onMinutesChanged: function (oEvent) {
            this.editTimeEntryViewModel.setMinutes(oEvent.getParameter("value"));
        },


        onStartTimeChanged: function (oEvent) {
            this.editTimeEntryViewModel.setStartTime(oEvent.getParameter("value"));
            //On change of start date, the end date should be equal to end date
            this.editTimeEntryViewModel.setEndTime(this.editTimeEntryViewModel.getData().timeEntry.START_TIME.substring(0,10) + this.editTimeEntryViewModel.getData().timeEntry.END_TIME.substring(10)  + "");
        },

        onEndTimeChanged: function (oEvent){
            this.editTimeEntryViewModel.setEndTime(oEvent.getParameter("value"));
        },

        onAddTimeEntryPressed: function () {
            var that = this;
            var onAcceptPressed = function (timeEntry, dialog) {

                var insertFunction = function (timeEntry, dialog) {
                    that.timeTabViewModel.insertNewTimeEntry(timeEntry, function () {
                        dialog.close();
                        that.workOrderDetailViewModel.loadAllCounters();
                        MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_SUCCESS_TEXT"));
                    });
                };

                if((!timeEntry.B_VALUE_ID || timeEntry.B_VALUE_ID !== "5") && that.timeTabViewModel._hasReachedMaxDailyWork(timeEntry)) {
                    MessageBox.confirm(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TEXT"), {
                        icon: MessageBox.Icon.INFORMATION,
                        title: that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TITLE"),
                        actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                        onClose: function(oAction) {
                            if (oAction === "YES") {
                                insertFunction(timeEntry, dialog);
                            }
                        }
                    });
                } else {
                    insertFunction(timeEntry, dialog);
                }

            };

            var openDialog = function (colleagues) {
                var dialog = that._getTimeEntryEditDialog(that.timeTabViewModel.getNewEditTimeEntry(), colleagues, false, onAcceptPressed);
                dialog.open();
            };

            try {
                this.timeTabViewModel.getColleagues(openDialog);
            } catch (err) {
                openDialog([]);
            }

        },

        onEditTimeEntryPressed: function (oEvent) {
            var that = this;
            var timeEntry = oEvent.getSource().getBindingContext("timeTabViewModel").getObject();
            var onAcceptPressed = function (timeEntry, dialog) {

                var updateFunction = function (timeEntry, dialog) {
                    that.timeTabViewModel.updateTimeEntry(timeEntry, function () {
                        dialog.close();
                        MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_UPDATE_TIME_ENTRY_SUCCESS_TEXT"));
                    });
                };

                if((!timeEntry.B_VALUE_ID || timeEntry.B_VALUE_ID !== "5") && that.timeTabViewModel._hasReachedMaxDailyWork(timeEntry)) {
                    MessageBox.confirm(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TEXT"), {
                        icon: MessageBox.Icon.INFORMATION,
                        title: that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TITLE"),
                        actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                        onClose: function(oAction) {
                            if (oAction === "YES") {
                                updateFunction(timeEntry, dialog);
                            }
                        }
                    });
                } else {
                    updateFunction(timeEntry, dialog);
                }

            };

            var openDialog = function (colleagues) {
                var dialog = that._getTimeEntryEditDialog(timeEntry, colleagues, true, onAcceptPressed);
                dialog.open();
            };

            try {
                this.timeTabViewModel.getColleagues(openDialog);
            } catch (err) {
                openDialog([]);
            }
        },

        onDeleteTimeEntryPressed: function (oEvent) {
            var that = this;
            var timeEntry = oEvent.getSource().getBindingContext("timeTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.timeTabViewModel.deleteTimeEntry(timeEntry, function () {
                    that.workOrderDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_DELETE_TIME_ENTRY_SUCCESS_TEXT"));
                });
            }).open();
        },
        onCopyTimeEntryPressed: function(oEvent){
             var that = this;
            
             // get dataset to copy
             var timeEntry = oEvent.getSource().getBindingContext("timeTabViewModel").getObject();
              // get missing data like orderid, userid, etc
             var newTimeEntry = that.timeTabViewModel.getNewEditTimeEntry();
             var timeEntryMerged = that.timeTabViewModel.getMergedTimeEntry(timeEntry,newTimeEntry);
             
                                
            var onAcceptPressed = function (timeEntry, dialog) {

                var insertFunction = function (timeEntry, dialog) {
                    that.timeTabViewModel.insertNewTimeEntry(timeEntry, function () {
                        dialog.close();
                        that.workOrderDetailViewModel.loadAllCounters();
                        MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_SUCCESS_TEXT"));
                    });
                };

                if((!timeEntry.B_VALUE_ID || timeEntry.B_VALUE_ID !== "5") && that.timeTabViewModel._hasReachedMaxDailyWork(timeEntry)) {
                    MessageBox.confirm(that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TEXT"), {
                        icon: MessageBox.Icon.INFORMATION,
                        title: that.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TITLE"),
                        actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                        onClose: function(oAction) {
                            if (oAction === "YES") {
                                insertFunction(timeEntry, dialog);
                            }
                        }
                    });
                } else {
                    insertFunction(timeEntry, dialog);
                }

            };

            var openDialog = function (colleagues) {
                var dialog = that._getTimeEntryEditDialog(timeEntryMerged, colleagues, false, onAcceptPressed);
                dialog.open();
            };

            try {
                this.timeTabViewModel.getColleagues(openDialog);
            } catch (err) {
                openDialog([]);
            }
        },

        // Select dialogs

        displayTeamMemberSelectDialog: function (oEvent) {
            this.editTimeEntryViewModel.displayTeamMemberSelectDialog(oEvent, this);
        },

        handleTeamMemberSelect: function (oEvent) {
            this.editTimeEntryViewModel.handleTeamMemberSelect(oEvent);
        },

        handleTeamMemberSearch: function (oEvent) {
            this.editTimeEntryViewModel.handleTeamMemberSearch(oEvent);
        },

        displayActTypeSelectDialog: function (oEvent) {
            this.editTimeEntryViewModel.displayActTypeDialog(oEvent, this);
        },

        handleActTypeSelect: function (oEvent) {
            this.editTimeEntryViewModel.handleActTypeSelect(oEvent);
        },

        handleActTypeSearch: function (oEvent) {
            this.editTimeEntryViewModel.handleActTypeSearch(oEvent);
        },

        _getTimeEntryEditDialog: function (timeEntry, colleagues, bEdit, acceptCb) {
            var onAcceptPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");

                if (editTimeEntryViewModel.validate()) {
                    acceptCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editTimeEntryViewModel = dialog.getModel("timeEntryModel");
                editTimeEntryViewModel.rollbackChanges();
                dialog.close();
            };

            this.editTimeEntryViewModel = new EditTimeEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTimeEntryViewModel.setEdit(bEdit);
            this.editTimeEntryViewModel.setTimeEntry(timeEntry);
            this.editTimeEntryViewModel.setColleagues(colleagues);

            if (!this._timeEntryEditPopup) {

                if (!this._timeEntryPopover) {
                    this._timeEntryPopover = sap.ui.xmlfragment(this.createId("editTimeEntryPopover"), "SAMMobile.components.WorkOrders.view.workOrders.detail.time.TimeEntryEditPopover", this);

                    // Prevent direct user input for DateTimePicker.
                    // Unfortunately DATETIMEPICKER object do not offer such a property, that´s why we need to set the input field as readonly manually
                    var oStartDateTimePicker = sap.ui.core.Fragment.byId("editTimeEntryPopover", "timeconf_start_time");
                    var oEndDateTimePicker = sap.ui.core.Fragment.byId("editTimeEntryPopover", "timeconf_end_time");

                    oStartDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oStartDateTimePicker);

                    oEndDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oEndDateTimePicker);
                }

                this._timeEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._timeEntryPopover,
                    beginButton: new Button({
                        text: '{= ${timeEntryModel>/view/edit} ? ${i18n_core>update} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {

                    }
                });

                this._timeEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._timeEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._timeEntryEditPopup.setTitle(this.editTimeEntryViewModel.getDialogTitle());
            this._timeEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._timeEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel, "timeEntryModel");
            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel.getValueStateModel(), "valueStateModel");

            return this._timeEntryEditPopup;
        },

        _getCopyToColleaguesDialog: function (colleagues, acceptCb) {
            var that = this;
            var onAcceptPressed = function () {
                var copyTimeEntriesViewModel = this.getParent().getModel("copyTimeEntriesViewModel");

                acceptCb(copyTimeEntriesViewModel.getSelectedColleagues(), this.getParent());
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            this.copyTimeEntriesViewModel = new CopyTimeEntriesViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.copyTimeEntriesViewModel.setColleagues(colleagues);

            if (!this._copyColleaguesDialog) {
                this._copyColleaguesPopover = sap.ui.xmlfragment("copyToColleaguesPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.time.TimeEntriesCopyPopover", this.copyTimeEntriesViewModel);

                this._copyColleaguesDialog = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "New Component",
                    content: this._copyColleaguesPopover,
                    beginButton: new Button({
                        text: '{i18n>copyToColl}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                        //this.destroy();
                    }
                });

                this._copyColleaguesDialog.setModel(this.component.getModel('i18n'), "i18n");
                this._copyColleaguesDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");

                this._copyColleaguesDialog.setModel(this.copyTimeEntriesViewModel, "copyTimeEntriesViewModel");
            }


            this._copyColleaguesDialog.setTitle(this.copyTimeEntriesViewModel.getDialogTitle());
            this._copyColleaguesDialog.getBeginButton().mEventRegistry.press = [];
            this._copyColleaguesDialog.getBeginButton().attachPress(onAcceptPressed);


            return this._copyColleaguesDialog;
        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("deleteTimeEntryConfirmation"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        actWorkToHours: function (actWork) {
            return parseInt(parseFloat(actWork)); //moment.utc(parseFloat(actWork)*3600000).hours();
        }
    });

});