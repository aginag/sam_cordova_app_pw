sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/EquipmentCharacteristic",
        "SAMMobile/models/dataObjects/FuncLocCharacteristic",
        "sap/m/MessageToast",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, EquipmentCharacteristic, FuncLocCharacteristic, MessageToast, GenericSelectDialog) {
        "use strict";

        // constructor
        function EditClassificationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this._FUNCLOC = null;
            this.component = component;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditClassificationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            getQueryParams: {
                value: function () {
                    return {
                        orderId: this._ORDERID,
                        spras: this.getLanguage(),
                        persNo: this.getUserInformation().personnelNumber
                    };
                }
            },

            setFunctionLocation: {
                value: function (funcLoc) {
                    this._FUNCLOC = funcLoc;
                }
            },

            getFunctionLocation: {
                value: function () {
                    return this._FUNCLOC;
                }
            },

            getMeasurementPoints: {
                value: function () {
                    return this.getProperty("/funcLocMeasurePoints");
                }
            },

            setMeasurementPoints: {
                value: function (points) {
                    this.setProperty("/funcLocMeasurePoints", points);
                }
            },

            getClassifications: {
                value: function () {
                    return this.getProperty("/classifications");
                }
            },

            setClassifications: {
                value: function (classifications) {
                    var that = this;
                    var mappedClassifications = classifications.map(function (characteristic) {
                        if (characteristic.EQUNR) {
                            characteristic.newClassField = characteristic.CLASS;
                            return new EquipmentCharacteristic(characteristic, that._requestHelper, that);
                        } else {
                            return new FuncLocCharacteristic(characteristic, that._requestHelper, that);
                        }
                    });
                    this.setProperty("/classifications", mappedClassifications);
                }
            },

            getClassification: {
                value: function () {
                    return this.getProperty("/classification");
                }
            },

            setClassification: {
                value: function (classification) {
                    this.setProperty("/classification", classification);
                }
            },

            getFuncLocId: {
                value: function () {
                    return this.getProperty("/functionalLocation/id");
                }
            },

            getEquipmentId: {
                value: function () {
                    return this.getProperty("/equipment/id");
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            getCharacteristicsSQLChangeString: {
                value: function () {
                    return this.getClassification().setChanged(true);
                }
            },


            setDataFromOrder: {
                value: function (order) {
                    this.setProperty("/functionalLocation", {
                        id: order.FUNCT_LOC,
                        description: order.FUNCLOC_DESC
                    });

                    this.setProperty("/equipment", {
                        id: order.EQUIPMENT,
                        description: order.EQUIPMENT_DESC
                    });
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        EditClassificationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditClassificationViewModel.prototype.setNewData = function (data) {

            var characObj = data.reduce((unique, o) => {
                if(!unique.some(obj => obj.CHARACT === o.CHARACT)) {
                  unique.push(o);
                }
                return unique;
            },[]);

            characObj.forEach(function (characteristic) {
                characteristic.VALUE_NEUTRAL_FROM = characteristic.VALUE_NEUTRAL_FROM.trim();
                var mask = characteristic.ATSCH;
                if (characteristic.ATFOR === 'NUM') {
                    characteristic.maskValue = mask.replace(new RegExp('_', 'g'), '9');
                } else {
                    characteristic.maskValue = mask.replace(new RegExp('_', 'g'), '9');
                }
            });
            this.setClassifications(data);
        };

        EditClassificationViewModel.prototype.loadFuncLocData = function (functionLocation, successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadFuncLocMeasPoints")));
            };

            this.setBusy(true);
            this._requestHelper.getData("FuncLocCharacs", {
                funcLoc: functionLocation.FUNCT_LOC,
                language: this.getLanguage()
            }, load_success, load_error, true);

        };

        EditClassificationViewModel.prototype.loadEquipmentData = function (equipment, successCb) {
            var that = this;
            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadEquipmentMeasPoints")));
            };

            this.setBusy(true);
            this._requestHelper.getData("EquipmentCharacs", {
                equipment: equipment.EQUIPMENT,
                language: this.getLanguage()
            }, load_success, load_error, true);

        };

        EditClassificationViewModel.prototype.resetData = function () {
            this.setProperty("/classifications", []);
        };

        EditClassificationViewModel.prototype.validateSelectedCharacteristicValue = function (characteristic) {
            var validator = new Validator(characteristic, this.getValueStateModel());

            validator.check("VALUE_NEUTRAL_FROM", this.component.i18n.getText("READING_EMPTY")).isEmpty();
            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditClassificationViewModel.prototype.validateCharacteristicValue = function (characteristic) {
            var validator = new Validator(characteristic, this.getValueStateModel());

            validator.check("VALUE_NEUTRAL_FROM", this.component.i18n.getText("READING_EMPTY")).isEmpty();
            validator.check("", this.component.i18n.getText("READING_AMOUNT_DIGITS_INCORRECT")).custom(function (value) {
                var reading = value.VALUE_NEUTRAL_FROM;
                var readingLength = reading.length;
                if (reading.includes('.')) {
                    readingLength = readingLength - 1;
                }
                if (readingLength !== parseInt(value.ANZST) || reading.includes('_')) {
                    return false;
                } else {
                    return true;
                }
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        EditClassificationViewModel.prototype.updateCharacteristic = function (characObject, acceptCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                acceptCb();
                that.refresh();
            };

            try {
                if (characObject.hasChanges()) {
                    sqlStrings.push(characObject.update(true));
                }

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that.component.i18n.getText("ERROR_CHARACTERISTIC")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }

        };

        EditClassificationViewModel.prototype.displayCharacValueSearch = function (oEvent, viewContext) {
            this.selectedCharacteristicBC = oEvent.getSource().getBindingContext('classificationModel');
            var selectedCharacteristic = this.selectedCharacteristicBC.getObject();
            if (!this._oCharacteristicValueDialog) {
                this._oCharacteristicValueDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.CharacteristicValueSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oCharacteristicValueDialog.dialog.setModel(this._component.getModel('i18n_core'), "i18n");

            this._oCharacteristicValueDialog.display(oEvent, {
                tableName: 'CharacteristicValues',
                params: {
                    atinn: selectedCharacteristic.ATINN,
                    language: this.getLanguage()
                }
            });
        };

        EditClassificationViewModel.prototype.handleCharacValueSearch = function (oEvent) {
            this._oCharacteristicValueDialog.search(oEvent, ['ATWRT', 'ATWTB']);
        };

        EditClassificationViewModel.prototype.handleCharacValueSelect = function (oEvent) {
            var sPath = this.selectedCharacteristicBC.getPath();
            var selectedCharacteristic = this.selectedCharacteristicBC.getObject();

            this._oCharacteristicValueDialog.select(oEvent, 'ATWRT', [{
                'VALUE_NEUTRAL_FROM': 'ATWRT',
                'ATWTB': 'ATWTB'
            },], this, sPath);

            this.updateSelectedCharacteristic(selectedCharacteristic, sPath);
            this.refresh();

        };

        EditClassificationViewModel.prototype.updateSelectedCharacteristic = function (selectedCharacteristic, sPath) {
            var that = this;
            if (this.validateSelectedCharacteristicValue(selectedCharacteristic)) {
                var valueCharacteristic = selectedCharacteristic.VALUE_NEUTRAL_FROM;
                that.updateCharacteristic(selectedCharacteristic, function () {
                });
                MessageToast.show(this.component.i18n.getText("SUCCESS_CHARACTERISTIC"));
                that.setProperty(sPath + '/VALUE_NEUTRAL_FROM', valueCharacteristic.toString());
            }
        };

        EditClassificationViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    VALUE_NEUTRAL_FROM: sap.ui.core.ValueState.None,
                    CLASS: sap.ui.core.ValueState.None,
                    CHARACT: sap.ui.core.ValueState.None,
                    CURRENCY_FROM: sap.ui.core.ValueState.None,
                    CURRENCY_TO: sap.ui.core.ValueState.None,
                    EQUNR: sap.ui.core.ValueState.None,
                    TYPE: sap.ui.core.ValueState.None,
                    UNIT_FROM: sap.ui.core.ValueState.None,
                    UNIT_TO: sap.ui.core.ValueState.None,
                    VALUE_DATE: sap.ui.core.ValueState.None,
                    VALUE_NEUTRAL_TO: sap.ui.core.ValueState.None,
                    VALUE_TO: sap.ui.core.ValueState.None,
                    ANZDZ: sap.ui.core.ValueState.None,
                    ANZST: sap.ui.core.ValueState.None,
                    ATFOR: sap.ui.core.ValueState.None,
                    ATINN: sap.ui.core.ValueState.None,
                    ATSCH: sap.ui.core.ValueState.None,
                    ATSON: sap.ui.core.ValueState.None,
                    ATWTB: sap.ui.core.ValueState.None,
                    MSEHT: sap.ui.core.ValueState.None,
                    POSNR: sap.ui.core.ValueState.None,
                });
            }

            return this.valueStateModel;
        };

        EditClassificationViewModel.prototype.constructor = EditClassificationViewModel;

        return EditClassificationViewModel;
    });
