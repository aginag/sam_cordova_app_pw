sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/TimeConfirmation",
        "SAMMobile/helpers/timeHelper"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, TimeConfirmation, TimeHelper) {
        "use strict";

        // constructor
        function TimeTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        TimeTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        timeEntries: [],
                        allTimeEntries: [],
                        view: { 
                        	editable: false,
                            busy: false,
                            errorMessages: [],
                            selectedTimeEntries: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            getOrderSQLChangeString: {
                value: function () {
                    return this.getOrder().setChanged(true);
                }
            },

            addTimeEntry: {
                value: function (entry) {
                    this.getProperty("/timeEntries").push(entry);
                    this.refresh();
                }
            },

            setSelectedTimeEntries: {
                value: function (listItems) {
                    var timeEntries = listItems.map(function (li) {
                        return li.getBindingContext("timeTabViewModel").getObject();
                    });

                    this.setProperty("/view/selectedTimeEntries", timeEntries);
                }
            },

            getSelectedTimeEntries: {
                value: function () {
                    return this.getProperty("/view/selectedTimeEntries");
                }
            },

            getColleagues: {
                value: function (successCb) {
                    var that = this;
                    successCb([]);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            setEditable: {
                value: function (bEditable) {
                    this.setProperty("/view/editable", bEditable);
                }
            }
        });

        TimeTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        TimeTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setProperty("/timeEntries", data);
                that.setBusy(false);
                that._needsReload = false;

                var load_timeConfirmation_success = function (data) {
                    var aData = data.map(function (oData) {
                        return new TimeConfirmation(oData, that._requestHelper, that);
                    });

                    that.setProperty("/allTimeEntries", aData);
                    if (successCb !== undefined) {
                        successCb();
                    }
                };

                var load_timeConfirmation_error = function (err) {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_TIME_TAB_TEXT")));
                };

                that._requestHelper.getData(
                    "TimeConfirmation",
                    {persNo: that._component.globalViewModel.model.getProperty("/personnelNumber")},
                    load_timeConfirmation_success,
                    load_timeConfirmation_error,
                    true,
                    "getForUser"
                );

            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_TIME_TAB_TEXT")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_ORDER_NOT_FOUND_TEXT")));
            }

            this.setSelectedTimeEntries([]);
            this.setBusy(true);
            this.getOrder().loadTimeEntries(load_success);
        };

        TimeTabViewModel.prototype.insertNewTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var sqlStrings = [];

            var aOverlappingTimeConfirmations = TimeHelper.getOverlappingTimeConfirmation(timeEntry, this.getProperty("/allTimeEntries"), this.getLanguage());
            if (aOverlappingTimeConfirmations && aOverlappingTimeConfirmations.length > 0) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_OVERLAPPING_TIMES_WARNING_TEXT")));
                return;
            }

            timeEntry.ACT_WORK = String(timeEntry.ACT_WORK);

            var currentTime = TimeHelper.getCurrentDateObject();
            timeEntry.CREATE_DAT = currentTime.dateString;
            timeEntry.CREATE_TIM = currentTime._dateString;

            this._handleKeyValueFields(timeEntry);

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (data) {
                that.insertToModelPath(data, "/timeEntries");
                that.insertToModelPath(data, "/allTimeEntries");
                successCb();
            };

            try {

                timeEntry.insert(false, function (mobileId) {
                    timeEntry.MOBILE_ID = mobileId;

                    onSuccess(new TimeConfirmation(timeEntry, that._requestHelper, that));
                }, onError.bind(this, new Error(this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_ERROR_TEXT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        TimeTabViewModel.prototype.copyToColleagues = function (colleagues, successCb) {
            var that = this;
            var sqlStrings = [];
            var timeEntriesToCopy = this.getSelectedTimeEntries();

            var onSuccess = function () {
                //need to reload data
                that.getOrder().resetTimeEntries();
                that.loadData(function () {
                    successCb();
                });
            };

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            try {
                timeEntriesToCopy.forEach(function (timeEntry) {

                    colleagues.forEach(function (colleague) {
                        var timeConfirmation = new TimeConfirmation(timeEntry.getCopy(), that._requestHelper, that);
                        timeConfirmation.PERNR = colleague.getPersNo();
                        timeConfirmation.ACT_TYPE = colleague.getActivityType();
                        timeConfirmation.CONF_NO = timeConfirmation.getRandomCharUUID(10);
                        timeConfirmation.CONF_CNT = timeConfirmation.getRandomCharUUID(8);

                        timeConfirmation.ACTION_FLAG = "N";

                        sqlStrings.push(timeConfirmation.insert(true));
                    });

                });

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, onError.bind(this, new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_UPDATING_PARENT_ORDER"))));
            } catch (err) {
                onError(err);
            }
        };

        TimeTabViewModel.prototype.updateTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function () {
                var oNewTimeEntry = new TimeConfirmation(timeEntry, that._requestHelper, that);
                that.updateModelPath(timeEntry, "/timeEntries", oNewTimeEntry);
                that.updateModelPath(timeEntry, "/allTimeEntries", oNewTimeEntry);
                that.refresh();
                successCb();
            };

            var aOverlappingTimeConfirmations = TimeHelper.getOverlappingTimeConfirmation(timeEntry, this.getProperty("/allTimeEntries"), this.getLanguage());
            if (aOverlappingTimeConfirmations && aOverlappingTimeConfirmations.length > 0) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_OVERLAPPING_TIMES_WARNING_TEXT")));
                return;
            }

            try {
                this._handleKeyValueFields(timeEntry);

                sqlStrings.push(timeEntry.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_UPDATING_COMPONENT")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        TimeTabViewModel.prototype.deleteTimeEntry = function (timeEntry, successCb) {
            var that = this;
            var sqlStrings = [];

            sqlStrings = sqlStrings.concat(timeEntry.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(timeEntry, "/timeEntries");
                that.removeFromModelPath(timeEntry, "/allTimeEntries");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_WHILE_DELETING_COMPONENT")));
            });
        };

        TimeTabViewModel.prototype.getNewEditTimeEntry = function () {
            var that = this;
            var timeModelData = new TimeConfirmation(null, this._requestHelper, this);

            timeModelData.ORDERID = this.getOrder().getOrderId();
            timeModelData.ACTIVITY = this.getOrder().OPERATION_ACTIVITY;
            timeModelData.PLANT = this.getOrder().PLANT;
            timeModelData.WORK_CNTR = this.getOrder().OPERATION_WK_CTR;
            timeModelData.KTEXT = this.getOrder().OPERATION_DESCRIPTION;
            timeModelData.UN_WORK = "MIN";
            timeModelData.CONF_NO = this.getOrder().OPERATION_CONF_NO;

            timeModelData.PERS_NO = this.getUserInformation().personnelNumber;
            timeModelData.USERID = this._globalViewModel.model.getProperty("/userName");

            // Set Start Time based on last time confirmation
            var aTimeConf = this.getProperty("/allTimeEntries");
            var oLastTimeConfDate = new Date(Math.max.apply(null, aTimeConf.map(function(oTimeConfirmation) {
                var oTimeConf = new TimeConfirmation(oTimeConfirmation, that._requestHelper, that);
                return moment(oTimeConf._END_DATE);
            })));

            if (!isNaN(oLastTimeConfDate)) {
                timeModelData.setStartEndTime(oLastTimeConfDate);
                timeModelData.setMinutes(timeModelData._DEFAULT_MINUTES);
            } else {
                var oDate = new Date();
                // Default to today at 7 am
                oDate.setHours(7, 0, 0, 0);
                timeModelData.setStartEndTime(oDate);
                timeModelData.setEndTime(oDate);
            }

            return timeModelData;
        };
        TimeTabViewModel.prototype.getMergedTimeEntry = function (timeEntry, newTimeEntry) {
            // map data 
             newTimeEntry.ACT_WORK = timeEntry.ACT_WORK ;
             newTimeEntry.B_EXP_FLAG = timeEntry.B_EXP_FLAG ;
             newTimeEntry.B_VALUE_ID = timeEntry.B_VALUE_ID ;
             newTimeEntry.END_DATE = timeEntry.END_DATE ;
             newTimeEntry.END_TIME = timeEntry["END_DATE"].substring(0,10) + timeEntry["END_TIME"].substring(10)  + "";
             newTimeEntry.FORMATTED_END_TIME = timeEntry.FORMATTED_END_TIME ;
             newTimeEntry.FORMATTED_START_TIME = timeEntry.FORMATTED_START_TIME ;
             newTimeEntry.K_EXP_FLAG = timeEntry.K_EXP_FLAG ;
             newTimeEntry.K_VALUE_ID = timeEntry.K_VALUE_ID ;
             newTimeEntry.START_DATE = timeEntry.START_DATE ;
             newTimeEntry.START_TIME = timeEntry["START_DATE"].substring(0,10) + timeEntry["START_TIME"].substring(10)  + "";
             newTimeEntry.T_VALUE_ID = timeEntry.T_VALUE_ID ;
             return newTimeEntry;

        };

        TimeTabViewModel.prototype.resetData = function () {
            this.setProperty("/timeEntries", []);
            this.setProperty("/allTimeEntries", []);
            this.setSelectedTimeEntries([]);
            this.setOrder(null);
        };

        TimeTabViewModel.prototype._handleKeyValueFields = function (timeEntry) {
            timeEntry.K_EXP_FLAG = timeEntry.K_VALUE_ID && timeEntry.K_VALUE_ID !== "0" ? "1" : "";
            timeEntry.T_EXP_FLAG = timeEntry.T_VALUE_ID && timeEntry.T_VALUE_ID !== "0" ? "2" : "";
            timeEntry.B_EXP_FLAG = timeEntry.B_VALUE_ID && timeEntry.B_VALUE_ID !== "0" ? "3" : "";

            if (!timeEntry.T_EXP_FLAG) {
                this.executeErrorCallback(new Error(this._component.getModel("i18n_core").getResourceBundle().getText("TIME_CONF_TRAVEL_COST_VALUE_STATE_ERROR")));
            }
        };

        TimeTabViewModel.prototype._hasReachedMaxDailyWork = function (timeEntry) {
            var that = this;
            var iMaxDailyWorkInMinutes = this._component.scenario.getMaxDailyWorkInMinutes();

            if (parseInt(timeEntry.ACT_WORK) > iMaxDailyWorkInMinutes) {
                return true;
            }

            var aAllTimeEntries = this.getProperty("/allTimeEntries");
            if (aAllTimeEntries.length === 0) {
                return false;
            }

            if (moment(timeEntry.START_TIME).date() === moment(timeEntry.END_TIME).date()) {
                var iSumDailyWorkInMinutes = 0;

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutes += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutes += parseInt(timeEntry.ACT_WORK);

                return iSumDailyWorkInMinutes > iMaxDailyWorkInMinutes;

            } else {
                var iSumDailyWorkInMinutesDayOne = 0;
                var iSumDailyWorkInMinutesDayTwo = 0;

                var iDurationDayOne = (60 * 24) - moment.duration(moment(timeEntry.START_TIME).diff(moment(timeEntry.START_DATE))).asMinutes();
                var iDuationDayTwo = moment.duration(moment(timeEntry.END_TIME).diff(moment(timeEntry.END_DATE))).asMinutes();

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutesDayOne += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                        iSumDailyWorkInMinutesDayTwo += that._calculateDailyWorkForDayTwo(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutesDayOne += iDurationDayOne;
                iSumDailyWorkInMinutesDayTwo += iDuationDayTwo;

                return iSumDailyWorkInMinutesDayOne > iMaxDailyWorkInMinutes || iSumDailyWorkInMinutesDayTwo > iMaxDailyWorkInMinutes;
            }

        };

        TimeTabViewModel.prototype._calculateDailyWorkForDayOne = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        TimeTabViewModel.prototype._calculateDailyWorkForDayTwo = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        TimeTabViewModel.prototype._checkDatesForSameMonthAndYear = function (timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate) {
            return moment(oTimeConfirmationStartDate).month() === moment(timeEntry.START_TIME).month() &&
                moment(oTimeConfirmationStartDate).year() === moment(timeEntry.START_TIME).year() &&
                moment(oTimeConfirmationEndDate).month() === moment(timeEntry.END_TIME).month() &&
                moment(oTimeConfirmationEndDate).year() === moment(timeEntry.END_TIME).year();
        };


        TimeTabViewModel.prototype.constructor = TimeTabViewModel;

        return TimeTabViewModel;
    });
