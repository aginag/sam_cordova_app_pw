sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/SAMChecklistPoint",
        "sap/m/MessageBox"
    ],

    function (JSONModel, ViewModel, Validator, Formatter, SAMChecklistPoint, MessageBox) {
        "use strict";

        // constructor
        function MinMaxTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this._NOTIFNO = "";
            this._CATALOG_TYPE = "S";
            this._CHECK_TYPE = "1";
            this._FUNCLOC_CLASS = "ANL_UMF";
            this._FUNCLOC_CHARACT = "WANDLER_GROESSE";
            this._EQUI_CLASS = "UMSP_ALLG";
            this._EQUI_CHARACT = "UMSP_SN";
            this._FUNCLOC_SUFFIX = "-020-UM";
            this._USER_TEXT = "Strom Aussenleiter";
            this._USER_TEXT_MAXIMALE_LEISTUNG = "Maximale Leistung";
            this._FUNCLOC_CHARACT_ANZ_WERT = "ANZ_WERT";
            this._CODE_GROUP = "PMC00005";
            this._CODE = "A001";

            this._ANZ_WERT = 1;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MinMaxTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        checklist: [],
                        view: {
                            busy: false,
                            errorMessages: [],
                            isEditable: false,
                            isRequired: true,
                            inputFieldsEditable: true
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            getOrderId: {
                value: function () {
                    return this._ORDERID;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            getNotificationNumber: {
                value: function () {
                    return this._NOTIFNO;
                }
            },

            setNotificationNumber: {
                value: function (sNotifNo) {
                    this._NOTIFNO = sNotifNo;
                }
            },

            setCheckList: {
                value: function (aChecklist) {
                    this.setProperty("/checklist", aChecklist);
                }
            },

            getCheckList: {
                value: function () {
                    return this.getProperty("/checklist");
                }
            },

            setCheckListNoMaxCapturing: {
                value: function (oCheckListPoint) {
                    this.setProperty("/checklistNoMaxCapturingPoint", oCheckListPoint);
                }
            },

            getCheckListNoMaxCapturing: {
                value: function () {
                    return this.getProperty("/checklistNoMaxCapturingPoint");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getDate: {
                value: function () {
                    var today_date = moment(new Date()).format("YYYY-MM-DD");
                    var today_time = " 00:00:00.000";

                    var today = today_date.concat(today_time);

                    return today;
                }
            },
        });

        MinMaxTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MinMaxTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {

                var aCheckList = data.MinMaxChecklist.map(function (oCheckPoint) {
                    var oCheckListPoint = new SAMChecklistPoint(oCheckPoint, that._requestHelper, that);

                    if (parseInt(oCheckListPoint.MRMIN) > 0 && parseInt(oCheckListPoint.MRMAX) > 0) {
                        oCheckListPoint.E_MRMIN = parseFloat(oCheckListPoint.MRMIN).toFixed(2);
                        oCheckListPoint.E_MRMAX = parseFloat(oCheckListPoint.MRMAX).toFixed(2);
                        oCheckListPoint.MINMAXUNIT = oCheckListPoint.MSEHT;
                    } else {
                        oCheckListPoint.E_MRMIN = "";
                        oCheckListPoint.E_MRMAX = "";
                        oCheckListPoint.MINMAXUNIT = "";
                    }
                    
                    for (var i = 0; i < data.MeasurementDocumentsLastReading.length; i++) {
                        var yesterday = moment(new Date(new Date().setDate(new Date().getDate()-1))).format("YYYY-MM-DD, HH:mm:ss");
                        var mdentry = data.MeasurementDocumentsLastReading[i];

                        if (mdentry.POINT === oCheckListPoint.MEAS_POINT && mdentry.ACTION_FLAG !== 'N' && mdentry.IDATE <= yesterday) {
                            oCheckListPoint.lastReading =  parseFloat(mdentry.RECDV).toFixed(0);
                            break;
                        }
                    }

                    oCheckListPoint.lastMeasPointReading = oCheckListPoint.ZRESULT_READING;
                    oCheckListPoint.isCheckpoint = true;
                    oCheckListPoint.isInactiveOrDeleted = !oCheckListPoint.FMP_MOBILE_ID || !!oCheckListPoint.FMP_INACT;

                    if (!oCheckListPoint.USER_TEXT.startsWith(that._USER_TEXT)) {
                        oCheckListPoint.isMaxLeistung = true;
                    }

                    return oCheckListPoint;
                });

                var aCheckListWOMaxLeistung = aCheckList.filter(function (oCheckListPoint) {
                    return !oCheckListPoint.isMaxLeistung;
                });

                var aCheckListWOMaxLeistungAndNotSynced = aCheckList.filter(function (oCheckListPoint) {
                    return !oCheckListPoint.isMaxLeistung && (oCheckListPoint.ACTION_FLAG === 'C' || (parseFloat(oCheckListPoint.ZRESULT_READING.replace(/,/, '.')) === 0 && oCheckListPoint.USER_FIELD === ""));
                });

                that.setProperty("/view/isEditable", aCheckListWOMaxLeistung.length === aCheckListWOMaxLeistungAndNotSynced.length);


                if (data.MinMaxCharacs && data.MinMaxCharacs.length > 0) {

                    var aCharacs = [];
                    var flagWarning = false;
                    data.MinMaxCharacs.forEach(function (oCharac) {
                        if (oCharac.CHARACT !== this._FUNCLOC_CHARACT_ANZ_WERT) {
                            aCharacs = aCharacs.concat({
                                USER_TEXT: oCharac.CHARACT_DESCR,
                                VALUE: oCharac.VALUE_NEUTRAL_FROM,
                                MSEHT: oCharac.MSEHT,
                                isCheckpoint: false
                            });
                        } else {
                            var dAnzWert = parseFloat(oCharac.VALUE_NEUTRAL_FROM.replace(/,/, '.'));

                            if ($.isNumeric(dAnzWert)) {
                                this._ANZ_WERT = dAnzWert;
                                if(dAnzWert > 0){
                            flagWarning = true;
                        }
                            }
                        }

                    }.bind(this));

                    if(!flagWarning && data.MinMaxChecklist.length > 0){
                        that.setProperty("/view/isEditable", false);
                        MessageBox.error(this._component.i18n.getText("WARNING_ANZ_WERT"));
                    }
                    aCheckList = aCheckList.concat(aCharacs);

                }

                if (data.MinMaxNoCapturing && data.MinMaxNoCapturing.length > 0) {
                    var oChecklistPoint = new SAMChecklistPoint(data.MinMaxNoCapturing[0], this._requestHelper, this);

                    oChecklistPoint.setUserStatuses(this.getCustomizingModelData("UserStatusMl").filter(function (oUserStatus) {
                        var sStatusProfile = data.MinMaxNoCapturing[0].XA_STAT_PROF;
                        return oUserStatus.STATUS_PROFILE === sStatusProfile;
                    }));

                    that.setProperty("/view/isRequired", oChecklistPoint.ZRESULT_CODE !== this._component.scenario.getOrderUserStatus().minMax.noReadingRequired.STATUS);
                    if ((!that.getProperty("/view/isRequired") && oChecklistPoint.ACTION_FLAG !== 'C') || !oChecklistPoint.userStatuses.length > 0) {
                        that.setProperty("/view/isEditable", false);
                    }

                    this.setCheckListNoMaxCapturing(oChecklistPoint);
                }

                if(data.MinMaxCharacs.length === 0){
                    that.setProperty("/view/isEditable", false);
                }

                this.setCheckList(aCheckList);

                this.setBusy(false);

                this._needsReload = false;
                if (successCb !== undefined) {
                    successCb();
                }
            }.bind(this);

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOAD_DATA_ORDER_DETAIL_MIN_MAX_TEXT")));
            };

            this.setBusy(true);
            this._requestHelper.getData(["MinMaxChecklist", "MeasurementDocumentsLastReading", "MinMaxCharacs", "MinMaxNoCapturing"], {
                orderId: this.getOrderId(),
                notifNo: this.getNotificationNumber(),
                language: this.getLanguage(),
                catalogType: this._CATALOG_TYPE,
                checkType: this._CHECK_TYPE,
                funcLocClass: this._FUNCLOC_CLASS,
                funcLocCharact: this._FUNCLOC_CHARACT,
                funcLocCharactAnz: this._FUNCLOC_CHARACT_ANZ_WERT,
                equiClass: this._EQUI_CLASS,
                equiCharact: this._EQUI_CHARACT,
                funcLoc: this.getOrder().FUNCT_LOC + this._FUNCLOC_SUFFIX,
                codeGroup: this._CODE_GROUP,
                code: this._CODE,
                point: this.TPLNR,
                mobile_id: this.mobile_id,
                date: this.getDate(),
            }, load_success, load_error);
        };

        MinMaxTabViewModel.prototype.resetData = function () {
            this._needsReload = true;
            this.setCheckList([]);
            this.setOrder(null);
            this.setNotificationNumber(null);
            this.setProperty("/view/isRequired", true);
            this.setCheckListNoMaxCapturing(null);
        };

        MinMaxTabViewModel.prototype.updateChecklistPoint = function(checklistPoint, successCb) {
            var that = this;
            if(parseInt(checklistPoint.ZRESULT_READING) == "0"){
                checklistPoint.USER_FIELD = checklistPoint.ZRESULT_READING;
            }else{
                checklistPoint.USER_FIELD = "";
            }

            if(checklistPoint.ZRESULT_READING === ""){
                checklistPoint.USER_FIELD = "";
            }

            try {
                if (checklistPoint.hasChanges()) {

                var oSuccess = function (){
                    if (successCb !== undefined) {
                        successCb();
                    }
                    that.refresh();
                };

                var onError = function (err) {
                    that.executeErrorCallback(err);
                };
                checklistPoint.update2(false, oSuccess, onError.bind(this, new Error(this._component.i18n.getText("ERROR_CHECKLIST_UPDATE"))));
               
                }else{
                    
                    if (successCb !== undefined) {
                        successCb();
                    }
                }
            } catch(err) {
                this.executeErrorCallback(err);
            }
        };

        MinMaxTabViewModel.prototype.updateAllChecklistPoint = function(checklistArr, successCb) {
            var that = this;
            var sqlStrings = [];
            try {
                
                var onError = function (err) {
                    that.executeErrorCallback(err);
                };
                checklistArr.forEach(function(checklistPoint){
                    var checkObj = checklistPoint.resetChecklist(true);
                    if(checkObj.values.length > 0){
                        sqlStrings.push(checkObj);
                    }
                });

                this._requestHelper.executeStatementsAndCommit(sqlStrings, successCb, onError.bind(this, this._component.i18n.getText("ERROR_CHECKLIST_UPDATE")));
            } catch(err) {
                this.executeErrorCallback(err);
            }
        };

        MinMaxTabViewModel.prototype.calculateMaximaleLeistung = function (successCb, page) {
            var that = this;
            var oMaximaleLeistungCheckpoint = this.getCheckList().find(function (oCheckpoint) {
                return oCheckpoint.USER_TEXT === this._USER_TEXT_MAXIMALE_LEISTUNG;
            }.bind(this));

            if (!oMaximaleLeistungCheckpoint) {
                successCb();
                return;
            }

            var checkP = this.getCheckList().filter(function(oCheckpoint){
                if(oCheckpoint.isCheckpoint && !oCheckpoint.isMaxLeistung){
                    if(parseFloat(oCheckpoint.ZRESULT_READING.replace(/,/, '.')) > 0){
                        return true;
                    }else if(oCheckpoint.USER_FIELD !== ""){
                        return true;
                    }
                    return false;
                }
            });

            var checkReset = this.getCheckList().filter(function(oCheckpoint){
                if(oCheckpoint.isCheckpoint && !oCheckpoint.isMaxLeistung){
                    if((oCheckpoint.ZRESULT_READING === "" || parseFloat(oCheckpoint.ZRESULT_READING.replace(/,/, '.')) == 0) && oCheckpoint.USER_FIELD === ""){
                        return true;
                    }
                    return false;
                }
                return false;
            });

            var iMaxReading = Math.max(...checkP.map(function(oCheckpoint){
                
                if(parseFloat(oCheckpoint.ZRESULT_READING.replace(/,/, '.')) > 0){
                    return oCheckpoint.ZRESULT_READING;
                }else if(oCheckpoint.USER_FIELD !== ""){
                    return 0;
                }
            }));

            // var iMaxReading = Math.max(...this.getCheckList().map(function (oCheckpoint) {
            //     return ( oCheckpoint.isCheckpoint && !oCheckpoint.isMaxLeistung ) ? oCheckpoint.ZRESULT_READING : 0;
            // }));

            var iReading = iMaxReading * Math.sqrt(3) * this._ANZ_WERT * 400 / 1000;

            oMaximaleLeistungCheckpoint.setResultReading(Math.round(iReading).toString());

            this.updateChecklistPoint(oMaximaleLeistungCheckpoint, function () {
                oMaximaleLeistungCheckpoint.lastMeasPointReading = oMaximaleLeistungCheckpoint.ZRESULT_READING;
                this.setUserStatusForMaxReadings(
                    parseFloat(oMaximaleLeistungCheckpoint.lastMeasPointReading) > 0,
                    this._component.scenario.getOrderUserStatus().minMax.readingCaptured.STATUS,
                    function(){
                        if(checkP.length === 0){
                            var checkListData = this.getCheckList();

                            var oQueue = [];
                            checkListData.forEach(function (checklistObject) {
                                if (checklistObject instanceof SAMChecklistPoint) {
                                    oQueue.push(checklistObject);
                                }
                            });
                            if(oQueue.length > 0){
                                this.updateAllChecklistPoint(oQueue, function(){
                                    this.loadData(successCb);
                                }.bind(this));
                            }else{
                                this.loadData(successCb);
                            }
                        }else 
                        if(checkReset.length > 0){
                            var sqlStr = [];
                            try {
                                checkReset.forEach(function(checklistPoint){
                                    var checkObj = checklistPoint.resetChecklist(true);
                                    if(checkObj.values.length > 0){
                                        sqlStr.push(checkObj);
                                    }
                                });
                
                                var onError = function (err) {
                                    that.executeErrorCallback(err);
                                };
                
                                this._requestHelper.executeStatementsAndCommit(sqlStr, function(){
                                    this.loadData(successCb);
                                }.bind(this), onError.bind(this, this._component.i18n.getText("ERROR_CHECKLIST_UPDATE")));
                            } catch(err) {
                                this.executeErrorCallback(err);
                            }

                        }else{
                            this.loadData(successCb);
                        }
                    }.bind(this));
            }.bind(this));

        };

        MinMaxTabViewModel.prototype.setUserStatusForMaxReadings = function (bReadingExists, sUserStatus, successCb) {
            var oCheckPoint = this.getCheckListNoMaxCapturing();
            var sUserStatusCode = '';

            if (bReadingExists) {
                var oUserStatus = oCheckPoint && oCheckPoint.userStatuses.find(function (oUserStatus) {
                    return oUserStatus.USER_STATUS === sUserStatus;
                }.bind(this));

                if (oUserStatus) {
                    sUserStatusCode = oUserStatus.USER_STATUS;
                } else {
                    var sErrText = this._component.i18n.getText(sUserStatus === this._component.scenario.getOrderUserStatus().minMax.readingCaptured.STATUS ?
                            "ERROR_MIN_MAX_READING_CAPTURED_NO_USER_STATUS_FOUND" :
                            "ERROR_MIN_MAX_NO_READING_REQUIRED_NO_USER_STATUS_FOUND");
                    this.executeErrorCallback(new Error(sErrText));
                    return;
                }
            }

            oCheckPoint.setResultCode(sUserStatusCode);
            this.updateChecklistPoint(oCheckPoint, successCb);

        };

        MinMaxTabViewModel.prototype.validate = function(completeCb) {
            var that = this;
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());


            var checklist = this.getCheckList();
            var measurementPoints = checklist.filter(function(checkPoint) {
                return checkPoint.isCheckpoint;
            });

            var measurementPointIndexes = measurementPoints.map(function(measPoint) {
                return checklist.indexOf(measPoint);
            });

            measurementPointIndexes.forEach(function(index) {
                var measPoint = checklist[index];
                measPoint.setValueState(sap.ui.core.ValueState.None);
                var readingIsNumber = !isNaN(measPoint.ZRESULT_READING);
                measPoint.setValueState(readingIsNumber ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);

                validator.check("checklist[" + index + "].ZRESULT_READING", that._component.i18n.getText("MIN_MAX_MEASUREMENT_POINT_READING_VALIDATION_MUST_BE_NUMBER")).custom(function(value) {
                    return !isNaN(value) || value === "";
                });
            });


            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            completeCb(errors ? false : true);
        };

        MinMaxTabViewModel.prototype.getValueStateModel = function() {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    ZRESULT_READING: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        MinMaxTabViewModel.prototype.resetValueStateModel = function() {
            if (!this.valueStateModel) {
                return;
            }

            this.valueStateModel.setProperty("/_MEAS_RESULT", sap.ui.core.ValueState.None);
            this.setProperty("/view/errorMessages", []);
        };

        MinMaxTabViewModel.prototype.constructor = MinMaxTabViewModel;

        return MinMaxTabViewModel;
    });
