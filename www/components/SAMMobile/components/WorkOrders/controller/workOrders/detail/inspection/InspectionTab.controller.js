sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/inspection/MeasuringDocumentHistoryViewModel",
    "sap/m/Dialog",
    'sap/m/Text',
    "sap/m/Button",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History"
], function (Controller,
             JSONModel,
             formatter,
             formatterComp,
             MeasuringDocumentHistoryViewModel,
             Dialog,
             Text,
             Button,
             MessageToast,
             History
) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.inspection.InspectionTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailInspection").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailInspection", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;
            this.measuringDocumentHistoryViewModel = new MeasuringDocumentHistoryViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            this.workOrderDetailViewModel = null;
            this.inspectionTabViewModel = null;

        },

        onRouteMatched: function (oEvent) {
            var oArgs = oEvent.getParameter("arguments");
            var orderId = oArgs.id;
            var persNo = oArgs.persNo;

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));


            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.inspectionTabViewModel = this.workOrderDetailViewModel.tabs.inspection.model;

                this.getView().setModel(this.inspectionTabViewModel, "inspectionTabViewModel");

            }

            if (this.workOrderDetailViewModel.needsReload()) {
                this.workOrderDetailViewModel.setOrderId(orderId, persNo);
                this.workOrderDetailViewModel.loadData(this.loadInspectionTabData.bind(this));
                return;
            }

            this.loadInspectionTabData();
        },

        loadInspectionTabData: function () {
            if (this.inspectionTabViewModel.needsReload()) {
                this.loadViewModelData();
                this.globalViewModel.setComponentHeaderText(this.workOrderDetailViewModel.getHeaderText());
            }
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailInspection", "refreshRoute", this.onRefreshRoute, this);
        },

        onNavBack: function () {
            var that = this;
            window.history.go(-1);
            setTimeout(function () {
                that.workOrderDetailViewModel.resetData();
            }, 200);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.inspectionTabViewModel.loadData(function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });
        },

        onDisplayMeasurementDocumentHistory: function (oEvent) {

            var that = this;
            var point;

            try {
                point = oEvent.getSource().getBindingContext("inspectionTabViewModel").getObject().EQUIPMENT_MEASUREMENT_POINT;
                this.measuringDocumentHistoryViewModel.loadData(point);
            } catch (err) {

            }

            var onCancelPressed = function () {
                var dialog = this.getParent();
                dialog.close();
            };

            this.pressDialog = null;

            if (!this.pressDialog) {

                this.pressDialog = sap.ui.xmlfragment("measuringDocHistory1", "SAMMobile.components.WorkOrders.view.workOrders.detail.inspection.MeasuringDocumentsHistoryPopup", this);

                this.pressDialog = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    title: "{i18n_core>TOMeasDocHistory}",
                    contentWidth: "80%",
                    contentHeight: "80%",
                    content: this.pressDialog,
                    endButton: new Button({
                        text: '{i18n_core>close}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });
                this.getView().addDependent(this.pressDialog);
            }

            this.measuringDocumentHistoryViewModel.loadData(point, function () {
                that.pressDialog.setModel(that.measuringDocumentHistoryViewModel, "documentModel");
                that.pressDialog.open();
            });

        },

        saveAllMeasurments: function (oEvent) {
            var that = this;
            var points = oEvent.getSource().getBindingContext("inspectionTabViewModel").getObject().points;
            points.forEach(function (point) {
                if (point.READING !== "") {
                    var document = that.measuringDocumentHistoryViewModel.getNewMeasurementDocument(point);

                    if (that.inspectionTabViewModel.validate(point)) {
                        that.inspectionTabViewModel.checkLimits(point);
                        that.measuringDocumentHistoryViewModel.insertNewMeasurementDocument(document, point);
                        this.inspectionTabViewModel.setIconColor(oEvent);
                        MessageToast.show("Inserted new Readings successfully");
                    }
                }
            });
        },

        saveMeasurement: function (oEvent) {
            var that = this;

            var point = oEvent.getSource().getBindingContext("inspectionTabViewModel").getObject();
            if (point.READING !== "") {
                var document = that.measuringDocumentHistoryViewModel.getNewMeasurementDocument(point);
                if (that.inspectionTabViewModel.validate(point)) {
                    that.inspectionTabViewModel.checkLimits(point);
                    that.measuringDocumentHistoryViewModel.insertNewMeasurementDocument(document, point);
                    this.inspectionTabViewModel.setIconColor(oEvent);
                    MessageToast.show("Inserted new Reading successfully");
                }
            }
        },

        expandAll: function (oEvent) {
            this.inspectionTabViewModel.setExpandAllPanels();
        },

        collapseAll: function (oEvent) {
            this.inspectionTabViewModel.setCollapseAllPanels();
        }
    });

});