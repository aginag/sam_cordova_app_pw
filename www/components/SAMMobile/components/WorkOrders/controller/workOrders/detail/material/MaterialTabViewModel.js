sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/OrderComponent",
        "SAMMobile/models/dataObjects/MaterialConfirmation"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, OrderComponent, MaterialConfirmation) {
        "use strict";

        // constructor
        function MaterialTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MaterialTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        components: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            getComponents: {
                value: function () {
                    this.getProperty("/components");
                }
            },

            getOrderSQLChangeString: {
                value: function () {
                    return this.getOrder().setChanged(true);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        MaterialTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MaterialTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
              
                that.setProperty("/components", data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
              
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOADING_DATA_FOR_MATERIAL_TAB")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOADING_DATA_FOR_MATERIAL_TAB_NO_ORDER")));
            }

            this.setBusy(true);
            this.getOrder().loadOrderComponents(load_success, load_error);
        };

        MaterialTabViewModel.prototype.loadMaterialConfirmationsData = function (successCb) {
            var that = this;

            var load_success = function (data) {
              
                that.setProperty("/materialConfirmations", data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
              
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOADING_DATA_FOR_MATERIAL_TAB")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOADING_DATA_FOR_MATERIAL_TAB_NO_ORDER")));
            }

            this.setBusy(true);
            this.getOrder().loadMaterialConfirmations(load_success, load_error);
        };


        MaterialTabViewModel.prototype.resetData = function () {
            this.setOrder(null);
            this._needsReload = true;
        };

        MaterialTabViewModel.prototype.addSelectedComponentsToList = function (editOrderComponentViewModel, successCb) {
            var that = this;

            var components = editOrderComponentViewModel.getProperty("/materialSave").map(function(listItem) {
                var orderComponentData = {
                    MATERIAL: listItem.MATERIAL,
                    // STGE_LOC: listItemData.STGE_LOC, //sap.ui.core.ValueState.None,
                    PLANT: that.getOrder().PLANT, //sap.ui.core.ValueState.None,
                    ENTRY_UOM: listItem.MEINS, //sap.ui.core.ValueState.None,
                    ENTRY_QNT: 1, //listItemData.ENTRY_QNT, //sap.ui.core.ValueState.None
                    REQUIREMENT_QUANTITY: '',
                    REQUIREMENT_QUANTITY_UNIT: listItem.MEINS,
                    ORDERID: that.getOrder().getOrderId(),
                    ACTIVITY: that.getOrder().OPERATION_ACTIVITY,
                    RESERV_NO: that.getOrder().getRandomCharUUID(10),
                    RES_ITEM: that.getOrder().getRandomCharUUID(4),
                    ACTION_FLAG: 'N',
                    ITEM_TEXT: listItem.MATL_DESC,
                    MATL_DESC: listItem.MATL_DESC,
                    MATERIAL_DISPLAY: listItem.MATERIAL,

                };

                var orderComponent = new OrderComponent(orderComponentData, that._requestHelper, that);
                return orderComponent;
            });

            components.forEach(function(component){
                that.insertToModelPath(component, "/components");
            });

            successCb();
        };

        MaterialTabViewModel.prototype.insertAddedComponent = function (oComponent, successCb) {
            try {
                oComponent.insert(false, function (mobileId) {
                    oComponent.MOBILE_ID = mobileId;
                    successCb();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("MATERIAL_CONFIRMATION_UPDATE_ERROR_TEXT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MaterialTabViewModel.prototype.insertNewComponent = function (editOrderComponentViewModel, successCb) {
            var that = this;
              
            var sqlStrings = [];

              // Patrick
              var components = editOrderComponentViewModel.getProperty("/materialSave").map(function(listItem) {
            var orderComponentData = {
                MATERIAL: listItem.MATERIAL,
//                STGE_LOC: listItemData.STGE_LOC, //sap.ui.core.ValueState.None,
                PLANT: that.getOrder().PLANT, //sap.ui.core.ValueState.None,
                ENTRY_UOM: listItem.MEINS, //sap.ui.core.ValueState.None,
                ENTRY_QNT: 1, //listItemData.ENTRY_QNT, //sap.ui.core.ValueState.None
                REQUIREMENT_QUANTITY: '',
                REQUIREMENT_QUANTITY_UNIT: listItem.MEINS,
                ORDERID: that.getOrder().getOrderId(),
                ACTIVITY: that.getOrder().OPERATION_ACTIVITY,
                RESERV_NO: that.getOrder().getRandomCharUUID(10),
                RES_ITEM: that.getOrder().getRandomCharUUID(4),
                ACTION_FLAG: 'N',
                ITEM_TEXT: listItem.MATL_DESC,
                MATL_DESC: listItem.MATL_DESC,
                MATERIAL_DISPLAY: listItem.MATERIAL,
            
            };
            
            var orderComponent = new OrderComponent(orderComponentData, that._requestHelper, that);
            return orderComponent;
            });

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
              
                // todo loop through components
                components.forEach(function(component){
                        that.insertToModelPath(component, "/components");
                });
                
                successCb();
            };

            var insertComponent = function(component) {
                return new Promise(function(resolve, reject) {
                                   component.insert(false, function(mobileId) {
                                                    component.MOBILE_ID = mobileId;
                                                    resolve();
                                                    }, function(err) {reject(err);})
                                   });
            };

            try {
                Promise.all(components.map(function(component) {
                                           return insertComponent(component);
                                           })).then(onSuccess.bind(this));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MaterialTabViewModel.prototype.insertNewOrderMaterialConfirmation = function (materialConfirmation, successCb) {
            var that = this;
            var sqlStrings = [];
            materialConfirmation.ENTRY_QNT = materialConfirmation.ENTRY_QNT.toString();

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var onSuccess = function (asd) {
                that.insertToModelPath(materialConfirmation, "/materialConfirmations");
                successCb();
            };

            try {
                materialConfirmation.insert(false, function (mobileId) {
                    materialConfirmation.MOBILE_ID = mobileId;
                    that._requestHelper.executeStatementsAndCommit([], onSuccess, onError.bind(this, new Error("Error while trying to update parent order")));
                }, onError.bind(this, new Error("Error while trying to insert new component")));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MaterialTabViewModel.prototype.updateMaterialConfirmation = function (materialConfirmation, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                materialConfirmation.ENTRY_QNT = materialConfirmation.ENTRY_QNT.toString();
                sqlStrings.push(materialConfirmation.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error("Error while trying to update component"));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MaterialTabViewModel.prototype.deleteMaterialConfirmation = function (materialConfirmation, successCb) {
            var that = this;
            var sqlStrings = [];
            sqlStrings = sqlStrings.concat(materialConfirmation.delete(true));

            var onSuccess = function () {
                that.removeFromModelPath(materialConfirmation, "/materialConfirmations");
                successCb();
            };

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error("Error while trying to delete component"));
            });
        };

        MaterialTabViewModel.prototype.getNewEditMaterialConfirmation = function () {
            var componentModelData = new MaterialConfirmation(null, this._requestHelper, this);

            componentModelData.ORDERID = this.getOrder().getOrderId();
            componentModelData.STGE_LOC = this.getUserInformation().storageLocation;
            componentModelData.PLANT = this.getUserInformation().plant;
            componentModelData.ACTIVITY = this.getOrder().OPERATION_ACTIVITY;

            // TODO find out how to determine them
            componentModelData.RESERV_NO = componentModelData.getRandomCharUUID(10);
            componentModelData.RES_ITEM = componentModelData.getRandomCharUUID(4);

            return componentModelData;
        };


        MaterialTabViewModel.prototype.updateComponent = function (component, successCb) {
            var that = this;
            var sqlStrings = [];

            var onSuccess = function (asdfasdf) {
                that.refresh();
                successCb();
            };

            try {
                component.REQUIREMENT_QUANTITY = component.REQUIREMENT_QUANTITY.replace(/,/, '.').toString();
                sqlStrings.push(component.update(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("MATERIAL_CONFIRMATION_UPDATE_ERROR_TEXT")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MaterialTabViewModel.prototype.deleteComponent = function (component, successCb) {

            if (component.MOBILE_ID) {
                var that = this;
                var sqlStrings = [];
                sqlStrings = sqlStrings.concat(component.delete(true));

                var onSuccess = function () {
                    that.removeFromModelPath(component, "/components");
                    successCb();
                };

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("MATERIAL_CONFIRMATION_DELETE_ERROR_TEXT")));
                });
            } else {
                this.removeFromModelPath(component, "/components");
                successCb();
            }

        };

        MaterialTabViewModel.prototype.getNewEditComponent = function () {
              
            var componentModelData = new OrderComponent(null, this._requestHelper, this);

            componentModelData.ORDERID = this.getOrder().getOrderId();
            componentModelData.STGE_LOC = this.getUserInformation().storageLocation;
            componentModelData.PLANT = this.getUserInformation().plant;
            componentModelData.ACTIVITY = this.getOrder().OPERATION_ACTIVITY;

            // TODO find out how to determine them
            componentModelData.RESERV_NO = componentModelData.getRandomCharUUID(10);
            componentModelData.RES_ITEM = componentModelData.getRandomCharUUID(4);

            return componentModelData;
        };

        MaterialTabViewModel.prototype.confirmComponent = function (component) {

            var componentModelData = new MaterialConfirmation(null, this._requestHelper, this);

            componentModelData.ORDERID = this.getOrder().getOrderId();
            componentModelData.STGE_LOC = this.getUserInformation().storageLocation;
            componentModelData.PLANT = this.getUserInformation().plant;
            componentModelData.ACTIVITY = this.getOrder().OPERATION_ACTIVITY;
            componentModelData.MATERIAL = component.MATERIAL;
            componentModelData.ITEM_TEXT = component.ITEM_TEXT;
            componentModelData.ENTRY_QNT = component.REQUIREMENT_QUANTITY;
            componentModelData.ENTRY_UOM = component.REQUIREMENT_QUANTITY_UNIT;
            componentModelData.PLANT = component.PLANT;
            componentModelData.STGE_LOC = component.STGE_LOC;

            // TODO find out how to determine them
            componentModelData.RESERV_NO = componentModelData.getRandomCharUUID(10);
            componentModelData.RES_ITEM = componentModelData.getRandomCharUUID(4);

            return componentModelData;
        };


        MaterialTabViewModel.prototype.validate = function(component, completeCb) {
            var that = this;
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());
            var iIndex = this.getProperty("/components").findIndex(function (oComponent) {
                return oComponent === component;
            });

            component.setValueState(sap.ui.core.ValueState.None);
            component.REQUIREMENT_QUANTITY = component.REQUIREMENT_QUANTITY.replace(/,/, '.');
            var readingIsNumber = !isNaN(component.REQUIREMENT_QUANTITY) && parseFloat(component.REQUIREMENT_QUANTITY) > 0;
            component.setValueState(readingIsNumber ? sap.ui.core.ValueState.None : sap.ui.core.ValueState.Error);

            validator.check("components[" + iIndex + "].REQUIREMENT_QUANTITY", that._component.i18n.getText("MATERIAL_CONFIRMATION_QUANTITY_VALIDATION_MUST_BE_NUMBER")).custom(function(value) {
                return !isNaN(value) && parseFloat(value) > 0;
            });


            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            completeCb(errors ? false : true);
        };

        MaterialTabViewModel.prototype.getValueStateModel = function() {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    REQUIREMENT_QUANTITY: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        MaterialTabViewModel.prototype.resetValueStateModel = function() {
            if (!this.valueStateModel) {
                return;
            }

            this.valueStateModel.setProperty("/REQUIREMENT_QUANTITY", sap.ui.core.ValueState.None);
            this.setProperty("/view/errorMessages", []);
        };


        MaterialTabViewModel.prototype.constructor = MaterialTabViewModel;

        return MaterialTabViewModel;
    });
