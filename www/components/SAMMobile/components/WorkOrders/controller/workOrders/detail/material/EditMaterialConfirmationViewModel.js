sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function EditMaterialConfirmationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditMaterialConfirmationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        materialConfirmation: null,
                        order: null,
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setMaterialConfirmation: {
                value: function (materialConfirmation) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = materialConfirmation.getCopy();
                    }

                    this.setProperty("/materialConfirmation", materialConfirmation);
                }
            },

            getMaterialConfirmation: {
                value: function () {
                    return this.getProperty("/materialConfirmation");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getMaterialConfirmation().setData(this._objectDataCopy);
                }
            },

            setOrder: {
                value: function (order) {
                    this.setProperty("/order", order);
                }
            },

            getOrder: {
                value: function () {
                    return this.getProperty("/order");
                }
            },

            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n").getResourceBundle().getText("EditMaterialConfirmation") : this._component.getModel("i18n").getResourceBundle().getText("NewMaterialConfirmation");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        EditMaterialConfirmationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditMaterialConfirmationViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("materialConfirmation.STGE_LOC", "Please select a Storage Location").isEmpty();
            validator.check("materialConfirmation.MATERIAL", "Please select a Material").isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
/*        EditMaterialConfirmationViewModel.prototype.displayStorageLocationDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oStorageLocationSelectDialog) {
                this._oStorageLocationSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.StorageLocSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oStorageLocationSelectDialog.display(oEvent, 'StorageLocation', function (storageLocation) {
                return storageLocation.ILART == that.getOrder().PMACTTYPE;
            });
        };*/

        EditMaterialConfirmationViewModel.prototype.displayStorageLocationDialog = function (oEvent, viewContext) {
            if (!this._oStorageLocationSelectDialog) {
                this._oStorageLocationSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.StorageLocSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            // TODO get correct activity type for current scenario
            this._oStorageLocationSelectDialog.display(oEvent, {
                tableName: 'MaterialStorageLocation',
                params: {
                    spras: this.getLanguage(),
                    plant: this.getProperty("/materialConfirmation/PLANT"),
                },
                actionName: "valueHelp"
            });
        };

        EditMaterialConfirmationViewModel.prototype.handleStorageLocationSelect = function (oEvent) {
            this._oStorageLocationSelectDialog.select(oEvent, 'STGE_LOC', [{
                'STGE_LOC': 'STGE_LOC'
            }, {
                'TEXT': 'TEXT'
            }], this, "/materialConfirmation");
        };

        EditMaterialConfirmationViewModel.prototype.handleStorageLocationSearch = function (oEvent) {
            this._oStorageLocationSelectDialog.search(oEvent, ["TEXT", "STGE_LOC"]);
        };

        EditMaterialConfirmationViewModel.prototype.displayPlantSelectDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oPlantSelectDialog) {
                this._oPlantSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.PlantSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            this._oPlantSelectDialog.display(oEvent, {
                tableName: 'PlantMaterial',
                params: {
                    spras: this.getLanguage()
                },
                actionName: "valueHelp"
            });
        };

        EditMaterialConfirmationViewModel.prototype.handlePlantSelect = function (oEvent) {
            this._oPlantSelectDialog.select(oEvent, 'PLANT', [{
                'PLANT': 'PLANT'
            }], this, "/materialConfirmation");
        };

        EditMaterialConfirmationViewModel.prototype.handlePlantSearch = function (oEvent) {
            this._oPlantSelectDialog.search(oEvent, ["PLANT"]);
        };

        EditMaterialConfirmationViewModel.prototype.displayMaterialDialog = function (oEvent, viewContext) {
            if (!this._oMaterialSelectDialog) {
                this._oMaterialSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.MaterialSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.DATABASE);
            }

            // TODO get correct activity type for current scenario
            this._oMaterialSelectDialog.display(oEvent, {
                tableName: 'ZMALFUNC_MATERIAL',//'InventoryMaterial',
                params: {
                    //spras: this.getLanguage(),
                },
                actionName: "valueHelp"
            });
        };

        EditMaterialConfirmationViewModel.prototype.handleMaterialSelect = function (oEvent) {
            this._oMaterialSelectDialog.select(oEvent, 'MATERIAL', [{
                'MATERIAL': 'MATERIAL',
                'ITEM_TEXT': 'MATL_DESC',
                'PLANT': 'WERKS',
                'ENTRY_UOM': 'ENTRY_UOM'
            }], this, "/materialConfirmation");
        };

        EditMaterialConfirmationViewModel.prototype.handleMaterialSearch = function (oEvent) {
            this._oMaterialSelectDialog.search(oEvent, ["MATL_DESC", "MATERIAL"]);
        };

        EditMaterialConfirmationViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    MATERIAL: sap.ui.core.ValueState.None,
                    STGE_LOC: sap.ui.core.ValueState.None,
                    PLANT: sap.ui.core.ValueState.None,
                    ENTRY_UOM: sap.ui.core.ValueState.None,
                    ENTRY_QNT: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditMaterialConfirmationViewModel.prototype.constructor = EditMaterialConfirmationViewModel;

        return EditMaterialConfirmationViewModel;
    });
