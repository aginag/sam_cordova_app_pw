sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog) {
        "use strict";

        // constructor
        function EditTimeEntryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this.valueStateModel = null;
            this._objectDataCopy = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        EditTimeEntryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        timeEntry: null,
                        colleagues: [],
                        view: {
                            busy: false,
                            edit: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setTimeEntry: {
                value: function (entry) {

                    if (this.getProperty("/view/edit")) {
                        this._objectDataCopy = entry.getCopy();

                    }

                    this.setProperty("/timeEntry", entry);
                }
            },

            getTimeEntry: {
                value: function () {
                    return this.getProperty("/timeEntry");
                }
            },

            setColleagues: {
                value: function (colleagues) {
                    this.setProperty("/colleagues", colleagues);
                }
            },

            getColleagues: {
                value: function () {
                    return this.getProperty("/colleagues");
                }
            },

            rollbackChanges: {
                value: function () {
                    this.getTimeEntry().setData(this._objectDataCopy);
                }
            },


            setEdit: {
                value: function (bEdit) {
                    this.setProperty("/view/edit", bEdit);
                }
            },

            getDialogTitle: {
                value: function () {
                    return this.getProperty("/view/edit") ? this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("EditTimeEntry") : this._component.getModel("i18n_core")
                        .getResourceBundle()
                        .getText("NewTimeEntry");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        EditTimeEntryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        EditTimeEntryViewModel.prototype.setMinutes = function (newMinutes) {
            this.getTimeEntry().setMinutes(newMinutes);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.setStartTime = function (newStartTime) {
            this.getTimeEntry().setStartTime(newStartTime);
            this.refresh();
        };

        EditTimeEntryViewModel.prototype.setEndTime = function (newEndTime) {
            this.getTimeEntry().setEndTime(newEndTime);
            this.refresh();
        };



        EditTimeEntryViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            var oMaxDate = moment();
            var oMinDate = moment().subtract(21, 'days');
            var oMaxDateForStopWatch = moment().add(5, 'minutes');


            validator.check("timeEntry.PERS_NO", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_PERS_NO")).isEmpty();
            validator.check("timeEntry.ACT_TYPE", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_ACT_TYPE")).isEmpty();

            validator.check("timeEntry.START_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_START_TIME")).isEmpty();
            // Start Date in Future
            validator.check("timeEntry.START_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_START_TIME_IN_FUTURE")).custom(function(value) {
                return !(moment(value).toDate() >= oMaxDateForStopWatch.toDate());
            });
            // Start Date more than 21 days in past
            validator.check("timeEntry.START_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_START_TIME_IN_PAST")).custom(function(value) {
                return !(moment(value).toDate() < oMinDate.toDate());
            });
            // Today more than 4 day of month and start date in last month
            validator.check("timeEntry.START_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_START_TIME_IN_LAST_MONTH")).custom(function(value) {
                return !(moment().date() > 4 && moment().month() > moment(value).month());
            });


            validator.check("timeEntry.END_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_END_TIME")).isEmpty();
            // Start Date in Future
            validator.check("timeEntry.END_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_END_TIME_IN_FUTURE")).custom(function(value) {
                return !(moment(value).toDate() >= oMaxDateForStopWatch.toDate());
            });
            // Start Date more than 21 days in past
            validator.check("timeEntry.END_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_END_TIME_IN_PAST")).custom(function(value) {
                return !(moment(value).toDate() < oMinDate.toDate());
            });
            // Today more than 4 day of month and start date in last month
            validator.check("timeEntry.END_TIME", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_END_TIME_IN_LAST_MONTH")).custom(function(value) {
                return !(moment().date() > 4 && moment().month() > moment(value).month());
            });


            validator.check("timeEntry.T_VALUE_ID", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_TRAVEL_KOST")).isEmpty();
            validator.check("timeEntry.ACT_WORK", this._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_VALIDATION_ERROR_DURATION")).custom(function(value) {
                return value && value > 0;
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Select Dialogs
        EditTimeEntryViewModel.prototype.displayTeamMemberSelectDialog = function (oEvent, viewContext) {
            if (!this._oTeamMemberSelectDialog) {
                this._oTeamMemberSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.TeamMemberSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oTeamMemberSelectDialog.display(oEvent, '', null, new JSONModel(this.getColleagues()));
        };

        EditTimeEntryViewModel.prototype.handleTeamMemberSelect = function (oEvent) {
            this._oTeamMemberSelectDialog.select(oEvent, 'persNo', [{
                'PERS_NO': 'persNo'
            }], this, "/timeEntry");
        };

        EditTimeEntryViewModel.prototype.handleTeamMemberSearch = function (oEvent) {
            this._oTeamMemberSelectDialog.search(oEvent, ["firstName", "lastName", "persNo"]);
        };

        EditTimeEntryViewModel.prototype.displayActTypeDialog = function (oEvent, viewContext) {
            var that = this;
            if (!this._oActTypeSelectDialog) {
                this._oActTypeSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.ActivityTypeSelectDialog", this._requestHelper, viewContext, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            // TODO add correct filtering
            this._oActTypeSelectDialog.display(oEvent, 'ActivityTypeMl');
        };

        EditTimeEntryViewModel.prototype.handleActTypeSelect = function (oEvent) {
            this._oActTypeSelectDialog.select(oEvent, 'LSTAR', [{
                'ACT_TYPE': 'LSTAR'
            }], this, "/timeEntry");


        };

        EditTimeEntryViewModel.prototype.handleActTypeSearch = function (oEvent) {
            this._oActTypeSelectDialog.search(oEvent, ["LSTAR", "KTEXT"]);
        };

        EditTimeEntryViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    PERS_NO: sap.ui.core.ValueState.None,
                    START_DATE: sap.ui.core.ValueState.None,
                    START_TIME: sap.ui.core.ValueState.None,
                    END_DATE: sap.ui.core.ValueState.None,
                    END_TIME: sap.ui.core.ValueState.None,
                    T_VALUE_ID: sap.ui.core.ValueState.None,
                    AWART: sap.ui.core.ValueState.None,
                    ACT_WORK: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        EditTimeEntryViewModel.prototype.constructor = EditTimeEntryViewModel;

        return EditTimeEntryViewModel;
    });
