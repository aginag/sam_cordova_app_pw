sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "sap/ui/core/routing/History"
], function(Controller, MessageToast, History) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.operations.OperationsTab", {

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailOperations").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailOperations", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = null;
            this.operationsTabViewModel = null;
        },

        onRouteMatched: function(oEvent) {
            var that = this;
            var oArgs = oEvent.getParameter("arguments");
            var orderId = oArgs.id;
            var operId = oArgs.operId;
            var persNo = oArgs.persNo;

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.operationsTabViewModel = this.workOrderDetailViewModel.tabs.operations.model;

                this.getView().setModel(this.operationsTabViewModel, "operationsTabViewModel");
            }

            if (this.workOrderDetailViewModel.needsReload()) {
                this.workOrderDetailViewModel.setOrderId(orderId, operId, persNo);
                this.workOrderDetailViewModel.loadData(this.loadViewModelData.bind(this));
                this.workOrderDetailViewModel.setSelectedTabKey("operations");
                return;
            }

            this.loadViewModelData();
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailOperations", "refreshRoute", this.onRefreshRoute, this);
        },

        onNavBack: function () {
            var that = this;
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();
            if (sPreviousHash.includes("mapsMarker") || sPreviousHash.includes("calendar")) {
                window.history.go(-1);
            }else if(sPreviousHash.includes("operations")){
                if(this.component.routeNumber == 0){
                    this.oRouter.navTo("workOrderOperations");
                }else{
                this.component.routeNumber--;
                var oArr = sPreviousHash.split("/");

                this.workOrderDetailViewModel.resetData();
                this.oRouter.navTo("workOrderDetailRoute", {
                    id:  oArr[1],
                    operId: oArr[2],
                    persNo: oArr[3]
                });
            }
            } else {
                if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                    this.oRouter.navTo("workOrders");
                } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                    this.oRouter.navTo("workOrderOperations");
                }
            }
            if(!sPreviousHash.includes("operations")){
                setTimeout(function () {
                    that.workOrderDetailViewModel.resetData();
                }, 200);
            }
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        onNavToOperationPressed: function(oEvent) {
            var operation = oEvent.getSource().getBindingContext("operationsTabViewModel").getObject();
            var orderId = operation.ORDERID;
            var operId = operation.ACTIVITY;
            var persNo = this.operationsTabViewModel.getUserInformation().personnelNumber ? this.operationsTabViewModel.getUserInformation().personnelNumber : '00000000';

            this.component.routeNumber++;
            this.workOrderDetailViewModel.resetData();
            this.oRouter.navTo("workOrderDetailRoute", {
                id: orderId,
                operId: operId,
                persNo: persNo
            });
        },

        loadViewModelData: function(bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.operationsTabViewModel.loadData(function() {
                that.globalViewModel.setComponentHeaderText(that.workOrderDetailViewModel.getHeaderText());
                that.workOrderDetailViewModel.loadAllCounters();
            });
        }
    });

});