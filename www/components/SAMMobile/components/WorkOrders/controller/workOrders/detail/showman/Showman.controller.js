sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "SAMMobile/helpers/fileHelper",
    "SAMMobile/helpers/FileSystem",
    "sap/m/MessageBox",
    "SAMMobile/controller/common/MediaAttachments.controller",
], function (Controller, MessageToast, fileHelper, FileSystem, MessageBox, MediaAttachmentsController) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.showman.Showman", {
        fileHelper: fileHelper,
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailShowman").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailRoute", "refreshRoute", this.onRefreshRoute, this);

            this.workOrderDetailViewModel = null;
            this.showmanTabViewModel = null;

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());

            this.fileSystem = new FileSystem();

        },

        onRouteMatched: function (oEvent) {
            var oArgs = oEvent.getParameter("arguments");

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.showmanTabViewModel = this.workOrderDetailViewModel.tabs.showman.model;

                this.getView().setModel(this.showmanTabViewModel, "showmanTabViewModel");
            }

            this.showmanTabViewModel.setOrderId(oArgs.id);

            this.loadViewModelData();
            this._tryCreateUserAttachmentsControl();
            if(this.showmanTabViewModel.getValueStateModel()){
                this.showmanTabViewModel.resetValueStateModel();
            }
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailRoute", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            this.showmanTabViewModel.loadData(function () {
            });
        },

        setStorageCheckboxValue: function (value, currentShowmanObject) {
            currentShowmanObject.showmanProcesses.forEach(function (showmanData) {
                if (currentShowmanObject.ORDERNO === showmanData.ORDERNO && currentShowmanObject.OPERATION === showmanData.OPERATION) {
                    showmanData.disableSelected = value;
                }
            });
            currentShowmanObject._saveLocalStorage();
        },

        onInstallationSelectionChange: function (oEvent) {
            var that = this;
            
            var showmanObject = this.showmanTabViewModel.getShowmanEntry();
            if (showmanObject.installationSelected) {
                showmanObject.installationOperation = showmanObject.OPERATION;
                this.showmanTabViewModel.setInstallationDate(showmanObject.getDateNow().dateTime);
            } else {
                showmanObject.installationOperation = "";
                showmanObject.INST_DAT = "";
                showmanObject.CONV_FACTOR = "";
                showmanObject.START_CNT_READ = "";
                showmanObject.CUST_PROPERTY_COUNT = "";
                if(showmanObject.removalOperation == showmanObject.OPERATION){
                    this.showmanTabViewModel._updateShowman(showmanObject);
                }else{
                    this.showmanTabViewModel.deleteShowmanEntry();
                }
            }

            this.showmanTabViewModel.refresh();
        },

        onRemovalSelectionChange: function (oEvent) {
            var that = this;
            
            var showmanObject = this.showmanTabViewModel.getShowmanEntry();

            if (showmanObject.removalSelected) {
                showmanObject.removalOperation = showmanObject.OPERATION;
                this.showmanTabViewModel.setRemovalDate(showmanObject.getDateNow().dateTime);
            } else {
                showmanObject.removalOperation = "";
                showmanObject.REMOVE_DAT = "";
                showmanObject.END_CNT_READ = "";
                
                if(showmanObject.installationOperation == showmanObject.OPERATION){
                    this.showmanTabViewModel._updateShowman(showmanObject);
                }else{
                    this.showmanTabViewModel.deleteShowmanEntry();
                }
            }

            this.showmanTabViewModel.refresh();
        },

        onDisableSelection: function (oEvent) {
            
            var showmanObject = this.showmanTabViewModel.getShowmanEntry();

            if (showmanObject.disableSelected) {
                this.setStorageCheckboxValue(showmanObject.disableSelected, showmanObject);

                showmanObject.installationEditable = false;
                showmanObject.removalEditable = false;

                if(showmanObject.installationOperation && showmanObject.installationOperation == showmanObject.OPERATION && showmanObject.installationSelected
                    && showmanObject.removalOperation && showmanObject.removalOperation == showmanObject.OPERATION && showmanObject.removalSelected){
                        this.clearShowmanFields(showmanObject);
                        this.showmanTabViewModel._deleteShowman(showmanObject, function(){
                            this.showmanTabViewModel.loadData();
                        }.bind(this));
                }else if(showmanObject.installationOperation && showmanObject.installationOperation == showmanObject.OPERATION && showmanObject.installationSelected){
                    if(showmanObject.removalOperation == showmanObject.OPERATION){
                        showmanObject.installationSelected = false;
                        showmanObject.installationOperation = "";
                        showmanObject.INST_DAT = "";
                        showmanObject.CONV_FACTOR = "";
                        showmanObject.START_CNT_READ = "";
                        showmanObject.CUST_PROPERTY_COUNT = "";
                        this.showmanTabViewModel._updateShowman(showmanObject);
                    }else{
                        this.clearShowmanFields(showmanObject);
                        this.showmanTabViewModel._deleteShowman(showmanObject, function(){
                            this.showmanTabViewModel.loadData();
                        }.bind(this));
                    }
                }else if(showmanObject.removalOperation && showmanObject.removalOperation == showmanObject.OPERATION && showmanObject.removalSelected){
                    if(showmanObject.installationOperation == showmanObject.OPERATION){
                        showmanObject.removalSelected = false;
                        showmanObject.removalOperation = "";
                        showmanObject.REMOVE_DAT = "";
                        showmanObject.END_CNT_READ = "";
                        this.showmanTabViewModel._updateShowman(showmanObject);
                    }else{
                        this.clearShowmanFields(showmanObject);
                        this.showmanTabViewModel._deleteShowman(showmanObject, function(){
                            this.showmanTabViewModel.loadData();
                        }.bind(this));
                    }

                }
                
            }else{
                this.setStorageCheckboxValue(showmanObject.disableSelected, showmanObject);
                this.showmanTabViewModel.loadData();
            }

            this.showmanTabViewModel.refresh();
        },

        clearShowmanFields: function (showmanObject) {
            showmanObject.installationSelected = false;
            showmanObject.installationOperation = "";
            showmanObject.removalSelected = false;
            showmanObject.removalOperation = "";
            showmanObject.INST_DAT = "";
            showmanObject.REMOVE_DAT = "";
            showmanObject.END_CNT_READ = "";
            showmanObject.CONV_FACTOR = "";
            showmanObject.START_CNT_READ = "";
            showmanObject.CUST_PROPERTY_COUNT = "";
            this.showmanTabViewModel.refresh();
        },

        onInstallationDateChanged: function (oEvent) {
            this.showmanTabViewModel.setInstallationDate(oEvent.getParameter("value"));
        },

        onRemovalDateChanged: function (oEvent) {
            this.showmanTabViewModel.setRemovalDate(oEvent.getParameter("value"));
        },

        onCounterStartStatusChanged: function (oEvent) {
            this.showmanTabViewModel.setCounterStart(oEvent.getParameter("value"));
        },

        onConverterRatioChanged: function (oEvent) {
            this.showmanTabViewModel.setConverterRatio(oEvent.getParameter("value"));
        },

        onCounterReadingChanged: function (oEvent) {
            this.showmanTabViewModel.setCounterEnd(oEvent.getParameter("value"));
        },


        _tryCreateUserAttachmentsControl: function () {
            var me = this;
            if (!me.userAttachmentControl) {
                me.userAttachmentControl = true;

                me.mediaAttachmentsController.onNewAttachment.successCb = function (bCtx, fileEntry, fileSize, type) {
                    var attachmentList = bCtx.getObject();
                    var fileName = fileEntry.name.split(".")[0];
                    var fileExt = getFileExtension(fileEntry.name);

                    var attachmentObj = me.showmanTabViewModel.getNewAttachmentObject(fileName, fileExt, fileSize.toString(), getFolderPath(fileEntry.nativeURL));
                    me.showmanTabViewModel.saveUserAttachment(attachmentObj, function () {
                        me.showmanTabViewModel.refresh(true);
                    });
                };

                me.mediaAttachmentsController.onEditAttachment.successCb = function (bCtx, fileEntry) {
                    var attachment = bCtx.getObject();
                    attachment.setFilename(getFileName(fileEntry.name));

                    me.showmanTabViewModel.updateUserAttachment(attachment);
                };

                me.mediaAttachmentsController.onDeleteAttachment.successCb = function (bCtx) {
                    var attachment = bCtx.getObject();

                    //in case file could not be deleted on file system we remove it from DB
                    me.showmanTabViewModel.deleteUserAttachment(attachment);
                };

                me.mediaAttachmentsController.formatAttachmentTypes = function (fileExt) {
                    return me.formatter._formatAttachmentType(fileExt);
                };
            }
        },

        _onCapturePhotoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.showmanTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("showmanTabViewModel");
            var that = this;
            if (!this.showmanTabViewModel.canAttachFiles()) {
                MessageBox.warning(that.showmanTabViewModel._component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCapturePhotoPress(null, bContext, null, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureVideoPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.showmanTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("showmanTabViewModel");
            var that = this;
            if (!this.showmanTabViewModel.canAttachFiles()) {
                MessageBox.warning(that.showmanTabViewModel._component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureVideoPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onCaptureAudioPress: function (oEvent) {
            var bContext = new sap.ui.model.Context(this.showmanTabViewModel, '/attachments/'); //oEvent.getSource().getBindingContext("showmanTabViewModel");
            var that = this;
            if (!this.showmanTabViewModel.canAttachFiles()) {
                MessageBox.warning(that.showmanTabViewModel._component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.mediaAttachmentsController._onCaptureAudioPress(null, bContext, function () {
                that.workOrderDetailViewModel.loadAllCounters();
            });

        },

        _onOpenAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getSelectedItem().getBindingContext("showmanTabViewModel");
            var attachmentObject = bContext.getObject();
            if (attachmentObject.USER_FIELD) {
                this.mediaAttachmentsController.redirectToCorrectUrl(attachmentObject.USER_FIELD);
            } else {
                this.mediaAttachmentsController._onOpenAttachment(null, bContext);
            }
        },

        _onEditAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("showmanTabViewModel");
            this.mediaAttachmentsController._onEditAttachment(null, bContext);
        },

        _onDeleteAttachment: function (oEvent) {
            var bContext = oEvent.getSource().getBindingContext("showmanTabViewModel");
            var that = this;
            MessageBox.confirm(that.showmanTabViewModel._component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_MESSAGE_TEXT"), {
                icon: MessageBox.Icon.INFORMATION,
                title: that.showmanTabViewModel._component.getModel("i18n").getResourceBundle().getText("ATTACHMENT_DELETION_TITLE_TEXT"),
                actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                onClose: function (oAction) {
                    if (oAction === "YES") {
                        this.mediaAttachmentsController._onDeleteAttachment(null, bContext, function () {
                            that.workOrderDetailViewModel.loadAllCounters();
                            MessageToast.show(that.showmanTabViewModel._component.i18n.getText("ATTACHMENT_DELETION_SUCCESS_TEXT"));
                        });
                    }
                }.bind(this)
            });
        },

        onUserAttachments: function (oEvent) {
            var bCtx = oEvent.getSource().getBindingContext();
            var taskObject = bCtx.getObject();
            var me = this;
            var context = new Context(me.checkListTaskModel, bCtx.sPath);
            me.userAttachmentControl.setBindingContext(context);
            me.userAttachmentControl.setModel(me.checkListTaskModel);

            var userAttTable = sap.ui.getCore().byId("checklistUserAttachmentsTable");
            userAttTable.removeSelections(true);

            me.userAttachmentControl.open();
        },

        handleTypeMissmatch: function (oEvent) {
            var aFileTypes = ["txt", "pdf", "jpeg", "png", "doc", "docx", "xls", "xlsx", "jpg"];
            jQuery.each(aFileTypes, function (key, value) {
                aFileTypes[key] = "*." + value;
            });
            MessageBox.warning(this.oBundle.getText("fileTypeNotSupported"));
        },

        onFileSelection: function (oEvent) {
            var that = this;
            var binaryString, fileBlob;
            var bContext = oEvent.getSource().getBindingContext();
            var file = oEvent.getParameter("files")[0];

            if (!this.showmanTabViewModel.canAttachFiles()) {
                MessageBox.warning(that.showmanTabViewModel._component.i18n.getText("attachmentsNoNotification"));
                return;
            }

            this.getOwnerComponent().setBusyOn();
            var reader = new FileReader();
            reader.onload = function (readerEvt) {
                binaryString = readerEvt.target.result.split(",")[1];
                fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);
                // On windows, we have to get the fileExt based on the file name
                // see https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file/index.html section "readAsDataURL"
                if (sap.ui.Device.os.name === sap.ui.Device.os.OS.WINDOWS) {
                    var fileExt = file.name.split(".").pop();
                } else {
                    var fileExt = that.fileHelper.blobExtToFileExtension(fileBlob);
                }

                var completeFileName = file.name;
                var indexOfPoint = completeFileName.lastIndexOf('.');
                var fileName = completeFileName.slice(0, indexOfPoint);
                if (fileName.length > 50) {
                    fileName = new Date().getTime().toString();
                }

                that.onWriteToFileSystem(fileName, fileExt, fileBlob, bContext);
            };
            reader.onerror = function (error) {
                MessageBox.warning(that.oBundle.getText("attachmentReadError"));
                that.getOwnerComponent().setBusyOff();
            };
            reader.readAsDataURL(file);
            //	    var path = (window.URL || window.webkitURL).createObjectURL(file);
            oEvent.getSource().clear();
        },

        onWriteToFileSystem: function (fileName, fileExt, fileBlob, bContext) {
            var that = this;

            var onWriteSuccess = function (filePath, e) {
                var iBlobSize = fileBlob.size > 0 ? fileBlob.size : e.target.length;
                var attachmentObj = that.showmanTabViewModel.getNewAttachmentObject(fileName, fileExt, iBlobSize.toString(), getFolderPath(filePath));
                that.addLocalAttachment(attachmentObj, bContext);
                that.getOwnerComponent().setBusyOff();
                that.workOrderDetailViewModel.loadAllCounters();
            };

            var onWriteError = function (msg) {
                that.getOwnerComponent().setBusyOff();
                MessageToast.show(msg);
            };

            this.fileSystem.write(fileName + "." + fileExt, fileBlob, onWriteSuccess, onWriteError);
        },

        addLocalAttachment: function (mediaObject, bContext) {
            var that = this;
            if (mediaObject instanceof Array) {
                for (var i = 0; i < mediaObject.length; i++) {
                    var attachmentObj = mediaObject[i];

                    that.showmanTabViewModel.saveUserAttachment(attachmentObj, function () {
                        that.showmanTabViewModel.refresh(true);
                    });
                }
            } else {
                that.showmanTabViewModel.saveUserAttachment(mediaObject, function () {
                    that.showmanTabViewModel.refresh(true);
                });
            }
        },

        onCheckboxCounterChanged: function (oEvent) {
            this.showmanTabViewModel.setCheckboxCounter(oEvent.getParameter("selected"));
        },

    });

});
