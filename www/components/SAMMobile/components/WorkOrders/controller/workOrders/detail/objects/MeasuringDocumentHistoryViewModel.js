sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/models/dataObjects/MeasurementDocument",
        "SAMMobile/models/dataObjects/MeasdocLongtexts",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/EditMeasurementPointViewModel"],

    function (JSONModel, ViewModel, Validator, MeasurementDocument, MeasdocLongtext, EditMeasurementPointViewModel) {
        "use strict";

        // constructor
        function MeasuringDocumentHistoryViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.component = component;
            this._OBJECTID = "";
            this._OBJECT = null;
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MeasuringDocumentHistoryViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        points: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setObjectId: {
                value: function (objectId) {
                    this._OBJECTID = objectId;
                }
            },

            setObject: {
                value: function (object) {
                    this._OBJECT = object;
                }
            },

            getObject: {
                value: function () {
                    return this._OBJECT;
                }
            },

            setDocuments: {
                value: function (documents) {
                    this.setProperty("/documents", documents);
                }
            },

            getDocument: {
                value: function () {
                    return this.getProperty("/document");
                }
            },

            setDocument: {
                value: function (document) {
                    this.setProperty("/document", document);
                }
            },

            getMeasureSQLChangeString: {
                value: function () {
                    return this.getDocument().setChanged(true);
                }
            },

            getDocuments: {
                value: function () {
                    return this.getProperty("/documents");
                }
            },

            setDocumentLongtexts: {
                value: function (longtexts) {
                    this.setProperty("/measdocLongtexts", longtexts);
                }
            },

            getDocumentLongtexts: {
                value: function () {
                    return this.getProperty("/measdocLongtexts");
                }
            },

            setDocumentLongtext: {
                value: function (longtext) {
                    this.setProperty("/measdocLongtext", longtext);
                }
            },

            getDocumentLongtext: {
                value: function () {
                    return this.getProperty("/measdocLongtext");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        MeasuringDocumentHistoryViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MeasuringDocumentHistoryViewModel.prototype.setModelData = function (data, point) {

            var that = this;
            var newData = [];
            var measDocData = [];

            var measDocuments = data.map(function (measDoc) {
                return new MeasurementDocument(measDoc, that._requestHelper, that);
            });

            measDocuments.forEach(function (measurementDocument) {
                measurementDocument.LONGTEXT = "";
                measurementDocument.INSPECTIONDATE = moment(measurementDocument.IDATE).format("YYYY-MM-DD") + " " + moment(measurementDocument.ITIME).format("HH:mm:ss");

                if (point.CODGR !== '') {
                    measurementDocument.INPUT = measurementDocument.VLCOD;
                } else {
                    measurementDocument.INPUT = parseFloat(measurementDocument.RECDV).toFixed(2);
                }

                if(moment(measurementDocument.INSPECTIONDATE).isValid()){
                    measDocData.push(measurementDocument);
                }

            });

            measDocData.sort(function (a, b) {
                var dateA = new Date(a.INSPECTIONDATE), dateB = new Date(b.INSPECTIONDATE);
                return dateA - dateB //sort by date ascending
            });

            this.setDocuments(measDocData);

            /*measDocuments.forEach(function (measurementDocument) {

                that.loadMeasurementLongtexts(measurementDocument).then(function (measdocLongtexts) {
                    var concatLongtext = "";
                    try {
                        measdocLongtexts.forEach(function (longtext) {
                            concatLongtext = concatLongtext.concat(longtext.TEXT_LINE + " ");
                        });
                    } catch (error) {

                    }
                    measurementDocument.LONGTEXT = concatLongtext;
                    if (moment(measurementDocument.ITIME).isValid()) {
                        newData.push(measurementDocument);
                    }
                    that.setDocuments(newData);
                }).catch(function () {
                });
            }); */
        };

        MeasuringDocumentHistoryViewModel.prototype.loadMeasurementLongtexts = function (measDoc) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadMeasdocLongtext(measDoc.MDOCM, resolve, reject);
            });
        };


        MeasuringDocumentHistoryViewModel.prototype.loadData = function (point, successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data, point);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(that.getDocuments());
                    that.refresh();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadDataMeasDocs")));
            };

            this.setBusy(true);
            this._requestHelper.getData("MeasurementDocuments", {
                point: point.MEASUREMENT_POINT
            }, load_success, load_error, true);
        };

        MeasuringDocumentHistoryViewModel.prototype.getNewMeasurementDocument = function (point) {

            var newMeasurementDocumentData = new MeasurementDocument(null, this._requestHelper, this);

            newMeasurementDocumentData.POINT = point.MEASUREMENT_POINT;
            newMeasurementDocumentData.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
            newMeasurementDocumentData.ITIME = moment(new Date()).format("HH:mm:ss");
            newMeasurementDocumentData.ERNAM = this.getUserInformation().userName;
            newMeasurementDocumentData.ACTION_FLAG = "N";

            return newMeasurementDocumentData;
        };

        MeasuringDocumentHistoryViewModel.prototype.insertNewMeasurementDocument = function (document, point, codeCatalog, successCb) {

            var that = this;
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var catalogType;

            try {
                catalogType = codeCatalog[0].CATALOG_TYPE;
            } catch (error) {
                that.executeErrorCallback(new Error(that.component.i18n.getText("errorCatalogType")));
            }

            if (point.CODGR !== '') {
                document.CODCT = catalogType;
                document.CODGR = point.CODGR;
                document.VLCOD = point.READING;
                document.CVERS = "000001";
                document.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
                document.READG_CHAR = point.CODE_TEXT;
            } else {
                document.READG = point.READING;
                document.RECDV = point.READING;
                document.RECDU = point.MRNGU;
                document.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
                document.READG_CHAR = point.CODE_TEXT;
                document.RECDVI = "X";
            }

            var onSuccess = function (asd) {
                //successCb();
            };

            try {
                document.insert(false, function (mobileId) {
                    document.MOBILE_ID = mobileId;
                    successCb();
                }, onError.bind(this, new Error(this.component.i18n.getText("ERROR_INSERT_MEAS_DOC"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MeasuringDocumentHistoryViewModel.prototype.updateMeasurementDocument = function (document, point, codeCatalog, successCb) {

            var that = this;
            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var catalogType;

            try {
                catalogType = codeCatalog[0].CATALOG_TYPE;
            } catch (error) {

            }

            if (point.CODGR !== '') {
                document.CODCT = catalogType;
                document.CODGR = point.CODGR;
                document.VLCOD = point.READING;
                document.CVERS = "000001"
                document.CODETEXT = point.CODE_TEXT;
                document.IDATE = moment(new Date()).format("YYYY-MM-DD, HH:mm:ss");
                document.MDTXT = point.LONGTEXT;
                document.READG_CHAR = point.CODE_TEXT;
                document.RECDV = point.CODE_TEXT;
                document.RECDV_CHAR = point.CODE_TEXT;
            } else {
                document.READG = point.READING;
                document.RECDV = point.READING;
                document.RECDU = point.MRNGU;
                document.RECDVI = "X";
            }

            var onSuccess = function (asd) {
                //successCb();
            };

            try {
                document.update(false, function (mobileId) {
                    document.MOBILE_ID = mobileId;
                    successCb();
                }, onError.bind(this, new Error(this.component.i18n.getText("ERROR_INSERT_MEAS_DOC"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        MeasuringDocumentHistoryViewModel.prototype.validateLongtext = function () {

            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("TEXT_LINE", this.component.i18n.getText("descriptionEmpty")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        MeasuringDocumentHistoryViewModel.prototype.loadMeasdocLongtext = function (documentNumber, successCb) {
            var that = this;
            var load_success = function (data) {
                that.setDocumentLongtexts(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb(that.getDocumentLongtexts());
                    that.refresh();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(this.component.i18n.getText("errorLoadMeasDocLongtext")));
            };

            this.setBusy(true);
            this._requestHelper.getData("MeasDocLongtext", {
                document: documentNumber
            }, load_success, load_error, true);
        };

        MeasuringDocumentHistoryViewModel.prototype.getNewMeasurementDocumentLongtext = function (document) {

            var newMeasurementDocumentLongText = new MeasdocLongtext(null, this._requestHelper, this);

            newMeasurementDocumentLongText.MDOCM = document.MDOCM;
            newMeasurementDocumentLongText.FORMAT_COL = "*";
            newMeasurementDocumentLongText.OBJKEY = "00000000";
            newMeasurementDocumentLongText.OBJTYPE = "IMRG";
            newMeasurementDocumentLongText.LINE_NUMER = "1";


            return newMeasurementDocumentLongText;
        };

        MeasuringDocumentHistoryViewModel.prototype.getValueStateModel = function () {

            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    RECDV: sap.ui.core.ValueState.None,
                    RECDU: sap.ui.core.ValueState.None,
                    MDTXT: sap.ui.core.ValueState.None,
                    MDOCM: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        MeasuringDocumentHistoryViewModel.prototype.constructor = MeasuringDocumentHistoryViewModel;

        return MeasuringDocumentHistoryViewModel;
    });
