sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "sap/m/Button",
    "sap/m/Dialog",
//    'sap/ui/model/Filter',
    'sap/m/Text',
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/material/EditOrderComponentViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/material/EditMaterialConfirmationViewModel",
    "sap/m/MessageToast"
], function (Controller, formatter, formatterComp, Button, Dialog, Text, EditOrderComponentViewModel, EditMaterialConfirmationViewModel, MessageToast) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.material.MaterialTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailMaterial").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailMaterial", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.workOrderDetailViewModel = null;
            this.materialTabViewModel = null;
            this.editOrderComponentViewModel = null;
            this.editMaterialConfirmationViewModel = null;
        },

        onRouteMatched: function (oEvent) {

            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.materialTabViewModel = this.workOrderDetailViewModel.tabs.material.model;

                this.getView().setModel(this.materialTabViewModel, "materialTabViewModel");
            }

            this.loadViewModelData();
        },

        onExit: function () {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailMaterial", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            this.loadViewModelData(true);
        },

        loadViewModelData: function (bSync) {
            var that = this;
            bSync = bSync == undefined ? false : bSync;

            this.materialTabViewModel.loadData(function () {

            });
        },

        onNewOrderComponentPressed: function () {
                             
            var that = this;
            var onAcceptPressed = function (oComponentModel, dialog) {
                that.materialTabViewModel.addSelectedComponentsToList(oComponentModel, function () {
                    dialog.close();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_INSERT_LOCALLY_SUCCESS_TEXT"));
                });
            };
            
            var dialog = this._getComponentEditDialog(this.materialTabViewModel.getNewEditComponent(), false, onAcceptPressed);
            dialog.open();
        },

        onNewOrderMaterialConfirmationPressed: function () {
            var that = this;
            var onAcceptPressed = function (materialConfirmation, dialog) {
                that.materialTabViewModel.insertNewOrderMaterialConfirmation(materialConfirmation, function () {
                    dialog.close();
                    that.workOrderDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_INSERT_SUCCESS_TEXT"));
                });
            };
            var dialog = this._getMaterialConfirmationEditDialog(this.materialTabViewModel.getNewEditMaterialConfirmation(), false, onAcceptPressed);
            dialog.open();
        },

        onEditOrderMaterialConfirmationPressed: function (oEvent) {
            var that = this;
            var materialConfirmation = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();
            var onAcceptPressed = function (materialConfirmation, dialog) {
                that.materialTabViewModel.updateMaterialConfirmation(materialConfirmation, function () {
                    dialog.close();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_UPDATE_SUCCESS_TEXT"));
                });
            };

            var dialog = this._getMaterialConfirmationEditDialog(materialConfirmation, true, onAcceptPressed);
            dialog.open();
        },

        onDeleteOrderMaterialConfirmationPressed: function (oEvent) {
            var that = this;
            var materialConfirmation = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();

            this._getConfirmActionDialogMaterialConf(function () {
                that.materialTabViewModel.deleteMaterialConfirmation(materialConfirmation, function () {
                    that.workOrderDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_DELETE_SUCCESS_TEXT"));
                });
            }).open();
        },

        onEditOrderComponentPressed: function (oEvent) {
            var that = this;
            var component = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();
            var onAcceptPressed = function (component, dialog) {
                that.materialTabViewModel.updateComponent(component, function () {
                    dialog.close();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_UPDATE_SUCCESS_TEXT"));
                });
            };

            var dialog = this._getComponentEditDialog(component, true, onAcceptPressed);
            dialog.open();
        },

        onDeleteOrderComponentPressed: function (oEvent) {
            var that = this;
            var component = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();

            this._getConfirmActionDialog(function () {
                that.materialTabViewModel.deleteComponent(component, function () {
                    that.workOrderDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_DELETE_SUCCESS_TEXT"));
                });
            }).open();
        },

        // Select dialogs
        displayStorageLocSelectDialog: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").parts.pop().path;
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.displayStorageLocationDialog(oEvent, this);
            } else {
                this.editOrderComponentViewModel.displayStorageLocationDialog(oEvent, this);
            }
        },

        handleStgLocSearch: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").parts.pop().path;
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.handleStorageLocationSearch(oEvent);
            } else {
                this.editOrderComponentViewModel.handleStorageLocationSearch(oEvent);
            }
        },

        handleStgLocSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").parts.pop().path;
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.handleStorageLocationSelect(oEvent);
            } else {
                this.editOrderComponentViewModel.handleStorageLocationSelect(oEvent);
            }
        },

        displayMaterialSearch: function (oEvent) {
                             
            var path = oEvent.getSource().getBindingInfo("value").binding.getPath();
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.displayMaterialDialog(oEvent, this);
            } else {
                this.editOrderComponentViewModel.displayMaterialDialog(oEvent, this);
            }
        },
            
        handleMaterialSearch: function (oEvent) {

//            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.getPath();
//            if (path.includes("materialConfirmation")) {
//                this.editMaterialConfirmationViewModel.handleMaterialSearch(oEvent);
//            } else {
                this.editOrderComponentViewModel.setProperty("/searchstring", oEvent.mParameters.newValue);
                             //detect if Ihla active or not
                if(oEvent.getSource().oParent.mAggregations.items[1].mProperties.pressed){
                    this.editOrderComponentViewModel.loadMalFiteredFuncMaterial(oEvent.mParameters.newValue);
                }else{
                  this.editOrderComponentViewModel.loadMalFuncMaterialWithoutIHLA(oEvent.mParameters.newValue);
                }
              
        },

         onQuantityTextChange : function (oEvent) {
             var that = this;
             var component = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();
             var value = oEvent.getParameter("value");
             component.REQUIREMENT_QUANTITY = value;

             this.materialTabViewModel.validate(component, function (bValidated) {
                 if (!bValidated) {
                     return;
                 }

                 if (component.MOBILE_ID) {
                     this.materialTabViewModel.updateComponent(component, function () {
                         MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_UPDATE_SUCCESS_TEXT"));
                     });
                 } else {
                     this.materialTabViewModel.insertAddedComponent(component, function () {
                         MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_INSERT_TO_DB_SUCCESS_TEXT"));
                         that.workOrderDetailViewModel.loadAllCounters();
                     }.bind(this));
                 }

             }.bind(this));
         },

         onIHLApress : function (oEvent){
            
                             
//           If IHLA active
             if(oEvent.getSource().getPressed()){
                if(!this.editOrderComponentViewModel.oData.allMaterials){
                    this.editOrderComponentViewModel.loadMalFuncMaterialWithIHLA();
                }
                    
             }else{
                    if(!this.editOrderComponentViewModel.oData.allMaterials){  
                        this.editOrderComponentViewModel.loadMalFuncMaterialWithoutIHLA(this.editOrderComponentViewModel.oData.searchstring);
                     }
                 }
         },

         onSelectionChange : function (oEvt) {
                             
             var that = this;
             var materialObj = oEvt.getParameter("listItem").getBindingContext("componentModel").getObject();
             if(typeof that.editOrderComponentViewModel.getProperty("/materialSave") == 'undefined' ){
                    that.editOrderComponentViewModel.setProperty("/materialSave",[]);
             }
            
             if(oEvt.getParameter("selected")){
                // insert into Model
                that.editOrderComponentViewModel.insertToModelPath(materialObj, "/materialSave");

             }else {
                //remove from Model
                that.editOrderComponentViewModel.removeFromModelPath(materialObj, "/materialSave");

             }
             
         },
                             
        handleMaterialSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.getPath();
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.handleMaterialSelect(oEvent);
            } else {
                this.editOrderComponentViewModel.handleMaterialSelect(oEvent);
            }
        },

        displayPlantSelectDialog: function (oEvent) {
            var path = oEvent.getSource().getBindingInfo("value").binding.getPath();
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.displayPlantSelectDialog(oEvent, this);
            } else {
                this.editOrderComponentViewModel.displayPlantSelectDialog(oEvent, this);
            }
        },

        handlePlantSearch: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.getPath();
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.handlePlantSearch(oEvent);
            } else {
                this.editOrderComponentViewModel.handlePlantSearch(oEvent);
            }
        },

        handlePlantSelect: function (oEvent) {
            var path = oEvent.getSource().parentInput.getBindingInfo("value").binding.getPath();
            if (path.includes("materialConfirmation")) {
                this.editMaterialConfirmationViewModel.handlePlantSelect(oEvent);
            } else {
                this.editOrderComponentViewModel.handlePlantSelect(oEvent);
            }
        },

        _getComponentEditDialog: function (component, bEdit, acceptCb) {     
            var that = this;
            var onAcceptPressed = function () {
                var editOrderComponentViewModel = this.getParent().getModel("componentModel");
//                if (editOrderComponentViewModel.validate()) {
                    acceptCb(editOrderComponentViewModel, this.getParent());
//                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editOrderComponentViewModel = dialog.getModel("componentModel");
                editOrderComponentViewModel.rollbackChanges();
                dialog.close();
            };

            this.editOrderComponentViewModel = new EditOrderComponentViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editOrderComponentViewModel.setEdit(bEdit);
            this.editOrderComponentViewModel.setOrderComponent(component);
            this.editOrderComponentViewModel.setOrder(this.materialTabViewModel.getOrder());
            // set the searchstring to empty instead of undefined
            this.editOrderComponentViewModel.setProperty("/searchstring", "");
//            this.editOrderComponentViewModel.setProperty("/allMaterials", {allmaterials: false, searchactive: false});
            this.editOrderComponentViewModel.loadMalFuncMaterial();
                             
            this._oMcPopoverContent = sap.ui.xmlfragment("editOrderComponentPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.material.OrderComponentEditPopover", this);


            this._matCompPopup = new Dialog({
                stretch: sap.ui.Device.system.phone,
                contentWidth: "29rem",
                title: "New Component",
                content: this._oMcPopoverContent,
                beginButton: new Button({
                    text: '{= ${componentModel>/view/edit} ? ${i18n>update} : ${i18n_core>create}}',
                    icon: "sap-icon://save",
                    press: onAcceptPressed
                }),
                endButton: new Button({
                    text: '{i18n_core>cancel}',
                    icon: "sap-icon://decline",
                    press: onCancelPressed
                }),
                afterClose: function () {

                }
            });

            this._matCompPopup.setModel(this.component.getModel('i18n'), "i18n");
            this._matCompPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");

            this._matCompPopup.setTitle(this.editOrderComponentViewModel.getDialogTitle());
            this._matCompPopup.getBeginButton().mEventRegistry.press = [];
            this._matCompPopup.getBeginButton().attachPress(onAcceptPressed);
            this._matCompPopup.setModel(this.editOrderComponentViewModel, "componentModel");

            this._matCompPopup.setModel(this.editOrderComponentViewModel.getValueStateModel(), "valueStateModel");

            return this._matCompPopup;
        },

        _getMaterialConfirmationEditDialog: function (materialConfirmation, bEdit, acceptCb) {
            var that = this;

            var onAcceptPressed = function () {
                var editMaterialConfirmationViewModel = this.getParent().getModel("materialConfirmationModel");

                if (editMaterialConfirmationViewModel.validate()) {
                    acceptCb(editMaterialConfirmationViewModel.getMaterialConfirmation(), this.getParent());
                }
            };

            var onCancelPressed = function () {
                var dialog = this.getParent();
                var editMaterialConfirmationViewModel = dialog.getModel("materialConfirmationModel");
                editMaterialConfirmationViewModel.rollbackChanges();
                dialog.close();
            };

            this.editMaterialConfirmationViewModel = new EditMaterialConfirmationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editMaterialConfirmationViewModel.setEdit(bEdit);
            this.editMaterialConfirmationViewModel.setMaterialConfirmation(materialConfirmation);
            this.editMaterialConfirmationViewModel.setOrder(this.materialTabViewModel.getOrder());


            this._oMcPopoverContentMat = sap.ui.xmlfragment("editMaterialConfirmationPopover", "SAMMobile.components.WorkOrders.view.workOrders.detail.material.MaterialConfirmationEditPopover", this);


            this._matConfPopup = new Dialog({
                stretch: sap.ui.Device.system.phone,
                title: "NEW MATERIAL CONFIRMATION",
                content: this._oMcPopoverContentMat,
                beginButton: new Button({
                    text: '{= ${materialConfirmationModel>/view/edit} ? ${i18n>update} : ${i18n_core>create}}',
                    icon: "sap-icon://save",
                    press: onAcceptPressed
                }),
                endButton: new Button({
                    text: '{i18n_core>cancel}',
                    icon: "sap-icon://decline",
                    press: onCancelPressed
                }),
                afterClose: function () {

                }
            });

            this._matConfPopup.setModel(this.component.getModel('i18n'), "i18n");
            this._matConfPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");

            this._matConfPopup.setTitle(this.editMaterialConfirmationViewModel.getDialogTitle());
            this._matConfPopup.getBeginButton().mEventRegistry.press = [];
            this._matConfPopup.getBeginButton().attachPress(onAcceptPressed);

            this._matConfPopup.setModel(this.editMaterialConfirmationViewModel, "materialConfirmationModel");
            this._matConfPopup.setModel(this.editMaterialConfirmationViewModel.getValueStateModel(), "valueStateModel");

            return this._matConfPopup;
        },

        _getConfirmActionDialog: function (executeCb) {
            var that = this;

            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("deleteOrderComponentConfirmation"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        _getConfirmActionDialogMaterialConf: function (executeCb) {
            var that = this;

            var _getConfirmActionDialogMaterialConf = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("deleteMaterialConfirmation"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _getConfirmActionDialogMaterialConf;
        },

        confirmComponent: function (oEvent) {
            var that = this;
            var component = oEvent.getSource().getBindingContext("materialTabViewModel").getObject();

            var materialConfirmation = this.materialTabViewModel.confirmComponent(component);

            that.materialTabViewModel.insertNewOrderMaterialConfirmation(materialConfirmation, function () {
                that.workOrderDetailViewModel.loadAllCounters();
                MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_INSERT_SUCCESS_TEXT"));

            });
        },

        confirmAllComponents: function () {
            
            var that = this;
            var components = this.materialTabViewModel.getData().components;

            components.forEach(function (component) {
                var materialConfirmation = that.materialTabViewModel.confirmComponent(component);
                that.materialTabViewModel.insertNewOrderMaterialConfirmation(materialConfirmation, function () {
                    that.workOrderDetailViewModel.loadAllCounters();
                    MessageToast.show(that.component.getModel("i18n").getResourceBundle().getText("MATERIAL_CONFIRMATION_INSERT_SUCCESS_TEXT"));
                });
            });

        },

    });

});
