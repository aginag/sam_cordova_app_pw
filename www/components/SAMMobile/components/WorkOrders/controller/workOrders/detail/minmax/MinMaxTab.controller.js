sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageToast",
    "SAMMobile/models/dataObjects/SAMChecklistPoint",
    "SAMMobile/controller/common/MediaAttachments.controller",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp"
], function(JSONModel, Controller, MessageToast, SAMChecklistPoint, MediaAttachmentsController, formatter, formatterComp) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.detail.minmax.MinMaxTab", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function() {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderDetailMinMax").attachPatternMatched(this.onRouteMatched, this);

            sap.ui.getCore()
                .getEventBus()
                .subscribe("workOrderDetailMinMax", "refreshRoute", this.onRefreshRoute, this);

            this.workOrderDetailViewModel = null;
            this.minMaxTabViewModel = null;

            this.delayedMeasurementPointTextSave = null;

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.mediaAttachmentsController = new MediaAttachmentsController();
            this.mediaAttachmentsController.initialize(this.getOwnerComponent());
        },

        onRouteMatched: function(oEvent) {
            var oArgs = oEvent.getParameter("arguments");

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(oEvent.getParameter('name'));

            if (!this.workOrderDetailViewModel) {
                this.workOrderDetailViewModel = this.getView().getModel("workOrderDetailViewModel");
                this.minMaxTabViewModel = this.workOrderDetailViewModel.tabs.minmax.model;

                this.getView().setModel(this.minMaxTabViewModel, "minMaxTabViewModel");
            }

            this.minMaxTabViewModel.setProperty("/view/inputFieldsEditable", true);

            this.minMaxTabViewModel.setOrderId(oArgs.id);
            this.minMaxTabViewModel.setNotificationNumber(oArgs.notifNo);

            this.loadViewModelData();
        },

        onNavBack: function () {
            var that = this;
            if (this.getOwnerComponent().getOrderListVariant() == "ORDER") {
                this.oRouter.navTo("workOrders");
            } else if (this.getOwnerComponent().getOrderListVariant() == "OPERATION") {
                this.oRouter.navTo("workOrderOperations");
            }

            setTimeout(function () {
                that.workOrderDetailViewModel.resetData();
            }, 200);
        },

        onExit: function() {
            sap.ui.getCore()
                .getEventBus()
                .unsubscribe("workOrderDetailMinMax", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function() {
            this.loadViewModelData(true);
        },

        loadViewModelData: function(bSync) {
            var that = this;
            this.getView().byId("idMinMax").setBusy(true);
            this.minMaxTabViewModel.loadData(function () {
                that.workOrderDetailViewModel.loadAllCounters();
                this.getView().byId("idMinMax").setBusy(false);
            }.bind(this));
        },

        onOpenCustomAttachmentsDialog: function (oEvent) {
            var data = [];
            this.workOrderDetailViewModel.loadCustomAttachments().then(function (data) {
                this.getCustomAttachmentDialog(data).open();
            }.bind(this));
        },

        onMeasurementPointRequiredPress: function (oEvent) {

            // bSelected = true --> No readings to be captured
            // bSelected = false --> Readings to be captured
            var bSelected = oEvent.getParameter("selected");
            this.minMaxTabViewModel.setProperty("/view/isEditable", false);
            var that = this;
            that.minMaxTabViewModel.refresh();
            this.getView().byId("idMinMax").setBusy(true);

            this.minMaxTabViewModel.setUserStatusForMaxReadings(bSelected, this.getOwnerComponent().scenario.getOrderUserStatus().minMax.noReadingRequired.STATUS , function () {
                this.minMaxTabViewModel.setProperty("/view/isRequired", !bSelected);


            var checkListData = this.minMaxTabViewModel.getCheckList();

            if(bSelected) {
                var oQueue = [];
                checkListData.forEach(function (checklistObject) {
                    if (checklistObject instanceof SAMChecklistPoint) {
                        oQueue.push(checklistObject);
                    }
                });
                if(oQueue.length > 0){
                    this.minMaxTabViewModel.updateAllChecklistPoint(oQueue, function(){
 
                   this.minMaxTabViewModel.loadData(function(){
                        this.getView().byId("idMinMax").setBusy(false);
                    }.bind(this));
                    this.minMaxTabViewModel.setProperty("/view/inputFieldsEditable", false);
                }.bind(this));
            }else{
                this.getView().byId("idMinMax").setBusy(false);
            }

            } else {
                that.getView().byId("idMinMax").setBusy(false);
                that.minMaxTabViewModel.setProperty("/view/inputFieldsEditable", true);
            }
            that.minMaxTabViewModel.setProperty("/view/isRequired", !bSelected);
            that.minMaxTabViewModel.setProperty("/view/isEditable", true);
            that.minMaxTabViewModel.refresh();

            }.bind(this));
        },

        onMeasurementCheckpointTextEntry: function (oEvent) {
            var checkListPoint = oEvent.getSource().getBindingContext("minMaxTabViewModel").getObject();
            var value = oEvent.getParameter("value");
            this.getView().byId("idMinMax").setBusy(true);

            clearTimeout(this.delayedMeasurementPointTextSave);
            this.delayedMeasurementPointTextSave = setTimeout(this.validateSaveMeasurmentPointText.bind(this, checkListPoint, value), 200);
        },

        formatMeasurmentPointResultToText: function (checklistPoint) {
            if (checklistPoint.hasOwnProperty("MOBILE_ID")) {
                return checklistPoint.formatMeasurmentPointResultToText();
            }
        },

        validateSaveMeasurmentPointText: function(checklistPoint, value) {
            checklistPoint.setResultReading(value);


            this.minMaxTabViewModel.validate(function (bValidated) {
                if (!bValidated) {
                    this.getView().byId("idMinMax").setBusy(false);
                    return;
                }

                this.minMaxTabViewModel.updateChecklistPoint(checklistPoint, function() {
                    this.minMaxTabViewModel.calculateMaximaleLeistung(function () {
                        this.getView().byId("idMinMax").setBusy(false);
                        this.minMaxTabViewModel.refresh(true);
                        MessageToast.show(this.getOwnerComponent().i18n.getText("MIN_MAX_MEASUREMENT_POINT_SAVED_SUCCESSFULLY_TEXT"));
                    }.bind(this), this.getView().byId("idMinMax"));
                }.bind(this));
            }.bind(this));
        },

        getCustomAttachmentDialog: function (data) {
            if (!this._oCustomDialog) {
                this._oCustomDialog = sap.ui.xmlfragment("attachmentDialog", "SAMMobile.components.WorkOrders.view.common.Attachments", this);
                this.getView().addDependent(this._oCustomDialog);
            }

            if (data) {
                this._oCustomDialog.setModel(new JSONModel({
                    attachments: data
                }), "customAttachmentsModel");
            }

            return this._oCustomDialog;
        },


        onCloseCustomAttachmentsDialog: function () {
            this.getCustomAttachmentDialog().close();
        },

        _onOpenCustomAttachment: function (oEvent) {
            var bContext = oEvent.getParameter("listItem").getBindingContext("customAttachmentsModel");
            this.mediaAttachmentsController._onOpenAttachment(null, bContext);
        },

    });

});
