sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "sap/m/MessageToast"],

    function (JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, MessageToast) {
        "use strict";

        // constructor
        function RejectOperationViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.formatter = new Formatter(this._component);
            this.valueStateModel = null;

            this._rejectOperationDialogs = {};

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        RejectOperationViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        orderOperation: null,
                        rejectionReasonDescription: '',
                        rejectionReason: '',
                        OrderOperationLongtextToAdd: '',
                        OrderOperationLongtext: '',
                        view: {
                            busy: false,
                            errorMessages: [],
                            openedFromList: true
                        }
                    };
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            getOwnerComponent: {
                value: function () {
                    return this._component;
                }
            }
        });

        RejectOperationViewModel.prototype.openDialog = function (oOperation, fromList) {

            this.setProperty("/view/openedFromList", fromList);

            var oDialog = this._rejectOperationDialogs["rejectOperation"];
            this.setProperty("/operation", oOperation);
            this.setProperty("/completeVisible", oOperation.CompleteVisible);

            if (!oDialog) {
                oDialog = sap.ui.xmlfragment("SAMMobile.components.WorkOrders.view.workOrders.detail.reject.RejectOperationDialog", this);
                this._rejectOperationDialogs["rejectOperation"] = oDialog;
            }

            oDialog.setModel(this, "rejectOperationModel");
            oDialog.setModel(this._component.getModel("i18n"), "i18n");
            oDialog.setModel(this._component.getModel("i18n_core"), "i18n_core");


            var fnSuccess = function (sLongtext) {
                this.setProperty("/OrderOperationLongtext", sLongtext);
            }.bind(this);

            this.extractRejectionLongtext(fnSuccess);

            oDialog.open();
        };

        RejectOperationViewModel.prototype.onCancelRejectOperationPress = function (oEvent) {
            this._clearAndClose();
        };

        RejectOperationViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        RejectOperationViewModel.prototype.displayOperationRejectionReasons = function(oEvent) {
            if (!this._oRejectionReasonSelectDialog) {
                this._oRejectionReasonSelectDialog = new GenericSelectDialog("SAMMobile.components.WorkOrders.view.common.OrderOperationRejectionReasonSelectDialog", this._requestHelper, this, this, GenericSelectDialog.mode.CUST_MODEL);
            }

            this._oRejectionReasonSelectDialog.dialog.setModel(this._component.getModel("i18n"), "i18n");

            this._oRejectionReasonSelectDialog.display(oEvent, 'ZPMC_REJ_REASONS');
        };

        RejectOperationViewModel.prototype.handleRejectionReasonSelect = function(oEvent) {
            this._oRejectionReasonSelectDialog.select(oEvent, 'statusDescription', [{
                'rejectionReason': 'status'
            }, {
                'rejectionReasonDescription': 'statusDescription'
            }], this);

        };

        RejectOperationViewModel.prototype.handleRejectionReasonSearch = function(oEvent) {
            this._oRejectionReasonSelectDialog.search(oEvent, ['status', 'statusDescription']);
        };

        RejectOperationViewModel.prototype.onSaveRejectOperationPress = function (oEvent) {
            var that = this;
            if (this.validate()) {
                var fnSuccessLongtext = function () {

                    var fnSuccessUserStatus = function () {
                        MessageToast.show(that._component.i18n.getText("REJECT_OPERATION_SUCCESS_TEXT"));
                        that._clearAndClose();
                    };

                    var oOperation = this.getProperty("/operation");
                    var statusProfile = oOperation.OPERATION_STATUS_PROFILE ? oOperation.OPERATION_STATUS_PROFILE : this._component.scenario.getOrderUserStatus().prio.STATUS_PROFILE;

                    /**Action flag of userstatus is changing wrongly to U after running func getUserStatusMl
                    * workaround to avoid this behaviour and set the correct action flag after func
                    **/
                    var tempOperationUserStatus = oOperation.OrderUserStatus.find(x => x.STATUS == 'E0003' && x.ACTION_FLAG == 'N');
                    var statusMl = this.getUserStatusMl(this._component.scenario.getOrderUserStatus().prio.STATUS, statusProfile);

                    if(typeof tempOperationUserStatus !== 'undefined'){

                        statusMl.ACTION_FLAG = tempOperationUserStatus.ACTION_FLAG;  

                    }
                    
                    // var statusMl = this.getUserStatusMl(this._component.scenario.getOrderUserStatus().prio.STATUS, statusProfile);
                    oOperation.changeUserStatus(
                        statusMl,
                        false,
                        fnSuccessUserStatus.bind(this),
                        this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                    );

                }.bind(this);

                this._saveRejectionLongtext(fnSuccessLongtext);
            }
        };

        RejectOperationViewModel.prototype.validate = function () {
            var modelData = this.getData();
            var validator = new Validator(modelData, this.getValueStateModel());

            validator.check("rejectionReason", this._component.getModel("i18n").getResourceBundle().getText("REJECT_OPERATION_VALIDATION_EMPTY_REJECTION_REASON")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        RejectOperationViewModel.prototype.onDiscardRejectOperationPress = function (oEvent) {

            var that = this;
         
            var oOperation = this.getProperty("/operation");
            var statusProfile = oOperation.OPERATION_STATUS_PROFILE ? oOperation.OPERATION_STATUS_PROFILE : this._component.scenario.getOrderUserStatus().down.STATUS_PROFILE;
            var statusMl = this.getUserStatusMl(this._component.scenario.getOrderUserStatus().prio.STATUS, statusProfile);
            
            var fnLongtextDeleteSuccess = function (data) {

                var fnSuccessUserStatus = function () {
                    MessageToast.show(that._component.i18n.getText("REJECT_OPERATION_DISCARD_SUCCESS_TEXT"));
                    that._clearAndClose();
                };
                 var tempOperationUserStatus = oOperation.OrderUserStatus.find(x => x.STATUS == 'E0003' && x.ACTION_FLAG == 'N');
                if(typeof tempOperationUserStatus !== 'undefined'){
                    statusMl.ACTION_FLAG = tempOperationUserStatus.ACTION_FLAG;  
                }
                // Change the Operation Status
                oOperation.setUserStatusToInactive(
                    statusMl,
                    false,
                    fnSuccessUserStatus.bind(this),
                    this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION_DISCARD")))
                );
            }.bind(this);

            this._requestHelper.setField("deleteRejectionLongtext", "OrderOperations", {
                orderId: oOperation.ORDERID,
                objkey: oOperation.ACTIVITY + "0000"
            }, fnLongtextDeleteSuccess, this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION_DISCARD"))));
        };

        RejectOperationViewModel.prototype.createRejectionLongtext = function () {
            var sRejectionReason = this.getProperty("/rejectionReason"),
                sRejectionDescription = this.getProperty("/rejectionReasonDescription"),
                sRejectionLongtext = this.getProperty("/OrderOperationLongtextToAdd"),
                sPersNo = this._globalViewModel.model.getProperty("/personnelNumber"), /*set the perso from global model*/
                sUserInformation = this.getUserInformation(),
                sUsername = sUserInformation.firstName + " " + sUserInformation.lastName;

            var sDate = new Date().toLocaleString();

            var sLongtextToAdd = sRejectionLongtext
            ? sDate + " - " + sPersNo + " - " + sUsername + " - " + sRejectionReason + " - " + sRejectionDescription + " - " + sRejectionLongtext
                : sDate + " - " + sPersNo + " - " + sUsername + " - " + sRejectionReason + " - " + sRejectionDescription;

            return sLongtextToAdd;
        };

        RejectOperationViewModel.prototype.extractRejectionLongtext = function (load_success) {
            var oOperation = this.getProperty("/operation");

            var fnSuccess = function () {
                var sLongtext = "";

                var aLongtexts = oOperation.OrderOperationLongtext;

                aLongtexts = aLongtexts.filter(function (oLongtext) {
                   return oLongtext.FORMAT_COL === "$" && ( oLongtext.ACTION_FLAG === "N" || oLongtext.ACTION_FLAG === "C" || oLongtext.ACTION_FLAG === "U" || oLongtext.ACTION_FLAG === "I" || oLongtext.ACTION_FLAG === "K" || oLongtext.ACTION_FLAG === "L");
                });

                aLongtexts.forEach(function (oLongtext) {
                    sLongtext = sLongtext + oLongtext.TEXT_LINE;
                });

                load_success(sLongtext);
            };

            var fnError = function (err) {
                console.log(err);
            };

            oOperation.loadLongText(this._component.scenario.getOperationsItemLongtextInformation().objectType, fnSuccess, fnError);
        };

        RejectOperationViewModel.prototype._saveRejectionLongtext = function (successCb) {
            var oOperation = this.getProperty("/operation"),
                sLongtextToAdd = this.createRejectionLongtext(),
                sObjectType = this._component.scenario.getOperationsItemLongtextInformation().objectType,
                strings = [];

            var onLongTextInsertedSuccessfully = function () {
                oOperation.LONGTEXT = oOperation.loadLongText(sObjectType, successCb, this.executeErrorCallback);
                this.refresh();
            }.bind(this);

            var onAllLongtextReceivedSuccessfully = function () {

                var aLongtextsToDelete = oOperation.OrderOperationLongtext;

                aLongtextsToDelete = aLongtextsToDelete.filter(function (oLongtext) {
                    return oLongtext.FORMAT_COL === "$" && oLongtext.ACTION_FLAG === 'N';
                });

                aLongtextsToDelete.forEach(function (oLongtextToDelete) {
                    strings = strings.concat(oLongtextToDelete.delete(true));
                });

                strings = strings.concat(oOperation.addLongtext(true, sLongtextToAdd, sObjectType, "$"));
                strings.push(oOperation.setChanged(true));

                try {
                    this._requestHelper.executeStatementsAndCommit(strings, onLongTextInsertedSuccessfully, this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION"))));
                } catch (err) {
                    this.executeErrorCallback(err);
                }

            }.bind(this);
            oOperation.loadLongText(this._component.scenario.getOperationsItemLongtextInformation().objectType, onAllLongtextReceivedSuccessfully, this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION"))));

        };

        RejectOperationViewModel.prototype._clearAndClose = function () {
            this.setProperty("/rejectionReasonDescription", "");
            this.setProperty("/rejectionReason", "");
            this.setProperty("/OrderOperationLongtextToAdd", "");
            this.setProperty("/view/errorMessages", []);

            if (this.getProperty("/view/openedFromList")) {
                sap.ui.getCore().getEventBus().publish("workOrderOperations", "refreshRoute");
            } else {
                sap.ui.getCore().getEventBus().publish("workOrderDetailRoute", "refreshRoute");
                this.getOwnerComponent().getRouter().navTo("workOrderOperations");
            }

            this._rejectOperationDialogs["rejectOperation"].close();
            if (this._rejectOperationDialogs) {
                this._rejectOperationDialogs["rejectOperation"].destroy();
            }
            this._rejectOperationDialogs = {};
        };

        RejectOperationViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    rejectionReason: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        RejectOperationViewModel.prototype.constructor = RejectOperationViewModel;

        return RejectOperationViewModel;
    });
