sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/Showman",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/helpers/FileSystem"
    ],

    function (JSONModel, ViewModel, Validator, Formatter, Showman, DMSAttachment, FileSystem) {
        "use strict";

        // constructor
        function ShowmanTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this.__showmanEntry = null;

            this.fileSystem = new FileSystem();

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        ShowmanTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        view: {
                            busy: false,
                            errorMessages: [],
                            isEditable: true,
                            isRequired: true,
                            installationSelected: false,
                            removalSelected: false,
                            disableSelected: false
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            getOrderId: {
                value: function () {
                    return this._ORDERID;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            setShowmanEntry: {
                value: function (showmanData) {
                    this._showmanEntry = showmanData;
                }
            },

            getShowmanEntry: {
                value: function () {
                    return this._showmanEntry;
                }
            },

            getNotificationNumber: {
                value: function () {
                    return this._NOTIFNO;
                }
            },

            canAttachFiles: {
                value: function () {
                    return this.getOrder().getOrderId() !== "";
                }
            },

            setNotificationNumber: {
                value: function (sNotifNo) {
                    this._NOTIFNO = sNotifNo;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        ShowmanTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        ShowmanTabViewModel.prototype.setNewData = function (data) {
            var showmanObject;
            var that = this;

            var attachmentButtonsNewPhoto = this._component.getAttachmentButtonOrderNewPhoto();
            this.setProperty("/showOtherAttachmentPhotoButtons", attachmentButtonsNewPhoto);

            var attachmentButtonsNewAudio = this._component.getAttachmentButtonOrderNewAudio();
            this.setProperty("/showOtherAttachmentAudioButtons", attachmentButtonsNewAudio);

            var attachmentButtonsNewVideo = this._component.getAttachmentButtonOrderNewVideo();
            this.setProperty("/showOtherAttachmentVideoButtons", attachmentButtonsNewVideo);

            var attachmentButtonsNewFile = this._component.getAttachmentButtonOrderNewFile();
            this.setProperty("/showOtherAttachmentFileButtons", attachmentButtonsNewFile);
            

            var showManObject = new Showman(null, this._requestHelper, this);
            var operation = this.getOrder().getOrderOperations();
            showManObject.ORDERNO = operation.ORDERID;
            showManObject.OPERATION = operation.ACTIVITY;
            
            data.forEach(function(showMan){
                if(showMan.INST_DAT){
                    showManObject.installationSelected = true;
                    showManObject.INST_DAT = showMan.INST_DAT;
                    showManObject.START_CNT_READ = showMan.START_CNT_READ;
                    showManObject.CONV_FACTOR = showMan.CONV_FACTOR;
                    showManObject.installationOperation = showMan.OPERATION;    
                    showManObject.CUST_PROPERTY_COUNT = showMan.CUST_PROPERTY_COUNT;
                }

                if(showMan.REMOVE_DAT){
                    showManObject.removalSelected = true;
                    showManObject.REMOVE_DAT = showMan.REMOVE_DAT;
                    showManObject.END_CNT_READ = showMan.END_CNT_READ;
                    showManObject.removalOperation = showMan.OPERATION;
                }

                if(showManObject.OPERATION == showMan.OPERATION){
                    showManObject.MOBILE_ID = showMan.MOBILE_ID;
                    showManObject.ACTION_FLAG = showMan.ACTION_FLAG;
                }
            });

            
            if(showManObject.ACTION_FLAG != "N"){
                showManObject.installationEditable = false;
                showManObject.removalEditable = false;
                showManObject.disableEditable = false;

            }else{
                if(showManObject.installationOperation){
                    if(showManObject.OPERATION == showManObject.installationOperation){
                        showManObject.installationEditable = true;
                    }else{
                        showManObject.installationEditable = false;
                    }
                }else{
                    showManObject.installationEditable = true;
                }


                if(showManObject.removalOperation){
                    if(showManObject.OPERATION == showManObject.removalOperation){
                        showManObject.removalEditable = true;
                    }else{
                        showManObject.removalEditable = false;
                    }
                }else{
                    showManObject.removalEditable = true;
                }
                showManObject.disableEditable = true;
            }

            var showmanForOrderOperation = showManObject.showmanProcesses.filter(function (singleShowmanData) {
                return singleShowmanData.ORDERNO === showManObject.ORDERNO && singleShowmanData.OPERATION === showManObject.OPERATION;
            });

            if (showmanForOrderOperation && showmanForOrderOperation.length > 0) {
                showManObject.disableSelected = showmanForOrderOperation[0].disableSelected;
            } else {
                showManObject.showmanProcesses.push({
                    ORDERNO: showManObject.ORDERNO,
                    OPERATION: showManObject.OPERATION,
                    disableSelected: false
                });
            }
            
            this.setShowmanEntry(showManObject);
            this.setProperty("/showman", showManObject);

            this.refresh();

            showManObject._saveLocalStorage();
        };

        ShowmanTabViewModel.prototype.loadData = function () {
            var that = this;

            var load_success = function (data) {
                that.setNewData(data);
            }.bind(this);

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOAD_DATA_ORDER_DETAIL_SHOWNMAN_TEXT")));
            };

            this.setBusy(true);
            this._requestHelper.getData("showManData", {
                orderId: this.getOrderId()
            }, load_success, load_error);
        };

        ShowmanTabViewModel.prototype._insertShowman = function (showmanObject) {
            var that = this;
            var onSuccess = function () {
                that.refresh();
            }.bind(this);

            try {
                showmanObject.insert(false, function (mobileId) {
                    showmanObject.MOBILE_ID = mobileId;
                    that.loadData();
                    onSuccess();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_SHOWMAN_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
            this.refresh();
        };

        ShowmanTabViewModel.prototype._updateShowman = function (showmanObject) {
            var that = this;

            var onSuccess = function () {
                that.refresh();
            }.bind(this);

            try {
                showmanObject.update(false, function () {
                    that.loadData();
                    onSuccess();
                }, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
            this.refresh();
        };

        ShowmanTabViewModel.prototype._deleteShowman = function (showmanObject, successCb) {
            var that = this;
            var sqlStrings = [];
            
            var onSuccess = function () {
                that.refresh();
                if(successCb != undefined){
                    successCb();
                }
            }.bind(this);

            sqlStrings = sqlStrings.concat(showmanObject.delete(true));

            this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_EXTRA_PAY_DELETE")));
            });

        };

        // Installation

        ShowmanTabViewModel.prototype.validateInstallationEntries = function (showmanObject) {
            var validator = new Validator(showmanObject, this.getValueStateModel());

            validator.check("INST_DAT", this._component.i18n.getText("INSTALLATION_DATE_EMPTY")).isEmpty();
            validator.check("START_CNT_READ", this._component.i18n.getText("START_COUNTER_READING_EMPTY")).isEmpty();
            validator.check("START_CNT_READ", this._component.i18n.getText("START_COUNTER_READING_EMPTY_NOT_NUMBER")).isNumber();
            validator.check("CONV_FACTOR", this._component.i18n.getText("CONV_FACTOR_EMPTY")).isEmpty();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        ShowmanTabViewModel.prototype.setInstallationDate = function (installationDate) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            if(installationDate !== "") {
                if (this.validateInstallationDate(installationDate, showmanObject)) {
                    showmanObject.setInstallationDate(installationDate);
                } else {
                    showmanObject.INST_DAT = showmanObject.getDateNow().dateTime;
                }
                that.updateOrInsertShowman(showmanObject);
            }
            this.refresh();
        };

        ShowmanTabViewModel.prototype.setCounterStart = function (startCounter) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            if(startCounter !== "") {
                if (this.validateCounterStart(showmanObject, startCounter)) {
                    showmanObject.START_CNT_READ = startCounter;
                } else {
                    showmanObject.START_CNT_READ = "";
                }
            }
            that.updateOrInsertShowman(showmanObject);
            this.refresh();
        };

        ShowmanTabViewModel.prototype.setCheckboxCounter = function (startCounter) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            showmanObject.CUST_PROPERTY_COUNT = startCounter ? 'X' : '';
            that.updateOrInsertShowman(showmanObject);
            this.refresh();
        };

        ShowmanTabViewModel.prototype.setConverterRatio = function (convFactor) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            if (this.validateConverterRatio(convFactor)) {
                showmanObject.CONV_FACTOR = convFactor;
                that.updateOrInsertShowman(showmanObject);
            }
            this.refresh();
        };

        ShowmanTabViewModel.prototype.validateConverterRatio = function (convFactor) {
            var that = this;
            var validator = new Validator(null, this.getValueStateModel());
            var value = convFactor;
            validator.check("", that._component.i18n.getText("SRB_READING_IS_NO_VALID_NUMBER")).custom(function () {
                return !isNaN(value);
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        // Removal

        ShowmanTabViewModel.prototype.setRemovalDate = function (removalDate) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            if(removalDate !== "") {
                if (this.validateRemovalDate(removalDate, showmanObject)) {
                    showmanObject.setRemovalDate(removalDate);
                } else {
                    showmanObject.REMOVE_DAT = showmanObject.getDateNow().dateTime;
                }
                that.updateOrInsertShowman(showmanObject);
            }
            this.refresh();
        };

        ShowmanTabViewModel.prototype.setCounterEnd = function (counterEnd) {
            var that = this;
            var showmanObject = this.getShowmanEntry();
            if(counterEnd !== "") {
                if (this.validateCounterEnd(showmanObject, counterEnd)) {
                    showmanObject.END_CNT_READ = counterEnd;
                } else {
                    showmanObject.END_CNT_READ = "";
                }
            }
            that.updateOrInsertShowman(showmanObject);
            this.refresh();
        };

        ShowmanTabViewModel.prototype.validateCounterStart = function (showmanObject, counterStart) {
            var that = this;
            var checkObject = {START_CNT_READ: counterStart, END_CNT_READ: showmanObject.END_CNT_READ};
            var validator = new Validator(checkObject, this.getValueStateModel());

            validator.check("", that._component.i18n.getText("VALIDATON_COUNTER_START")).custom(function (value) {
                var startCounter = parseInt(value.START_CNT_READ);
                var endCounter = parseInt(value.END_CNT_READ);

                if (value.START_CNT_READ && startCounter && endCounter && value.END_CNT_READ) {
                    return endCounter > startCounter;
                } else {
                    return true;
                }
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        ShowmanTabViewModel.prototype.validateCounterEnd = function (showmanObject, counterEnd) {
            var that = this;
            var checkObject = {START_CNT_READ: showmanObject.START_CNT_READ, END_CNT_READ: counterEnd};
            var validator = new Validator(checkObject, this.getValueStateModel());

            validator.check("", that._component.i18n.getText("VALIDATION_COUNTER_END")).custom(function (value) {
                var startCounter = parseInt(value.START_CNT_READ);
                var endCounter = parseInt(value.END_CNT_READ);

                if (value.START_CNT_READ && startCounter && endCounter && value.END_CNT_READ) {
                    return endCounter > startCounter;
                } else {
                    return true;
                }
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        ShowmanTabViewModel.prototype.validateInstallationDate = function (installationDate, showmanObject) {
            var that = this;
            var checkObject = {INST_DAT: installationDate, REMOVE_DAT: showmanObject.REMOVE_DAT};
            var validator = new Validator(checkObject, this.getValueStateModel());

            validator.check("", that._component.i18n.getText("VALIDATON_INSTALLATIONDATE")).custom(function (value) {
                var instDat = value.INST_DAT;
                var removeDat = value.REMOVE_DAT;
                var unixTimeInstDat = parseInt(moment(instDat).format("x"));
                var unixTimeRemoveDat = parseInt(moment(removeDat).format("x"));
                if(instDat && moment(instDat).isValid() && removeDat && moment(removeDat).isValid()){
                    return unixTimeRemoveDat >= unixTimeInstDat;
                } else {
                    return true;
                }
            });

            // Executed Date in Future
            validator.check("INST_DAT", that._component.i18n.getText("INSTALLATION_DATE_ERROR_START_TIME_IN_FUTURE")).custom(function (value) {
                return moment(value).isBefore(moment());
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };


        ShowmanTabViewModel.prototype.validateRemovalDate = function (removalDate, showmanObject) {
            var that = this;
            var checkObject = {INST_DAT: showmanObject.INST_DAT, REMOVE_DAT: removalDate}
            var validator = new Validator(checkObject, this.getValueStateModel());

            validator.check("", that._component.i18n.getText("VALIDATON_REMOVALDATE")).custom(function (value) {
                var instDat = value.INST_DAT;
                var removeDat = value.REMOVE_DAT;
                var unixTimeInstDat = parseInt(moment(instDat).format("x"));
                var unixTimeRemoveDat = parseInt(moment(removeDat).format("x"));
                if(instDat && moment(instDat).isValid() && removeDat && moment(removeDat).isValid()){
                    return unixTimeRemoveDat >= unixTimeInstDat;
                } else {
                    return true;
                }
            });

            // Executed Date in Future
            validator.check("REMOVE_DAT", that._component.i18n.getText("REMOVAL_DATE_ERROR_START_TIME_IN_FUTURE")).custom(function (value) {
                return moment(value).isBefore(moment());
            });

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        ShowmanTabViewModel.prototype.validateRemovalEntries = function (showmanObject) {
            var validator = new Validator(showmanObject, this.getValueStateModel());

            validator.check("REMOVE_DAT", this._component.i18n.getText("REMOVAL_DATE_EMPTY")).isEmpty();
            validator.check("END_CNT_READ", this._component.i18n.getText("END_COUNTER_READING_EMPTY")).isEmpty();
            validator.check("END_CNT_READ", this._component.i18n.getText("END_COUNTER_READING_NOT_A_NUMBER")).isNumber();

            var errors = validator.checkErrors();
            this.setProperty("/view/errorMessages", errors ? errors : []);

            return errors ? false : true;
        };

        ShowmanTabViewModel.prototype.updateOrInsertShowman = function (showmanObject) {
            
            var that = this;
            var tempShowmanObject = showmanObject;
            if(showmanObject.installationOperation != this.getOrder().OPERATION_ACTIVITY){
                tempShowmanObject.INST_DAT = "";
                tempShowmanObject.CONV_FACTOR = "";
                tempShowmanObject.START_CNT_READ = "";
            }
            
            if(showmanObject.removalOperation != this.getOrder().OPERATION_ACTIVITY){
                tempShowmanObject.REMOVE_DAT = "";
                tempShowmanObject.END_CNT_READ = "";
            }
            that.showmanAlreadyExists().then(function (existing) {
                if (existing) {
                    that._updateShowman(tempShowmanObject);
                } else {
                    that._insertShowman(tempShowmanObject);
                }
            }).catch(function () {
            });
        };

        ShowmanTabViewModel.prototype.showmanAlreadyExists = function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                that.loadShowman(resolve, reject);
            });
        };

        ShowmanTabViewModel.prototype.loadShowman = function (successCb) {
            var that = this;

            var onDataFetched = function (data) {
                if (successCb !== undefined) {
                    if (data[0].showman && data[0].showman.length > 0) {
                        successCb(true);
                    } else {
                        successCb(false);
                    }
                }
            };

            var onError = function (err) {
                this.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_LOAD_DATA_ORDER_DETAIL_SHOWNMAN_TEXT")));
            };

            Promise.all([this._fetchShowmanForOperation()]).then(onDataFetched.bind(this)).catch(onError.bind(this));

        };

        ShowmanTabViewModel.prototype._fetchShowmanForOperation = function () {
            return this._requestHelper.fetch({
                id: "showman",
                statement:
                    "SELECT * FROM ZPMC_SHOWM_BILLING WHERE ORDERNO= '@orderId' AND OPERATION= '@operation'"
            }, {
                orderId: this.getOrder().ORDERID,
                operation: this.getOrder().getOrderOperations().ACTIVITY
            });
        };

        // Disable / Delete

        ShowmanTabViewModel.prototype.deleteShowmanEntry = function () {
            var that = this;
            var showmanObject = this.getShowmanEntry();

            // For deleting the ACTION_FLAG has to be set to N
            that.showmanAlreadyExists().then(function (existing) {
                if (existing) {
                    that._deleteShowman(showmanObject);
                }
            }).catch(function () {
            });

        };

        ShowmanTabViewModel.prototype.resetData = function () {
            this._needsReload = true;
            this.setOrder(null);
        };

        ShowmanTabViewModel.prototype.getValueStateModel = function () {
            if (!this.valueStateModel) {
                this.valueStateModel = new JSONModel({
                    ZRESULT_READING: sap.ui.core.ValueState.None
                });
            }

            return this.valueStateModel;
        };

        ShowmanTabViewModel.prototype.resetValueStateModel = function () {
            if (!this.valueStateModel) {
                return;
            }

            this.setProperty("/view/errorMessages", []);
        };

        ShowmanTabViewModel.prototype.getNewAttachmentObject = function (fileName, fileExt, fileSize, folderPath) {

            var that = this;
            var attachmentScenario = this._component.getAttachmentScenario();
            var attachment = null;

            if (attachmentScenario === "DMS") {
                attachment = new DMSAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(that._component.i18n_core.getText("ORDER_TABNAME"));
                attachment.setObjectKey(that.getOrder().getOrderId());
                attachment.setAttachmentHeader(that._component.i18n_core.getText("ORDER_TABNAME") + ' - ' + this._ORDER.SHORT_TEXT.toUpperCase());
                attachment.setUsername(that.getUserInformation().userName.toUpperCase());
                attachment.setDocumentPart("000");
                attachment.setDocumentType("DAF");
                attachment.setDocumentVersion("00");
                attachment.DISPLAY_TABNAME = that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                attachment.setDescription(fileName);
            }

            return attachment;
        };

        ShowmanTabViewModel.prototype.saveUserAttachment = function (attachment, fnCallback) {
            var that = this;
            var onSuccess = function () {
                fnCallback();
                that.refresh();
            };

            if (!this.canAttachFiles()) {
                this.executeErrorCallback(new Error(this._component.i18n.getText("NOTIF_NO_REFERENCE")));
                return;
            }

            try {
                attachment.insert(false, function (mobileId) {
                    attachment.MOBILE_ID = mobileId;
                    that._requestHelper.executeStatementsAndCommit([], onSuccess,
                        that.executeErrorCallback.bind(this, new Error(that._component.i18n.getText("ERROR_NOTIF_UPDATE"))));
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_ATTACHMENT_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        ShowmanTabViewModel.prototype.constructor = ShowmanTabViewModel;

        return ShowmanTabViewModel;
    });
