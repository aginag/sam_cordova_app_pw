sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/helpers/formatter",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/Order",
        "SAMMobile/models/dataObjects/OrderOperation",
        "SAMMobile/models/dataObjects/NotificationHeader",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/overview/OverviewTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/description/DescriptionTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/operations/OperationsTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/material/MaterialTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/objects/ObjectsTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/attachments/AttachmentTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/inspection/InspectionTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/checkList/CheckListViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/minmax/MinMaxTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/TimeTabViewModel",
        "SAMMobile/components/WorkOrders/controller/workOrders/detail/showman/ShowmanTabViewModel",
        "SAMMobile/models/dataObjects/OrderUserStatus",
        "SAMMobile/models/dataObjects/Showman",
        "SAMMobile/models/dataObjects/ZPWAttachment",
        "SAMMobile/helpers/timeHelper",
        "SAMMobile/models/dataObjects/TimeConfirmation"],

    function (JSONModel,
              ViewModel,
              Validator,
              formatter,
              Formatter,
              GenericSelectDialog,
              Order,
              OrderOperation,
              NotificationHeader,
              OverviewTabViewModel,
              DescriptionTabViewModel,
              OperationsTabViewModel,
              MaterialTabViewModel,
              ObjectsTabViewModel,
              AttachmentTabViewModel,
              InspectionTabViewModel,
              CheckListViewModel,
              MinMaxTabViewModel,
              TimeTabViewModel,
              ShowmanTabViewModel,
              OrderUserStatus,
              Showman,
              ZPWAttachment,
              TimeHelper,
              TimeConfirmation) {
        "use strict";

        // constructor
        function WorkOrderDetailViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this.orderUserStatus = null;
            this.scenario = this._component.scenario;
            this._formatter = new Formatter(this._component);
            this._stopwatchManager = this._component.stopwatchManager;
            this._ORDERID = "";
            this._ACTIVITY = "";
            this._PERS_NO = "";
            this._FUNCLOC_SUFFIX = "-020-UM";
            this._CODE_GROUP_TO_EXCLUDE = "PMC00005";
            this._CODE = "A001";
            this.meterReplacementInfo = this.scenario.getMeterReplacementInfo();
            this.setProperty("/view/isObjectsVisible", false);
            this.setProperty("/view/meterReplacementVisible", this.meterReplacementInfo.visible);

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        WorkOrderDetailViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        order: null,
                        nbLongtext: 0,
                        nbOperations: 0,
                        nbTimeConfirmations: 0,
                        nbChecklists: 0,
                        nbMinMax: 0,
                        counterSAMObjects: 0,
                        customAttachments: [],
                        view: {
                            busy: false,
                            selectedTabKey: "overview",
                            viewOnly: false,
                            meterReplacementVisible: false,
                            statusButtons: [],
                            isMinMaxVisible: false,
                            isChecklistVisible: false
                        }
                    };
                }
            },

            getNotifNo: {
                value: function () {
                    return this._NOTIFIACTIONNO;
                }
            },

            getOrderId: {
                value: function () {
                    return this._ORDERID;
                }
            },

            setOrderId: {
                value: function (orderId, operId, persNo) {
                    this._ORDERID = orderId;
                    this._ACTIVITY = operId;
                    this._PERS_NO = persNo;
                    this.initTabModels(orderId, this._requestHelper, this._globalViewModel, this._component);
                }
            },

            setOrder: {
                value: function (order) {
                    if (order) {

                        order._IS_HISTORIC = order.HISTORIC != "";
                        order._IS_EDITABLE = order._IS_EDITABLE != null ? order._IS_EDITABLE : true;
                        this.setStatusButtonsForStatus(order.OPERATION_USER_STATUS, order.OPERATION_STATUS_PROFILE);
                    }
                    this.setProperty("/order", order);
                    this.setOrderOnTabModels(order);
                }
            },

            setEditable: {
                value: function (bEditable) {
                    this.setProperty("/order/_IS_EDITABLE", bEditable);
                }
            },


            getOrder: {
                value: function () {
                    return this.getProperty("/order");
                }
            },

            getOperationStatusProfileForOrder: {
                value: function () {
                    return this.getOrder().OPERATION_STATUS_PROFILE;
                }
            },

            getStatusHierarchy: {
                value: function () {
                    return this._component.statusHierarchy;
                }
            },

            getMPoints: {
                value: function () {
                    return {
                        besZustnd: this.BES_ZUSTND,
                        aiZustnd: this.AI_ZUSTND,
                        schaltzeit: this.SCHALTZEIT,
                        widerstand: this.WIDERSTAND,
                        gasOelAnalyse: this.GAS_OEL_ANALYSE,
                        oelAnalyse: this.OEL_ANALYSE,
                        oelTendenz: this.OEL_TENDENZ
                    };
                }
            },


            getQueryParams: {
                value: function () {
                    return {
                        orderId: this._ORDERID,
                        operId: this._ACTIVITY,
                        spras: this.getLanguage(),
                        persNo: this._PERS_NO
                    };
                }
            },

            getHeaderText: {
                value: function () {
                    return this.getOrder()._IS_HISTORIC ? "HISTORIC - " + formatter.removeLeadingZeros(this._ORDERID) + "/" + formatter.removeLeadingZeros(this._ACTIVITY) : formatter.removeLeadingZeros(this._ORDERID) + "/" + formatter.removeLeadingZeros(this._ACTIVITY);
                }
            },

            setSelectedTabKey: {
                value: function (key) {
                    this.setProperty("/view/selectedTabKey", key);
                }
            },

            setStatusButtonsForStatus: {
                value: function (status, statusProfile) {
                    this.setProperty("/view/statusButtons", this.getStatusHierarchy().getOptionsForStatus(status, statusProfile));
                }
            },

            statusIsReject: {
                value: function (status) {
                    return status == this.orderUserStatus.reject.status;
                }
            },

            statusIsComplete: {
                value: function (status) {
                    return status == this.orderUserStatus.complete.status;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            },

            _getReturnToListError: {
                value: function (message) {
                    var error = new Error(message);
                    error.name = "RETURN_TO_LIST";

                    return error;
                }
            }
        });

        WorkOrderDetailViewModel.prototype.orderUserStatus = null;

        WorkOrderDetailViewModel.prototype.tabs = {
            overview: {
                actionName: "overview",
                routeName: "workOrderDetailOverview",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new OverviewTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            description: {
                actionName: "description",
                routeName: "workOrderDetailDescription",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new DescriptionTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            operations: {
                actionName: "operations",
                routeName: "workOrderDetailOperations",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new OperationsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            objects: {
                actionName: "objects",
                routeName: "workOrderDetailObjects",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new ObjectsTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            material: {
                actionName: "material",
                routeName: "workOrderDetailMaterial",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new MaterialTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            time: {
                actionName: "time",
                routeName: "workOrderDetailTime",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new TimeTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            checklist: {
                actionName: "checklist",
                routeName: "workOrderCheckList",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new CheckListViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            minmax: {
                actionName: "minmax",
                routeName: "workOrderDetailMinMax",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new MinMaxTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },


            showman: {
                actionName: "showman",
                routeName: "workOrderDetailShowman",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new ShowmanTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            },

            attachments: {
                actionName: "attachments",
                routeName: "workOrderDetailAttachments",
                model: null,
                initModel: function (orderId, reqHelper, globalViewModel, component, errorCallback) {
                    if (!this.model) {
                        this.model = new AttachmentTabViewModel(reqHelper, globalViewModel, component);
                        this.model.setErrorCallback(errorCallback);
                    }
                    this.model.setOrderId(orderId);
                }
            }
        };

        WorkOrderDetailViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        WorkOrderDetailViewModel.prototype.getTabRouteName = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            return tabInfo.routeName;
        };

        WorkOrderDetailViewModel.prototype.navigateToLocation = function (oEvent, customLocation) {
            var that = this;
            var location = customLocation ? customLocation : this.getOrder().getLocation();

            if (!location) {
                this.executeErrorCallback(new Error("Could not start navigation: No GPS data maintained."));
                return;
            }

            launchnavigator.isAppAvailable(launchnavigator.APP.HERE_MAPS, function (isAvailable) {
                var app;
                if (isAvailable) {
                    that.startNavigation(location, launchnavigator.APP.HERE_MAPS);
                } else {
                    var dialog = that._component.rootComponent.navigator.getSelectionDialog(function (oEvent) {
                        var selectedItem = oEvent.getParameter("listItem");
                        var app = selectedItem.getBindingContext("undefined").getObject().app;
                        that.startNavigation(location, app);
                        dialog.close();
                    });

                    dialog.open();
                }
            });
        };

        WorkOrderDetailViewModel.prototype.startNavigation = function (address, app) {
            this._component.rootComponent.navigator.navTo(address, function () {
            }, function () {
            }, app);
        };

        WorkOrderDetailViewModel.prototype.setOrderStatus = function (status, statusProfile, successCb) {
            var that = this;
            var load_error = function (err) {
                that.executeErrorCallback(err);
            };

            var order = this.getOrder();
            var statusMl = this.getUserStatusMl(status, statusProfile);
            if (!statusMl) {
                load_error(new Error("UserStatusMl not found"));
                return;
            }

            this.setBusy(true);
            order.setOperationsUserStatus(statusMl, function () {
                // Check if statusMl.USER_STATUS needs stop watch action
                var watchStopped = false;
                order.OPERATION_USER_STATUS = statusMl.USER_STATUS;
                order.OPERATION_USER_STATUS_CODE = statusMl.USER_STATUS_CODE;
                order.OPERATION_USER_STATUS_TEXT = statusMl.USER_STATUS_DESC;

                if (status == that.orderUserStatus.complete.status || status == that.orderUserStatus.reject.status || (statusMl.USER_STATUS_CODE.startsWith("RE") && !statusMl.USER_STATUS_CODE.startsWith("REC"))) {
                    that.setEditable(false);
                }

                that.setStatusButtonsForStatus(order.OPERATION_USER_STATUS, order.OPERATION_STATUS_PROFILE);
                that.setBusy(false);
                that.refresh();
                successCb(that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE), watchStopped);
            }, load_error);
        };

        WorkOrderDetailViewModel.prototype.setOperationStatus = function (status, statusProfile, bSetToDone, successCb) {
            var that = this;

            var operation = this.getOrder().getOrderOperations();
            /**Action flag of userstatus is changing wrongly to U after running func getUserStatusMl
             * workaround to avoid this behaviour and set the correct action flag after func
             **/

            var tempOperationUserStatus = operation.OrderUserStatus.find(x => x.STATUS == 'E0002');
            var statusMl = this.getUserStatusMl(status, statusProfile);
            if (typeof tempOperationUserStatus !== 'undefined') {
                statusMl.ACTION_FLAG = tempOperationUserStatus.ACTION_FLAG;
            }

            if (!statusMl) {
                this.executeErrorCallback(new Error(this._component.getModel("i18n").getProperty("userStatusNotFound")));
            }

            var fnSuccessUserStatus = function () {

                that.handleStopWatch(statusMl, function (oWatch) {

                    that.setBusy(false);
                    that.refresh();

                    successCb(that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE), oWatch.timeEntry);
                }.bind(this));
            };

            this.setBusy(true);

            if (bSetToDone) {
                operation.changeUserStatus(
                    statusMl,
                    false,
                    fnSuccessUserStatus.bind(this),
                    this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                );
            } else {
                operation.setUserStatusToInactive(
                    statusMl,
                    false,
                    fnSuccessUserStatus.bind(this),
                    this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                );
            }
        };

        WorkOrderDetailViewModel.prototype.handleStopWatch = function (statusMl, completeCb) {

            var that = this;
            var statusObject = this.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE);
            var order = this.getOrder();

            if (!statusObject) {
                completeCb({timeEntry: null});
                return;
            }

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var addTimeEntryToOrder = function (entry) {
                if (!entry) {
                    // No watch stopped
                    completeCb({timeEntry: null});
                    return;
                }

                // Watch stopped
                completeCb({timeEntry: entry});
            };

            if (statusObject.startsStopwatch()) {
                // start a stop watch
                // TODO SET CORRECT ACTIVITY TYPE HERE
                this._stopwatchManager.start(order, function () {
                    that.stopOtherTimers(function (timeEntry) {
                        addTimeEntryToOrder(timeEntry);
                    });
                }, onError);
            } else {
                this._stopwatchManager.stop(order, function (timeEntry) {
                    addTimeEntryToOrder(timeEntry);
                }, onError);
            }
        };

        WorkOrderDetailViewModel.prototype.handleStopWatchTeamMember = function (completeCb) {
            var that = this;
            var order = this.getOrder();

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var addTimeEntryToOrder = function (entry, statusObject) {
                if (!entry) {
                    // No watch stopped
                    completeCb(entry, null);
                    return;
                }

                if (order.isTimeEntriesLoaded()) {
                    if(order.ORDERID == entry.ORDERID)
                    {
                        order.addTimeEntry(entry);
                    }
                    that.tabs.time.model.refresh();
                    that.refresh();
                }

                // Watch stopped
                completeCb(entry, statusObject);
            };

            if(order.WATCH_ICON){
                that._stopwatchManager.start(order, function () {

                    that.stopOtherTimers(function (timeEntry, statusObject) {
                        addTimeEntryToOrder(timeEntry, statusObject);
                    });
                }, onError);
            }else{
                that._stopwatchManager.stop(order, function (timeEntry) {
                    addTimeEntryToOrder(timeEntry, null);
                }, onError);
            }
        };

        WorkOrderDetailViewModel.prototype.updateTimeConfirmation = function (timeEntry, successCb) {
            var that = this;

            var order = this.getOrder();
            var onSuccess = function (timeEntry) {
                if (order.ORDERID == timeEntry.ORDERID) {
                    var oNewTimeEntry = new TimeConfirmation(timeEntry, that._requestHelper, that);
                    that.tabs.time.model.insertToModelPath(oNewTimeEntry, "/timeEntries");
                    that.tabs.time.model.insertToModelPath(oNewTimeEntry, "/allTimeEntries");
                    that.tabs.time.model.refresh();
                    that.loadAllCounters();
                }
                successCb();
            };

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var order = new Order({
                ORDERID: timeEntry.ORDERID,
                OPERATION_ACTIVITY: timeEntry.ACTIVITY
            }, this._requestHelper, this);
            order.setPersNo(this.getUserInformation().personnelNumber);

            var aOverlappingTimeConfirmations = TimeHelper.getOverlappingTimeConfirmation(timeEntry, this.getProperty("/allTimeEntries"), this.getLanguage());
            if (aOverlappingTimeConfirmations && aOverlappingTimeConfirmations.length > 0) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_OVERLAPPING_TIMES_WARNING_TEXT")));
                return;
            }

            this._handleKeyValueFields(timeEntry);

                try {
                    timeEntry.insert(false, function (mobileId) {
                        timeEntry.MOBILE_ID = mobileId;

                        onSuccess(timeEntry);
                    }, onError.bind(this, new Error(this._component.i18n.getText("ERROR_COMPONENT_UPDATE"))));
                } catch (err) {
                    this.executeErrorCallback(err);
                }
        };

        WorkOrderDetailViewModel.prototype.loadAMObjList = function (successCb) {
            var that = this;
            var loadAMObjListSuccess = function (data) {
                for(var i = 0; i < data.AM_ObjList.length; i++) {
                    if (that.oData.order.PMACTTYPE === data.AM_ObjList[i].ILA ) {
                        that.setProperty("/view/isObjectsVisible", true);
                    }
                }
            };

            var loadAMObjListError = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_TIME_TAB_TEXT")));
            };

            that._requestHelper.getData(["AM_ObjList"],
                { },
                loadAMObjListSuccess,
                loadAMObjListError,
                true,
            );
        };

        WorkOrderDetailViewModel.prototype.loadAllTimeEntries = function (successCb) {
            var that = this;
            var load_timeConfirmation_success = function (data) {
                var aData = data.map(function (oData) {
                    return new TimeConfirmation(oData, that._requestHelper, that);
                });

                that.setProperty("/allTimeEntries", aData);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_timeConfirmation_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_TIME_TAB_TEXT")));
            };

            that._requestHelper.getData(
                "TimeConfirmation",
                {persNo: that._component.globalViewModel.model.getProperty("/personnelNumber")},
                load_timeConfirmation_success,
                load_timeConfirmation_error,
                true,
                "getForUser"
            );
        };

        WorkOrderDetailViewModel.prototype._handleKeyValueFields = function (timeEntry) {

            timeEntry.K_EXP_FLAG = timeEntry.K_VALUE_ID && timeEntry.K_VALUE_ID !== "0" ? "1" : "";
            timeEntry.T_EXP_FLAG = timeEntry.T_VALUE_ID && timeEntry.T_VALUE_ID !== "0" ? "2" : "";
            timeEntry.B_EXP_FLAG = timeEntry.B_VALUE_ID && timeEntry.B_VALUE_ID !== "0" ? "3" : "";

            if (!timeEntry.T_EXP_FLAG) {
                this.viewModel.executeErrorCallback(new Error(this.component.getModel("i18n_core").getResourceBundle().getText("TIME_CONF_TRAVEL_COST_VALUE_STATE_ERROR")));
            }
        };

        WorkOrderDetailViewModel.prototype.stopOtherTimers = function (successCb) {
            var that = this;
            var watches = this._stopwatchManager.watches;
            var order = this.getOrder();
            var orderID;
            var activity;
            var whichUser;

            var foundWatches = watches.filter(function (w) {
                if(w.orderId !== order.ORDERID){
                    return true;
                }else if(w.activity !== order.OPERATION_ACTIVITY){
                    return true;
                }
                return false;
            });

            if (foundWatches.length > 0) {
                orderID = foundWatches[0].orderId;
                activity = foundWatches[0].activity;
                whichUser = foundWatches[0].whichUser;

                var load_success = function(timeEntry, statusObject){
                    successCb(timeEntry, statusObject);
                };

                var onError = function (err) {
                    that.executeErrorCallback(err);
                };

                Promise.all([this.loadRunningWatches(orderID, activity)]).then(function (result) {
                    if(whichUser == "1"){
                        var operation = new OrderOperation(result[0].StopWatchOperationDetails[0], that._requestHelper, that);

                        that._stopwatchManager.stop(operation, function(timeEntry){
                            load_success(timeEntry, null);
                        }, onError);
                    }else{

                    var operation = new OrderOperation(result[0].StopWatchOperationDetails[0], that._requestHelper, that);

                        var operationUserStatus = result[0].StopWatchoperUserStatus.filter(function (userStatus) {
                            return userStatus.OBJNR === operation.OBJECT_NO && userStatus.ORDERID === operation.ORDERID;
                        });

                        var aOperationUserStatus = operationUserStatus.map(function (oOperationUserStatus) {
                        return new OrderUserStatus(oOperationUserStatus, that._requestHelper, that);
                    });

                        operation.setUserStatus(aOperationUserStatus);
                    aOperationUserStatus.forEach(function (userStatus) {
                        if (userStatus.isActive() && userStatus.STATUS == "E0013") {

                                var statusMl = that.getUserStatusMl("E0012", operation.OPERATION_STATUS_PROFILE);

                                var fnSuccessUserStatus = function () {
                                    that._stopwatchManager.stop(operation, function(timeEntry){
                                        load_success(timeEntry, that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE));
                                    }, onError);
                                };

                            operation.changeUserStatus(
                                statusMl,
                                false,
                                        fnSuccessUserStatus.bind(that),
                                that.executeErrorCallback.bind(that, new Error(that._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                            );
                        }
                    });
                    }

                }).catch(function (err) {
                    onError(err.msg);
                });
            } else {
                successCb(null, null);
            }
        };

        WorkOrderDetailViewModel.prototype.loadRunningWatches = function (orderID, activity) {
            var that = this;
            return new Promise(function (resolve, reject) {
                that._requestHelper.getData(["StopWatchOperationDetails", "StopWatchoperUserStatus"], {
                    language: that._component.globalViewModel.getActiveUILanguage(),
                    orderId: orderID,
                    activity: activity
                }, resolve, reject);
            });
        };

        WorkOrderDetailViewModel.prototype.setNewOrderData = function (data) {
            var that = this;
            var order = new Order(data[0], this._requestHelper, this);
            order.setPersNo(this._PERS_NO);
            order.RejectVisible = false;
            order.CompleteVisible = false;
            order.CancelRejectVisible = false;
            order.SetInProgressVisible = false;
            order.ToolbarVisible = false;
            order.WatchEnabled = false;
            order.ZFIX_visible = false;
            order.rejectStatusAvailable = false;

            var orderOperation = new OrderOperation({
                MOBILE_ID: data[0].MOBILE_ID,
                OBJECT_NO: data[0].OBJECT_NO,
                ORDERID: data[0].ORDERID,
                ACTIVITY: data[0].OPERATION_ACTIVITY,
                ACTION_FLAG: data[0].ACTION_FLAG,
                TERMIN_START: data[0].TERMIN_START,
                TERMIN_END: data[0].TERMIN_END,
                ACTTYPE: data[0].ACTTYPE
            }, this._requestHelper, this);

            var notification = new NotificationHeader({
                MOBILE_ID: data[0].NOTIF_MOBILE_ID,
                OBJECT_NO: data[0].NOTIF_OBJECT_NO,
                NOTIF_NO: data[0].NOTIF_NO,
                ACTION_FLAG: data[0].NOTIF_ACTION_FLAG,
                XA_STAT_PROF: data[0].NOTIF_STATUS_PROFILE,
                SHORT_TEXT: data[0].NOTIFICATION_DESCRIPTION
            }, this._requestHelper, this);

            var userStatusPrioAvailableAndSynced = false;


            var onLoadUserStatusesSuccess = function (data) {
                var watchCounter = 0;
                var doneStatus = false;
                data.forEach(function (userStatus) {

                    if (userStatus.INACT === '') {
                        switch (userStatus.STATUS) {
                            case "E0002": //done
                                order.SetInProgressVisible = true;
                                doneStatus = true;
                                break;
                            case "E0003": // prio
                                if (userStatus.ACTION_FLAG === "N" || userStatus.ACTION_FLAG === "C") {
                                    order.CancelRejectVisible = true;
                                } else {
                                    userStatusPrioAvailableAndSynced = true;
                                }
                                order.rejectStatusAvailable = true;
                                break;
                            case "E0005": // down
                                order.RejectVisible = true;
                                order.CompleteVisible = true;
                                break;
                            case "E0018": // zfix
                                order.ZFIX_visible = true;
                                break;

                            case "E0012": // stop
                                order.WatchEnabled = true;
                                order.WATCH_ICON = true;
                                order.startStopstatus = "E0013";
                                watchCounter++;
                                break;

                            case "E0013": // start
                                order.WatchEnabled = true;
                                order.WATCH_ICON = false;
                                order.startStopstatus = "E0012";
                                watchCounter++;
                                break;
                        }
                    }
                });

                if (userStatusPrioAvailableAndSynced || order.CancelRejectVisible || order.SetInProgressVisible) {
                    order.RejectVisible = false;

                    // In case the operation is completed or rejected (no matter if synced or not), all input fields / data manipulation shall be disabled
                    order._IS_EDITABLE = false;
                }

                if (order.SetInProgressVisible || order.CancelRejectVisible) {
                    order.CompleteVisible = false;

                    // In case the operation is completed or rejected (no matter if synced or not), all input fields / data manipulation shall be disabled
                    order._IS_EDITABLE = false;
                }

                if (!order.WatchEnabled) {
                    var startFlag = false;
                    var stopFlag = false;
                    var statusArr = that.getStatusHierarchy().status;

                    statusArr.forEach(function (status) {
                        if (status.statusProfile == order.OPERATION_STATUS_PROFILE && status.status == "INBE") {
                            startFlag = true;
                        } else if (status.statusProfile == order.OPERATION_STATUS_PROFILE && status.status == "STOP") {
                            stopFlag = true;
                        }
                    });

                    if (startFlag && stopFlag) {
                        order.WatchEnabled = true;
                        order.startStopstatus = "E0013";
                    } else {
                        order.WatchEnabled = false;
                    }
                    order.WATCH_ICON = true;
                }

                if (watchCounter == 2) {
                    order.WatchEnabled = true;
                    order.WATCH_ICON = true;
                    order.startStopstatus = "E0013";
                }

                //For operation assigned to personal value should be "0";
                //For operation assigned to team member value should be "1";
                //For operation assigned to team lead value should be "2";
                order.whichUser = "0";
                // Check if the operation is link to a TEAM
                if (order.TEAM_GUID) {
                    order.whichUser = "1";
                    //Check if the user is the teamLead on this operation
                    that.getUserInformation().teams.forEach(function (oTeam) {
                        //if user team correspond to the Operation Team
                        if (oTeam.teamGuid === order.TEAM_GUID) {
                            if (!oTeam.isTeamLead) {
                                //user is just member of the operation Team so i cannot Complete or reject the operation
                                order.CompleteVisible = false;
                                order.RejectVisible = false;
                        		if(doneStatus && order.ACTION_FLAG === 'U'){
                                    order.SetInProgressVisible = false;
                                }
                        	}else{
                                order.whichUser = "2";
                            }
                        }
                    });
                }

                //If operation is asisigned to team member then check if any timer is running.
                //Here timer user status of operattion does not matter
                if( order.whichUser == "1"){
                    var runningWatch = that._stopwatchManager.getStopwatchForOrderId(order);
                    order.WatchEnabled = true;
                    if(runningWatch){
                        order.WATCH_ICON = false;
                    }else{
                        order.WATCH_ICON = true;
                    }
                }

                if(!order.WATCH_ICON){
                    order.timeStartedBtnDisabled = false;
                }else{
                    order.timeStartedBtnDisabled = true;
                }

                order.ToolbarVisible = order.RejectVisible || order.CompleteVisible || order.CancelRejectVisible || order.SetInProgressVisible || order.WatchEnabled;

                that.setOrder(order);
                that.tabs.overview.model.setDataFromOrder(that.getOrder());
            };

            var onLoadTeamSuccess = function (data) {

                var team = {
                    name: "",
                    start: "",
                    end: "",
                    lead: "",
                    members: []
                };

                if (data.teamInfo !== undefined && data.teamInfo.length > 0) {
                    team.name = data.teamInfo[0].TEAM_NAME;
                    team.start = moment(data.teamInfo[0].TEAM_BEG_DATUM).format("DD.MM.YYYY") + " " + moment(data.teamInfo[0].TEAM_BEG_UZEIT).format("HH:mm");
                    team.end = moment(data.teamInfo[0].TEAM_END_DATUM).format("DD.MM.YYYY") + " " + moment(data.teamInfo[0].TEAM_END_UZEIT).format("HH:mm");
                    team.TEAM_BEG_DATUM = data.teamInfo[0].TEAM_BEG_DATUM;
                    team.TEAM_BEG_UZEIT = data.teamInfo[0].TEAM_BEG_UZEIT;
                    team.TEAM_END_DATUM = data.teamInfo[0].TEAM_END_DATUM;
                    team.TEAM_END_UZEIT = data.teamInfo[0].TEAM_END_UZEIT;
                }

                if (data.teamMembers !== undefined && data.teamMembers.length > 0) {
                    data.teamMembers.forEach(function (member) {

                        if (member.RES_FLAG_RESP) {
                            team.lead = member;
                        } else {
                            team.members.push(member);
                        }
                    });
                }

                order.team = team;
                order.setTimeForOperation();
                that.setOrder(order);
                that.tabs.overview.model.setDataFromOrder(that.getOrder());
            };

            var onLoadError = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_ORDER_DETAIL_USER_STATUS_TEXT")));
            };
            var onLoadTeamError = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_ORDER_DETAIL_TEAM_TEXT")));
            };

            // Load Operation user status
            orderOperation.loadUserStatuses(onLoadUserStatusesSuccess.bind(this), onLoadError.bind(this));

            // Load Team Information + Team Members
            if (order.TEAM_GUID) {
                this._requestHelper.getData(["teamInfo", "teamMembers"],
                    {teamGuid: order.TEAM_GUID},
                    onLoadTeamSuccess,
                    onLoadTeamError,
                    true);
            }else{
                order.setTimeForOperation();
            }

            order.setNotification(notification);
            order.setOrderOperations(orderOperation);

            this.setOrder(order);
            this.tabs.overview.model.setDataFromOrder(this.getOrder());

            var success = function (data) {
                if (data) {
                    that.setProperty("/nbLongtext", that.getProperty("/nbLongtext") + 1);
                }
            };

            this.setProperty("/nbLongtext", 0);

            // Check if OrderLongtext exist
            order.loadLongText("AUFK", success);

            // Check if Operation Longtext Exist
            orderOperation.loadLongText("AVOT", success);

            // Check if Notification Longtext Exist
            notification.loadLongText("QMEL").then(success);

        };

        WorkOrderDetailViewModel.prototype.loadData = function (successCb) {
            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {

                if (data.length == 0) {
                    //that.executeErrorCallback(that._getReturnToListError("No order found for this order id"));
                    return;
                }

                that.setNewOrderData(data);
                that.setBusy(false);
                that._needsReload = false;

                that.setProperty("/view/isMinMaxVisible", that._checkMinMaxTabVisibility());
                that.setProperty("/view/isChecklistVisible", that._checkChecklistTabVisibility());
                if (data[0]) {
                    that.setProperty("/view/isShowmanVisible", that._checkShowmanTabVisibility(data[0].PMACTTYPE));
                }


                that.loadAllTimeEntries();
                if (successCb !== undefined) {
                    successCb(that.getOrder());
                }

                that.loadAMObjList();
                if (successCb !== undefined) {
                    successCb(that.getOrder());
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(that._getReturnToListError("Error while fetching order"));
            };

            this.orderUserStatus = this.scenario.getOrderUserStatus();
            this.setBusy(true);
            this._requestHelper.getData("OrderOperationDetail", this.getQueryParams(), load_success, load_error, true);
        };

        WorkOrderDetailViewModel.prototype._checkMinMaxTabVisibility = function () {
            var sWorkCenter = this.getUserInformation().workCenterId,
                sPersNo = this.getUserInformation().personnelNumber,
                oOrder = this.getOrder();

            if ((oOrder.PMACTTYPE === "N72" || oOrder.PMACTTYPE === "N73") && (sWorkCenter === "N44" || sWorkCenter === "N45" || sWorkCenter === "N46" || sWorkCenter === "N47")) {
                if (oOrder.TEAM_PERSNO === sPersNo || oOrder.PERS_NO === sPersNo) {
                    return true;
                } else if (oOrder.TEAM_GUID) {
                    return this.getUserInformation().teams.filter(function (oTeam) {
                        return oTeam.teamGuid === oOrder.TEAM_GUID && oTeam.isTeamLead;
                    }).length > 0;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        };

        WorkOrderDetailViewModel.prototype._checkChecklistTabVisibility = function () {
            var sWorkCenter = this.getUserInformation().workCenterId,
                sPersNo = this.getUserInformation().personnelNumber,
                oOrder = this.getOrder();

            if ((oOrder.PMACTTYPE === "N72" || oOrder.PMACTTYPE === "N73") && (sWorkCenter === "N41" || sWorkCenter === "N44" || sWorkCenter === "N45" || sWorkCenter === "N46" || sWorkCenter === "N47")) {
                if (oOrder.TEAM_PERSNO === sPersNo || oOrder.PERS_NO === sPersNo) {
                    return true;
                } else if (oOrder.TEAM_GUID) {
                    return this.getUserInformation().teams.filter(function (oTeam) {
                        return oTeam.teamGuid === oOrder.TEAM_GUID && oTeam.isTeamLead;
                    }).length > 0;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        };

        WorkOrderDetailViewModel.prototype._checkShowmanTabVisibility = function (ACTTYPE) {

            var sPersNo = this.getUserInformation().personnelNumber,
                oOrder = this.getOrder();

            if (ACTTYPE === "N98") {
                if (oOrder.TEAM_PERSNO === sPersNo || oOrder.PERS_NO === sPersNo) {
                    return true;
                } else if (oOrder.TEAM_GUID) {
                    return this.getUserInformation().teams.filter(function (oTeam) {
                        return oTeam.teamGuid === oOrder.TEAM_GUID && oTeam.isTeamLead;
                    }).length > 0;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        };

        WorkOrderDetailViewModel.prototype.loadAllCounters = function (successCb) {
            var that = this;
            var load_success = function (data) {
                that.setProperty("/nbOperations", data[0].CounterOperations[0].COUNT);
                that.setProperty("/nbMaterials", data[1].CounterMaterials[0].COUNT);
                that.setProperty("/counterChecklist", data[3].CounterChecklist[0].COUNT > 0 ? "1" : "0");
                that.setProperty("/nbTimeConfirmations", data[2].CounterTimeConfirmation[0].COUNT);
                that.setProperty("/nbMinMax", data[4].CounterMinMax[0].COUNT > 0 ? "1" : "0");
                that.setProperty("/counterSAMObjects", data[5].CounterSAMObjects[0].COUNT);
                that.setProperty("/counterObjectsTab", data[6].CounterObjectsTab[0].COUNT > 0 ? "1" : "0")
                //used to hide materials tab dependend from users workcenter
                that.setProperty("/userWorkCenter", that._globalViewModel.model.oData.workCenterId);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_COUNTER_DATA_LOAD_NO_ORDER")));
            }

            this.setBusy(true);
            this.getOrder().loadCounters(load_success);
        };

        WorkOrderDetailViewModel.prototype.resetData = function () {
            this.setOrder(null);
            this._ORDERID = "";
            this._ACTIVITY = "";
            this._needsReload = true;
            this.setProperty("/view/isMinMaxVisible", false);
            this.setProperty("/view/isChecklistVisible", false);
            this.setProperty("/view/isObjectsVisible", false);
            this.resetTabModels();
        };

        WorkOrderDetailViewModel.prototype.refreshData = function (successCb) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // Mark all other tabs as needsReload = true
            for (var key in this.tabs) {
                if (key != selectedTabKey) {
                    this.tabs[key].model._needsReload = true;
                }
            }

            this.orderUserStatus = this.scenario.getOrderUserStatus();
            this.loadData(function () {
                // Publish refresh event for current active tab
                sap.ui.getCore()
                    .getEventBus()
                    .publish(tabInfo.routeName, "refreshRoute");

                if (successCb) {
                    successCb();
                }
            });
        };

        WorkOrderDetailViewModel.prototype.initTabModels = function () {
            for (var key in this.tabs) {
                this.tabs[key].initModel(this.getOrderId(), this._requestHelper, this._globalViewModel, this._component, this._errorCallback);
            }
        };

        WorkOrderDetailViewModel.prototype.setOrderOnTabModels = function (order) {
            for (var key in this.tabs) {
                var tab = this.tabs[key];
                if (tab.model) {
                    tab.model.setOrder(order);
                }
            }
        };

        WorkOrderDetailViewModel.prototype.resetTabModels = function () {
            for (var key in this.tabs) {
                this.tabs[key].model._needsReload = true;
                this.tabs[key].model.resetData();
            }
        };

        WorkOrderDetailViewModel.prototype.getTabByRouteName = function (routeName) {
            for (var key in this.tabs) {
                if (this.tabs[key].routeName == routeName) {
                    return this.tabs[key];
                }
            }

            return null;
        };

        WorkOrderDetailViewModel.prototype.checkShowmanProcessFullfilled = function (showmanData) {
            var check = false;
            var that = this;
            
            var ACTTYPE = this.getOrder().PMACTTYPE;
            if (this._checkShowmanTabVisibility(ACTTYPE)) {
                var currentShowmanObject;

                if (showmanData && showmanData.length > 0) {
                    currentShowmanObject = new Showman(showmanData[0], this._requestHelper, this);
                } else {
                    currentShowmanObject = new Showman(null, this._requestHelper, this);
                }
                currentShowmanObject.showmanProcesses.forEach(function (showDat) {

                    var orderId = that.getOrder().ORDERID;
                    var operation = that.getOrder().getOrderOperations().ACTIVITY;
                    if (showDat.ORDERNO === orderId && showDat.OPERATION === operation) {
                        if (showDat.disableSelected === true) {
                            check = true;
                        }
                    }
                });
                
                if(!check){
                    check = currentShowmanObject.checkProcessFullfiled();
                }

            } else {
                check = true;
            }
            return check;
        };

        WorkOrderDetailViewModel.prototype.validateOperationCompletion = function (oOperation, successCb) {
            var that = this;
            var ACTTYPE = this.getOrder().PMACTTYPE;

            var fnLoadSuccess = function (data) {
                var allZero = data.OperationCompletionAllZeroMinMax.filter(function(ord){
                    return ord.USER_FIELD !== "" && parseInt(ord.USER_FIELD) == "0";
                });

                var bAllCompleted = (data.OperationCompletionChecklistCount[0].COUNT === "0" || data.OperationCompletionChecklist[0].COUNT === data.OperationCompletionChecklistCount[0].COUNT)
                    && (data.OperationCompletionMinMax[0].MAX === "" || data.OperationCompletionMinMax[0].MAX > 0 || allZero.length > 0  || data.OperationCompletionNoMinMaxRequired[0].COUNT === "1")
                    && (that.checkShowmanProcessFullfilled(data.showManDataWithOperation));
                successCb(bAllCompleted);
            };

            var fnLoadError = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_VALIDATE_OPERATION_COMPLETION")));
            };

            this._requestHelper.getData(["OperationCompletionChecklist", "OperationCompletionMinMax", "OperationCompletionAllZeroMinMax", "OperationCompletionChecklistCount", "OperationCompletionNoMinMaxRequired", "showManDataWithOperation"], {
                orderId: oOperation.ORDERID,
                notifNo: this.getOrder().NOTIF_NO,
                language: this.getLanguage(),
                funcloc: this.getOrder().FUNCT_LOC,
                funclocUM: this.getOrder().FUNCT_LOC + this._FUNCLOC_SUFFIX,
                persNo: this.getUserInformation().userName,
                codeGroup: this._CODE_GROUP_TO_EXCLUDE,
                code: this._CODE,
                operation: oOperation.ACTIVITY
            }, fnLoadSuccess, fnLoadError, true);
        };

        WorkOrderDetailViewModel.prototype._hasReachedMaxDailyWork = function (timeEntry) {
            var that = this;
            var iMaxDailyWorkInMinutes = this._component.scenario.getMaxDailyWorkInMinutes();

            if (parseInt(timeEntry.ACT_WORK) > iMaxDailyWorkInMinutes) {
                return true;
            }

            var aAllTimeEntries = this.getProperty("/allTimeEntries");
            if (aAllTimeEntries.length === 0) {
                return false;
            }

            if (moment(timeEntry.START_TIME).date() === moment(timeEntry.END_TIME).date()) {
                var iSumDailyWorkInMinutes = 0;

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutes += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutes += parseInt(timeEntry.ACT_WORK);

                return iSumDailyWorkInMinutes > iMaxDailyWorkInMinutes;

            } else {
                var iSumDailyWorkInMinutesDayOne = 0;
                var iSumDailyWorkInMinutesDayTwo = 0;

                var iDurationDayOne = (60 * 24) - moment.duration(moment(timeEntry.START_TIME).diff(moment(timeEntry.START_DATE))).asMinutes();
                var iDuationDayTwo = moment.duration(moment(timeEntry.END_TIME).diff(moment(timeEntry.END_DATE))).asMinutes();

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutesDayOne += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                        iSumDailyWorkInMinutesDayTwo += that._calculateDailyWorkForDayTwo(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutesDayOne += iDurationDayOne;
                iSumDailyWorkInMinutesDayTwo += iDuationDayTwo;

                return iSumDailyWorkInMinutesDayOne > iMaxDailyWorkInMinutes || iSumDailyWorkInMinutesDayTwo > iMaxDailyWorkInMinutes;
            }

        };

        WorkOrderDetailViewModel.prototype._calculateDailyWorkForDayOne = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        WorkOrderDetailViewModel.prototype._calculateDailyWorkForDayTwo = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        WorkOrderDetailViewModel.prototype._checkDatesForSameMonthAndYear = function (timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate) {
            return moment(oTimeConfirmationStartDate).month() === moment(timeEntry.START_TIME).month() &&
                moment(oTimeConfirmationStartDate).year() === moment(timeEntry.START_TIME).year() &&
                moment(oTimeConfirmationEndDate).month() === moment(timeEntry.END_TIME).month() &&
                moment(oTimeConfirmationEndDate).year() === moment(timeEntry.END_TIME).year();
        };

        WorkOrderDetailViewModel.prototype.getTabByActionName = function (actionName) {
            for (var key in this.tabs) {
                if (this.tabs[key].actionName == actionName) {
                    return this.tabs[key];
                }
            }

            return null;
        };

        WorkOrderDetailViewModel.prototype.loadCustomAttachments = function () {
            var that = this;
            this.setBusy(true);

            return new Promise(function(resolve, reject) {
                this._requestHelper.getData("CustomAttachments", {}, function(data) {
                    const attachments = data.map(function(attachment) {
                       return new ZPWAttachment(attachment, that._requestHelper, that);
                    });

                    that.setBusy(false);
                    resolve(attachments);
                }, function(err) {
                    that.setBusy(false);
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_CUSTOM_ATTACHMENT_DATA_LOAD")));
                    reject(err);
                }, true);
            }.bind(this));
        };

        WorkOrderDetailViewModel.prototype.constructor = WorkOrderDetailViewModel;

        return WorkOrderDetailViewModel;
    });
