sap.ui.define(["sap/ui/model/json/JSONModel",
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/Validator",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/helpers/FileSystem",
        "SAMMobile/models/dataObjects/SAMObjectAttachment",
        "SAMMobile/models/dataObjects/DMSAttachment",
        "SAMMobile/models/dataObjects/ArchiveLinkAttachment",
        "SAMMobile/controller/common/GenericSelectDialog",
        "SAMMobile/models/dataObjects/ChecklistFormSave",
        "SAMMobile/helpers/checklistFormMapping"],

    function (JSONModel, ViewModel, Validator, Formatter, FileSystem, SAMObjectAttachment, DMSAttachment, ArchiveLinkAttachment, GenericSelectDialog, ChecklistFormSave, checklistFormMapping) {
        "use strict";

        // constructor
        function AttachmentTabViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);

            this._formatter = new Formatter(this._component);
            this._ORDERID = "";
            this._ORDER = null;
            this.scenario = this._component.scenario;

            this.fileSystem = new FileSystem();

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        AttachmentTabViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        attachments: [],
                        availableChecklists: [],
                        view: {
                            busy: false,
                            errorMessages: []
                        }
                    };
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this._ORDERID = orderId;
                }
            },

            setOrder: {
                value: function (order) {
                    this._ORDER = order;
                }
            },

            getOrder: {
                value: function () {
                    return this._ORDER;
                }
            },

            getAvailableChecklists: {
                value: function () {
                    return this.getProperty("/availableChecklists");
                }
            },

            canAttachFiles: {
                value: function () {
                    return this.getOrder().getOrderId() !== "";
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        AttachmentTabViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        AttachmentTabViewModel.prototype.getNewAttachmentObject = function (fileName, fileExt, fileSize, folderPath) {

            var that = this;
            var attachmentScenario = this._component.getAttachmentScenario();
            var attachment = null;

            if(attachmentScenario === "DMS"){
                attachment = new DMSAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(that._component.i18n_core.getText("ORDER_TABNAME"));
                attachment.setObjectKey(that.getOrder().getOrderId());
                attachment.setAttachmentHeader(that._component.i18n_core.getText("ORDER_TABNAME") + ' - ' + this._ORDER.SHORT_TEXT.toUpperCase());
                attachment.setUsername(that.getUserInformation().userName.toUpperCase());
                attachment.setDocumentPart("000");
                attachment.setDocumentType("DAF");
                attachment.setDocumentVersion("00");
                attachment.DISPLAY_TABNAME=that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                attachment.setDescription(fileName);
            } else if(attachmentScenario === "ALINK"){
                attachment = new ArchiveLinkAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setArchivID(that.scenario.getArchiveLinkInfo().archivID);
                attachment.setArObject(that.scenario.getArchiveLinkInfo().archiveLinkObject);
                attachment.setObjectID(that.getOrder().getOrderId());
                attachment.setTabName(that._component.i18n_core.getText("ORDER_TABNAME"));
                attachment.setUsername(that.getUserInformation().userName.toUpperCase());
                attachment.DISPLAY_TABNAME=that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                attachment.setAttachmentHeader(that._component.i18n_core.getText("ORDER_TABNAME") + ' - ' + this._ORDER.SHORT_TEXT.toUpperCase());
            } else if(attachmentScenario === "GOS"){
                attachment = new SAMObjectAttachment(null, that._requestHelper, that);
                attachment.setFilename(fileName);
                attachment.setFileExt(fileExt);
                attachment.setFilesize(fileSize);
                attachment.setFolderPath(folderPath);
                attachment.setTabName(that._component.i18n_core.getText("ORDER_TABNAME"));
                attachment.setObjectKey(that.getOrder().getOrderId());
                attachment.DISPLAY_TABNAME=that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                attachment.setAttachmentHeader(that._component.i18n_core.getText("ORDER_TABNAME") + ' - ' + that._ORDER.SHORT_TEXT.toUpperCase());
            }

            return attachment;
        };

        AttachmentTabViewModel.prototype.getHtmlForChecklist = function (checklistId, successCb, errorCb) {
            var that = this;
            var checklists = this.getAvailableChecklists();

            for (var i = 0; i < checklists.length; i++) {
                if (checklists[i].ID == checklistId) {
                    successCb(checklists[i].HTML);
                    return;
                }
            }

            errorCb(new Error(that._component.i18n.getText("WORKORDERS_CHECKLIST_NOT_FOUND")));
        };

        AttachmentTabViewModel.prototype.saveUserAttachment = function (attachment, fnCallback) {
            var that = this;

            if (!this.canAttachFiles()) {
                this.executeErrorCallback(new Error(this._component.i18n.getText("NOTIF_NO_REFERENCE")));
                return;
            }

            try {
                attachment.insert(false, function (mobileId) {
                    attachment.MOBILE_ID = mobileId;
                    that.insertToModelPath(attachment, "/attachments");
                    fnCallback();
                    that.refresh();
                }, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_ATTACHMENT_INSERT"))));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        AttachmentTabViewModel.prototype.updateUserAttachment = function (attachment) {
            var that = this;

            if (attachment.ACTION_FLAG !== "N") {
                this.executeErrorCallback(new Error(this._component.i18n.getText("ATTACHMENT_DELETE_ONLY_WHEN_ACTION_FLAG_N")));
                return;
            }

            try {
                attachment.update(false, function () {
                    that.refresh();
                }, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        AttachmentTabViewModel.prototype.deleteUserAttachment = function (attachment) {
            var that = this;

            attachment.delete(false, function () {
                that.removeFromModelPath(attachment, "/attachments");
            }, function () {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_ATTACHMENT_DELETE")));
            });
        };

        AttachmentTabViewModel.prototype.setModelData = function (data) {
            var that = this;
            var newAttachmentArray = [];

            var attachmentButtonsNewPhoto = this._component.getAttachmentButtonOrderNewPhoto();
            this.setProperty("/showOtherAttachmentPhotoButtons", attachmentButtonsNewPhoto);

            var attachmentButtonsNewAudio = this._component.getAttachmentButtonOrderNewAudio();
            this.setProperty("/showOtherAttachmentAudioButtons", attachmentButtonsNewAudio);

            var attachmentButtonsNewVideo = this._component.getAttachmentButtonOrderNewVideo();
            this.setProperty("/showOtherAttachmentVideoButtons", attachmentButtonsNewVideo);

            var attachmentButtonsNewFile = this._component.getAttachmentButtonOrderNewFile();
            this.setProperty("/showOtherAttachmentFileButtons", attachmentButtonsNewFile);

            if (data.SAMObjectAttachments) {
                data.SAMObjectAttachments.forEach(function (samObjectAttachment) {
                    var attObject = new SAMObjectAttachment(samObjectAttachment, that._requestHelper, that);
                    if(attObject.USER_FIELD){
                        attObject.FILEEXT = "url";
                    }
                    newAttachmentArray.push(attObject);
                });
            }

            if (data.DMSAttachments) {
                data.DMSAttachments.forEach(function (samObjectAttachment) {
                    var attObject = new DMSAttachment(samObjectAttachment, that._requestHelper, that);
                    newAttachmentArray.push(attObject);
                });
            }

            if (data.ArchiveLinkAttachments) {
                data.ArchiveLinkAttachments.forEach(function (samObjectAttachment) {
                    var attObject = new ArchiveLinkAttachment(samObjectAttachment, that._requestHelper, that);
                    newAttachmentArray.push(attObject);
                });
            }

            var arrData = newAttachmentArray.map(function (attObject) {

                if (attObject.ACTION_FLAG == "N") {
                    attObject.setFolderPath(that.fileSystem.filePathDoc);
                } else {
                    attObject.setFolderPath(that.fileSystem.getRootDataStorage());
                }

                switch (attObject.TABNAME) {
                    case 'Orders':
                        attObject.TABNAME = that._component.i18n_core.getText("ORDER_TABNAME");
                        attObject.DISPLAY_TABNAME = that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                        attObject.ATTACHMENTHEADER = that._component.i18n_core.getText("ORDERS_ATTACHMENT_HEADER") + ' - ' + attObject.SHORT_TEXT;
                        break;
                    case 'Equipment':
                        attObject.ATTACHMENTHEADER = that._component.i18n_core.getText("EQUIPMENT_ATTACHMENT_HEADER") + ' - ' + attObject.EQUIPMENT_DESC;
                        attObject.DISPLAY_TABNAME = that._component.i18n_core.getText("EQUIPMENT_DISPLAY_TABNAME");
                        break;
                    case 'FunctionLoc':
                        attObject.TABNAME = that._component.i18n_core.getText("FUNCLOC_TABNAME");
                        attObject.DISPLAY_TABNAME = that._component.i18n_core.getText("FUNCLOC_DISPLAY_TABNAME");
                        attObject.ATTACHMENTHEADER = that._component.i18n_core.getText("FUNCLOC_TABNAME") + ' - ' + attObject.FUNCLOC_DESC;
                        break;
                    case 'NotificationTask':
                    case 'NotificationHeader':
                        attObject.TABNAME = that._component.i18n_core.getText("NOTIFICATION_TABNAME");
                        attObject.DISPLAY_TABNAME = that._component.i18n_core.getText("NOTIFICATION_DISPLAY_TABNAME");
                        attObject.ATTACHMENTHEADER = that._component.i18n_core.getText("NOTIFICATION_ATTACHMENT_HEADER") + ' - ' + attObject.TXT_TASKCD;
                        break;
                    default:
                        attObject.DISPLAY_TABNAME = that._component.i18n_core.getText("ORDER_DISPLAY_TABNAME");
                        attObject.ATTACHMENTHEADER = that._component.i18n_core.getText("ORDERS_ATTACHMENT_HEADER") + ' - ' + attObject.SHORT_TEXT;
                }

                return attObject;
            });

            var savedForms = data.ChecklistSavedHtmlForms.map(function(savedForm) {
                return new ChecklistFormSave(savedForm, that._requestHelper, that);
            });

            arrData = arrData.concat(savedForms);

            var attachmentData = arrData.filter(function(attachment){
                return attachment.ACTION_FLAG !== "B" && attachment.ACTION_FLAG !== "A";
            });

            this.setProperty("/attachments", attachmentData);
            var checklistData = data.ChecklistHtmlForms;

            this.setProperty("/availableChecklists", checklistData);
            this.setProperty("/isAvailableChecklists", checklistData.length > 0 ? true : false);
        };

        AttachmentTabViewModel.prototype.loadData = function (successCb) {
            var that = this;

            var load_success = function (data) {
                that.setModelData(data);
                that.setBusy(false);
                that._needsReload = false;

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD")));
            };

            if (!this.getOrder()) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_TAB_DATA_LOAD_NO_ORDER")));
                return;
            }

            this.setBusy(true);
            // TODO - use actual activity type here
            this._requestHelper.getData(["SAMObjectAttachments", "DMSAttachments", "ChecklistHtmlForms", "ChecklistSavedHtmlForms"], {
                objKey: this.getOrder().getOrderId(),
                notificationKey: this.getOrder().NOTIF_NO,
                funcLocKey: this.getOrder().FUNCT_LOC,
                equipmentKey: this.getOrder().EQUIPMENT,
                activityType: this.getOrder().PMACTTYPE
            }, load_success, load_error, true);
        };

        AttachmentTabViewModel.prototype.resetData = function () {
            this.setProperty("/attachments", []);
            this.setOrder(null);
        };

        AttachmentTabViewModel.prototype.saveChecklistState = function(savedState, successCb) {
            const that = this;
            var checklistFormSave = new ChecklistFormSave(null, this._requestHelper, this);
            checklistFormSave.setHTML(savedState.document);
            checklistFormSave.setState(savedState.state);
            checklistFormSave.setFormName(savedState.formName);
            checklistFormSave.setOrderId(this.getOrder().getOrderId());

            try {
                checklistFormSave.insert(false, function(mobileId) {
                    checklistFormSave.MOBILE_ID = mobileId;

                    that.insertToModelPath(checklistFormSave, "/attachments");
                    successCb(checklistFormSave);
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        AttachmentTabViewModel.prototype.updateChecklistState = function(checklistFormSave, successCb) {
            try {
                checklistFormSave.update(false, function() {
                    successCb(checklistFormSave);
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        AttachmentTabViewModel.prototype.deleteChecklistState = function(checklistFormSave, successCb) {
            const that = this;

            try {
                checklistFormSave.delete(false, function() {
                    that.removeFromModelPath(checklistFormSave, "/attachments");
                    successCb(checklistFormSave);
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        AttachmentTabViewModel.prototype.getChecklistInfoObject = function(checklistName) {
            var order = this.getOrder();
            order._FUNCT_LOC = order.FUNCT_LOC.substring(8, 16);

            return {
                mapping: checklistFormMapping[checklistName],
                orderInfo: {
                    order: this.getOrder(),
                    orderPartner: {
                        NAME_LIST: "",
                        TEL_NUMBER: "",
                        MOBILE_NUMBER: "",
                        EMAIL_ADDR: "",
                        ADR_LAT: "",
                        ADR_LON: "",
                        ZZMAKANI: "",
                        PURCH_NO_C: "",
                        STREET: "",
                        CITY: ""
                    },
                    user: this.getUserInformation(),
                    teamData: this._TEAM_DATA
                }
            };
        };

        AttachmentTabViewModel.prototype.constructor = AttachmentTabViewModel;

        return AttachmentTabViewModel;
    });
