sap.ui.define(["sap/ui/model/json/JSONModel",
      "SAMContainer/models/ViewModel",
      "SAMMobile/helpers/Validator",
      "SAMMobile/components/WorkOrders/helpers/Formatter",
      "SAMMobile/controller/common/GenericSelectDialog",
      "SAMMobile/models/dataObjects/TimeConfirmation"],
   
   function(JSONModel, ViewModel, Validator, Formatter, GenericSelectDialog, TimeConfirmation) {
      "use strict";
      
      // constructor
      function CopyTimeEntriesViewModel(requestHelper, globalViewModel, component) {
         ViewModel.call(this, component, globalViewModel, requestHelper);
         
         this._formatter = new Formatter(this._component);
         this.valueStateModel = null;
         this._objectDataCopy = null;
         
         this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
      }
      
      CopyTimeEntriesViewModel.prototype = Object.create(ViewModel.prototype, {
         getDefaultData: {
            value: function() {
               return {
                  colleagues: null,
                  selectedColleagues: [],
                  view: {
                     busy: false,
                     edit: false,
                     errorMessages: []
                  }
               };
            }
         },
         
         setColleagues: {
            value: function(colleagues) {
               //filter out team lead
               var filteredColleagues = colleagues.filter(function(colleague) {
                  return !colleague.isTeamLead();
               });
               
               this.setProperty("/colleagues", filteredColleagues)
            }
         },
         
         getColleagues: {
           value: function() {
              return this.getProperty("/colleagues");
           }
         },
   
         setSelectedColleagues: {
            value: function(listItems) {
               var colleagues = listItems.map(function(li) {
                  return li.getBindingContext("copyTimeEntriesViewModel").getObject();
               });
               
               this.setProperty("/selectedColleagues", colleagues);
            }
         },
   
         getSelectedColleagues: {
            value: function() {
               return this.getProperty("/selectedColleagues");
            }
         },
         
         getDialogTitle: {
            value: function() {
               return this._component.getModel("i18n")
                  .getResourceBundle()
                  .getText("CopyToColleagues")
            }
         },
         
         setBusy: {
            value: function(bBusy) {
               this.setProperty("/view/busy", bBusy);
            }
         }
      });
      
      CopyTimeEntriesViewModel.prototype.onPropertyChanged = function(changeEvent, modelData) {
      
      };
      
      CopyTimeEntriesViewModel.prototype.onColleagueTableSelectionChanged = function(oEvent) {

         this.setSelectedColleagues(oEvent.getSource().getSelectedItems());
      };
      
      
      CopyTimeEntriesViewModel.prototype.constructor = CopyTimeEntriesViewModel;
      
      return CopyTimeEntriesViewModel;
   });
