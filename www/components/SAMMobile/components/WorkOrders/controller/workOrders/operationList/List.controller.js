sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/operationList/OperationListViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/list/RejectionReasonDialogViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/map/MapMarkerViewModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Text",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/reject/RejectOperationViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/EditTimeEntryViewModel"
], function (Controller,
             OperationListViewModel,
             RejectionReasonDialogViewModel,
             MapMarkerViewModel,
             formatter,
             formatterComp,
             MessageBox,
             MessageToast,
             Button,
             Dialog,
             Text,
             RejectOperationViewModel,
             EditTimeEntryViewModel) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.operationList.List", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrderOperations").attachPatternMatched(this._onRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("workOrderOperations", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.operationListViewModel = new OperationListViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.operationListViewModel.setErrorCallback(this.onViewModelError.bind(this));
            this.operationListViewModel.setList(this.byId("operationList"));

            this.rejectOperationViewModel = new RejectOperationViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.rejectOperationViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.mapMarkerViewModel = new MapMarkerViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            this.initialized = false;
            this.getView().setModel(this.operationListViewModel, "operationListViewModel");
            this._sortDialogs = {};
        },

        _onRouteMatched: function (oEvent) {
            var that = this;

            var onMasterDataLoaded = function (routeName) {
                this.globalViewModel.setComponentBackNavEnabled(false);
                this.globalViewModel.setCurrentRoute(routeName);
                this.globalViewModel.setComponentHeaderText(this.component.i18n.getText("operationListTitle"));
                this.operationListViewModel.setSelectedOperation(null);
                this.operationListViewModel.refresh(true);

                if (this.operationListViewModel.needsReload()) {
                    // In case of reload required -> make sure that all tabs are reloaded..
                    this.operationListViewModel.resetModel();
                    this.loadOperations(false);
                }
            };

            if (!this.initialized) {
                const routeName = oEvent.getParameter('name');
                setTimeout(function () {
                    that.loadMasterData(onMasterDataLoaded.bind(that, routeName));
                }, 500);
            } else {
                this.loadMasterData(onMasterDataLoaded.bind(this, oEvent.getParameter('name')));
            }

        },

        onExit: function () {
            sap.ui.getCore().getEventBus().unsubscribe("workOrderOperations", "refreshRoute", this.onRefreshRoute, this);
        },

        onRefreshRoute: function () {
            jQuery.sap.log.info("PriSec - OperationList Controller - Refresh Event triggered");
            this.loadOperations(true);
            this.operationListViewModel.refresh(true);
        },

        loadMasterData: function (successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        loadOperations: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            this.operationListViewModel.fetchCounts();

            if (bSync) {
                this.operationListViewModel.refreshData();
            } else {
                this.operationListViewModel.resetModel();
                this.operationListViewModel.fetchData(function () {
                    if (!this.initialized) {
                        this.initialized = true;
                        this.component.onAfterSync(false);
                    }
                }.bind(this));
            }
        },

        onTabSelect: function (oEvent) {
            this.operationListViewModel.onTabChanged();
        },

        onTableSearch: function (oEvt) {
            if(oEvt.getSource().getValue() && oEvt.getSource().getValue().length > 0){
                this.operationListViewModel.onSearch(oEvt.getSource().getValue());
            } else {
                this.operationListViewModel.filterList(this.operationListViewModel.getListFilter());
            }
        },

        onSortTableConfirm: function (oEvt) {
            this.operationListViewModel.onSort(oEvt);
        },

        onListGrowing: function (oEvt) {
            this.operationListViewModel.onListGrowing(oEvt);
        },

        onItemPress: function (oEvt) {
            var bindingContext = oEvt.getParameter("listItem").getBindingContext("operationListViewModel");
            var operation = bindingContext.getObject();

            this.operationListViewModel.setSelectedOperation(operation);
            this.component.routeNumber = 0;
            this.oRouter.navTo("workOrderDetailRoute", {
                id: operation.ORDERID,
                operId: operation.ACTIVITY,
                persNo: this.operationListViewModel.getUserInformation().personnelNumber ? this.operationListViewModel.getUserInformation().personnelNumber : '00000000'
            }, false);
        },

        onNavigateToLocationPressed: function () {
            var selectedOperation = this.operationListViewModel.getSelectedOperation();
            var selectedTabKey = this.operationListViewModel.getSelectedTabKey();
            this.oRouter.navTo("mapsMarkerRoute", {
                    query: {
                        equipment: selectedOperation.EQUIPMENT,
                        orderid: selectedOperation.ORDERID,
                        selectedTabKey: selectedTabKey
                    }
                }
            );
        },

        onNavToDetailPress: function (oEvt) {
            var bindingContext = oEvt.getSource().getBindingContext("operationListViewModel");
            var operation = bindingContext.getObject();

            this.oRouter.navTo("workOrderDetailRoute", {
                id: operation.ORDERID,
                operId: operation.ACTIVITY,
                persNo: this.operationListViewModel.getUserInformation().personnelNumber ? this.operationListViewModel.getUserInformation().personnelNumber : '00000000'
            }, false);
        },

        onNavToMapMarkerPress: function () {
            sap.ui.getCore().getEventBus().publish("sideNavButtonPress", "onSideNavButtonPress");

            var iTems = this.operationListViewModel.getList().getItems();
            var Orders = [];
            iTems.forEach(function (iTem) {

                if (iTem.getBindingContext("operationListViewModel").getObject().FUNCT_LOC.match(/D-N-\d\d/)) {
                    return;
                }

                var order = {
                     ORDERID: iTem.getBindingContext("operationListViewModel").getObject().ORDERID,
                     ACTIVITY: iTem.getBindingContext("operationListViewModel").getObject().ACTIVITY,
                     DESCRIPTION: iTem.getBindingContext("operationListViewModel").getObject().DESCRIPTION,
                     FUNCLOC_DESC: iTem.getBindingContext("operationListViewModel").getObject().FUNCLOC_DESC,
                     FUNCT_LOC: iTem.getBindingContext("operationListViewModel").getObject().FUNCLOC_DESC,
                     POST_CODE: iTem.getBindingContext("operationListViewModel").getObject().POST_CODE1,
                     STREET: iTem.getBindingContext("operationListViewModel").getObject().STREET,
                     COUNTRY: iTem.getBindingContext("operationListViewModel").getObject().COUNTRY,
                     CITY: iTem.getBindingContext("operationListViewModel").getObject().CITY1,
                    latitude: iTem.getBindingContext("operationListViewModel").getObject().latitude,
                    longitude: iTem.getBindingContext("operationListViewModel").getObject().longitude
                };

                Orders.push(order);

            });

            var selectedTabKey = this.operationListViewModel.getSelectedTabKey();
            this.oRouter.navTo("mapsMarkerRoute", {
                    query: {
                        selectedTabKey: selectedTabKey,
                        filteredOperation: encodeURIComponent(JSON.stringify(Orders))
                    }
                }
            );
        },

        onSortButtonPress: function (oEvt) {
            var oDialog = this._sortDialogs["sortFragment"];

            if (!oDialog) {
                oDialog = sap.ui.xmlfragment("SAMMobile.components.WorkOrders.view.workOrders.operationList.SortDialog", this);
                this._sortDialogs["sortFragment"] = oDialog;
                oDialog.setModel(this.component.getModel('i18n'), "i18n");
                oDialog.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            oDialog.open();
        },

        onInitSort: function (a, b) {

            var aDate = a.split(' ').shift();
            var bDate = b.split(' ').shift();
            var aTime = a.split(' ').pop();
            var bTime = b.split(' ').pop();

            if (this.operationListViewModel.getLanguage() === 'D') {
                var aYear = aDate.split('.')[2];
                var aMonth = aDate.split('.')[1];
                var aDay = aDate.split('.')[0];

                var bYear = bDate.split('.')[2];
                var bMonth = bDate.split('.')[1];
                var bDay = bDate.split('.')[0];
            } else {
                var aYear = aDate.split('/')[0];
                var aMonth = aDate.split('/')[1];
                var aDay = aDate.split('/')[1];

                var bYear = bDate.split('/')[0];
                var bMonth = bDate.split('/')[1];
                var bDay = bDate.split('/')[2];
            }

            var aHour = aTime.split(':').shift();
            var bHour = bTime.split(':').shift();

            var aMin = aTime.split(':').pop();
            var bMin = bTime.split(':').pop();

            // parse to Date object
            if (!aYear) {
                return 1;
            } else {
                var aDate = new Date(aYear, aMonth, aDay, aHour, aMin);
            }

            if (!bYear) {
                return -1;
            } else {
                var bDate = new Date(bYear, bMonth, bDay, bHour, bMin);
            }

            if (aDate < bDate) {
                return -1;
            }
            if (aDate > bDate) {
                return 1;
            }
            return 0;
        },

        onNavToCalendarPress: function () {
            sap.ui.getCore().getEventBus().publish("sideNavButtonPress", "onSideNavButtonPress");
            this.oRouter.navTo("workOrdersCalendar");
        },

        formatDate: function (dateString) {
            return moment(dateString).format("YYYY/MM/DD");
        },

        formatTime: function (timeString) {
            return moment(timeString).format("hh:mm");
        },

        onFixTermPress: function (oEvent) {

            var oPopover = new sap.m.Popover({
                content: [new Text({
                    text: "Fixtermin",
                    textAlign: 'Center',
                    width: '100%'
                })],
                showHeader: false,
                placement: "Bottom",
                afterClose: function () {
                    this.destroy();
                }

            });

            oPopover.openBy(oEvent.getSource());

        },

        onStatusButtonPressed: function (oEvt) {
            var that = this;

            var customData = oEvt.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error("Could not read status or status profile for pressed button"));
                return;
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");

            var onActionConfirmed = function () {
                that.operationListViewModel.setUserStatus(status, statusProfile, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }
                    var syncMode = statusObject.async ? "" : "S";

                    if (bStopwatchStopped) {
                        that._getConfirmActionDialog(that.component.i18n.getText("timeEntryCreateConfirmation"), that.component.sync.bind(that.component, syncMode, true)).open();
                        return;
                    }

                    that.component.sync(syncMode, true);
                });
            };


            if (this.operationListViewModel.statusIsReject(status)) {
                this.displayRejectionReasonSearch(oEvt);
            } else if (this.operationListViewModel.statusIsComplete(status)) {
                // get complete dialog
                this._getCompleteOrderDialog(onActionConfirmed).open();
            } else {
                this._getConfirmActionDialog(this.component.i18n.getText("changeOrderStatusConfirmation"), onActionConfirmed).open();
            }

        },

        //set Operation In progress
        onSetInProgressPress: function (oEvt) {
            var that = this;

            var operation = oEvt.getSource().getBindingContext("operationListViewModel").getObject();
            var statusProfile = operation.OPERATION_STATUS_PROFILE ? operation.OPERATION_STATUS_PROFILE : that.component.scenario.getOrderUserStatus().down.STATUS_PROFILE;

            var onActionConfirmed = function () {
                that.operationListViewModel.setUserStatus(that.component.scenario.getOrderUserStatus().done.STATUS, statusProfile, false, function (statusObject, bStopwatchStopped) {
                    MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                    sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                    that.loadOperations(true);

                    if (!statusObject || !statusObject.autoSync) {
                        return;
                    }

                });
            };

            this.operationListViewModel.setSelectedOperation(operation);
            this._getConfirmActionDialog(this.component.i18n.getText("CHANGE_ORDER_STATUS_TO_IN_PROGRESS_CONFIRMATION_TEXT"), onActionConfirmed).open();
        },

        //set Operation as Complete
        onSetCompletePress: function (oEvt) {
            var that = this;
            var operation = oEvt.getSource().getBindingContext("operationListViewModel").getObject();
            var statusProfile = operation.OPERATION_STATUS_PROFILE ? operation.OPERATION_STATUS_PROFILE : that.component.scenario.getOrderUserStatus().down.STATUS_PROFILE;

            var onActionConfirmed = function () {

                var fnCompleteOperation = function () {
                    that.operationListViewModel.setUserStatus(that.component.scenario.getOrderUserStatus().done.STATUS, statusProfile, true, function (statusObject, bStopwatchStopped) {
                        MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));

                        sap.ui.getCore().getEventBus().publish("home", "refreshCounters");
                        that.loadOperations(true);

                        if (!statusObject || !statusObject.autoSync) {
                            return;
                        }
                    });
                };

                if (operation.WORK_CNTR === 'N42' || operation.WORK_CNTR === 'N43') {
                    fnCompleteOperation();
                } else {
                    that.operationListViewModel.validateOperationCompletion(operation, function (bComplete) {
                        if (bComplete) {
                            fnCompleteOperation();
                        } else {
                            that.operationListViewModel.executeErrorCallback(new Error(that.component.i18n.getText("CHANGE_ORDER_STATUS_TO_COMPLETE_HAVING_DEPENDENCIES_ERROR_TEXT")));
                        }
                    });
                }
            };
            this.operationListViewModel.setSelectedOperation(operation);
            this._getConfirmActionDialog(this.component.i18n.getText("CHANGE_ORDER_STATUS_TO_COMPLETE_CONFIRMATION_TEXT"), onActionConfirmed).open();
        },

        displayRejectionReasonSearch: function (oEvent) {
            this.rejectionDialogViewModel = new RejectionReasonDialogViewModel(this.component.requestHelper, this.globalViewModel, this.component);

            var operationStatusProfile = this.operationListViewModel.getOperationStatusProfileForSelectedOrder();

            this.rejectionDialogViewModel.setStatusProfile(operationStatusProfile);
            this.rejectionDialogViewModel.display(oEvent, this);
        },

        onOpenRejectOperationDialogPress: function (oEvent) {
            var operation = oEvent.getSource().getBindingContext("operationListViewModel").getObject();
            this.rejectOperationViewModel.openDialog(operation, true);
            this.loadOperations(true);
        },

        _getConfirmActionDialog: function (message, executeCb) {
            var that = this;
            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: message,
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        executeCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("no"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        onListFilterChanged: function (oEvent) {
            this.operationListViewModel.filterList(oEvent.getSource().getSelectedKey());
            this.operationListViewModel.setListFilter(oEvent.getSource().getSelectedKey());
            this.operationListViewModel.onSearch(this.operationListViewModel.getProperty("/searchString"));
            this.operationListViewModel.refresh();
        },

        onStopWatchPressed: function (oEvent) {
            var that = this;
            
            var operation = oEvent.getSource().getBindingContext("operationListViewModel").getObject();

            var customData = oEvent.getSource().getCustomData();
            var status = customData.filter(function (data) {
                return data.getProperty("key") == "status";
            });

            var statusProfile = customData.filter(function (data) {
                return data.getProperty("key") == "statusProfile";
            });

            if (status.length == 0 || statusProfile.length == 0) {
                that.onViewModelError(new Error(that.component.i18n.getText("BUTTON_PROFILE_COULDNOT_READ")));
            }

            status = status[0].getProperty("value");
            statusProfile = statusProfile[0].getProperty("value");

            this.operationListViewModel.setSelectedOperation(operation);
            
            if(operation.whichUser == "1"){
                this.operationListViewModel.handleStopWatchTeamMember(function(oCreatedTimeEntry, statusObject){
                    that.loadOperations(false);
                    
                    if (oCreatedTimeEntry) {
                        that.onTimeConfirmationFinished(oCreatedTimeEntry, statusObject);
                    }
                });
            }else{
            that.operationListViewModel.setUserStatus(status, statusProfile, true, function (statusObject, oCreatedTimeEntry) {
                MessageToast.show(that.component.i18n.getText("orderStatusChangeSuccess"));
                
                if (!statusObject) {
                    return;
                }
 
                that.loadOperations(false);
                
                if (oCreatedTimeEntry) {
                    that.onTimeConfirmationFinished(oCreatedTimeEntry, statusObject);
                    return;
                }

                var syncMode = statusObject.async ? "" : "S";
                if (statusObject.autoSync) {
                    that.component.sync(syncMode, true);
                }
            });

            }
        },

        onTimeConfirmationFinished: function (oCreatedTimeEntry, statObject){
                this.syncMode;
                this.statusObject = statObject;
                if(this.statusObject){
                    this.syncMode = this.statusObject.async ? "" : "S";
                }
                var onTimeEntryAdjusted = function (updatedTimeEntry, editDialog) {
                    var updateFunction = function (updatedTimeEntry, editDialog) {
                        this.operationListViewModel.updateTimeConfirmation(updatedTimeEntry, function () {
                            MessageToast.show(this.component.i18n.getText("TIME_CONF_UPDATE_TIME_ENTRY_SUCCESS_TEXT"));
                            editDialog.close();

                            if (this.statusObject && this.statusObject.autoSync) {
                                this.component.sync(this.syncMode, true);
                            }
                        }.bind(this));
                    }.bind(this);


                    if((!updatedTimeEntry.B_VALUE_ID || updatedTimeEntry.B_VALUE_ID !== "5") && this.operationListViewModel._hasReachedMaxDailyWork(updatedTimeEntry)) {
                        MessageBox.confirm(this.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TEXT"), {
                            icon: MessageBox.Icon.INFORMATION,
                            title: this.component.getModel("i18n").getResourceBundle().getText("TIME_CONF_MAX_DAILY_WORKING_TIME_REACHED_TITLE"),
                            actions: [MessageBox.Action.YES, MessageBox.Action.NO],
                            onClose: function(oAction) {
                                if (oAction === "YES") {
                                    updateFunction(updatedTimeEntry, editDialog);
                                }
                            }
                        });
                    } else {
                        updateFunction(updatedTimeEntry, editDialog);
                    }
                };

                var onCancelPressed = function (tmeEntry, dialog) {
                    MessageBox.warning(this.component.i18n.getText("DELETE_TIMER_MESSAGE"), {
                        title: this.component.i18n_core.getText("warning"),
                        actions: [this.component.i18n_core.getText("ok"), this.component.i18n_core.getText("cancel")],
                        onClose: function(oAction) {
                            if (oAction === this.component.i18n_core.getText("ok")) {
                                MessageToast.show(this.component.i18n.getText("TIME_CONF_DELETE_TIME_ENTRY_SUCCESS_TEXT"));
                                dialog.close();
                                if (this.statusObject && this.statusObject.autoSync) {
                                    this.component.sync(this.syncMode, true);
                                }
                            }
                        }.bind(this)
                    });
                };

                this._getTimeEntryEditDialog(oCreatedTimeEntry, true, onTimeEntryAdjusted.bind(this), onCancelPressed.bind(this)).open();
        },

        onCompleteWeekPressed: function () {
            var that = this;
            this._getManualCompleteConfirmActionDialog(function () {
                that.operationListViewModel.onCompleteWeekOperations(function(operationCount){
                    that.loadOperations(true);
                    MessageToast.show(that.component.i18n.getText("COMPLETE_WEEK_SUCCESS1") + " " +operationCount + " " +that.component.i18n.getText("COMPLETE_WEEK_SUCCESS2"));
                })
            }).open();
        },


        _getManualCompleteConfirmActionDialog: function (acceptCb) {
            
            var _oConfirmationDialog = new Dialog({
                title: this.component.i18n_core.getText("Warning"),
                type: 'Message',
                state: 'Warning',
                content: new Text({
                    text: this.component.i18n.getText("COMPLETE_WEEK_MSG"),
                    textAlign: 'Center',
                    width: '100%'
                }),
                beginButton: new Button({
                    text: this.component.i18n_core.getText("yes"),
                    press: function () {
                        acceptCb();
                        this.getParent().close();
                    }
                }),
                endButton: new Button({
                    text: this.component.i18n_core.getText("cancel"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return _oConfirmationDialog;
        },

        _getTimeEntryEditDialog: function (timeEntry, bEdit, acceptCb, cancelCb) {
            var onAcceptPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");

                if (editTimeEntryViewModel.validate()) {
                    acceptCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
                }

            };

            var onCancelPressed = function () {
                var editTimeEntryViewModel = this.getParent().getModel("timeEntryModel");
                cancelCb(editTimeEntryViewModel.getTimeEntry(), this.getParent());
            };

            this.editTimeEntryViewModel = new EditTimeEntryViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.editTimeEntryViewModel.setEdit(bEdit);
            this.editTimeEntryViewModel.setTimeEntry(timeEntry);

            
            if (!this._timeEntryEditPopup) {

                if (!this._timeEntryPopover) {
                    this._timeEntryPopover = sap.ui.xmlfragment(this.createId("timeConfirmationPopoverList"), "SAMMobile.components.WorkOrders.view.common.TimeConfirmationPopover", this);
                    
                    var oStartDateTimePicker = sap.ui.core.Fragment.byId(this.createId("timeConfirmationPopoverList"), "timeconf-start-time");
                    var oEndDateTimePicker = sap.ui.core.Fragment.byId(this.createId("timeConfirmationPopoverList"), "timeconf-end-time");

                    oStartDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oStartDateTimePicker);

                    oEndDateTimePicker.addEventDelegate({
                        onAfterRendering: function(){
                            var oDateInner = this.$().find('.sapMInputBaseInner');
                            var oID = oDateInner[0].id;
                            $('#'+oID).attr("readonly", "true");
                        }}, oEndDateTimePicker);
                }

                this._timeEntryEditPopup = new Dialog({
                    stretch: sap.ui.Device.system.phone,
                    content: this._timeEntryPopover,
                    beginButton: new Button({
                        text: '{= ${timeEntryModel>/view/edit} ? ${i18n_core>STOP_WATCH_BUTTON} : ${i18n_core>create}}',
                        press: onAcceptPressed
                    }),
                    endButton: new Button({
                        text: '{i18n_core>cancel}',
                        press: onCancelPressed
                    }),
                    afterClose: function () {
                    }
                });

                this._timeEntryEditPopup.setModel(this.component.getModel('i18n'), "i18n");
                this._timeEntryEditPopup.setModel(this.component.getModel('i18n_core'), "i18n_core");
            }

            this._timeEntryEditPopup.setTitle(this.editTimeEntryViewModel.getDialogTitle());
            this._timeEntryEditPopup.getBeginButton().mEventRegistry.press = [];
            this._timeEntryEditPopup.getBeginButton().attachPress(onAcceptPressed);

            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel, "timeEntryModel");
            this._timeEntryEditPopup.setModel(this.editTimeEntryViewModel.getValueStateModel(), "valueStateModel");

            return this._timeEntryEditPopup;
        },

        onStartTimeChanged: function (oEvent) {
            this.editTimeEntryViewModel.setStartTime(oEvent.getParameter("value"));
        },

        onEndTimeChanged: function (oEvent){
            this.editTimeEntryViewModel.setEndTime(oEvent.getParameter("value"));
        }

    });

});