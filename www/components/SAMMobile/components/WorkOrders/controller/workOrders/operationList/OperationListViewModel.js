sap.ui.define(["SAMContainer/models/ViewModel",
        "sap/ui/model/Filter",
        "sap/ui/model/FilterOperator",
        "sap/ui/model/Sorter",
        "SAMMobile/models/dataObjects/Order",
        "SAMMobile/models/dataObjects/OrderOperation",
        "SAMMobile/models/dataObjects/OrderUserStatus",
        "SAMMobile/components/WorkOrders/controller/workOrders/map/MapMarkerViewModel",
        "SAMMobile/components/WorkOrders/helpers/Formatter",
        "SAMMobile/models/dataObjects/Showman",
        "SAMMobile/helpers/timeHelper",
        "SAMMobile/models/dataObjects/TimeConfirmation",
        "SAMMobile/models/dataObjects/ZOPTime"],

    function (ViewModel, Filter, FilterOperator, Sorter, Order, OrderOperation, OrderUserStatus, MapMarkerViewModel, Formatter, Showman, TimeHelper, TimeConfirmation, ZOPTime) {
        "use strict";

        // constructor
        function OperationListViewModel(requestHelper, globalViewModel, component) {

            ViewModel.call(this, component, globalViewModel, requestHelper, "operations");

            this.operationUserStatus = null;
            this._GROWING_DATA_PATH = "/operations";

            this._FUNCLOC_SUFFIX = "-020-UM";
            this._CODE_GROUP_TO_EXCLUDE = "PMC00005";
            this._CODE = "A001";

            this._formatter = new Formatter(this._component);

            this._oList = null;

            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        OperationListViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        operations: [],
                        selectedOperation: null,
                        searchString: "",
                        counts: {
                            mine: 0,
                            done: 0,
                            unassigned: 0,
                            all: 0
                        },
                        view: {
                            busy: false,
                            selectedTabKey: "mine",
                            setListFilter: "all",
                            operationSelected: false,
                            growingThreshold: this.getGrowingListSettings().growingThreshold,
                            statusButtons: []
                        }
                    };
                }
            },

            getSelectedOperation: {
                value: function () {
                    return this.getProperty("/selectedOperation");
                }
            },

            getOperationQueryParams: {
                value: function (startAt) {
                    return {
                        noOfRows: this.getGrowingListSettings().loadChunk,
                        startAt: startAt,
                        persNo: this.getUserInformation().personnelNumber,
                        workCenter: this.getUserInformation().workCenterId,
                        language: this.getLanguage(),
                        doneStatus: this.orderUserStatus.done.STATUS,
                        prioStatus: this.orderUserStatus.prio.STATUS,
                        searchString: this.getSearchString(),
                        startCons: moment().add(10, 'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                        startDat: moment().add(10, 'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                        startTime: moment().format('hh:mm:ss'),
                        yesterday: moment().add(-1, 'days').format("YYYY-MM-DD"),
                        teamGuids: this.getUserInformation().teams.map(function (oTeam) {
                            return "'" + oTeam.teamGuid + "'";
                        }),
                        today: TimeHelper.getCurrentDateObject().dateString
                    };
                }
            },

            getSearchString: {
                value: function () {
                    return this.getProperty("/searchString");
                }
            },

            setStatusButtonsForStatus: {
                value: function (status, statusProfile) {
                    // Check if status needs stop watch action
                    this.setProperty("/view/statusButtons", this.getStatusHierarchy().getOptionsForStatus(status, statusProfile));
                }
            },

            getOperationStatusProfileForSelectedOrder: {
                value: function (status) {
                    return this.getProperty("/selectedOperation").OPERATION_STATUS_PROFILE;
                }
            },

            getStatusHierarchy: {
                value: function () {
                    return this._component.statusHierarchy;
                }
            },

            getScenario: {
                value: function () {
                    return this._component.scenario;
                }
            },

            getStopwatchManager: {
                value: function () {
                    return this._component.stopwatchManager;
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setList: {
                value: function (oList) {
                    this._oList = oList;
                }
            },

            getSelectedTabKey: {
                value: function () {
                    return this.getProperty("/view/selectedTabKey");
                }
            },

            setSelectedTabKey: {
                value: function (selectedTabKey) {
                    this.setProperty("/view/selectedTabKey", selectedTabKey);
                }
            },

            getListFilter: {
                value: function () {
                    return this.getProperty("/view/setListFilter");
                }
            },

            setListFilter: {
                value: function (selectedTabKey) {
                    this.setProperty("/view/setListFilter", selectedTabKey);
                }
            },

            statusIsReject: {
                value: function (status) {
                    return status == this.orderUserStatus.reject.status;
                }
            },

            statusIsComplete: {
                value: function (status) {
                    return status == this.orderUserStatus.complete.status;
                }
            },

            getList: {
                value: function () {
                    return this._oList;
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });


        OperationListViewModel.prototype.orderUserStatus = null;

        OperationListViewModel.prototype.tabs = {
            mine: {
                actionName: "mine",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/mine", this.getProperty("/counts/mine") + value);
                }
            },

            done: {
                actionName: "done",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/done", this.getProperty("/counts/done") + value);
                }
            },

            unassigned: {
                actionName: "unassigned",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/unassigned", this.getProperty("/counts/unassigned") + value);
                }
            },

            all: {
                actionName: "all",
                needsReload: true,
                lastSearch: "",
                cached: [],
                changeCountBy: function (value) {
                    this.setProperty("/counts/all", this.getProperty("/counts/all") + value);
                }
            }
        };

        OperationListViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        OperationListViewModel.prototype.setUserStatus = function (status, statusProfile, bSetToDone, successCb) {
            var that = this;

            var operation = this.getProperty("/selectedOperation");
            /**Action flag of userstatus is changing wrongly to U after running func getUserStatusMl
             * workaround to avoid this behaviour and set the correct action flag after func
             **/
            var tempOperationUserStatus = operation.OrderUserStatus.find(x => x.STATUS == 'E0002');
            var statusMl = this.getUserStatusMl(status, statusProfile);
            if (typeof tempOperationUserStatus !== 'undefined') {
                statusMl.ACTION_FLAG = tempOperationUserStatus.ACTION_FLAG;
            }
            
            if (!statusMl) {
                this.executeErrorCallback(new Error(this._component.getModel("i18n").getProperty("userStatusNotFound")));
            }

            var fnSuccessUserStatus = function () {
                var selectedTabKey = that.getProperty("/view/selectedTabKey");
                var tabInfo = that.tabs[selectedTabKey];
                tabInfo.needsReload = true;

                
                that.handleStopWatch(statusMl, function (oWatch) {

                    if (!bSetToDone) {
                        that._moveOrderFromTabToTab(that.tabs["done"], that.tabs["mine"], operation);
                    } else {
                        that._moveOrderFromTabToTab(that.tabs["mine"], that.tabs["done"], operation);
                    }

                    that.setBusy(false);
                    that.refresh();
                    // Remove the selection as item will move away from this list
                    that.getList().removeSelections();
                    successCb(that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE), oWatch.timeEntry);
                }.bind(this));
            };

            this.setBusy(true);

            if (bSetToDone) {
                operation.changeUserStatus(
                    statusMl,
                    false,
                    fnSuccessUserStatus.bind(this),
                    this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                );
            } else {
                operation.setUserStatusToInactive(
                    statusMl,
                    false,
                    fnSuccessUserStatus.bind(this),
                    this.executeErrorCallback.bind(this, new Error(this._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                );
            }
        };

        OperationListViewModel.prototype.handleStopWatch = function (statusMl, completeCb) {
            var that = this;
            var statusObject = this.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE);

            if (!statusObject) {
                completeCb({timeEntry: null});
                return;
            }

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            if (statusObject.startsStopwatch()) {
                // start a stop watch
                // TODO USE APPROPRIATE ACT TYPE HERE

                that.getStopwatchManager().start(that.getProperty("/selectedOperation"), function () {

                    that.stopOtherTimers(function (timeEntry) {
                        completeCb({timeEntry: timeEntry});
                    });
                }, onError);

            } else {
                this.getStopwatchManager().stop(this.getProperty("/selectedOperation"), function (timeEntry) {
                    completeCb({timeEntry: timeEntry});
                }, onError);
            }
        };

        OperationListViewModel.prototype.handleStopWatchTeamMember = function (completeCb) {
            var that = this;

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var operation = that.getProperty("/selectedOperation");
            if(operation.WATCH_ICON){
                that.getStopwatchManager().start(operation, function () {

                    that.stopOtherTimers(function (timeEntry, statusObject) {
                        completeCb(timeEntry, statusObject);
                    });
                }, onError);
            }else{
                that.getStopwatchManager().stop(operation, function (timeEntry) {
                    completeCb(timeEntry, null);
                }, onError);
            }
        };

        OperationListViewModel.prototype.updateTimeConfirmation = function (timeEntry, successCb) {
            var that = this;

            var sqlStrings = [];

            var onSuccess = function () {
                successCb();
            };

            var order = new Order({
                ORDERID: timeEntry.ORDERID,
                OPERATION_ACTIVITY: timeEntry.ACTIVITY
            }, this._requestHelper, this);
            order.setPersNo(this.getUserInformation().personnelNumber);

            var aOverlappingTimeConfirmations = TimeHelper.getOverlappingTimeConfirmation(timeEntry, this.getProperty("/allTimeEntries"), this.getLanguage());
            if (aOverlappingTimeConfirmations && aOverlappingTimeConfirmations.length > 0) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("TIME_CONF_ADD_TIME_ENTRY_OVERLAPPING_TIMES_WARNING_TEXT")));
                return;
            }

            this._handleKeyValueFields(timeEntry);

            try {
                sqlStrings.push(timeEntry.insert(true));

                this._requestHelper.executeStatementsAndCommit(sqlStrings, onSuccess, function () {
                    that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_COMPONENT_UPDATE")));
                });
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        OperationListViewModel.prototype.loadAllTimeEntries = function (successCb) {
            var that = this;
            var load_timeConfirmation_success = function (data) {
                var aData = data.map(function (oData) {
                    return new TimeConfirmation(oData, that._requestHelper, that);
                });

                that.setProperty("/allTimeEntries", aData);

                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_timeConfirmation_error = function (err) {
                that.executeErrorCallback(new Error(that._component.getModel("i18n").getResourceBundle().getText("ERROR_LOAD_DATA_TIME_TAB_TEXT")));
            };

            that._requestHelper.getData(
                "TimeConfirmation",
                {persNo: that._component.globalViewModel.model.getProperty("/personnelNumber")},
                load_timeConfirmation_success,
                load_timeConfirmation_error,
                true,
                "getForUser"
            );
        };

        OperationListViewModel.prototype._handleKeyValueFields = function (timeEntry) {

            timeEntry.K_EXP_FLAG = timeEntry.K_VALUE_ID && timeEntry.K_VALUE_ID !== "0" ? "1" : "";
            timeEntry.T_EXP_FLAG = timeEntry.T_VALUE_ID && timeEntry.T_VALUE_ID !== "0" ? "2" : "";
            timeEntry.B_EXP_FLAG = timeEntry.B_VALUE_ID && timeEntry.B_VALUE_ID !== "0" ? "3" : "";

            if (!timeEntry.T_EXP_FLAG) {
                this.viewModel.executeErrorCallback(new Error(this.component.getModel("i18n_core").getResourceBundle().getText("TIME_CONF_TRAVEL_COST_VALUE_STATE_ERROR")));
            }
        };

        OperationListViewModel.prototype.stopOtherTimers = function (successCb) {
            var that = this;
            var orderOperations = this.getProperty("/operations");

            var load_success = function (timeEntry, statusObject) {
                successCb(timeEntry, statusObject);
            };

            var onError = function (err) {
                that.executeErrorCallback(err);
            };

            var watchFound = false;

            orderOperations.forEach(function (operation) {
                if (that.getProperty("/selectedOperation").ORDERID !== operation.ORDERID) {
                    if(operation.whichUser == "1"){
                        var runningWatch = that.getStopwatchManager().getStopwatchForOrderId(operation);
                        if(runningWatch){
                            watchFound = true;
                            that.getStopwatchManager().stop(operation, function (timeEntry) {
                                load_success(timeEntry, null);
                            }, onError);
                        }
                    }else{

                        var aOperationUserStatus = operation.getUserStatus();
                        aOperationUserStatus.forEach(function (userStatus) {
                            if (userStatus.isActive() && userStatus.STATUS == "E0013") {
                                watchFound = true;
                                var statusMl = that.getUserStatusMl("E0012", operation.OPERATION_STATUS_PROFILE);

                                var fnSuccessUserStatus = function () {
                                    that.getStopwatchManager().stop(operation, function (timeEntry) {
                                        load_success(timeEntry, that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE));
                                    }, onError);
                                };

                                operation.changeUserStatus(
                                    statusMl,
                                    false,
                                    fnSuccessUserStatus.bind(that),
                                    that.executeErrorCallback.bind(that, new Error(that._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                                );
                            }
                        });
                    }
                }else if(that.getProperty("/selectedOperation").ACTIVITY !== operation.ACTIVITY){
                    if(operation.whichUser == "1"){
                        var runningWatch = that.getStopwatchManager().getStopwatchForOrderId(operation);
                        if(runningWatch){
                            watchFound = true;
                            that.getStopwatchManager().stop(operation, function (timeEntry) {
                                load_success(timeEntry, null);
                            }, onError);
                        }
                    }else{

                        var aOperationUserStatus = operation.getUserStatus();
                        aOperationUserStatus.forEach(function (userStatus) {
                            if (userStatus.isActive() && userStatus.STATUS == "E0013") {
                                watchFound = true;
                                var statusMl = that.getUserStatusMl("E0012", operation.OPERATION_STATUS_PROFILE);

                                var fnSuccessUserStatus = function () {
                                    that.getStopwatchManager().stop(operation, function (timeEntry) {
                                        load_success(timeEntry, that.getStatusHierarchy().getStatusObject(statusMl.USER_STATUS, statusMl.STATUS_PROFILE));
                                    }, onError);
                                };

                                operation.changeUserStatus(
                                    statusMl,
                                    false,
                                    fnSuccessUserStatus.bind(that),
                                    that.executeErrorCallback.bind(that, new Error(that._component.getModel("i18n").getProperty("ERROR_REJECT_OPERATION")))
                                );
                            }
                        });
                    }
                }
            });
            if (!watchFound) {
                load_success(null, null);
            }

        };

        OperationListViewModel.prototype.onTabChanged = function () {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            // MODEL SEARCH
            // Reset binding and resetting searchValue when switching tabs
            this.setProperty("/searchString", "");
            this.getList().getBinding("items").filter(null);

            this.resetGrowingListDelegate();

            if (tabInfo.needsReload) {
                this.fetchData();
            } else {
                this.setNewData(tabInfo.cached);
            }
        };

        OperationListViewModel.prototype.onSearch = function (sValue) {
            // DATABASE SEARCH
            // clearTimeout(this._delayedSearch);
            // this._delayedSearch = setTimeout(this.fetchData.bind(this), 400);
            // MODEL SEARCH
            var aFilters = [];
            var oBinding = this.getList().getBinding("items");

            var sKey = this.getListFilter();

            switch (sKey) {
                case "all":
                case "mine":
                    // do nothing - no filter shall be applied
                    aFilters.push(new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("ACTIVITY", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("DESCRIPTION", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("STARTDATE", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("ENDDATE", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("FUNCLOC_DESC", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("PRIORITY", sap.ui.model.FilterOperator.Contains, sValue));
                    aFilters.push(new Filter("PRIORITY_TEXT", sap.ui.model.FilterOperator.Contains, sValue));
                    var allFilter = new Filter(aFilters);

                    oBinding.filter(allFilter);
                    break;

                case "day":
                    var dayFilter = new Filter({
                        filters: [
                            new Filter([
                                new Filter({
                                    path: "",
                                    test: function(operation) {
                                        let today = moment();
                                        return (operation["STARTDATE"] && operation["ENDDATE"])
                                            &&((moment(operation._startDate).isSameOrBefore(today) && moment(operation._endDate).isSameOrAfter(today)) || moment(operation._startDate).isSame(today, "day"));
                                    }
                                })
                            ], true),
                            new Filter([
                                new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("ACTIVITY", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("DESCRIPTION", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("FUNCLOC_DESC", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("PRIORITY", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("PRIORITY_TEXT", sap.ui.model.FilterOperator.Contains, sValue)
                            ]),
                        ],
                        and: true
                    });
                    oBinding.filter(dayFilter);
                    break;

                case "week":
                    // moment.js sets Sunday as first weekday
                    var sBeginOfWeek = TimeHelper.getDateObject(1 - moment().day(), 'days').dateString,
                        sEndOfWeek = TimeHelper.getDateObject(7 - moment().day(), 'days').dateString;

                    var weekFilter = new Filter({
                        filters: [
                            new Filter([
                                new Filter("_startDate", FilterOperator.BT, sBeginOfWeek, sEndOfWeek),
                                new Filter("_endDate", FilterOperator.BT, sBeginOfWeek, sEndOfWeek),
                            ]),
                            new Filter([
                                new Filter("ORDERID", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("ACTIVITY", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("DESCRIPTION", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("FUNCT_LOC", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("FUNCLOC_DESC", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("PRIORITY", sap.ui.model.FilterOperator.Contains, sValue),
                                new Filter("PRIORITY_TEXT", sap.ui.model.FilterOperator.Contains, sValue)
                            ]),
                        ],
                        and: true
                    });

                    oBinding.filter(weekFilter);

                    break;
            }
        };

        OperationListViewModel.prototype.onSort = function (oEvt) {
            var that = this;
            var aSorters = [],
                mParams = oEvt.getParameters(),
                oBinding = this.getList().getBinding("items"),
                sPath,
                bDescending;

            sPath = mParams.sortItem.getKey();
            bDescending = mParams.sortDescending;
            aSorters = new Sorter(sPath, bDescending);


            // override compare function only for Date path

            if (sPath === "STARTDATE" || sPath === "ENDDATE") {
                aSorters.fnCompare = function (a, b) {

                    var aDate = a.split(' ').shift();
                    var bDate = b.split(' ').shift();
                    var aTime = a.split(' ').pop();
                    var bTime = b.split(' ').pop();

                    if (that.getLanguage() === 'D') {
                        var aYear = aDate.split('.')[2];
                        var aMonth = aDate.split('.')[1];
                        var aDay = aDate.split('.')[0];

                        var bYear = bDate.split('.')[2];
                        var bMonth = bDate.split('.')[1];
                        var bDay = bDate.split('.')[0];
                    } else {
                        var aYear = aDate.split('/')[0];
                        var aMonth = aDate.split('/')[1];
                        var aDay = aDate.split('/')[1];

                        var bYear = bDate.split('/')[0];
                        var bMonth = bDate.split('/')[1];
                        var bDay = bDate.split('/')[2];
                    }

                    var aHour = aTime.split(':').shift();
                    var bHour = bTime.split(':').shift();

                    var aMin = aTime.split(':').pop();
                    var bMin = bTime.split(':').pop();

                    // parse to Date object
                    if (!aYear) {
                        return 1;
                    } else {
                        var aDate = new Date(aYear, aMonth, aDay, aHour, aMin);
                    }

                    if (!bYear) {
                        return -1;
                    } else {
                        var bDate = new Date(bYear, bMonth, bDay, bHour, bMin);
                    }

                    if (aDate == bDate) {
                        return 0;
                    }
                    if (aDate < bDate) {
                        return -1;
                    }
                    if (aDate > bDate) {
                        return 1;
                    }
                    return 0;
                };
            }

            // apply the selected sort and group settings
            oBinding.sort(aSorters);

        };

        OperationListViewModel.prototype.setSelectedOperation = function (operation) {
            var selectedTabKey = this.getProperty("/view/selectedTabKey");

            if (selectedTabKey == this.tabs.all.actionName) {
                operation = null;
            }

            this.setProperty("/selectedOperation", operation);
            this.setProperty("/view/operationSelected", !operation || operation == undefined ? false : true);

            this.setStatusButtonsForStatus(!operation ? "" : operation.OPERATION_USER_STATUS, !operation ? "" : operation.OPERATION_STATUS_PROFILE);
        };

        OperationListViewModel.prototype.navigateToLocation = function (oEvent) {
            var that = this;
            var location = this.getSelectedOperation().getLocation();

            if (!location) {
                this.executeErrorCallback(new Error("Could not start navigation: No GPS data maintained."));
                return;
            }

            launchnavigator.isAppAvailable(launchnavigator.APP.HERE_MAPS, function (isAvailable) {
                var app;
                if (isAvailable) {
                    that.startNavigation(location, launchnavigator.APP.HERE_MAPS);
                } else {
                    var dialog = that._component.rootComponent.navigator.getSelectionDialog(function (oEvent) {
                        var selectedItem = oEvent.getParameter("listItem");
                        var app = selectedItem.getBindingContext("undefined").getObject().app;
                        that.startNavigation(location, app);
                        dialog.close();
                    });

                    dialog.open();
                }
            });
        };

        OperationListViewModel.prototype.startNavigation = function (address, app) {
            this._component.rootComponent.navigator.navTo(address, function () {
            }, function () {
            }, app);
        };

        OperationListViewModel.prototype.setNewData = function (data) {
            var that = this;

            var spras = this.getLanguage();
            var priorities = this.getCustomizingModelData('PriorityTypes');

            var orderOperations = data.OperationList.map(function (operationData) {
                return new OrderOperation(operationData, that._requestHelper, that);
            });

            orderOperations.forEach(function (operation) {
                operation.ZFIX_visible = false;
                operation.RejectVisible = false;
                operation.CompleteVisible = false;
                operation.CancelRejectVisible = false;
                operation.SetInProgressVisible = false;
                operation.rejectStatusAvailable = false;

                var operationUserStatus = data.operUserStatus.filter(function (userStatus) {
                    return userStatus.OBJNR === operation.OBJECT_NO && userStatus.ORDERID === operation.ORDERID;
                });

                var aOperationUserStatus = operationUserStatus.map(function (oOperationUserStatus) {
                    return new OrderUserStatus(oOperationUserStatus, that._requestHelper, that);
                });

                operation.setUserStatus(aOperationUserStatus);

                var userStatusPrioAvailableAndSynced = false;

                var watchCounter = 0;
                var doneStatus = false;

                aOperationUserStatus.forEach(function (userStatus) {
                    if (userStatus.isActive()) {
                        switch (userStatus.STATUS) {
                            case "E0002": //done
                                operation.SetInProgressVisible = true;
                                doneStatus = true;
                                break;
                            case "E0003": // prio
                            case "E0021":
                                if (userStatus.ACTION_FLAG === "N" || userStatus.ACTION_FLAG === "C") {
                                    operation.CancelRejectVisible = true;
                                } else {
                                    userStatusPrioAvailableAndSynced = true;
                                }
                                operation.rejectStatusAvailable = true;
                                break;
                            case "E0005": // down
                                operation.RejectVisible = true;
                                operation.CompleteVisible = true;
                                break;
                            case "E0018": // zfix
                                operation.ZFIX_visible = true;
                                break;

                            case "E0012": // stop
                                operation.WatchEnabled = true;
                                operation.WATCH_ICON = true;
                                operation.startStopstatus = "E0013";
                                watchCounter++;
                                break;

                            case "E0013": // start
                                operation.WatchEnabled = true;
                                operation.WATCH_ICON = false;
                                operation.startStopstatus = "E0012";
                                watchCounter++;
                                break;
                        }
                    }
                });
                
                let zpmcStatus = aOperationUserStatus.filter(function(status) {
                   return status.isActive() && status.STATUS == "E0021";
                });

                if (zpmcStatus.length > 0) {
                    operation.CancelRejectVisible = false;
                    operation.RejectVisible = false;
                    operation.CompleteVisible = false;
                }

                if (userStatusPrioAvailableAndSynced || operation.CancelRejectVisible || operation.SetInProgressVisible) {
                    operation.RejectVisible = false;
                }

                if (operation.SetInProgressVisible || operation.CancelRejectVisible) {
                    operation.CompleteVisible = false;
                }

                if(userStatusPrioAvailableAndSynced){
                    operation.CompleteVisible = false;
                }

                if (!operation.WatchEnabled) {
                    var startFlag = false;
                    var stopFlag = false;
                    var statusArr = that.getStatusHierarchy().status;

                    statusArr.forEach(function (status) {
                        if (status.statusProfile == operation.OPERATION_STATUS_PROFILE && status.status == "INBE") {
                            startFlag = true;
                        } else if (status.statusProfile == operation.OPERATION_STATUS_PROFILE && status.status == "STOP") {
                            stopFlag = true;
                        }
                    });

                    if (startFlag && stopFlag) {
                        operation.WatchEnabled = true;
                        operation.startStopstatus = "E0013";
                    } else {
                        operation.WatchEnabled = false;
                    }
                    operation.WATCH_ICON = true;
                }

                if (watchCounter == 2) {
                    operation.WatchEnabled = true;
                    operation.WATCH_ICON = true;
                    operation.startStopstatus = "E0013";
                }

                if (priorities && operation.PRIORITY) {
                    for (var i = 0; i < priorities.length; i++) {
                        var prioObj = priorities[i];

                        if (prioObj.PRIORITY === operation.PRIORITY && prioObj.PRIOTYPE === operation.ARTPR) {
                            operation.PRIORITY_TEXT = prioObj.PRIORITY_DESC;
                        }
                    }
                }

                operation.teamLead = false;
                //For operation assigned to personal value should be "0";
                //For operation assigned to team member value should be "1";
                //For operation assigned to team lead value should be "2";
                operation.whichUser = "0";
                // Check if the operation is link to a TEAM and set teamLead flag
                if (operation.TEAM_GUID) {
                    operation.whichUser = "1";
                    //Check if the user is the teamLead on this operation
                    that.getUserInformation().teams.forEach(function (oTeam) {
                        //if user team correspond to the Operation Team
                        if (oTeam.teamGuid === operation.TEAM_GUID) {
                            if (oTeam.isTeamLead) {
                                //user is teamLead so he's the operation team lead
                                operation.teamLead = true;
                                operation.whichUser = "2";
                            } else {
                                //user is just member of the operation Team so i cannot Complete or reject the operation
                                operation.CompleteVisible = false;
                                operation.RejectVisible = false;
                                operation.CancelRejectVisible = false;
                                if(doneStatus && operation.ACTION_FLAG === 'U'){
                                    operation.SetInProgressVisible = false;
                                }
                            }
                        }
                    });

                    var team = {
                        start: "",
                        end: ""
                    };


                    var oStart = moment(operation.TEAM_BEG_DATUM.split(" ").shift() + " " + operation.TEAM_BEG_UZEIT.split(" ").pop());
                    team.start = moment(oStart).format("YYYY/MM/DD HH:mm");

                    var oEnd = moment(operation.TEAM_END_DATUM.split(" ").shift() + " " + operation.TEAM_END_DATUM.split(" ").pop());
                    team.end = moment(oEnd).format("YYYY/MM/DD HH:mm");

                    operation.team = team;
                }

                //If operation is asisigned to team member then check if any timer is running.
                //Here timer user status of operattion does not matter
                if( operation.whichUser == "1"){
                    var runningWatch = that.getStopwatchManager().getStopwatchForOrderId(operation);
                    operation.WatchEnabled = true;
                    if(runningWatch){
                        operation.WATCH_ICON = false;
                    }else{
                        operation.WATCH_ICON = true;
                    }
                }

                if(!operation.WATCH_ICON){
                    operation.timeStartedBtnDisabled = false;
                }else{
                    operation.timeStartedBtnDisabled = true;
                }

                // check and set the dates. 1st Own Dates, 2nd Team Check, 3rd normal date check
                if (operation.TERMIN_START && operation.TERMIN_END) {
                    // nothing to do here as already covered in OrderOperation.prototype.onAfterExistingDataSet

                    if(operation.rejectStatusAvailable){
                        operation.TERMIN_START = "";
                        operation.TERMIN_END = "";
                        that.deleteOwnDates(operation.OBJECT_NO, function(){});
                    }

                } else {
                    // If start date and end date differ (= more than one day), we need to clear them as they shall not be displayed in the list
                    // This is now also true in case the operation is assigned to a team
                    // See https://msc-mobile.atlassian.net/browse/PFAL-849
                    if (operation.START_CONS !== operation.FIN_CONSTR) {
                        operation.STARTDATE = "";
                        operation.ENDDATE = "";
                    }
                }

                // Check for order type and set funcloc description
                // https://msc-mobile.atlassian.net/browse/PFAL-615

                if (operation.ORDER_TYPE === "SM03" || operation.ORDER_TYPE === "ZM06" || operation.ORDER_TYPE === "ZM09") {
                    operation.FUNCLOC_DESC = operation.CITY1 + " " + operation.STREET;
                }

                operation.setTimeForOperation();
            });

            this.setProperty("/operations", orderOperations);
            this.refresh(true);
            this.setSelectedOperation(null);
        };

        OperationListViewModel.prototype.fetchData = function (successCb, bGrowing) {
            bGrowing = bGrowing === undefined ? false : bGrowing;

            var that = this;
            var selectedTabKey = this.getProperty("/view/selectedTabKey");
            var tabInfo = this.tabs[selectedTabKey];

            var load_success = function (data) {
                tabInfo.needsReload = false;
                tabInfo.lastSearch = that.getSearchString();
                that.setNewData(data);

                tabInfo.cached = data;
                that.setBusy(false);

                that.loadAllTimeEntries();
                if (successCb !== undefined) {
                    successCb();
                }
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_WHILE_FETCHING_ORDERS")));
            };

            this.orderUserStatus = this.getScenario().getOrderUserStatus();
            this.setBusy(true);

            this._requestHelper.getData(["OperationList", "operUserStatus"],
                this.getOperationQueryParams(bGrowing ? this.getProperty("/operations").length + 1 : 1),
                load_success,
                load_error,
                true,
                tabInfo.actionName);
        };

        OperationListViewModel.prototype.deleteOwnDates = function (OBJECT_NO, successCb) {
            try {
                this.getOwnDatesObject(OBJECT_NO).then(function(data){
                    var oZOPTime = new ZOPTime(data[0],this._requestHelper, this);
                    oZOPTime.ACTION_FLAG = "N"; // This is required as otherwise the delete sql statement is not executed
                    oZOPTime.delete(false, successCb, this.executeErrorCallback.bind(this, new Error(this._component.i18n.getText("ERROR_DELETE_OWN_DATES"))));
                }.bind(this));
            } catch (err) {
                this.executeErrorCallback(err);
            }
        };

        OperationListViewModel.prototype.getOwnDatesObject = function (OBJECT_NO){
            
            return new Promise(function (resolve, reject) {
                this._requestHelper.getData("ZOPTime", {
                    objnr: OBJECT_NO
                }, resolve, reject, true);
            }.bind(this));
            
        },

        OperationListViewModel.prototype.refreshData = function (successCb) {
            this.resetModel();
            this.refresh(true);
            this.fetchData(successCb);
        };

        OperationListViewModel.prototype.resetModel = function () {
            for (var key in this.tabs) {
                this.tabs[key].needsReload = true;
            }
        };

        OperationListViewModel.prototype.fetchCounts = function () {
            var that = this;

            var load_success = function (data) {

                that.setProperty("/counts/mine", parseInt(data.MineOperationCount ? data.MineOperationCount[0].COUNT : 0));
                that.setProperty("/counts/unassigned", parseInt(data.UnassignedOperationCount ? data.UnassignedOperationCount[0].COUNT : 0));
                that.setProperty("/counts/all", parseInt(data.AllOperationCount ? data.AllOperationCount[0].COUNT : 0));
                that.setProperty("/counts/done", parseInt(data.DoneOperationCount ? data.DoneOperationCount[0].COUNT : 0));
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_WHILE_FETCHING_TAB_COUNTS")));
            };

            this.orderUserStatus = this.getScenario().getOrderUserStatus();

            this._requestHelper.getData(["MineOperationCount", "DoneOperationCount"], {
                workCenter: this.getUserInformation().workCenterId,
                doneStatus: this.orderUserStatus.done.STATUS,
                prioStatus: this.orderUserStatus.prio.STATUS,
                persNo: this.getUserInformation().personnelNumber,
                teamGuids: this.getUserInformation().teams.map(function (oTeam) {
                    return "'" + oTeam.teamGuid + "'";
                }),
                completeStatus: this.orderUserStatus.complete.status,
                rejectedStatus: this.orderUserStatus.reject.status,
                today: TimeHelper.getCurrentDateObject().dateString,
                startCons: moment().add(10, 'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                yesterday: moment().add(-1, 'days').format("YYYY-MM-DD"),
            }, load_success, load_error, true);
        };

        OperationListViewModel.prototype._moveOrderFromTabToTab = function (fromTab, toTab, operation) {
            this.setSelectedOperation(null);
            this.removeFromModelPath(operation, "/operations");
            fromTab.cached = this.getProperty("/operations");
            toTab.needsReload = true;
            toTab.changeCountBy.apply(this, [1]);
            fromTab.changeCountBy.apply(this, [-1]);
        };

        OperationListViewModel.prototype._checkShowmanTabVisibility = function (oOrder) {
            var sPersNo = this.getUserInformation().personnelNumber;
            if (oOrder.PMACTTYPE === "N98") {
                if (oOrder.TEAM_PERSNO === sPersNo || oOrder.PERS_NO === sPersNo) {
                    return true;
                } else if (oOrder.TEAM_GUID) {
                    return this.getUserInformation().teams.filter(function (oTeam) {
                        return oTeam.teamGuid === oOrder.TEAM_GUID && oTeam.isTeamLead;
                    }).length > 0;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        };

        OperationListViewModel.prototype.checkShowmanProcessFullfilled = function (showmanData, oOperation) {
            var check = false;
            var that = this;
            
            if (this._checkShowmanTabVisibility(oOperation)) {

                var currentShowmanObject;

                if (showmanData && showmanData.length > 0) {
                    currentShowmanObject = new Showman(showmanData[0], this._requestHelper, this);
                } else {
                    currentShowmanObject = new Showman(null, this._requestHelper, this);
                }

                currentShowmanObject.showmanProcesses.forEach(function (showDat) {
                    var orderId = oOperation.ORDERID;
                    var operation = oOperation.ACTIVITY;
                    if (showDat.ORDERNO === orderId && showDat.OPERATION === operation) {
                        if (showDat.disableSelected === true) {
                            check = true;
                        }
                    }
                });

                if(!check){
                    check = currentShowmanObject.checkProcessFullfiled();
                }
            } else {
                check = true;
            }

            return check;
        };

        OperationListViewModel.prototype.validateOperationCompletion = function (oOperation, successCb) {
            var that = this;

            var fnLoadSuccess = function (data) {
                var allZero = data.OperationCompletionAllZeroMinMax.filter(function(ord){
                    return ord.USER_FIELD !== "" && parseInt(ord.USER_FIELD) == "0";
                });

                var bAllCompleted = ( data.OperationCompletionChecklistCount[0].COUNT === "0" || data.OperationCompletionChecklist[0].COUNT === data.OperationCompletionChecklistCount[0].COUNT )
                    && (data.OperationCompletionMinMax[0].MAX === "" || data.OperationCompletionMinMax[0].MAX > 0 || allZero.length > 0 || data.OperationCompletionNoMinMaxRequired[0].COUNT === "1") && (that.checkShowmanProcessFullfilled(data.showManDataWithOperation, oOperation));
                successCb(bAllCompleted);
            };

            var fnLoadError = function (err) {
                that.executeErrorCallback(new Error(that._component.i18n.getText("ERROR_VALIDATE_OPERATION_COMPLETION")));
            };

            this._requestHelper.getData(["OperationCompletionChecklist", "OperationCompletionMinMax", "OperationCompletionAllZeroMinMax", "OperationCompletionChecklistCount", "OperationCompletionNoMinMaxRequired", "showManDataWithOperation"], {
                orderId: oOperation.ORDERID,
                notifNo: oOperation.NOTIF_NO,
                language: this.getLanguage(),
                funcloc: oOperation.FUNCT_LOC,
                funclocUM: oOperation.FUNCT_LOC + this._FUNCLOC_SUFFIX,
                persNo: this.getUserInformation().userName,
                codeGroup: this._CODE_GROUP_TO_EXCLUDE,
                code: this._CODE,
                operation: oOperation.ACTIVITY
            }, fnLoadSuccess, fnLoadError, true);
        };

        OperationListViewModel.prototype.filterList = function (sKey) {
            var aFilters = [],
                oBinding = this.getList().getBinding("items");

            switch (sKey) {
                case "all":
                    // do nothing - no filter shall be applied
                    break;

                case "day":
                    var sToday = TimeHelper.getCurrentDateObject().dateString;
                    var sTomorrow = moment(sToday).add(1, "days").format("YYYY-MM-DD HH:mm:ss.SSS");
                    aFilters.push(new Filter([
                        new Filter("_startDate", FilterOperator.GE, sToday),
                        new Filter("_endDate", FilterOperator.LT, sTomorrow),
                    ], true));
                    break;

                case "week":
                    // moment.js sets Sunday as first weekday
                    var sBeginOfWeek = TimeHelper.getDateObject(1 - moment().day(), 'days').dateString,
                        sEndOfWeek = TimeHelper.getDateObject(7 - moment().day(), 'days').dateString;

                    aFilters.push(new Filter([
                        new Filter("_startDate", FilterOperator.BT, sBeginOfWeek, sEndOfWeek),
                        new Filter("_endDate", FilterOperator.BT, sBeginOfWeek, sEndOfWeek),
                    ], false));
                    break;
            }

            oBinding.filter(aFilters);
        };

        OperationListViewModel.prototype.onCompleteWeekOperations = function (successCb) {
            var that = this;
            var sqlStrings = [];
            var orderOperations = this.getProperty("/operations");
            var operationCount = 0;

            var aOperationsToBeCompleted = orderOperations.filter(function (operation) {
                return operation.teamLead && operation["team"] && that.checkDateValidityOfTeam(operation.team.start, operation.team.end);
            });

            //PFAL-1253 - Only consider operations that fall within the current week
            const startOfCurrentWeek = moment().locale(window.navigator.language).startOf("week");
            const endOfCurrentWeek = moment().locale(window.navigator.language).endOf("week");
            aOperationsToBeCompleted = aOperationsToBeCompleted.filter(function (operation) {
                if (!operation.START_CONS &&
                    !operation.STRTTIMCON &&
                    !operation.FIN_CONSTR &&
                    !operation.FINTIMCONS) {
                    return false;
                }

                const operationStart = moment(operation.START_CONS.split(" ").shift() + " " + operation.STRTTIMCON.split(" ").pop());
                const operationEnd = moment(operation.FIN_CONSTR.split(" ").shift() + " " + operation.FINTIMCONS.split(" ").pop());

                return operationStart.isSameOrAfter(startOfCurrentWeek) && operationEnd.isSameOrBefore(endOfCurrentWeek);
            });

            if (aOperationsToBeCompleted.length == 0) {
                successCb(operationCount);
                return;
            }

            var onError = function () {
                this.executeErrorCallback(new Error(that._component.i18n.getText("COMPLETE_WEEK_ERROR")));
            };

            var onLongtextLoaded = function () {
                aOperationsToBeCompleted.forEach(function (operation) {
                    var statusMl = that.getUserStatusMl("E0021", operation.OPERATION_STATUS_PROFILE);
                    var longtext = operation.addLongtext(true, "Manueller Wochenabschluss", "AVOT");

                    var userStatus = operation.changeUserStatus(statusMl, true);
                    if (userStatus.length > 0) {
                        sqlStrings.push(userStatus[0]);
                        sqlStrings = sqlStrings.concat(longtext);
                        operationCount++;
                    }
                });

                try {
                    this._requestHelper.executeStatementsAndCommit(sqlStrings, function () {
                        successCb(operationCount);
                    }, onError.bind(this));
                } catch (err) {
                    onError.bind(this);
                }
            };

            // Load longtext
            var aLongtextLoadingQueue = aOperationsToBeCompleted.map(function (operation) {
                return operation.loadLongTextPromise("AVOT");
            });

            Promise.all(aLongtextLoadingQueue).then(onLongtextLoaded.bind(this)).catch(onError.bind(this));
        };

        OperationListViewModel.prototype.checkDateValidityOfTeam = function (start, end) {
            var today = moment().format("YYYY/MM/DD HH:mm");

            var oStart = moment(start).format("YYYY/MM/DD HH:mm");
            var oEnd = moment(end).format("YYYY/MM/DD HH:mm");

            var flag = today >= oStart && today <= oEnd;

            return flag;
        };

        OperationListViewModel.prototype.changeUserStatus = function (operation) {

            var statusMl = this.getUserStatusMl("E0003", operation.OPERATION_STATUS_PROFILE);

            return new Promise(function (resolve, reject) {
                operation.changeUserStatus(
                    statusMl,
                    true,
                    resolve,
                    reject)
            });
        };

        OperationListViewModel.prototype._hasReachedMaxDailyWork = function (timeEntry) {
            var that = this;
            var iMaxDailyWorkInMinutes = this._component.scenario.getMaxDailyWorkInMinutes();

            if (parseInt(timeEntry.ACT_WORK) > iMaxDailyWorkInMinutes) {
                return true;
            }

            var aAllTimeEntries = this.getProperty("/allTimeEntries");
            if (aAllTimeEntries.length === 0) {
                return false;
            }

            if (moment(timeEntry.START_TIME).date() === moment(timeEntry.END_TIME).date()) {
                var iSumDailyWorkInMinutes = 0;

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutes += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutes += parseInt(timeEntry.ACT_WORK);

                return iSumDailyWorkInMinutes > iMaxDailyWorkInMinutes;

            } else {
                var iSumDailyWorkInMinutesDayOne = 0;
                var iSumDailyWorkInMinutesDayTwo = 0;

                var iDurationDayOne = (60 * 24) - moment.duration(moment(timeEntry.START_TIME).diff(moment(timeEntry.START_DATE))).asMinutes();
                var iDuationDayTwo = moment.duration(moment(timeEntry.END_TIME).diff(moment(timeEntry.END_DATE))).asMinutes();

                aAllTimeEntries.forEach(function (oTimeConfirmation) {
                    if (oTimeConfirmation.MOBILE_ID !== timeEntry.MOBILE_ID) {
                        iSumDailyWorkInMinutesDayOne += that._calculateDailyWorkForDayOne(timeEntry, oTimeConfirmation);
                        iSumDailyWorkInMinutesDayTwo += that._calculateDailyWorkForDayTwo(timeEntry, oTimeConfirmation);
                    }
                });

                iSumDailyWorkInMinutesDayOne += iDurationDayOne;
                iSumDailyWorkInMinutesDayTwo += iDuationDayTwo;

                return iSumDailyWorkInMinutesDayOne > iMaxDailyWorkInMinutes || iSumDailyWorkInMinutesDayTwo > iMaxDailyWorkInMinutes;
            }

        };

        OperationListViewModel.prototype._calculateDailyWorkForDayOne = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        OperationListViewModel.prototype._calculateDailyWorkForDayTwo = function (timeEntry, oTimeConfirmation) {
            var oTimeConfirmationStartDate = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop()),
                oTimeConfirmationEndDate = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());

            if (this._checkDatesForSameMonthAndYear(timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate)) {
                if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return parseInt(oTimeConfirmation.ACT_WORK);
                } else if (moment(oTimeConfirmationStartDate).date() === moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() !== moment(timeEntry.END_TIME).date()) {
                    return (60 * 24) - moment.duration(moment(oTimeConfirmation.START_TIME).diff(moment(oTimeConfirmation.START_DATE))).asMinutes();
                } else if (moment(oTimeConfirmationStartDate).date() !== moment(timeEntry.START_TIME).date() && moment(oTimeConfirmationEndDate).date() === moment(timeEntry.END_TIME).date()) {
                    return moment.duration(moment(oTimeConfirmation.END_TIME).diff(moment(oTimeConfirmation.END_DATE))).asMinutes();
                }
            }

            return 0;
        };

        OperationListViewModel.prototype._checkDatesForSameMonthAndYear = function (timeEntry, oTimeConfirmationStartDate, oTimeConfirmationEndDate) {
            return moment(oTimeConfirmationStartDate).month() === moment(timeEntry.START_TIME).month() &&
                moment(oTimeConfirmationStartDate).year() === moment(timeEntry.START_TIME).year() &&
                moment(oTimeConfirmationEndDate).month() === moment(timeEntry.END_TIME).month() &&
                moment(oTimeConfirmationEndDate).year() === moment(timeEntry.END_TIME).year();
        };

        OperationListViewModel.prototype.constructor = OperationListViewModel;

        return OperationListViewModel;
    });
