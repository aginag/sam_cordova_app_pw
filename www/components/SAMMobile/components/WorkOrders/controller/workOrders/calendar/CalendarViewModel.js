sap.ui.define([
        "SAMContainer/models/ViewModel",
        "SAMMobile/helpers/timeHelper",
        "sap/ui/unified/library"
    ],

    function (ViewModel, TimeHelper, unifiedLibrary) {
        "use strict";

        // constructor
        function CalendarViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        CalendarViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        appointments: [],
                        view: {
                            busy: false,
                            fullDay: false
                        }
                    };
                }
            },

            getOperationQueryParams: {
                value: function () {
                    return {
                        persNo: this.getUserInformation().personnelNumber,
                        workCenter: this.getUserInformation().workCenterId,
                        doneStatus: this.orderUserStatus.done.STATUS,
                        prioStatus: this.orderUserStatus.prio.STATUS,
                        zfixStatus: this.orderUserStatus.zfix.STATUS,
                        language: this.getLanguage(),
                        teamGuids: this.getUserInformation().teams.map(function (oTeam) { return "'" + oTeam.teamGuid + "'"; }),
                        inTwoWeeks: TimeHelper.getDateObject(2, 'week').dateString,
                        yesterday: moment().add(-1,'days').format("YYYY-MM-DD")
                    };
                }
            },

            setAppointments: {
                value: function (appointments) {
                    this.setProperty("/appointments", appointments);
                }
            },

            getAppointments: {
                value: function (bBusy) {
                    return this.getProperty("/appointments");
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        CalendarViewModel.prototype.orderUserStatus = null;

        CalendarViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        CalendarViewModel.prototype.setNewData = function (data) {

            var appointments = data.OperationList.map(function(operation) {

                if (operation.TERMIN_START && operation.TERMIN_END) {
                    var startDate = moment(operation.TERMIN_START);
                    var endDate = moment(operation.TERMIN_END);
                    operation.Type = sap.ui.unified.CalendarDayType.Type08;
                } else if (operation.START_CONS !== operation.FIN_CONSTR) { // If start date and end date differ (= more than one day), we need to clear them as they shall not be displayed in the list
                    var startDate = moment(operation.START_CONS.split(" ").shift());
                    var endDate = moment(operation.FIN_CONSTR.split(" ").shift()); 
                    operation.Type = unifiedLibrary.CalendarDayType.Type09;
                } else {
                    var startDate = moment(operation.START_CONS.split(" ").shift() + " " + operation.STRTTIMCON.split(" ").pop());
                    var endDate = moment(operation.FIN_CONSTR.split(" ").shift() + " " + operation.FINTIMCONS.split(" ").pop());
                    operation.Type = unifiedLibrary.CalendarDayType.Type01;
                }

                operation._START_DATE_TIMESTAMP = startDate.format("YYYY-MM-DD HH:mm:ss.SSS");
                operation._START_DATE = startDate.isValid() ? startDate.toDate() : null;
                operation._END_DATE = endDate.isValid() ? endDate.toDate() : null;


                // Check for order type and set funcloc description
                // https://msc-mobile.atlassian.net/browse/PFAL-615
                if (operation.ORDER_TYPE === "SM03") {
                    operation.FUNCLOC_DESC = operation.CITY1 + " " + operation.STREET;
                }

                operation.ZFIX_visible = !!data.operUserStatus.find(oObject => oObject.OBJECT_NO === operation.OBJECT_NO);

                if (operation.ZFIX_visible) {
                    operation.ICON = "sap-icon://instance";
                    operation.Type = unifiedLibrary.CalendarDayType.Type03;
                } else if (operation.TERMIN_START && operation.TERMIN_END) {
                    operation.ICON = "sap-icon://customer-history";
                } else {
                    operation.ICON = "sap-icon://eam-work-order";
                }
                
                return operation;
            });

            this.setAppointments(appointments.filter(function(operation) {
                return operation._START_DATE != null && operation._END_DATE != null;
            }));
        };

        CalendarViewModel.prototype.fetchData = function (equipment, orderId) {
            var that = this;

            var load_success = function (data) {
                that.setNewData(data);
                that.setBusy(false);
            };

            var load_error = function (err) {
                that.executeErrorCallback(new Error("Error while fetching marker"));
            };

            this.setBusy(true);

            this.orderUserStatus = this._component.scenario.getOrderUserStatus();

            this._requestHelper.getData(["OperationList", "operUserStatus"],
                this.getOperationQueryParams(),
                load_success,
                load_error,
                true,
                "calendar");
        };

        CalendarViewModel.prototype.refreshData = function (successCb) {
            this.fetchData(successCb);
        };

        CalendarViewModel.prototype.constructor = CalendarViewModel;

        return CalendarViewModel;
    }
);
