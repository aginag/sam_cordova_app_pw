sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "SAMMobile/components/WorkOrders/controller/workOrders/calendar/CalendarViewModel",
    "SAMMobile/helpers/formatter",
    "SAMMobile/components/WorkOrders/helpers/formatterComp"
], function (Controller, MessageBox, CalendarViewModel, formatter, formatterComp) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.calendar.Calendar", {
        formatter: formatter,
        formatterComp: formatterComp,

        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("workOrdersCalendar").attachPatternMatched(this._onCustomRouteMatched, this);

            sap.ui.getCore().getEventBus().subscribe("workOrdersCalendar", "refreshRoute", this.onRefreshRoute, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.calendarViewModel = new CalendarViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.calendarViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.setCalenderPlanningViews();

            this.getView().setModel(this.calendarViewModel, "calendarViewModel");
        },

        onAfterRendering: function() {
            var oCalendarGrid = this.getView().byId("SPC2").getAggregation("_grid");

            // override the method which is causing issue https://msc-mobile.atlassian.net/browse/PFAL-814
            // the origin method is only checking for equals 0 - but the modulo comparison only will return 0 in case both dates are within the same time zone (winter vs. summer time)
            // in case the start date is in summer time and the end date is in winter time, the modulo comparison results "3600000"
            // in case the start date is in winter time and the end date is in summer time, the modulo comparison results "82800000"
            // as a result, the check must be enhanced by these two additional checks
            oCalendarGrid._isEndTime0000 = function (t, e) {
                var P = 864e5;
                var iResult = (e.getTime() - t.getTime()) % P;
                return iResult === 0 || iResult === 3600000 || iResult === 82800000;
            };

            oCalendarGrid.addEventDelegate({
                onAfterRendering: function() {
                    var oFullDayRow = this.$().find('.sapMSinglePCBlockersRow');
                    var oGrid = this.getParent().$().find('.sapMSinglePCGrid');

                    if (oFullDayRow[0] && oGrid[0]) {
                        $(oFullDayRow[0]).appendTo(oGrid[0]);
                    }
                }
            }, oCalendarGrid);
        },

        _onCustomRouteMatched: function (oEvent) {
            const routeName = oEvent.getParameter('name');

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));
            this.globalViewModel.setCurrentRoute(routeName);
            this.globalViewModel.setComponentHeaderText(this.component.i18n.getText("calendar"));

            if (this.calendarViewModel.needsReload()) {
                this.loadCalendar();
            }

        },

        onRefreshRoute: function () {
            this.loadCalendar(true);
        },

        onExit: function() {
            sap.ui.getCore().getEventBus().unsubscribe("workOrdersCalendar", "refreshRoute", this.onRefreshRoute, this);
        },

        handleAppointmentSelect: function(oEvent) {
            var oAppointment = oEvent.getParameter("appointment") && oEvent.getParameter("appointment").getBindingContext("calendarViewModel").getObject();

            if (oAppointment) {
                this.oRouter.navTo("workOrderDetailRoute", {
                    id: oAppointment.ORDERID,
                    operId: oAppointment.ACTIVITY,
                    persNo: this.calendarViewModel.getUserInformation().personnelNumber ? this.calendarViewModel.getUserInformation().personnelNumber : '00000000',
                    query: {
                        directTo: "workOrderDetailOverview"
                    }
                });
            }
        },

        setCalenderPlanningViews: function(){
            this.calendarControl = this.byId("SPC2");

            var oDayView = new sap.m.SinglePlanningCalendarDayView({
                title: this.component.i18n.getText("CALENDAR_DAY_BUTTON_TEXT"),
                key: "DayView"
            });
            var oWorkWeekView = new sap.m.SinglePlanningCalendarWorkWeekView({
                key: "WorkWeekView",
                title:  this.component.i18n.getText("CALENDAR_WORK_WEEK_BUTTON_TEXT")
            });
            var oWeekView = new sap.m.SinglePlanningCalendarWeekView({
                key: "WeekView",
                title:  this.component.i18n.getText("CALENDAR_WEEK_BUTTON_TEXT")
            });
            if(sap.ui.Device.system.phone){
                this.calendarControl.addView(oDayView);
            } else {
                this.calendarControl.addView(oWeekView);
                this.calendarControl.addView(oWorkWeekView);
                this.calendarControl.addView(oDayView);
            }
        },

        loadCalendar: function (bSync) {
            bSync = bSync == undefined ? false : bSync;

            if (bSync) {
                this.calendarViewModel.refreshData();
            } else {
                this.calendarViewModel.fetchData(function () {

                });
            }
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        onNavBack: function () {
            window.history.go(-1);
        },

        toggleFullDay: function () {
            this.calendarViewModel.setProperty("/view/fullDay", !this.calendarViewModel.getProperty("/view/fullDay"));
        }

    });

});