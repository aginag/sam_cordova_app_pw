sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "SAMMobile/components/WorkOrders/controller/workOrders/map/MapMarkerViewModel",
    "SAMMobile/components/WorkOrders/controller/workOrders/operationList/OperationListViewModel",
    "sap/m/Dialog",
    "sap/m/Button",
    "sap/m/Table",
	"sap/m/List",
	"sap/m/StandardListItem",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment"
], function (Controller, MapMarkerViewModel, OperationListViewModel, Dialog, Button, Table, List, StandardListItem, JSONModel, Fragment) {
    "use strict";

    return Controller.extend("SAMMobile.components.WorkOrders.controller.workOrders.map.MapMarker", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            this.oRouter.getRoute("mapsMarkerRoute").attachPatternMatched(this._onCustomRouteMatched, this);

            this.component = this.getOwnerComponent();
            this.globalViewModel = this.component.globalViewModel;

            this.mapMarkerViewModel = new MapMarkerViewModel(this.component.requestHelper, this.globalViewModel, this.component);
            this.mapMarkerViewModel.setErrorCallback(this.onViewModelError.bind(this));

            this.initialized = false;
            this.getView().setModel(this.mapMarkerViewModel, "mapMarkerViewModel");

        },

        _onCustomRouteMatched: function (oEvent) {

            var equipment;
            var orderId;
            var selectedTabKey;
            var filteredOperation;
            if(oEvent.mParameters.arguments['?query'] !== undefined) {
                equipment = oEvent.mParameters.arguments['?query'].equipment;
                orderId = oEvent.mParameters.arguments['?query'].orderid;
                selectedTabKey = oEvent.mParameters.arguments['?query'].selectedTabKey;
                filteredOperation = JSON.parse(decodeURIComponent(oEvent.mParameters.arguments['?query'].filteredOperation));
            }

            this.mapMarkerViewModel.setSelectedTabKey(selectedTabKey);
            this.mapMarkerViewModel.setFilterOperation(filteredOperation);

            this.globalViewModel.setComponentBackNavEnabled(true);
            this.globalViewModel.setComponentBackNavFunction(this.onNavBack.bind(this));

            this.loadMarker(filteredOperation);
            // check if there is some Gis Missing

            if (this.mapMarkerViewModel.getMissingGis().length > 0){
            	this._getGisMissingDialog().open();
            }

        },

        loadMasterData: function (successCb) {
            if (this.component.masterDataLoaded) {
                successCb();
                return;
            }

            this.component.loadMasterData(true, successCb);
        },

        onViewModelError: function (err) {
            MessageBox.error(err.message);
        },

        loadMarker: function (oPerations) {
            this.mapMarkerViewModel.buildGISdata(oPerations);
        },

        removeLeadingZeros: function(number) {
            if (number !== "" && number !== undefined && typeof number !== "number") {
                return isNaN(number) ? number : parseInt(number, 10);
            }
            return number;
        },

        onNavBack: function () {
        this.oRouter.navTo("workOrderOperations");
        },

        onNavToOrderOverView: function(oEvent){
        	
            this.oRouter.navTo("workOrderDetailRoute", {
                id: this.mapMarkerViewModel.getOrderId(),
                operId: this.mapMarkerViewModel.getOperId(),
                persNo: this.mapMarkerViewModel.getUserInformation().personnelNumber,
                query: {
                    directTo: "workOrderDetailOverview"
                }
            });
        },

        onMapMarkerNavigate: function (oEvent) {

            var markerObject = oEvent.mParameters.markerObject;

            this.mapMarkerViewModel.setOrderId(markerObject.ORDERID);
            this.mapMarkerViewModel.setOperId(markerObject.ACTIVITY);
            
            this.pressDialog = null;
            var markerModel = new JSONModel({marker: markerObject});

            if (!this.pressDialog) {

                this.pressDialog = sap.ui.xmlfragment("SAMMobile.components.WorkOrders.view.workOrders.map.MapMarkerPopup", this);

                //to get access to the global model
                this.getView().addDependent(this.pressDialog);
            }

            this.pressDialog.setModel(markerModel, "markerModel");

            this.pressDialog.open();
        },

        onClose: function(){
            this.pressDialog.close();
        },
        
        _getGisMissingDialog: function () {
            var that = this;

            var _oGisMissingCodeDialog = new Dialog({
                title: that.component.i18n.getText("GIS_TITLE"),
                contentWidth: "60%",
				contentHeight: "60%",
				stretch:true,
                content: 
                new List({
                	headerText: that.component.i18n.getText("GIS_TEXT"),
					items: {
						path: "/GISMissing",
						template: new StandardListItem({
							title: "{FUNCT_LOC}",
							description: "{subTitle}"
						})
					}
                }),
                
                endButton: new Button({
                    icon: "sap-icon://accept",
                    press: function () {
	                    this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            _oGisMissingCodeDialog.setModel(new JSONModel({
            	GISMissing: that.mapMarkerViewModel.getMissingGis()
            }));
            
            return _oGisMissingCodeDialog;
        },

    });

});