sap.ui.define([
        "SAMContainer/models/ViewModel"
    ],

    function (ViewModel) {
        "use strict";

        // constructor
        function MapMarkerViewModel(requestHelper, globalViewModel, component) {
            ViewModel.call(this, component, globalViewModel, requestHelper);
            this.attachPropertyChange(this.getData(), this.onPropertyChanged.bind(this), this);
        }

        MapMarkerViewModel.prototype = Object.create(ViewModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {
                        marker: [],
                        gisMissing: [],
                        selectedTabKey: "mine",
                        filteredOperation: [],
                        selectedMarker: null,
                        view: {
                            busy: false,
                            markerSelected: false
                        },
                        orderId: null,
                        operId: null
                    };
                }
            },

            getUser: {
                value: function () {
                    return this.getUserInformation().personnelNumber;
                }
            },

            setOrderId: {
                value: function (orderId) {
                    this.setProperty("/orderId", orderId);
                }
            },

            setOperId: {
                value: function (operId) {
                    this.setProperty("/operId", operId);
                }
            },
            
            getOrderId: {
                value: function () {
                    return this.getProperty("/orderId");
                }
            },
            
            getOperId: {
                value: function () {
                    return this.getProperty("/operId");
                }
            },
            
            getMarkerQueryParams: {
                value: function () {
                    return {
                        persNo: this.getUserInformation().personnelNumber,
                        language: this.getLanguage(),
                        startCons: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                        yesterday: moment().add(-1,'days').format("YYYY-MM-DD"),
                        doneStatus: this.orderUserStatus.done.STATUS,
                        prioStatus: this.orderUserStatus.prio.STATUS,
                        teamGuids: this.getUserInformation().teams.map(function (oTeam) { return "'" + oTeam.teamGuid + "'"; })
                    };
                }
            },

            getSelectedTabKey: {
                value: function () {
                    return this.getProperty("/selectedTabKey");
                }
            },

            setSelectedTabKey: {
                value: function (selectedTabKey) {
                    this.setProperty("/selectedTabKey", selectedTabKey);
                }
            },
            
            getMissingGis: {
                value: function () {
                    return this.getProperty("/gisMissing");
                }
            }, 
            
            setFilterOperation: {
                value: function (filteredOperation) {
                    this.setProperty("/filteredOperation", filteredOperation);
                }
            },

            getScenario: {
                value: function () {
                    return this._component.scenario;
                }
            },

            handleListGrowing: {
                value: function () {
                    this.fetchData(function () {
                    }, true);
                }
            },

            setBusy: {
                value: function (bBusy) {
                    this.setProperty("/view/busy", bBusy);
                }
            }
        });

        MapMarkerViewModel.prototype.orderUserStatus = null;

        MapMarkerViewModel.prototype.onPropertyChanged = function (changeEvent, modelData) {

        };

        MapMarkerViewModel.prototype.startNavigation = function (address, app) {
            this._component.rootComponent.navigator.navTo(address, function () {
            }, function () {
            }, app);
        };

        MapMarkerViewModel.prototype.setNewData = function (data, gisMissing) {
             this.setProperty("/marker", data);
             this.setProperty("/gisMissing", gisMissing);
        };

        MapMarkerViewModel.prototype.buildGISdata = function (data) {
            this.setBusy(true);
            var gisData = [];
            var gisMissing = [];
 
        	data.forEach(function (oData) {   
            	var gis = {};
            	gis.text = oData.ORDERID;
            	gis.title = oData.DESCRIPTION;
                gis.subTitle = oData.FUNCLOC_DESC;
                gis.ORDERID = oData.ORDERID;
                gis.ACTIVITY = oData.ACTIVITY;
                gis.FUNCT_LOC = oData.FUNCT_LOC;
                gis.POSTAL = oData.POST_CODE;
                gis.STREET = oData.STREET;
                gis.COUNTRY = oData.COUNTRY;
                gis.CITY = oData.CITY;
                gis.latitude = oData.latitude;
                gis.longitude = oData.longitude;	
                
	            if (oData.latitude && parseInt(oData.latitude) > 0 && oData.longitude && parseInt(oData.longitude) > 0) {
	                gisData.push(gis);
	            }else{
	            	// in case there is not GIS information, we have to notify the User for that
	            	gisMissing.push(gis);
	            }
        	});
		   
        	this.setNewData(gisData, gisMissing);
        	this.setBusy(false);
        };

        MapMarkerViewModel.prototype.constructor = MapMarkerViewModel;

        return MapMarkerViewModel;
    }
);
