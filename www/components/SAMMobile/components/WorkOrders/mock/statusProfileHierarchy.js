sap.ui.define([],
    function () {
        "use strict";
        return {
            statusSteps: [
                {
                    id: 1,
                    STATUS: "E0001",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Accept",
                    UI5_ICON: "sap-icon://accept"
                },
                {
                    id: 2,
                    STATUS: "E0002",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Reject",
                    UI5_ICON: "sap-icon://decline"
                },
                {
                    id: 3,
                    STATUS: "E0003",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Enroute",
                    UI5_ICON: "sap-icon://begin"
                },
                {
                    id: 4,
                    STATUS: "E0004",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "On Hold",
                    UI5_ICON: "sap-icon://media-pause"
                },
                {
                    id: 5,
                    STATUS: "E0005",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Onsite",
                    UI5_ICON: "sap-icon://factory"
                },
                {
                    id: 6,
                    STATUS: "E0007",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Work Finished",
                    UI5_ICON: "sap-icon://kpi-managing-my-area"
                },
                {
                    id: 7,
                    STATUS: "E0006",
                    STATUS_PROFILE: "SAMSTOP",
                    STATUS_DESC: "Complete",
                    UI5_ICON: "sap-icon://complete"
                }
            ],
            statusRelations: [
                {
                    PARENT_ID: 1,
                    CHILD_ID: 2
                },
                {
                    PARENT_ID: 1,
                    CHILD_ID: 4
                },
                {
                    PARENT_ID: 1,
                    CHILD_ID: 3
                },
                {
                    PARENT_ID: 3,
                    CHILD_ID: 2
                },
                {
                    PARENT_ID: 3,
                    CHILD_ID: 5
                },
                {
                    PARENT_ID: 3,
                    CHILD_ID: 4
                },
                {
                    PARENT_ID: 5,
                    CHILD_ID: 2
                },
                {
                    PARENT_ID: 5,
                    CHILD_ID: 6
                },
                {
                    PARENT_ID: 5,
                    CHILD_ID: 4
                },
                {
                    PARENT_ID: 4,
                    CHILD_ID: 2
                },
                {
                    PARENT_ID: 4,
                    CHILD_ID: 3
                },
                {
                    PARENT_ID: 4,
                    CHILD_ID: 5
                },
                {
                    PARENT_ID: 6,
                    CHILD_ID: 7
                }
            ]
        };
    });