sap.ui.define(["sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel", "sap/m/MessageBox" ,"SAMMobile/helpers/RequestHelperCore", "SAMMobile/components/WorkOrders/helpers/StopwatchManager", "SAMMobile/components/WorkOrders/helpers/Scenario", "SAMMobile/components/WorkOrders/helpers/scenarioConfig", "SAMMobile/components/WorkOrders/udb/RequestHelperExtensions", "SAMMobile/helpers/statusProfile/StatusHierarchy", "SAMMobile/models/CustomizingModel", "SAMMobile/components/WorkOrders/helpers/AfterSyncOperations", "SAMContainer/models/ViewModel"
    ],

    function (UIComponent,
              JSONModel,
              MessageBox,
              RequestHelperCore,
              StopwatchManager,
              Scenario,
              scenarioConfig,
              RequestHelperExtensions,
              StatusHierarchy,
              CustomizingModel,
              AfterSyncOperations,
              ViewModel) {
        "use strict";

        return UIComponent.extend("SAMMobile.components.WorkOrders.Component", {
            metadata: {
                manifest: "json"
            },

            customizingTables: ["SAMUsers", "UserStatusMl", "WorkCenterMl", "NotificationType", "PriorityTypes", "ActivityTypeMl", "MeasurementUnit", "StorageLocation", "OrderTypes", "ZPMC_REJ_REASONS", "CodeMl"],
            columnDefinitions: [],

            /**
             * The component is initialized by UI5 automatically during the startup
             * of the app and calls the init method once.
             *
             * @public
             * @override
             */
            init: function () {
                var that = this;
                // call the base component's init function
                UIComponent.prototype.init.apply(this, arguments);

                sap.ui.getCore().getEventBus().subscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().subscribe("home", "onLogout", this.onLogout, this);
                jQuery.sap.log.setLevel(jQuery.sap.log.Level.INFO);

                this.rootComponent = sap.ui.getCore().byId("samComponent").getComponentInstance();
                this.rootComponent.scenarioComponent = this;

                this.i18n = this.getModel("i18n").getResourceBundle();
                this.i18n_core = this.getModel("i18n_core").getResourceBundle();

                var queryExtensions = this._getRequestHelperExtensions();
                this.requestHelper = new RequestHelperCore(queryExtensions.tableColumns, queryExtensions.queries);
                this.formatter = this.rootComponent.formatter;

                this.globalViewModel = this.rootComponent.globalViewModel;
                this.customizingModel = new CustomizingModel(this.getManifestObject(), this.requestHelper, this.customizingTables, "SAM_WO_CUSTOMIZING_MODEL");

                this.statusHierarchy = null;
                this.scenario = null;
                this.stopwatchManager = new StopwatchManager(this, new ViewModel(this, this.globalViewModel, this.requestHelper, ""));
                this.afterSyncOperations = new AfterSyncOperations(this);
                this.masterDataLoaded = false;
                this.routeNumber = 0;

                this.setModel(this.customizingModel.model, "customizingModel");
                this.setModel(this.rootComponent.getModel("device"), "device");

                this.getRouter().initialize();
            },

            setBusyOn: function () {
                this.rootComponent.globalViewModel.model.setProperty('/busy', true);
            },

            setBusyOff: function () {
                this.rootComponent.globalViewModel.model.setProperty('/busy', false);
            },

            getBarcodeScannerSettings: function() {
                var samConfig = this.getManifestObject().getEntry("sap.app").SAMConfig;
                return samConfig.barcodeScanner;
            },

            onBeforeRendering: function () {
                var sheet = document.createElement('style');
                sheet.innerHTML = ".box-center {\n" +
                    "\ttext-align:center;\n" +
                    "}\n" +
                    "\n" +
                    ".vertical-center {\n" +
                    "\tjustify-content: center;\n" +
                    "}\n" +
                    "\n" +
                    ".font-bold {\n" +
                    "\tfont-weight: bold!important;\n" +
                    "}\n" +
                    "\n" +
                    ".statusBadge {\n" +
                    "\tbackground: #346187;\n" +
                    "\tpadding: .4rem .5rem;\n" +
                    "\tborder-radius: 0.5rem;\n" +
                    "\tborder: 1px solid #dedede;\n" +
                    "\tcolor: white;\n" +
                    "\tfont-weight: bold;\n" +
                    "}\n" +
                    "\n" +
                    ".grid-vertical-center {\n" +
                    "\tdisplay: flex;\n" +
                    "\talign-items: center;\n" +
                    "}\n" +
                    "\n" +
                    ".description-text {\n" +
                    "\tfont-size: 1.3rem!important;\n" +
                    "\tfont-weight:bold!important;\n" +
                    "}\n" +
                    "\n" +
                    ".float-right {\n" +
                    "\tfloat:right;\n" +
                    "}\n" +
                    "\n" +
                    ".sapMBtn[data-highlight=\"true\"] {\n" +
                    "\tborder-bottom: 2px solid red;\n" +
                    "}\n" +
                    "\n" +
                    ".listIconVBox {\n" +
                    "\twidth: 1.5rem;\n" +
                    "}\n" +
                    "\n" +
                    ".orderListItemFlexBox > div:nth-child(2) {\n" +
                    "\twidth:5rem;\n" +
                    "}\n" +
                    "\n" +
                    ".orderListItemTitle {\n" +
                    "\tcolor: #cb8f00!important;\n" +
                    "\tfont-size: 1rem;\n" +
                    "}\n" +
                    "\n" +
                    ".noMarginTop {\n" +
                    "\tpadding-top: 0;\n" +
                    "}\n" +
                    "\n" +
                    ".orderListItemGrid .sapUiRespGridHSpace1{\n" +
                    "\tpadding: 0 0.5rem 0 0!important;\n" +
                    "}\n" +
                    "\n" +
                    "/*\n" +
                    "Disables manual text entries for the SAPUI5 StepInput control by default\n" +
                    "*/\n" +
                    ".sapMStepInput-CTX .sapMInput .sapMInputBaseInner:not(.sapMInputBaseReadonlyInner) {\n" +
                    "\t-webkit-user-select: none!important;\n" +
                    "\t-moz-user-select: none!important;\n" +
                    "\t-ms-user-select: none!important;\n" +
                    "\tuser-select: none!important;\n" +
                    "}\n" +
                    "\n" +
                    ".sg3_orderlist .sapMLIB[data-dueState=\"due\"] {\n" +
                    "\tborder-color: red !important;\n" +
                    "\tborder: 2px solid;\n" +
                    "}\n" +
                    ".sg3_orderlist .sapMLIB[data-dueState=\"notDue\"] {\n" +
                    "\t\n" +
                    "}\n" +
                    "\n" +
                    "[data-ownOperation=\"true\"]{\n" +
                    "\tcolor: #cb8f00!important;\n" +
                    "\tfont-size: 1rem;\n" +
                    "\tfont-weight: bold;\n" +
                    "}\n" +
                    "\n" +
                    ".fixtermin {\n" +
                    "\tcolor: white!important;\n" +
                    "\tbackground-color: red!important;\n" +
                    "}\n" +
                    "\n" +
                    ".fixtermin > .sapUiIcon {\n" +
                    "\tfont-size: 1.5rem!important;\n" +
                    "}\n" +
                    "\n" +
                    ".ownDates {\n" +
                    "\tcolor: white!important;\n" +
                    "\tbackground-color: #6e8c1f!important;\n" +
                    "}\n" +
                    "\n" +
                    ".ownDates > .sapUiIcon {\n" +
                    "\tfont-size: 1.5rem!important;\n" +
                    "}\n" +
                    "\n" +
                    ".spannung {\n" +
                    "\tcolor: black!important;\n" +
                    "\tbackground-color: yellow!important;\n" +
                    "}\n" +
                    "\n" +
                    ".spannung > .sapUiIcon {\n" +
                    "\tfont-size: 1.5rem!important;\n" +
                    "}\n" +
                    "\n" +
                    "[data-colorexpandicon=\"red\"] .sapMPanelExpandableIcon{\n" +
                    "\tcolor: red!important;\n" +
                    "}\n" +
                    "\n" +
                    "[data-colorexpandicon=\"orange\"] .sapMPanelExpandableIcon{\n" +
                    "\tcolor: orange!important;\n" +
                    "}\n" +
                    "\n" +
                    "[data-colorexpandicon=\"gray\"] .sapMPanelExpandableIcon{\n" +
                    "\tcolor: gray!important;\n" +
                    "}\n" +
                    "\n" +
                    "[data-colorexpandicon=\"green\"] .sapMPanelExpandableIcon{\n" +
                    "\tcolor: green!important;\n" +
                    "}\n" +
                    "\n" +
                    ".minMaxListItem .sapMILILabel {\n" +
                    "\twidth: 50%\n" +
                    "}\n" +
                    "\n" +
                    ".minMaxListItem .sapMInputBaseReadonlyWrapper {\n" +
                    "\tborder: hidden\n" +
                    "}\n" +
                    "\n" +
                    ".calendarFullDayAtBottom .sapMSinglePCGridContent {\n" +
                    "\tpadding-top: 10px;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblAlternateRowColorsPopin>:nth-child(4n-1), .operationListCompact .sapMListTblAlternateRowColorsPopin>:nth-child(4n) {\n" +
                    "\tbackground-color: #cecece;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblCell {\n" +
                    "\tpadding: 0 0 0 0;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblCell .sapMText {\n" +
                    "\tpadding-top: 0.25rem;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblSubCnt {\n" +
                    "\tpadding: 0 0 0 0.875rem;\n" +
                    "\tmargin-top: -3rem !important;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblHighlightCell+.sapMListTblCell {\n" +
                    "\tpadding-left: 0.875rem;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMLIBSelected+.sapMListTblSubRow, .operationListCompact .sapMLIBSelected.sapMListTblRowAlternate+.sapMListTblSubRow {\n" +
                    "\tbackground-color: #e8eff6!important;\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMLIB.sapMLIBSelected {\n" +
                    "\tbackground-color: #e8eff6!important;\n" +
                    "}\n" +
                    "\n" +
                    "\n" +
                    "@media screen and (min-width: 700px) {\n" +
                    "\t.operationListButton {\n" +
                    "\t\tposition: absolute;\n" +
                    "\t\tmargin-top: 20px;\n" +
                    "\t\tmargin-left: -20px;\n" +
                    "\t}\n" +
                    "}\n" +
                    "@media screen and (max-width: 700px) {\n" +
                    "\t.operationListButton.sapMBtnBase.sapMBtn.darkButton {\n" +
                    "\t\tpadding-top: 35px!important;\n" +
                    "\t\tmargin-right: 20px;\n" +
                    "\t\tmargin-left: -20px;\n" +
                    "\t}\n" +
                    "}\n" +
                    "\n" +
                    "@media screen and (min-width: 700px) {\n" +
                    "\t.operationListButton2 {\n" +
                    "\t\tmargin-top: -10px;\n" +
                    "\t\tposition: absolute;\n" +
                    "\t\tmargin-left: -20px;\n" +
                    "\t}\n" +
                    "}\n" +
                    "\n" +
                    "@media screen and (max-width: 700px) {\n" +
                    "\t.operationListButton2.sapMBtnBase.sapMBtn.darkButton {\n" +
                    "\t\tpadding-top: 35px!important;\n" +
                    "\t\tmargin-right: 20px;\n" +
                    "\t\tmargin-left: -20px;\n" +
                    "\t}\n" +
                    "}\n" +
                    "\n" +
                    ".operationListCompact .sapMListTblRow {\n" +
                    "    \theight: 4rem !important;\n" +
                    "}\n" +
                    "\n" +
                    ".marignForOrderID {\n" +
                    "\tmargin-bottom: 4.7rem !important;\n" +
                    "}\n" +
                    "\n" +
                    ".sapMSticky>.sapMListHdrTBar.sapMTB-Transparent-CTX {\n" +
                    "\tbackground: white!important;\n" +
                    "}.startStopButtonDetail {\n" +
                    "\tmargin-right: 20px;\n" +
                    "}\n" +
                    ".showmanToolbar {\n" +
                    "\tbackground: #3c5b70 !important;\n" +
                    "}\n" +
                    "\n" +
                    ".showmanToolbar .sapMBtnInner{\n" +
                    "\tbackground: #3c5b70!important;\n" +
                    "\tborder: none!important;\n" +
                    "\tcolor: white!important;\n" +
                    "}\n" +
                    "\n" +
                    ".showmanToolbar .sapMTitle{\n" +
                    "\tcolor: white !important;\n" +
                    "}\n" +
                    "\n" +
                    ".showmanToolbar .sapMBtnIcon{\n" +
                    "\tcolor: white !important;\n" +
                    "}\n" +
                    ".sapMSinglePCRowHeader, .sapMSinglePCNowMarkerText{\n" +
                    "\ttext-align: right !important;\n" +
                    "}\n";
                document.body.appendChild(sheet);
            },
            exit: function () {
                sap.ui.getCore().getEventBus().unsubscribe("sync", "onAfterSync", this.onAfterSync, this);
            },

            onAfterSync: function (bTriggeredBySync) {
                var that = this;
                bTriggeredBySync = bTriggeredBySync ? bTriggeredBySync : false;

                // Reload the Team Information for the user. during the sync the user can be link to another teams
                var successCb = function () {
                    that.changeOrderUserStatuses().then(function (bChanges) {
                        if (bChanges) {
                            that.sync("S");
                            return;
                        }

                        if (bTriggeredBySync) {
                            that.executingSyncRefresh();
                        }
                    }).catch(function (err) {
                        that.executingSyncRefresh();
                    });
                };

                this.handleTeamUpdates(successCb);
            },

            handleTeamUpdates: function(successCb) {
                var that = this;
                    var success = function (data) {
                        // Realod Team Information
                        var aTeams = [];
                        data.forEach(function (entry) {

                            aTeams = aTeams.concat({
                                teamGuid: entry.TEAM_GUID,
                                isTeamLead: entry.RES_FLAG_RESP
                            });
                        });

                        that.globalViewModel.model.setProperty('/teams', aTeams);
                        successCb();
                    };
                    var load_error = jQuery.proxy(this.load_error, this);
                    this.requestHelper.searchData("SAMUsers", {
                        "userName": that.globalViewModel.model.getProperty("/userName")
                    }, success, load_error);
            },

            handleOrderUserStatusErrors: function () {
                var that = this;

                return new Promise(function (resolve, reject) {
                    try {
                        that.afterSyncOperations.handleOrderUserStatusErrors(resolve);
                    } catch (err) {
                        reject(err);
                    }
                });
            },

            changeOrderUserStatuses: function () {
                var that = this;
                var afterSyncStatuses = this.scenario.getAfterSyncStatuses();

                return new Promise(function (resolve, reject) {
                    try {
                        that.afterSyncOperations.changeOrderUserStatuses(afterSyncStatuses.fromStatus, afterSyncStatuses.toStatus, function(bChanges) {
                            resolve(bChanges);
                        });
                    } catch (err) {
                        reject(err);
                    }
                });
            },

            onLogout: function () {
                //Clearing localstorage
                this.customizingModel.resetCache();
                localStorage.removeItem(this.customizingModel.customizingModelLsKey);
                this.masterDataLoaded = false;
            },

            executingSyncRefresh: function () {
                var that = this;

                this.loadMasterData(true, function () {
                    switch (that.globalViewModel.model.getProperty("/currentRoute")) {
                        case "workOrders":
                            sap.ui.getCore().getEventBus().publish("workOrdersRoute", "refreshRoute");
                            break;
                        case "workOrderOperations":
                            sap.ui.getCore().getEventBus().publish("workOrderOperations", "refreshRoute");
                            break;
                        case "workOrderDetailOverview":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailRoute", "refreshRoute");
                            break;
                        case "workOrderDetailDescription":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailDescription", "refreshRoute");
                            break;
                        case "workOrderDetailObjects":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailObjects", "refreshRoute");
                            break;
                        case "workOrderDetailMaterial":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailMaterial", "refreshRoute");
                            break;
                        case "workOrderDetailTime":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailTime", "refreshRoute");
                            break;
                        case "workOrderDetailMinMax":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailMinMax", "refreshRoute");
                            break;
                        case "workOrderDetailAttachments":
                            sap.ui.getCore().getEventBus().publish("workOrderDetailAttachments", "refreshRoute");
                            break;
                        case "workOrderCheckList":
                            sap.ui.getCore().getEventBus().publish("workOrderCheckListRoute", "refreshRoute");
                            break;
                        case "workOrderDetailOperations":
                        	sap.ui.getCore().getEventBus().publish("workOrderDetailOperations", "refreshRoute");
                            break;
                        case "workOrdersCalendar":
                            sap.ui.getCore().getEventBus().publish("workOrdersCalendar", "refreshRoute");
                            break;
                        default:
                            break;
                    }

                });
            },

            sync: function (syncMode, bStatusUpdate) {
                bStatusUpdate = bStatusUpdate == null || bStatusUpdate == undefined ? false : bStatusUpdate;
                //TODO WORKAROUND, calling sync need to be available in the rootComponent
                var view = this.rootComponent.getRouter().getView("SAMMobile.view.home.HomeToolpage");
                var controller = view.getController();
                var syncModel = this.rootComponent.publicationAutomatedSync;

                controller.onSync(controller, syncMode || "", syncModel);
            },

            loadMasterData: function (hardReset, successCb) {
                var that = this;

                this.setBusyOn();

                Promise.all([this.loadColumnDefinitions(hardReset), this.loadCustomizingModel(hardReset), this.loadStatusFlow()]).then(function (result) {

                    // Set status profile flow/hierarchy
                    that.scenario = new Scenario("SAM", scenarioConfig["SAM"]);
                    that.statusHierarchy = new StatusHierarchy(result[2], [{
                        STATUS: "E0001",
                        STATUS_PROFILE: "ZSAMOPER"
                    }, {
                        STATUS: "E0005",
                        STATUS_PROFILE: "ZSAMOPER"
                    }], this);
                    that.setBusyOff();
                    that.masterDataLoaded = true;
                    successCb();

                }).catch(function (err) {
                    alert(err.message+ "\n" + err.stack);
                });
            },

            loadCustomizingModel: function (bHardReset) {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.customizingModel.loadData(that.globalViewModel.getActiveUILanguage(), bHardReset, resolve, reject);
                });
            },

            loadColumnDefinitions: function (bHardReset) {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.requestHelper.loadColumnDefinitions(that.columnDefinitions, bHardReset, resolve, reject);
                });
            },

            loadScenarioName: function () {
                var that = this;

                return new Promise(function (resolve, reject) {
                    that.requestHelper.getData("Scenario", {
                        persNo: that.globalViewModel.model.getProperty("/personnelNumber")
                    }, resolve, reject);
                });
            },

            loadStatusFlow: function () {
                var that = this;
                return new Promise(function (resolve, reject) {
                    that.requestHelper.getData("StatusFlow", {
                        language: that.globalViewModel.getActiveUILanguage()
                    }, resolve, reject);
                });
            },

            getGrowingListSettings: function (object) {
                var samConfig = this.getManifestObject().getEntry("sap.app").SAMConfig;
                var listConfig = samConfig.growingLists;

                switch (object) {
                    case "selectDialogs":
                        return listConfig.selectDialogs;
                    default:
                        return {};
                }
            },

            getOrderListVariant: function () {
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.orderListVariant;
            },

            getAttachmentScenario: function () {
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentScenario;
            },

            getAttachmentButtonOrderNewPhoto: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonOrderNewPhoto;
            },
            getAttachmentButtonOrderNewAudio: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonOrderNewAudio;
            },
            getAttachmentButtonOrderNewVideo: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonOrderNewVideo;
            },
            getAttachmentButtonOrderNewFile: function(){
                return this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.attachmentButtonOrderNewFile;
            },

            _getRequestHelperExtensions: function () {
                return {
                    tableColumns: Object.assign(RequestHelperExtensions.tableColumns, {}),
                    queries: Object.assign(RequestHelperExtensions.queries, {})
                };
            },

        });
    });
