sap.ui.define([ "sap/ui/model/json/JSONModel", "SAMMobile/models/ObjectModel", "SAMMobile/helpers/formatter", "SAMMobile/helpers/FileSystem", "sap/m/MessageBox" ],

function(JSONModel, ObjectModel, formatter, FileSystem, MessageBox) {
    "use strict";
    // constructor
    function NotificationModel(isLite, language, requestHelper) {
	this.model = new ObjectModel();
	this.model.setObjectName("Notifications");
	this.requestHelper = requestHelper;
	this.customizingModel = {};
	this.fileSystem = new FileSystem();
	this.newObject = false;
	this.columnNames = {};
	this.columnsSQL = {};
	this.loadColumnNames(isLite);
	this.language = language ? language : 'E';
    }

    NotificationModel.prototype = {
	setNotificationId : function(notificationId, mobile_id) {
	    this.notificationId = notificationId;
	    if (this.notificationId == " ") {
		this.newObject = true;
		this.mobileId = mobile_id;
		this.notificationUUID = mobile_id;
	    }
	},
	setObjectNrById : function(success, error) {
	    var that = this;
	    var load_success = function(data) {
		if(data.length === 0 && typeof error === 'function'){
		    error();
		    return;
		}
		if (data.length === 1) {
		    that.objnr = data[0].OBJECT_NO;
		}
		success(that.objnr || "");
	    };
	    var load_error = jQuery.proxy(this.load_error, this)

	    this.requestHelper.getData("NotificationObjectNr", {
		"notifNo" : this.notificationId
	    }, load_success, load_error);

	},
	loadColumnNames : function(isLite) {
	    var successCallBack = jQuery.proxy(this.loadColumnSuccess, this);
	    this.requestHelper.getColumnNames("SAM_NOTIFICATION_COLUMNS", [], successCallBack);
	},
	loadColumnSuccess : function(entityName, data, columnSQLString) {
	    this.columnNames = data;
	    this.columnsSQL = columnSQLString;
	},
	loadData : function(success) {
	    var load_success = jQuery.proxy(function(data) {
		this.setAllData(data);
		// this.loadCheckLists();
		success();
	    }, this);
	    var load_error = jQuery.proxy(this.load_error, this);
	    this.requestHelper.getData([ "NotificationHeaderDetail", "NotificationItemDetail", "NotificationActivityDetail", "NotificationPartnerDetail", "NotificationTaskDetail", "NotificationCauseDetail", "NotificationUsrStatDetail", "NotificationSysStatDetail", "NotificationLongtextDetail", "SAMObjectAttachments" ], {
		"notifNo" : this.notificationId,
		"objNr" : this.objnr,
		"objKey" : this.notificationId,
		"columnName" : "MOBILE_ID",
		"keyValue" : this.mobileId,
		"parentId" : this.mobileId,
		"parentTable" : "Order",
		"isNewObject" : this.newObject,
		'language': this.language
	    }, load_success, load_error, true);

	},
	loadObjectData : function(success) {
	    this.loadData(function() {
		success();
	    });
	},
	load_error : function(result) {

	},
	reload : function() {
	    this.model.getObjectData();
	},
	setData : function(data, tab) {
	    this.model.setObjectData(data, tab);
	},
	setAllData : function(data) {
	    this.onBeforeSettingData(data, this.model.entityName);
	    this.model.setData(data.NotificationHeaderDetail[0]);
	    this.model.setProperty('/items', data.NotificationItemDetail);
	    this.model.setProperty('/longText', data.NotificationLongtextDetail);
	    this.model.setProperty('/tasks', data.NotificationTaskDetail);
	    this.model.setProperty('/activities', data.NotificationActivityDetail);
	    this.model.setProperty('/causes', data.NotificationCauseDetail);
	    this.model.setProperty('/partners', data.NotificationPartnerDetail);
	    this.model.setProperty('/attachments', data.SAMObjectAttachmentsDetail);
	    this.model.setProperty('/userStatus', data.NotificationUsrStatDetail);
	    this.model.setProperty('/systemStatus', data.NotificationSysStatDetail);
	},
	onBeforeSettingData : function(data, entityName) {
	    var that = this;
	    this.notificationUUID = data.NotificationHeaderDetail[0].MOBILE_ID;
	    var newObject = data.NotificationHeaderDetail[0].ACTION_FLAG === 'N';
	    if(newObject){
		data.NotificationHeaderDetail[0].longTextMl = this.getLongText(data, 'QMEL', '');
	    }else{
		data.NotificationHeaderDetail[0].longTextMl = this.getLongText(data, 'QMEL', formatter.removeLeadingZeros(data.NotificationHeaderDetail[0].NOTIF_NO).toString());
	    }
	    //Read items, tasks, activities, and causes and determine
	    //1. the latest key for each
	    //2. Remove the rows with delete_flag
	    //3. Fetch long texts for each
	    //4. Segregate entities that are part of item and those that arent part of any item
	    this.item_key = data.NotificationItemDetail.length > 0 ? data.NotificationItemDetail[data.NotificationItemDetail.length - 1].ITEM_KEY : '0000';
	    this.act_key = data.NotificationActivityDetail.length > 0 ? data.NotificationActivityDetail[data.NotificationActivityDetail.length - 1].ACT_KEY : '0000';
	    this.task_key = data.NotificationTaskDetail.length > 0 ? data.NotificationTaskDetail[data.NotificationTaskDetail.length - 1].TASK_KEY : '0000';
	    this.cause_key = data.NotificationCauseDetail.length > 0 ? data.NotificationCauseDetail[data.NotificationCauseDetail.length - 1].CAUSE_KEY : '0000';
	    
	    data.NotificationItemDetail = data.NotificationItemDetail.filter(function(item){
		return item.DELETE_FLAG == '';
		});
	    data.NotificationActivityDetail = data.NotificationActivityDetail.filter(function(activity){
		return activity.DELETE_FLAG == '';
		});
	    data.NotificationTaskDetail = data.NotificationTaskDetail.filter(function(task){
		return task.DELETE_FLAG == '';
		});
	    data.NotificationCauseDetail = data.NotificationCauseDetail.filter(function(cause){
		return cause.DELETE_FLAG == '';
		});

	    data.NotificationActivityDetail.forEach(function(activity){
		activity.longTextMl = that.getLongText(data, 'QMMA', '0000' + activity.ACT_KEY);
		});
	    data.NotificationTaskDetail.forEach(function(task){
		    task.longTextMl = that.getLongText(data, 'QMSM', '0000' + task.TASK_KEY);
		});
	    data.NotificationCauseDetail.forEach(function(cause){
		    cause.longTextMl = that.getLongText(data, 'QMUR', '0000' + cause.CAUSE_KEY);
		});
	    data.NotificationItemDetail.forEach(function(item) {
		//Get Long text
		item.longTextMl = that.getLongText(data, 'QMFE', formatter.addLeadingZeroes('00000000', item.ITEM_KEY));
		// Extract activities, causes and tasks for each item
		var activities = data.NotificationActivityDetail.filter(function(activity) {
		    return activity.ITEM_KEY == item.ITEM_KEY;
		});
		var tasks = data.NotificationTaskDetail.filter(function(task) {
		    return task.ITEM_KEY == item.ITEM_KEY;
		});
		var causes = data.NotificationCauseDetail.filter(function(cause) {
		    return cause.ITEM_KEY == item.ITEM_KEY;
		});
		item.activities = activities;
		item.tasks = tasks;
		item.causes = causes;

	    });
	    data.NotificationActivityDetail = data.NotificationActivityDetail.filter(function(activity) {
		return activity.ITEM_KEY == "0000";
	    });
	    data.NotificationTaskDetail = data.NotificationTaskDetail.filter(function(task) {
		return task.ITEM_KEY == "0000";
	    });
	    
	    data.NotificationCauseDetail = data.NotificationCauseDetail.filter(function(cause) {
		return cause.ITEM_KEY == "0000";
	    });
	    var dataFolder = that.fileSystem.getRootDataStorage();
            data.SAMObjectAttachments.forEach(function (att) {
                att["folderPath"] = dataFolder;
            });
	    // new Partner
	    /*this.newPartner = this.model.createNewObject(this.columnNames["NotificationPartner"]);
	    this.newPartner.OBJNR = 'QM' + this.notificationId;
	    // new Item
	    this.newItem = this.model.createNewObject(this.columnNames["NotificationItem"]);
	    this.newItem.NOTIF_NO = this.notificationId;
	    this.newItem.DL_CAT_TYP = "B";
	    this.newItem.D_CAT_TYP = "C";
	    this.newItem.activities = [];
	    this.newItem.tasks = [];
	    this.newItem.causes = [];
	    this.newItem.longTextMl = '';
	    // new Activity
	    this.newActivity = this.model.createNewObject(this.columnNames["NotificationActivity"]);
	    this.newActivity.NOTIF_NO = this.notificationId;
	    this.newActivity.ACT_CAT_TYP = "A";
	    this.newActivity.longTextMl = '';
	    // new Task
	    this.newTask = this.model.createNewObject(this.columnNames["NotificationTask"]);
	    this.newTask.NOTIF_NO = this.notificationId;
	    this.newTask.TASK_CAT_TYP = '2';
	    this.newTask.longTextMl = '';
	    // new Cause
	    this.newCause = this.model.createNewObject(this.columnNames["NotificationCause"]);
	    this.newCause.NOTIF_NO = this.notificationId;
	    this.newCause.CAUSE_CAT_TYP = '5';
	    this.newCause.longTextMl = '';
	    // new System status
	    this.newSystemStatus = this.model.createNewObject(this.columnNames["NotificationSysStat"]);
	    this.newSystemStatus.OBJNR = 'QM' + this.notificationId;
	    this.newSystemStatus.CHANGED = 'X';*/
	    
	},
	getLongText: function(data, objType, objKey){
	    return this.model.getLongText(data, objType, objKey,  "NotificationLongtextDetail");
//	    var longTextMl = '';
//	    data.NotificationLongtext.forEach(function(longText) {
//               if (longText.OBJTYPE === objType && longText.OBJKEY === objKey ) {
//        	   longTextMl += longText.TEXT_LINE;
//               }
//            });
//            return longTextMl;
	},
	getNewActivityRow : function(itemPath) {
	    var activityRow = this.model.getClonedObject(this.newActivity);
	    var itemKey = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_KEY');
	    var itemSortNo = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_SORT_NO');
//	    var activitiesData = this.model.getProperty(itemPath + '/activities').filter(function(activity) {
//		return activity.ITEM_KEY === itemKey;
//	    });
//	    var activityKey;
//	    if (activitiesData.length > 0) {
//		activityKey = parseInt(activitiesData[activitiesData.length - 1].ACT_KEY);
//		activityKey = formatter.addLeadingZeroes('0000', (activityKey + 1).toString());
//	    } else {
//		activityKey = '0001';
//	    }
	    var activityKey = parseInt(this.act_key);
	    activityKey = formatter.addLeadingZeroes('0000', (activityKey + 1).toString());
	    this.act_key = activityKey;
	    activityRow.ACT_KEY = activityKey;
	    activityRow.ACT_SORT_NO = activityKey;
	    activityRow.ITEM_KEY = itemKey;
	    activityRow.ITEM_SORT_NO = itemSortNo;

	    return this.model.getClonedObject(activityRow);
	},
	getNewTaskRow : function(itemPath) {
	    var taskRow = this.model.getClonedObject(this.newTask);
	    var itemKey = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_KEY');
	    var itemSortNo = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_SORT_NO');
//	    var tasksData = this.model.getProperty(itemPath + '/tasks').filter(function(task) {
//		return task.ITEM_KEY === itemKey;
//	    });
//	    var taskKey;
//	    if (tasksData.length > 0) {
//		taskKey = parseInt(tasksData[tasksData.length - 1].TASK_KEY);
//		taskKey = formatter.addLeadingZeroes('0000', (taskKey + 1).toString());
//	    } else {
//		taskKey = '0001';
//	    }
	    
	    var taskKey = parseInt(this.task_key);
	    taskKey = formatter.addLeadingZeroes('0000', (taskKey + 1).toString());
	    this.task_key = taskKey;
	    taskRow.ITEM_KEY = itemKey;
	    taskRow.ITEM_SORT_NO = itemSortNo;
	    taskRow.TASK_KEY = taskKey;
	    taskRow.TASK_SORT_NO = taskKey;
	    taskRow.OBJECT_NO = "QA" + this.notificationId + taskKey;

	    return this.model.getClonedObject(taskRow);
	},
	getNewCauseRow : function(itemPath) {
	    var causeRow = this.model.getClonedObject(this.newCause);
	    var itemKey = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_KEY');
	    var itemSortNo = itemPath === '' ? '0000' : this.model.getProperty(itemPath + '/ITEM_SORT_NO');
//	    var causesData = this.model.getProperty(itemPath + '/causes').filter(function(cause) {
//		return cause.ITEM_KEY === itemKey;
//	    });
//	    var causeKey;
//	    if (causesData.length > 0) {
//		causeKey = parseInt(causesData[causesData.length - 1].CAUSE_KEY);
//		causeKey = formatter.addLeadingZeroes('0000', (causeKey + 1).toString());
//	    } else {
//		causeKey = '0001';
//	    }
	    var causeKey = parseInt(this.cause_key);
	    causeKey = formatter.addLeadingZeroes('0000', (causeKey + 1).toString());
	    this.cause_key = causeKey;
	    causeRow.ITEM_KEY = itemKey;
	    causeRow.ITEM_SORT_NO = itemSortNo;
	    causeRow.CAUSE_KEY = causeKey;
	    causeRow.CAUSE_SORT_NO = causeKey;
	    return this.model.getClonedObject(causeRow);
	},
	getNewItemRow : function() {
	    var itemRow = this.model.getClonedObject(this.newItem);
//	    var itemsData = this.model.getProperty('/items').filter(function(item) {
//		return item.ITEM_KEY !== '0000';
//	    });
//	    var itemKey;
//	    if (itemsData.length > 0) {
//		itemKey = parseInt(itemsData[itemsData.length - 1].ITEM_KEY);
//		itemKey = formatter.addLeadingZeroes('0000', (itemKey + 1).toString());
//	    } else {
//		itemKey = '0001';
//	    }
	    var itemKey = parseInt(this.item_key);
	    itemKey = formatter.addLeadingZeroes('0000', (itemKey + 1).toString());
	    this.item_key = itemKey;
	    itemRow.ITEM_KEY = itemKey;
	    itemRow.ITEM_SORT_NO = itemKey;
	    return this.model.getClonedObject(itemRow);
	},
	deleteRow : function(index, path) {
	    var oData = this.model.getProperty(path);
	    oData.splice(index, 1);
	    this.model.setProperty(path, oData);
	},
	refresh : function() {
	    this.model.refresh();
	},
	getNewPartnerRow : function() {
	    return this.model.getClonedObject(this.newPartner);
	},
	getNewSystemStatus : function() {
	    return this.model.getClonedObject(this.newSystemStatus);
	},
	getNewNotificationRow : function(fnCallback) {
	    if (this.newNotification === undefined) {
		// new Header
		this.newNotification = this.model.createNewObject(this.columnNames["NotificationHeader"]);
	    }

	    var notifHeader = this.model.getClonedObject(this.newNotification);
	    // Get new UUID
	    //this.model.getNewUUID(function(newUUID) {
		//newUUID = newUUID.substring(0, 12);
	    	var newUUID = this.model.getRandomUUID(12);
		// Default values
		// Temporary defaulting - needs to be removed
		notifHeader.NOTIF_NO = newUUID;
		notifHeader.OBJECT_NO = 'QM' + newUUID;
		if (fnCallback && typeof fnCallback === "function") {
		    fnCallback(notifHeader);
		}
//	    }, function() {
//		// error handling
//		return null;
//	    });

	},
	createNewNotification : function(notifHeader, fnCallback) {
	    // create new header row
	    var that = this;
	    var sqlStrings = [], success_callback;
	    // loop at column entries
	    //this.model.setData(notifHeader);
	    this.model.buildSQLAndInsert(notifHeader, 'NotificationHeader', this.columnNames['NotificationHeader'], this.columnsSQL['NotificationHeader'], [], [], sqlStrings);
	    if (typeof fnCallback === "function") {
		fnCallback(sqlStrings);
	    }

	},
	insertNotification : function(fnCallback) {
	    var that = this;
	    var model = this.model.oData;
	    var sqlStrings = [], success_callback;
	    // loop at column entries
	    this.model.buildSQLAndInsert(model, 'NotificationHeader', this.columnNames['NotificationHeader'], this.columnsSQL['NotificationHeader'], [ 'NOTIF_NO' ], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.items, 'NotificationItem', this.columnNames['NotificationItem'], this.columnsSQL['NotificationItem'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.tasks, 'NotificationTask', this.columnNames['NotificationTask'], this.columnsSQL['NotificationTask'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.activities, 'NotificationActivity', this.columnNames['NotificationActivity'], this.columnsSQL['NotificationActivity'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.causes, 'NotificationCause', this.columnNames['NotificationCause'], this.columnsSQL['NotificationCause'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.partners, 'NotificationPartner', this.columnNames['NotificationPartner'], this.columnsSQL['NotificationPartner'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.userStatus, 'NotificationUsrStat', this.columnNames['NotificationUsrStat'], this.columnsSQL['NotificationUsrStat'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.systemStatus, 'NotificationSysStat', this.columnNames['NotificationSysStat'], this.columnsSQL['NotificationSysStat'], [], [], sqlStrings);
	    this.model.buildSQLAndInsert(model.longText, 'NotificationLongtext', this.columnNames['NotificationLongtext'], this.columnsSQL['NotificationLongtext'], [], [], sqlStrings);
	    if (fnCallback && typeof fnCallback === "function") {
		success_callback = fnCallback;
	    } else {
		success_callback = function(result) {
		    // jQuery.proxy(this.writeSuccess(tableName), this);
		};
	    }

	    var error_callback = function() {
		MessageBox.alert("Insert failed!");
	    };
	    this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);
	},
	updateNotification : function(fnCallback, errorCallback) {
	    var that = this;
	    var sqlStrings = [], success_callback;

	    if (JSON.stringify(this.model.oData) === JSON.stringify(this.model._prevModelData)) {
		// no data to save
		if (fnCallback && typeof fnCallback === "function") {
		    fnCallback();
		}
		return;
	    }

	    var moveSuccessful = function() {
		that.model.buildSQLAndUpdate('', 'NotificationHeader', that.columnNames['NotificationHeader'], that.columnsSQL['NotificationHeader'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('items', 'NotificationItem', that.columnNames['NotificationItem'], that.columnsSQL['NotificationItem'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('tasks', 'NotificationTask', that.columnNames['NotificationTask'], that.columnsSQL['NotificationTask'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('activities', 'NotificationActivity', that.columnNames['NotificationActivity'], that.columnsSQL['NotificationActivity'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('causes', 'NotificationCause', that.columnNames['NotificationCause'], that.columnsSQL['NotificationCause'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('partners', 'NotificationPartner', that.columnNames['NotificationPartner'], that.columnsSQL['NotificationPartner'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('userStatus', 'NotificationUsrStat', that.columnNames['NotificationUsrStat'], that.columnsSQL['NotificationUsrStat'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('systemStatus', 'NotificationSysStat', that.columnNames['NotificationSysStat'], that.columnsSQL['NotificationSysStat'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('longText', 'NotificationLongtext', that.columnNames['NotificationLongtext'], that.columnsSQL['NotificationLongtext'], sqlStrings, [], [], 'NotificationHeader', that.notificationUUID);
		that.model.buildSQLAndUpdate('attachments', 'SAMObjectAttachments', that.columnNames['SAMObjectAttachments'], that.columnsSQL['SAMObjectAttachments'], sqlStrings, [], [ "LINKID" ], 'NotificationHeader', that.notificationUUID);

		// newMethod
		if (fnCallback && typeof fnCallback === "function") {
		    success_callback = fnCallback;
		} else {
		    success_callback = function(result) {
			// jQuery.proxy(this.writeSuccess(tableName), this);
		    };
		}

		var error_callback = function(result) {
		    MessageBox.alert(result);
		    errorCallback();
		};
		that.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);
	    };

	    var moveError = function() {
		MessageBox.alert("Error occured on attachment moving");
		fnCallback();
	    };

	    this.model.moveAttachmentsToDocDir(moveSuccessful, moveError);

	}
    };

    NotificationModel.prototype.getUserStatuses = function(statusProfile) {
	var userStatus = this.customizingModel.oData.UserStatusMl;
	var taskStatus = {};
	taskStatus.unsorted = [];
	taskStatus.level1 = [];
	taskStatus.level2 = [];
	taskStatus.level3 = [];
	for (var i = 0; i < userStatus.length; i++) {
	    var status = userStatus[i];
	    if (status.STATUS_PROFILE === statusProfile) {
		if (status.USER_STATUS_CODE.length === 1) {
		    taskStatus.level1.push(status);
		} else if (status.USER_STATUS_CODE.length === 2) {
		    status.subStatuses = this.getSubUserStatuses(status.USER_STATUS_CODE, userStatus, statusProfile);
		    taskStatus.level2.push(status);
		} else if (status.USER_STATUS_CODE.length === 3) {
		    taskStatus.level3.push(status);

		} else {
		    taskStatus.unsorted.push(status);
		}

	    }
	}
	return taskStatus;
    };

    NotificationModel.prototype.getSubUserStatuses = function(statusCode, userStatuses, statusProfile) {
	var subStatuses = [];
	var subStatusCodeLength = statusCode.length + 1;
	for (var i = 0; i < userStatuses.length; i++) {
	    var status = userStatuses[i];
	    if (status.STATUS_PROFILE === statusProfile) {
		if (status.USER_STATUS_CODE.length === subStatusCodeLength && status.USER_STATUS_CODE.startsWith(statusCode)) {
		    subStatuses.push(status);
		}
	    }
	}

	return subStatuses;
    };

    NotificationModel.prototype.getNewAttachmentObject = function(fileName, fileExt) {
	var attachmentObj = this.model.createNewObject(this.columnNames["SAMObjectAttachments"]);
	attachmentObj["OBJKEY"] = this.notificationId;
	attachmentObj["TABNAME"] = "NotificationHeader";
	attachmentObj["FILENAME"] = fileName;
	attachmentObj["FILEEXT"] = fileExt;
	attachmentObj["ACTION_FLAG"] = "N";

	return attachmentObj;
    };

    NotificationModel.prototype.checkData = function() {
	return true;
    };
    NotificationModel.prototype.setLongText = function(objType, objKey, textValue) {
	this.model.setLongText(objType, objKey, textValue, "NOTIF_NO", this.notificationId);
    };
    // NotificationModel.prototype.save = function() {
    // // check incoming data if implemented in subclass
    // var checkFlag = this.checkData();
    // if (!checkFlag) {
    // // check failed, show messages on screen!
    // }
    //
    // // call requestHelper to insert/update based on mobile_action flag
    // // if current flag is blank, its a new row. Call insert
    // // if current flag is C or U, then its an update. Call update
    // // if current flag is D, then this is a case that shouldnt happen!!
    // // error out
    // // reload data - here we set prevModelData to current
    // };
    // NotificationModel.prototype.cancelChanges = function() {
    // // reload data
    // };

    var NotificationModel = NotificationModel;

    return NotificationModel;
});

