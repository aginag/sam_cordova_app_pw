sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/models/ObjectModel", "SAMMobile/helpers/FileSystem", "sap/m/MessageBox", "SAMMobile/helpers/JSONHelper"],

    function (JSONModel, ObjectModel, FileSystem, MessageBox, JSONHelper) {
        "use strict";

        // constructor
        function TechnicalObjectsModel(isLite, language, requestHelper) {
            this.model = new ObjectModel();
            this.requestHelper = requestHelper;
            this.copyModel = new ObjectModel();
            this.newObject = false;
            this.columnNames = {};
            this.cacheKeys = {};
            this.loadColumnNames(isLite);
            this.fileSystem = new FileSystem();
            this.language = language ? language : 'E';
        }

        TechnicalObjectsModel.prototype = {
            setObjectName: function (objectName) {
                this.model.setObjectName(objectName);
            },
            setObjectIdAndObjectNumber: function (objectId, objectNumber, mobile_id) {
                this.newObject = false, this.mobileId = "";
                if (this.model.entityName === 'equipments') {
                    this.equnr = objectId;
                    if (this.equnr == " ") {
                        this.newObject = true;
                        this.mobileId = mobile_id;
                    }
                } else {
                    this.tplnr = objectId;
                    if (this.tplnr == " ") {
                        this.newObject = true;
                        this.mobileId = mobile_id;
                    }
                }

                this.objnr = objectNumber;
            },

            loadColumnNames: function (isLite, finishCb) {
                var successCallBack = function (entityName, data, columnSQLString) {
                    this.loadColumnSuccess(entityName, data, columnSQLString);

                    if (finishCb && typeof finishCb === "function") {
                        finishCb();
                    }
                }.bind(this);

                if (isLite) {
                    this.requestHelper.getColumnNames("SAM_EQUIPMENT_COLUMNS_LITE", [], successCallBack);
                } else {
                    this.requestHelper.getColumnNames("SAM_EQUIPMENT_COLUMNS", [], successCallBack);
                }

            },
            loadColumnSuccess: function (entityName, data, columnSQLString) {
                this.columnNames = data;
                this.columnsSQL = columnSQLString;
            },
            loadData: function (success) {
                var load_success = jQuery.proxy(function (data) {
                    this.setAllData(data);
                    success(this.model.oData);
                }, this);
                var load_error = jQuery.proxy(this.load_error, this);
                if (this.model.entityName === 'equipments') {
                    this.requestHelper.getData(["Equipment", "EquipmentCharacs", "EquipmentNotifHist", "EquipmentMeasPoint", "EquipmentInstalled", "EquipmentWarranty", "EquipmentOrderHist", "EquipmentPartner", "EquipmentGIS",
                        "FunctionLocForEqui", "EquipmentUserStatus", "EquipmentSystStatus", "EquipmentLongtext", "SAMObjectAttachments"], {
                        "equipmentNumber": this.equnr,
                        "objKey": this.equnr,
                        "columnName": "EQUNR",
                        "keyValue": this.equnr,
                        "startAt": 1,
                        "noOfRows": 100,
                        "objectNumber": this.objnr,
                        "spras": this.language
                    }, load_success, load_error);
                } else {
                    this.requestHelper.getData(["FunctionLoc", "FunctionLocCharacs", "FunctionLocNotifHist", "FunctionLocMeasPoint", "FunctionLocInstalled", "FunctionLocWarranty", "FunctionLocOrderHist", "FunctionLocPartner", "FunctionLocGIS",
                        "FunctionLocUserStat", "FunctionLocSystStat", "FunctionLocLongtext", "SAMObjectAttachments"], {
                        "funcLocId": this.tplnr,
                        "objKey": this.tplnr,
                        "columnName": "TPLNR",
                        "keyValue": this.tplnr,
                        "startAt": 1,
                        "noOfRows": 100,
                        "objectNumber": this.objnr,
                        "spras": this.language
                    }, load_success, load_error);
                }
            },
            loadNewObjectData: function (success) {
                var load_success = jQuery.proxy(function (data) {
                    this.setAllData(data);
                    success();
                }, this);
                var load_error = jQuery.proxy(this.load_error, this);
                if (this.model.entityName === 'equipments') {
                    this.requestHelper.getData(["Equipment", "EquipmentCharacs", "EquipmentNotifHist", "EquipmentMeasPoint", "EquipmentInstalled", "EquipmentWarranty", "EquipmentOrderHist", "EquipmentPartner", "EquipmentGIS",
                        "FunctionLocForEqui", "EquipmentUserStatus", "EquipmentSystStatus", "EquipmentLongtext"], {
                        "columnName": "MOBILE_ID",
                        "keyValue": this.mobileId,
                        "parentId": this.mobileId,
                        "parentTable": "Equipment",
                        "startAt": 1,
                        "noOfRows": 20,
                        "isNewObject": true,
                        "spras": this.language
                    }, load_success, load_error);
                } else {
                    this.requestHelper.getData(["FunctionLoc", "FunctionLocCharacs", "FunctionLocNotifHist", "FunctionLocMeasPoint", "FunctionLocInstalled", "FunctionLocWarranty", "FunctionLocOrderHist", "FunctionLocPartner", "FunctionLocGIS",
                        "FunctionLocUserStat", "FunctionLocSystStat", "FunctionLocLongtext"], {
                        "columnName": "MOBILE_ID",
                        "keyValue": this.mobileId,
                        "parentId": this.mobileId,
                        "parentTable": "FunctionLoc",
                        "startAt": 1,
                        "noOfRows": 20,
                        "isNewObject": true,
                        "spras": this.language
                    }, load_success, load_error);
                }
            },
            loadObjectData: function (success) {
                if (this.newObject) {
                    this.loadNewObjectData(function () {
                        success();
                    });
                } else {
                    this.loadData(function () {
                        success();
                    });
                }

            },
            load_error: function (result) {
                // do something
            },
            reload: function () {
                this.model.getObjectData();
            },
            setTabData: function (data, tabName) {
                // this.model.setObjectData(data, tab);
                this.model.setProperty('/' + tabName, data);

            },
            setAllData: function (data) {
                // this.model.setData(data);
                this.onBeforeSettingData(data, this.model.entityName);
                if (this.model.entityName === 'equipments') {
                    this.model.setData(data.Equipment[0]);
                    this.model.setProperty('/characteristics', data.EquipmentCharacs);
                    this.model.setProperty('/measurementPoint', data.EquipmentMeasPoint);
                    this.model.setProperty('/notifHistory', data.EquipmentNotifHist);
                    this.model.setProperty('/installed', data.EquipmentInstalled);
                    this.model.setProperty('/warranty', data.EquipmentWarranty);
                    this.model.setProperty('/orderHistory', data.EquipmentOrderHist);
                    this.model.setProperty('/partner', data.EquipmentPartner);
                    this.model.setProperty('/gis', data.EquipmentGIS);
                    this.model.setProperty('/userStatus', data.EquipmentUserStatus);
                    this.model.setProperty('/systStatus', data.EquipmentSystStatus);
                    //this.model.setProperty('/longText', data.EquipmentMl);
                    this.model.setProperty('/attachments', data.SAMObjectAttachments);
                    this.model.setProperty('/longText', data.EquipmentLongtext);
                    this.model.setProperty('/functionLoc', data.FunctionLocForEqui[0]);
                    this.setCopyModelData();
                } else {
                    this.model.setData(data.FunctionLoc[0]);
                    this.model.setProperty('/characteristics', data.FunctionLocCharacs);
                    this.model.setProperty('/measurementPoint', data.FunctionLocMeasPoint);
                    this.model.setProperty('/notifHistory', data.FunctionLocNotifHist);
                    this.model.setProperty('/installed', data.FunctionLocInstalled);
                    this.model.setProperty('/warranty', data.FunctionLocWarranty);
                    this.model.setProperty('/orderHistory', data.FunctionLocOrderHist);
                    this.model.setProperty('/partner', data.FunctionLocPartner);
                    this.model.setProperty('/gis', data.FunctionLocGIS);
                    this.model.setProperty('/userStatus', data.FunctionLocUserStat);
                    this.model.setProperty('/systStatus', data.FunctionLocSystStat);
                    //this.model.setProperty('/longText', data.FunctionLocMl);
                    this.model.setProperty('/attachments', data.SAMObjectAttachments);
                    this.model.setProperty('/longText', data.FunctionLocLongtext);
                    this.setCopyModelData();
                }

            },
            setCopyModelData: function (fnCallback) {
                var equipment = JSONHelper.getClonedObject(this.model.oData);
                var that = this;
//	    this.model.getNewUUID(function(newUUID) {
//		newUUID = newUUID.substring(0, 12);
                var newUUID = this.model.getRandomUUID(18);
                // Default values
                // Temporary defaulting - needs to be removed
                equipment[that.model.entityName === 'equipments' ? "EQUNR" : "TPLNR"] = newUUID;
                equipment[that.model.entityName === 'equipments' ? "EQUIPMENT_DISPLAY" : "FUNCLOC_DISP"] = newUUID;
                equipment.OBJNR = that.model.entityName === 'equipments' ? 'IE' + newUUID : 'IF' + newUUID;
                equipment.characteristics.forEach(function (characteristic) {
                    characteristic.EQUNR = newUUID;
                });
                equipment.gis.forEach(function (gis) {
                    gis.EQUNR = newUUID;
                });
                equipment.longText.forEach(function (longText) {
                    longText.EQUNR = newUUID;
                });
                equipment.measurementPoint.forEach(function (measurementPoint) {
                    measurementPoint.OBJNR = equipment.OBJNR;
                });
                equipment.partner.forEach(function (partner) {
                    partner.OBJNR = equipment.OBJNR;
                });
                equipment.userStatus.forEach(function (userStatus) {
                    userStatus.OBJNR = equipment.OBJNR;
                });
                equipment.warranty.forEach(function (warranty) {
                    warranty.TECHNICAL_OBJECT = equipment.OBJNR;
                });

                equipment.installed.forEach(function (installed) {
                    installed.HEQUI = newUUID;
                });

                equipment.notifHistory = [];
                equipment.orderHistory = [];

                equipment.attachments = [];
                that.copyModel.setData(equipment);

                if (fnCallback && typeof fnCallback === "function") {
                    fnCallback(equipment);
                }
//	    }, function() {
//		// error handling
//		fnCallback(null);
//	    });

                // equipment.MOBILE_ID = "";
                // equipment.ACTION_FLAG = "N";
                // equipment.EQUNR = "";
                // equipment.OBJNR = "";

            },
            onBeforeSettingData: function (data, entityName) {
                var that = this;
                var partnerColumnKey = this.model.entityName === 'equipments' ? "EquipmentPartner" : "FunctionLocPartner";
                var warrantyColumnKey = this.model.entityName === 'equipments' ? "EquipmentWarranty" : "FunctionLocWarranty";

                //this.objectUUID = data[this.model.entityName === "equipments" ? "Equipment" : "FunctionLoc"][0].MOBILE_ID;

                /*this.newPartnerObject = this.model.createNewObject(this.columnNames[partnerColumnKey]);
                this.newPartnerObject.OBJNR = this.objnr;
                this.newPartnerObject.ACTION_FLAG = 'N';

                this.newWarrantyObject = this.model.createNewObject(this.columnNames[warrantyColumnKey]);
                this.newWarrantyObject.TECHNICAL_OBJECT = this.objnr;
                this.newWarrantyObject.ACTION_FLAG = 'N';*/

                // if (this.model.entityName == "equipments" &&
                // data.Equipment.length > 0) {
                // var equi = data.Equipment[0];
                // equi._constructionDate = equi.BAUJJ !== "" && equi.BAUMM !== "" ?
                // (equi.BAUJJ + "/" + equi.BAUMM) : "";
                // equi.longTextMl = this.getLongText(data, 'LTXT', '00000000',
                // 'EquipmentLongtext');
                // }
                var dataFolder = that.fileSystem.getRootDataStorage();
                data.SAMObjectAttachments.forEach(function (att) {
                    att["folderPath"] = dataFolder;
                });
                if (this.model.entityName == "equipments") {
                    var equi = data.Equipment[0];
                    /*if (data.EquipmentMl.length > 0) {
                        equi.SHTXT = data.EquipmentMl[0].SHTXT;
                        equi.TPLNR_SHTXT = data.EquipmentMl[0].TPLNR_SHTXT;
                        equi.HEQUI_SHTXT = data.EquipmentMl[0].HEQUI_SHTXT;
                    }*/
                    equi._constructionDate = equi.BAUJJ !== "" && equi.BAUMM !== "" ? (equi.BAUJJ + "/" + equi.BAUMM) : "";
                    equi._longTextMl = this.getLongText(data, 'LTXT', '00000000', 'EquipmentLongtext');
                    equi._activeState = true;
                    data.EquipmentSystStatus.forEach(function (status) {
                        if (status.SYST_STATUS_CODE === 'INAC' && status.INACT != 'X') {
                            data.Equipment[0]._activeState = false;
                        }
                    });

                } else {
                    var floc = data.FunctionLoc[0];
                    floc._activeState = true;
                    /*if (data.FunctionLocMl.length > 0) {
                        floc.SHTXT = data.FunctionLocMl[0].SHTXT;
                        floc.TPLMA_SHTXT = data.FunctionLocMl[0].TPLMA_SHTXT;
                    }*/
                    floc._longTextMl = this.getLongText(data, 'LTXT', '00000000', 'FunctionLocLongtext');
                    data.FunctionLocSystStat.forEach(function (status) {
                        if (status.SYST_STATUS_CODE === 'INAC' && status.INACT != 'X') {
                            floc._activeState = false;
                        }
                    });

                }
            },
            refresh: function () {
                this.model.refresh();
            },
            insertObject: function (fnCallback, bCopyModel) {
                //determines whether to use this.model or this.copyModel
                bCopyModel = bCopyModel !== undefined ? bCopyModel : false;

                if (this.model.entityName === 'equipments') {
                    this.insertEquipment(fnCallback, bCopyModel);
                } else {
                    // this.insertFuncLocation(fnCallback);
                }
            },
            copyObject: function (fnCallback, bCopyModel) {
                bCopyModel = bCopyModel !== undefined ? bCopyModel : false;

                if (this.model.entityName === 'equipments') {
                    this.copyEquipment(fnCallback, bCopyModel);
                } else {
                    this.copyFuncLocation(fnCallback, bCopyModel);
                }
            },
            insertFuncLocation: function (fnCallback, bCopyModel) {
                var model = bCopyModel ? bCopyModel.oData : this.model.oData;
                // Modify TPLNR, last_modified in all
                // loop at column entries
                var sqlStrings = [], success_callback;
                this.model.buildSQLAndInsert(model, 'FunctionLoc', this.columnNames['FunctionLoc'], this.columnsSQL['FunctionLoc'], ['TPLNR', 'OBJNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.characteristics, 'FunctionLocCharacs', this.columnNames['FunctionLocCharacs'], this.columnsSQL['FunctionLocCharacs'], ['TPLNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.notifHistory, 'FunctionLocNotifHist', this.columnNames['FunctionLocNotifHist'], this.columnsSQL['FunctionLocNotifHist'], ['FUNCLOC'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.measurementPoint, 'FunctionLocMeasPoint', this.columnNames['FunctionLocMeasPoint'], this.columnsSQL['FunctionLocMeasPoint'], ['OBJNR'], [], sqlStrings);
                // this.model.buildSQLAndInsert('installed','FunctionLocInstalled',
                // this.columnNames['FunctionLocInstalled'], this.columnsSQL['FunctionLocInstalled'],  ['HEQUI'], [],
                // sqlStrings);
                this.model.buildSQLAndInsert(model.warranty, 'FunctionLocWarranty', this.columnNames['FunctionLocWarranty'], this.columnsSQL['FunctionLocWarranty'], ['TECHNICAL_OBJECT'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.orderHistory, 'FunctionLocOrderHist', this.columnNames['FunctionLocOrderHist'], this.columnsSQL['FunctionLocOrderHist'], ['FUNCLOC'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.partner, 'FunctionLocPartner', this.columnNames['FunctionLocPartner'], this.columnsSQL['FunctionLocPartner'], ['OBJNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.gis, 'FunctionLocGIS', this.columnNames['FunctionLocGIS'], this.columnsSQL['FunctionLocGIS'], ['TPLNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.userStatus, 'FunctionLocUserStat', this.columnNames['FunctionLocUserStat'], this.columnsSQL['FunctionLocUserStat'], ['OBJNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.ml, 'FunctionLocMl', this.columnNames['FunctionLocMl'], this.columnsSQL['FunctionLocMl'], ['TPLNR'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.longText, 'FunctionLocLongtext', this.columnNames['FunctionLocLongtext'], this.columnsSQL['FunctionLocLongtext'], [], [], sqlStrings);
                if (fnCallback && typeof fnCallback === "function") {
                    success_callback = fnCallback;
                } else {
                    success_callback = function (result) {
                        // jQuery.proxy(this.writeSuccess(tableName), this);
                    };
                }

                var error_callback = function () {
                    MessageBox.alert("Insert failed!");
                };
                this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);

            },
            insertEquipment: function (fnCallback, bCopyModel) {
                var model = bCopyModel ? bCopyModel.oData : this.model.oData;
                // Modify EQUNR in all
                // loop at column entries
                var sqlStrings = [], success_callback;
                this.model.buildSQLAndInsert(model, 'Equipment', this.columnNames['Equipment'], this.columnsSQL['Equipment'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.characteristics, 'EquipmentCharacs', this.columnNames['EquipmentCharacs'], this.columnsSQL['EquipmentCharacs'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.notifHistory, 'EquipmentNotifHist', this.columnNames['EquipmentNotifHist'], this.columnsSQL['EquipmentNotifHist'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.measurementPoint, 'EquipmentMeasPoint', this.columnNames['EquipmentMeasPoint'], this.columnsSQL['EquipmentMeasPoint'], [], [], sqlStrings);
                // this.model.buildSQLAndInsert('installed','EquipmentInstalled', this.columnNames['Orders'],
                // this.columnsSQL['Orders'],  ['HEQUI'], [], sqlStrings);
                this.model.buildSQLAndInsert(model.warranty, 'EquipmentWarranty', this.columnNames['EquipmentWarranty'], this.columnsSQL['EquipmentWarranty'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.orderHistory, 'EquipmentOrderHist', this.columnNames['EquipmentOrderHist'], this.columnsSQL['EquipmentOrderHist'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.partner, 'EquipmentPartner', this.columnNames['EquipmentPartner'], this.columnsSQL['EquipmentPartner'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.gis, 'EquipmentGIS', this.columnNames['EquipmentGIS'], this.columnsSQL['EquipmentGIS'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.userStatus, 'EquipmentUserStatus', this.columnNames['EquipmentUserStatus'], this.columnsSQL['EquipmentUserStatus'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.ml, 'EquipmentMl', this.columnNames['EquipmentMl'], this.columnsSQL['EquipmentMl'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.longText, 'EquipmentLongtext', this.columnNames['EquipmentLongtext'], this.columnsSQL['EquipmentLongtext'], [], [], sqlStrings);
                if (fnCallback && typeof fnCallback === "function") {
                    success_callback = fnCallback;
                } else {
                    success_callback = function (result) {
                        // jQuery.proxy(this.writeSuccess(tableName), this);
                    };
                }

                var error_callback = function () {
                    MessageBox.alert("Insert failed!");
                };
                this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);

            },
            copyFuncLocation: function (fnCallback, bCopyModel) {
                var model = bCopyModel ? bCopyModel.oData : this.copyModel.oData;
                model = this.adjustSystStatus(model);
                var sqlStrings = [], success_callback;
                this.model.buildSQLAndInsert(model, 'FunctionLoc', this.columnNames['FunctionLoc'], this.columnsSQL['FunctionLoc'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.systStatus, 'FunctionLocSystStat', this.columnNames['FunctionLocSystStat'], this.columnsSQL['FunctionLocSystStat'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.partner, 'FunctionLocPartner', this.columnNames['FunctionLocPartner'], this.columnsSQL['FunctionLocPartner'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(model.longText, 'FunctionLocLongtext', this.columnNames['FunctionLocLongtext'], this.columnsSQL['FunctionLocLongtext'], [], [], sqlStrings);

                if (fnCallback && typeof fnCallback === "function") {
                    success_callback = fnCallback;
                } else {
                    success_callback = function (result) {
                        // jQuery.proxy(this.writeSuccess(tableName), this);
                    };
                }

                var error_callback = function () {
                    MessageBox.alert("Insert failed!");
                };

                this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);

            },
            copyEquipment: function (fnCallback, bCopyModel) {
                var model = bCopyModel ? bCopyModel.oData : this.copyModel.oData;
                var that = this;
                model = this.adjustSystStatus(model);

                var sqlStrings, success_callback;
                var hequi_mobileId = '', hequi_actionFlag;
                if (fnCallback && typeof fnCallback === "function") {
                    success_callback = fnCallback;
                } else {
                    success_callback = function (result) {
                        // jQuery.proxy(this.writeSuccess(tableName), this);
                    };
                }

                var error_callback = function () {
                    MessageBox.alert("Insert failed!");
                };
                this.getEquiById(model.HEQUI, function (equipmentHeader) {
                    if (equipmentHeader.length > 0) {
                        hequi_mobileId = equipmentHeader[0].MOBILE_ID;
                        hequi_actionFlag = equipmentHeader[0].ACTION_FLAG;
                    }

                    that.getFuncLocById(model.TPLNR, function (functionLoc) {
                        var fl_mobileId = functionLoc[0].MOBILE_ID;
                        var fl_actionFlag = functionLoc[0].ACTION_FLAG;
                        sqlStrings = that.getCopyEquipmentStringsFromObject(model, fl_mobileId, fl_actionFlag);
                        that.getHEQUIInstalledString(model, hequi_mobileId, hequi_actionFlag, sqlStrings);
                        that.getInstalledTreeStrings(sqlStrings, model.EQUNR, fl_mobileId, fl_actionFlag, model.installed, function (strings) {
                            this.requestHelper.executeStatementsAndCommit(strings, success_callback, error_callback);
                        });
                    });
                });

            },

            getCopyEquipmentStringsFromObject: function (equipment, fl_mobileId, fl_actionFlag) {
                var sqlStrings = [];
                this.model.buildSQLAndInsert(equipment, 'Equipment', this.columnNames['Equipment'], this.columnsSQL['Equipment'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(equipment.characteristics, 'EquipmentCharacs', this.columnNames['EquipmentCharacs'], this.columnsSQL['EquipmentCharacs'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(equipment.installed, 'EquipmentInstalled', this.columnNames['EquipmentInstalled'], this.columnsSQL['EquipmentInstalled'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(equipment.partner, 'EquipmentPartner', this.columnNames['EquipmentPartner'], this.columnsSQL['EquipmentPartner'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(equipment.systStatus, 'EquipmentSystStatus', this.columnNames['EquipmentSystStatus'], this.columnsSQL['EquipmentSystStatus'], [], [], sqlStrings);
                this.model.buildSQLAndInsert(equipment.longText, 'EquipmentLongtext', this.columnNames['EquipmentLongtext'], this.columnsSQL['EquipmentLongtext'], [], [], sqlStrings);
                this.getFLInstalledString(equipment, fl_mobileId, fl_actionFlag, sqlStrings);
                return sqlStrings;
            },
            getFLInstalledString: function (equipmentHeader, fl_mobileId, fl_actionFlag, sqlStrings) {
                // Here we build the FunctionLocInstalled string for a given
                // equipment
                if (fl_actionFlag !== 'N') {
                    sqlStrings.push({
                        type: "update",
                        mainTable: true,
                        tableName: "FunctionLoc",
                        values: [["ACTION_FLAG", "C"]],
                        replacementValues: [],
                        whereConditions: "MOBILE_ID = '" + fl_mobileId + "'",
                        mainObjectValues: []
                    });
                }
                var values = [];
                values.push(["ACTION_FLAG", "N"]);
                values.push(["EQUNR", equipmentHeader.EQUNR]);
                values.push(["HEQUI", equipmentHeader.HEQUI]);
                values.push(["SHTXT", equipmentHeader.SHTXT]);
                values.push(["TPLMA", equipmentHeader.TPLMA]);
                values.push(["EQTYP", equipmentHeader.EQTYP]);
                values.push(["SERNR", equipmentHeader.SERNR]);
                values.push(["BAUJJ", equipmentHeader.BAUJJ]);
                values.push(["TPLNR", equipmentHeader.TPLNR]);
                values.push(["OBJNR", equipmentHeader.OBJNR]);
                values.push(["WERK", equipmentHeader.WERK]);
                values.push(["BAUMM", equipmentHeader.BAUMM]);
                sqlStrings.push({
                    type: "insert",
                    mainTable: true,
                    tableName: "FunctionLocInstalled",
                    values: values,
                    replacementValues: [],
                    whereConditions: "",
                    mainObjectValues: ["FunctionLoc", fl_mobileId]
                });

            },
            getHEQUIInstalledString: function (equipmentHeader, hequi_mobileId, hequi_actionFlag, sqlStrings) {
                if (hequi_mobileId === '') {
                    return;
                }
                // Here we build the FunctionLocInstalled string for a given
                // equipment
                var values = [];
                values.push(["ACTION_FLAG", "N"]);
                values.push(["EQUNR", equipmentHeader.EQUNR]);
                values.push(["HEQUI", equipmentHeader.HEQUI]);
                values.push(["HEQUI_SHTXT", equipmentHeader.HEQUI_SHTXT]);
                values.push(["SHTXT", equipmentHeader.SHTXT]);
                values.push(["TPLMA", equipmentHeader.TPLMA]);
                values.push(["EQTYP", equipmentHeader.EQTYP]);
                values.push(["SERNR", equipmentHeader.SERNR]);
                values.push(["BAUJJ", equipmentHeader.BAUJJ]);
                values.push(["TPLNR", equipmentHeader.TPLNR]);
                values.push(["OBJNR", equipmentHeader.OBJNR]);
                values.push(["WERK", equipmentHeader.WERK]);
                values.push(["BAUMM", equipmentHeader.BAUMM]);
                sqlStrings.push({
                    type: "insert",
                    mainTable: true,
                    tableName: "EquipmentInstalled",
                    values: values,
                    replacementValues: [],
                    whereConditions: "",
                    mainObjectValues: ["Equipment", hequi_mobileId]
                });
                if (hequi_actionFlag !== 'N') {
                    sqlStrings.push({
                        type: "update",
                        mainTable: true,
                        tableName: "Equipment",
                        values: [["ACTION_FLAG", "C"]],
                        replacementValues: [],
                        whereConditions: "MOBILE_ID = '" + hequi_mobileId + "'",
                        mainObjectValues: []
                    });
                }

            },
            getFuncLocById: function (tplnr, successCb, errorCb) {
                this.requestHelper.getData("FunctionLoc", {
                    columnName: "TPLNR",
                    keyValue: tplnr
                }, function (data) {
                    successCb(data);
                }, function (err) {
                    errorCb(err);
                });
            },
            getFuncLocPartners: function (objnr, newObjnr, successCb, errorCb) {
                this.requestHelper.getData("FunctionLocPartner", {
                    objectNumber: objnr
                }, function (data) {
                    data.forEach(function (partnerRow) {
                        partnerRow.MOBILE_ID = '';
                        partnerRow.ACTION_FLAG = 'N';
                        partnerRow.OBJNR = newObjnr;
                    });
                    successCb(data);
                }, function (err) {
                    errorCb(err);
                });
            },
            getEquiById: function (hequi, successCb, errorCb) {
                this.requestHelper.getData("Equipment", {
                    columnName: "EQUNR",
                    keyValue: hequi
                }, function (data) {
                    successCb(data);
                }, function (err) {
                    errorCb(err);
                });
            },
            getInstalledTreeStrings: function (sqlStrings, HEQUI, fl_mobileId, fl_actionFlag, installed, finishCb) {
                var that = this;
                var deferreds = [];
                var toModel = new TechnicalObjectsModel(false, "E");
                toModel.model.entityName = "equipments";

                var installedStrings = this.getInstalledSQLStringsForHEQUI(sqlStrings, HEQUI);

                for (var i = 0; i < installed.length; i++) {

                    var loadData = function (index, deferredsArray, installedStrings) {
                        toModel.equnr = installed[index].EQUNR;
                        toModel.objnr = installed[index].OBJNR;
                        var installedString = installedStrings[index];

                        toModel.loadData(function (data) {
                            // Get sqlStrings from data and append them
                            toModel.setCopyModelData(function (copyModel) {
                                copyModel.HEQUI = HEQUI;
                                sqlStrings = sqlStrings.concat(toModel.getCopyEquipmentStringsFromObject(copyModel, fl_mobileId, fl_actionFlag));

                                for (var i = 0; i < installedString.values.length; i++) {
                                    if (installedString.values[i].includes("EQUNR")) {
                                        installedString.values[i][1] = copyModel.EQUNR;
                                    }
                                }

                                if (data.installed.length > 0) {
                                    toModel.getInstalledTreeStrings(sqlStrings, copyModel.EQUNR, fl_mobileId, fl_actionFlag, copyModel.installed, function (sqlStringsNew) {
                                        // Merge newSqlStrings with oldSqlStrings
                                        for (var i = 0; i < sqlStringsNew.length; i++) {
                                            var newString = sqlStringsNew[i];
                                            var exists = false;

                                            for (var j = 0; j < sqlStrings.length; j++) {
                                                var oldString = sqlStrings[j];

                                                if (newString === oldString) {
                                                    exists = true;
                                                    break;
                                                }
                                            }

                                            if (!exists) {
                                                sqlStrings.push(newString);
                                            }
                                        }

                                        deferredsArray[index].resolve();
                                    });
                                } else {
                                    deferredsArray[index].resolve();
                                }
                            });
                        });
                    };

                    deferreds[i] = new $.Deferred();
                    loadData(i, deferreds, installedStrings);
                }

                $.when.apply($, deferreds).then(function onSuccess() {
                    finishCb(sqlStrings);
                }, function onError() {
                    errorCb();
                });
            },
            getInstalledTree: function (tree, finishCb) {
                var that = this;
                var deferreds = [];
                var toModel = new TechnicalObjectsModel(false, "E");
                toModel.model.entityName = "equipments";

                tree.subEquipments = [];
                for (var i = 0; i < tree.installed.length; i++) {

                    var loadData = function (index, deferredsArray) {
                        toModel.equnr = tree.installed[index].EQUNR;
                        toModel.objnr = tree.installed[index].OBJNR;

                        toModel.loadData(function (data) {

                            if (data.installed.length > 0) {
                                toModel.getInstalledTree(Object.assign({}, data), function (subTree) {
                                    tree.subEquipments.push(subTree);
                                    deferredsArray[index].resolve();
                                });
                            } else {
                                data.subEquipments = [];
                                tree.subEquipments.push(data);
                                deferredsArray[index].resolve();
                            }

                        });
                    };

                    deferreds[i] = new $.Deferred();
                    loadData(i, deferreds);
                }

                $.when.apply($, deferreds).then(function onSuccess() {
                    finishCb(tree);
                }, function onError() {
                    errorCb();
                });
            },

            getInstalledSQLStringsForHEQUI: function (sqlStrings, HEQUI) {
                return sqlStrings.filter(function (string) {
                    if (string.tableName !== "EquipmentInstalled") {
                        return false;
                    }

                    for (var i = 0; i < string.values.length; i++) {
                        if (string.values[i].includes("HEQUI") && string.values[i].includes(HEQUI)) {
                            return true
                        }
                    }

                    return false
                });
            },

            getDeleteObjectForTree: function (tree, tableName) {
                var that = this;
                var deleteObjects = [];
                deleteObjects.push({
                    MOBILE_ID: tree.MOBILE_ID,
                    action_flag: tree.ACTION_FLAG,
                    tableName: tableName
                });

                tree.subEquipments.forEach(function (subEquipment) {
                    var subDObjects = that.getDeleteObjectForTree(subEquipment, tableName);
                    subDObjects.forEach(function (o) {
                        deleteObjects.push(o);
                    });
                });

                return deleteObjects;
            },
            getNewAttachmentObject: function (fileName, fileExt) {
                var attachmentObj = this.model.createNewObject(this.columnNames["SAMObjectAttachments"]);

                attachmentObj["OBJKEY"] = this.model.entityName === "equipments" ? this.equnr : this.tplnr;
                attachmentObj["TABNAME"] = this.model.entityName === "equipments" ? "Equipments" : "FunctionLoc";
                attachmentObj["FILENAME"] = fileName;
                attachmentObj["FILEEXT"] = fileExt;
                attachmentObj["ACTION_FLAG"] = "N";

                return attachmentObj;
            },
            getNewPartnerRow: function () {
                return this.model.getClonedObject(this.newPartnerObject);
            },
            getNewWarrantyRow: function () {
                return this.model.getClonedObject(this.newWarrantyObject);
            },
            getMeasurementDocumentsByPoint: function (queryParams, successCb, errorCb) {
                this.requestHelper.getData("MeasurementDocument", {
                    noOfRows: queryParams.noOfRows,
                    startAt: queryParams.startAt,
                    point: queryParams.point
                }, function (data) {
                    if (typeof successCb === "function") {
                        successCb(data);
                    }
                }, function (err) {
                    if (typeof successCb === "function") {
                        errorCb(err);
                    }
                });
            },
            getNewEquipment: function (fnCallback) {
                if (this.newEquipment === undefined) {
                    // new Header
                    this.newEquipment = this.model.createNewObject(this.columnNames["Equipment"]);
                }

                var equipmentHeader = this.model.getClonedObject(this.newEquipment);
                // Get new UUID
// 	    this.model.getNewUUID(function(newUUID) {
// 		newUUID = newUUID.substring(0, 12);
                var newUUID = this.model.getRandomUUID(18);
                // Default values
                // Temporary defaulting - needs to be removed
                equipmentHeader.EQUNR = newUUID;
                equipmentHeader.EQUIPMENT_DISPLAY = newUUID;
                equipmentHeader.EQTYP = "S";
                equipmentHeader.OBJNR = 'IE' + newUUID;
                equipmentHeader._constructionDate = "";
                equipmentHeader.BAUJJ = "";
                equipmentHeader.BAUMM = "";
                equipmentHeader._activeState = true;
                equipmentHeader.systStatus = [];
                equipmentHeader.longText = [];
                equipmentHeader._longTextMl = '';
                if (fnCallback && typeof fnCallback === "function") {
                    fnCallback(equipmentHeader);
                }
// 	    }, function() {
// 		// error handling
// 		return null;
// 	    });

            },
            createNewEquipment: function (equipmentHeader, fnCallback) {
                // create new header row
                var that = this;
                var sqlStrings = [], success_callback;
                var hequi_mobileId = '', hequi_actionFlag;
                equipmentHeader = this.adjustSystStatus(equipmentHeader);
                this.setLongText('LTXT', '00000000', equipmentHeader._longTextMl, 'EQUNR', equipmentHeader.EQUNR, equipmentHeader);
                this.getEquiById(equipmentHeader.HEQUI, function (hequi) {
                    if (hequi.length > 0) {
                        hequi_mobileId = hequi[0].MOBILE_ID;
                        hequi_actionFlag = hequi[0].ACTION_FLAG;
                    }
                    that.getFuncLocById(equipmentHeader.TPLNR, function (functionLoc) {
                        that.getFuncLocPartners(functionLoc[0].OBJNR, equipmentHeader.OBJNR, function (functionLocPartners) {
                            var fl_mobileId = functionLoc[0].MOBILE_ID;
                            var fl_actionFlag = functionLoc[0].ACTION_FLAG;
                            // loop at column entries
                            equipmentHeader.partner = functionLocPartners;
                            that.getFLInstalledString(equipmentHeader, fl_mobileId, fl_actionFlag, sqlStrings);
                            that.model.buildSQLAndInsert(equipmentHeader, 'Equipment', that.columnNames['Equipment'], that.columnsSQL['Equipment'], [], [], sqlStrings);
                            that.getHEQUIInstalledString(equipmentHeader, hequi_mobileId, hequi_actionFlag, sqlStrings);
                            that.model.buildSQLAndInsert(equipmentHeader.systStatus, 'EquipmentSystStatus', that.columnNames['EquipmentSystStatus'], that.columnsSQL['EquipmentSystStatus'], [], [], sqlStrings);
                            that.model.buildSQLAndInsert(equipmentHeader.partner, 'EquipmentPartner', that.columnNames['EquipmentPartner'], that.columnsSQL['EquipmentPartner'], [], [], sqlStrings);
                            that.model.buildSQLAndInsert(equipmentHeader.longText, 'EquipmentLongtext', that.columnNames['EquipmentLongtext'], that.columnsSQL['EquipmentLongtext'], [], [], sqlStrings);

                            if (typeof fnCallback === "function") {

                                fnCallback(sqlStrings);
                            }
                        });
                    });
                });

            },
            getNewFunctionLoc: function (fnCallback) {
                if (this.newFunctionLoc === undefined) {
                    // new Header
                    this.newFunctionLoc = this.model.createNewObject(this.columnNames["FunctionLoc"]);
                }

                var functionLocHeader = this.model.getClonedObject(this.newFunctionLoc);
                // Get new UUID
// 	    this.model.getNewUUID(function(newUUID) {
// 		newUUID = newUUID.substring(0, 12);
                var newUUID = this.model.getRandomUUID(18);
                // Default values
                // Temporary defaulting - needs to be removed
                functionLocHeader.TPLNR = newUUID;
                functionLocHeader.FUNCLOC_DISP = newUUID;
                functionLocHeader.OBJNR = 'IF' + newUUID;
                functionLocHeader.FLTYP = 'S';
                functionLocHeader._activeState = true;
                functionLocHeader.systStatus = [];
                functionLocHeader.longText = [];
                functionLocHeader._longTextMl = '';

                if (fnCallback && typeof fnCallback === "function") {
                    fnCallback(functionLocHeader);
                }
// 	    }, function() {
// 		// error handling
// 		return null;
// 	    });

            },
            createNewFunctionLoc: function (functionLocHeader, fnCallback) {
                // create new header row
                var that = this;
                var sqlStrings = [], success_callback;
                functionLocHeader = this.adjustSystStatus(functionLocHeader);
                that.getFuncLocById(functionLocHeader.TPLMA, function (functionLoc) {
                    that.getFuncLocPartners(functionLoc[0].OBJNR, functionLocHeader.OBJNR, function (functionLocPartners) {
                        functionLocHeader.partner = functionLocPartners;
                        // loop at column entries
                        that.setLongText('LTXT', '00000000', functionLocHeader._longTextMl, 'TPLNR', functionLocHeader.TPLNR, functionLocHeader);
                        that.model.buildSQLAndInsert(functionLocHeader, 'FunctionLoc', that.columnNames['FunctionLoc'], that.columnsSQL['FunctionLoc'], [], [], sqlStrings);
                        that.model.buildSQLAndInsert(functionLocHeader.systStatus, 'FunctionLocSystStat', that.columnNames['FunctionLocSystStat'], that.columnsSQL['FunctionLocSystStat'], [], [], sqlStrings);
                        that.model.buildSQLAndInsert(functionLocHeader.longText, 'FunctionLocLongtext', that.columnNames['FunctionLocLongtext'], that.columnsSQL['FunctionLocLongtext'], [], [], sqlStrings);
                        that.model.buildSQLAndInsert(functionLocHeader.partner, 'FunctionLocPartner', that.columnNames['FunctionLocPartner'], that.columnsSQL['FunctionLocPartner'], [], [], sqlStrings);

                        if (typeof fnCallback === "function") {
                            fnCallback(sqlStrings);
                        }
                    });
                });
            },

            writeSuccess: function (tableName) {
                // Here we have to read the last updated item
                //
            },
            updateObject: function (optionalSqlStrings, fnCallback, errorCallback) {
                if (this.model.entityName === 'equipments') {
                    this.updateEquipment(optionalSqlStrings, fnCallback, errorCallback);
                } else {
                    this.updateFuncLoc(optionalSqlStrings, fnCallback, errorCallback);
                }
            },
            updateEquipment: function (optionalSqlStrings, fnCallback, errorCallback) {
                var that = this;
                var sqlStrings = [], success_callback;
                // if(!this.checkOrderDataBeforeSave()){
                // return;
                // }
                if (JSON.stringify(this.model.oData) === JSON.stringify(this.model._prevModelData)) {
                    //no data to save
                    if (fnCallback && typeof fnCallback === "function") {
                        fnCallback();
                    }
                    return;
                }
                this.setLongText('LTXT', '00000000', this.model.oData._longTextMl, 'EQUNR', this.model.oData.EQUNR);

                var moveSuccessful = function () {
                    that.model.buildSQLAndUpdate('', 'Equipment', that.columnNames['Equipment'], that.columnsSQL['Equipment'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('characteristics', 'EquipmentCharacs', that.columnNames['EquipmentCharacs'], that.columnsSQL['EquipmentCharacs'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('notifHistory', 'EquipmentNotifHist', that.columnNames['EquipmentNotifHist'], that.columnsSQL['EquipmentNotifHist'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('measurementPoint', 'EquipmentMeasPoint', that.columnNames['EquipmentMeasPoint'], that.columnsSQL['EquipmentMeasPoint'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('installed', 'EquipmentInstalled', that.columnNames['EquipmentInstalled'], that.columnsSQL['EquipmentInstalled'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('warranty', 'EquipmentWarranty', that.columnNames['EquipmentWarranty'], that.columnsSQL['EquipmentWarranty'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('orderHistory', 'EquipmentOrderHist', that.columnNames['EquipmentOrderHist'], that.columnsSQL['EquipmentOrderHist'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('partner', 'EquipmentPartner', that.columnNames['EquipmentPartner'], that.columnsSQL['EquipmentPartner'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('gis', 'EquipmentGIS', that.columnNames['EquipmentGIS'], that.columnsSQL['EquipmentGIS'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('userStatus', 'EquipmentUserStatus', that.columnNames['EquipmentUserStatus'], that.columnsSQL['EquipmentUserStatus'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('attachments', 'SAMObjectAttachments', that.columnNames['SAMObjectAttachments'], that.columnsSQL['SAMObjectAttachments'], sqlStrings, [], ["LINKID"], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('ml', 'EquipmentMl', that.columnNames['EquipmentMl'], that.columnsSQL['EquipmentMl'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    that.model.buildSQLAndUpdate('longText', 'EquipmentLongtext', that.columnNames['EquipmentLongtext'], that.columnsSQL['EquipmentLongtext'], sqlStrings, [], [], 'Equipment', that.objectUUID);
                    // newMethod
                    if (fnCallback && typeof fnCallback === "function") {
                        success_callback = fnCallback;
                    } else {
                        success_callback = function (result) {
                            // jQuery.proxy(this.writeSuccess(tableName), this);
                        };
                    }

                    var error_callback = function (result) {
                        MessageBox.alert(result);
                        errorCallback();
                    };

                    sqlStrings = optionalSqlStrings ? sqlStrings.concat(optionalSqlStrings) : sqlStrings;

                    this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);
                };

                var moveError = function () {
                    MessageBox.alert("Error occured on attachment movement");
                    fnCallback();
                };

                this.model.moveAttachmentsToDocDir(moveSuccessful, moveError);

            },
            checkSuperFLEQChange: function (sqlStrings, fnCallback) {
                var errorCb = function (err) {
                    MessageBox.alert(err);
                };
                var that = this;
                if (this.model.oData.TPLNR !== this.model._prevModelData.TPLNR) {
                    this.requestHelper.getData("FunctionLocInstalled", {
                        equnr: this.model.oData.EQUNR,
                        funcLocId: this.model._prevModelData.TPLNR
                    }, function (functionLocInstalled) {
                        that.model.buildSQLAndDelete(functionLocInstalled[0].MOBILE_ID, functionLocInstalled[0].ACTION_FLAG, "FunctionLocInstalled", sqlStrings);
                        that.getFuncLocById(that.model.oData.TPLNR, function (functionLoc) {
                            that.getFLInstalledString(that.model.oData, functionLoc[0].MOBILE_ID, functionLoc[0].ACTION_FLAG, sqlStrings);
                            that.checkSuperEQChange(sqlStrings, fnCallback);
                        });
                    }, function (err) {
                        errorCb(err);
                    }, false, 'forEquipment');
                } else {
                    //Just check superior equipment
                    that.checkSuperEQChange(sqlStrings, fnCallback);
                }

            },
            checkSuperEQChange: function (sqlStrings, fnCallback) {
                var that = this;
                if (this.model.oData.HEQUI !== this.model._prevModelData.HEQUI) {
                    this.requestHelper.getData("EquipmentInstalled", {
                        equnr: this.model.oData.EQUNR,
                        hequi: this.model._prevModelData.HEQUI
                    }, function (equipmentInstalled) {
                        if (equipmentInstalled.length > 0) {
                            that.model.buildSQLAndDelete(equipmentInstalled[0].MOBILE_ID, equipmentInstalled[0].ACTION_FLAG, "EquipmentInstalled", sqlStrings);
                        }
                        that.getEquiById(that.model.oData.HEQUI, function (equi) {
                            that.getHEQUIInstalledString(that.model.oData, equi[0].MOBILE_ID, equi[0].ACTION_FLAG, sqlStrings);
                            fnCallback(sqlStrings);
                        });
                    }, function (err) {
                        errorCb(err);
                    }, false, 'forEquipment');
                } else {
                    fnCallback(sqlStrings);
                }

            },
            updateFuncLoc: function (optionalSqlStrings, fnCallback, errorCallback) {
                var that = this;
                var sqlStrings = [], success_callback;
                // if(!this.checkOrderDataBeforeSave()){
                // return;
                // }
                if (JSON.stringify(this.model.oData) === JSON.stringify(this.model._prevModelData)) {
                    // no data to save
                    if (fnCallback && typeof fnCallback === "function") {
                        fnCallback();
                    }
                    return;
                }
                this.setLongText('LTXT', '00000000', this.model.oData._longTextMl, 'TPLNR', this.model.oData.TPLNR);
                var moveSuccessful = function () {
                    that.model.buildSQLAndUpdate('', 'FunctionLoc', that.columnNames['FunctionLoc'], that.columnsSQL['FunctionLoc'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('characteristics', 'FunctionLocCharacs', that.columnNames['FunctionLocCharacs'], that.columnsSQL['FunctionLocCharacs'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('notifHistory', 'FunctionLocNotifHist', that.columnNames['FunctionLocNotifHist'], that.columnsSQL['FunctionLocNotifHist'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('measurementPoint', 'FunctionLocMeasPoint', that.columnNames['FunctionLocMeasPoint'], that.columnsSQL['FunctionLocMeasPoint'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('installed', 'FunctionLocInstalled', that.columnNames['FunctionLocInstalled'], that.columnsSQL['FunctionLocInstalled'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('warranty', 'FunctionLocWarranty', that.columnNames['FunctionLocWarranty'], that.columnsSQL['FunctionLocWarranty'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('orderHistory', 'FunctionLocOrderHist', that.columnNames['FunctionLocOrderHist'], that.columnsSQL['FunctionLocOrderHist'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('partner', 'FunctionLocPartner', that.columnNames['FunctionLocPartner'], that.columnsSQL['FunctionLocPartner'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('gis', 'FunctionLocGIS', that.columnNames['FunctionLocGIS'], that.columnsSQL['FunctionLocGIS'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('userStatus', 'FunctionLocUserStat', that.columnNames['FunctionLocUserStat'], that.columnsSQL['FunctionLocUserStat'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('systStatus', 'FunctionLocSystStat', that.columnNames['FunctionLocSystStat'], that.columnsSQL['FunctionLocSystStat'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('attachments', 'SAMObjectAttachments', that.columnNames['SAMObjectAttachments'], that.columnsSQL['SAMObjectAttachments'], sqlStrings, [], ["LINKID"], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('ml', 'FunctionLocMl', that.columnNames['FunctionLocMl'], that.columnsSQL['FunctionLocMl'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);
                    that.model.buildSQLAndUpdate('longText', 'FunctionLocLongtext', that.columnNames['FunctionLocLongtext'], that.columnsSQL['FunctionLocLongtext'], sqlStrings, [], [], 'FunctionLoc', that.objectUUID);

                    // newMethod
                    if (fnCallback && typeof fnCallback === "function") {
                        success_callback = fnCallback;
                    } else {
                        success_callback = function (result) {
                            // jQuery.proxy(this.writeSuccess(tableName), this);
                        };
                    }

                    var error_callback = function (result) {
                        MessageBox.alert(result);
                        errorCallback();
                    };

                    sqlStrings = optionalSqlStrings ? sqlStrings.concat(optionalSqlStrings) : sqlStrings;

                    this.requestHelper.executeStatementsAndCommit(sqlStrings, success_callback, error_callback);
                };

                var moveError = function () {
                    MessageBox.alert("Error occured on attachment movement");
                    fnCallback();
                };

                this.model.moveAttachmentsToDocDir(moveSuccessful, moveError);

            },
            deleteObject: function (mobile_id, action_flag, tableName, successCb, errorCb) {

                // loop at column entries
                var sqlStrings = [], success_callback;
                var success_callback = function () {
                    successCb();
                };

                var error_callback = function () {
                    errorCb();
                    // MessageBox.alert("Delete failed!");
                };
                this.model.deleteEntity(mobile_id, action_flag, tableName, success_callback, error_callback);
            },

            deletePartnerRow: function (index) {
                var businessPartnerData = this.model.getProperty('/partner');
                businessPartnerData.splice(index, 1);

                this.model.setProperty('/partner', businessPartnerData);
            },
            getLocation: function () {
                var street = this.model.getProperty("/STREET");
                var zipCode = this.model.getProperty("/POST_CODE1");
                var city = this.model.getProperty("/CITY1");

                return street + ", " + zipCode + " " + city;
            },
            setObjectNrById: function (success, error) {
                var that = this;
                var load_success = function (data) {
                    if (data.length === 0 && typeof error === 'function') {
                        error();
                        return;
                    }
                    if (data.length === 1) {
                        that.objnr = data[0].OBJNR;
                    }
                    success(that.objnr || "");
                };
                var load_error = jQuery.proxy(this.load_error, this)

                if (this.model.entityName === "equipments") {
                    this.requestHelper.getData("EquipmentObjectNr", {
                        "equipmentNumber": this.equnr
                    }, load_success, load_error);
                } else {
                    this.requestHelper.getData("FunctionLocObjectNr", {
                        "funcLocId": this.tplnr
                    }, load_success, load_error);
                }
            },
            getLongText: function (data, objType, objKey, tableName) {
                return this.model.getLongText(data, objType, objKey, tableName);
            },
            setLongText: function (objType, objKey, textValue, entityType, entityId, model) {
                this.model.setLongText(objType, objKey, textValue, entityType, entityId, model);
            },
            adjustSystStatus: function (objectData) {
                if (objectData._activeState) {
                    // active
                    objectData.systStatus.forEach(function (status) {
                        if (status.SYST_STATUS_CODE === 'INAC') {
                            status.INACT = 'X';
                            status.CHANGED = 'X';
                        }
                    });
                } else {
                    // inactive
                    var statusReversed = false;
                    objectData.systStatus.forEach(function (status) {
                        if (status.SYST_STATUS_CODE === 'INAC') {
                            status.INACT = '';
                            status.CHANGED = 'X';
                            statusReversed = true;
                        }
                    });
                    if (!statusReversed) {
                        // insert inactive entry
                        objectData.systStatus.push({
                            'MOBILE_ID': '',
                            'ACTION_FLAG': 'N',
                            'CHANGED': 'X',
                            'INACT': '',
                            'OBJNR': objectData.OBJNR,
                            'STATUS': 'I0320',
                            'SYST_STATUS_CODE': 'INAC'
                        });
                    }
                }
                return objectData;
            }
        };

        var TechnicalObjectsModel = TechnicalObjectsModel;

        return TechnicalObjectsModel;
    });
