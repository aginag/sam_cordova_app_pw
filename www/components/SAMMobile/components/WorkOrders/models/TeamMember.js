sap.ui.define(["SAMMobile/components/WorkOrders/models/TeamTool"],
   
   function(TeamTool) {
      "use strict";
      
      // constructor
      function TeamMember(dbInfo) {
         this.rGuid = dbInfo.R_GUID;
         this.tGuid = dbInfo.T_GUID;
         this.persNo = dbInfo.R_GUID;
         this.actType = dbInfo.ACTTYPE;
         this.responsible = dbInfo.RESP;
         this.firstName = dbInfo.FIRTNAME;
         this.lastName = dbInfo.LASTNAME;
         
         this.tools = [];
         this.VEHICLE = null;
      }
      
      TeamMember.prototype = {
         setTools: function(toolInfo) {
            var that = this;
             this.tools = toolInfo.map(function(info) {
               return new TeamTool(info);
            });
            for(var i = 0; i < toolInfo.length; i++){
                if(toolInfo[i].T_TYPE === 'D' || toolInfo[i].T_TYPE === 'B' || toolInfo[i].T_TYPE === 'F'){
            	that.VEHICLE = toolInfo[i];
            	break;
                }
            };
         },
   
         getId: function() {
            return this.rGuid;
         },
         
         getTeamGuid: function() {
            return this.tGuid;
         },
         
         getPersNo: function() {
            return this.persNo;
         },
         
         getActivityType: function() {
            return this.actType;
         },
         
         getFullName: function() {
            return this.firstName + " " + this.lastName;
         },
         
         isTeamLead: function() {
            return this.responsible == 'X';
         }
      };
      
      TeamMember.prototype.constructor = TeamMember;
      
      return TeamMember;
   });