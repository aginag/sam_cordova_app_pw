sap.ui.define([],
   
   function() {
      "use strict";
      
      // constructor
      function TeamTool(dbInfo) {
         this.tGuid = dbInfo.T_GUID;
         this.rGuid = dbInfo.T_ID;
         this.description = dbInfo.T_TEXT;
      }
      
      TeamTool.prototype = {
         getMemberId: function() {
            return this.rGuid;
         },
         
         getTeamGuid: function() {
            return this.tGuid;
         },
         
         getDescription: function() {
            return this.description;
         }
      };
      
      TeamTool.prototype.constructor = TeamTool;
      
      return TeamTool;
   });