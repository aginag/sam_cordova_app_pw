sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function Scenario(name, config) {
            this.name = name;
            this.orderUserStatus = config.orderUserStatus;
            this.longTexts = config.longTexts;
            this.complaintNotification = config.complaintNotification;
            this.availableNotificationTypes = config.availableNotificationTypes;
            this.afterSync = config.afterSync;
            this.meterReplacementInfo = config.meterReplacementInfo;
            this.summary = config.summary;
            this.maxDailyWorkInMinutes = config.maxDailyWorkInMinutes;
        }

        Scenario.prototype = {
            getOrderUserStatus: function () {
                return this.orderUserStatus;
            },

            getListItemLongtextInformation: function () {
                return this.longTexts.orders.list;
            },

            getDescriptionTabLongtextInformation: function() {
                return this.longTexts.orders.descriptionTab;
            },

            getOperationsItemLongtextInformation: function () {
                return this.longTexts.operations;
            },

            getComplaintNotificationInfo: function() {
                return this.complaintNotification;
            },
            
            getMeterReplacementInfo: function() {
                return this.meterReplacementInfo;
            },
            
            getSummaryInfo: function() {
                return this.summary;
            },

            getAcceptedNotificationTypes: function () {
                return this.availableNotificationTypes;
            },

            getAfterSyncStatuses: function() {
                return this.afterSync;
            },

            getMaxDailyWorkInMinutes: function () {
                return this.maxDailyWorkInMinutes;
            }
        };

        Scenario.prototype.constructor = Scenario;

        return Scenario;
    });