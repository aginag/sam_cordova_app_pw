sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function Stopwatch(orderId, activity, perNr, whichUser) {
            this.id = new Date().getTime();
            var start = moment();
            var min = 5 * Math.round( (start.minute() + (start.second() / 60)) / 5 );
            start.set({m: min});
            this.startDateTime = start.format("YYYY-MM-DD HH:mm");
            this.endDateTime = "";
            this.elapsedHours = "";
            this.orderId = orderId;
            this.activity = activity;
            this.perNr = perNr;
            this.whichUser = whichUser
        }

        Stopwatch.prototype = {
            stop: function () {
                
                var end = moment();
                var min = 5 * Math.round( (end.minute() + (end.second() / 60)) / 5 );
                end.set({m: min});
                this.endDateTime = end.format("YYYY-MM-DD HH:mm");

                var duration = moment.duration(moment(this.endDateTime).diff(moment(this.startDateTime)));
                // Round to the next half hour
                var durationRounded = new Date(Math.ceil(new Date(duration.asMilliseconds())/1800000)*1800000).getTime();
                this.elapsedHours = durationRounded / 3600000;
            },
   
           setData: function(data) {
              for (var i in data) {
                 if (data.hasOwnProperty(i)) {
                    this[i] = data[i];
                 }
              }
           }
        };
        

        Stopwatch.prototype.constructor = Stopwatch;

        return Stopwatch;
    }
);