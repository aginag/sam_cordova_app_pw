sap.ui.define(["SAMMobile/models/dataObjects/OrderUserStatus", "SAMMobile/models/dataObjects/OrderOperation", "SAMMobile/models/dataObjects/OrderUserStatus"],

    function (Order, OrderOperation, OrderUserStatus) {
        "use strict";

        // constructor
        function AfterSyncOperations(component) {
            this.component = component;
            this.requestHelper = this.component.requestHelper;
            this._errorCallback = null;
        }

        AfterSyncOperations.prototype = {

        };

        AfterSyncOperations.prototype.changeOrderUserStatuses = function(fromStatus, toStatus, successCb) {
            var sqlStrings = [];

            var errorCb = function(err) {
                throw err;
            };

            var onOrderOperationsLoaded = function(operations) {
                // 2. For each orderOperation

                var operationUserStatusToBeChanged = operations.filter(function(operation) {


                    var isTeamLeadForOperation = false;
                    this.component.globalViewModel.model.getProperty("/teams").forEach(function (oTeam) {
                        if (oTeam.isTeamLead && oTeam.teamGuid === operation.TEAM_GUID) {
                            isTeamLeadForOperation = true;
                        }
                    });

                    if (operation.PERS_NO !== this.component.globalViewModel.model.getProperty("/personnelNumber")
                        && !isTeamLeadForOperation
                        && operation.TEAM_PERSNO !== this.component.globalViewModel.model.getProperty("/personnelNumber")) {
                        return false;
                    }

                    var aOperationStatus = operation.getUserStatus().filter(function (oOperationUserStatus) {
                        return oOperationUserStatus.STATUS === toStatus.USER_STATUS && oOperationUserStatus.isActive();
                    });

                    return aOperationStatus.length === 0;

                }.bind(this));

                if (operationUserStatusToBeChanged.length == 0) {
                    // Nothing needs to be done
                    successCb(false);
                    return;
                }

                operationUserStatusToBeChanged.forEach(function(operation) {
                    sqlStrings = sqlStrings.concat(operation.changeUserStatus(toStatus, true));
                });

                this.requestHelper.executeStatementsAndCommit(sqlStrings, successCb.bind(null, true), errorCb.bind(this, new Error("AfterSyncOperations: Error while inserting User Status changes!")));
            }.bind(this);

            var toStatusMl = this._getUserStatusMl(toStatus.STATUS, toStatus.STATUS_PROFILE);

            if (!toStatusMl) {
                throw new Error("Could not change user status: User status not found");
                return;
            }

            toStatus = toStatusMl;

            // 1. Fetch all orderUserStatuses
            this._fetchOrderOperations(onOrderOperationsLoaded.bind(this), errorCb.bind(this, new Error("AfterSyncOperations: Error while fetching Order Operations")))
        };


        AfterSyncOperations.prototype.handleOrderUserStatusErrors = function(successCb) {
            var sqlStrings = [];
            var errorCb = function(err) {
                throw err;
            };

            var onOrderUserStatusLoaded = function(orderUserStatus) {
                var orderUserStatusToBeChanged = orderUserStatus;

                if (orderUserStatusToBeChanged.length == 0) {
                    // Nothing needs to be done
                    successCb(false);
                    return;
                }

                // 2. For each orderUserStatus
                // -> change status to inactive and action flag to 'C'
                orderUserStatusToBeChanged.forEach(function(status) {
                    if (status.ACTION_FLAG == "P") {
                        status.ACTION_FLAG = "C";
                        status.CHANGED = "X";
                    } else if (status.ACTION_FLAG == "O") {
                        status.ACTION_FLAG = "N";
                        status.CHANGED = "X";
                    }

                    sqlStrings.push(status.update2(true));
                });

                this.requestHelper.executeStatementsAndCommit(sqlStrings, successCb.bind(null, true), errorCb.bind(this, new Error("AfterSyncOperations: Error while inserting User Status changes!")));
            };

            // 1. Fetch all orderUserStatus in error
            this._fetchOrderUserStatusInError(onOrderUserStatusLoaded.bind(this), errorCb.bind(this, new Error("AfterSyncOperations: Error while fetching Order User Status")));
        };

        AfterSyncOperations.prototype._fetchOrderOperations = function(successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var orderOperations = data.OperationList.map(function (operationData) {
                    var operationObject = new OrderOperation(operationData, that.requestHelper, that);

                    var operationUserStatus = data.OrderOperationStatus.filter(function (status) {
                        return status.OBJNR == operationData.OBJECT_NO;
                    });

                    operationObject.setUserStatus(operationUserStatus.map(function (statusData) {
                        return new OrderUserStatus(statusData, that.requestHelper, that);
                    }));

                    return operationObject;
                });

                successCb(orderOperations);
            };

            this.requestHelper.getData(["OperationList", "OrderOperationStatus"], 
            		{persNo: this.component.globalViewModel.model.getProperty("/personnelNumber"),
            	     teamGuids: this.component.globalViewModel.model.getProperty("/teams").map(function (oTeam) { return "'" + oTeam.teamGuid + "'"; }),
            	     language: this.component.globalViewModel.getActiveUILanguage(),
            	     startCons: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
            	     startDat: moment().add(10,'days').format("YYYY-MM-DD HH:mm:ss.SSSS"),
                     yesterday: moment().add(-1,'days').format("YYYY-MM-DD"),
            	     doneStatus: this.component.scenario.getOrderUserStatus().done.STATUS,
                     prioStatus: this.component.scenario.getOrderUserStatus().prio.STATUS
            	    },
            	    load_success, 
                	errorCb, 
                	true,
                	"mine");        
        };

        AfterSyncOperations.prototype._fetchOrderUserStatusInError = function(successCb, errorCb) {
            var that = this;
            var load_success = function (data) {

                var aUserStatus = data.filter(function (oUserStatus) {

                    var isTeamLeadForOperation = false;
                    this.component.globalViewModel.model.getProperty("/teams").forEach(function (oTeam) {
                        if (oTeam.isTeamLead && oTeam.teamGuid === oUserStatus.TEAM_GUID) {
                            isTeamLeadForOperation = true;
                        }
                    });

                    return oUserStatus.PERS_NO === this.component.globalViewModel.model.getProperty("/personnelNumber")
                        || isTeamLeadForOperation
                        || oUserStatus.TEAM_PERSNO === this.component.globalViewModel.model.getProperty("/personnelNumber");
                }.bind(this));

                var orderUserStatus = aUserStatus.map(function (statusData) {
                    return new OrderUserStatus(statusData, this.requestHelper);
                }.bind(this));

                successCb(orderUserStatus);
            };

            this.requestHelper.getData("OrderOperationStatus", {}, load_success.bind(this), errorCb, true, "inError");
        };

        AfterSyncOperations.prototype._getOrderChangedSqlStringsByOrderIds = function(orderIds) {
            var sqlChangeStrings = orderIds.map(function(id) {
                return {
                    type: "update",
                    mainTable: true,
                    tableName: "Orders",
                    values: [["ACTION_FLAG", "C"]],
                    replacementValues: [],
                    whereConditions: " ORDERID = '" + id + "'",
                    mainObjectValues: []
                };
            });

            return sqlChangeStrings;
        };

        AfterSyncOperations.prototype._getUserStatusMl = function (status, statusProfile) {
            var data = this.component.customizingModel.model.getProperty("/UserStatusMl");

            for (var i = 0; i < data.length; i++) {
                var stat = data[i];

                if (stat.STATUS_PROFILE == statusProfile && stat.USER_STATUS == status) {
                    return stat;
                }
            }

            return null;
        };

        AfterSyncOperations.prototype.constructor = AfterSyncOperations;

        return AfterSyncOperations;
    });