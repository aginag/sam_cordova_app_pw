sap.ui.define([
    "SAMMobile/components/WorkOrders/helpers/Stopwatch",
    "SAMMobile/models/dataObjects/TimeConfirmation",
    "SAMMobile/components/WorkOrders/controller/workOrders/detail/time/EditTimeEntryViewModel",
    "sap/m/Button",
    "sap/m/Dialog",
    "SAMMobile/helpers/formatter",
    "SAMMobile/helpers/timeHelper",
    "sap/m/MessageBox"
    ],

    function (Stopwatch, TimeConfirmation, EditTimeEntryViewModel, Button, Dialog, formatter, TimeHelper, MessageBox) {
        "use strict";

        // constructor
        function StopwatchManager(component, viewModel) {
            this._LS_KEY = "SAM_DEWA_STOPWATCHES";
            this.component = component;
            this.viewModel = viewModel;
            this.requestHelper = this.component.requestHelper;
            this.watches = [];
            this.viewModel.setErrorCallback(this.onViewModelError.bind(this));

            if (localStorage.getItem(this._LS_KEY) === null) {
                localStorage.setItem(this._LS_KEY, JSON.stringify(this.watches));
            }

            this._initWatches();
        }

        StopwatchManager.prototype = {
            start: function (order, successCb, errorCb) {    
                // Check if there is a stop watch running for order.orderId currently
                // -> Yes? Stop it. -> Start new
                // -> No? Start new

                var that = this;
                var runningWatch = this.getStopwatchForOrderId(order);

                if (runningWatch) {
                    this.stop(order, function(timeHeader) {
                        that._start(order);
                        successCb(timeHeader);
                    }, errorCb);
                } else {
                    this._start(order);
                    successCb();
                }
            },

            stop: function (order, successCb, errorCb) {    
                var that = this;
                this.successStop = successCb;
                this.errorStop = errorCb;
                
                var runningWatch = this.getStopwatchForOrderId(order);

                if (runningWatch) {
                    runningWatch.stop();

                        this._insertTimeEntry(order, runningWatch, function(watch, timeHeader) {
                            that._removeWatch(watch);
                            that.successStop(timeHeader);
                        }, that.errorStop);
                    }else{
                        that.successStop();
                    }
            },

            getStopwatchForOrderId: function (order) {
                var foundWatches = this.watches.filter(function(w) {
                    return w.orderId == order.ORDERID && w.activity == order.OPERATION_ACTIVITY;
                });

                return foundWatches.length > 0 ? foundWatches[0] : null;
            }
        };

        StopwatchManager.prototype.onViewModelError = function (err) {
            MessageBox.error(err.message);
        };
       
        StopwatchManager.prototype._insertTimeEntry = function(order, watch, successCb, errorCb) {
            
            var that = this;
            this.runningWatch = watch;
            var timeHeader = new TimeConfirmation(null, this.requestHelper, this.viewModel);
            timeHeader.setStartTime(watch.startDateTime);
            timeHeader.setEndTime(watch.endDateTime);

            timeHeader.ORDERID = watch.orderId;
            timeHeader.DISPLAY_ORDERID = formatter.removeLeadingZeros(watch.orderId).toString();
            timeHeader.PERS_NO = watch.perNr;
            if(order.ACTIVITY) {
                timeHeader.ACTIVITY = order.ACTIVITY;
            } else {
                timeHeader.ACTIVITY = order.OPERATION_ACTIVITY;
            }
            timeHeader.DISPLAY_ACTIVITY = formatter.removeLeadingZeros(timeHeader.ACTIVITY).toString();
            // TODO add plant + work center data from user information
            timeHeader.WORK_CNTR = order.OPERATION_WK_CTR;
            timeHeader.UN_WORK = "MIN";
            timeHeader.PLANT = order.PLANT;
            timeHeader.USERID = this.component.globalViewModel.model.getProperty("/userName");
            timeHeader.CONF_NO = order.OPERATION_CONF_NO;
            timeHeader.KTEXT = order.OPERATION_DESCRIPTION;
    
            var onSuccess = function(timeHeader) {
                successCb(that.runningWatch, timeHeader);
            };

            
            var currentTime = TimeHelper.getCurrentDateObject();
            timeHeader.CREATE_DAT = currentTime.dateString;
            timeHeader.CREATE_TIM = currentTime._dateString;

            onSuccess(timeHeader)
        };

        StopwatchManager.prototype.loadAllTimeEntries= function () {
            var that = this;
            return new Promise(function (resolve, reject) {
                
                that.component.requestHelper.getData(
                    "TimeConfirmation",
                    {persNo: that.component.globalViewModel.model.getProperty("/personnelNumber")},
                    resolve, 
                    reject,
                    true,
                    "getForUser"
                );
            });

        };

   
       StopwatchManager.prototype._start = function (order) {
          this.watches.push(this._getNewWatch(order));
          this._saveLocalStorage();
       };
       
        StopwatchManager.prototype._initWatches = function () {
            this.watches = JSON.parse(localStorage.getItem(this._LS_KEY)).map(function(watchData) {
                var watch = new Stopwatch();
                watch.setData(watchData);
                return watch;
            });
        };

        StopwatchManager.prototype._saveLocalStorage = function () {
            localStorage.setItem(this._LS_KEY, JSON.stringify(this.watches));
        };

        StopwatchManager.prototype._getNewWatch = function(order) {
            return new Stopwatch(order.getOrderId(), order.OPERATION_ACTIVITY, this.viewModel.getUserInformation().personnelNumber, order.whichUser);
        };

        StopwatchManager.prototype._removeWatch = function(watch) {
            for (var i = 0; i < this.watches.length; i++) {
                if (watch.id == this.watches[i].id) {
                    this.watches.splice(i, 1);
                    break;
                }
            }

            this._saveLocalStorage();
        };

        StopwatchManager.prototype.constructor = StopwatchManager;

        return StopwatchManager;
    }
);
