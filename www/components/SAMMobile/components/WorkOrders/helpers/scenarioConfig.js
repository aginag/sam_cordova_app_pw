sap.ui.define([],
    function () {
        "use strict";
        return {

            SAM: {
                orderUserStatus: {
                    reject: {
                        key: "reject",
                        status: "E0002"
                    },
                    workFinished: {
                        key: "workFinished",
                        status: 'E0004'
                    },
                    complete: {
                        key: "complete",
                        status: 'E0006'
                    },
                    done: {
                        key: "done",
                        STATUS: "E0002",
                        USER_STATUS: "E0002",
                        USER_STATUS_DESC: "Done",
                        USER_STATUS_CODE: "DONE",
                        STATUS_PROFILE: "PM_MRSS"
                    },
                    prio: {
                        key: "prio",
                        STATUS: "E0003",
                        USER_STATUS: "E0003",
                        USER_STATUS_DESC: "Priorisiert einzuplanen",
                        USER_STATUS_CODE: "PRIO",
                        STATUS_PROFILE: "PM_MRSS"
                    },
                    down: {
                        key: "down",
                        STATUS: "E0005",
                        USER_STATUS: "E0005",
                        USER_STATUS_DESC: "Download erfolgt",
                        USER_STATUS_CODE: "DOWN",
                        STATUS_PROFILE: "PM_MRSS"
                    },
                    zfix: {
                        key: "zfix",
                        STATUS: "E0018"
                    },
                    minMax: {
                        noReadingRequired: {
                            STATUS: "E0001",
                            STATUS_PROFILE: "PMC_ZE3"
                        },
                        readingCaptured: {
                            STATUS: "E0002",
                            STATUS_PROFILE: "PMC_ZE3"
                        }
                    }
                },
                longTexts: {
                    orders: {
                        list: {
                            isOrderLongtext: true,
                            objectType: "AUFK",
                            readOnly: true
                        },
                        descriptionTab: {
                            isOrderLongtext: true,
                            objectType: "AUFK",
                            objectName: "Order",
                            readOnly: false
                        }
                    },
                    operations: {
                        isOrderLongtext: true,
                        objectType: "AVOT",
                        readOnly: true
                    }
                },
                summary: {
                    custSideVisible: true,
                    dewaSideVisible: true,
                    moveToVisible: true
                },
                meterReplacementInfo: {
                    visible: true
                },
                complaintNotification: {
                    visible: true,
                    completeStatus: {
                        STATUS_PROFILE: "NORDEX3",
                        USER_STATUS: "E0004"
                    },
                    longTextObjectType: "QMEL",
                    withReferenceToServiceOrder: true,
                    showWarrantyFlag: true
                },
                
                availableNotificationTypes: ["M1", "M2", "M3"],
                afterSync: {
                    fromStatus: "E0004",
                    toStatus: {
                        STATUS: "E0005",
                        STATUS_PROFILE: "PM_MRSS"
                    }
                },

                maxDailyWorkInMinutes: 600
            }

        };
    });