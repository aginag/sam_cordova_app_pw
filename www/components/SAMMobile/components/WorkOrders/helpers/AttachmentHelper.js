sap.ui.define(["SAMMobile/helpers/MediaCapture", "SAMMobile/helpers/FileSystem", "SAMMobile/helpers/fileHelper", "SAMMobile/models/dataObjects/SAMObjectAttachment"],

    function (MediaCapture, FileSystem, fileHelper, SAMObjectAttachment) {
        "use strict";

        // constructor
        function AttachmentHelper(requestHelper) {
            this.mediaCapture = new MediaCapture();
            this.fileSystem = new FileSystem();
            this.fileHelper = fileHelper;

            this.requestHelper = requestHelper;
            this._errorCallback = null;
        }

        AttachmentHelper.prototype = {
            setErrorCallback: function(errorCb) {
                this._errorCallback = errorCb;
            },

            getErrorCallback: function(errorCb) {
                return this._errorCallback;
            },

            executeErrorCallback: function(error) {
                if (!this.getErrorCallback()) {
                    return;
                }

                this.getErrorCallback()(error);
            },

            getNewAttachmentObject: function() {
                return new SAMObjectAttachment(null, this.requestHelper, this);
            },

            capturePhoto: function(objectAttachment, successCb) {
                this.mediaCapture.capturePhoto(this._onPhotoCaptureSuccess.bind(this, objectAttachment, successCb), this._errorCallback.bind(this, new Error("Error while capturing photo")));
            }
        };

        AttachmentHelper.prototype.addFileFromSystem = function(file, objectAttachment, successCb) {
            var that = this;
            var binaryString, fileBlob;

            var reader = new FileReader();

            reader.onload = function(readerEvt) {
                binaryString = readerEvt.target.result.split(",")[1];
                fileBlob = that.fileHelper.b64toBlob(binaryString, file.type, 512);

                objectAttachment.setFilename(new Date().getTime().toString());
                objectAttachment.setFileExt(fileBlob.type.split("/")[1]);
                objectAttachment.setFilesize(readerEvt.total.toString());

                that._writeToFileSystem(objectAttachment, fileBlob, successCb);
            };

            reader.readAsDataURL(file);
        };

        AttachmentHelper.prototype._writeToFileSystem = function(objectAttachment, fileBlob, successCb) {
            var that = this;

            var onWriteSuccess = function(filePath) {
                objectAttachment.setFolderPath(getFolderPath(filePath));

                successCb(objectAttachment);
                //that.addLocalAttachment(attachmentObj, bContext);
            };

            var onWriteError = function(msg) {
                this._errorCallback(new Error(msg));
            };

            this.fileSystem.write(objectAttachment.getFileName() + "." + objectAttachment.getFileExt(), fileBlob, onWriteSuccess, onWriteError);
        };

        AttachmentHelper.prototype._onPhotoCaptureSuccess = function(data, objectAttachment, successCb) {

            var that = this;
            var photo = data[0];

            var onPhotoMoved = function(fileEntry) {

                objectAttachment.setFolderPath(fileEntry.nativeURL);

                successCb(objectAttachment);
            };

            that._resizeImage(photo.localURL, photo.name, function(filePath, fileSize){
                var device = sap.ui.Device.os;
                var os = device.name;
                var fileExt = getFileExtension(filePath);
                var fileName = new Date().getTime().toString() + "." + fileExt;

                objectAttachment.setFilename(fileName);
                objectAttachment.setFileExt(fileExt);
                objectAttachment.setFilesize(fileSize);

                switch (os) {
                    case device.OS.ANDROID:
                    case device.OS.IOS:
                        that.fileSystem.moveFile2(filePath, that.fileSystem.getRootDataStorage(),
                            fileName, onPhotoMoved, that._errorCallback.bind(this, new Error("Error while trying to move file to root data storage")));
                        break;
                    default:
                        //windows
                        window.resolveLocalFileSystemURL(filePath, onPhotoMoved, that._errorCallback.bind(this, new Error("Error while trying to move file to root data storage")));
                        break;
                }

            }, that._errorCallback.bind(this, new Error("Error while trying to resize image")));
        };

        AttachmentHelper.prototype._resizeImage = function(filePath, fileName, successCallback, errorCallback) {
            var that = this;

            this.fileSystem.getFileSize(filePath, function(fileSize){
                //, width, height
                if(fileSize > that.maxSize){
                    //resize
                    var options =  {
                        uri: filePath,
                        quality: 80,
                        height: me.minHeight,
                        width: me.minWidth,
                        fileName: fileName};

                    window.ImageResizer.resize(options,
                        function(imagePath) {
                            // success: image is the new resized image
                            //delete old image
                            that.fileSystem.deleteByPath(filePath,function(){
                                //call resize again
                                var filename = imagePath.split("/").pop();
                                that._resizeImage(imagePath, filename, successCallback, errorCallback);
                            }, errorCallback);
                        }, function() {
                            //compression failed - error callback
                            errorCallback('image resize failed');
                        });
                }else{
                    //call success callback
                    successCallback(filePath, fileSize);
                }

            }, errorCallback);

        };

        AttachmentHelper.prototype.constructor = AttachmentHelper;

        return AttachmentHelper;
    });