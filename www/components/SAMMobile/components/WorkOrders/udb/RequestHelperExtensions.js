sap.ui.define([],
    function () {
        "use strict";
        return {
            tableColumns: {
                OrdersJoinOrderOperationsDetailScreen: ["hist.ORDERID as HISTORIC", "us.STATUS AS OPERATION_USER_STATUS", "us.USER_STATUS_CODE AS OPERATION_USER_STATUS_CODE", "us.ACTION_FLAG as OPERATION_USER_STATUS_ACTIONFLAG", "a.MOBILE_ID", "a.ORDERID", "a.NOTIF_NO", "a.FUNCT_LOC", "a.FUNCLOC_DESC", "a.EQUIPMENT", "a.EQUIPMENT_DESC", "a.PRIORITY", "a.ORDERID_DISPLAY", "b.OBJECT_NO", "a.ORDER_TYPE", "a.USERSTATUS", "a.STAT_PROF", "a.MN_WK_CTR",
                    "a.SHORT_TEXT", "a.SYS_STATUS", "a.PMACTTYPE", "b.WORK_CNTR", "b.ACTTYPE", "a.PLANGROUP", "a.PLANT", "a.PLANPLANT", "a.ACTION_FLAG", "a.ORDER_TYPE", "b.CONF_NO as OPERATION_CONF_NO", "b.PERS_NO", "b.TEAM_GUID", "b.TEAM_PERSNO", "b.ACTIVITY as OPERATION_ACTIVITY", "b.XA_STAT_PROF AS OPERATION_STATUS_PROFILE", "b.START_CONS AS OPERATION_STARTDATE", "b.STRTTIMCON AS OPERATION_STARTTIME", "b.FIN_CONSTR AS OPERATION_ENDDATE", "b.FINTIMCONS AS OPERATION_ENDTIME", "b.DESCRIPTION as OPERATION_DESCRIPTION",
                    "b.WORK_CNTR as OPERATION_WK_CTR", "b.SYSTCOND", "nh.NOTIF_TYPE", "nh.OBJECT_NO AS NOTIF_OBJECT_NO", "nh.SHORT_TEXT as NOTIFICATION_DESCRIPTION", "nh.MOBILE_ID AS NOTIF_MOBILE_ID", "nh.ACTION_FLAG AS NOTIF_ACTION_FLAG", "nh.XA_STAT_PROF AS NOTIF_STATUS_PROFILE", "fl.TPLMA", "fl.TPLMA_SHTXT", "ot.TERMIN_START", "ot.TERMIN_END"],
                OrdersJoinOrderOperations: ["us.STATUS AS OPERATION_USER_STATUS", "us.USER_STATUS_CODE AS OPERATION_USER_STATUS_CODE", "us.ACTION_FLAG as OPERATION_USER_STATUS_ACTIONFLAG", "usml.USER_STATUS_DESC AS OPERATION_USER_STATUS_TEXT", "b.ACTIVITY as OPERATION_ACTIVITY", "b.START_CONS AS OPERATION_STARTDATE", "b.STRTTIMCON AS OPERATION_STARTTIME", "b.FIN_CONSTR AS OPERATION_ENDDATE", "b.FINTIMCONS AS OPERATION_ENDTIME", "a.MOBILE_ID", "a.ORDERID", "a.NOTIF_NO", "a.FUNCT_LOC", "a.FUNCLOC_DESC", "a.PRIORITY", "a.ORDERID_DISPLAY", "a.OBJECT_NO", "a.ORDER_TYPE", "a.USERSTATUS", "a.STAT_PROF",
                    "a.SHORT_TEXT", "a.SYS_STATUS", "b.WORK_CNTR", "a.ACTION_FLAG", "b.PERS_NO", "b.XA_STAT_PROF AS OPERATION_STATUS_PROFILE", "b.LATE_SCHED_START_DATE", "b.LATE_SCHED_START_TIME", "pt.PRIORITY_DESC", "nh.NOTIF_TYPE", "nh.OBJECT_NO as NOTIF_OBJECT_NO", "nh.MOBILE_ID as NOTIF_MOBILE_ID", "nh.ACTION_FLAG as NOTIF_ACTION_FLAG"],
                OrderOperationsLite: ["MOBILE_ID", "OBJECT_NO", "ORDERID", "ACTIVITY", "ACTION_FLAG", "WORK_CNTR"],
                OrderOperationsJoinsUserStatus: ["os.MOBILE_ID", "os.STATUS", "os.ORDERID", "os.USER_STATUS_DESC", "os.USER_STATUS_CODE", "os.OBJNR", "os.INACT", "os.CHGNR", "os.CHANGED", "os.ACTION_FLAG", "o.PERS_NO", "o.TEAM_GUID", "o.TEAM_PERSNO"],
                OrderOperationsJoinsMaterialConfirmations: ["mc.MOBILE_ID", "mc.ORDER_ITNO", "mc.ITEM_TEXT", "mc.SERIALNO", "mc.ACTIVITY", "mc.ORDERID", "mc.MATERIAL", "mc.ENTRY_QNT", "mc.ENTRY_UOM", "mc.MAT_DOC", "mc.DOC_YEAR", "mc.PLANT", "mc.STGE_LOC", "mc.CALC_MOTIVE", "mc.ACTION_FLAG"],
                OrderOperationsJoinsComponents: ["oc.MOBILE_ID", "ml.MATL_DESC", "oc.ITEM_TEXT", "oc.ITEM_CAT", "oc.ACTIVITY", "oc.ORDERID", "oc.MATERIAL", "oc.ENTRY_QNT", "oc.ENTRY_UOM", "oc.REQUIREMENT_QUANTITY", "oc.REQUIREMENT_QUANTITY_UNIT", "oc.RESERV_NO", "oc.RES_ITEM", "oc.PLANT", "oc.STGE_LOC", "oc.ACTION_FLAG"],
                OrderOperationsJoinsCatsHeader: ["ch.MOBILE_ID", "ch.ACTIVITY", "ch.AWART", "ch.BEGUZ", "ch.ENDUZ", "ch.BEMOT", "ch.CATSHOURS", "ch.COUNTER", "ch.LSTAR", "ch.MEINH", "ch.PERNR", "ch.RAUFNR", "ch.SKOSTL", "ch.WERKS", "ch.WORKDATE", "ch.WORK_CNTR", "ch.ACTION_FLAG"],

                OrderSiteDetails: ["na.ACTTEXT", "cd.CODE_GROUP as ACT_CODEGRP", "cd.CODE as ACT_CODE", "cd.CODE_TEXT", "cd.CATALOG_TYPE as ACT_CAT_TYP", "na.TXT_ACTGRP ", "na.ACTION_FLAG", "na.MOBILE_ID", "nh.NOTIF_NO", "nh.MOBILE_ID as NOTIF_MOBILE_ID"],
                OrderOperationsJoinsTimeConf: ["t.MOBILE_ID", "t.ACTIVITY", "t.ACT_TYPE", "t.START_TIME", "t.END_TIME", "t.CONF_TEXT", "t.CONF_NO", "t.CONF_CNT", "t.CALC_MOTIVE", "t.ACT_WORK", "t.UN_WORK", "t.PERS_NO", "t.COMPLETE", "t.WORK_CNTR", "t.ACTION_FLAG"],

                InventoryList: ["i.MOBILE_ID", "i.MATERIAL", "i.PLANT", "i.STGE_LOC", "i.ENTRY_QNT", "i.MATL_DESC", "i.ENTRY_UOM", "i.ACTION_FLAG", "i.BATCH", "sl.TEXT", "ml.BATCH as BATCH_REQ", "ml.MATL_DESC"],
                EquipmentsJoinsMl: ["e.MOBILE_ID", "e.TPLNR", "e.HEQUI", "e.EQUIPMENT_DISPLAY", "e.SHTXT", "e.OBJNR", "e.EQUNR", "e.STREET", "e.SWERK", "e.POST_CODE1", "e.CITY1", "e.COUNTRY", "e.RBNR", "e.TPLNR_SHTXT", "e.ACTION_FLAG"],
                FunctionLocs: ["MOBILE_ID", "EQUNR", "TPLNR", "FUNCLOC_DISP", "EQTYP", "EQART", "OBJNR", "MATNR", "WERK", "LAGER", "SHTXT", "TPLNR_SHTXT", "TPLMA_SHTXT", "FLTYP", "SERNR", "STREET", "POST_CODE1",
                    "SWERK", "HERLD", "TPLMA", "CITY1", "COUNTRY", "ACTION_FLAG"],
                ChecklistJoinsNotificationHeader: ["cl.MOBILE_ID", "cl.ORDERID", "cl.NOTIF_NO", "cl.SORT_NO", "cl.TASK_NO", "cl.TPLNR", "cl.SPRAS", "cl.CHECK_TYPE", "cl.CHECK_KEY", "cl.TASK_TEXT", "cl.DESCRIPTION", "cl.ZRESULT_READING", "cl.ZRESULT_CODE", "cl.DECIM", "cl.MRMIN", "cl.MRMAX", "cl.USER_TEXT", "cl.ACTION_FLAG", /*"nh.MOBILE_ID as NOTIF_MOBILE_ID",*/ "nt.TXT_TASKCD", "nt.TXT_TASKGRP", "nt.TASK_CODEGRP", "nt.XA_STAT_PROF", "cl.USER_TEXT"],
                ChecklistJoinsNotificationHeaderCategories: ["nt.TXT_TASKGRP", "nt.TASK_CODEGRP"],

                MinMaxChecklist: ["cl.MOBILE_ID", "cl.ACTION_FLAG", "cl.ORDERID", "cl.NOTIF_NO", "cl.SORT_NO", "cl.TASK_NO", "cl.TPLNR", "cl.SPRAS", "cl.CHECK_TYPE", "cl.CHECK_KEY", "cl.TASK_TEXT", "cl.DESCRIPTION", "cl.USER_TEXT", "cl.ZRESULT_READING", "cl.ZRESULT_CODE", "cl.DECIM", "cl.MEAS_POINT", "mu.MSEHT", "cl.USER_FIELD"],

                FunctionLocJoinsMl: ["f.MOBILE_ID", "f.EQUNR", "f.TPLNR", "f.FUNCLOC_DISP", "f.EQTYP", "f.EQART", "f.OBJNR", "f.MATNR", "f.WERK", "f.LAGER", "f.SHTXT", "fml.SHTXT AS TPLNR_SHTXT",
                    "fml.TPLMA_SHTXT", "f.FLTYP", "f.SERNR", "f.STREET", "f.POST_CODE1", "f.SWERK", "f.HERLD", "f.TPLMA", "f.CITY1", "f.COUNTRY", "f.ACTION_FLAG", "ec.TYPTX"],
                DewaMaterialsJoinsInventoryList: ["m.MATNR", "m.LGORT", "m.WERKS", "m.MANDT", "ml.MATL_DESC", "ml.ENTRY_UOM"], // "i.MATL_DESC", "i.ENTRY_UOM"]
                SAMObjectAttachments: ["a.MOBILE_ID", "a.TABNAME", "a.OBJKEY", "a.NEW_MOBILE_ID", "a.LINKID", "a.FILENAME", "a.FILEEXT", "a.DOC_SIZE", "a.ACTION_FLAG", "b.EQUIPMENT_DESC", "b.FUNCLOC_DESC", "b.SHORT_TEXT", "c.TXT_TASKCD"],

                //Operations
                operationOverview: ["ACTIVITY", "START_CONS", "STRTTIMCON", "FIN_CONSTR", "FINTIMCONS", "DESCRIPTION", "PERS_NO", "ORDERID", "DURATION_NORMAL", "DURATION_NORMAL_UNIT"],

                OperationsJOIN: ["a.ORDERID", "a.WORK_CNTR", "a.ACTTYPE", "a.OBJECT_NO", "a.ACTIVITY", "a.ACTIVITY as OPERATION_ACTIVITY", "a.START_CONS", "b.PMACTTYPE", "a.STRTTIMCON", "a.FIN_CONSTR", "a.FINTIMCONS", "a.DESCRIPTION", "a.SYSTCOND", "a.MOBILE_ID", "a.PERS_NO", "a.TEAM_GUID", "a.TEAM_PERSNO", "a.ZZ_GEOLAT AS latitude", "a.ZZ_GEOLON AS longitude", "b.ORDER_TYPE", "b.PRIORITY", "b.FUNCT_LOC", "b.FUNCLOC_DESC", "b.EQUIPMENT", "b.NOTIF_NO", "a.XA_STAT_PROF AS OPERATION_STATUS_PROFILE", "o.ARTPR", "t.TEAM_BEG_DATUM", "t.TEAM_BEG_UZEIT", "t.TEAM_END_DATUM", "t.TEAM_END_UZEIT", "f.CITY1", "f.STREET", "f.COUNTRY", "f.POST_CODE1", "ot.TERMIN_START", "ot.TERMIN_END",
                    "a.WORK_CNTR as OPERATION_WK_CTR", "b.PLANT", "a.CONF_NO as OPERATION_CONF_NO", "a.DESCRIPTION as OPERATION_DESCRIPTION"],

                OperationsJOINCalendar: ["a.ORDERID", "a.OBJECT_NO", "a.ACTIVITY", "a.START_CONS", "a.STRTTIMCON", "a.FIN_CONSTR", "a.FINTIMCONS", "a.DESCRIPTION", "a.MOBILE_ID", "a.TEAM_GUID", "t.TEAM_BEG_DATUM", "t.TEAM_BEG_UZEIT", "t.TEAM_END_DATUM", "t.TEAM_END_UZEIT", "ot.TERMIN_START", "ot.TERMIN_END", "b.FUNCT_LOC", "b.FUNCLOC_DESC", "f.CITY1", "f.STREET", "b.ORDER_TYPE"],
                OperationsJOINCalendarUserStatus: ["o.ORDERID", "o.OBJECT_NO", "o.ACTIVITY", "o.MOBILE_ID"],

                //MapsMarker
                MapMarkerAll: ["a.ORDERID", "a.ACTIVITY", "a.DESCRIPTION", "b.ORDER_TYPE", "b.FUNCT_LOC", "b.FUNCLOC_DESC", "f.CITY1 AS TP_CITY", "f.STREET AS TP_STREET", "f.COUNTRY AS TP_COUNTRY", "f.POST_CODE1 AS TP_POST", "f.GEOLAT AS latitude", "f.GEOLON AS longitude"],
                EquipmentMarker: ["a.ACTIVITY", "b.ORDERID", "b.EQUIPMENT", "d.LONGITUDE", "d.LATITUDE", "d.SHTXT", "d.POST_CODE1", "d.STREET"],
                EquipmentOrderHist: ["o.MOBILE_ID", "o.ORDERID", "o.ORDER_TYPE", "o.SHORT_TEXT", "o.START_DATE", "o.ACTION_FLAG", "op.ACTIVITY", "op.PERS_NO"],
                EquipmentNotifHist: ["nh.MOBILE_ID", "nh.NOTIF_NO", "nh.NOTIF_TYPE", "nh.SHORT_TEXT", "nh.NOTIF_DATE", "nh.ACTION_FLAG"],
                Inspections: ["o.ORDERID", "oo.DESCRIPTION", "oo.ACTIVITY", "emp.MEASUREMENT_DESC as EQU_MEASUREMENT_DESC", "ool.FUNCT_LOC", "ool.FUNCLDESCR", "ool.EQUIPMENT", "ool.EQUIDESCR", "emp.MEASUREMENT_POINT as EQUIPMENT_MEASUREMENT_POINT", "emp.MEASUREMENT_DESC as EQUIPMENT_MEASUREMENT_DESC", "fmp.MEASUREMENT_DESC as FUNCT_LOC_MEASUREMENT_DESC ", "fmp.MEASUREMENT_POINT as FUNCT_LOC_MEASUREMENT_POINT", "emp.MRNGU as UNIT_EQU", "fmp.MRNGU as UNIT_FUNC", "emp.MRMAX as EQUIPMENT_MEASPOINT_MAX", "emp.MRMIN as EQUIPMENT_MEASPOINT_MIN", "fmp.MRMIN as FUNCLOC_MEASPOINT_MIN", "fmp.MRMAX as FUNCLOC_MEASPOINT_MAX"],
                MeasurementDocuments: ["MOBILE_ID", "ERDAT", "ERNAM", "READG", "POINT", "RECDV", "RECDU", "MDTXT", "CODGR", "VLCOD"],
                MeasurementDocumentsLastReading: ["md.ACTION_FLAG", "md.POINT", "md.RECDV", "md.IDATE"],
                EquipmentHierarchy: ["EQUNR", "SHTXT", "TPLNR", "HEQUI"],
                FunclocHierarchy: ["TPLNR", "SHTXT", "TPLMA"],
                TeamInfo: ["TEAM_BEG_DATUM", "TEAM_BEG_UZEIT", "TEAM_END_DATUM", "TEAM_END_UZEIT", "TEAM_GUID", "TEAM_NAME"],
                TeamMembers: ["TEAM_GUID", "RES_NAME", "RES_FLAG_RESP"],
                SAM_CHECKLIST_FORM_SAVE: ["MOBILE_ID", "FORM_NAME", "HTML_CONTENT", "HTML_STATE", "ORDERID"],
                EquipmentCharacsJoinsCharacteristics: ["ec.mobile_id", "ec.CHARACT", "ec.CLASS", "ec.CHARACT_DESCR", "ec.EQUNR", "ec.VALUE_NEUTRAL_FROM", "ec.VALUE_NEUTRAL_TO", "ec.VALUE_TO", "ec.VALUE_FROM", "ec.UNIT_FROM", "ec.UNIT_TO",
                    "ec.VALUE_DATE", "ec.TYPE", "ec.ACTION_FLAG", "cv.ATINN", "c.ATFOR", "c.ATEIN", "c.ATSON", "c.ANZDZ", "c.ANZST", "c.ATSCH", "u.MSEHT", "cc.POSNR"],
                FuncLocCharacsJoinsCharacteristics: ["fc.mobile_id", "fc.CHARACT", "fc.CLASS", "fc.CHARACT_DESCR", "fc.TPLNR", "fc.VALUE_NEUTRAL_FROM", "fc.VALUE_NEUTRAL_TO", "fc.VALUE_TO", "fc.VALUE_FROM", "fc.UNIT_FROM", "fc.UNIT_TO",
                    "fc.VALUE_DATE", "fc.TYPE", "fc.ACTION_FLAG", "cv.ATINN", "c.ATFOR", "c.ATEIN", "c.ATSON", "c.ANZDZ", "c.ANZST", "c.ATSCH", "u.MSEHT", "cc.POSNR"],
                EquipmentMeasurementPoints: ["em.ACTION_FLAG", "em.ATINN", "em.BEGRU", "em.CDSUF", "em.CJUMP", "em.CODGR", "em.DECIM", "em.DESIR", "em.DSTXT", "em.EXPON", "em.INDCT", "em.INDTR", "em.LOCAS", "em.PSORT", "em.MEASUREMENT_DESC", "em.MEASUREMENT_POINT", "em.MOBILE_ID", "em.MPTYP", "em.MRMAX", "em.MRMIN", "em.MRNGU", "em.OBJNR", "em.PYEAR", "em.EQUNR", "em.TRANS", "em.XA_MEASUREMENT_POINT"],
                CharacteristicValuesJoinsMl: ["cv.ADZHL", "cv.ATFLB", "cv.ATFLV", "cv.ATINN", "cv.ATWRT", "cvml.ATWTB", "cv.ATZHL"],
                EquipmentDetails: ["eq.EQUNR", "wml.KTEXT as WKCTR", "eq.TPLMA_SHTXT as CATEGORY_SHTXT", "eq.TPLNR_SHTXT as TYP_SHTXT", "eq.HERST", "fl.TPLNR_SHTXT as PLANTLOCATION_SHTXT", "eq.BAUMM", "eq.BAUJJ", "eq.SERGE"],
                AM_ObjList: ["ILAR"],
                AM_MPoint: ["ILART", "MPOS"],
                FuncLocMeasurementPoints: ["fm.ACTION_FLAG", "fm.ATINN", "fm.BEGRU", "fm.CDSUF", "fm.CJUMP", "fm.CODGR", "fm.DECIM", "fm.DESIR", "fm.DSTXT", "fm.EXPON", "fm.INDCT", "fm.INDTR", "fm.LOCAS", "fm.MEASUREMENT_DESC", "fm.MEASUREMENT_POINT", "fm.MOBILE_ID", "fm.MPTYP", "fm.MRMAX", "fm.MRMIN", "fm.MRNGU", "fm.OBJNR", "fm.PSORT", "fm.PYEAR", "fm.TPLNR", "fm.TRANS", "fm.XA_MEASUREMENT_POINT"]

            },
            queries: {
                Orders: {
                    get: {
                        query: "SELECT $COLUMNS FROM Orders as a "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID AND c.PERS_NO = '$4') "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN NotificationHeader as nh on nh.NOTIF_NO = a.NOTIF_NO "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE a.ORDERID = '$1'", // AND b.PERS_NO = '$2'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "persNo"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "persNo"
                        }],
                        columns: "OrdersJoinOrderOperationsDetailScreen"
                    },

                    open: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM Orders as a LEFT JOIN PriorityTypes as pt ON pt.PRIORITY = a.PRIORITY AND pt.PRIOTYPE = substring(a.ORDER_TYPE, 1, 2) AND pt.SPRAS = '$3' "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID AND c.PERS_NO = '$13') "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN USERSTATUSML as usml on usml.USER_STATUS = us.STATUS and usml.STATUS_PROFILE = b.XA_STAT_PROF and usml.SPRAS = '$14' "
                            + "LEFT JOIN NotificationHeader as nh on nh.NOTIF_NO = a.NOTIF_NO "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO = '$4' AND (OPERATION_USER_STATUS IS NULL OR (OPERATION_USER_STATUS != '$11' AND OPERATION_USER_STATUS != '$12' AND (OPERATION_USER_STATUS_CODE LIKE 'REC%' OR OPERATION_USER_STATUS_CODE NOT LIKE 'RE%'))) "
                            + "AND (a.ORDERID LIKE '%$5%' OR a.START_DATE LIKE '%$6%' OR a.FINISH_DATE LIKE '%$7%' OR UPPER(a.FUNCT_LOC) LIKE UPPER('%$8%') "
                            + "OR UPPER(a.SHORT_TEXT) LIKE UPPER('%$9%') OR UPPER(a.FUNCLOC_DESC) LIKE UPPER('%$10%')) ORDER BY a.START_DATE ASC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "persNo"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "searchString"
                        }, {
                            parameter: "$11",
                            value: "completeStatus"
                        }, {
                            parameter: "$12",
                            value: "rejectedStatus"
                        }, {
                            parameter: "$13",
                            value: "persNo"
                        }, {
                            parameter: "$14",
                            value: "spras"
                        }],
                        columns: "OrdersJoinOrderOperations"
                    },
                    complete: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM Orders as a LEFT JOIN PriorityTypes as pt ON pt.PRIORITY = a.PRIORITY AND pt.PRIOTYPE = substring(a.ORDER_TYPE, 1, 2) AND pt.SPRAS = '$3' "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID AND c.PERS_NO = '$13') "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN USERSTATUSML as usml on usml.USER_STATUS = us.STATUS and usml.STATUS_PROFILE = b.XA_STAT_PROF and usml.SPRAS = '$14' "
                            + "LEFT JOIN NotificationHeader as nh on nh.NOTIF_NO = a.NOTIF_NO "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO = '$4' AND OPERATION_USER_STATUS = '$11' "
                            + "AND (a.ORDERID LIKE '%$5%' OR a.START_DATE LIKE '%$6%' OR a.FINISH_DATE LIKE '%$7%' OR UPPER(a.FUNCT_LOC) LIKE UPPER('%$8%') "
                            + "OR UPPER(a.SHORT_TEXT) LIKE UPPER('%$9%') OR UPPER(a.FUNCLOC_DESC) LIKE UPPER('%$10%')) ORDER BY a.START_DATE ASC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "persNo"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "searchString"
                        }, {
                            parameter: "$11",
                            value: "completeStatus"
                        }, {
                            parameter: "$12",
                            value: "rejectedStatus"
                        }, {
                            parameter: "$13",
                            value: "persNo"
                        }, {
                            parameter: "$14",
                            value: "spras"
                        }],
                        columns: "OrdersJoinOrderOperations"
                    },
                    rejected: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM Orders as a LEFT JOIN PriorityTypes as pt ON pt.PRIORITY = a.PRIORITY AND pt.PRIOTYPE = substring(a.ORDER_TYPE, 1, 2) AND pt.SPRAS = '$3' "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID AND c.PERS_NO = '$13') "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '') "
                            + "LEFT JOIN USERSTATUSML as usml on usml.USER_STATUS = us.STATUS and usml.STATUS_PROFILE = b.XA_STAT_PROF and usml.SPRAS = '$14' "
                            + "LEFT JOIN NotificationHeader as nh on nh.NOTIF_NO = a.NOTIF_NO "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO LIKE '%%' AND (OPERATION_USER_STATUS_CODE LIKE 'RE%' AND OPERATION_USER_STATUS_CODE NOT LIKE 'REC%') "
                            + "AND (a.ORDERID LIKE '%$5%' OR a.START_DATE LIKE '%$6%' OR a.FINISH_DATE LIKE '%$7%' OR UPPER(a.FUNCT_LOC) LIKE UPPER('%$8%') "
                            + "OR UPPER(a.SHORT_TEXT) LIKE UPPER('%$9%') OR UPPER(a.FUNCLOC_DESC) LIKE UPPER('%$10%')) ORDER BY a.START_DATE ASC",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "persNo"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "searchString"
                        }, {
                            parameter: "$10",
                            value: "searchString"
                        }, {
                            parameter: "$11",
                            value: "completeStatus"
                        }, {
                            parameter: "$12",
                            value: "rejectedStatus"
                        }, {
                            parameter: "$13",
                            value: "persNo"
                        }, {
                            parameter: "$14",
                            value: "spras"
                        }],
                        columns: "OrdersJoinOrderOperations"
                    }
                },
                OrderOperationDetail: {
                    get: {
                        query: "SELECT $COLUMNS FROM OrderOperations as b "
                            + "LEFT JOIN Orders as a ON a.ORDERID=b.ORDERID "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN NotificationHeader as nh on nh.NOTIF_NO = a.NOTIF_NO "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "LEFT JOIN FUNCTIONLOC as fl on a.FUNCT_LOC = fl.TPLNR "
                            + "LEFT JOIN ZPW_OP_TIME as ot ON b.OBJECT_NO = ot.OBJNR "
                            + "WHERE b.ORDERID = '$1' and b.ACTIVITY = '$4'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "persNo"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "operId"
                        }],
                        columns: "OrdersJoinOrderOperationsDetailScreen"
                    }
                },
                OpenOrderCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM Orders as a "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID) "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO LIKE '%$1%' AND (us.STATUS IS NULL OR (us.STATUS != '$2' AND us.STATUS != '$3' AND (us.USER_STATUS_CODE LIKE 'REC%' OR us.USER_STATUS_CODE NOT LIKE 'RE%'))) ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "completeStatus"
                        }, {
                            parameter: "$3",
                            value: "rejectedStatus"
                        }],
                        columns: ""
                    }
                },

                CompleteOrderCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM Orders as a "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID) "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO LIKE '%$1%' AND us.STATUS = '$2' ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "completeStatus"
                        }, {
                            parameter: "$3",
                            value: "rejectedStatus"
                        }],
                        columns: ""
                    }
                },

                RejectedOrderCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM Orders as a "
                            + "LEFT JOIN OrderOperations as b ON b.ORDERID=a.ORDERID AND b.ACTIVITY = (SELECT MAX(ACTIVITY) FROM OrderOperations as c WHERE c.ORDERID=b.ORDERID) "
                            + "LEFT JOIN OrderUserStatus as us ON us.OBJNR = b.OBJECT_NO AND (us.INACT IS NULL OR us.INACT =  '')  "
                            + "LEFT JOIN SAM_ORDER_HIST as hist on hist.ORDERID = a.ORDERID "
                            + "WHERE hist.ORDERID IS NULL AND b.PERS_NO LIKE '%%' AND us.STATUS = '$3' OR (us.USER_STATUS_CODE NOT LIKE 'REC%' AND us.USER_STATUS_CODE LIKE 'RE%') ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "completeStatus"
                        }, {
                            parameter: "$3",
                            value: "rejectedStatus"
                        }],
                        columns: ""
                    }
                },

                OrderOperations: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o WHERE o.ORDERID = '$1' AND o.PERS_NO = '$2'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "persNo"
                        }],
                        columns: "OrderOperationsJoinsUserStatus"
                    },

                    getPersNoByOrderId: {
                        query: "SELECT TOP 1 PERS_NO FROM ORDEROPERATIONS WHERE ORDERID = '$1' ORDER BY ACTIVITY DESC",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }]
                    },

                    all: {
                        query: "SELECT o.MOBILE_ID as ORDER_MOBILE_ID, op.MOBILE_ID, op.OBJECT_NO, op.ORDERID, op.ACTIVITY, op.SUB_ACTIVITY, op.ACTION_FLAG, op.PERS_NO, op.TEAM_PERSNO FROM ORDEROPERATIONS as op " +
                            "JOIN ORDERS as o on o.ORDERID = op.ORDERID",
                        params: [{}]
                    },

                    userStatuses: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO "
                            + "WHERE o.OBJECT_NO = '$1' AND os.ACTION_FLAG != 'N'",
                        params: [{
                            parameter: "$1",
                            value: "objectNo"
                        }],
                        columns: "OrderOperationsJoinsUserStatus"
                    },

                    deleteRejectionLongtext: {
                        query: "DELETE FROM ORDERLONGTEXT WHERE ORDERID = '$1' AND OBJKEY = '$2' AND OBJTYPE = 'AVOT' AND FORMAT_COL = '$' AND ACTION_FLAG = 'N'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "objkey"
                        }]
                    }
                },

                OrderOperationStatus: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO "
                            + "WHERE o.ORDERID = '$1' AND o.PERS_NO = '$2' AND os.ACTION_FLAG != 'N'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "persNo"
                        }],
                        columns: "OrderOperationsJoinsUserStatus"
                    },

                    all: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO " +
                            "WHERE os.ACTION_FLAG != 'L'",
                        params: [{}],
                        columns: "OrderOperationsJoinsUserStatus"
                    },

                    inError: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO " +
                            "WHERE os.ACTION_FLAG = 'Q' OR os.ACTION_FLAG = 'P' OR os.ACTION_FLAG = 'O'",
                        params: [{}],
                        columns: "OrderOperationsJoinsUserStatus"
                    },

                    mine: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o JOIN ORDERUSERSTATUS as os on os.OBJNR = o.OBJECT_NO " +
                            "WHERE os.ACTION_FLAG != 'L'",
                        params: [{}],
                        columns: "OrderOperationsJoinsUserStatus"
                    }
                },

                Equipments: {
                    installedAtFuncLoc: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM EQUIPMENT as e "
                            + "WHERE e.TPLNR='$4' AND (UPPER(e.SHTXT) LIKE UPPER('%$5') OR e.EQUIPMENT_DISPLAY LIKE '%$6')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "tplnr"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "EquipmentsJoinsMl"
                    }
                },
                FunctionLocs: {
                    get: {
                        query: "SELECT TOP $1 START AT $2 $COLUMNS FROM FUNCTIONLOC  "
                            + "WHERE (UPPER(SHTXT) LIKE UPPER('%$5') OR FUNCLOC_DISPLAY LIKE '%$6')",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "FunctionLocs"
                    }
                },

                FunctionalLocation: {
                    get: {
                        query: "SELECT $COLUMNS FROM FunctionLoc AS f " +
                            "LEFT JOIN FunctionLocMl as fml ON fml.TPLNR = f.TPLNR AND fml.SPRAS='$2' " +
                            "LEFT JOIN EquipmentCategoryMl AS ec ON ec.EQTYP = f.FLTYP AND ec.SPRAS = '$3' " +
                            "WHERE f.TPLNR='$1'",
                        params: [{
                            parameter: "$1",
                            value: "objectId"
                        }, {
                            parameter: "$2",
                            value: "spras"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }],
                        columns: "FunctionLocJoinsMl"
                    },
                },

                Checklist: {
                    get: {
                        query: "SELECT $COLUMNS FROM Checklists as cl " +
                            "LEFT JOIN NOTIFICATIONTASK as nt on nt.NOTIF_NO = cl.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' and cl.CHECK_TYPE = '0' and nt.TASK_CODEGRP <> 'PMC00005' " +
                            "order by (case nt.TASK_CODEGRP when '00001PMC' then 2 else 1 end), nt.TXT_TASKGRP, cl.TASK_NO asc ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "language"
                        }],
                        columns: "ChecklistJoinsNotificationHeader"
                    }
                },

                ChecklistCategories: {
                    get: {
                        query: "SELECT $COLUMNS FROM Checklists as cl " +
                            "LEFT JOIN NOTIFICATIONTASK as nt on nt.NOTIF_NO = cl.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' and cl.CHECK_TYPE = '0' and nt.TASK_CODEGRP <> 'PMC00005' " +
                            "GROUP BY nt.TXT_TASKGRP,nt.TASK_CODEGRP order by (case nt.TASK_CODEGRP when '00001PMC' then 1 else 2 end)desc",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "ChecklistJoinsNotificationHeaderCategories"
                    }
                },

                MinMaxChecklist: {
                    get: {
                        query: "SELECT $COLUMNS, fmp.MOBILE_ID as FMP_MOBILE_ID, fmp.INACT as FMP_INACT FROM Checklists as cl " +
                            "LEFT JOIN MEASUREMENTUNIT as mu on mu.MSEHI = cl.MSEHI and mu.SPRAS = '$1' " +
                            "LEFT JOIN FUNCTIONLOCMEASPOINT as fmp on fmp.MEASUREMENT_POINT = cl.MEAS_POINT " +
                            "WHERE cl.ORDERID = '$7' AND cl.NOTIF_NO = '$8' AND cl.SPRAS = '$9' AND cl.CHECK_TYPE = '$10' AND cl.TPLNR = '$11' ",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }, {
                            parameter: "$7",
                            value: "orderId"
                        }, {
                            parameter: "$8",
                            value: "notifNo"
                        }, {
                            parameter: "$9",
                            value: "language"
                        }, {
                            parameter: "$10",
                            value: "checkType"
                        }, {
                            parameter: "$11",
                            value: "funcLoc"
                        }],
                        columns: "MinMaxChecklist"
                    }
                },

                MinMaxCharacs: {
                    get: {
                        query: "SELECT CHARACT, CHARACT_DESCR, CLASS, VALUE_NEUTRAL_FROM, UNIT_FROM AS MSEHT FROM FUNCTIONLOCCHARACS " +
                            "WHERE TPLNR = '$2' AND ( (CLASS = '$3' AND CHARACT = '$4') OR (CLASS = '$5' AND CHARACT = '$6') ) " +
                            "UNION " +
                            "SELECT CHARACT, CHARACT_DESCR, CLASS, VALUE_NEUTRAL_FROM, UNIT_FROM AS MSEHT FROM EQUIPMENT AS c " +
                            "LEFT JOIN EQUIPMENTCHARACS AS ec ON ec.EQUNR = c.EQUNR " +
                            "WHERE TPLNR = '$8' AND ec.CLASS = '$9' AND ec.CHARACT = '$10' ",
                        params: [{
                            parameter: "$2",
                            value: "funcLoc"
                        }, {
                            parameter: "$8",
                            value: "funcLoc"
                        }, {
                            parameter: "$3",
                            value: "funcLocClass"
                        }, {
                            parameter: "$5",
                            value: "funcLocClass"
                        }, {
                            parameter: "$4",
                            value: "funcLocCharact"
                        }, {
                            parameter: "$6",
                            value: "funcLocCharactAnz"
                        }, {
                            parameter: "$9",
                            value: "equiClass"
                        }, {
                            parameter: "$10",
                            value: "equiCharact"
                        }]
                    }
                },

                MinMaxNoCapturing: {
                    get: {
                        query: "SELECT $COLUMNS FROM Checklists as cl " +
                            "LEFT JOIN NOTIFICATIONTASK as nt on nt.NOTIF_NO = cl.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND nt.TASK_CODEGRP = '$4' AND nt.TASK_CODE = '$5' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "codeGroup"
                        }, {
                            parameter: "$5",
                            value: "code"
                        }],
                        columns: "ChecklistJoinsNotificationHeader"
                    }
                },

                SAMObjectAttachments: {
                    get: {
                        query: "SELECT a.USER_FIELD, a.MOBILE_ID,a.TABNAME,a.OBJKEY,a.NEW_MOBILE_ID,a.FILENAME,a.FILEEXT,a.DOC_SIZE,a.ACTION_FLAG,b.EQUIPMENT_DESC,b.FUNCLOC_DESC,b.SHORT_TEXT,c.TXT_TASKCD "
                            + "FROM SAMOBJECTATTACHMENTS as a "
                            + "LEFT JOIN ORDERS as b ON a.OBJKEY=b.ORDERID "
                            + "LEFT JOIN NOTIFICATIONTASK as c ON a.OBJKEY=c.OBJECT_NO "
                            + "WHERE (a.OBJKEY = '$1' OR a.OBJKEY = '$2' OR a.OBJKEY = '$3' OR a.OBJKEY = '$4') AND a.ACTION_FLAG!='A' AND a.ACTION_FLAG!='B' ",
                        params: [{
                            parameter: "$1",
                            value: "objKey"
                        }, {
                            parameter: "$2",
                            value: "notificationKey"
                        }, {
                            parameter: "$3",
                            value: "funcLocKey"
                        }, {
                            parameter: "$4",
                            value: "equipmentKey"
                        }],
                        columns: "SAMObjectAttachments"
                    },
                    preSync: {
                        query: "SELECT $COLUMNS FROM SAMOBJECTATTACHMENTS WHERE ACTION_FLAG='N'",
                        params: [],
                        columns: "SAMObjectAttachments"
                    }
                },

                DMSAttachments: {
                    get: {
                        query: "SELECT a1.MOBILE_ID,a1.TABNAME,a1.OBJKEY,a1.NEW_MOBILE_ID,a1.FILENAME,a1.FILEEXT,a1.DOC_SIZE,a1.ACTION_FLAG,b1.EQUIPMENT_DESC,b1.FUNCLOC_DESC,b1.SHORT_TEXT,c1.TXT_TASKCD "
                            + "FROM DMSATTACHMENTS as a1 "
                            + "LEFT JOIN ORDERS as b1 ON a1.OBJKEY=b1.ORDERID "
                            + "LEFT JOIN NOTIFICATIONTASK as c1 ON a1.OBJKEY=c1.OBJECT_NO "
                            + "WHERE (a1.OBJKEY = '$1' OR a1.OBJKEY = '$2' OR a1.OBJKEY = '$3' OR a1.OBJKEY = '$4') AND a1.ACTION_FLAG!='A' AND a1.ACTION_FLAG!='B'",
                        params: [{
                            parameter: "$1",
                            value: "objKey"
                        }, {
                            parameter: "$2",
                            value: "notificationKey"
                        }, {
                            parameter: "$3",
                            value: "funcLocKey"
                        }, {
                            parameter: "$4",
                            value: "equipmentKey"
                        }],
                        columns: "SAMObjectAttachments"
                    },
                    preSync: {
                        query: "SELECT $COLUMNS FROM SAMOBJECTATTACHMENTS WHERE ACTION_FLAG='N'",
                        params: [],
                        columns: "SAMObjectAttachments"
                    }
                },

                ArchiveLinkAttachments: {
                    get: {
                        query: "SELECT a2.MOBILE_ID,a2.TABNAME,a2.OBJECT_ID,a2.NEW_MOBILE_ID,a2.FILENAME,a2.FILEEXT,a2.DOC_SIZE,a2.ACTION_FLAG,b2.EQUIPMENT_DESC,b2.FUNCLOC_DESC,b2.SHORT_TEXT,c2.TXT_TASKCD " +
                            "FROM ARCHIVELINK as a2 " +
                            "LEFT JOIN ORDERS as b2 ON a2.OBJECT_ID=b2.ORDERID " +
                            "LEFT JOIN NOTIFICATIONTASK as c2 ON a2.OBJECT_ID=c2.OBJECT_NO " +
                            "WHERE (a2.OBJECT_ID = '$1' OR a2.OBJECT_ID = '$2' OR a2.OBJECT_ID = '$3' OR a2.OBJECT_ID = '$4') AND a2.ACTION_FLAG!='A' AND a2.ACTION_FLAG!='B' ",
                        params: [{
                            parameter: "$1",
                            value: "objKey"
                        }, {
                            parameter: "$2",
                            value: "notificationKey"
                        }, {
                            parameter: "$3",
                            value: "funcLocKey"
                        }, {
                            parameter: "$4",
                            value: "equipmentKey"
                        }],
                        columns: "SAMObjectAttachments"
                    },
                    preSync: {
                        query: "SELECT $COLUMNS FROM SAMOBJECTATTACHMENTS WHERE ACTION_FLAG='N'",
                        params: [],
                        columns: "SAMObjectAttachments"
                    }
                },


                ChecklistAttachmentsMap: {
                    get: {
                        query: "SELECT cl.MOBILE_ID, count(att.MOBILE_ID) as ATTACHMENT_COUNT FROM Checklists as cl " +
                            "LEFT JOIN SAMOBJECTATTACHMENTS as att on att.TABNAME = 'NotificationTask' AND att.OBJKEY = (cl.NOTIF_NO + cl.TASK_NO) " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' GROUP BY cl.MOBILE_ID",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }]
                    },
                },
                NotificationLongtext: {
                    get: {
                        query: "SELECT * FROM NOTIFICATIONLONGTEXT WHERE NOTIF_NO = '$1'  ORDER by OBJKEY, LINE_NUMBER",
                        params: [{
                            parameter: "$1",
                            value: "notifNo"
                        }]
                    }
                },

                ScenarioInventoryList: {
                    searchMine: {
                        query: "SELECT TOP $1 START AT $2  $COLUMNS FROM InventoryList AS i LEFT JOIN StorageLocation AS sl on sl.STGE_LOC = i.STGE_LOC AND sl.PLANT = i.PLANT " +
                            "LEFT JOIN MaterialMl as ml on ml.MATERIAL = i.MATERIAL and ml.spras = '$9' " +
                            "WHERE (i.MATERIAL LIKE '%$3%' "
                            + "OR UPPER(ml.MATL_DESC) LIKE UPPER('%$4%') OR UPPER(sl.TEXT) LIKE UPPER('%$5%') OR i.ENTRY_QNT LIKE '%$6%' OR i.ENTRY_UOM LIKE '%$7%' OR i.BATCH LIKE '%$8%') AND i.STGE_LOC LIKE '%$11' AND cast(i.ENTRY_QNT as float) > 0 ORDER BY $10",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "searchString"
                        }, {
                            parameter: "$8",
                            value: "searchString"
                        }, {
                            parameter: "$9",
                            value: "spras"
                        }, {
                            parameter: "$10",
                            value: "orderByString"
                        }, {
                            parameter: "$11",
                            value: "storageLocation"
                        }],
                        columns: "InventoryList"
                    }
                },

                DewaMaterials: {
                    searchMine: {
                        query: "SELECT TOP $1 START AT $2  $COLUMNS FROM ZDEWAMATERIALS AS m " +
                            "JOIN MATERIALML as ml on ml.MATERIAL = m.MATNR AND ml.SPRAS = '$4' " +
                            "WHERE m.ILART = '$3' AND (m.MATNR LIKE '%$5%')", // OR UPPER(i.MATL_DESC) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "ilart"
                        }, {
                            parameter: "$4",
                            value: "spras"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }],
                        columns: "DewaMaterialsJoinsInventoryList"
                    }
                },

                InventoryMaterial: {
                    get: {
                        query: "SELECT * FROM INVENTORYLIST",
                        params: [],
                        columns: ""
                    },
                    valueHelp: {
                        query: "SELECT * FROM INVENTORYLIST",
                        params: [],
                        columns: ""
                    }
                },
                ZMALFUNC_MATERIAL: {
                    get: {
                        query: "select replace(replace(ASSET_STRUCTURE_NUMBER, '+', '_'),'CREATE PROCEDURE','ALTER PROCEDURE') as funcy, ASSET_STRUCTURE_NUMBER, ZMALFUNC_MATERIAL.MATL_DESC, MEINS, ORDERID, FUNCT_LOC, ILART, ZMALFUNC_MATERIAL.MATERIAL, patindex('%' + funcy, FUNCT_LOC) as padd from ZMALFUNC_MATERIAL right join Orders on PMActtype = ilart where ORDERID = '$1' order by ZMALFUNC_MATERIAL.MATERIAL ASC",
                        params: [{
                            parameter: "$1",
                            value: "orderid"
                        }],
                        columns: ""
                    },
                    valueHelp: {
                        query: "SELECT * FROM ZMALFUNC_MATERIAL",
                        params: [],
                        columns: ""
                    },
                    filtered: {
                        query: "select replace(replace(ASSET_STRUCTURE_NUMBER, '+', '_'),'CREATE PROCEDURE','ALTER PROCEDURE') as funcy, ASSET_STRUCTURE_NUMBER, ZMALFUNC_MATERIAL.MATL_DESC, MEINS, ORDERID, FUNCT_LOC, ILART, ZMALFUNC_MATERIAL.MATERIAL, patindex('%' + funcy, FUNCT_LOC) as padd from ZMALFUNC_MATERIAL right join Orders on PMActtype = ilart where ORDERID = '$1' and (UCASE(ZMALFUNC_MATERIAL.MATL_DESC) LIKE UCASE('%$2%')OR UCASE(ZMALFUNC_MATERIAL.MATERIAL) LIKE UCASE('%$3%')) order by ZMALFUNC_MATERIAL.MATERIAL ASC",
                        params: [{
                            parameter: "$1",
                            value: "orderid"
                        }, {
                            parameter: "$2",
                            value: "searchstring"
                        }, {
                            parameter: "$3",
                            value: "searchstring"
                        }],
                        columns: ""
                    },
                    withoutIHLA: {
                        query: "select * from ZMALFUNC_MATERIAL where ilart = '' and (UCASE(ZMALFUNC_MATERIAL.MATL_DESC) LIKE UCASE('%$1%')OR UCASE(ZMALFUNC_MATERIAL.MATERIAL) LIKE UCASE('%$2%')) order by ZMALFUNC_MATERIAL.MATERIAL ASC",
                        params: [{
                            parameter: "$1",
                            value: "searchstring"
                        }, {
                            parameter: "$2",
                            value: "searchstring"
                        }],
                        columns: ""
                    },
                    getAll: {
                        query: "select * from ZMALFUNC_MATERIAL",
                        params: [],
                        columns: ""
                    },
                    searchAll: {
                        query: "select * from ZMALFUNC_MATERIAL where UCASE(ZMALFUNC_MATERIAL.MATL_DESC) LIKE UCASE('%$1%')OR UCASE(ZMALFUNC_MATERIAL.MATERIAL) LIKE UCASE('%$2%') order by ZMALFUNC_MATERIAL.MATERIAL ASC",
                        params: [{
                            parameter: "$1",
                            value: "searchstring"
                        }, {
                            parameter: "$2",
                            value: "searchstring"
                        }],
                        columns: ""
                    },
                    count: {
                        query: "select count(*) as count from ZMALFUNC_MATERIAL where ILART = ''",
                        params: [],
                        columns: ""
                    }
                },
                PlantMaterial: {
                    get: {
                        query: "SELECT PLANT FROM INVENTORYLIST",
                        params: [],
                        columns: ""
                    },
                    valueHelp: {
                        query: "SELECT PLANT FROM INVENTORYLIST",
                        params: [],
                        columns: ""
                    }
                },

                UserStatusMl: {
                    get: {
                        query: "SELECT * FROM UserStatusMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }],
                        columns: ""
                    }
                },

                StorageLocation: {
                    get: {
                        query: "SELECT * FROM StorageLocation",
                        params: []
                    }
                },

                MaterialStorageLocation: {
                    get: {
                        query: "SELECT * FROM StorageLocation",
                        params: []
                    },
                    valueHelp: {
                        query: "SELECT * FROM StorageLocation WHERE PLANT='$1'",
                        params: [{
                            parameter: "$1",
                            value: "plant"
                        }, {
                            parameter: "$2",
                            value: "spras"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }]
                    }
                },

                NotificationType: {
                    get: {
                        query: "SELECT * FROM NotificationType WHERE SPRAS='$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                WorkCenterMl: {
                    get: {
                        query: "SELECT * FROM WorkCenterMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                PriorityTypes: {
                    get: {
                        query: "SELECT * FROM PriorityTypes WHERE SPRAS = '$1' ORDER BY PRIORITY ASC",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                OrderTypes: {
                    get: {
                        query: "SELECT * FROM OrderTypes WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                ActivityTypeMl: {
                    get: {
                        query: "SELECT * FROM ActivityTypeMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                CodeGroupMl: {
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 cg.mobile_id, ml.SPRAS, cg.CODE_CAT_GROUP, cg.CODEGROUP, cg.CATALOG_PROFILE, ml.CODEGROUP_TEXT FROM CODEGROUP as cg " +
                            "JOIN CODEGROUPML as ml on ml.CODE_CAT_GROUP = cg.CODE_CAT_GROUP and ml.CATALOG_PROFILE = cg.CATALOG_PROFILE "
                            + "WHERE cg.CATALOG_TYPE = '$7' AND cg.CATALOG_PROFILE = '$4' AND ml.SPRAS = '$3' AND (UPPER(ml.CODEGROUP_TEXT) LIKE UPPER('%$5%') OR UPPER(cg.CODE_CAT_GROUP) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "catalogProfile"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    }
                },

                CodeMl: {
                    get: {
                        query: "SELECT * FROM CodeMl WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    },
                    valueHelp: {
                        query: "SELECT TOP $1 START AT $2 * FROM CodeMl "
                            + "WHERE SPRAS = '$3' AND  CODE_GROUP = '$4' AND CATALOG_TYPE = '$7' AND (UPPER(CODE_TEXT) LIKE UPPER('%$5%') OR UPPER(CODE) LIKE UPPER('%$6%'))",
                        params: [{
                            parameter: "$1",
                            value: "noOfRows"
                        }, {
                            parameter: "$2",
                            value: "startAt"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }, {
                            parameter: "$4",
                            value: "codeGroup"
                        }, {
                            parameter: "$5",
                            value: "searchString"
                        }, {
                            parameter: "$6",
                            value: "searchString"
                        }, {
                            parameter: "$7",
                            value: "catalogType"
                        }]
                    },
                    search: {
                        query: "SELECT * FROM CodeMl WHERE SPRAS = '$1' AND CATALOG_TYPE = '$2' AND CODE_GROUP IN $3 ",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }, {
                            parameter: "$2",
                            value: "catalogType"
                        }, {
                            parameter: "$3",
                            value: "codeGroups"
                        }]
                    }
                },

                CodeMLMassinsp: {
                    get: {
                        query: "SELECT * FROM CodeMl WHERE CODE = '$1' AND CODE_GROUP = '$2' AND SPRAS = '$3' ",
                        params: [{
                            parameter: "$1",
                            value: "code"
                        }, {
                            parameter: "$2",
                            value: "codgr"
                        }, {
                            parameter: "$3",
                            value: "spras"
                        }]
                    }
                },

                CodeMLCodeGroup: {
                    get: {
                        query: "SELECT * FROM CodeMl WHERE CODE_GROUP = '$1' AND SPRAS = '$2' ",
                        params: [{
                            parameter: "$1",
                            value: "codgr"
                        }, {
                            parameter: "$2",
                            value: "spras"
                        }]
                    }
                },

                ZDewaUserStatus: {
                    get: {
                        query: "SELECT zu.ID, zu.ACT_TYPE, zu.ASYNC, zu.CHILD, zu.ICON, zu.STATUS, zu.STATUS_PROFILE, zu.STOPWATCH, zu.AUTOSYNC, ml.USER_STATUS_DESC FROM ZDEWAUSERSTATUS as zu LEFT JOIN USERSTATUSML as ml on ml.STATUS_PROFILE  = zu.STATUS_PROFILE AND ml.USER_STATUS = zu.STATUS AND ml.SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },

                ZDEWACHKFORMS: {
                    get: {
                        query: "SELECT * FROM ZDEWACHKFORMS WHERE ACTTYPE = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "activityType"
                        }]
                    }
                },

                Scenario: {
                    get: {
                        query: "SELECT sc.NAME FROM SAMUSERS as su JOIN SAMSCENARIOS as sc on sc.id = su.SCENARIO_VERSION_ID  WHERE su.EMPLOYEE_NUMBER = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }]
                    }
                },

                MeasurementUnit: {
                    get: {
                        query: "SELECT * FROM MEASUREMENTUNIT WHERE SPRAS = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        }]
                    }
                },
                operUserStatus: {
                    mine: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERS as b ON b.ORDERID=o.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as os ON os.OBJNR= o.OBJECT_NO "
                            + "WHERE ( o.PERS_NO = '$1' OR (o.TEAM_GUID IN ($7) AND o.TEAM_GUID IS NOT NULL AND o.TEAM_GUID <> '') OR o.TEAM_PERSNO = '$8') ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "startCons"
                            },
                            {
                                parameter: "$3",
                                value: "startTime"
                            },
                            {
                                parameter: "$4",
                                value: "startDat"
                            },
                            {
                                parameter: "$5",
                                value: "language"
                            },
                            {
                                parameter: "$6",
                                value: "doneStatus"
                            },
                            {
                                parameter: "$7",
                                value: "teamGuids"
                            }, {
                                parameter: "$8",
                                value: "persNo"
                            }],
                        columns: "OrderOperationsJoinsUserStatus"
                    },
                    done: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERS as b ON b.ORDERID=o.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as os ON os.OBJNR= o.OBJECT_NO "
                            + "WHERE ( o.PERS_NO = '$1' OR (o.TEAM_GUID IN ($7) AND o.TEAM_GUID IS NOT NULL AND o.TEAM_GUID <> '') OR o.TEAM_PERSNO = '$8') ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "startCons"
                            },
                            {
                                parameter: "$3",
                                value: "startTime"
                            },
                            {
                                parameter: "$4",
                                value: "startDat"
                            },
                            {
                                parameter: "$5",
                                value: "language"
                            },
                            {
                                parameter: "$6",
                                value: "doneStatus"
                            },
                            {
                                parameter: "$7",
                                value: "teamGuids"
                            }, {
                                parameter: "$8",
                                value: "persNo"
                            }],
                        columns: "OrderOperationsJoinsUserStatus"
                    },
                    calendar: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERS as b ON b.ORDERID=o.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as os ON os.OBJNR= o.OBJECT_NO "
                            + "WHERE ( o.PERS_NO = '$1' OR (o.TEAM_GUID IN ($7) AND o.TEAM_GUID IS NOT NULL AND o.TEAM_GUID <> '') OR o.TEAM_PERSNO = '$8') "
                            + "AND os.STATUS = '$2' AND (os.ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "zfixStatus"
                        }, {
                            parameter: "$7",
                            value: "teamGuids"
                        }, {
                            parameter: "$8",
                            value: "persNo"
                        }],
                        columns: "OperationsJOINCalendarUserStatus"
                    }
//                    unassigned: {
//                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
//                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
//                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
//                            + "WHERE PERS_NO != '$1'",
//                        params: [{
//                            parameter: "$1",
//                            value: "persNo"
//                        }],
//                        columns: "OperationsJOIN"
//                    },
//
//                    all: {
//                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
//                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID " +
//                            "LEFT JOIN FUNCTIONLOC as f on f.TPLNR = b.FUNCT_LOC "
//                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR = a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='')",
//                        params: [{}],
//                        columns: "OperationsJOIN"
//                    },
//
//                    calendar: {
//                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
//                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
//                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
//                            + "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE " +
//                            "LEFT JOIN FUNCTIONLOC as f on f.TPLNR = b.FUNCT_LOC "
//                            + "WHERE a.WORK_CNTR = '$1' AND o.spras = '$5'", // AND START_CONS LT '$2' ",
//                        params: [{
//                            parameter: "$1",
//                            value: "workCenter"
//                        },
//                            {
//                                parameter: "$5",
//                                value: "language"
//                            }],
//                        columns: "OperationsJOIN"
//                    },
                },
                OperationList: {
                    mine: {
                        query: "SELECT  $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "LEFT JOIN FUNCTIONLOC as f ON f.TPLNR = b.FUNCT_LOC "
                            + "LEFT JOIN ZPW_OP_TIME as ot ON a.OBJECT_NO = ot.OBJNR "
                            + "WHERE ( a.PERS_NO = '$1' OR (a.TEAM_GUID IN ($7) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$8' ) " +
                            "AND o.spras = '$5' " +
                            "AND ( a.START_CONS <= '$2' or t.TEAM_BEG_DATUM <= '$4' ) " +
                            "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$11' ) " +
                            "AND NOT EXISTS ( " +
                            "SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = a.OBJECT_NO " +
                            "AND (ORDERUSERSTATUS.STATUS = '$6' OR ORDERUSERSTATUS.STATUS = '$9' OR ORDERUSERSTATUS.STATUS = 'E0021')  " +
                            "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ) ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "startCons"
                            },
                            {
                                parameter: "$3",
                                value: "startTime"
                            },
                            {
                                parameter: "$4",
                                value: "startDat"
                            },
                            {
                                parameter: "$5",
                                value: "language"
                            },
                            {
                                parameter: "$6",
                                value: "doneStatus"
                            },
                            {
                                parameter: "$7",
                                value: "teamGuids"
                            }, {
                                parameter: "$8",
                                value: "persNo"
                            }, {
                                parameter: "$9",
                                value: "prioStatus"
                            }, {
                                parameter: "$10",
                                value: "today"
                            }, {
                                parameter: "$11",
                                value: "yesterday"
                            }],
                        columns: "OperationsJOIN"
                    },
                    done: {
                        query: "SELECT DISTINCT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
                            + "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "LEFT JOIN FUNCTIONLOC as f ON f.TPLNR = b.FUNCT_LOC "
                            + "LEFT JOIN ZPW_OP_TIME as ot ON a.OBJECT_NO = ot.OBJNR "
                            + "WHERE ( a.PERS_NO = '$1' OR (a.TEAM_GUID IN ($7) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$8' ) AND o.spras = '$5' "
                            + "AND ( a.START_CONS <= '$10' or t.TEAM_BEG_DATUM <= '$11' ) "
                            + "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$12' ) "
                            + "and (c.STATUS = '$6' OR c.STATUS = '$9' OR c.STATUS = 'E0021') AND ( c.ACTION_FLAG = 'N' OR c.ACTION_FLAG = 'C' OR c.ACTION_FLAG = 'U' OR c.ACTION_FLAG = 'I' OR c.ACTION_FLAG = 'L' OR c.ACTION_FLAG = 'K') ",

                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "startCons"
                            },
                            {
                                parameter: "$3",
                                value: "startTime"
                            },
                            {
                                parameter: "$4",
                                value: "startDat"
                            },
                            {
                                parameter: "$5",
                                value: "language"
                            },
                            {
                                parameter: "$6",
                                value: "doneStatus"
                            },
                            {
                                parameter: "$7",
                                value: "teamGuids"
                            }, {
                                parameter: "$8",
                                value: "persNo"
                            }, {
                                parameter: "$9",
                                value: "prioStatus"
                            }, {
                                parameter: "$10",
                                value: "startCons"
                            }, {
                                parameter: "$11",
                                value: "startCons"
                            }, {
                                parameter: "$12",
                                value: "yesterday"
                            }],
                        columns: "OperationsJOIN"
                    },
                    unassigned: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
                            + "WHERE PERS_NO != '$1'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }],
                        columns: "OperationsJOIN"
                    },

                    all: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID " +
                            "LEFT JOIN FUNCTIONLOC as f on f.TPLNR = b.FUNCT_LOC "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR = a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='')",
                        params: [{}],
                        columns: "OperationsJOIN"
                    },

                    calendar: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE "
                            + "LEFT JOIN FUNCTIONLOC as f on f.TPLNR = b.FUNCT_LOC "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "LEFT JOIN ZPW_OP_TIME as ot ON a.OBJECT_NO = ot.OBJNR "
                            + "WHERE ( a.PERS_NO = '$1' OR (a.TEAM_GUID IN ($7) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$8' ) "
                            + "AND o.spras = '$5' "
                            + "AND ( a.START_CONS <= '$10' or t.TEAM_BEG_DATUM <= '$11' ) "
                            + "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$12' ) "
                            + "AND NOT EXISTS ( SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = a.OBJECT_NO "
                            + "AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ( ORDERUSERSTATUS.STATUS = '$6' OR ORDERUSERSTATUS.STATUS = '$9') ) "
                            + "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT=''))",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$5",
                                value: "language"
                            },
                            {
                                parameter: "$6",
                                value: "doneStatus"
                            },
                            {
                                parameter: "$7",
                                value: "teamGuids"
                            }, {
                                parameter: "$8",
                                value: "persNo"
                            }, {
                                parameter: "$9",
                                value: "prioStatus"
                            }, {
                                parameter: "$10",
                                value: "inTwoWeeks"
                            }, {
                                parameter: "$11",
                                value: "inTwoWeeks"
                            }, {
                                parameter: "$12",
                                value: "yesterday"
                            }],
                        columns: "OperationsJOINCalendar"
                    },
                },
                AllOperationCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM ORDEROPERATIONS as a JOIN ORDERS as b ON b.ORDERID=a.ORDERID ",
                        params: [],
                        columns: ""

                    }
                },
                UnassignedOperationCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM ORDEROPERATIONS as a JOIN ORDERS as b ON b.ORDERID=a.ORDERID WHERE a.PERS_NO != '$1'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }],
                        columns: ""
                    }
                },
                MineOperationCount: {
                    get: {
                        query: "SELECT COUNT(a.MOBILE_ID) as COUNT FROM ORDEROPERATIONS as a "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "WHERE ( a.PERS_NO = '$1' OR (a.TEAM_GUID IN ($2) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$4' ) " +
                            "AND ( a.START_CONS <= '$6' or t.TEAM_BEG_DATUM <= '$7' ) " +
                            "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$12' ) " +
                            "AND NOT EXISTS ( " +
                            "SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = a.OBJECT_NO " +
                            "AND (ORDERUSERSTATUS.STATUS = '$3' OR ORDERUSERSTATUS.STATUS = '$9' OR ORDERUSERSTATUS.STATUS = 'E0021')  " +
                            "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ) ",

                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "teamGuids"
                            },
                            {
                                parameter: "$3",
                                value: "doneStatus"
                            }, {
                                parameter: "$4",
                                value: "persNo"
                            }, {
                                parameter: "$5",
                                value: "prioStatus"
                            },
                            {
                                parameter: "$6",
                                value: "startCons"
                            }, {
                                parameter: "$7",
                                value: "startCons"
                            }, {
                                parameter: "$9",
                                value: "prioStatus"
                            },{
                                parameter: "$10",
                                value: "today"
                            }, {
                                parameter: "$12",
                                value: "yesterday"
                            }],
                        columns: ""
                    }
                },

                DoneOperationCount: {
                    get: {
                        query: "SELECT COUNT(DISTINCT a.MOBILE_ID) as COUNT FROM ORDEROPERATIONS as a "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
                            + "WHERE ( a.PERS_NO = '$1' or (a.TEAM_GUID IN ($2) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$4' ) "
                            + "AND ( a.START_CONS <= '$6' or t.TEAM_BEG_DATUM <= '$7' ) "
                            + "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$12' ) "
                            + "AND (c.STATUS = '$3' OR c.STATUS = '$5' OR c.STATUS = 'E0021')  AND ( c.ACTION_FLAG = 'N' OR c.ACTION_FLAG = 'C' OR c.ACTION_FLAG = 'U' OR c.ACTION_FLAG = 'U' OR c.ACTION_FLAG = 'I' OR c.ACTION_FLAG = 'L' OR c.ACTION_FLAG = 'K') ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        },
                            {
                                parameter: "$2",
                                value: "teamGuids"
                            },
                            {
                                parameter: "$3",
                                value: "doneStatus"
                            }, {
                                parameter: "$4",
                                value: "persNo"
                            },{
                                parameter: "$5",
                                value: "prioStatus"
                            },
                            {
                                parameter: "$6",
                                value: "startCons"
                            }, {
                                parameter: "$7",
                                value: "startCons"
                            }, {
                                parameter: "$10",
                                value: "today"
                            }, {
                                parameter: "$12",
                                value: "yesterday"
                            }],
                        columns: ""
                    }
                },

                MapMarkerList: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
                            + "LEFT JOIN EQUIPMENT as d ON b.EQUIPMENT=d.EQUNR "
                            + "WHERE a.PERS_NO = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }],
                        columns: "EquipmentMarker"
                    }
                },

                MapMarkerListAll: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a " +
                            "JOIN ORDERS as b ON b.ORDERID=a.ORDERID " +
                            "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE  " +
                            "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID " +
                            "JOIN FUNCTIONLOC as f ON f.TPLNR = b.FUNCT_LOC " +
                            "WHERE ( a.PERS_NO = '$1' OR (a.TEAM_GUID IN ($2) AND a.TEAM_GUID IS NOT NULL AND a.TEAM_GUID <> '') OR a.TEAM_PERSNO = '$3' )  " +
                            "AND o.spras = '$4' " +
                            "AND b.FUNCT_LOC NOT LIKE 'D-N-__' " +
                            "AND ( a.START_CONS <= '$5' or t.TEAM_BEG_DATUM <= '$6' ) " +
                            "AND ( t.TEAM_END_DATUM IS NULL OR t.TEAM_END_DATUM >= '$7' ) " +
                            "AND NOT EXISTS ( " +
                            "SELECT ORDERUSERSTATUS.MOBILE_ID FROM ORDERUSERSTATUS WHERE ORDERUSERSTATUS.OBJNR = a.OBJECT_NO " +
                            "AND ( ORDERUSERSTATUS.STATUS = '$8' OR ( ORDERUSERSTATUS.STATUS = '$9' AND ( ORDERUSERSTATUS.ACTION_FLAG <> 'N' AND ORDERUSERSTATUS.ACTION_FLAG <> 'C' ) ) ) " +
                            "AND (ORDERUSERSTATUS.INACT IS NULL OR ORDERUSERSTATUS.INACT='') ) ",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "teamGuids"
                        }, {
                            parameter: "$3",
                            value: "persNo"
                        }, {
                            parameter: "$4",
                            value: "language"
                        }, {
                            parameter: "$5",
                            value: "startCons"
                        }, {
                            parameter: "$6",
                            value: "startCons"
                        }, {
                            parameter: "$7",
                            value: "yesterday"
                        }, {
                            parameter: "$8",
                            value: "doneStatus"
                        }, {
                            parameter: "$9",
                            value: "prioStatus"
                        },],
                        columns: "MapMarkerAll"
                    }
                },

                MapSingleMarker: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as c ON c.OBJNR= a.OBJECT_NO AND (c.INACT IS NULL OR c.INACT='') "
                            + "LEFT JOIN EQUIPMENT as d ON b.EQUIPMENT=d.EQUNR "
                            + "WHERE a.PERS_NO = '$1' AND b.EQUIPMENT = '$2' AND a.ORDERID = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }, {
                            parameter: "$2",
                            value: "equipmentNr"
                        }, {
                            parameter: "$3",
                            value: "orderId"
                        }],
                        columns: "EquipmentMarker"
                    }
                },

                operationsOverview: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS WHERE ORDERID='$1'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }],
                        columns: "operationOverview"
                    }
                },

                EquipmentOrderHist: {
                    get: {
                        query: "SELECT $COLUMNS FROM SAM_ORDER_HIST as oh " +
                            "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                            "JOIN ORDEROPERATIONS as op on op.ORDERID = o.ORDERID " +
                            "WHERE o.EQUIPMENT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "equipmentNumber"
                        }],
                        columns: "EquipmentOrderHist"
                    }
                },

                EquipmentNotifHist: {
                    get: {
                        query: "SELECT $COLUMNS FROM SAM_NOTIF_HIST as oh " +
                            "JOIN NOTIFICATIONHEADER as nh on nh.NOTIF_NO = oh.NOTIF_NO " +
                            "WHERE nh.EQUIPMENT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "equipmentNumber"
                        }],
                        columns: "EquipmentNotifHist"
                    }
                },

                FunctionLocOrderHist: {
                    get: {
                        query: "SELECT * FROM SAM_ORDER_HIST as oh " +
                            "JOIN ORDERS as o on o.ORDERID = oh.ORDERID " +
                            "JOIN ORDEROPERATIONS as op on op.ORDERID = o.ORDERID " +
                            "WHERE o.FUNCT_LOC = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "funcLocId"
                        }]
                    }
                },

                FunctionLocNotifHist: {
                    get: {
                        query: "SELECT * FROM SAM_NOTIF_HIST as oh " +
                            "JOIN NOTIFICATIONHEADER as nh on nh.NOTIF_NO = oh.NOTIF_NO " +
                            "WHERE nh.FUNCT_LOC = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "funcLocId"
                        }]
                    }
                },

                Inspections: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDERS as o " +
                            "LEFT JOIN ORDEROPERATIONS as oo ON oo.ORDERID=o.ORDERID " +
                            "LEFT JOIN ORDEROBJECTLIST as ool ON ool.ORDERID=oo.ORDERID AND ool.ACTIVITY=oo.ACTIVITY " +
                            "LEFT JOIN EQUIPMENTMEASPOINT as emp ON emp.EQUNR=ool.EQUIPMENT " +
                            "LEFT JOIN FUNCTIONLOCMEASPOINT as fmp ON fmp.OBJNR=o.OBJECT_NO " +
                            "LEFT JOIN MEASUREMENTUNIT as mu ON mu.MSEHI=emp.MRNGU AND mu.MSEHI=fmp.MRNGU " +
                            "WHERE o.ORDERID='$1'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }],
                        columns: "Inspections"
                    }
                },


                MeasurementDocuments: {
                    get: {
                        query: "SELECT * FROM MEASUREMENTDOCUMENT WHERE POINT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "point"
                        }],
                        columns: "MeasurementDocuments"
                    },
                },

                MeasurementDocumentsLastReading: {
                    get: {
                        query: "SELECT $COLUMNS FROM CHECKLISTS as cl " +
                            "LEFT JOIN MEASUREMENTDOCUMENT as md on md.POINT = cl.MEAS_POINT AND md.POINT <> '' " +
                            "WHERE cl.MEAS_POINT <> '' AND cl.ORDERID = $2 " +
                            "ORDER BY md.IDATE DESC ",

                        params: [{
                            parameter: "$1",
                            value: "date"
                        }, {
                            parameter: "$2",
                            value: "orderId"
                        }],
                        columns: "MeasurementDocumentsLastReading"
                    },
                },

                teamInfo: {
                    get: {
                        query: "SELECT $COLUMNS FROM ZMRSTEAM_HEAD WHERE TEAM_GUID = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "teamGuid"
                        }],
                        columns: "TeamInfo"
                    },
                },

                teamMembers: {
                    get: {
                        query: "SELECT $COLUMNS FROM ZMRSTEAM_RES WHERE TEAM_GUID = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "teamGuid"
                        }],
                        columns: "TeamMembers"
                    },
                },

                FunclocHierarchy: {
                    get: {
                        query: "SELECT $COLUMNS FROM FUNCTIONLOC WHERE UPPER(TPLNR) LIKE UPPER('$1%') " +
                            "ORDER BY TPLMA ASC ",
                        params: [{
                            parameter: "$1",
                            value: "funcLoc"
                        }],
                        columns: "FunclocHierarchy"
                    },
                },
                EquipmentHierarchy: {
                    get: {
                        query: "SELECT $COLUMNS FROM EQUIPMENT WHERE UPPER(TPLNR) LIKE UPPER('$1%')" +
                            "ORDER BY HEQUI ASC ",
                        params: [{
                            parameter: "$1",
                            value: "funcLoc"
                        }],
                        columns: "EquipmentHierarchy"
                    },
                },

                ZPMC_REJ_REASONS: {
                    get: {
                        query: "SELECT STATUS as status, USR_STAT_DESCR as statusDescription FROM ZPMC_REJ_REASONS",
                        params: [{}]
                    }
                },

                TimeConfirmation: {
                    getForUser: {
                        query: "SELECT * FROM ZPMC_TIMECONF WHERE PERS_NO = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "persNo"
                        }]
                    }
                },

                ZOPTime: {
                    get: {
                        query: "SELECT * FROM ZPW_OP_TIME WHERE OBJNR = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "objnr"
                        }]
                    }
                },

                OperationCompletionChecklistCount: {
                    get: {
                        query: "SELECT COUNT(cl.MOBILE_ID) AS COUNT " +
                            "FROM CHECKLISTS AS cl " +
                            "JOIN NotificationTask AS nt ON cl.NOTIF_NO = nt.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND cl.CHECK_TYPE = '0' AND cl.TPLNR = '$4' AND nt.TASK_CODEGRP <> '$5' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "funcloc"
                        }, {
                            parameter: "$5",
                            value: "codeGroup"
                        }]
                    }
                },
                OperationCompletionChecklist: {
                    get: {
                        query: "SELECT COUNT(cl.MOBILE_ID) AS COUNT " +
                            "FROM CHECKLISTS AS cl " +
                            "JOIN NotificationTask AS nt ON cl.NOTIF_NO = nt.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND cl.CHECK_TYPE = '0' AND cl.TPLNR = '$4' " +
                            "AND ( ( cl.ACTION_FLAG = 'C' AND cl.ZRESULT_CODE <> '' ) OR ( cl.ZRESULT_CODE <> '' AND CL.ACTION_FLAG = 'U' )) " +
                            "AND nt.TASK_CODEGRP <> '$6' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "funcloc"
                        }, {
                            parameter: "$5",
                            value: "persNo"
                        }, {
                            parameter: "$6",
                            value: "codeGroup"
                        }]
                    }
                },
                OperationCompletionMinMax: {
                    get: {
                        query: "SELECT MAX(cl.ZRESULT_READING) AS MAX " +
                            "FROM CHECKLISTS as cl " +
                            "LEFT JOIN FUNCTIONLOCMEASPOINT as fmp on fmp.MEASUREMENT_POINT = cl.MEAS_POINT and fmp.INACT = '' " +
                            "WHERE fmp.MOBILE_ID IS NOT NULL AND cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND cl.CHECK_TYPE = '1' AND cl.TPLNR = '$4' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "funclocUM"
                        }]
                    }
                },

                OperationCompletionAllZeroMinMax: {
                    get: {
                        query: "SELECT cl.USER_FIELD " +
                            "FROM CHECKLISTS as cl " +
                            "LEFT JOIN FUNCTIONLOCMEASPOINT as fmp on fmp.MEASUREMENT_POINT = cl.MEAS_POINT and fmp.INACT = '' " +
                            "WHERE fmp.MOBILE_ID IS NOT NULL AND cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND cl.CHECK_TYPE = '1' AND cl.TPLNR = '$4' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "funclocUM"
                        }]
                    }
                },

                OperationCompletionNoMinMaxRequired: {
                    get: {
                        query: "SELECT COUNT(cl.MOBILE_ID) AS COUNT " +
                            "FROM Checklists as cl " +
                            "LEFT JOIN NOTIFICATIONTASK as nt on nt.NOTIF_NO = cl.NOTIF_NO and nt.TASK_KEY = cl.TASK_NO " +
                            "WHERE cl.ORDERID = '$1' AND cl.NOTIF_NO = '$2' AND cl.SPRAS = '$3' AND nt.TASK_CODEGRP = '$4' AND nt.TASK_CODE = '$5' AND cl.ZRESULT_CODE IS NOT NULL AND cl.ZRESULT_CODE <> '' ",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }, {
                            parameter: "$2",
                            value: "notifNo"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }, {
                            parameter: "$4",
                            value: "codeGroup"
                        }, {
                            parameter: "$5",
                            value: "code"
                        }]
                    }
                },
                StopWatchOperationDetails: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as a "
                            + "JOIN ORDERS as b ON b.ORDERID=a.ORDERID "
                            + "JOIN ORDERTYPES as o ON o.AUART=b.ORDER_TYPE "
                            + "LEFT JOIN ZMRSTEAM_HEAD as t on t.TEAM_GUID = a.TEAM_GUID "
                            + "LEFT JOIN FUNCTIONLOC as f ON f.TPLNR = b.FUNCT_LOC "
                            + "LEFT JOIN ZPW_OP_TIME as ot ON a.OBJECT_NO = ot.OBJNR "
                            + "WHERE o.spras = '$1' and a.ORDERID = '$2' and a.ACTIVITY = '$3'",
                        params: [{
                            parameter: "$1",
                            value: "language"
                        },
                            {
                                parameter: "$2",
                                value: "orderId"
                            },
                            {
                                parameter: "$3",
                                value: "activity"
                            }],
                        columns: "OperationsJOIN"
                    }
                },
                StopWatchoperUserStatus: {
                    get: {
                        query: "SELECT $COLUMNS FROM ORDEROPERATIONS as o "
                            + "JOIN ORDERS as b ON b.ORDERID=o.ORDERID "
                            + "LEFT JOIN ORDERUSERSTATUS as os ON os.OBJNR= o.OBJECT_NO "
                            + "WHERE o.ORDERID = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }],
                        columns: "OrderOperationsJoinsUserStatus"
                    }
                },
                ChecklistHtmlForms: {
                    get: {
                        query: "SELECT * FROM CHECKLISTHTMLFORMS WHERE ACTTYPE = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "activityType"
                        }]
                    }
                },

                ChecklistSavedHtmlForms: {
                    get: {
                        query: "SELECT * FROM SAM_CHECKLIST_FORM_SAVE WHERE ORDERID = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "objKey"
                        }]
                    }
                },
                FuncLocCharacs: {
                    get: {
                        query: "SELECT $COLUMNS, COUNT(fc.MOBILE_ID) as cv_count FROM FUNCTIONLOCCHARACS as fc " +
                            "LEFT JOIN Characteristics AS c ON fc.CHARACT = c.ATNAM " +
                            "LEFT JOIN MeasurementUnit AS u ON u.MSEH3 = c.MSEHI " +
                            "LEFT JOIN Classification as cl ON cl.CLASS=fc.CLASS " +
                            "LEFT JOIN ClassificationChar AS cc ON c.ATINN = cc.IMERK AND cc.CLINT=cl.CLINT " +
                            "LEFT JOIN CharacteristicValues as cv on cv.ATINN = c.ATINN " +
                            "WHERE fc.TPLNR='$1' AND fc.ACTION_FLAG!='D' AND fc.ACTION_FLAG!='M' and fc.ATZHL is not NULL GROUP BY fc.mobile_id, fc.CHARACT, fc.CLASS, fc.CHARACT_DESCR, fc.TPLNR, fc.VALUE_NEUTRAL_FROM, " +
                            "fc.VALUE_NEUTRAL_TO, fc.VALUE_TO, fc.VALUE_FROM, fc.UNIT_FROM, fc.UNIT_TO, " +
                            "fc.VALUE_DATE, fc.TYPE, fc.ACTION_FLAG, cv.ATINN,c.ATFOR, c.ATSON, c.ANZDZ, c.ATEIN, c.ANZST, " +
                            "c.ATSCH, u.MSEHT, cc.POSNR  ORDER BY cc.POSNR",
                        params: [{
                            parameter: "$1",
                            value: "funcLoc"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "FuncLocCharacsJoinsCharacteristics"
                    }
                },

                EquipmentCharacs: {
                    get: {
                        query: "SELECT $COLUMNS, COUNT(ec.MOBILE_ID) as cv_count FROM EquipmentCharacs AS ec LEFT JOIN Characteristics AS c ON ec.CHARACT = c.ATNAM " +
                            "LEFT JOIN MeasurementUnit AS u ON u.MSEH3 = c.MSEHI AND u.SPRAS = '$2' " +
                            "LEFT JOIN Classification as cl ON cl.CLASS=ec.CLASS " +
                            "LEFT JOIN ClassificationChar AS cc ON c.ATINN = cc.IMERK AND cc.CLINT=cl.CLINT " +
                            "LEFT JOIN CharacteristicValues as cv on cv.ATINN = c.ATINN " +
                            "WHERE ec.EQUNR='$1' AND ec.ACTION_FLAG!='D' AND ec.ACTION_FLAG!='M' and ec.ATZHL is not NULL  " +
                            "GROUP BY ec.MOBILE_ID ,ec.CHARACT,ec.CLASS, ec.CHARACT_DESCR,EQUNR, ec.VALUE_NEUTRAL_FROM, ec.VALUE_NEUTRAL_TO, " +
                            "ec.VALUE_TO, ec.VALUE_FROM, ec.UNIT_FROM, ec.UNIT_TO,ec.VALUE_DATE, ec.TYPE, ec.ACTION_FLAG, cv.ATINN, c.ATFOR, c.ATEIN, c.ATSON, c.ANZDZ, " +
                            "c.ANZST, c.ATSCH, u.MSEHT, cc.POSNR ORDER BY cc.POSNR",
                        params: [{
                            parameter: "$1",
                            value: "equipment"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "EquipmentCharacsJoinsCharacteristics"
                    }
                },
                CharacteristicValues: {
                    get: {
                        query: "SELECT $COLUMNS FROM CharacteristicValues AS cv " +
                            "LEFT JOIN CharacteristicValMl as cvml ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cv.ATZHL=cvml.ATZHL AND cvml.SPRAS='$2' " +
                            "WHERE cv.ATINN='$1' ORDER BY cv.ATZHL",
                        params: [{
                            parameter: "$1",
                            value: "atinn"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }],
                        columns: "CharacteristicValuesJoinsMl"
                    },
                    search: {
                        query: "SELECT $COLUMNS FROM CharacteristicValues AS cv LEFT JOIN CharacteristicValMl as cvml  ON cv.ADZHL=cvml.ADZHL AND cv.ATINN=cvml.ATINN AND cv.ATZHL=cvml.ATZHL AND cvml.SPRAS='$6' "
                            + " WHERE ( UPPER(cv.ATWRT) LIKE UPPER('%$1%') OR UPPER(cv.ATFLB) LIKE UPPER('%$2%')  OR UPPER(cv.ATFLV) LIKE UPPER('%$3%') OR UPPER(cvml.ATWTB) LIKE UPPER('%$4%')) AND cv.ATINN='$5'" +
                            " ORDER BY cv.ATZHL",
                        params: [{
                            parameter: "$1",
                            value: "searchString"
                        }, {
                            parameter: "$2",
                            value: "searchString"
                        }, {
                            parameter: "$3",
                            value: "searchString"
                        }, {
                            parameter: "$4",
                            value: "searchString"
                        }, {
                            parameter: "$5",
                            value: "atinn"
                        }, {
                            parameter: "$6",
                            value: "language"
                        }],
                        columns: "CharacteristicValuesJoinsMl"
                    }
                },
                EquipmentMeasurementPoints: {
                    get: {
                        query: "SELECT $COLUMNS FROM EQUIPMENTMEASPOINT as em WHERE em.EQUNR = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "equipment"
                        }],
                        columns: "EquipmentMeasurementPoints"
                    },
                },
                EquipmentMeasurementDocuments: {
                    get: {
                        query: "SELECT POINT, IDATE, ITIME, ERNAM, ERDAT, CODGR, WOOBJ, VLCOD, READG_CHAR FROM MeasurementDocument WHERE POINT = '$1' and ACTION_FLAG != 'N' ORDER BY IDATE DESC",
                        params: [{
                            parameter: "$1",
                            value: "point"
                        }]
                    },
                },

                EquipmentAMMeasurementDocuments: {
                    get: {
                        query: "SELECT * FROM MeasurementDocument WHERE POINT = '$1'  ORDER BY IDATE DESC",
                        params: [{
                            parameter: "$1",
                            value: "point"
                        }]
                    },
                },

                AllEquipmentMeasurementPoints: {
                    get: {
                        query: "SELECT ep.EQUNR, ep.MEASUREMENT_POINT, md.MDOCM, md.VLCOD, md.ERDAT, md.READR, md.READG_CHAR, md.MDTXT, md.ACTION_FLAG, md.IDATE, md.ERNAM, md.ITIME FROM EquipmentMeasPoint as ep "+
                               "LEFT JOIN MeasurementDocument as MD ON md.POINT = ep.MEASUREMENT_POINT AND md.WOOBJ = '$2' "+
                               "WHERE ep.EQUNR = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "equnr"
                        },{
                            parameter: "$2",
                            value: "objectNo"
                        }]
                    },
                },

                MeasPointLongtext: {
                    get: {
                        query: "SELECT * FROM MEASPOINTLONGTEXT WHERE MEASUREMENT_POINT = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "point"
                        }],
                        columns: ""
                    },
                },

                FuncLocMeasurementPoints: {
                    get: {
                        query: "SELECT $COLUMNS FROM FUNCTIONLOCMEASPOINT as fm WHERE fm.TPLNR = '$1'",
                        params: [{
                            parameter: "$1",
                            value: "funcLocId"
                        }],
                        columns: "FuncLocMeasurementPoints"
                    },
                },

                AllFuncLocMeasurementPoints: {
                    get: {
                        query: "SELECT fm.TPLNR, fm.MEASUREMENT_POINT, md.MDOCM, md.VLCOD, md.ERDAT, md.READR, md.READG_CHAR, md.MDTXT, md.ACTION_FLAG, md.IDATE, md.ERNAM, md.ITIME FROM FUNCTIONLOCMEASPOINT as fm " +
                               "LEFT JOIN MeasurementDocument as MD ON md.POINT = fm.MEASUREMENT_POINT AND md.WOOBJ = '$2' "+
                               "WHERE fm.TPLNR = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "tplnr"
                        },{
                            parameter: "$2",
                            value: "objectNo"
                        }]
                    },
                },

                EquipmentDetails: {
                    get: {
                        query: "SELECT $COLUMNS FROM Equipment as eq "+
                        "LEFT JOIN  WorkCenterMl as wml on wml.OBJID = eq.WKCTR and wml.SPRAS = '$3' "+
                        "LEFT JOIN FunctionLoc as fl on fl.BEBER = eq.BEBER and fl.TPLMA like '%SRB-'+wml.ARBPL and fl.SPRAS = '$2' "+
                        "WHERE eq.EQUNR = '$1' ",
                        params: [{
                            parameter: "$1",
                            value: "equipment"
                        }, {
                            parameter: "$2",
                            value: "language"
                        }, {
                            parameter: "$3",
                            value: "language"
                        }],
                        columns: "EquipmentDetails"
                    },
                },
                OrderEquipmentMeasurementDocuments: {
                    get: {
                        query: "SELECT * FROM MeasurementDocument WHERE WOOBJ = '$1' and POINT = '$2'",
                        params: [{
                            parameter: "$1",
                            value: "obId"
                        }, {
                            parameter: "$2",
                            value: "point"
                        }]
                    },
                },

                showManData: {
                    get: {
                        query: "SELECT * FROM ZPMC_SHOWM_BILLING "
                            + "WHERE ORDERNO= '$1'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        }],
                        columns: ""
                    }
                },

                showManDataWithOperation: {
                    get: {
                        query: "SELECT * FROM ZPMC_SHOWM_BILLING "
                            + "WHERE ORDERNO= '$1' AND OPERATION= '$2'",
                        params: [{
                            parameter: "$1",
                            value: "orderId"
                        },{
                            parameter: "$2",
                            value: "operation"
                        }],
                        columns: ""
                    }
                },

                AM_ObjList: {
                    get: {
                        query: "SELECT * FROM  ZPMC_AM_OBJLIST",
                        params: [],
                        columns: ""
                    },
                },

                AM_MPoint: {
                    get: {
                        query: "SELECT * FROM  ZPMC_AM_MPOINT",
                        params: [],
                        columns: ""
                    },
                },

                SRBMeasDoc: {
                    get: {
                        query: "SELECT POINT, MDOCM, MDTXT, ACTION_FLAG as IS_LOCAL, VLCOD, WOOBJ, ERDAT FROM MeasurementDocument "+
                               "WHERE WOOBJ = '$1' ORDER BY ERDAT DESC",
                        params: [{
                            parameter: "$1",
                            value: "woobj"
                        }],
                        columns: ""
                    },
                },

                CustomAttachments: {
                    get: {
                        query: "SELECT * FROM  ZPW_ATTACHMENTS",
                        params: [],
                        columns: ""
                    },
                },

            },
        };
    });
