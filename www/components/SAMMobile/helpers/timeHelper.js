sap.ui.define([
        "sap/ui/core/format/DateFormat"
    ],

    function (DateFormat) {
        "use strict";
        return {
            calcTimeDifference: function (timeConfObj, timeType) {
                var diffMs;
                var startDate = timeConfObj.START_DATE;
                var endDate = timeConfObj.END_DATE;
                var startTime = timeConfObj.START_TIME;
                var endTime = timeConfObj.END_TIME;

                var startTimeArr = startTime.split(":");
                var endTimeArr = endTime.split(":");

                if (endTimeArr[0] === "00" && endTimeArr[1] == "00") {
                    endTimeArr[0] = "23";
                    endTimeArr[1] = "59";
                }

                var startDateObj = new Date(this.formatBackendDateToUIDate(startDate));
                var endDateObj = new Date(this.formatBackendDateToUIDate(endDate));

                startDateObj.setHours(parseInt(startTimeArr[0]));
                startDateObj.setMinutes(parseInt(startTimeArr[1]));

                endDateObj.setHours(parseInt(endTimeArr[0]));
                endDateObj.setMinutes(parseInt(endTimeArr[1]));

                diffMs = endDateObj.getTime() - startDateObj.getTime();

                if (diffMs <= 0) {
                    return 0;
                } else {
                    return this.formatMsIntoHours(diffMs);
                }
            },

            calcTimeDifferenceNew: function (timeConfObj, timeType, bDir) {
                var resultObj = {};
                var startDate = moment(timeConfObj._start_date);
                var endDate = moment(timeConfObj._end_date);
                var timeDiff = endDate - startDate;
                var mTimeType = this.getMomentTimeType(timeType);

                if (mTimeType === null) {
                    timeType = "H";
                    mTimeType = "hours";
                    resultObj["workUnitChanged"] = true;
                } else {
                    resultObj["workUnitChanged"] = false;
                }

                var actWork = endDate.diff(startDate, mTimeType, true);
                var actWorkRounded = +(Math.round(actWork + "e+2") + "e-2");
                var time = moment.utc(moment.duration(timeDiff).asMilliseconds()).format("HH:mm:ss.SSS");

                resultObj["actWork"] = actWorkRounded.toFixed(2);
                resultObj["workUnit"] = timeType;
                resultObj["endTime"] = bDir ? startDate.format("HH:mm:ss.SSS") : endDate.format("HH:mm:ss.SSS");

                return resultObj;
            },

            getEndDateByAddingActWork: function (timeConfObj, timeType) {
                var actWork = timeConfObj.ACT_WORK;
                var startDate = timeConfObj._start_date;
                var startTime = timeConfObj.START_TIME;

                var startDateArr = startDate.split(" ");
                var startTimeArr = startTime.split(":");

                var startDateObj = new Date(this.formatBackendDateToUIDate(startDate));
                startDateObj.setHours(parseInt(startTimeArr[0]));
                startDateObj.setMinutes(parseInt(startTimeArr[1]));

                var endDateObj = new Date(startDateObj.getTime() + (actWork * 1000 * 60 * 60));

                var endDate = endDateObj.toJSON().split("T")[0] + " " + startDateArr[1];
                var endTime = endDateObj.toISOString().substr(11, 5);
                // var endTime = endDateObj.getHours() + ":" +
                // endDateObj.getMinutes();

                return {
                    endDate: endDate,
                    endTime: endTime
                };
            },

            getEndDateByAddingActWorkNew: function (timeConfObj, timeType) {
                var actWork = timeConfObj.ACT_WORK;
                var startDate = timeConfObj._start_date;
                var mTimeType = this.getMomentTimeType(timeType);

                var endDateObj = moment(startDate).add(actWork, mTimeType);
                var endDate = endDateObj.format(endDateObj._f);
                var endTime = endDateObj.format("HH:mm:ss.SSS");

                return {
                    endDate: endDate,
                    endTime: endTime
                }
            },

            getMomentTimeType: function (timeType) {
                switch (timeType) {
                    case "JHR":
                        return "years";
                        break;
                    case "WCH":
                        return "weeks";
                        break;
                    case "S":
                        return "seconds";
                        break;
                    case "MS":
                        return "milliseconds";
                        break;
                    case "MON":
                        return "months";
                        break;
                    case "MIN":
                        return "minutes";
                        break;
                    case "STD":
                        return "hours";
                        break;
                    case "H":
                        return "hours";
                        break;
                    case "TAG":
                        return "days";
                        break;
                    case "D":
                        return "days";
                        break;
                    default:
                        return null;
                        break;
                }
            },

            getNewDateObject: function () {
                var newDate = moment(new Date());
                newDate.hours(0);
                newDate.minutes(0);
                newDate.seconds(0);
                newDate.milliseconds(0);

                return newDate;
            },

            getNewDateObjectForTime: function (newStartTime) {
                var dateMoment = moment(newStartTime);
                dateMoment.hours(0);
                dateMoment.minutes(0);
                dateMoment.seconds(0);
                dateMoment.milliseconds(0);

                return dateMoment;
            },

            getCurrentDateObject: function () {
                var newDateObj = moment(new Date());
                var zeroDateObj = this.getNewDateObject();

                var dateString = zeroDateObj.format("YYYY-MM-DD HH:mm:ss.SSS");
                var timeString = newDateObj.format("HH:mm:ss.SSSS");
                var _dateString = newDateObj.format("YYYY-MM-DD HH:mm:ss.SSS");

                return {
                    dateString: dateString,
                    _dateString: _dateString,
                    timeString: timeString
                }
            },

            getDateObject: function (iValue, sUnit) {
                var newDateObj = moment().add(iValue, sUnit);

                var zerDateObj = newDateObj;
                zerDateObj.hours(0);
                zerDateObj.minutes(0);
                zerDateObj.seconds(0);
                zerDateObj.milliseconds(0);

                var dateString = zerDateObj.format("YYYY-MM-DD HH:mm:ss.SSS");
                var timeString = newDateObj.format("HH:mm:ss.SSSS");
                var _dateString = newDateObj.format("YYYY-MM-DD HH:mm:ss.SSS");

                return {
                    dateString: dateString,
                    _dateString: _dateString,
                    timeString: timeString
                }

            },

            addHoursToStartEndDate: function (timeConfObj) {
                var startDateObj = moment(timeConfObj.START_DATE);
                var endDateObj = moment(timeConfObj.END_DATE);

                var startTime = moment(timeConfObj.START_TIME, "HH:mm:ss.SSS");
                var endTime = moment(timeConfObj.END_TIME, "HH:mm:ss.SSS");
                startDateObj.add(startTime.hours(), "hours");
                startDateObj.add(startTime.minutes(), "minutes");
                startDateObj.add(startTime.seconds(), "seconds");
                startDateObj.add(startTime.milliseconds(), "milliseconds");

                endDateObj.add(endTime.hours(), "hours");
                endDateObj.add(endTime.minutes(), "minutes");
                endDateObj.add(endTime.seconds(), "seconds");
                endDateObj.add(endTime.milliseconds(), "milliseconds");

                return {
                    _startDate: startDateObj.format(startDateObj._f),
                    _endDate: endDateObj.format(endDateObj._f)
                }
            },

            setHoursZero: function (date) {
                var newDate = moment(date);
                newDate.hours(0);
                newDate.minutes(0);
                newDate.seconds(0);
                newDate.milliseconds(0);

                return newDate.format(newDate._f);
            },

            formatMsIntoHours: function (differenceMs) {
                var hourInMs = 1000 * 60 * 60;

                return (differenceMs / hourInMs).toFixed(2);
            },

            formatBackendDateToUIDate: function (backendDate) {
                //	    var oDateFormat = sap.ui.core.format.DateFormat.getInstance({
                //		pattern : "yyyy-MM-dd"
                //	    });
                //	    var oDateFormat2 = sap.ui.core.format.DateFormat.getInstance({
                //		pattern : "yyyy-MM-dd HH:mm:ss.S"
                //	    });
                //	    return oDateFormat.format(oDateFormat2.parse(backendDate));
                return backendDate ? backendDate.substr(0, 10) : "";
            },

            /**
             * This method checks the to be created / updated time confirmation on any overlapping times to existing time confirmations.
             * Therefore, 5 scenarios are possible where the edge cases (same start time, same end time) are also considered.
             *
             * @param timeEntry time confirmation that shall be saved (new or updated)
             * @param allTimeEntries all existing time confirmations
             * @returns array of time confirmations that are overlapping
             */
            getOverlappingTimeConfirmation: function (timeEntry, allTimeEntries, sLanguage) {
                return allTimeEntries.filter(function (oTimeConfirmation) {

                    // Filter out same time confirmation for edit case
                    if (timeEntry.MOBILE_ID === oTimeConfirmation.MOBILE_ID) {
                        return false;
                    }

                    var timeEntryStartTime;
                    var timeEntryEndTime;

                    var timeConfStartTime = moment(oTimeConfirmation.START_DATE.split(" ").shift() + " " + oTimeConfirmation.START_TIME.split(" ").pop());
                    timeEntryStartTime = moment(timeConfStartTime).format("YYYY/MM/DD HH:mm");
                    var timeConfEndTime = moment(oTimeConfirmation.END_DATE.split(" ").shift() + " " + oTimeConfirmation.END_TIME.split(" ").pop());
                    timeEntryEndTime = moment(timeConfEndTime).format("YYYY/MM/DD HH:mm");

                    var startTime, endTime;
                    startTime = moment(timeEntry.START_TIME).format("YYYY/MM/DD HH:mm");
                    endTime = moment(timeEntry.END_TIME).format("YYYY/MM/DD HH:mm");

                    // 1. check: Overlapping Start date
                    //             ----------        Existing
                    //       ----------------        New
                    var bCheck1 = startTime < timeEntryStartTime && startTime < timeEntryEndTime &&
                        endTime > timeEntryStartTime && endTime <= timeEntryEndTime;

                    // 2. check: Overlapping End date
                    //             ----------        Existing
                    //             --------------     New
                    var bCheck2 = startTime >= timeEntryStartTime && startTime < timeEntryEndTime &&
                        endTime > timeEntryStartTime && endTime > timeEntryEndTime;

                    // 3. check: Exactly same rang
                    //             ----------        Existing
                    //             ----------        New
                    var bCheck3 = startTime === timeEntryStartTime && startTime < timeEntryEndTime &&
                        endTime > timeEntryStartTime && endTime === timeEntryEndTime;

                    // 4. check: Within range
                    //             ----------        Existing
                    //                -------        New
                    //             --------          New
                    var bCheck4 = startTime >= timeEntryStartTime && startTime < timeEntryEndTime &&
                        endTime > timeEntryStartTime && endTime <= timeEntryEndTime;

                    // 5. check: Outside range
                    //             ----------        Existing
                    //          -----------------    New
                    var bCheck5 = startTime < timeEntryStartTime && startTime < timeEntryEndTime &&
                        endTime > timeEntryStartTime && endTime > timeEntryEndTime;

                    return bCheck1 || bCheck2 || bCheck3 || bCheck4 || bCheck5;
                });


            },

            getDisplayTimeFormatForCurrentLanguage: function (language) {
                this.rootComponent = sap.ui.getCore().byId("samComponent").getComponentInstance();

                var availableDisplayTimeFormats = this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.displayTimeFormat;

                // set default german timeFormat
                var timeFormat = this.rootComponent.getManifestObject().getEntry("sap.app").SAMConfig.displayTimeFormat.D;

                // set selected timeFormat
                Object.entries(availableDisplayTimeFormats).forEach(function (format) {
                    if (format[0] === language) {
                        timeFormat = format[1];
                    }
                });

                return timeFormat;

            },

        };
    });