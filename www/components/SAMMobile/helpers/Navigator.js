sap.ui.define([ "sap/ui/model/json/JSONModel",
                "SAMMobile/helpers/formatter",
                'sap/m/Dialog',
               	'sap/m/Button',
               	'sap/m/Text',
               	'sap/m/List',
               	'sap/m/StandardListItem'],

function(JSONModel, formatter, Dialog, Button, Text, List, StandardListItem) {
    "use strict";
    // constructor
    function Navigator() {
	this.model = new JSONModel();
	this.setAvailableApps();
    }

    Navigator.prototype = {
	app : null,
	availableApps : [],
	navTo : function(location, successCb, errorCb, app) {
	    var that = this;
	    var launchApp = app || this.app;
	    launchnavigator.navigate(location, {
		app : launchApp,
		successCallback : successCb,
		errorCallback : errorCb,
		enableDebug : true
	    });
	},
	setAvailableApps : function() {
	    var that = this;
	    launchnavigator.availableApps(function(results) {
		for ( var app in results) {
		    if (results[app]) {
			var obj = {};
			obj.app = app;
			that.availableApps.push(obj);
		    }
		}
	    });

	    // if(this.availableApps.length === 1) {
	    // this.setDefaultApp(this.availableApps[0]);
	    // }

	    this.model.setData(this.availableApps);
	},
	setDefaultApp : function(appName) {
	    this.app = appName;
	}
    };

    Navigator.prototype.getSelectionDialog = function(onChange) {
	var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
	var oList = new List({
	    items : []
	});
	var items = new StandardListItem({
	    title : {
		parts : [ {
		    path : "app"
		} ],
		formatter : formatter.formatToAppName
	    },
	    type : sap.m.ListType.Active
	});

	oList.setModel(this.model);
	oList.bindAggregation("items", "/", items);
	oList.attachItemPress(onChange);

	return new Dialog({
	    title : oBundle.getText("selectNavigationApp"),
	    type : sap.m.DialogType.Message,
	    content : oList,
	    beginButton : new Button({
		text : oBundle.getText("cancel"),
		press : function() {
		    this.getParent().close();
		}
	    }),
	    afterClose : function() {
		this.destroy();
	    }
	});
    }

    var Navigator = Navigator;

    return Navigator;
});