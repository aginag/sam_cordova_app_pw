sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMMobile/helpers/formatter",
        "SAMMobile/helpers/FileSystem",
        "sap/m/Dialog",
        "sap/m/Button",
        "sap/m/Text",
        "sap/ui/core/HTML",
        "sap/m/Image",
        "sap/m/LightBox",
        "sap/m/LightBoxItem",
        "sap/m/Input"
    ],
    function (JSONModel, formatter, FileSystem, Dialog, Button, Text, HTML, Image, LightBox, LightBoxItem, Input) {
        "use strict";

        // constructor
        function MediaCapture() {
            this.mediaCapture = navigator.device.capture;
            this.fileSystem = new FileSystem();

            this.oBundle = sap.ui.getCore()
                .byId("samComponent")
                .getComponentInstance()
                .getModel("i18n")
                .getResourceBundle();
            this.os = sap.ui.Device.os.name;
            this.device = sap.ui.Device.os;
        }

        MediaCapture.prototype = {

            captureAudio: function (success, error) {
                var that = this;

                var successCb = function (data) {
                    data = JSON.parse(data);

                    data.full_path = that.fileSystem._setPrefixFilePath(data.full_path);
                    success(data);
                };

                var errorCb = function (data) {
                    data = JSON.parse(data);
                    error(data);
                };

                navigator.device.audiorecorder.recordAudio(successCb, errorCb);
            },

            captureVideo: function (success, error) {
                var that = this;

                var successCb = function (data) {
                    var successObj = that.getCaptureSuccessObject(data);

                    if (that.os === that.device.OS.IOS || that.os === that.device.OS.ANDROID) {
                        successObj.customPath = successObj.fullPath;
                    }

                    success(data, successObj);
                };

                var errorCb = function (data) {
                    error(data);
                };
                this.mediaCapture.captureVideo(successCb, errorCb, {
                    limit: 1,
                    duration: 30,
                    quality: 0
                });
            },

            capturePhoto: function (success, error) {
                var that = this;

                var successCb = function (data) {
                    var successObj = that.getCaptureSuccessObject(data);

                    if (that.os === that.device.OS.ANDROID) {
                        successObj.customPath = successObj.fullPath;
                    }

                    success(data, successObj);
                };

                var errorCb = function (data) {
                    error(data);
                };

                this.mediaCapture.captureImage(successCb, errorCb, {
                    limit: 1
                });
            },

            getCaptureSuccessObject: function (data) {
                return {
                    fullPath: data[0].fullPath,
                    fileName: data[0].name,
                    customPath: null,
                    dialog: this.getEditAttachmentDialog()
                }
            },

            getMediaFileObject: function () {
                return {
                    end: 0,
                    fullPath: "",
                    lastModified: null,
                    lastModifiedDate: null,
                    localURL: "",
                    name: "",
                    size: 0,
                    start: 0,
                    type: ""
                }
            }
        };

        MediaCapture.prototype.onAfterAddedFileName = function (payload, success, error) {
            // Bound to the view context in which the function is called

            var that = this;
            this.getOwnerComponent().setBusyOn();
            var newAttachment = payload.dialog.getModel("attachment").oData;

            if (payload.isSkip) {
                newAttachment.fileName = payload.objectType + "_" + new Date().getTime().toString();
            }

            this.mediaCapture.checkFileNameUnique(payload.attachmentObj, newAttachment, function () {
                if (payload.customPath) {
                    var filePath = payload.customPath;
                } else {
                    var filePath = that.mediaCapture.getCapturedAttachmentPath(payload.attachmentObj);
                }
                var fileName = newAttachment.fileName + "." + payload.attachmentObj.FILEEXT;

                that.fileSystem.renameFile(filePath, fileName, function () {
                    payload.attachmentObj["FILENAME"] = newAttachment.fileName;
                    that.getOwnerComponent().setBusyOff();
                    payload.dialog.close();

                    success(payload.attachmentObj);
                }, function () {
                    that.getOwnerComponent().setBusyOff();
                    payload.dialog.close();
                });
            }, function () {
                error();
            });

        };

        MediaCapture.prototype.getAttachmentDialog = function (selectedItem) {
            var that = this;
            switch (selectedItem.FILEEXT.toLowerCase()) {
                case "jpg":
                    return this.getTaskAttachmentImgDialog(selectedItem);
                case "jpeg":
                    return this.getTaskAttachmentImgDialog(selectedItem);
                case "png":
                    return this.getTaskAttachmentImgDialog(selectedItem);
                case "pdf":
                    openFile(this.getAttachmentPath(selectedItem));
                    break;
                case "txt":
                    return this.getTaskAttachmentTxtDialog(selectedItem);
                case "mov":
                    return this.getTaskAttachmentVideoDialog(selectedItem);
                case "mp4":
                    return this.getTaskAttachmentVideoDialog(selectedItem);
                case "wav":
                    return this.getTaskAttachmentAudioDialog(selectedItem);
                case "amr":
                    return this.getTaskAttachmentAudioDialog(selectedItem);
                default:
                    break;
            }

            function openFile(filePath) {
                if (!isWindowsOs) {
                    //this does not work on windows: inappbrowser cannot open pdfs, see http://stackoverflow.com/questions/34751851/unable-to-open-local-file-using-cordova-inappbrowser-on-windows-8-1-platform
                    window.open(filePath, '_blank', 'location=yes');
                }
                else {
                    openLocalFile(filePath,
                        function onSuccess() {
                        },
                        function onError(error) {
                            //this should not be done here...
                            MessageBox.alert(that.oBundle.getText("ERROR_COULDNOT_OPEN_FILE",[filePath, error]));
                        }
                    );
                }
            }
        };

        MediaCapture.prototype.getTaskAttachmentTxtDialog = function (txtObj) {
            var that = this;
            var fullPath = this.getAttachmentPath(txtObj);
            var htmlString = "<iframe src='" + fullPath + "' frameborder='0' height='400' width='95%'></iframe>";
            var dialog = new Dialog({
                title: txtObj.FILENAME,
                content: new HTML({
                    content: htmlString
                }),
                beginButton: new Button({
                    text: that.oBundle.getText("close"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return dialog;
        };

        MediaCapture.prototype.getTaskAttachmentImgDialog = function (imageObj) {
            var that = this;
            var fullPath = this.getAttachmentPath(imageObj);
            var lightBox = new LightBox({
                imageContent: new LightBoxItem({
                    imageSrc: fullPath,
                    alt: "Alt",
                    title: imageObj.FILENAME + "." + imageObj.FILEEXT,
                    subTitle: imageObj.TABNAME
                })
            });
            return lightBox;
        };

        MediaCapture.prototype.getTaskAttachmentVideoDialog = function (videoObj) {
            var that = this;
            var fullPath = this.getAttachmentPath(videoObj);
            var mime = this.getMimeTypeForFileExt(videoObj.FILEEXT);
            var htmlString = "<video width='100%' height='100%' controls='controls'><source src='" + fullPath + "' type='" + mime + "'></video>";
            var dialog = new Dialog({
                title: videoObj.FILENAME,
                content: new HTML({
                    content: htmlString
                }),
                beginButton: new Button({
                    text: that.oBundle.getText("close"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return dialog;
        };

        MediaCapture.prototype.getTaskAttachmentAudioDialog = function (audioObj) {
            var that = this;
            var fullPath = this.getAttachmentPath(audioObj);
            var mime = this.getMimeTypeForFileExt(audioObj.FILEEXT);

            var htmlString = "<audio style='width:100%;' controls><source src='" + fullPath + "' type='" + mime + "'>Browser does not supperot</audio>";

            var dialog = new Dialog({
                title: audioObj.FILENAME,
                content: new HTML({
                    content: htmlString
                }),
                beginButton: new Button({
                    text: that.oBundle.getText("close"),
                    press: function () {
                        this.getParent().close();
                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            return dialog;
        };

        MediaCapture.prototype.getEditAttachmentDialog = function () {
            var that = this;
            var dialog = new Dialog({
                title: that.oBundle.getText("EnterFileName"),
                content: new Input({
                    maxLength: 50,
                    placeholder: that.oBundle.getText("EnterFileName"),
                    value: "{attachment>/fileName}",
                    valueLiveUpdate: true
                }),
                beginButton: new Button({
                    text: that.oBundle.getText("skip"),
                    press: function () {

                    }
                }),
                endButton: new Button({
                    text: that.oBundle.getText("save"),
                    enabled: "{= ${attachment>/fileName}.length > 0}",
                    press: function () {

                    }
                }),
                afterClose: function () {
                    this.destroy();
                }
            });

            dialog.setModel(new JSONModel({
                fileName: ""
            }), "attachment");

            return dialog;
        };
        MediaCapture.prototype.getErrorDialog = function (message, type, endButton) {
            var that = this;
            var dialog = new Dialog({
                title: 'Error',
                type: sap.m.DialogType.Message,
                state: type,
                content: new Text({
                    text: message
                }),
                beginButton: new Button({
                    text: 'OK',
                    press: function () {
                        dialog.close();
                    }
                }),
                endButton: endButton !== undefined ? endButton : null,
                afterClose: function () {
                    dialog.destroy();
                }
            });

            return dialog;
        };

        MediaCapture.prototype.getAttachmentPath = function (attachment) {
            var fullPath, fileName;
            if (attachment.ACTION_FLAG === "N" && attachment.MOBILE_ID !== "") {
                fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                fullPath = this.fileSystem.filePathDoc + fileName;
                return fullPath;
            } else if (attachment.ACTION_FLAG === "N" && attachment.MOBILE_ID === "") {
                fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                fullPath = this.fileSystem.filePathTmp + fileName;
                return fullPath;
            } else if (attachment.ACTION_FLAG === "I") {
                fileName = attachment.MOBILE_ID.replace(/-/g, '') + "." + attachment.FILEEXT;
                fullPath = this.fileSystem.filePathDoc + fileName.toUpperCase();
                return fullPath;
            }
        };

        MediaCapture.prototype.getCapturedAttachmentPath = function (attachment) {
            var fullPath, fileName;
            var device = sap.ui.Device.os;
            var os = device.name;

            switch (os) {
                case device.OS.ANDROID:
                    fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                    fullPath = this.fileSystem.filePathTmp + fileName;
                    return fullPath;
                case device.OS.IOS:
                    fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                    fullPath = this.fileSystem.filePathTmp + fileName;
                    return fullPath;
                case device.OS.WINDOWS:
                    fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                    fullPath = this.fileSystem.filePathDoc + fileName;
                    return fullPath;
                case device.OS.WINDOWS_PHONE:
                    fileName = attachment.FILENAME + "." + attachment.FILEEXT;
                    fullPath = this.fileSystem.filePathDoc + fileName;
                    return fullPath;
                default:
                    break;
            }
        };

        MediaCapture.prototype.getMimeTypeForFileExt = function (ext) {
            switch (ext.toLowerCase()) {
                case "mov":
                    return "video/quicktime";
                case "m4a":
                case "mp4":
                    return "video/mp4";
                case "3gp":
                    return "video/3gpp";
                case "wav":
                    return "audio/wav";
                case "mp3":
                    return "audio/mpeg3";
                case "amr":
                    return "audio/amr";
                default:
                    return "";
            }
        };

        MediaCapture.prototype.checkFileNameUnique = function (oldAttachment, newAttachment, successCb, errorCb) {
            var tmpAttachment = Object.assign({}, oldAttachment);
            tmpAttachment["FILENAME"] = newAttachment.fileName.replace(/\.[^/.]+$/, "");
            var filePath = this.getAttachmentPath(tmpAttachment);
            var fileName = filePath.split("/").pop();

            //check if entered fileName exists,
            this.fileSystem.exists(fileName, function () {
                errorCb();
            }, function () {
                successCb(tmpAttachment);
            });
        };

        var MediaCapture = MediaCapture;

        return MediaCapture;
    });
