sap.ui.define(["SAMMobile/helpers/statusProfile/StatusObject"],

    function (StatusObject) {
        "use strict";

        // constructor
        function StatusHierarchy(status, defaults, component) {
            this.component = component;

            this.defaults = defaults;
            this.status = status.map(function (s) {
                return new StatusObject(s);
            });

            this._buildRelations();
        }

        StatusHierarchy.prototype = {
            getOptionsForStatus: function (status, statusProfile) {
                var that = this;
                var options = [];
                // When no status is set on operation, we default it to accept and reject
                if (status == "") {
                    try {
                        that.defaults.forEach(function (defaultStatus) {
                            that.status.forEach(function (stat) {
                                if (stat.status === defaultStatus.STATUS && stat.statusProfile === defaultStatus.STATUS_PROFILE) {
                                    options.push(that.getStatusObject(defaultStatus.STATUS, defaultStatus.STATUS_PROFILE));
                                }
                            });
                        });
                    } catch (err) {
                        throw new Error(that.component.i18n.getText("ERROR_USER_STATUS_SET_DEFAULT_FOR_OPERATION"));
                    }

                    return options;
                }

                for (var i = 0; i < this.status.length; i++) {
                    var statusObject = this.status[i];
                    if (statusObject.status == status && statusObject.statusProfile == statusProfile) {
                        return statusObject.options;
                    }
                }

                return options;
            },

            getStatusObject: function (status, statusProfile) {
                for (var i = 0; i < this.status.length; i++) {
                    var statusObject = this.status[i];

                    if (statusObject.userStatus == status && statusObject.statusProfile == statusProfile) {
                        return statusObject;
                    }
                }

                return null;
            },

            _buildRelations: function () {
                var that = this;
                this.status.forEach(function (status) {

                    status.options = status.children.map(function (childId) {
                        return that._getStatusById(childId);
                    });
                });
            },

            _getStatusById: function (id) {
                for (var i = 0; i < this.status.length; i++) {
                    var status = this.status[i];

                    if (status.id == id) {
                        return status;
                    }
                }
                return null;
            }
        };

        return StatusHierarchy;
    }
);