sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function StatusObject(status) {
            this.id = status.ID;
            this.actType = status.ACT_TYPE;
            this.async = status.ASYNC == "X" ? true : false;
            this.autoSync = status.AUTOSYNC == "X" ? true : false;
            this.stopWatch = status.STOPWATCH;
            this.status = status.STATUS;
            this.statusProfile = status.STATUS_PROFILE;
            this.statusDescription = status.USER_STATUS_DESC;
            this.userStatus = status.USER_STATUS;
            this.icon = status.ICON;
            this.children = this._getChildrenIdArray(status.CHILD);


            this.options = [];
        }

        StatusObject.prototype = {
            startsStopwatch: function () {
                return this.stopWatch === "X";
            },

            _getChildrenIdArray: function (childrenString) {
                if (childrenString.length == 0) {
                    return [];
                }

                // Split by comma or semicolon
                return childrenString.split(/[\s,;]+/).map(function (childId) {
                    return parseInt(childId);
                });
            }
        };

        return StatusObject;
    }
);