sap.ui.define([],
    function () {
        "use strict";
        return {
            UNDERTAKING_SERVICES: [
                {
                    id: "notification-no",
                    source: ["order.NOTIF_NO"]
                },
                {
                    id: "customer-name",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "customer-name-footer",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "mobile-number",
                    source: ["orderPartner.MOBILE_NUMBER"]
                },
                {
                    id: "ss-no",
                    source: ["order.FUNCT_LOC"]
                },
                {
                    id: "tech-name",
                    source: ["user.firstName", "user.lastName"]
                },
            ],

            SAFETY_NOTICE: [
                {
                    id: "name",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "address",
                    source: ["orderPartner.STREET", "orderPartner.CITY1"]
                }
                // On top, addreess like dispalyed on order detail screen
                // Legacy No
            ],

            SAFETY_CHECKLIST: [
                {
                    id: "department",
                    source: ["order.WORK_CNTR"]
                },
                {
                    id: "substation",
                    source: ["order.FUNCT_LOC"]
                }
                ,
                {
                    id: "section",
                    source: ["order._FORM_SECTION"]
                }
            ],

            OVERLOAD_NOTICE: [
                {
                    id: "complain-no",
                    source: ["order.NOTIF_NO"]
                },
                {
                    id: "to",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "name",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "ss",
                    source: ["order.FUNCT_LOC"]
                },
                {
                    id: "address",
                    source: ["orderPartner.STREET", "orderPartner.CITY1"]
                }
                // On top, addreess like dispalyed on order detail screen
                // Legacy No
            ],

            JOINT_CHECKLIST: [
                {
                    id: "notification",
                    source: ["order.NOTIF_NO"]
                },
                {
                    id: "gps",
                    source: ["orderPartner.ADR_LAT", "orderPartner.ADR_LON"]
                },
                {
                    id: null,
                    source: ["teamData"],
                    function: "prefillAttendeeSelects"
                }
            ],

            DAMAGE_REPORT: [
                {
                    id: "notification-no",
                    source: ["order.NOTIF_NO"]
                },
                {
                    id: "name",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "tel",
                    source: ["orderPartner.TEL_NUMBER"]
                },
                {
                    id: "mobile",
                    source: ["orderPartner.MOBILE_NUMBER"]
                },
                {
                    id: "email",
                    source: ["orderPartner.EMAIL_ADDR"]
                },
                {
                    id: "tech-pr",
                    source: ["user.personnelNumber"]
                }
                // On top, Same tel number like displayed on order details screen
                // Same mobile number like displayed on order details screen
            ],

            FIRE_INCIDENT: [
                {
                    id: "sspkpmno",
                    source: ["order.FUNCT_LOC"]
                },
                {
                    id: "consumer",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "tel",
                    source: ["orderPartner.TEL_NUMBER"]
                },
                {
                    id: "tech-pr",
                    source: ["user.personnelNumber"]
                }
                // On top, Same tel number like displayed on order details screen
                // Same mobile number like displayed on order details screen
            ],

            GENERAL_UNDERTAKING: [
                {
                    id: "name",
                    source: ["orderPartner.NAME_LIST"]
                },
                {
                    id: "mobile",
                    source: ["orderPartner.MOBILE_NUMBER"]
                },
                {
                    id: "house",
                    source: ["orderPartner.ZZMAKANI"]
                },
                {
                    id: "contract",
                    source: ["orderPartner.PURCH_NO_C"]
                },
                {
                    id: "street",
                    source: ["orderPartner.STREET", "orderPartner.CITY1"]
                },
                // On top, Same tel number like displayed on order details screen
                // Same mobile number like displayed on order details screen
                // Same house no like on order details screen
                // Same Street like on order details screen
                // Contract Account No displayed on order details screen
            ],

            RTA: [
                {
                    id: "sap",
                    source: ["order.NOTIF_NO"]
                },
                {
                    id: "ref",
                    source: ["order.FUNCT_LOC"]
                },
                {
                    id: "ref-2",
                    source: ["order.FUNCT_LOC"]
                },
                {
                    id: "order-1",
                    source: ["order.ORDERID"]
                },
                {
                    id: "order-2",
                    source: ["order.ORDERID"]
                },
                {
                    id: "complaint",
                    source: ["order.NOTIF_NO"]
                },
            ],

            TBT_REPORT: [
                {
                    id: null,
                    source: ["teamData"],
                    function: "prefillAttendees"
                },
                {
                    id: "dept",
                    source: ["order.WORK_CNTR"]
                },
                {
                    id: "givenby",
                    source: ["user.firstName", "user.lastName"]
                },
                {
                    id: "location",
                    source: ["order._FUNCT_LOC"]
                }
            ]
        };
    });