sap.ui.define([],
   
   function() {
      "use strict";
      
      function FileSystem() {
          try {
              this.oBundle = this.component.getModel("i18n").getResourceBundle();
          } catch(err) {

          }

          this._setPaths();
      }
      
      FileSystem.prototype.write = function(fileName, blob, successCb, errorCb) {
         var that = this;
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(this.filePathDoc, function(directoryEntry) {
            directoryEntry.getFile(fileName, {
               create: true
            }, function(fileEntry) {
               fileEntry.createWriter(function(fileWriter) {
                  fileWriter.onwriteend = function(e) {
                     successCb(that.filePathDoc + "/" + fileName, e);
                  };
                  
                  fileWriter.onerror = function(e) {
                     that.errorHandler(onFsError, e);
                  };
                  
                  fileWriter.write(blob);
               }, that.errorHandler.bind(null, onFsError));
            }, that.errorHandler.bind(null, onFsError));
         }, that.errorHandler.bind(null, onFsError));
      };

       FileSystem.prototype.writeToTemp = function(fileName, blob, successCb, errorCb) {
           var that = this;
           var onFsError = function(msg) {
               errorCb(msg);
           };

           window.resolveLocalFileSystemURL(this.filePathTmp, function(directoryEntry) {
               directoryEntry.getFile(fileName, {
                   create: true
               }, function(fileEntry) {
                   fileEntry.createWriter(function(fileWriter) {
                       fileWriter.onwriteend = function(e) {
                           successCb(that.filePathTmp + "/" + fileName, e);
                       };

                       fileWriter.onerror = function(e) {
                           that.errorHandler(onFsError, e);
                       };

                       fileWriter.write(blob);
                   }, that.errorHandler.bind(null, onFsError));
               }, that.errorHandler.bind(null, onFsError));
           }, that.errorHandler.bind(null, onFsError));
       };
      
      /**
       * returns the full path of the device's data directory 
       * @returns {string} 
       */
      FileSystem.prototype.getRootDataStorage = function () {
          var device = sap.ui.Device.os;
          var os = device.name;

          switch (os) {
              case device.OS.ANDROID:
                  return cordova.file.dataDirectory + "files/";
              case device.OS.IOS:
                  return cordova.file.documentsDirectory;
              case device.OS.WINDOWS:
                  return cordova.file.dataDirectory;
              case device.OS.WINDOWS_PHONE:
                  return cordova.file.dataDirectory; //needs to be verified
              default:
                  return null;
          }
      };

       FileSystem.prototype.getFileSize = function (filePath, successCb, errorCb) {
           var that = this;
           window.resolveLocalFileSystemURL(filePath,
               function (fileEntry) {
                   fileEntry.getMetadata(
                       function (metadata) {
                           successCb(metadata.size); // return file size
                       },
                       onError
                   );
               },
               onError
           );

           function onError(error) {
               var errorMsg = error.code ? error.code : error;
               var msg = that.oBundle.getText("ERROR_FILE_READ") + filePath + ": " + errorMsg;
               errorCb(error);
           }
       };
      
      FileSystem.prototype.deleteByPath = function(filePath, successCb, errorCb) {
         var that = this;
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(filePath, function(file) {
            file.remove(function() {
               successCb(filePath);
            }, that.errorHandler.bind(null, onFsError));
         }, that.errorHandler.bind(null, onFsError));
      };

       FileSystem.prototype.deleteByPathPromise = function(filePath) {
           var that = this;
           return new Promise(function(resolve, reject) {
                that.deleteByPath(filePath, resolve, reject);
           });
       };
      
      FileSystem.prototype.exists = function(fileName, successCb, errorCb) {
         var that = this;
         var tmpPath = this.filePathTmp + fileName;
         var docPath = this.filePathDoc + fileName;
         
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(tmpPath, function(file) {
            successCb();
         }, window.resolveLocalFileSystemURL.bind(null, docPath, function(file2) {
            successCb();
         }, that.errorHandler.bind(null, onFsError)));
      };

       /**
        *
        * @param {type} fileNamePath
        * @param {type} successCb if the file could be accessed
        * @param {type} errorCb returns a FileError object
        */
       FileSystem.prototype.exists2 = function (filePath, successCb, errorCb) {
           window.resolveLocalFileSystemURL(filePath, successCb, errorCb);
       };

       FileSystem.prototype.existsPromise = function (filePath) {
           return new Promise(function(resolve, reject) {
               window.resolveLocalFileSystemURL(filePath, reject, resolve);
           });
       };
      
      FileSystem.prototype.createDir = function(dirName, successCb, errorCb) {
         var that = this;
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(this.filePathTmp, function(directoryEntry) {
            directoryEntry.getDirectory(dirName, {
               create: true
            }, function(dirEntry) {
               successCb(that.filePathTmp + "/" + dirName);
            }, that.errorHandler.bind(null, onFsError));
         }, that.errorHandler.bind(null, onFsError));
      };
      
      FileSystem.prototype.moveFile = function(filePath, fileName, successCb, errorCb) {
         var that = this;
         
         filePath = this._setPrefixFilePath(filePath);
         
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(filePath, function(fileEntry) {
            var newFileUri = that.filePathDoc;
            var oldFileUri = filePath;
            
            var newFileName = fileName;
            window.resolveLocalFileSystemURL(newFileUri, function(dirEntry) {
               // move the file to a new directory and rename it
               var onMoveSuccess = function(data) {
                  var fileObj = {};
                  fileObj["name"] = data.name.split(".")[0];
                  fileObj["ext"] = data.name.split(".")[1];
                  successCb(fileObj);
               };
               
               fileEntry.moveTo(dirEntry, newFileName, onMoveSuccess, that.errorHandler.bind(null, onFsError));
            }, that.errorHandler.bind(null, onFsError));
         }, that.errorHandler.bind(null, onFsError));
      };
      
      /**
       * moves a file to another directory and optionally renames it
       * @param {string} filePath: as URI, e.g: "ms-appdata:///local/cameraCaptureImage.jpg"
       * @param {string} destFolderPath: destination folder as URI
       * @param {string} newFileName: optional, set to null if you do not want to rename the file
       * @param {func} successCb
       * @param {func} errorCb
       */
      FileSystem.prototype.moveFile2 = function (filePath, destFolderPath, newFileName, successCb, errorCb) {
          var that = this;
          //filePath = this._setPrefixFilePath(filePath);

          window.resolveLocalFileSystemURL(filePath,
              function (fileEntry) {
                  window.resolveLocalFileSystemURL(destFolderPath,
                      function (destEntry) {
                          var destFileName = fileEntry.name;
                          if (newFileName) {
                              destFileName = newFileName;
                          }
                          fileEntry.moveTo(destEntry, newFileName, successCb, onError);
                      },
                       onError
                   );
              },
              onError
           );

          function onError(error) {
              var errorMsg = error.code ? error.code : error;
              var msg = that.oBundle.getText("ERROR_FILE_MOVE") + filePath + that.oBundle.getText("TO") + destFolderPath + "/" + newFileName + ": " + errorMsg;
              errorCb(error);
          }
      };

      FileSystem.prototype.moveFilePromise = function (filePath, destFolderPath, newFileName) {
          var that = this;
          return new Promise(function(resolve, reject) {
                that.moveFile2(filePath, destFolderPath, newFileName, resolve, reject);
          });
      };
      
      FileSystem.prototype.renameFile = function(filePath, fileName, successCb, errorCb) {
         var that = this;
         
         filePath = this._setPrefixFilePath(filePath);
         
         var onFsError = function(msg) {
            errorCb(msg);
         };
         
         window.resolveLocalFileSystemURL(filePath, function(fileEntry) {
            var newFileUri = that.filePathTmp;
            var oldFileUri = filePath;
            
            var newFileName = fileName;
            window.resolveLocalFileSystemURL(newFileUri, function(dirEntry) {
               // move the file to a new directory and rename it
               var onMoveSuccess = function(data) {
                  var fileObj = {};
                  fileObj["name"] = data.name.split(".")[0];
                  fileObj["ext"] = data.name.split(".")[1];
                  successCb(fileObj);
               };
               
               fileEntry.moveTo(dirEntry, newFileName, onMoveSuccess, that.errorHandler.bind(null, onFsError));
            }, that.errorHandler.bind(null, onFsError));
         }, that.errorHandler.bind(null, onFsError));
      };

       /**
        * path must contain "/" characters only
        * @param {string} filePath as URI, e.g: "ms-appdata:///local/cameraCaptureImage.jpg"
        * @param {string} newFileName
        * @param {func} successCb
        * @param {func} errorCb
        * @return the fileEntry object of the renamed file
        */
       FileSystem.prototype.renameFile2 = function (filePath, newFileName, successCb, errorCb) {
           var that = this;
           filePath = this._setPrefixFilePath(filePath);

           var parentPath = "";
           //TODO: make this work also with "\" chars
           var pathSeparatorIndex = filePath.lastIndexOf("/");
           if (pathSeparatorIndex > 0) {
               parentPath = filePath.substring(0, pathSeparatorIndex + 1);
           }

           window.resolveLocalFileSystemURL(filePath,
               function (fileEntry) {
                   window.resolveLocalFileSystemURL(parentPath,
                       function (parentEntry) {
                           fileEntry.moveTo(parentEntry, newFileName, successCb, onError);
                       },
                       onError
                   );
               },
               onError
           );

           function onError(error) {
               var errorMsg = error.code ? error.code : error;
               var msg = that.oBundle.getText("ERROR_FILE_REMAIN") + filePath + that.oBundle.getText("TO") + newFileName + ": " + errorMsg;
               errorCb(error);
           }
       };

       FileSystem.prototype.renameFilePromise = function(filePath, newFileName) {
           const that = this;
            return new Promise(function(resolve, reject) {
                that.renameFile2(filePath, newFileName, resolve, reject);
            });
       };
      
      FileSystem.prototype.errorHandler = function(callBack, e) {
          var that = this;
         var msg = '';

        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
               msg = that.oBundle.getText("STORAGE_QUOTA_EXCEEDED");
               break;
            case FileError.NOT_FOUND_ERR:
               msg = that.oBundle.getText("FILE_NOT_FOUND");
               break;
            case FileError.SECURITY_ERR:
               msg = that.oBundle.getText("ERROR_SECURITY");
               break;
            case FileError.INVALID_MODIFICATION_ERR:
               msg = that.oBundle.getText("INVALID_MODIFICATION");
               break;
            case FileError.INVALID_STATE_ERR:
               msg = that.oBundle.getText("INVALID_STATE");
               break;
            default:
               msg = that.oBundle.getText("ERROR_UNKNOWN");
               break;
         }
         ;
         
         callBack(msg);
      };
      
      FileSystem.prototype.getAttachmentPath = function(attachment) {
         var fullPath, fullFileName, fileName;
         
         if (attachment.hasOwnProperty("_oldFileName")) {
            fileName = attachment._oldFileName;
         } else {
            fileName = attachment.FILENAME;
         }
         
         if (attachment.ACTION_FLAG === "N" && attachment.MOBILE_ID !== "") {
            fullFileName = fileName + "." + attachment.FILEEXT;
            fullPath = this.filePathDoc + fullFileName;
            return fullPath;
         } else if (attachment.ACTION_FLAG === "N" && attachment.MOBILE_ID === "") {
            fullFileName = fileName + "." + attachment.FILEEXT;
            fullPath = this.filePathTmp + fullFileName;
            return fullPath;
         } else if (attachment.ACTION_FLAG === "I") {
            fullFileName = attachment.LINKID + "." + fileName + "." + attachment.FILEEXT;
            fullPath = this.filePathDoc + fullFileName;
            return fullPath;
         }
      };
      
      FileSystem.prototype._setPaths = function() {
         var device = sap.ui.Device.os;
         var os = device.name;
         
         switch (os) {
            case device.OS.ANDROID:
                this.filePathDoc = cordova.file.dataDirectory + "files/";
                this.filePathTmp = cordova.file.cacheDirectory;
               break;
            case device.OS.IOS:
               this.filePathDoc = cordova.file.documentsDirectory;
               this.filePathTmp = cordova.file.tempDirectory;
               break;
            case device.OS.WINDOWS:
               this.filePathDoc = cordova.file.dataDirectory;
               this.filePathTmp = cordova.file.tempDirectory;
               break;
            case device.OS.WINDOWS_PHONE:
               this.filePathDoc = cordova.file.dataDirectory;
               this.filePathTmp = cordova.file.tempDirectory;
               break;
            default:
               break;
         }
      };
      
      FileSystem.prototype._setPrefixFilePath = function(filePath) {
         var device = sap.ui.Device.os;
         var os = device.name;
         
         if (os === device.OS.ANDROID || os === device.OS.IOS) {
            if (!filePath.startsWith("file://")) {
               filePath = "file://" + filePath;
               return filePath;
            }
         } else if (os === device.OS.WINDOWS || os === device.OS.WINDOWS_PHONE) {
            if (!filePath.startsWith("ms-appdata://")) {
               filePath = "ms-appdata://" + filePath;
               return filePath;
            }
         }
         
         return filePath;
      };
      
      var FileSystem = FileSystem;
      
      return FileSystem;
      
   });
