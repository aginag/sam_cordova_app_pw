sap.ui.define(["sap/ui/model/json/JSONModel", "SAMMobile/helpers/formatter"],

   function(JSONModel, formatter) {
      "use strict";

      // Extension to get access by path for nested Objects
      // see here http://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
      Object.byString = function(o, s) {
         s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
         s = s.replace(/^\./, '');           // strip a leading dot
         var a = s.split('.');
         for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
               o = o[k];
            } else {
               return;
            }
         }
         return o;
      };

      // Validator
      function Validator(model, valueStateModel, mode) {
         this.dataModel = model;
         this.valueStateModel = valueStateModel;
         this.mode = mode === undefined ? Validator.modes.SINGULAR : mode;

         this.checks = [];

         //Static check
         this.sharedCheck = new Check(null, null, null);
      }

      Validator.prototype.check = function(property, errorMessage, errorCategory) {

         if (this.mode === Validator.modes.SINGULAR) {

            var checkExists = this._checkExists(property, errorMessage);

            if (!checkExists) {
               var newCheck = new Check(property, this._getValueForPath(property), errorMessage, errorCategory);
               this.checks.push(newCheck);

               return newCheck;
            } else {
               checkExists.value = this._getValueForPath(property);
               return checkExists;
            }

         } else if (this.mode === Validator.modes.MODULAR) {

            var newCheck = new Check(property, this._getValueForPath(property), errorMessage, errorCategory);
            this.checks.push(newCheck);

            return newCheck;

         }
         
      };

      Validator.prototype.checkErrors = function() {
         var that = this;

         var checksWithError = this.checks.filter(function(check) {
            if (check.errors.length > 0) {
               that.valueStateModel.oData[check.property.split(".").pop()] = sap.ui.core.ValueState.Error;
               return true;
            } else {
               return false;
            }
         });

         this.valueStateModel.refresh();

         return checksWithError.length > 0 ? checksWithError : null;
      };

      Validator.prototype._getValueForPath = function(path) {
         if (path === "") {
            return this.dataModel;
         } else {
            return Object.byString(this.dataModel, path);
         }
      };

      Validator.prototype._checkExists = function(property, errorMessage) {
         var check = this.checks.filter(function(check) {
            return check.property === property && check.errorMessage === errorMessage;
         });

         return check.length > 0 ? check[0] : null;
      };

      Validator.errorTypes = {
         IS_EMPTY: 1,
         NO_DATE: 2,
         DATE_SMALL: 3,
         NO_NUMBER: 4,
         NUMBER_SMALL: 5,
         DUPLICATE_ERROR: 6,
         CUSTOM: 7
      };

      Validator.modes = {
         SINGULAR: 1,
         MODULAR: 2
      };

      // Check
      function Check(property, value, errorMessage, errorCategory) {
         this.property = property;
         this.value = value;
         this.errorMessage = errorMessage;
         this.errorCategory = errorCategory;
         this.errors = [];
      }

      Check.prototype.isEmpty = function(value) {
         
         var valueToCheck = this.value === null ? value : this.value;
         var check = valueToCheck === "";

         if (check) {
            this._pushError(Validator.errorTypes.IS_EMPTY);
         } else {
            this._removeError(Validator.errorTypes.IS_EMPTY);
         }

         return check;
      };

      Check.prototype.isDate = function(format, value) {
         var valueToCheck = this.value === null ? value : this.value;
         var check = moment(valueToCheck, format, true).isValid();

         if (!check) {
            this._pushError(Validator.errorTypes.NO_DATE);
         } else {
            this._removeError(Validator.errorTypes.NO_DATE);
         }

         return check;
      };

      Check.prototype.dateGreaterThan = function(x, value) {
         // Only check if this.value/value and x are valid dates
         // Otherwise return false

         var valueToCheck = this.value === null ? value : this.value;
         var check;

         if (moment(valueToCheck).isValid() && moment(x).isValid()) {
            check = moment(valueToCheck).isAfter(x);
         } else {
            check = false;
         }

         if (!check) {
            this._pushError(Validator.errorTypes.DATE_SMALL);
         } else {
            this._removeError(Validator.errorTypes.DATE_SMALL);
         }

         return check;
      };
      Check.prototype.dateLesserThan = function(x, value) {
         // Only check if this.value/value and x are valid dates
         // Otherwise return false

         var valueToCheck = this.value === null ? value : this.value;
         var check;

         if (moment(valueToCheck).isValid() && moment(x).isValid()) {
            check = moment(valueToCheck).isBefore(x);
         } else {
            check = false;
         }

         if (!check) {
            this._pushError(Validator.errorTypes.DATE_SMALL);
         } else {
            this._removeError(Validator.errorTypes.DATE_SMALL);
         }

         return check;
      };

      Check.prototype.isNumber = function(value) {
         var check = isNaN(this.value || value);

         if (check) {
            this._pushError(Validator.errorTypes.NO_NUMBER);
         } else {
            this._removeError(Validator.errorTypes.NO_NUMBER);
         }

         return !check;
      };

      Check.prototype.numberGreaterThan = function(x, value) {
         var check = parseInt(this.value || value) > x && !isNaN(this.value || value);

         if (!check) {
            this._pushError(Validator.errorTypes.NUMBER_SMALL);
         } else {
            this._removeError(Validator.errorTypes.NUMBER_SMALL);
         }

         return check;
      };
      Check.prototype.numberLesserThan = function(x, value) {
	var check = parseInt(this.value || value) < x && !isNaN(this.value || value);

	if (!check) {
	    this._pushError(Validator.errorTypes.NUMBER_SMALL);
	} else {
	    this._removeError(Validator.errorTypes.NUMBER_SMALL);
	}

	return check;
      };
      Check.prototype.checkDuplicateKey = function(keyFields, existingData) {
         var check = true;
         var rowCheck = true;
         var existingRow, key, rowSame;
         var currentRow = this.value;
         for (var i in existingData) {
            rowSame = true;
            existingRow = existingData[i];
            for (var j in keyFields) {
               key = keyFields[j];
               if (existingRow[key] !== currentRow[key]) {
                  rowSame = false;
               }
            }
            if (rowSame) {
               rowCheck = false;
               break;
            }

         }
         if (!rowCheck) {
            this._pushError(Validator.errorTypes.DUPLICATE_ERROR);
         } else {
            this._removeError(Validator.errorTypes.DUPLICATE_ERROR);
         }
      };
      Check.prototype.custom = function(fn) {
         var check = fn(this.value);

         if (!check) {
            this._pushError(Validator.errorTypes.CUSTOM)
         } else {
            this._removeError(Validator.errorTypes.CUSTOM);
         }

         return check;
      };

      Check.prototype._pushError = function(errorType) {
         var that = this;
         // Check if property and errorType combination already exists
         // -> NO push the error

         var existingErrors = this.errors.filter(function(error) {
            return error.property === that.property && error.type === errorType;
         });

         if (existingErrors.length === 0) {
            this.errors.push({property: this.property, type: errorType});
         }

      };

      Check.prototype._removeError = function(errorType) {
         var that = this;

         var filteredErrors = this.errors.filter(function(error) {
            return error.property !== that.property && error.type !== errorType;
         });

         this.errors = filteredErrors;
      };

      var Validator = Validator;

      return Validator;
   }
);