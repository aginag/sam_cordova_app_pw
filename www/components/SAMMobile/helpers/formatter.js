sap.ui.define(["sap/ui/core/format/DateFormat"],

   function(DateFormat) {
      "use strict";
      return {

         formatToPriorityText: function(prioId, prioType) {
            var priorityTypes = this.getOwnerComponent().getModel("customizingModel").oData.PriorityTypes;
            if (!priorityTypes) {
               return '';
            }
				if (prioId === undefined || prioType === undefined) {
					return "";
            }
            for (var i = 0; i < priorityTypes.length; i++) {
               var type = (prioType.length > 2) ? prioType.substring(0, 2) : prioType;
               var prioObj = priorityTypes[i];
               if (prioObj.PRIOTYPE === type && prioObj.PRIORITY === prioId) {
                  var prioText = prioObj.PRIORITY_DESC;

                  if (prioText !== undefined) {
                     if (prioText.split("-").length > 1) {
                        var split = prioText.split("-");
                        if (jQuery.isNumeric(split[0])) {
                           return split[1];
                        } else {
                           return prioText;
                        }
                     } else if (prioText.split(":").length > 1) {
                        var split = prioText.split(":");
                        if (jQuery.isNumeric(split[0])) {
                           return split[1];
                        } else {
                           return prioText;
                        }
                     } else {
                        return prioText;
                     }
                  } else {
                     return prioId;
                  }

               }
            }

            return "";

         },

         formatPriorityText: function(text) {
            var prioText = text;

            if (prioText !== undefined) {
               if (prioText.split("-").length > 1) {
                  var split = prioText.split("-");
                  if (jQuery.isNumeric(split[0])) {
                     return split[1];
                  } else {
                     return prioText;
                  }
               } else if (prioText.split(":").length > 1) {
                  var split = prioText.split(":");
                  if (jQuery.isNumeric(split[0])) {
                     return split[1];
                  } else {
                     return prioText;
                  }
               } else {
                  return prioText;
               }
            } else {
               return prioText;
            }

         },

         formatToOrderTypeText: function(orderTypeId) {
            var orderTypes = this.getOwnerComponent().getModel("customizingModel").oData.OrderTypes;
            if (!orderTypes) {
               return '';
            }
            for (var i = 0; i < orderTypes.length; i++) {
               var typeObj = orderTypes[i];
               if (typeObj.AUART === orderTypeId) {
                  return typeObj.TXT;
               }
            }

            return "N/A";
         },
         formatToNotifTypeText: function(notificationTypeId) {
            var notificationTypes = this.getOwnerComponent().getModel("customizingModel").oData.NotificationType;
            if (!notificationTypes) {
               return '';
            }
            for (var i = 0; i < notificationTypes.length; i++) {
               var typeObj = notificationTypes[i];
               if (typeObj.QMART === notificationTypeId) {
                  return typeObj.QMARTX;
               }
            }

            return "N/A";
         },

         formatToActivityTypeText: function(orderType, actTypeId) {
            var activityTypes = this.getOwnerComponent().getModel("customizingModel").oData.PMActivityTypesMl;
            if (!activityTypes) {
               return '';
            }
				if (orderType === undefined || actTypeId === undefined) {
					return "";
            }
            for (var i = 0; i < activityTypes.length; i++) {
               var type = activityTypes[i];
               if (type.AUART === orderType && type.ILART === actTypeId) {
                  return type.ILATX;
               }
            }

            return "";
         },

         formatToActivityTypeTcText: function(lstar) {
            var activityTypes = this.getOwnerComponent().getModel("customizingModel").oData.ActivityTypeMl;
            if (!activityTypes) {
               return '';
            }

            for (var i = 0; i < activityTypes.length; i++) {
               var type = activityTypes[i];
               if (type.LSTAR === lstar) {
                  return type.KTEXT;
               }
            }

            return "";
         },

         formatToUnitMeasurementText: function(uom) {
            var unitMeasurement = this.getOwnerComponent().getModel("customizingModel").oData.MeasurementUnit;
            if (!unitMeasurement) {
               return '';
            }

            for (var i = 0; i < unitMeasurement.length; i++) {
               var unit = unitMeasurement[i];
               if (unit.MSEH3 === uom) {
                  return unit.MSEHT;
               }
            }

            return uom;
         },

         formatToAccIndicatorText: function(bemot) {
            var accIndicators = this.getOwnerComponent().getModel("customizingModel").oData.AccountingIndicMl;
            if (!accIndicators) {
               return '';
            }
            for (var i = 0; i < accIndicators.length; i++) {
               var accInd = accIndicators[i];
               if (accInd.BEMOT === bemot) {
                  return accInd.BEMOT_TXT;
               }
            }

            return "";
         },

         formatToPlantText: function(plantId) {
            var plants = this.getOwnerComponent().getModel("customizingModel").oData.PlantMl;
            if (!plants) {
               return '';
            }
            for (var i = 0; i < plants.length; i++) {
               var plant = plants[i];
               if (plant.PLANT === plantId) {
                  return plant.TXTMD;
               }
            }

            return "";
         },

         formatToStorageLocText: function(plantId, stgeLocId) {
            var context = this instanceof sap.ui.core.UIComponent ? this : this.getOwnerComponent();
            var stgeLocs = context.getModel("customizingModel").oData.StorageLocation;
            if (!stgeLocs) {
               return '';
            }
            for (var i = 0; i < stgeLocs.length; i++) {
               var stgeLoc = stgeLocs[i];
               if (stgeLoc.PLANT === plantId && stgeLoc.STGE_LOC === stgeLocId) {
                  return stgeLoc.TEXT;
               }
            }

            return stgeLocId;
         },

         formatToControlKeyText: function(controlKey) {
            var controlKeyTexts = this.getOwnerComponent().getModel("customizingModel").oData.ControlKeyMl;
            if (!controlKeyTexts) {
               return '';
            }
            for (var i = 0; i < controlKeyTexts.length; i++) {
               var text = controlKeyTexts[i];
               if (text.CONTROL_KEY === controlKey) {
                  return text.CONTROL_KEY_DESC;
               }
            }

            return "";
         },

         formatToWorkCenterText: function(workCtrKey) {
            var workCenter = this.getOwnerComponent().getModel("customizingModel").oData.WorkCenterMl;
            if (!workCenter) {
               return '';
            }
            for (var i = 0; i < workCenter.length; i++) {
               var center = workCenter[i];
               if (center.ARBPL === workCtrKey) {
                  return center.KTEXT;
               }
            }

            return "";
         },
         formatToWorkCenter: function(component, workCtrId) {
             var workCenter = component.getModel("customizingModel").oData.WorkCenterMl;
             if (!workCenter) {
                return '';
             }
             for (var i = 0; i < workCenter.length; i++) {
                var center = workCenter[i];
                if (center.OBJID === workCtrId) {
                   return center.ARBPL;
                }
             }

             return "";
          },
         formatUserStatus: function(userStatus, statusProfile, language) {
            var userStatusList = this.getOwnerComponent().getModel("customizingModel").oData.UserStatusMl;
            if (!userStatusList) {
               return '';
            }
            for (var i = 0; i < userStatusList.length; i++) {
               var status = userStatusList[i];
               if (status.USER_STATUS === userStatus && status.STATUS_PROFILE === statusProfile && status.SPRAS === language) {
                  return status.USER_STATUS_DESC;
               }
            }

            return "";
         },

         formatUserStatusCodeToText: function(statusCode, statusProfile) {
            var userStatusList = this.getOwnerComponent().getModel("customizingModel").oData.UserStatusMl;
            if (!userStatusList) {
               return '';
            }
            if (statusCode === undefined || statusProfile === undefined) {
               return "";
            }

            for (var i = 0; i < userStatusList.length; i++) {
               var status = userStatusList[i];
               if (status.USER_STATUS_CODE === statusCode && status.STATUS_PROFILE === statusProfile) {
                  return status.USER_STATUS_DESC;
               }
            }

            return "";
         },

         formatUserStatusCodeToStatus: function(statusCode, statusProfile) {
            var userStatusList = this.getOwnerComponent().getModel("customizingModel").oData.UserStatusMl;
            if (!userStatusList) {
               return '';
            }
            if (statusCode === undefined || statusProfile === undefined) {
               return "";
            }

            for (var i = 0; i < userStatusList.length; i++) {
               var status = userStatusList[i];
               if (status.USER_STATUS_CODE === statusCode && status.STATUS_PROFILE === statusProfile) {
                  return status.USER_STATUS;
               }
            }

            return "";
         },

         formatAddressToText: function(street, city, post_code, country) {
            var addressString = (city === "" || post_code === "") ? post_code + city : post_code + " - " + city;
            addressString = (street === "" || addressString === "") ? street + addressString : street + ", " + addressString;
            addressString = (addressString === "" || country === "") ? addressString + country : addressString + ", " + country;
            return addressString;
         },

         formatToEquiCategoryText: function(equiType) {
            var categories = this.getOwnerComponent().getModel("customizingModel").oData.EquipmentCategoryMl;
            if (!categories) {
               return '';
            }
            for (var i = 0; i < categories.length; i++) {
               var category = categories[i];
               if (category.EQTYP === equiType) {
                  return category.TYPTX;
               } else if (category.FLTYP === equiType) {
                  return category.TYPTX;
               }
            }

            return "N/A";
         },

         formatToWarrantyCategoryText: function(catId) {
            var categories = this.getOwnerComponent().getModel("customizingModel").oData.WarrantyCategoryMl;
            if (!categories) {
               return '';
            }
            for (var i = 0; i < categories.length; i++) {
               var category = categories[i];
               if (category.GATYP === catId) {
                  return category.KTEXT;
               }
            }

            return "";
         },

         formatToWarrantyTypeText: function(warrType) {
            var types = this.getOwnerComponent().getModel("customizingModel").oData.WarrantyTypesMl;
            if (!types) {
               return '';
            }
            for (var i = 0; i < types.length; i++) {
               var type = types[i];
               if (type.GAART === warrType) {
                  return type.KTEXT;
               }
            }

            return "";
         },

         formatToTechnicianName: function(persNo) {
            var users = this.getOwnerComponent().getModel("customizingModel").oData.SAMUsers;
            if (!users) {
               return '';
            }
            for (var i = 0; i < users.length; i++) {
               var user = users[i];

               if (user.employee_number === persNo) {
                  return user.first_name + " " + user.last_name;
               }
            }

            return persNo;
         },

         formatToMaterialQuantity: function(entryQnt) {
            var context = this instanceof sap.ui.core.UIComponent ? this : this.getOwnerComponent();
            return entryQnt;
         },

         formatToAvailableQuantity: function(materialNo) {
            var context = this instanceof sap.ui.core.UIComponent ? this : this.getOwnerComponent();
            var materials = context.getModel("inventoryModel").oData;
            if (!materials) {
               return '';
            }
            for (var i = 0; i < materials.length; i++) {
               var material = materials[i];
               var condition1 = isNaN(materialNo) ? material.MATERIAL : this.formatter.removeLeadingZeros(material.MATERIAL);
               var condition2 = isNaN(materialNo) ? materialNo : parseInt(materialNo);

               if (condition1 === condition2) {
                  return material.ENTRY_QNT;
               }
            }

            return "0.000";
         },

         formatQuantityToUIText: function(qnt) {
            if (isNaN(qnt)) {
               return qnt;
            } else {
               return qnt === "" ? "" : parseFloat(qnt);
            }
         },

         formatQuantityToCommaSep: function(qnt){
          if (isNaN(qnt)) {
               return qnt;
            } else {
              
              qnt === "" ? "" : qnt = parseFloat(qnt).toString();
              qnt = qnt.replace(".", ',');
              return qnt;
            }
         },

         formatToCommaSep: function(qnt){
            if (isNaN(qnt)) {
                 return qnt;
              } else {
                
                qnt = qnt.replace(".", ',');
                return qnt;
              }
           },

         formatMeasDocReadingToUIText: function(reading) {
           var uiReading = reading.split(".");
            uiReading.pop();

            return uiReading.join(".");
         },

         formatToComponentStatus: function(component) {
            var requiredQty = parseInt(component.REQUIREMENT_QUANTITY);
            component._fieldControl = {
               icon: "",
               iconColor: ""
            };

            var availableQty = parseInt(component._availableQty);

            if (requiredQty > availableQty) {
               component._fieldControl.iconColor = "red";
               component._fieldControl.icon = "sap-icon://error";
            } else if (requiredQty === availableQty) {
               component._fieldControl.iconColor = "#fbb100";
               component._fieldControl.icon = "sap-icon://message-warning";
            } else {
               component._fieldControl.iconColor = "green";
               component._fieldControl.icon = "sap-icon://accept";
            }
         },

         formatBackendDateToUIDate: function(backendDate) {
            return backendDate ? backendDate.substr(0, 10) : "";
         },
         formatBackendDateToUIDateTime: function(backendDate) {
            return backendDate ? backendDate.substr(0, 15) : "";
         },

         formatBackendDateToShortText: function(backendDate) {
             var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
             var oLocale = sap.ui.getCore().getConfiguration().getFormatSettings().getFormatLocale();
             var date = moment(backendDate).lang(oLocale.getLanguage());
             
            var todayNative = new Date();
            var yesterday = moment(todayNative).subtract(1, "day");
            var tomorrow = moment(todayNative).add(1, "day");

            //Process
            // Check if date was/is
            // Today, Yesterday, Tomorrow or in the next calendar year
            // Format appropriateley and return

            if (date.isSame(todayNative, "day")) {
               return oBundle.getText('today');
            } else if (date.isSame(yesterday, "day")) {
               return oBundle.getText('yesterday');
            } else if (date.isSame(tomorrow, "day")) {
               return oBundle.getText('tomorrow');
            } else if (!date.isSame(todayNative, "year")) {
               return date.format("dddd, D MMM YYYY");
            } else {
               return date.format("dddd, D MMM");
            }

         },

         formatUIDateToBackendDate: function(uiDate) {
            return oDateFormat2.format(oDateFormat.parse(uiDate));
         },

         formatCombinedDateAndTime: function(date, time) {
            return this.formatter.formatBackendDateToUIDate(date) + " " + this.formatter.formatBackendTimeToUiTime(time);
         },
         formatBackendTimeToUiTime: function(backendTime) {
            return backendTime ? backendTime.substr(0, 5) : "";
         },

         formatObjectListItem: function(object) {
            var fieldControl = {
               woLabelVisible: false,
               notifLabelVisible: false,
               eqpmtLabelVisible: false,
               fLocLabelVisible: false,
               objectType: ''
            };

            object._fieldControl = fieldControl;

            if (object.NOTIF_NO) {
               object._objectHeaderTxt = object.SHORT_TEXT;
               object._fieldControl.notifLabelVisible = true;
               object._fieldControl.objectType = 'notification';
               return "sap-icon://notification-2";
            } else if (object.EQUIPMENT) {
               object._objectHeaderTxt = object.EQUIDESCR;
               object._fieldControl.eqpmtLabelVisible = true;
               object._fieldControl.objectType = 'equipment';
               return "sap-icon://machine";
            } else if (object.FUNCT_LOC) {
               object._objectHeaderTxt = object.FUNCLDESCR;
               object._fieldControl.fLocLabelVisible = true;
               object._fieldControl.objectType = 'funcLoc';
               return "sap-icon://functional-location";
            } else {
               return "";
            }
         },
         formatToObjectTypeText: function(EQART) {
             var objectTypes = this.getOwnerComponent().getModel("customizingModel").oData.ObjectTypeMl;

             if (objectTypes === undefined) return EQART;

             for (var i = 0; i < objectTypes.length; i++) {
                var objectType = objectTypes[i];

                if (objectType.EQART === EQART) {
                   return objectType.EARTX;
                }
             }

             return EQART;
          },
          formatToCountryCodeText: function(cCode) {
              var countryCodes = this.getOwnerComponent().getModel("customizingModel").oData.CountryCodesMl;

              if (countryCodes === undefined) return cCode;

              for (var i = 0; i < countryCodes.length; i++) {
                 var countryCode = countryCodes[i];

                 if (countryCode.LAND1 === cCode) {
                    return countryCode.LANDX;
                 }
              }

              return cCode;
           },

          formatToCatalogProfileText: function(CAT_PROFILE) {
              var catalogProfiles = this.getOwnerComponent().getModel("customizingModel").oData.CatalogProfile;

              if (catalogProfiles === undefined) return CAT_PROFILE;

              for (var i = 0; i < catalogProfiles.length; i++) {
                 var catalogProfile = catalogProfiles[i];

                 if (catalogProfile.CAT_PROFILE === CAT_PROFILE) {
                    return catalogProfile.RBNRX === "" ? CAT_PROFILE : catalogProfile.RBNRX;
                 }
              }

              return CAT_PROFILE;
           },

         formatAttachmentTypeToIcon: function(type) {

            if (!type) {
               return "sap-icon://attachment";
            }

            switch (type.toLowerCase()) {
               case "jpg":
                  return "sap-icon://attachment-photo";
                  break;
               case "jpeg":
                  return "sap-icon://attachment-photo";
                  break;
               case "png":
                  return "sap-icon://attachment-photo";
                  break;
               case "pdf":
                  return "sap-icon://pdf-attachment";
                  break;
               case "txt":
                  return "sap-icon://attachment-text-file";
                  break;
               case "mov":
                  return "sap-icon://attachment-video";
                  break;
               case "wav":
                  return "sap-icon://attachment-audio";
                  break;
               case "url":
                  return "sap-icon://internet-browser";
                  break;
               default:
                  return "sap-icon://attachment";
                  break;
            }
         },

         numberToString: function(number) {
            return number.toString();
         },

         getObjectType: function(object) {
            if (object.EQUIPMENT && object.NOTIF_NO) {
               return "equipment";
            } else if (object.NOTIF_NO) {
               return "notification";
            } else if (object.FUNCT_LOC) {
               return "funcLoc";
            } else {
               return "";
            }
         },

         getPartnerByKey: function(object) {
            var partners = this.getOwnerComponent().getModel("partnersModel").oData;
            if (!partners) {
               return '';
            }
            for (var i = 0; i < partners.length; i++) {
               var partner = partners[i];
               if (partner.PARTNER_KEY === object.PARTNER_KEY) {
                  return partner;
               }
            }

            return {};
         },

         formatCheckListTaskIcon: function(status) {
            switch (status) {
               case "1":
                  return "#b6d7a8";
                  break;
               case "2":
                  return "#ea9999";
                  break;
               case "3":
                  return "#ffe599";
                  break;
               default:
                  return "black";
                  break;
            }
         },

         formatMsToTimeText: function(durationMs) {
            var secondInMs = 1000;
            var minuteInMs = secondInMs * 60;
            var hourInMs = minuteInMs * 60;

            if (durationMs < secondInMs) {
               return durationMs + "ms";
            } else if (durationMs < minuteInMs) {
               var seconds = Math.floor(durationMs / secondInMs);

               return seconds + "s";
            } else if (durationMs < hourInMs) {
               var minutes = Math.floor(durationMs / minuteInMs);
               var seconds = Math.round((durationMs / secondInMs) % 60);

               return minutes + "m " + seconds + "s";
            } else if (durationMs >= hourInMs) {
               var hours = Math.floor(durationMs / hourInMs);
               var minutes = Math.round((durationMs / minuteInMs) % 60);

               return hours + "h " + minutes + "m";
            } else {
               return durationMs + "ms";
            }
         },
         formatSecToTimeText: function(durationSec) {
            var durationSec = parseInt(durationSec, 10);
            var hours = Math.floor(durationSec / 3600);
            var minutes = Math.floor((durationSec - (hours * 3600)) / 60);
            var seconds = durationSec - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
               hours = "0" + hours;
            }
            if (minutes < 10) {
               minutes = "0" + minutes;
            }
            if (seconds < 10) {
               seconds = "0" + seconds;
            }
            return hours + ':' + minutes + ':' + seconds + '  ';

         },

         removeLeadingZeros: function(number) {
            if (number !== "" && number !== undefined && typeof number !== "number" && number !== null) {
               return isNaN(number) ? number : parseInt(number, 10);
            }

            return number;
         },

         removePostZeros: function(number){
            return Math.abs(number);
         },

            formatDate: function (date) {
                if (moment(date).isValid()) {
                    if (this.globalViewModel.model.getProperty("/language") === "D") {
                        return moment(date).format("DD.MM.YYYY");
                    } else {
                        return moment(date).format("YYYY/MM/DD");
                    }
                } else {
                    return "";
                }
            },

            formatTime: function (time) {
                if (moment(time).isValid()) {
                    return moment(time).format("HH:mm");
                } else {
                    return "";
                }
            },

            formatIdWithDescription: function (sId, sDescription) {
                var sIdWOLeadingZeros = this.removeLeadingZeros(sId);
                return sDescription ? sIdWOLeadingZeros + " - " + sDescription : sIdWOLeadingZeros;
            },

            addLeadingZeroes: function (template, number) {
                return template.substring(0, template.length - number.length) + number;
            },
            getImageForSeverity: function (status) {
                switch (status) {
                    case 'E':
                        return 'sap-icon://message-error';
                    case 'W':
                        return 'sap-icon://message-warning';
                    case 'S':
                        return 'sap-icon://message-success';
                }
            },

            formatUIusername: function (firstName, lastName) {
                if (sap.ui.Device.system.phone) {
                    return firstName.charAt(0) + ". " + lastName.charAt(0) + ".";
                } else {
                    return firstName + " " + lastName;
                }
            },

            formatToAppName: function (appId) {
                return launchnavigator.getAppDisplayName(appId);
            },

            setVisibileOnPhone: function (oEvent) {
                return sap.ui.Device.system.phone;
            },

            setInvisibileOnPhone: function (oEvent) {
                return !sap.ui.Device.system.phone;
            },

            formatDateTime(dateTime) {
                if (this.globalViewModel.model.getProperty("/language") === "D") {
                    return moment(dateTime).format("DD.MM.YYYY HH:mm");
                } else {
                    return moment(dateTime).format("YYYY/MM/DD HH:mm");
                }
            },

            twoDecimals(number) {
                return parseFloat(number).toFixed(2);
            },

            withoutDezimals(number, placeholder) {
               if(number){
                  var decimal = 0;
                  if(placeholder && placeholder.includes(".")){
                     decimal = (placeholder.length - 1) - (placeholder.indexOf("."));
                  } else if(placeholder && placeholder.includes(",")){
                     decimal = (placeholder.length - 1) - (placeholder.indexOf(","));
                 }                 
                  var newNumber = parseFloat(number).toFixed(decimal);
                  return newNumber;
               }
            },

            availableState: function (sStateValue) {

                if (sStateValue == null) {
                    return 8;
                }
                var sStateValueToLower = sStateValue.toLowerCase();

                switch (sStateValueToLower) {
                    case "accept":
                        return 8;
                    case "reject":
                        return 3;
                    case "stop":
                        return 3;
                    case "travel":
                        return 5;
                    default:
                        return 7;
                }
            },

         formatCodeAndDescription: function (sCode, sDescription) {
            return sCode ? sCode + " - " + sDescription : "";
         },

         formatOperationRejectionMessage: function (sTokenText, sRejectionText) {
            return sTokenText + "\n\n" + sRejectionText;
         },

         formatOperationTitle: function (sActionFlag, sActivity, sSubActivity, sDescription) {
            var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
            var sSpacingText = " - ";
            if (sSubActivity) {
               sSpacingText =  "/" + sSubActivity + sSpacingText;
            }

            if (sActionFlag === "N") {
               return oBundle.getText("WOLOCAL") + sSpacingText + sDescription;
            } else
               return sActivity + sSpacingText + sDescription;
         },

         formatCharacsValueText: function(characValue) {

            if (characValue === undefined) {
               return "";
            }

            if (characValue.ATWTB != "") {
               return characValue.ATWTB;
            }

            if (characValue.ATFLB != '0') {
               return characValue.ATFLV + " - " + characValue.ATFLB;
            }
            return characValue.ATFLV ;

         },

         formatDisplayCodeGrpDescription: function(codeGrp, Description) {
            if(codeGrp && Description){
               return codeGrp + " - " + Description;
            }
            return codeGrp;
         },

         formatDecimalVal: function(value) {
            return parseFloat(value).toFixed(2);
         },

         formatItemTitle: function(actionFlag, ITEM_KEY){
            var oBundle = sap.ui.getCore().byId("samComponent").getComponentInstance().getModel("i18n").getResourceBundle();
            if(actionFlag === 'N'){
               return oBundle.getText("WOLOCAL");
            } else {
               return ITEM_KEY;
            }
         },

         formatBytesToSize: function(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (isNaN(bytes)) {
                return bytes;
            }
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        },

      };
   });
