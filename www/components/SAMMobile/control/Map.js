sap.ui.define(["sap/ui/core/Control"],

    function (Control) {
        return Control.extend("SAMMobile.control.Map", {
            metadata: {
                properties: {
                    marker: {type: "object", defaultValue: {}},
                    customMarker: {type: "object", defaultValue: {}},
                },
                events: {
                    navigate: {
                        parameters: {
                            markerObject: {type: "object"},
                            event: {type: "object"}
                        }
                    }
                }
            },

            onAfterRendering: function () {
                this._drawMap();
            },

            _drawMap: function () {

                var that = this;

                // Properties
                var marker = this.getProperty("marker");
                var currentPositionMarker = this.getProperty("customMarker");

                // set service
                var platform = new H.service.Platform({
                    app_id: 'devportal-demo-20180625',
                    app_code: '9v2BkviRwi9Ot26kp2IysQ',
                    useHTTPS: true
                });

                // set the defaultLayer
                var pixelRatio = window.devicePixelRatio || 1;
                var defaultLayers = platform.createDefaultLayers({
                    tileSize: pixelRatio === 1 ? 256 : 512,
                    ppi: pixelRatio === 1 ? undefined : 320
                });

                var map = new H.Map(document.getElementById(this.getId()),
                    defaultLayers.normal.map, {
                        center: {lat: 48.8567, lng: 2.3508},
                        zoom: 6,
                        pixelRatio: pixelRatio
                    });

                // MapEvents enables the event system
                // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
                var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

                // Create the default UI components
                var ui = H.ui.UI.createDefault(map, defaultLayers);

                var auftragIcon = new H.map.Icon('img/Auftrag.svg', {size: {w: 50, h: 50}});

                // add customMarker for currentPosition
                // Get location of user
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        that.setProperty("customMarker", {
                            "lat": position.coords.latitude,
                            "lng": position.coords.longitude
                        });
                    });
                    if (currentPositionMarker.lat !== undefined && currentPositionMarker.lng) {
                        var icon = new H.map.Icon('img/Ressource.svg', {size: {w: 50, h: 50}});
                        var customMarker = new H.map.Marker({
                            lat: currentPositionMarker.lat,
                            lng: currentPositionMarker.lng
                        }, {icon: icon});

                        customMarker.addEventListener("tap", that._onMapMarkerButtonClick.bind(
                            that, {
                                SHTXT: "",
                                ORDERID: 0,
                                EQUIPMENT: "",
                                POST_CODE1: "",
                                STREET: ""
                            }));

                        map.addObject(customMarker);
                    }
                }

                var group = new H.map.Group();

                var dataPoints = [];

                // set each marker to the group

                marker.forEach(function (mapMarker) {
                    if (mapMarker.LATITUDE !== "" && mapMarker.LONGITUDE !== "") {

                        dataPoints.push(new H.clustering.DataPoint(parseFloat(mapMarker.LATITUDE), parseFloat(mapMarker.LONGITUDE), null, mapMarker));

                        marker = new H.map.Marker({
                            lat: parseFloat(mapMarker.LATITUDE),
                            lng: parseFloat(mapMarker.LONGITUDE)
                        }, {icon: auftragIcon});

                        group.addObject(marker);
                    }
                });

                // Create a clustered data provider and a theme implementation
                var clusteredDataProvider = new H.clustering.Provider(dataPoints, {
                    clusteringOptions: {
                        // Maximum radius of the neighborhood
                        eps: 128,
                        // minimum weight of points required to form a cluster
                        minWeight: 2
                    },
                    theme: {
                        getClusterPresentation: function (cluster) {
                            var clusterSvgTemplate = that._getAuftragSvgMarker(cluster.getWeight());

                            // Use cluster weight to change the icon size
                            var weight = cluster.getWeight(),
                                // Calculate circle size
                                radius = 50,
                                diameter = 50,

                                // Create an icon
                                // Note that we create a different icon depending from the weight of the cluster
                                clusterIcon = new H.map.Icon(clusterSvgTemplate, {
                                    size: {w: diameter, h: diameter},
                                    anchor: {x: radius, y: radius},
                                }),

                                // Create a marker for the cluster
                                clusterMarker = new H.map.Marker(cluster.getPosition(), {
                                    icon: clusterIcon,

                                    // Set min/max zoom with values from the cluster, otherwise
                                    // clusters will be shown at all zoom levels
                                    min: cluster.getMinZoom(),
                                    max: cluster.getMaxZoom()
                                });

                            // Bind cluster data to the marker
                            clusterMarker.setData(cluster);

                            return clusterMarker;
                        },
                        getNoisePresentation: function (noisePoint) {
                            // Create a marker for noise points:
                            var noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
                                icon: auftragIcon,

                                // Use min zoom from a noise point to show it correctly at certain zoom levels
                                min: noisePoint.getMinZoom()
                            });

                            noiseMarker.addEventListener("tap", that._onMapMarkerButtonClick.bind(that, noisePoint));

                            // Bind noise point data to the marker:
                            noiseMarker.setData(noisePoint);
                            return noiseMarker;
                        }
                    }
                });

                // Create a layer that includes the data provider and its data points:
                var layer = new H.map.layer.ObjectLayer(clusteredDataProvider);

                // Add the layer to the map:
                map.addLayer(layer);

                clusteredDataProvider.addEventListener('tap', function (event) {
                    // Log data bound to the marker that has been tapped:
                    that.fireEvent("navigate", {
                        markerObject: event.target.getData(),
                        event: event
                    });
                });

                // get geo bounding box for the group and set it to the map
                if (group.getBounds()) {
                    map.setViewBounds(group.getBounds());
                }

            },

            // set the weight off equipments (counter in svg)
            _getAuftragSvgMarker(weight) {
                return "<svg viewBox='-7.684410095214844 -8.196708679199219 300.2049255371094 500' width='$WIDTH$' height='$HEIGHT$'" +
                    " xmlns='http://www.w3.org/2000/svg'>" +
                    "<path d='M 141.987 2.653 C 62.814 2.653 -1.365 66.831 -1.365 146.005 C -1.365 167.269 4.459 191.67 14.306 210.516 L 15.577 212.8 L 135.224 427.754 C 135.224 427.754 137.15 432.746 141.987 432.746 C 146.588 432.746 148.75 427.754 148.75 427.754 L 269.062 211.594 C 279.697 191.504 285.339 167.269 285.339 146.005 C 285.339 66.831 221.155 2.653 141.987 2.653 Z' " +
                    " fill='#e2101d' stroke='#323638' stroke-linecap='round' stroke-linejoin='round' stroke-miterlimit='10' " +
                    " stroke-width='2' />" +
                    "<ellipse cx='141.987' cy='146.13' fill='#ffffff' rx='110.981' ry='109.635' stroke='#323638' " +
                    " stroke-linecap='round' stroke-linejoin='round' stroke-miterlimit='10' stroke-width='2'/>" +
                    "<path d='M 279.016 431.158 C 279.016 454.723 223.761 479.957 141.504 479.957 C 59.247 479.957 3.993 454.7233.993 431.158 C 3.993 412.892 35.479 395.608 82.056 387.375 L 76.548 377.481 C 25.413 387.15 -6.733 407.444 -6.733 431.158 C -6.728 464.538 58.384 490.682 141.504 490.682 C 224.625 490.682 289.742 464.538 289.742 431.158 C 289.742 407.637 257.923 387.402 207.324 377.652 L 201.822 387.536 C 247.879 395.833 279.016 413.064 279.016 431.158 Z' " +
                    " fill='#323638'/>" +
                    "<text x='135' y='200' text-anchor='middle' fill='#$TEXT_COLOR$' " +
                    "style='font-family: impact; font-size:150px;'>" + weight + "</text>" +
                    "</svg>";
            },

            _onMapMarkerButtonClick: function (evt, mapMarker) {
                this.fireEvent("navigate", {
                    singleMarkerObject: mapMarker.target.getData(),
                    event: evt
                });
            },

            renderer: function (oRM, oControl) {
                oRM.write("<div");
                oRM.writeControlData(oControl);
                oRM.addStyle('width', '100%');
                oRM.addStyle('height', '100%');
                oRM.addStyle('background', 'grey');
                oRM.writeStyles();
                oRM.writeClasses();
                oRM.write(">");
                oRM.write("</div>");
            }
        });
    });