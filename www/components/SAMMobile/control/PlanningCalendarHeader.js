sap.ui.define([ "sap/m/PlanningCalendarHeader" ],

    function(PlanningCalendarHeader) {
        return PlanningCalendarHeader.extend("SAMMobile.control.PlanningCalendarHeader", {
            metadata : {
                properties : {}
            },
            renderer : {},

            setPickerText: function (sText) {
                var formattedText = sText;

                if (sap.ui.Device.system.phone) {
                    var startDate = this.getStartDate();
                    var localeMoment = moment(startDate);
                    localeMoment.locale(window.navigator.language);

                    var dateFormat = this.getParent().getDatePickerTextFormat();
                    formattedText = localeMoment.format(dateFormat);
                }

                return PlanningCalendarHeader.prototype.setPickerText.apply(this, [formattedText]);
            }
        });
    });