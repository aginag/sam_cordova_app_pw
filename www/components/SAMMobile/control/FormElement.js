sap.ui.define([
	"sap/ui/layout/form/FormElement"
 ], 
 
 function(FormElement) {
    return FormElement.extend("SAMMobile.control.FormElement", {
	metadata : {
	    properties : {
		mandatory : {
		    type : "boolean",
		    group:"Misc",
		    defaultValue : false
		}
	    }
	},
	setMandatory : function(value) {
		var that = this;
	    this.setProperty("mandatory", value, true);

	    setTimeout(function() {
            if (that._oLabel) {
                that._oLabel.setRequired(value);
            }
		}, 300);
	}
    });
});