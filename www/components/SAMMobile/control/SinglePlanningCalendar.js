sap.ui.define([ "sap/m/SinglePlanningCalendar", "SAMMobile/control/PlanningCalendarHeader", "sap/ui/core/InvisibleText" ],

    function(SinglePlanningCalendar, PlanningCalendarHeader, InvisibleText) {
        return SinglePlanningCalendar.extend("SAMMobile.control.SinglePlanningCalendar", {
            metadata : {
                properties : {
                    // This property will be used formatting the date with moment.format
                    // Phone only (sap.ui.Device.system.phone == true)
                    datePickerTextFormat: {type: "string", defaultValue: "DD, MM YYYY"},
                }
            },
            renderer : {},

            _createHeader: function () {
                var oHeader = new PlanningCalendarHeader(this.getId() + "-Header");

                oHeader.getAggregation("_actionsToolbar")
                    .addAriaLabelledBy(InvisibleText.getStaticId("sap.m", "SPC_ACTIONS_TOOLBAR"));

                oHeader.getAggregation("_navigationToolbar")
                    .addAriaLabelledBy(InvisibleText.getStaticId("sap.m", "SPC_NAVIGATION_TOOLBAR"));

                return oHeader;
            }
        });
    });