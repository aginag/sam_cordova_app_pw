sap.ui.define([ "sap/m/Panel" ],

function(Panel) {
    return Panel.extend("SAMMobile.control.Panel", {
	metadata : {
	    properties : {
		expandIcon : 'string'
	    }
	},
	renderer : {},
	onBeforeRendering : function() {
	    if (Panel.prototype.onBeforeRendering) {
		Panel.prototype.onBeforeRendering.apply(this, arguments);
	    }
	    if (this.oIconCollapsed && this.getExpandIcon()) {
		this.oIconCollapsed.setSrc(this.getExpandIcon());
	    }
	    if (this.clickEventSet == undefined) {
		if (this.getHeaderToolbar()) {
		    var hdrTitleAggregation = this.getHeaderToolbar().getAggregation("content");
		    var functionToggle = jQuery.proxy(this.togglePanel, this);
		    hdrTitleAggregation.forEach(function(hdrTitle){
			if(hdrTitle.getMetadata().getName() === "sap.m.Title"){
			    hdrTitle.attachBrowserEvent("click", functionToggle);
			}
		    });
		    
		}
		this.clickEventSet = true;
	    }

	},
	togglePanel : function() {
	    this.setExpanded(!this.getExpanded());
	}
    });
});
