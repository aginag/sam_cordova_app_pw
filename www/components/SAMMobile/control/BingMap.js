sap.ui.define(["sap/ui/core/Control"],

    function (Control) {
        return Control.extend("SAMMobile.control.Map", {
            metadata: {
                properties: {
                    marker: {type: "object", defaultValue: {}},
                    customMarker: {type: "object", defaultValue: {lat: 48.8567, lng: 2.3508}},
                },
                events: {
                    navigate: {
                        parameters: {
                            markerObject: {type: "object"},
                            event: {type: "object"}
                        }
                    }
                }
            },
            
            onAfterRendering: function () {
                this._drawMap();
            },

            _drawMap: function () {

            	var that = this;
            	var lat;
            	var lon;
                var aMarker = this.getProperty("marker");
                
                if (navigator.geolocation){
                	navigator.geolocation.getCurrentPosition(function(position){
                        that.setProperty("customMarker", {
                            "lat": position.coords.latitude,
                            "lng": position.coords.longitude
                        });
                	})
                }
                
                var customMarker = this.getProperty("customMarker");
                var oMap = new Microsoft.Maps.Map(document.getElementById(this.getId()), {
                    credentials: 'OufepAQIythR3eBxuPNO~R66rRuTy9csILDfE6kasaA~AnRdxtBGW-6O5_APGogHNI85rZxc71sMAdp5_F0ku4ykY-wwqtrPBXZqC-VXoV8j',
                    center: {latitude: customMarker.lat, longitude: customMarker.lng},
                    zoom: 10
                });
                
                var Locations = [];

                aMarker.forEach(function (oMapMarker) {
                    if (oMapMarker.latitude !== "" && oMapMarker.longitude !== "") {

                        var oPushpin = new Microsoft.Maps.Pushpin({
                            latitude: parseFloat(oMapMarker.longitude),
                            longitude: parseFloat(oMapMarker.latitude)
                        }, { text: '', title: oMapMarker.text, subTitle: oMapMarker.subTitle });

                      //Add mouse events to the pushpin.
                        
                        Microsoft.Maps.Events.addHandler(oPushpin, 'click', that._onMapMarkerButtonClick.bind(that, oMapMarker));		                        
                        Locations.push(oPushpin.getLocation());
                        oMap.entities.push(oPushpin);
                    }
                });
                
                if (Locations.length > 0){
                	var rect = Microsoft.Maps.LocationRect.fromLocations(Locations);
                	oMap.setView({ bounds: rect, padding: 80 , zoom: 10});
                }

            },
            
            _onMapMarkerButtonClick: function (mapMarker, evt) {
                this.fireEvent("navigate", {
                	markerObject: mapMarker,
                    event: evt
                });
            },
            
            renderer: function (oRM, oControl) {
                oRM.write("<div");
                oRM.writeControlData(oControl);
                oRM.addStyle('width', '100%');
                oRM.addStyle('height', '100%');
                oRM.addStyle('background', 'grey');
                oRM.writeStyles();
                oRM.writeClasses();
                oRM.write(">");
                oRM.write("</div>");
            }
        });
    });