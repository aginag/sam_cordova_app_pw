﻿//adds various quirks for windows, e.g. alert functionality, body height/scroll bar fix
//alert function for windows; piles alerts while alert box is being shown; 
//shows the next alert when closing the current alert box
if (window.navigator.appVersion.indexOf('Windows NT') >= 0) {
    var alertsToShow = [];
    var dialogVisible = false;

    function showPendingAlerts() {
        if (dialogVisible || !alertsToShow.length) {
            return;
        }

        dialogVisible = true;
        (new Windows.UI.Popups.MessageDialog(alertsToShow.shift())).showAsync().done(function () {
            dialogVisible = false;
            showPendingAlerts();
        })
    }
    window.alert = function (message) {
        if (window.console && window.console.log) {
            window.console.log(message);
        }

        alertsToShow.push(message);
        showPendingAlerts();
    }

    //scroll bar fix:
    document.addEventListener('DOMContentLoaded', function () {
        var body = document.body;
        body.style.overflow = "hidden";
        body.style.position = "fixed";
    });
}

//jquery quirks for windows 8 & 8.1; do not run this on windows 10 as it will fail
if (window.navigator.appVersion.indexOf('Windows NT 6.') >= 0) {
    var oldAppendChild = HTMLElement.prototype.appendChild;
    HTMLElement.prototype.appendChild = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldAppendChild.apply(self, args);
        });
    };

    var oldinsertBefore = HTMLElement.prototype.insertBefore;
    HTMLElement.prototype.insertBefore = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldinsertBefore.apply(self, args);
        });
    };

    var oldcreateElement = HTMLElement.prototype.createElement;
    HTMLElement.prototype.createElement = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldcreateElement.apply(self, args);

        });
    };

    var oldremoveChild = HTMLElement.prototype.removeChild;
    HTMLElement.prototype.removeChild = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldremoveChild.apply(self, args);
        });
    };

    var oldreplaceChild = HTMLElement.prototype.replaceChild;
    HTMLElement.prototype.replaceChild = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldreplaceChild.apply(self, args);
        });
    };

    var oldcloneNode = HTMLElement.prototype.cloneNode;
    HTMLElement.prototype.cloneNode = function () {
        var self = this;
        var args = arguments;
        return MSApp.execUnsafeLocalFunction(function () {
            return oldcloneNode.apply(self, args);
        });
    };
}