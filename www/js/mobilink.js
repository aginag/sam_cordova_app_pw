﻿var start = 0;
var image_path = "";
var pluginName = 'MscMobiLinkPlugIn';
var fileTransfers = [];
var dbFileName = "sam.udb";
var logFileName = "Logs.txt";
var syncObject = null;
var windowsPluginInstance = null;
var syncPhases = {
    'fileUpload': 'fileUpload',
    'dbSync': 'dbSync',
    'fileDownload': 'fileDownload'
};
var syncDbPhases = {
    'up': 'up',
    'down': 'down'
};
var syncProgressType = {
    fileUpload: 'fileUpload',
    fileDownload: 'fileDownload',
    dbSync: 'dbSync',
};

function handleOpenURL(url) {
    var objectUrl = {};
    objectUrl.url = url;

    window.SAM_APP_URL = url;

    setTimeout(function () {
        sap.ui.getCore().getEventBus().publish("appUrl", "newRequest", objectUrl);
    }, 0);
}

/**
 * note: for internal use only
 * expects following response object:
 * { fileId, status, fileSize, bytesTransferred, error }
 *
 * the execution of this method might be time critical
 * (during debugging the app crashed maybe due to too slow debugging)
 * @param {type} response
 */
function fileTransferListener(response) {
    var obj = tryParseJson(response);
    var fileTransfer = null;

    //find corresponding entry in file download queue
    for (var i = 0; i < fileTransfers.length; i++) {
        if (fileTransfers[i].fileId == obj.fileId) {
            fileTransfer = fileTransfers[i];
            break;
        }
    }

    if (fileTransfer != null) {
        fileTransfer.fileSize = parseFloat(obj.fileSize);
        fileTransfer.bytesTransferred = parseFloat(obj.bytesTransferred);
        fileTransfer.percent = Math.round((fileTransfer.bytesTransferred / fileTransfer.fileSize) * 100);
        fileTransfer.onProgress(fileTransfer);
    }
}

/**
 * note: for internal use only
 * @param {type} response: json string
 */
function syncListener(response) {
    var obj = tryParseJson(response);
    if (syncObject != null) {
        syncObject.onProgress(obj);
    }
}

function fileTransferInfo(transferType, fileId, fileName, successCallback, errorCallback, progressCallback, fileSize,
                          bytesTransferred, data) {
    this.finished = false;
    this.success = false;
    this.transferType = transferType;
    this.fileId = fileId;
    this.fileName = fileName;
    this.onSuccess = successCallback;
    this.onError = errorCallback;
    this.onProgress = progressCallback;
    this.fileSize = fileSize;
    this.percent = 0;
    this.bytesTransferred = bytesTransferred;
    this.data = data;
}

function syncProgressInfo(syncPhase, total, current, fileTransfers, state) {
    this.syncPhase = syncPhase;
    this.total = total;
    this.current = current;
    this.percent = Math.round((this.current / this.total) * 100);
    this.fileTransfers = fileTransfers;
    this.state = state;

    this.isUploading = false;
    this.isDownloading = false;

    if (this.state == "STATE_SENDING_HEADER" ||
        this.state == "STATE_SENDING_TABLE" ||
        this.state == "STATE_FINISHING_UPLOAD" ||
        this.state == "STATE_WAITING_FOR_UPLOAD_ACK" ||
        this.state == "STATE_PROCESSING_UPLOAD_ACK") {
        this.isUploading = true;
    }
}

function syncErrorInfo(syncPhase, error, obj) {
    this.syncPhase = syncPhase;
    this.error = error;
    this.obj = obj;
}

function executeQuery(sqlStatement, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeQuery', [sqlStatement]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeQuery', [sqlStatement]);
    }
}

/**
 * executes all sqlStatements;
 * calls successCallback when all sqlStatements completed successfully;
 * calls errorCallback if any sqlStatements fails
 */
function executeQueries(sqlStatementSet, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeQueries', [sqlStatementSet]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeQueries', [sqlStatementSet]);
    }
}

function executeQueriesBackground(sqlStatementSet, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeQueriesBackground', [sqlStatementSet]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeQueriesBackground', [sqlStatementSet]);
    }
}

function executeQueriesSequentially(sqlStatements, successCallback, errorCallback) {
    var startTime = new Date();
    var stmts = [];
    var results = {};
    for (var i in sqlStatements) {
        stmts.push(sqlStatements[i].statement);
    }

    //wait till all queries are done
    $.when.apply($, deferreds).then(
        function onSuccess(result) {
            successCallback(resultSet);
        },
        function onError(error) {
            errorCallback(error);
        }
    );
}

function executeUpdate(sqlStatement, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeUpdate', [sqlStatement]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeUpdate', [sqlStatement]);
    }
}

function RunTestPlugin(sqlStatement, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'runTest', [sqlStatement]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'runTest', [sqlStatement]);
    }
}

/**
 * executes all sqlStatements;
 * calls successCallback when all sqlStatements completed successfully;
 * calls errorCallback if any sqlStatements fails
 */
function executeUpdates(sqlStatements, successCallback, errorCallback) {
    var executeUpdatePromise = function(sql) {
        return new Promise(function(resolve, reject) {
            executeUpdate(sql, resolve, reject);
        });
    };

    Promise.all(sqlStatements.map(function(sqlStatement) {
        return executeUpdatePromise(sqlStatement);
    })).then(function() {
        successCallback();
    }).catch(function(err) {
        errorCallback(err);
    });
}

/**
 * executes all sqlStatements;
 * calls successCallback when all sqlStatements completed successfully;
 * calls errorCallback if any sqlStatements fails
 */
function executeUpdatesSequentially(sqlStatements, successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, pluginName, 'executeUpdatesSequentially', [sqlStatements]);
}

/**
 * executes all sqlStatements;
 * calls successCallback when all sqlStatements completed successfully;
 * calls errorCallback if any sqlStatements fails
 */
function executeStatementsAndCommit(sqlStatements, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeStatementsAndCommit', [sqlStatements]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeStatementsAndCommit', [sqlStatements]);
    }
}

function executeInsert(sqlStatement, success_method, error_method) {
    if (error_method == undefined) {
        cordova.exec(success_method, executeQuery_failed, pluginName, 'executeInsert', [sqlStatement]);
    } else {
        cordova.exec(success_method, error_method, pluginName, 'executeInsert', [sqlStatement]);
    }
}

function defineMLInfo(protocol, hostName, portNumber, subscriptionName, version, publicationName, urlSuffix, dbFileName, sync_success, sync_error) {
    if (!(protocol.toLowerCase() == "http" || protocol.toLowerCase() == "https" || protocol.toLowerCase() == "tcpip")) {
        throw new Error("mobilink.defineMLInfo - Invalid protocol: " + protocol);
    }

    //for windows platform we need to get the plugin instance object and subscribe to file transfer and sync events
    if (window.navigator.appVersion.indexOf('Windows NT') >= 0) {
        cordova.exec(
            function onSuccess(instance) {
                windowsPluginInstance = instance;
                windowsPluginInstance.addEventListener("ftevent", fileTransferListener, true);
                windowsPluginInstance.addEventListener("syncevent", syncListener, true);
                windowsPluginInstance.addEventListener("locationtrackingevent", locationTrackingListener, true);
                callNative();
            },
            sync_error,
            pluginName, "getPluginInstance", []
        );
    } else {
        callNative();
    }

    function callNative() {
        cordova.exec(sync_success, sync_error, pluginName, 'defineMLInfo', [hostName, portNumber, subscriptionName, version, publicationName, dbFileName, protocol, urlSuffix]);
    }

}

//function sync(user, pwd, sync_success, sync_error, progress_handler) {
//    syncInfo = {
//            user: user, //needed?
//            canceled: false,
//            onProgress: progress_handler,
//            syncPhase: syncPhases.fileUpload,
//            syncDbPhase: syncDbPhases.up,
//            progressFilesTotal: 0,
//            fileTransfers: [],
//            data: data,
//        }
//    start = new Date().getTime();
//    if (sync_error == undefined) {
//        cordova.exec(sync_success, sync_failed, pluginName, 'sync', [user, pwd]);
//    }
//    else
//    {
//        cordova.exec(sync_success, sync_error, pluginName, 'sync', [user, pwd]);
//    }
//}

function resetDB(user, pwd, success, error) {
    cordova.exec(success, error, pluginName, 'resetDB', [user, pwd]);
}

function setKeyValue(key, value, success, error) {
    cordova.exec(success, error, pluginName, 'setKeyValue', [key, value]);
}

function removeKey(key, success, error) {
    cordova.exec(success, error, pluginName, 'removeKey', [key]);
}

function getKeyValue(key, success, error) {
    cordova.exec(success, error, pluginName, 'getKeyValue', [key]);
}

function setUserPwd(user, pwd, success, error) {
    cordova.exec(success, error, pluginName, 'setUserPwd', [user, pwd]);
}

function locationTrackingListener(data) {
    var response = tryParseJson(data);
    sap.ui.getCore().getEventBus().publish("locationTracking", "newResponse", response);
}

function startLocationTracking(interval, successCb, errorCb) {
    var device = sap.ui.Device.os;
    var os = device.name;

    cordova.exec(successCb, errorCb, pluginName, 'startLocationTracking', [interval]);
}

function stopLocationTracking(successCb, errorCb) {
    var device = sap.ui.Device.os;
    var os = device.name;

    cordova.exec(successCb, errorCb, pluginName, 'stopLocationTracking', []);
}

function clearUserData(successCb, errorCb) {
    cordova.exec(successCb, errorCb, pluginName, 'clearUserData', []);
}

function login(user, pwd, success_method, error_method, progress_handler, fileName, encryptionKeyFileName) {
    if (progress_handler != null) {

        // File download
        var afterDBDownload = function (result) {
            // DB downloaded, move it to user folder
            //for windows platform we need to get the plugin instance object and subscribe to file transfer and sync events
            var userFolder = user;
            var device = sap.ui.Device.os;
            var os = device.name;

            var afterDbFileMoved = function () {
                if (encryptionKeyFileName != "") {
                    // After downloading encryption key
                    var encryption_dlError = function () {
                        error_method('cannotReadEncryptionKey');
                    };
                    var set_encryptionKey = function () {
                        // Get connections
                        var success_callback = function () {
                            deleteFile(encryptionKeyFileName, function () {
                                // Login
                                cordova.exec(success_method, error_method, pluginName, 'login', [user, pwd]);
                            }, error_method);

                        };
                        // Read encryption key file contents
                        getFileContent(encryptionKeyFileName, function (encryptionKey) {
                            cordova.exec(success_callback, error_method, pluginName, 'setKeyValue', ["SAMEncKey" + user, encryptionKey]);
                        }, function (error) {
                            console.log("Error reading encryption key: " + error.toString());
                            error_method('cannotReadEncryptionKey');
                        });

                    }
                    // Download encryption key
                    downloadFile(encryptionKeyFileName, set_encryptionKey, encryption_dlError, progress_handler);
                } else {
                    // Encryption is not active, delete any prev encryption
                    // key and just log the user in
                    cordova.exec(function () {
                        cordova.exec(success_method, error_method, pluginName, 'login', [user, pwd]);
                    }, error_method, pluginName, 'removeKey', ["SAMEncKey" + user]);
                }
            };
			
			if (os === device.OS.WINDOWS) {
				userFolder = '/Users/' + userFolder;
			}
			
            moveFile(fileName, "", fileName, userFolder, afterDbFileMoved.bind(this), error_method.bind(null, result));
        };
        var downloadFailed = function (result) {
            error_method(result);
        }
        var afterUserPwdSet = function () {
            downloadFile(fileName, afterDBDownload, downloadFailed, progress_handler);
        };
        setUserPwd(user, pwd, afterUserPwdSet, error_method);
    } else {
        // Not a new user, check entered password
        var afterCheckPwd = function () {
            //password OK, store the user, pwd and login
            var afterUserPwdSet = function () {
                cordova.exec(success_method, error_method, pluginName, 'login', [user, pwd]);
            };
            setUserPwd(user, pwd, afterUserPwdSet, error_method);
        }

        getKeyValue('SAMPwd' + user, function (storedPassword) {
            if (storedPassword === pwd) {
                afterCheckPwd();
            } else {
                error_method(67);
            }
        });
    }
}

//function login(user, pwd, success_method, error_method, progress_handler, fileName) {
//    if(progress_handler != null){
//	var ftInfo = new fileTransferInfo("D", fileName, fileName, success_method, error_method, progress_handler, 0, 0, null);
//	  //check if a file transfer for the log file is already in progress
//	    for (var i in fileTransfers) {
//	        var fileTransfer = fileTransfers[i];
//	        if (ftInfo.fileId == fileTransfer.fileId) {
//	            var message = "Cannot start file transfer for " + ftInfo.fileName + "; a file transfer for this file is already in progress";
//	            ftInfo.onError(message);
//	            return;
//	        }
//	    }
//	    fileTransfers.push(ftInfo);
//	    var success_login = function(result){
//
//			if(typeof result === 'object'){
//			    //call the progress handler
//			    fileTransferListener(result);
//			}
//			else{
//			    //call the success method
//			    onFileTransferDone(ftInfo, true, result);
//			}
//		    }
//	    var error_login = function(result){
//			onFileTransferDone(ftInfo, false, result);
//		    }
//    }else{
//	var success_login = success_method;
//	var error_login = error_method;
//    }
//
//    //if (sync_error == undefined) {
//        cordova.exec(success_login, error_login, pluginName, 'login', [user, pwd]);
////    }
////    else
////    {
////        cordova.exec(success_method, sync_error, pluginName, 'login', [user, pwd]);
////    }
//}
//function syncFiles(success_method, sync_error, progress_handler) {
//    var success_sync = function(result){
//
//	if(typeof result === 'object'){
//	    //call the progress handler
//	    syncListener(result);
//	}
//	else{
//	    //call the success method
//	    success_method(result);
//	}
//    }
//    if (sync_error == undefined) {
//        cordova.exec(success_method, sync_failed, pluginName, 'syncFiles', []);
//    }
//    else
//    {
//	cordova.exec(success_method, sync_error, pluginName, 'syncFiles', []);
//    }
//}
function onFileTransferDone(ftInfo, isSuccess, result) {
    ftInfo.finished = true;
    console.log("file transfer finished: " + ftInfo.fileId);
    ftInfo.success = isSuccess;
    removeItemFromArray(fileTransfers, ftInfo);
    if (isSuccess) {
        ftInfo.onSuccess(result);
    } else {
        ftInfo.onError(result);
    }
}

function downloadFile(fileName, success_method, error_method, progress_handler) {
    var ftInfo = new fileTransferInfo("D", fileName, fileName, success_method, error_method, progress_handler, 0, 0, null);
    transferFileInternal(ftInfo);
}

function uploadFile(fileName, success_method, error_method, progress_handler) {
    var ftInfo = new fileTransferInfo("U", fileName, fileName, success_method, error_method, progress_handler, 0, 0, null);
    transferFileInternal(ftInfo);
}

function transferFileInternal(ftInfo) {
    //check if a file transfer for this file is already in progress
    for (var i in fileTransfers) {
        var fileTransfer = fileTransfers[i];
        if (ftInfo.fileId == fileTransfer.fileId) {
            var message = "Cannot start file transfer for " + ftInfo.fileName + "; a file transfer for this file is already in progress";
            ftInfo.onError(message);
            return;
        }
    }
    fileTransfers.push(ftInfo);
    var success_ft = function (result) {

        if (typeof result === 'object') {
            //call the progress handler
            fileTransferListener(result);
        } else {
            //call the success method
            onFileTransferDone(ftInfo, true, result);
        }
    }
    var error_ft = function (result) {
        onFileTransferDone(ftInfo, false, result);
    }
    if (ftInfo.transferType == "U") {
        cordova.exec(success_ft, error_ft, pluginName, 'uploadFile', [ftInfo.fileName]);
    } else {
        cordova.exec(success_ft, error_ft, pluginName, 'downloadFile', [ftInfo.fileName]);
    }

}

/**
 * runs completely async; aborted file download will be handled by progress listener
 */
function cancelFileTransfer(fileId, successCallback, errorCallback) {
    cordova.exec(
        successCallback,
        function onError(error) {
            errorCallback("cancelFileTransfer failed for: " + fileId + ": " + error);
        },
        pluginName, "cancelFileTransfer", [fileId]
    );
};

function uploadDBFile(dbFileName, success_method, error_method, progress_handler) {
    var ftInfo = new fileTransferInfo("U", dbFileName, dbFileName, success_method, error_method, progress_handler, 0, 0, null);
    //check if a file transfer for the log file is already in progress
    for (var i in fileTransfers) {
        var fileTransfer = fileTransfers[i];
        if (ftInfo.fileId == fileTransfer.fileId) {
            var message = "Cannot start file transfer for " + ftInfo.fileName + "; a file transfer for this file is already in progress";
            ftInfo.onError(message);
            return;
        }
    }
    fileTransfers.push(ftInfo);
    var success_upload = function (result) {

        if (typeof result === 'object') {
            //call the progress handler
            fileTransferListener(result);
        } else {
            //call the success method
            onFileTransferDone(ftInfo, true, result);
        }
    }
    var error_upload = function (result) {
        onFileTransferDone(ftInfo, false, result);
    }

    cordova.exec(success_upload, error_upload, pluginName, 'uploadDBFile', [dbFileName]);

}

function uploadLogFiles(logFileName, success_method, error_method, progress_handler) {

    var ftInfo = new fileTransferInfo("U", logFileName, logFileName, success_method, error_method, progress_handler, 0, 0, null);
    //check if a file transfer for the log file is already in progress
    for (var i in fileTransfers) {
        var fileTransfer = fileTransfers[i];
        if (ftInfo.fileId == fileTransfer.fileId) {
            var message = "Cannot start file transfer for " + ftInfo.fileName + "; a file transfer for this file is already in progress";
            ftInfo.onError(message);
            return;
        }
    }
    fileTransfers.push(ftInfo);
    var success_upload = function (result) {

        if (typeof result === 'object') {
            //call the progress handler
            fileTransferListener(result);
        } else {
            //call the success method
            onFileTransferDone(ftInfo, true, result);
        }
    }
    var error_upload = function (result) {
        onFileTransferDone(ftInfo, false, result);
    }
//    if (sync_error == undefined) {
//        cordova.exec(success_method, sync_failed, pluginName, 'uploadLogFiles', []);
//    }
//    else
//    {
    cordova.exec(success_upload, error_upload, pluginName, 'uploadLogFiles', [logFileName]);
//    }
}

function setNewPassword(newPwd, oldPwd, success_method, sync_error) {

    if (sync_error == undefined) {
        cordova.exec(success_method, sync_failed, pluginName, 'setNewPassword', [newPwd, oldPwd]);
    } else {
        cordova.exec(success_method, sync_error, pluginName, 'setNewPassword', [newPwd, oldPwd]);
    }
}

function resizeImage(fileUrl, compressionQuality, success_method, sync_error) {
    cordova.exec(success_method, sync_error, pluginName, 'resizeImage', [fileUrl, compressionQuality]);
}

function checkNewUser(user, success_method, error_method) {
    cordova.exec(success_method, error_method, pluginName, 'checkNewUser', [user]);
}

function resetLogFiles(success_method, error_method) {
    cordova.exec(success_method, error_method, pluginName, 'deleteLogFiles', []);
}

function openLocalFile(filePath, successCb, errorCb) {
    var device = sap.ui.Device.os;
    var os = device.name;
    var mimeType = "";

    switch (getFileExtension(filePath).toLowerCase()) {
        case "pdf":
            mimeType = "application/pdf";
            break;
        case "doc":
            mimeType = "application/msword";
            break;
        case "docx":
            mimeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            break;
        case "xls":
            mimeType = "application/vnd.ms-excel";
            break;
        case "xlsx":
            mimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            break;
        case "csv":
            mimeType = "text/csv";
            break;
        default:
            mimeType = "application/octet-stream";
            break;
    }

    if (os === device.OS.IOS) {
        cordova.exec(successCb, errorCb, pluginName, 'openFile', [filePath]);
    } else {
        cordova.plugins.fileOpener2.open(
            filePath,
            mimeType,
            {
                error: errorCb,
                success: successCb
            }
        );
    }
}

function createPDF(htmlString, filePath, successCb, errorCb) {
    cordova.exec(successCb, errorCb, pluginName, 'createPDF', [htmlString, filePath]);
}

function logMessage(logLevel, message, isTransaction, success_method, error_method) {
    cordova.exec(success_method, error_method, pluginName, 'log', [logLevel, message, isTransaction]);
}

function executeQuery_failed(result) {
    //do something
}

function sync_failed(result) {
    //do something
}

function checkConnectivity(syncModel, success_method, error_method) {
    cordova.exec(success_method, error_method, pluginName, 'checkConnectivity', [syncModel]);
}

function checkConnection() {
    var networkState = navigator.connection.type;
    return (networkState !== Connection.NONE);
}


/**
 * completeCallback is called at the end of the sync process, regardless if successful or not
 * errorCallback is called on each error, may be called multiple times
 */
function sync(completedCallback, errorCallback, progressCallback, data, fileCompressionQuality) {
    if (syncObject != null) {
        if (syncObject.cancelled === false) {
            errorCallback("Cannot start new DB sync as another sync is still in progress");
            return;
        }
    }

    var syncCanceledMessage = "Synchronization cancel request received";

    syncObject = {
        canceled: false,
        onProgress: syncOnprogressHandler,
        syncPhase: syncPhases.fileUpload,
        syncDbPhase: syncDbPhases.up,
        progressFilesTotal: 0,
        fileTransfers: []
    }

    //1. Compress files before uploading it; abort on errors
    //2. upload files to mobilink; don't abort on file transfer error
    //3. execute pre sync DB scripts; abort on errors
    //4. sync DB; abort on errors
    //5. execute after sync DB scripts; abort on errors
    //6. download files from mobilink; don't abort on file transfer error
    //7. execute SQL commands; abort on errors

    //1.
    compressFiles(
        function onCompressionCompleted() {
            //2.
            uploadFiles(
                function onCompleted() {
                    if (syncObject.canceled) {
                        abortSync(syncCanceledMessage);
                    } else {
                        syncObject.syncPhase = syncPhases.dbSync;
                        //3.
                        preSyncOperations(
                            function onSuccess() {
                                //4.
                                syncDb(
                                    function onSuccess() {
                                        //5.
                                        afterSyncOperations(true,
                                            function onSuccess() {
                                                //6.
                                                syncObject.syncPhase = syncPhases.fileDownload;
                                                downloadFiles(
                                                    function onCompleted() {
                                                        if (syncObject.canceled) {
                                                            abortSync(syncCanceledMessage);
                                                        } else {
                                                            //7.
                                                            completeSync();
                                                            //executeSyncCommands(completeSync, abortSync);
                                                        }
                                                    },
                                                    errorCallback
                                                );
                                            },
                                            abortSync
                                        );
                                    },
                                    onDbSyncFailed, data
                                );
                            },
                            abortSync
                        );
                    }
                },
                errorCallback, syncOnprogressHandler
            );
        },
        errorCallback,
        syncOnprogressHandler,
        fileCompressionQuality
    );


    //1. Compress files
    function compressFiles(completedCallback, errorCallback, progressCallback, fileCompressionQuality) {
        var sqlGetFile = "SELECT ACTION_FLAG, FILENAME, DOC_SIZE, FILEEXT, MOBILE_ID, NEW_MOBILE_ID, OBJKEY, TABNAME FROM SAMObjectAttachments WHERE ACTION_FLAG='N' AND (lower(FILEEXT) = 'jpg' OR lower(FILEEXT) = 'png' OR lower(FILEEXT) = 'heic' OR lower(FILEEXT) = 'jpeg') " +
        "UNION ALL SELECT ACTION_FLAG, FILENAME, DOC_SIZE, FILEEXT, MOBILE_ID, NEW_MOBILE_ID, OBJKEY, TABNAME FROM DMSATTACHMENTS WHERE ACTION_FLAG='N' AND (lower(FILEEXT) = 'jpg' OR lower(FILEEXT) = 'png' OR lower(FILEEXT) = 'heic' OR lower(FILEEXT) = 'jpeg')";

        executeQuery(sqlGetFile,
            function onSuccess(result) {
                if (result && result.length > 0) {
                    var filesProcessed = 0;
                    for (var i in result) {

                        (function (file) {

                            var mobileId = file.MOBILE_ID;

                            var folderPath = "";
                            var fileName = file.FILENAME + '.' + file.FILEEXT;
                            var filePath = joinPaths(folderPath, fileName);

                            var image = [];
                            image.path = filePath;

                            getFileInfo(filePath, onGetFileInfoCompleted, onGetFileInfoCompleted);

                            function onGetFileInfoCompleted(folderPath, fileSize) {
                                resizeImage(folderPath + fileName, fileCompressionQuality, resizeImageSuccess, abortSync);

                                function resizeImageSuccess() {
                                    //File Compression done; fetch new file information and updating DB
                                    getFileInfo(filePath, onGetFileInfoCompletedForDBUpdate, onGetFileInfoCompleted);

                                    function onGetFileInfoCompletedForDBUpdate(folderPath, fileSize) {

                                        var sqlUpdateFileSize = "UPDATE SAMObjectAttachments SET DOC_SIZE = '" + fileSize + "' WHERE MOBILE_ID = '" + mobileId + "'";
                                        var sqlUpdateFileSizeDMS = "UPDATE DMSATTACHMENTS SET DOC_SIZE = '" + fileSize + "' WHERE MOBILE_ID = '" + mobileId + "'";

                                        if (fileName.toLocaleLowerCase().endsWith(".heic")) {
                                            sqlUpdateFileSize = "UPDATE SAMObjectAttachments SET DOC_SIZE = '" + fileSize + "', FILEEXT = 'jpeg' WHERE MOBILE_ID = '" + mobileId + "'";
                                            sqlUpdateFileSizeDMS = "UPDATE DMSATTACHMENTS SET DOC_SIZE = '" + fileSize + "', FILEEXT = 'jpeg' WHERE MOBILE_ID = '" + mobileId + "'";
                                        }

                                        executeUpdate(
                                            sqlUpdateFileSize,
                                            function onSuccess() {
                                                filesProcessed++;
                                                if (filesProcessed === result.length) {
                                                    completedCallback();
                                                }
                                            },
                                            abortSync
                                        );

                                        executeUpdate(
                                            sqlUpdateFileSizeDMS,
                                            function onSuccess() {
                                                filesProcessed++;
                                                if (filesProcessed === result.length) {
                                                    completedCallback();
                                                }
                                            },
                                            abortSync
                                        );

                                    }

                                }

                            }
                        })(result[i]);
                    }
                } else {
                    completedCallback();
                }
            },
            abortSync
        );
    }


    //2. upload files to mobilink
    function uploadFiles(completedCallback, errorCallback, progressCallback) {
        var sqlGetFile = "SELECT ACTION_FLAG, FILENAME, DOC_SIZE, FILEEXT, MOBILE_ID, NEW_MOBILE_ID, OBJKEY, TABNAME FROM SAMObjectAttachments " +
        "WHERE ACTION_FLAG='N' " +
        "UNION ALL SELECT ACTION_FLAG, FILENAME, DOC_SIZE, FILEEXT, MOBILE_ID, NEW_MOBILE_ID, OBJKEY, TABNAME FROM DMSATTACHMENTS " +
        "WHERE ACTION_FLAG='N'";

        executeQuery(sqlGetFile,
            function onSucess(result) {
                //var files = JSON.parse(result);
                var files = result;
                if (files && files.length > 0) {
                    //create file transfer object for each file to upload
                    for (var i in files) {

                        (function (file) {
                            var folderPath = "";
//                            if (file.fileDir != undefined) {
//                                folderPath = file.fileDir;
//                            }
                            var fileName = file.FILENAME + '.' + file.FILEEXT;
                            var filePath = joinPaths(folderPath, fileName);

                            getFileInfo(filePath, onGetFileInfoCompleted, onGetFileInfoCompleted);

                            function onGetFileInfoCompleted(folderPath, fileSize) {
                                //don't check for valid path, the error will be caught later

                                if (typeof fileSize != 'number') {
                                    fileSize = 0;
                                }

                                var dInfo = new fileTransferInfo("U", fileName, fileName, null, null, progressCallback, 0, 0, null);
                                syncObject.fileTransfers.push(dInfo);

                                //start file transfers after all file transfers have been successfully created
                                if (syncObject.fileTransfers.length == files.length) {
                                    transferFiles(syncObject.fileTransfers, completedCallback, onFileTransferError)
                                }
                            }
                        })(files[i]);

                    }
                } else {
                    completedCallback();
                }
            },
            abortSync
        );
    }

    //3. execute pre sync DB scripts
    function preSyncOperations(successCallback, errorCallback) {
        console.log("preSyncOperations executing");
        //For now, do nothing
        successCallback();
    }

    //4. sync DB
    function syncDb(successCallback, errorCallback, data) {
        cordova.exec(
            //called for sync progress (Android & iOS) and when file sync finished successfully
            function onSuccess(progress) {
                //case progress: we expect a json structure
                if (typeof progress == 'object') {
                    //var progress = JSON.parse(json);
                    if (progress.State != undefined) {
                        var msg = "sync progress; state: " + progress.State + ": TableCount: " + progress.TableCount +
                            " TableIndex: " + progress.TableIndex;
                        console.log(msg);

                        syncListener(progress);
                    }
                } else {
                    successCallback(progress);
                }
            },
            errorCallback, pluginName, 'sync',
            data
        );
    }

    function onDbSyncFailed(error) {
        afterSyncOperations(false,
            function onSuccess() {
                abortSync(error);
            },
            abortSync
        );
    }

    //5. execute after sync DB scripts
    function afterSyncOperations(success, successCallback, errorCallback) {
        console.log("afterSyncOperations executing");
        // Mark the attachments downloaded with action flag B, to be deleted in next sync
        var downloadedAttachmentsSQL = "SELECT a.mobile_id as mobile_id, b.mobile_id as new_mobile_id, b.FILENAME, b.FILEEXT, a.FILEEXT as local_fileext " +
        "FROM SAMObjectAttachments AS a " +
        "INNER JOIN SAMObjectAttachments as b WHERE a.ACTION_FLAG='A' AND a.NEW_MOBILE_ID=b.mobile_id " +
        "UNION ALL SELECT a1.mobile_id as mobile_id, b1.mobile_id as new_mobile_id, b1.FILENAME, b1.FILEEXT, a1.FILEEXT as local_fileext " +
        "FROM DMSAttachments AS a1 " +
        "INNER JOIN DMSAttachments as b1 WHERE a1.ACTION_FLAG='A' AND a1.NEW_MOBILE_ID=b1.mobile_id ";
        var updateStatements = [];
        var deferreds = [];
        var device = sap.ui.Device.os;
        var os = device.name;
        var directoryPath = "/";

        if (os === device.OS.ANDROID) {
            directoryPath = "";
        }

        executeQuery(downloadedAttachmentsSQL,
            function onSucess(result) {
                //var files = JSON.parse(result);
                var files = result;

                if (files && files.length > 0) {
                    //create file transfer object for each file to upload
                    for (var i in files) {

                        (function (i, file, deferredsArray) {
                            var folderPath = "";
                            deferredsArray[i] = new $.Deferred();
                            var fileName = file.FILENAME + '.' + file.local_fileext;
                            //var fileTargetName = file.LINKID + '.' + file.FILENAME + '.' + file.FILEEXT;
                            var fileTargetName = file.new_mobile_id.toUpperCase().replace(/-/g, '') + '.' + file.local_fileext;
                            var filePath = joinPaths(folderPath, fileName);

                            renameFile(fileName, directoryPath, fileTargetName, onMoveFileCompleted, onMoveFileFailed);

                            function onMoveFileFailed(){
                                //do nothing for now
                                deferredsArray[i].resolve();
                            }

                            function onMoveFileCompleted() {
                                //Add entry for update call
                                updateStatements.push({
                                    type: "update",
                                    mainTable: true,
                                    tableName: 'SAMObjectAttachments',
                                    values: [["ACTION_FLAG", "B"]],
                                    replacementValues: [],
                                    whereConditions: "mobile_id='" + file.mobile_id + "'",
                                    mainObjectValues: []
                                });

                                updateStatements.push({
                                    type: "update",
                                    mainTable: true,
                                    tableName: 'DMSAttachments',
                                    values: [["ACTION_FLAG", "B"]],
                                    replacementValues: [],
                                    whereConditions: "mobile_id='" + file.mobile_id + "'",
                                    mainObjectValues: []
                                });
                                deferredsArray[i].resolve();

                            }
                        })(i, files[i], deferreds);

                    }
                    $.when.apply($, deferreds).then(
                        function onSuccess(result) {
                            if (updateStatements.length > 0) {
                                executeStatementsAndCommit(updateStatements, successCallback, errorCallback);
                            } else {
                                successCallback();
                            }
                        },
                        function onError(error) {
                            //errorCallback(error);
                            //Still process the successful files in case atleast one succeeded?
                            if (updateStatements.length > 0) {
                                executeStatementsAndCommit(updateStatements, successCallback, errorCallback);
                            } else {
                                errorCallback(error);
                            }
                        }
                    );
                    //successCallback();
                } else {
                    successCallback();
                }

            },
            abortSync
        );


    }

    //6. download files from mobilink
    function downloadFiles(completedCallback, errorCallback) {
        console.log("download files called");
        //For now, we dont download anything from Mobilink server after sync
        completedCallback();
    }

    function syncOnprogressHandler(obj) {

        var pInfo = null;
        switch (syncObject.syncPhase) {
            //DB sync
            case syncPhases.dbSync:
                var totalTables = obj.TableCount * 2; //upload + download
                var curTableIndex = obj.TableIndex; //starts at 0 for upload and download

                if (obj.State == "STATE_RECEIVING_TABLE" || parseInt(obj.State) >= 13) { //we are in download phase now
                    syncObject.syncDbPhase = syncDbPhases.down;
                }

                if (syncObject.syncDbPhase == syncDbPhases.down) {
                    //for download phase the index starts with 0 again
                    curTableIndex = parseInt(obj.TableCount) + parseInt(obj.TableIndex);
                }

                //var percentDone = (curTableIndex / totalTables) * 100;
                pInfo = new syncProgressInfo(syncObject.syncPhase, totalTables, curTableIndex, null, obj.State);
                break;

            //file transfer
            default:
                var totalBytes = 0;
                var bytesTransferred = 0;

                //the real size of each file is available after file transfer start,
                //so we need to continuously determine the total bytes to transmit
                for (var i in syncObject.fileTransfers) {
                    var fileTransfer = syncObject.fileTransfers[i];
                    totalBytes = totalBytes + parseFloat(fileTransfer.fileSize);
                    bytesTransferred = bytesTransferred + parseFloat(fileTransfer.bytesTransferred);
                }

                //update values in the syncObject
                if (totalBytes > syncObject.progressFilesTotal) {
                    syncObject.progressFilesTotal = totalBytes;
                }

                pInfo = new syncProgressInfo(syncObject.syncPhase, syncObject.progressFilesTotal, bytesTransferred, syncObject.fileTransfers);
                break;
        }
        progressCallback(pInfo);
    }

    function onFileTransferError(error, fileTransfer) {
        callErrorCallback(error, fileTransfer);
    }

    function callErrorCallback(error) {
        errorCallback(error.Error ? error.Error : error);
    }

    function abortSync(error) {
        callErrorCallback(error);
    }

    function completeSync() {
        syncObject = null;
        completedCallback();
    }
};

/**
 * this will just trigger the cancel mechanism;
 * successCallback and errorCallback only signal if triggering the cancel
 * process was successfull or not;
 * the sync function will fail and call its errorCallback method
 */
function cancelSync(completedCallback, errorCallback) {
    if (syncObject != null) {
        syncObject.canceled = true;
        switch (syncObject.syncPhase) {

            //db sync
            case syncPhases.dbSync:
                cordova.exec(completedCallback,
                    function onError() {
                    },
                    pluginName, 'cancelSync', []
                );
                break;
            //file transfers
            default:
                var completes = [];
                for (var i in syncObject.fileTransfers) {

                    (function (counter) {
                        var fileTransfer = syncObject.fileTransfers[counter];

                        console.log("canceling transfer: " + fileTransfer.fileName);
                        cancelFileTransfer(fileTransfer.fileId,
                            function onSuccess() {
                                completes.push(true);
                                callCompletedIfDone();
                            },
                            function onError(error) {
                                completes.push(true);
                                errorCallback(error);
                                callCompletedIfDone();
                            }
                        );

                    })(i);

                    /*
                    //use this implementation when switching to Windows 10
                    (function (counter) {
                        var fileTransfer = syncObject.fileTransfers[counter];
                        // native javascript promises are supported beginning with Windows 10
                        var promise = new Promise(function (resolve, reject) {
                            cancelFileTransfer(fileTransfer.fileId,
                                function onSuccess() {
                                    resolve(fileTransfer.fileName + " trigger cancel success");
                                },
                                function onError(error) {
                                    reject(error);
                                }
                            );
                        });
                        promises.push(promise);

                    })(i);
                    */
                }
                //completedCallback();
                /*
                Promise.all(promises).then(function (arrayOfResults) {
                    completedCallback();
                });
                */
                break;
        }
    }

    function callCompletedIfDone() {
        if (completes.length == syncObject.fileTransfers.length) {
            completedCallback();
        }
    }
};
