﻿//NOTE: Please note that this file requires cordova file plugin to be installed, to work!!!
var isWindowsOs = window.navigator.appVersion.indexOf('Windows NT') >= 0;

//provides alert functionality for windows
var enableAlertFunction = (function () {
    if (isWindowsOs) {
        //var msgBox = new Windows.UI.Popups.MessageDialog("");
        //window.alert = function (message) {
        //    try {
        //        msgBox.content = message;
        //        msgBox.showAsync();
        //    }
        //    catch (ex) {
        //    }
        //}
    }
}());

function getMakaniInfoFromWsdl(makani, callbackFn){
    var xmlhttp = new XMLHttpRequest();
    var urlname = 'https://www.makani.ae/MakaniPublicDataService/MakaniPublic.svc/basic';
    xmlhttp.open('POST', urlname, true);

    // build SOAP request
    var sr =
        '<?xml version="1.0" encoding="utf-8"?>' +
        '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/">' +
        '<SOAP-ENV:Body>' +
        '<ns1:GetMakaniDetails>' +
        '<ns1:makanino>' + makani + '</ns1:makanino>' +
      '</ns1:GetMakaniDetails>' +
        '</SOAP-ENV:Body>' +
      '</SOAP-ENV:Envelope>';

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
        	var xmlresponse = new DOMParser().parseFromString(xmlhttp.responseText, "text/xml").getElementsByTagName("GetMakaniDetailsResult")[0].innerHTML;
        	var jsonResponse = JSON.parse(xmlresponse);
        	callbackFn(jsonResponse.MAKANI_INFO ? jsonResponse.MAKANI_INFO[0].LATLNG : null);
            }
            else{
        	callbackFn(null);
            }
        }
    }
    // Send the POST request
    xmlhttp.setRequestHeader('Content-Type', 'text/xml');
    xmlhttp.setRequestHeader('SOAPAction', 'http://tempuri.org/IMakaniPublic/GetMakaniDetails');
    xmlhttp.send(sr);
}

/**
 * available after deviceready event
 * checks if fileName exists in the app's root directory
 * returns true if the file exists, false otherwise
 * returns an error if the file system could not be accessed
 */
function fileExists(fileName, callback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, callback);

    function onInitFs(fs) {

        fs.root.getFile(fileName, {},
            function onSuccess(fileEntry) {
                callback(true);
            },
            function onError(error) {
                console.log("fileExists error: " + error.toString());
                callback(false);
            }
        );

    }
}

/**
 * available after deviceready event
 * checks if folderName exists in the app's root directory
 * returns true if the folder exists, false otherwise
 * returns an error if the file system could not be accessed
 */
function folderExists(folderName, callback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, callback);

    function onInitFs(fs) {

        fs.root.getDirectory(folderName, {},
            function onSuccess(folderEntry) {
                callback(true);
            },
            function onError(error) {
                console.log("folderExists error: " + error.toString());
                callback(false);
            }
        );

    }
}

/**
 * removes the first occurence of item from array if item is included in array
 */
function removeItemFromArray(array, item) {
    if (item) {
        var index = array.indexOf(item);

        if (index != -1) {
            array.splice(index, 1);
        }
    }
}

function createFolderInPersistentStorage(folderName, successCallback, errorCallback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        fs.root.getDirectory(folderName, { create: true },
            function (dirEntry) {
                var folderPath = "";
                if (isWindowsOs) {
                    folderPath = dirEntry.filesystem.winpath + dirEntry.name;
                }
                else {
                    folderPath = dirEntry.nativeURL;
                }
                successCallback(folderPath);
            },
            errorCallback
        );
    }
}

/**
 * folderPath must be a relative path; folders in folderPath will be created if not existent
 * returns the full path to folderPath
 */
function createFolderInPersistentStorage(folderPath, successCallback, errorCallback) {
    var splitChar = null;
    if (folderPath.indexOf('/') != -1) {
        splitChar = '/';
    } else if (folderPath.indexOf('\\') != -1) {
        splitChar = '\\';
    }

    var folders = [folderPath];
    if (splitChar != null) {
        folders = folderPath.split(splitChar);
    }

    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        createSubFolders(fs.root, folders, 0,
            function onSuccess(dir) {
                var folderPath = getFileEntryPath(dir);
                successCallback(folderPath);
            },
            errorCallback
        );
    }

    function createSubFolders(folder, folders, i, successCallback, errorCallback) {
        if (i >= folders.length) {
            successCallback(folder);
        }
        else {
            var subFolderName = folders[i];
            folder.getDirectory(subFolderName, { create: true },
                function onSuccess(subFolder) {
                    createSubFolders(subFolder, folders, i + 1, successCallback, errorCallback);
                },
                errorCallback
            );
        }
    }
}

function transferFiles(fileTransferInfos, completedCallback, errorCallback) {
    for (var i in fileTransferInfos) {
        var fileTransfer = fileTransferInfos[i];

        fileTransfer.onSuccess = completedCallbackIfFinished;
        fileTransfer.onError = function (error, fTransfer) {
            fTransfer.finished = true;
            errorCallback(error, fTransfer);
            completedCallbackIfFinished();
        }
        transferFileInternal(fileTransfer);
    }

    function completedCallbackIfFinished() {
        var finishedTotal = 0;
        for (var i in fileTransferInfos) {
            if (fileTransferInfos[i].finished) {
                finishedTotal++;
            }
        }
        if (finishedTotal == fileTransferInfos.length) {
            completedCallback();
        }
    }
}

/**
 * filePath is a relative path to the app's persistent storage
 * returns the full path of the folder where the file is located and the size of the file in bytes
 */
function getFileInfo(filePath, successCallback, errorCallback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        fs.root.getFile(filePath, { create: false },
            function onSuccess(file) {
                file.getMetadata(
                    function onSuccess(metadata) {
                        var filePath = getFileEntryPath(file);
                        var folderPath = getFolderPath(filePath);
                        successCallback(folderPath, metadata.size);
                    },
                    onFileSizeError
                );
            },
            onFileSizeError
        );
    }

    function onFileSizeError(error) {
        var message = "error determining file size for: '" + filePath + "'; error: ";
        var errorMessage = error;
        if (error.code != undefined) {
            errorMessage = error.code;
        }
        message += errorMessage;
        console.log(message);

        successCallback(null, 0);
    }
}

/**
 * reads the content from filePath and returns its content as a string
 */
function getFileContent(filePath, successCallback, errorCallback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        fs.root.getFile(filePath, { create: false },
            function onSuccess(fileEntry) {
                fileEntry.file( function(file) {
                        var reader = new FileReader();
                        reader.onloadend = function (e) {
                            successCallback(this.result);
                        };
                        reader.readAsText(file);
                    },
                    errorCallback
                );
            },
            errorCallback
        );
    }
}

function deleteFile(filePath, successCallback, errorCallback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        fs.root.getFile(filePath, { create: false },
            function onSuccess(fileEntry) {
                fileEntry.remove(
                    function onSuccess() {
                        console.log('successfully deleted file: ' + filePath);
                        successCallback();
                    },
                    function onError(error) {
                        console.log('error deleting file: ' + filePath + ' error: ' + error);
                        errorCallback();
                    }
                );
            },
            errorCallback
        );
    }
}

/**
 * available after deviceready event
 * deletes the given folder from the app's root directory
 * in case there are sub-directories or files included, the error code 9 is returned
 */
function deleteFolder(folderPath, successCallback, errorCallback) {
    window.requestFileSystem(window.PERSISTENT, 0, onInitFs, errorCallback);

    function onInitFs(fs) {
        fs.root.getDirectory(folderPath, { create: false },
            function onSuccess(folderEntry) {
                folderEntry.remove(
                    function onSuccess() {
                        console.log('successfully deleted folder: ' + folderPath);
                        successCallback();
                    },
                    function onError(error) {
                        console.log('error deleting folder: ' + folderPath + ' error: ' + error);
                        errorCallback();
                    }
                );
            },
            errorCallback
        );
    }
}

function renameFile(currentName, currentDir, newName, successFunction, errorFunction) {
    function renameFail() {
	   console.log('error moving file ' + currentName);
	   errorFunction();
    }
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {

        fileSystem.root.getFile(currentDir + currentName, null, function (fileEntry) {
            fileSystem.root.getDirectory(currentDir, {create: true}, function (dirEntry) {
                parentEntry = new DirectoryEntry(currentName, currentDir + currentName);

                fileEntry.moveTo(dirEntry, newName, function () {
                    console.log('successfully renamed file ' + currentName);
                    successFunction();

                }, renameFail);
            }, renameFail);
        }, renameFail);

    }, renameFail);
}
function moveFile(currentName, currentDir, newName, newDir, successFunction, errorFunction) {
    function moveFail() {
	   console.log('error moving file ' + currentName);
	   errorFunction();
    }
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {

        fileSystem.root.getFile(currentDir + currentName, null, function (fileEntry) {
            fileSystem.root.getDirectory(newDir, {create: true}, function (dirEntry) {
                fileEntry.moveTo(dirEntry, newName, function () {
                    console.log('successfully renamed file ' + currentName);
                    successFunction();

                }, moveFail);
            }, moveFail);
        }, moveFail);

    }, moveFail);
}
function createTextFile(name, dir, text, successFunction, errorFunction){
    function createFail() {
	   console.log('error creating file ' + name);
	   errorFunction();
    }
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {     
         fileSystem.root.getDirectory(dir, {create: false}, function (dirEntry) {
             dirEntry.getFile(name, {create:true}, function (file) {
                 file.createWriter(function(writer){
                     writer.write(text);
                     successFunction();
                 }, createFail);
             }, createFail);
         }, createFail);
    }, createFail);
    
}
/**
 * returns the path of the fileEntry (cordova-plugin-file)
 */
function getFileEntryPath(fileEntry) {
    var folderPath = "";
    if (isWindowsOs) {
        folderPath = fileEntry.filesystem.winpath + fileEntry.fullPath.slice(1);
    }
    else {
        folderPath = fileEntry.nativeURL;
    }
    return folderPath;
}

/**
 * returns leftPath/rightPath
 */
function joinPaths(leftPath, rightPath) {
    var left = leftPath;
    var right = rightPath;

    //remove last char from leftPath if "\" or "/"
    var lastCharLeftPath = leftPath.slice(-1);
    if (lastCharLeftPath == "/" || lastCharLeftPath == "\\") {
        left = leftPath.substring(0, leftPath.length - 1);
    }

    //remove first char from rightPath if "\" or "/"
    var firstCharRightPath = rightPath.charAt(0);
    if (firstCharRightPath == "/" || firstCharRightPath == "\\") {
        right = rightPath.substr(1);
    }

    return left + "/" + right;
}

function tryParseJson(json) {
    var result = null;
    try{
        result = JSON.parse(json);
    }
    catch (ex) {
        console.log("tryParseJson: could not parse: " + json);
        result = json;
    }
    return result;
}

/**
 * returns the name of the file without the extension (e.g. .jpg);
 * @param {string} fullFileName
 * @returns {string} fullFileName if extension could not be determined
 */
function getFileName(fullFileName) {
   var result = fullFileName;
   
   var separatorIndex = fullFileName.lastIndexOf(".");
   if (separatorIndex >= 0) {
      result = fullFileName.substring(0, separatorIndex);
   }
   return result;
}

function getFileExtension(fileName) {
   var result = null;
   
   var separatorIndex = fileName.lastIndexOf(".");
   if (separatorIndex >= 0) {
      result = fileName.substring(separatorIndex + 1, fileName.length);
   }
   return result;
}

/**
 * returns the folder path from filepath, or null if no folder could be determined;
 *  e.g. "ms-appdata:///local/cameraCaptureImage.jpg" -> "ms-appdata:///local/"
 * @param {string} filePath
 */
function getFolderPath(filePath) {
   var parentPath = null;
   //TODO: make this work also with "\" chars
   var pathSeparatorIndex = filePath.lastIndexOf("/");
   if (pathSeparatorIndex >= 0) {
      parentPath = filePath.substring(0, pathSeparatorIndex + 1);
   }
   return parentPath;
}