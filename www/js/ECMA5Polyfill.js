/* This class holds all kind of polyfills */

// The trimEnd() method removes whitespace from the end of a string. trimRight() is an alias of this method.
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/trimEnd
if (!String.prototype.trimEnd) {
    String.prototype.trimEnd = function () {
        return this.trimRight();
    };
}

if (!String.prototype.includes) {
    String.prototype.includes = function (sNeedle) {
        if (sNeedle) {
            return this.indexOf(sNeedle) !== -1;
        }
    };
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (sNeedle) {
        if (sNeedle) {
            return this.indexOf(sNeedle) === 0;
        }
    };
}

if (!String.prototype.endsWith) {
    String.prototype.endsWith = function(search, this_len) {
        if (this_len === undefined || this_len > this.length) {
            this_len = this.length;
        }
        return this.substring(this_len - search.length, this_len) === search;
    };
}

if (!Array.prototype.find) {
    Array.prototype.find = function (fFilter) {
        if (fFilter) {
            var aMatches = this.filter(fFilter);
            return aMatches && aMatches.length && aMatches[0];
        }
    };
}

if (!Array.prototype.findIndex) {
    Array.prototype.findIndex = function (fFilter) {
        if (fFilter) {
            var iResult = -1;
            for (var iIndex = 0; iIndex < this.length; ++iIndex) {
                if (fFilter(this[iIndex], iIndex, this)) {
                    iResult = iIndex;
                    break;
                }
            }
            return iResult;
        }
    };
}

if (!Array.prototype.includes) {
    Array.prototype.includes = function (vNeedle, iFromIndex) {
        if (vNeedle) {
            var isNeedle = function (vValue) {
                return vValue === vNeedle || (typeof vValue === "number" && typeof vNeedle === "number" && isNaN(vValue) && isNaN(vNeedle));
            };
            for (var iIndex = iFromIndex | 0; iIndex < this.length; ++iIndex) {
                if (isNeedle(this[iIndex])) {
                    return true;
                }
            }
            return false;
        }
    };
}
