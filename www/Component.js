sap.ui.define(["sap/ui/core/UIComponent",
        "SAMContainer/models/GlobalViewModel",
        "SAMContainer/helpers/formatter",
        "sap/ui/model/json/JSONModel",
        "sap/ui/Device",
        "sap/m/MessageBox"],

    function (UIComponent,
              GlobalViewModel,
              formatter,
              JSONModel,
              Device,
              MessageBox) {
        "use strict";

        return UIComponent.extend("SAMContainer.Component", {
            formatter: formatter,
            metadata: {
                manifest: "json"
            },

            /**
             * The component is initialized by UI5 automatically during the startup
             * of the app and calls the init method once.
             *
             * @public
             * @override
             */
            init: function () {
                // call the base component'ss init function
                UIComponent.prototype.init.apply(this, arguments);
                this.getRouter().initialize();

                sap.ui.getCore().getEventBus().subscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().subscribeOnce("loginRoute", "onLoginSuccess", this.onAfterFirstLogin, this);

                this.globalViewModel = new GlobalViewModel();

                this.i18n = this.getModel("i18n").getResourceBundle();
                var deviceModel = new JSONModel(Device);
                deviceModel.setDefaultBindingMode("OneWay");

                this.publication = this.getManifestObject().getEntry("sap.app").MLSetup.publication;
                this.setModel(this.globalViewModel.model, "globalViewModel");
                this.setModel(deviceModel, "device");

                this.initTheme();
            },

            exit: function() {
                sap.ui.getCore().getEventBus().unsubscribe("sync", "onAfterSync", this.onAfterSync, this);
                sap.ui.getCore().getEventBus().subscribeOnce("loginRoute", "onLoginSuccess", this.onAfterFirstLogin, this);
            },

            onAfterSync: function () {
                const samUser = this.globalViewModel.getSAMUser();

                var onSAMUserUpdated = function() {
                    this.checkAppUgradeNeeded();
                };

                var onSAMUserUpdateFailed = function(err) {
                    MessageBox.alert(err.message);
                };

                //Update SAMUser
                samUser.init().then(onSAMUserUpdated.bind(this)).catch(onSAMUserUpdateFailed.bind(this));
            },

            onAfterFirstLogin: function() {
                this.checkAppUgradeNeeded();
            },

            checkAppUgradeNeeded: function () {
                var that = this;
                var samUser = this.globalViewModel.getSAMUser();

                if (samUser.getUdb().getUpgradeNeeded()) {
                    // Show client update dialog
                    samUser._fetchLatestSAMScenarioVersion().then(function() {
                        that.getSAMUpgradeDialog().open();
                    });
                }
            },

            onCloseUpgradeDialog: function () {
                this.getSAMUpgradeDialog().close();
            },

            onUpgradeSAMPressed: function () {
                this._performSAMUpgrade();
            },

            _performSAMUpgrade: function() {
                var that = this;

                const samUser = this.globalViewModel.getSAMUser();

                this.onCloseUpgradeDialog();
                this.setBusyOn();

                var onResetUdbSuccess = function (result) {
                    that.setBusyOff();
                    sap.ui.getCore().getEventBus().publish("home", "onLogout");
                };

                var onResetUdbError = function (result) {
                    MessageBox.alert(that.i18n.getText("ERROR_SAM_UPGRADE"));
                    that.setBusyOff();
                };

                var onRemoteUpgradeUdbSuccess = function(completeCb) {
                    // 3. Update the scenario_version_id column in SAM_USER_UDB with the new scenario version id
                    samUser.setUpgradeDone().then(function() {
                        // 4. Execute sync
                        sap.ui.getCore().getEventBus().publish("rootComponent", "onSyncRequest", {syncMode: "S", completeCb: completeCb});
                    }.bind(this)).catch(function(err) {
                        MessageBox.alert(err.message);
                    }).finally(function() {
                        this.setBusyOff();
                    }.bind(this));
                };

                var onRemoteUpgradeUdbError = function(err) {
                    resetDB(samUser.getUserName(), "", onResetUdbSuccess, onResetUdbError);
                };

                var onLatestScenarioVersionFetchError = function(err) {
                    this.setBusyOff();
                    MessageBox.alert(err.message);
                };

                var onLatestScenarioVersionFetched = function() {
                    // Upgrade component preload
                    // Remote upgrade udb
                    var componentPreloadUpgrade = samUser.getScenario().getVersion().hasComponent();
                    var remoteUdbUpgrade = samUser.getScenario().getVersion().hasUpgradeSQLScript();

                    var promiseQueue = [];

                    if (componentPreloadUpgrade) {
                        promiseQueue.push(samUser.upgradeComponentPreload());
                    }

                    if (remoteUdbUpgrade) {
                        promiseQueue.push(samUser.upgradeUdb());
                    }

                    if (!componentPreloadUpgrade && !remoteUdbUpgrade) {
                        promiseQueue.push(samUser.resetUdb());
                    }

                    Promise.all(promiseQueue).then(function(result) {
                        // If component & remote udb upgrade -> Execute a synchronization, afterwards nav to login and window.location.reload()
                        if ((remoteUdbUpgrade && componentPreloadUpgrade) || componentPreloadUpgrade) {
                            onRemoteUpgradeUdbSuccess.apply(that, [function(bSyncSuccess) {
                                if (!bSyncSuccess) {
                                    return;
                                }

                                // Delete obsolete preload files
                                samUser.deleteObsoleteComponentPreloadFiles().finally(function () {
                                    sap.ui.getCore().getEventBus().publish("core", "onLoggedOut", {hardReload: true});
                                });

                            }]);
                            return;
                        }

                        // If remote udb update -> Execute a synchronization
                        if (remoteUdbUpgrade) {
                            onRemoteUpgradeUdbSuccess.apply(that);
                            return;
                        }
                        // If reset udb -> logout
                        if (!componentPreloadUpgrade && !remoteUdbUpgrade) {
                            sap.ui.getCore().getEventBus().publish("home", "onLogout");
                        }
                    }).catch(function(err) {
                        if (remoteUdbUpgrade && componentPreloadUpgrade) {
                            onRemoteUpgradeUdbError.apply(that, [err]);
                            return;
                        }

                        if (componentPreloadUpgrade) {
                            MessageBox.alert(that.i18n.getText("ERROR_SAM_COMPONENT_PRELOAD_UPGRADE"));
                            return;
                        }

                        if (remoteUdbUpgrade) {
                            onRemoteUpgradeUdbError.apply(that, [err]);
                            return;
                        }
                        // If reset udb -> logout
                        if (!componentPreloadUpgrade && !remoteUdbUpgrade) {
                            onResetUdbError.apply(that);
                            return;
                        }
                    }).finally(function() {
                        that.setBusyOff();
                    });
                };

                // 1. Get latest active scenario version of the current scenario
                samUser.initLatestScenarioVersion().then(onLatestScenarioVersionFetched.bind(this)).catch(onLatestScenarioVersionFetchError.bind(this));
            },

            getSAMUpgradeDialogText: function(samUser) {
                var oBundle = this.getModel("i18n").getResourceBundle();
                if (!samUser.getScenario()) {
                    return oBundle.getText("SAM_UPGRADE_COMPONENT_PRELOAD");
                }
                var componentPreloadUpgrade = samUser.getScenario()._latestScenarioVersion.hasComponent();
                var remoteUdbUpgrade = samUser.getScenario()._latestScenarioVersion.hasUpgradeSQLScript();

                if (componentPreloadUpgrade && remoteUdbUpgrade) {
                    return oBundle.getText("SAM_UPGRADE_COMPONENT_AND_REMOTE_UDB");
                }

                if (componentPreloadUpgrade) {
                   return oBundle.getText("SAM_UPGRADE_COMPONENT_PRELOAD");
                }

                if (remoteUdbUpgrade) {
                   return oBundle.getText("SAM_UPGRADE_REMOTE_UDB");
                }

                if (!componentPreloadUpgrade && !remoteUdbUpgrade) {
                    return oBundle.getText("SAM_UPGRADE_UDB_RESET");
                }
            },

            getSyncErrorMessageForCode: function (code) {
                var oBundle = this.getModel("i18n").getResourceBundle();

                switch (code) {
                    case 57:
                        return oBundle.getText("ML_ERROR_HOST_NAME_NOT_FOUND");
                    case 63:
                        return oBundle.getText("ML_ERROR_UNABLE_CONNECT_TO_SOCKET");
                    case 67:
                        return oBundle.getText("ML_ERROR_WRONG_LOCAL_PASSWORD");
                    case 313:
                        return oBundle.getText("ML_ERROR_UNABLE_CONNECT_TO_SOCKET");
                    case 2000:
                        return oBundle.getText("ML_ERROR_DATABASE_BEING_CREATED");
                    case 2001:
                        return oBundle.getText("ML_ERROR_DATABASE_BEING_CREATED"); // ML_ERROR_UDB_ALREADY_DOWNLOADED
                    case 2002:
                        return oBundle.getText("ML_ERROR_REMOTEID_INVALID");
                    case 4000:
                        return oBundle.getText("ML_ERROR_AUTHENTICATION_FAIL");
                    case 4001:
                        return oBundle.getText("ML_ERROR_AUTHENTICATION_USER_NOTFOUND");
                    case 4002:
                        return oBundle.getText("ML_ERROR_DATABASE_LOCKED");
                    case 4003:
                        return oBundle.getText("ML_ERROR_NO_MWSERVER_AVAILABLE");
                    case 4004:
                        return oBundle.getText("ML_ERROR_INITIALREP_NOT_FINISHED");
                    case 4006:
                        return oBundle.getText("ML_ERROR_SAM_LICENSE_EXPIRED");
                    case 8:
                        return oBundle.getText("ML_ERROR_UNABLE_READ_BYTES");
                    case 24:
                        return oBundle.getText("ML_ERROR_NO_TRUSTED_CERT");
                    case 40:
                        return oBundle.getText("ML_ERROR_CERT_NOT_FOUND");
                    case 201:
                        return oBundle.getText("ML_ERROR_TIMED_OUT");
                    case 218:
                        return oBundle.getText("ML_ERROR_OPERATION_INTERRUPTED");
                    case 228:
                        return oBundle.getText("ML_SERVER_ERROR");
                    case 249:
                        return oBundle.getText("ML_ERROR_SERVER_AUTHENTICATION_FAILED");
                    case 5001:
                        return oBundle.getText("ML_SOCKET_ERROR");
                    case 5002:
                        return oBundle.getText("ML_ANOTHER_SYNC_RUNNING");
                    case 5003:
                        return oBundle.getText("ML_ANOTHER_UNKNOWN_PUBLICATION");
                    case 4005:
                        return oBundle.getText("ML_ERROR_DURING_SYNC");
                    case 2005:
                        return oBundle.getText("ML_ERROR_DURING_FILE_TRANSFER");
                    case 2004:
                        return oBundle.getText("ML_ERROR_FILE_NOT_FOUND");
                    case 2003:
                        return oBundle.getText("ML_ERROR_NO_UDB_AVAILABLE");
                    default:
                        return oBundle.getText("ML_COMMUNICATION_ERROR_" + code);
                }
            },

            setUiLanguage: function (langCode, reloadModels) {
                // set preferred language on localStorage
                localStorage.setItem("SAM_lang", langCode);
                sap.ui.getCore().getConfiguration().setLanguage(langCode);
                if (localStorage.hasOwnProperty('SAM_CUSTOMIZING_MODEL')) {
                    localStorage.removeItem('SAM_CUSTOMIZING_MODEL');
                }
                if (reloadModels) {
                    sap.ui.getCore().getEventBus().publish("home", "onModelsReload");
                }
            },

            setBusyOn: function () {
                this.globalViewModel.model.setProperty('/busy', true);
            },

            setBusyOff: function () {
                this.globalViewModel.model.setProperty('/busy', false);
            },

            initTheme: function () {
                var theme = localStorage.getItem("SAM_theme");

                if (theme == null) {
                    theme = "sap_belize";
                    localStorage.setItem("SAM_theme", theme);
                }


                this.globalViewModel.model.setProperty("/selectedTheme", theme);
                this.changeTheme(theme);
            },

            changeTheme: function (theme) {
                localStorage.setItem("SAM_theme", theme);

                if (theme !== "sam") {
                    // Remove SAM custom style
                    $('link[rel=stylesheet][href="css/samCustom.css"]').remove();
                } else {
                    // Add SAM custom style
                    $('head').append('<link rel="stylesheet" type="text/css" href="css/samCustom.css">');
                    theme = "sap_fiori_3";
                }

                sap.ui.getCore().applyTheme(theme);
            },

            getSAMUpgradeDialog: function () {
                if (!this._upgradeDialog) {
                    this._upgradeDialog = sap.ui.xmlfragment("samUpgradeDialog", "SAMContainer.view.home.SAMUpgradeDialog", this);
                    this._upgradeDialog.setModel(this.getModel("i18n"), "i18n");
                    this._upgradeDialog.setModel(this.getModel("globalViewModel"), "globalViewModel");
                }

                return this._upgradeDialog;
            }
        });

    });