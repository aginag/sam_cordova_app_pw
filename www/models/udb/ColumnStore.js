sap.ui.define(["SAMContainer/models/udb/Table"],
   
   function(Table) {
      "use strict";
      
      // constructor
      function ColumnStore() {
         this.tables = [];
      }
      
      ColumnStore.prototype = {
         setTables: function(data) {
            this.tables = this._initTables(data);
         },
         
         getTable: function(tableName) {
            var table = this.tables.filter(function(t) {
               return t.name == tableName;
            });
            
            return table.length == 0 ? null : table[0];
         }
      };
      
      ColumnStore.prototype._initTables = function(data) {
         var tables = [];

         for (var tableName in data) {
            var columnsJSON = data[tableName];
            tables.push(new Table(tableName, columnsJSON));
         }
         
         return tables;
      };
   
      ColumnStore.prototype.constructor = ColumnStore;
      
      return ColumnStore;
   });
