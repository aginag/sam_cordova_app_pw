sap.ui.define([],

    function () {
        "use strict";

        // constructor
        function DataObject(tableName, columnsIdentifier, requestHelper, model) {
            this.requestHelper = requestHelper;
            this.tableName = tableName;
            this.mainTable = true;

            this.columns = this.requestHelper.tableColumnJSONModel.getProperty("/" + columnsIdentifier);
            this.tableDefinition = this.requestHelper.columnStore.getTable(this.tableName);

            this.callbacks = this._getCallbacks();
            this.data = null;

            if (this.model !== undefined) {
                this.model = model;
            }
        }

        DataObject.prototype = {
            data: null,
            requestHelper: null,
            tableName: null,
            mainTable: null,
            columns: [],
            model: null,
            callbacks: null,

            getRequestHelper: function () {
                return this.requestHelper;
            },

            setDefaultData: function () {
                var that = this;

                if (!this.data || this.data == undefined) {
                    this.initNewObject();
                } else {
                    this.initExisting();
                }
            },

            initNewObject: function () {
                var that = this;

                this.columns.forEach(function (column) {
                    var value = "";

                    if (column == "ACTION_FLAG") {
                        value = "N";
                    } else if (column == "MOBILE_ID") {
                        value = "";
                    }

                    that[column] = value;
                });
            },

            initExisting: function () {
                this.setData(this.data);

                this.callbacks.afterExistingDataSet.apply(this);
            },

            insert: function (bStrings, successCb, errorCb) {
                var that = this;
                bStrings = bStrings == undefined ? false : bStrings;

                var values = this.getSqlValues();

                var sqlString = {
                    type: "insert",
                    mainTable: this.mainTable,
                    tableName: this.tableName,
                    values: values,
                    replacementValues: [],
                    whereConditions: "",
                    mainObjectValues: []
                };

                this.data = {};

                this.columns.forEach(function(column) {
                    that.data[column] = that[column] || "";
                });

                if (bStrings) {
                    return sqlString;
                } else {
                    this.requestHelper.executeStatementsAndCommit([sqlString], function (e) {
                        successCb(e);
                    }, errorCb);
                }
            },

            hasChanges: function () {
                if (!this.data) {
                    return true;
                }

                var values = this.getSqlValues();
                var valuesForUpdate = this.getChangedSqlValues(values);

                return valuesForUpdate.length > 0;
            },

            update: function (bStrings, successCb, errorCb) {
        	    this.ACTION_FLAG = this.ACTION_FLAG == "N" || this.ACTION_FLAG == "O" ? "N" : "C";
                var values = this.getSqlValues();

                var sqlString = {
                    type: "update",
                    mainTable: this.mainTable,
                    tableName: this.tableName,
                    values: values,
                    replacementValues: [],
                    whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
                    mainObjectValues: []
                };

                if (bStrings) {
                    return sqlString;
                } else {
                        this.requestHelper.executeStatementsAndCommit([sqlString], function (e) {
                        successCb(e);
                    }, errorCb);
                }

            },

            // Only updates the values that have actually changed
            update2: function (bStrings, successCb, errorCb) {
                var that = this;
                this.ACTION_FLAG = this.ACTION_FLAG == "N" ? "N" : "C";

                var valuesForUpdate = this.getChangedSqlValues(this.getSqlValues());

                // Update the current state with the updated values
                this.columns.forEach(function(column) {
                    that.data[column] = that[column] || "";
                });

                var sqlString = {
                    type: "update",
                    mainTable: this.mainTable,
                    tableName: this.tableName,
                    values: valuesForUpdate,
                    replacementValues: [],
                    whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
                    mainObjectValues: []
                };

                if (bStrings) {
                    return sqlString;
                } else {
                    this.requestHelper.executeStatementsAndCommit([sqlString], function (e) {
                        successCb(e);
                    }, errorCb);
                }
            },

            setChanged: function (bStrings, successCb, errorCb) {
                var sqlString = {
                    type: "update",
                    mainTable: this.mainTable,
                    tableName: this.tableName,
                    values: [["ACTION_FLAG", "C"]],
                    replacementValues: [],
                    whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
                    mainObjectValues: []
                };

                if (bStrings) {
                    return sqlString;
                } else {
                    this.requestHelper.executeStatementsAndCommit([sqlString], function (e) {
                        successCb(e);
                    }, errorCb);
                }
            },

            delete: function (bStrings, successCb, errorCb) {
                var sqlStrings = [];

                if (this.ACTION_FLAG == "N") {
                    sqlStrings = {
                        type: "delete",
                        mainTable: this.mainTable,
                        tableName: this.tableName,
                        values: [],
                        replacementValues: [],
                        whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
                        mainObjectValues: []
                    };
                } else {
                    this.ACTION_FLAG ='D';
                    sqlStrings.push({
                        type: "update",
                        mainTable: this.mainTable,
                        tableName: this.tableName,
                        values: this.getChangedSqlValues(this.getSqlValues()),
                        replacementValues: [],
                        whereConditions: " MOBILE_ID = '" + this.MOBILE_ID + "'",
                        mainObjectValues: []
                    });
                }

                if (bStrings) {
                    return sqlStrings;
                } else {
                    if(sqlStrings instanceof Array){
                        this.requestHelper.executeStatementsAndCommit(sqlStrings, function (e) {
                            successCb(e);
                        }, errorCb);
                    } else {
                        var deleteArray = [];
                        deleteArray.push(sqlStrings);
                        this.requestHelper.executeStatementsAndCommit(deleteArray, function (e) {
                            successCb(e);
                        }, errorCb);
                    }

                }
            },

            getSqlValues: function () {
                var that = this;
                var values = [];

                this.columns.forEach(function (column) {
                    if (column != "MOBILE_ID") {
                        var columnDefinition = that.tableDefinition ? that.tableDefinition.getColumn(column) : null;
                        var value = that[column];

                        if (columnDefinition) {
                            columnDefinition.check(value);
                        }

                        if (value !== undefined && value !== null) {
                            values.push([column, value]);
                        }
                    }
                });

                return values;
            },

            getChangedSqlValues: function (values) {
                var changedSqlValues = [];

                for (var i = 0; i < values.length; i++) {
                    var columnValuePair = values[i];
                    var column = columnValuePair[0];
                    var value = columnValuePair[1];

                    if (this.data[column] === undefined || this.data[column] !== value) {
                        changedSqlValues.push(columnValuePair);
                    }
                }

                return changedSqlValues;
            },

            getData: function () {
                return this.data;
            },

            getCopy: function () {
                return Object.assign({}, this);
            },

            mapDataToDataObject: function (G_DataObject, data) {
                var that = this;
                return data.map(function (objectData) {
                    var g_object = new G_DataObject(objectData, that.getRequestHelper(), that.model);

                    return g_object;
                });
            },

            setData: function (data) {
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        this[i] = data[i];
                    }
                }
            },

            getRandomUUID: function (length) {
                var result = '';
                var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
                return result;
            },

            getRandomCharUUID: function (length) {
                var result = '';
                var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
                return result;
            },

            getRandomIntegerUUID: function (length) {
                var result = '';
                var chars = '0123456789';
                for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
                return result;
            },

            getDateNow: function () {
                var time = moment();
                var date = moment();
                date.hours(0);
                date.minutes(0);
                date.seconds(0);
                date.milliseconds(0);

                return {
                    date: date.format("YYYY-MM-DD HH:mm:ss.SSS"),
                    time: time.format("HH:mm:ss.SSS"),
                    dateTime: time.format("YYYY-MM-DD HH:mm:ss.SSS")
                };
            },

            _getCallbacks: function () {
                return Object.assign({}, {
                    afterExistingDataSet: function () {
                    }
                });
            }
        };

        return DataObject;
    }
);