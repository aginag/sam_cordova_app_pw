sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/helpers/FileSystem", "SAMContainer/models/ViewModelFormatter"],

    function (JSONModel, FileSystem, ViewModelFormatter) {
        "use strict";

        // constructor
        function ViewModel(component, globalViewModel, requestHelper, growingListId) {
            this._GROWING_LIST_ID = growingListId;
            this._GROWING_DATA_PATH = "";

            this._component = component;
            this._globalViewModel = globalViewModel;
            this._requestHelper = requestHelper;
            this._needsReload = true;
            this._delayedSearch = null;

            this._formatter = new ViewModelFormatter(this._component);

            this._growingDelegate = null;
            this._errorCallback = null;

            this.fileSystem = new FileSystem();

            JSONModel.call(this, this.getDefaultData());
        }

        ViewModel.prototype = Object.create(JSONModel.prototype, {
            getDefaultData: {
                value: function () {
                    return {};
                }
            },

            getFormatter: {
                value: function () {
                    return this._formatter;
                }
            },

            getLanguage: {
                value: function () {
                    return this._globalViewModel.getActiveUILanguage();
                }
            },

            setErrorCallback: {
                value: function (errorCb) {
                    this._errorCallback = errorCb;
                }
            },

            getErrorCallback: {
                value: function (errorCb) {
                    return this._errorCallback;
                }
            },

            executeErrorCallback: {
                value: function (error) {
                    if (!this.getErrorCallback()) {
                        return;
                    }

                    this.getErrorCallback()(error);
                }
            },

            removeFromModelPath: {
                value: function (object, modelPath) {
                    var indexOf = this.getProperty(modelPath).indexOf(object) > -1 ?
                        this.getProperty(modelPath).indexOf(object) :
                        this.getProperty(modelPath).map(item => item.MOBILE_ID).indexOf(object.MOBILE_ID);

                    if (indexOf == -1) {
                        return;
                    }

                    this.getProperty(modelPath).splice(indexOf, 1);
                    this.refresh();
                }
            },

            insertToModelPath: {
                value: function (object, modelPath) {
                    this.getProperty(modelPath).push(object);
                    this.refresh();
                }
            },

            updateModelPath: {
                value: function (oldObject, modelPath, newObject) {
                    var indexOf = this.getProperty(modelPath).indexOf(oldObject) > -1 ?
                        this.getProperty(modelPath).indexOf(oldObject) :
                        this.getProperty(modelPath).map(item => item.MOBILE_ID).indexOf(oldObject.MOBILE_ID);

                    if (indexOf === -1) {
                        return;
                    }

                    this.setProperty(modelPath + "/" + indexOf, newObject);
                    this.refresh();
                }
            },

            getUserInformation: {
                value: function () {
                    return {
                        personnelNumber: this._globalViewModel.model.getProperty("/personnelNumber"),
                        workCenterId: this._globalViewModel.model.getProperty("/workCenterId"),
                        firstName: this._globalViewModel.model.getProperty("/firstName"),
                        lastName: this._globalViewModel.model.getProperty("/lastName"),
                        storageLocation: this._globalViewModel.model.getProperty("/storage_location"),
                        plant: this._globalViewModel.model.getProperty("/plant"),
                        teams: this._globalViewModel.model.getProperty("/teams"),
                        userName: this._globalViewModel.model.getProperty("/userName")
                    };
                }
            },

            getUserStatusMl: {
                value: function (status, statusProfile) {
                    var data = this.getCustomizingModelData("UserStatusMl");

                    for (var i = 0; i < data.length; i++) {
                        var stat = data[i];

                        if (stat.STATUS_PROFILE == statusProfile && stat.USER_STATUS == status) {
                            return stat;
                        }
                    }

                    return null;
                }
            },

            getUserStatusMlForButtons: {
                value: function (status, statusProfile) {
                    var data = this.getCustomizingModelData("UserStatusMl");

                    for (var i = 0; i < data.length; i++) {
                        var stat = data[i];

                        if (stat.STATUS_PROFILE == statusProfile && stat.USER_STATUS_CODE == status) {
                            return stat;
                        }
                    }

                    return null;
                }
            },

            getUserStatusFlowMl: {
                value: function (statusProfile) {
                    return this.getCustomizingModelData("UserStatusMl").filter(function (status) {
                        return status.STATUS_PROFILE === statusProfile;
                    });
                }
            },

            needsReload: {
                value: function () {
                    return this._needsReload;
                }
            },

            getGrowingListSettings: {
                value: function () {
                    var samConfig = this._component.getManifestObject().getEntry("sap.app").SAMConfig;
                    var listConfig = samConfig.growingLists;
                    var settings = listConfig[this._GROWING_LIST_ID];

                    if (settings == undefined) {
                        return {
                            loadChunk: 0,
                            growingThreshold: 0
                        }
                    }

                    return {
                        loadChunk: parseInt(settings.loadChunk),
                        growingThreshold: parseInt(settings.growingThreshold)
                    }
                }
            },

            setGrowingListDelegate: {
                value: function (delegate) {
                    if (!this._growingDelegate) {
                        this._growingDelegate = delegate;
                    }
                }
            },

            getGrowingListDelegate: {
                value: function () {
                    return this._growingDelegate;
                }
            },

            getCustomizingModelData: {
                value: function (modelName) {
                    return this._component.customizingModel.model.getProperty("/" + modelName);
                }
            },

            resetGrowingListDelegate: {
                value: function () {
                    if (!this.getGrowingListDelegate()) {
                        return;
                    }

                    this.getGrowingListDelegate()._iLimit = this.getGrowingListSettings().growingThreshold;
                }
            },

            handleListGrowing: {
                value: function () {

                }
            }
        });

        ViewModel.prototype.onListGrowing = function (oEvt) {
            // Based on category select query name
            if (oEvt.getParameter("reason") !== "Growing") {
                return;
            }

            var currentCount = oEvt.getParameter("actual");

            this.setGrowingListDelegate(oEvt.getSource()._oGrowingDelegate);

            if ((currentCount + this.getGrowingListSettings().growingThreshold) >= this.getProperty(this._GROWING_DATA_PATH).length) {
                this.handleListGrowing();
            }
        };

        ViewModel.prototype._moveAttachmentsFromTempToDoc = function (attachments) {
            const that = this;
            var device = sap.ui.Device.os;

            // Filter out the attachments that do not have to move as they are already at the target directory
            var aFilteredAttachments = attachments.filter(function (oAttachment) {

                while(oAttachment.folderPath[oAttachment.folderPath.length -1] === "/"){
                    oAttachment.folderPath = oAttachment.folderPath.substr(0, oAttachment.folderPath.lastIndexOf("/"));
                }

                var systemPath = that.fileSystem.filePathTmp;

                while(systemPath[systemPath.length -1] === "/"){
                    systemPath = systemPath.substr(0, systemPath.lastIndexOf("/"));
                }

                return oAttachment.folderPath === systemPath;

            });

            var moveQueue = aFilteredAttachments.map(function(attachment) {
                var tmpPath = that.fileSystem.filePathTmp + "/" + attachment.getFileName() + "." + attachment.getFileExt();
                return that.fileSystem.moveFilePromise(tmpPath, that.fileSystem.getRootDataStorage(), null);
            });

            return Promise.all(moveQueue);
        };

        ViewModel.prototype._deleteAttachments = function (attachments) {
            const that = this;
            var deleteQueue = attachments.map(function (attachment) {
                var filePath = "";
                var fileName = attachment.getFileName() + "." + attachment.getFileExt();

                if (attachment.MOBILE_ID == "") {
                    filePath = that.fileSystem.filePathTmp + "/" + fileName;
                } else {
                    filePath = that.fileSystem.getRootDataStorage() + "/" + fileName;
                }

                return that.fileSystem.deleteByPathPromise(filePath);
            });

            return Promise.all(deleteQueue);
        };

        ViewModel.prototype._renameAttachments = function (attachments) {
            const that = this;
            var renameQueue = attachments.map(function (attachment) {
                var filePath = "";
                var fileName = attachment.getFileName() + "." + attachment.getFileExt();
                var oldFileName = attachment._OLD_FILENAME + "." + attachment.getFileExt();

                if (attachment.MOBILE_ID == "") {
                    filePath = that.fileSystem.filePathTmp + "/" + oldFileName;
                } else {
                    filePath = that.fileSystem.getRootDataStorage() + "/" + oldFileName;
                }

                return that.fileSystem.renameFilePromise(filePath, fileName);
            });

            return Promise.all(renameQueue);
        };

        return ViewModel;
    });
