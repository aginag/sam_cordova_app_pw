sap.ui.define([
        "sap/ui/model/json/JSONModel",
        "SAMContainer/models/DataObject",
        "SAMContainer/models/sam/SAMUdb",
        "SAMContainer/models/sam/SAMScenario",
        "SAMContainer/models/sam/SAMScenarioVersion",
        "SAMContainer/models/sam/SAMSyncModel",
        "SAMContainer/models/sam/SAMComponent"
    ],

    function (JSONModel, DataObject, SAMUdb, SAMScenario, SAMScenarioVersion, SAMSyncModel, SAMComponent) {
        "use strict";

        // constructor
        function SAMUser(data, requestHelper, model) {
            DataObject.call(this, "SAM_USERS", "SAM_USERS", requestHelper, model);

            this.data = data;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.columns = ["UDB_DLED"];
            this.setDefaultData();

            this._udb = null;
            this._scenario = null;
        }

        SAMUser.prototype = Object.create(DataObject.prototype, {});

        SAMUser.prototype.onAfterExistingDataSet = function () {

        };

        SAMUser.prototype.getScenario = function () {
            return this._scenario;
        };

        SAMUser.prototype.getUdb = function () {
            return this._udb;
        };

        SAMUser.prototype.getUserName = function () {
            return this.USER_NAME;
        };

        SAMUser.prototype._getI18n = function () {
            return this.model._component.i18n;
        };

        SAMUser.prototype.initNewObject = function () {
            throw new Error("Not allowed to create users on client");
        };

        SAMUser.prototype.init = function () {
            const that = this;
            // Fetch SAM_USER_UDB
            // Fetch SAM_SCENARIO
            // Fetch SAM_SCENARIO_VERSION
            // Fetch SAM_SYNC_MODELS
            const promiseQueue = [this._fetchSAMUserUdb(), this._fetchSAMScenario()];

            return Promise.all(promiseQueue).then(function (results) {
                if (!results[0] instanceof SAMUdb) {
                    throw new Error(that._getI18n().getText("ERROR_SAM_USER_INIT_NO_UDB_INFO"));
                }

                if (!results[1] instanceof SAMScenario) {
                    throw new Error(that._getI18n().getText("ERROR_SAM_USER_INIT_NO_SCENARIO_INFO"));
                }

                that._udb = results[0];
                that._scenario = results[1];

                return true;
            });
        };

        SAMUser.prototype.sync = function(progressHandler, publication, syncMode, fileCompressionQuality) {
            var syncModels = [];

            if (!publication) {
                syncModels = this.getScenario().syncModels;
            } else {
                syncModels = this.getScenario().syncModels.filter(function(syncModel) {
                    return syncModel.publication.toLowerCase() == publication.toLowerCase();
                });
            }

            syncModels = syncModels.filter(function(syncModel) {
                return syncModel.publication.toUpperCase() != "LIVE_TRACKING";
            });

            var changedOrderSyncModels = [];

            if(syncModels.length > 0){
                if(syncModels[0].name === 'PW_SMALL'){
                    changedOrderSyncModels.push(syncModels[1]);
                    changedOrderSyncModels.push(syncModels[0]);
                } else {
                    changedOrderSyncModels = syncModels;
                }
            }

            changedOrderSyncModels = changedOrderSyncModels.filter(function( element ) {
                return element !== undefined;
             });

            return new Promise(function(resolve, reject) {
                if (changedOrderSyncModels.length == 0) {
                    reject(5003);
                    return;
                }

                changedOrderSyncModels.reduce(function(currentModel, nextModel) {
                    return currentModel.then(function() {
                        return nextModel.synchronize(progressHandler, syncMode, fileCompressionQuality);
                    });
                }, Promise.resolve()).then(function() {
                    resolve();
                }).catch(function(err) {
                    reject(err);
                });
            });
        };

        SAMUser.prototype.initLatestScenarioVersion = function() {
            const that = this;

            if (!this._udb || !this._scenario) {
                throw new Error(this._getI18n().getText("ERROR_SAM_USER_NO_UDB_SCENARIO_INITIALIZED"));
            }

            return this._fetchLatestSAMScenarioVersion().then(function(scenarioVersion) {
                that.getScenario().setVersion(scenarioVersion);
            });
        };

        SAMUser.prototype.upgradeUdb = function () {
            const that = this;

            return new Promise(function (resolve, reject) {
                if (!that.getScenario().getVersion().hasUpgradeSQLScript()) {
                    reject(new Error(that._getI18n().getText("ERROR_SAM_USER_NO_REMOTE_UDB_UPGRADE_NEEDED")));
                    return;
                }

                var upgradeScripts = that.getScenario().getVersion().getUpgradeScript();
                executeUpdatesSequentially(upgradeScripts, resolve, reject.bind(null, new Error(that._getI18n().getText("ERROR_DURING_REMOTE_UPGRADE"))));
            });
        };

        SAMUser.prototype.upgradeComponentPreload = function () {
            // 1. Download new component preload
            const component = this.getScenario().getVersion().component;
            return component.download(this, this.getScenario().getVersion());
        };

        SAMUser.prototype.deleteObsoleteComponentPreloadFiles = function () {
            return new Promise(function (resolve, reject) {
                try {
                    this._fetchObsoleteSAMScenarioVersions().then(function (data) {
                        var aDeleteFilesPromises = [];
                        data.SAMScenarioVersionObsolete.forEach(function (oData) {
                            if (oData.VERSION && oData.UI5_COMPONENT_NAME) {
                                var oPromise = new Promise(function (resolve, reject) {
                                    // delete file and directory
                                    var device = sap.ui.Device.os,
                                        os = device.name,
                                        folderName = this.USER_NAME + '/' + oData.UI5_COMPONENT_NAME + '_' + oData.VERSION;

                                    if (os === device.OS.WINDOWS) {
                                        folderName = "Users/" + folderName;
                                    }
                                    var filePath = folderName + "/Component-preload.js";

                                    fileExists(filePath, function (result) {
                                        if (!result) {
                                            resolve(filePath);
                                        } else {
                                            deleteFile(filePath, function() {
                                                folderExists(folderName, function (resultFolder) {
                                                    if (!resultFolder) {
                                                        resolve(folderName);
                                                    } else {
                                                        deleteFolder(folderName, resolve, reject);
                                                    }
                                                });
                                            }, reject);
                                        }
                                    });

                                }.bind(this));
                                aDeleteFilesPromises = aDeleteFilesPromises.concat(oPromise);
                            }
                        }.bind(this));
                        Promise.all(aDeleteFilesPromises).then(resolve).catch(reject);

                    }.bind(this)).catch(reject);
                } catch (err) {
                    reject(err);
                }
            }.bind(this));
        };

        SAMUser.prototype.resetUdb = function () {
            const that = this;

            return new Promise(function(resolve, reject) {
                resetDB(that.getUserName(), "", resolve, reject);
            });
        };

        SAMUser.prototype.setUpgradeDone = function() {
            // Update SAM_USER_UDB with latest scenario version id
            // Update UPDATE_DONE column to done
            return new Promise(function(resolve, reject) {
                try {
                    this.getUdb().setScenarioVersionId(this.getScenario().getVersion().getId());
                    this.getUdb().setUpgradeDone();

                    if (!this.getUdb().hasChanges()) {
                        resolve();
                        return;
                    }

                    this.getUdb().update2(false, resolve, reject.bind(null, this._getI18n().getText("ERROR_SAM_USER_UDB_UPGRADE_DONE_FAILED")));
                } catch(err) {
                    reject(err);
                }
            }.bind(this));
        };

        SAMUser.prototype._fetchSAMUserUdb = function () {
            const that = this;
            // Fetch SAM_USER_UDB
            return this.requestHelper.fetch({
                id: "SAMUserUdb",
                statement: "SELECT * FROM SAM_USER_UDB WHERE USER_ID = '@userId'"
            }, {
                userId: this.ID
            }).then(function (data) {
                if (data.SAMUserUdb.length == 0) {
                    throw new Error(that._getI18n().getText("ERROR_SAM_USER_INIT_NO_UDB_INFO"));
                }

                return new SAMUdb(data.SAMUserUdb[0], that.requestHelper);
            });
        };

        SAMUser.prototype._fetchSAMScenario = function () {
            // Fetch SAM_SCENARIO
            // Fetch SAM_SCENARIO_VERSION
            // Fetch SAM_SYNC_MODELS
            return Promise.all([
                this._fetchSAMScenarioVersion(),
                this._fetchSAMSyncModels(),
                this.requestHelper.fetch({
                    id: "SAMScenario",
                    statement: "SELECT * FROM SAM_SCENARIOS " +
                        "WHERE ID = '@scenarioId'"
                }, {
                    scenarioId: this.SCENARIO_ID
                })
            ]).then(function (results) {
                if (results[0].SAMScenarioVersion.length == 0) {
                    throw new Error(this._getI18n().getText("ERROR_SAM_USER_INIT_NO_SCENARIO_VERSION_INFO"));
                }

                if (results[2].SAMScenario.length == 0) {
                    throw new Error(this._getI18n().getText("ERROR_SAM_USER_INIT_NO_SCENARIO_INFO"));
                }

                var ui5Component = null;
                var scenarioVersionInfo = results[0].SAMScenarioVersion[0];
                var syncModelsInfo = results[1].SAMSyncModel;
                var scenarioInfo = results[2].SAMScenario[0];

                if (scenarioVersionInfo.UI5_COMPONENT_NAME != "" & scenarioVersionInfo.UI5_FILE_NAME != "") {
                    ui5Component = new SAMComponent(scenarioVersionInfo.UI5_COMPONENT_NAME, scenarioVersionInfo.UI5_FILE_NAME)
                }

                var scenarioVersion = new SAMScenarioVersion(scenarioVersionInfo, ui5Component);

                var syncModels = syncModelsInfo.map(function (syncModelInfo) {
                    return new SAMSyncModel(syncModelInfo.ID, syncModelInfo.NAME, syncModelInfo.PUBLICATION);
                });

                return new SAMScenario(scenarioInfo, scenarioVersion, syncModels);
            }.bind(this));
        };

        SAMUser.prototype.fetchInitialDeltaDone = function () {
            return this.requestHelper.fetch({
                id: "SAMInitialDeltaDone",
                statement: "SELECT sudb.INITIAL_DELTA_DONE FROM SAM_USERS as su " +
                    "JOIN SAM_USER_UDB as sudb on sudb.USER_ID = su.ID " +
                    "WHERE su.ID = '@userId'"
            }, {
                userId: this.ID
            });
        };

        SAMUser.prototype._fetchSAMScenarioVersion = function () {
            return this.requestHelper.fetch({
                id: "SAMScenarioVersion",
                statement: "SELECT sv.ID, sv.VERSION, sv.UPGRADE_SQL_CMDS, sv.CLIENT_MIN_VERSION, sv.UI5_COMPONENT_NAME, sv.UI5_FILE_NAME, sv.VERSION FROM SAM_USER_UDB as udb " +
                    "JOIN SAM_SCENARIO_VERSIONS as sv on sv.ID = udb.SCENARIO_VERSION_ID " +
                    "WHERE udb.USER_ID = '@userId'"
            }, {
                userId: this.ID
            });
        };

        SAMUser.prototype._fetchLatestSAMScenarioVersion = function () {
            const that = this;

            if (!this._udb || !this._scenario) {
                throw new Error(this._getI18n().getText("ERROR_SAM_USER_NO_UDB_SCENARIO_INITIALIZED"));
            }

            return this.requestHelper.fetch({
                id: "SAMScenarioVersion",
                statement: "SELECT TOP 1 * FROM SAM_SCENARIO_VERSIONS WHERE SCENARIO_ID = '@scenarioId' AND ACTIVE = 1 ORDER BY VERSION DESC"
            }, {
                scenarioId: this.getScenario().getId()
            }).then(function(data) {
                if (data.SAMScenarioVersion.length == 0) {
                    throw new Error(that._getI18n().getText("ERROR_SAM_USER_INIT_NO_SCENARIO_VERSION_INFO"));
                }

                var scenarioVersionInfo = data.SAMScenarioVersion[0];

                var ui5Component = null;

                if (scenarioVersionInfo.UI5_COMPONENT_NAME != "" & scenarioVersionInfo.UI5_FILE_NAME != "") {
                    ui5Component = new SAMComponent(scenarioVersionInfo.UI5_COMPONENT_NAME, scenarioVersionInfo.UI5_FILE_NAME)
                }

                var scenarioVersion = new SAMScenarioVersion(scenarioVersionInfo, ui5Component);

                that.getScenario()._latestScenarioVersion = scenarioVersion;

                return scenarioVersion;
            });
        };

        SAMUser.prototype._fetchObsoleteSAMScenarioVersions = function () {
            return this.requestHelper.fetch({
                id: "SAMScenarioVersionObsolete",
                statement: "SELECT DISTINCT scv2.UI5_COMPONENT_NAME, scv2.VERSION FROM SAM_SCENARIO_VERSIONS as scv2 " +
                    "WHERE NOT EXISTS (" +
                    "SELECT scv.VERSION FROM SAM_USERS as su " +
                    "JOIN SAM_USER_UDB as udb on udb.USER_ID = su.ID " +
                    "JOIN SAM_SCENARIO_VERSIONS as scv on scv.ID = udb.SCENARIO_VERSION_ID " +
                    "WHERE scv.ID = scv2.ID )"
            });
        };

        SAMUser.prototype._fetchSAMSyncModels = function () {
            return this.requestHelper.fetch({
                id: "SAMSyncModel",
                statement: "SELECT sm.ID, sm.NAME, sm.\"PUBLICATION\" FROM SAM_USER_UDB as udb " +
                    "JOIN SAM_SC_VERS_SYNC_MODELS as svsm on svsm.VERSION_ID = udb.SCENARIO_VERSION_ID " +
                    "JOIN SAM_SYNC_MODELS as sm on sm.ID = svsm.SYNC_MODEL_ID " +
                    "WHERE USER_ID = '@userId'"
            }, {
                userId: this.ID
            });
        };

        SAMUser.prototype.constructor = SAMUser;

        return SAMUser;
    }
);