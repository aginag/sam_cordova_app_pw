sap.ui.define([],

    function() {
        "use strict";

        function SAMScenarioVersion(info, component) {
            this.id = info.ID;
            this.minimumClientVersion = info.CLIENT_MIN_VERSION;
            this.version = info.VERSION;
            this.upgradeScript = info.UPGRADE_SQL_CMDS;
            this.component = component;
        }

        SAMScenarioVersion.prototype.getId = function() {
            return this.id;
        };

        SAMScenarioVersion.prototype.getRequiredVersionNumber = function() {
            return this.minimumClientVersion;
        };

        SAMScenarioVersion.prototype.getUpgradeScript = function() {
            var statements = this.upgradeScript.replace(new RegExp("COMMIT", 'g'), "").split("%end%");
            var statements = statements.map(function(statement) {
                return statement.trim();
            }).filter(function(statement) {
               return statement.length > 0;
            });

            return statements;
        };

        SAMScenarioVersion.prototype.hasComponent = function() {
            return this.component != null;
        };

        SAMScenarioVersion.prototype.hasUpgradeSQLScript = function() {
            return this.upgradeScript != null && this.upgradeScript.length > 0;
        };

        /*https://gist.github.com/TheDistantSea/8021359*/
        SAMScenarioVersion.prototype.isMinimumVersionStatisfied = function(currentVersion) {
            var lexicographical = false,
                zeroExtend = true,
                currentVersion = currentVersion.split('.'),
                requiredVersion = this.minimumClientVersion.split('.');

            function isValidPart(x) {
                return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
            }

            if (!currentVersion.every(isValidPart) || !requiredVersion.every(isValidPart)) {
                return false;
            }

            if (zeroExtend) {
                while (currentVersion.length < requiredVersion.length) currentVersion.push("0");
                while (requiredVersion.length < currentVersion.length) requiredVersion.push("0");
            }

            if (!lexicographical) {
                currentVersion = currentVersion.map(Number);
                requiredVersion = requiredVersion.map(Number);
            }

            for (var i = 0; i < currentVersion.length; ++i) {
                if (requiredVersion.length == i) {
                    return true;
                }

                if (currentVersion[i] == requiredVersion[i]) {
                    continue;
                }
                else if (currentVersion[i] > requiredVersion[i]) {
                    return true;
                }
                else {
                    return false;
                }
            }

            if (currentVersion.length != requiredVersion.length) {
                return false;
            }

            return true;
        };

        SAMScenarioVersion.prototype.constructor = SAMScenarioVersion;

        return SAMScenarioVersion;
    }
);