sap.ui.define([],

    function() {
        "use strict";

        function SAMSyncModel(id, name, publication) {
            this.id = id;
            this.name = name;
            this.publication = publication;
        }

        SAMSyncModel.prototype.constructor = SAMSyncModel;

        SAMSyncModel.prototype.synchronize = function (progressHandler, syncMode, fileCompressionQuality) {
            var data = [];
            data.push(syncMode ? syncMode : "");
            data.push(this.publication);

            return new Promise(function(resolve, reject) {
                sync(resolve, reject, progressHandler, data, fileCompressionQuality);
            });
        };

        return SAMSyncModel;
    }
);