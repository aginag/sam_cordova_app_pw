sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/models/DataObject"],

    function(JSONModel, DataObject) {
        "use strict";
        // constructor
        function SAMUdb(data, requestHelper, model) {
            DataObject.call(this, "SAM_USER_UDB", "SAM_USER_UDB", requestHelper, model);

            this.data = data;

            this.callbacks.afterExistingDataSet = this.onAfterExistingDataSet;

            this.columns = ["UPGRADE_DONE", "UPGRADE_NEEDED", "SCENARIO_VERSION_ID"];
            this.setDefaultData();
        }

        SAMUdb.prototype = Object.create(DataObject.prototype, {});

        SAMUdb.prototype.onAfterExistingDataSet = function() {

        };

        SAMUdb.prototype.setScenarioVersionId = function(id) {
            this.SCENARIO_VERSION_ID = id;
        };

        SAMUdb.prototype.setUpgradeDone = function() {
            this.UPGRADE_DONE = "1";
            this.UPGRADE_NEEDED = "0";
        };

        SAMUdb.prototype.getUpgradeNeeded = function() {
            return this.UPGRADE_NEEDED == "1";
        };

        SAMUdb.prototype.getInitialDeltaDone = function() {
            return this.INITIAL_DELTA_DONE == "1";
        };

        SAMUdb.prototype.initNewObject = function() {
            throw new Error("Not allowed to create new object on client");
        };

        // @overwrite
        SAMUdb.prototype.update2 = function (bStrings, successCb, errorCb) {
            var that = this;
            var valuesForUpdate = this.getChangedSqlValues(this.getSqlValues());

            // Update the current state with the updated values
            this.columns.forEach(function(column) {
                that.data[column] = that[column] || "";
            });

            var sqlString = {
                type: "update",
                mainTable: this.mainTable,
                tableName: this.tableName,
                values: valuesForUpdate,
                replacementValues: [],
                whereConditions: " USER_ID = '" + this.USER_ID + "'",
                mainObjectValues: []
            };

            if (bStrings) {
                return sqlString;
            } else {
                this.requestHelper.executeStatementsAndCommit([sqlString], function (e) {
                    successCb(e);
                }, errorCb);
            }
        };

        SAMUdb.prototype.constructor = SAMUdb;

        return SAMUdb;
    }
);