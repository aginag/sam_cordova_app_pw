sap.ui.define([],

    function() {
        "use strict";

        function SAMScenario(info, version, syncModels) {
            this.id = info.ID;
            this.name = info.NAME;
            this.liveTrackingEnabled = info.LIVE_TRACK_ENABLED;
            this.liveTrackingFrequency = info.LIVE_TRACK_FREQUENCY;
            this.version = version;
            this.syncModels = syncModels;
        }

        SAMScenario.prototype.constructor = SAMScenario;

        SAMScenario.prototype.getId = function() {
            return this.id;
        };

        SAMScenario.prototype.getVersion = function() {
            return this.version;
        };

        SAMScenario.prototype.setVersion = function(version) {
            this.version = version;
        };

        SAMScenario.prototype.isLiveTrackEnabled = function() {
            return this.liveTrackingEnabled == "1";
        };

        SAMScenario.prototype.getLiveTrackingFrequency = function() {
            return this.liveTrackingFrequency;
        };

        return SAMScenario;
    }
);