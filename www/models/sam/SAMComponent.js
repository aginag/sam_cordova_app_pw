sap.ui.define([],

    function () {
        "use strict";

        function SAMComponent(name, fileName) {
            this.name = name;
            this.fileName = fileName;

            this.COMPONENT_PRELOAD_FILE_NAME = "Component-preload.js";
        }

        SAMComponent.prototype.download = function (samUser, scenarioVersion) {
            const that = this;


            return new Promise(function(resolve, reject) {
                var device = sap.ui.Device.os;
                var os = device.name;
                // Download component preload from middleware and initialize it
                const component = that;
                const scenarioVersionNumber = scenarioVersion.version;

                var folderName = samUser.USER_NAME + '/' + component.name + '_' + scenarioVersionNumber;

                if (os === device.OS.WINDOWS) {
                    folderName = "Users/" + folderName;
                }

                const filePath = folderName + '/' + that.COMPONENT_PRELOAD_FILE_NAME;

                fileExists(filePath, function (result) {
                    if (result) {
                        resolve(filePath);
                    } else {
                        // Download component preload
                        downloadFile(component.fileName, function () {
                            //create folder
                            createFolderInPersistentStorage(folderName, function () {
                                //rename and move file
                                moveFile(component.fileName, '', that.COMPONENT_PRELOAD_FILE_NAME, folderName, resolve.bind(null, filePath), reject.bind(null, new Error("Error moving preload file")));
                            }, reject.bind(null, new Error("Error creating persistent storage")));
                        }, reject.bind(null, new Error("Error on download preload file")), function() {});
                    }
                });
            });
        };

        SAMComponent.prototype.constructor = SAMComponent;

        return SAMComponent;
    }
);