﻿sap.ui.define(["sap/ui/model/json/JSONModel", "SAMContainer/helpers/RequestHelper"],

   function(JSONModel, RequestHelper) {
      "use strict";
      // constructor
      function GlobalViewModel() {
         this.defaultLanguage = {
             id: "D",
             ui5Lang: "de",
             text: "Deutsch"
         };
         this.model = new JSONModel({
            busy: false,
            fileLoading: false,
            hasChanges: false,
             deltaDone: false,
             editMode: false,
            personnelNumber: '',
            workCenterId: '',
            firstName: '',
            lastName: '',
             userName: '',
            teams: [],
            language: this.getPrefLanguage(),
            availableLanguages: this.defaultLanguages,
            headerText: '',
            currentRoute: '',
            backNavEnabled: false,
            calendarMode: false,
            unreadMessages: 0,
            assignedOrders: 0,
             selectedTheme: sap.ui.getCore().getConfiguration().getTheme(),
            assignedNotifications: 0,
             samUser: null,
             remoteId: "",
             component: {
                 headerText: '',
                 backNavEnabled: false,
                 backNavFunction: null
             }
         });
      }

      GlobalViewModel.prototype = {
         requestHelper : RequestHelper,

         defaultLanguages: [{
             id: "D",
             ui5Lang: "de",
             text: "Deutsch"
         }, {
            id: "E",
            ui5Lang: "en",
            text: "English"
         }],
         getActiveUILanguage: function() {
            return this.model.oData.language;
         },

         getPrefLanguage: function() {
            if (localStorage.getItem("SAM_lang") !== null) {
               var ui5Lang = localStorage.getItem("SAM_lang");
               sap.ui.getCore().getConfiguration().setLanguage(ui5Lang);
            } else {
               sap.ui.getCore().getConfiguration().setLanguage(this.defaultLanguage.ui5Lang);
               return this.defaultLanguage.id;
            }
            for (var i = 0; i < this.defaultLanguages.length; i++) {
               var lang = this.defaultLanguages[i];
               if (lang.ui5Lang === ui5Lang) {
                  return lang.id;
               }
            }
            return this.defaultLanguage.id;

         },

         getOS: function() {
            var device = sap.ui.Device.os;
            return device.name;
         },

         getUserName: function() {
            return this.model.oData.firstName + " " + this.model.oData.lastName;
         },

         setHeaderText: function(text) {
            this.model.setProperty("/headerText", text);
         },
         setCurrentRoute: function(route) {
            this.model.setProperty("/currentRoute", route);
         },
          getCurrentRoute: function() {
              return this.model.getProperty("/currentRoute");
          },
          getSAMUser: function() {
              return this.model.getProperty("/samUser");
          },
         setBackNavEnabled: function(bool) {
            this.model.setProperty("/backNavEnabled", bool);
         },
          setComponentHeaderText: function(text) {
              this.model.setProperty("/component/headerText", text);
              this.setHeaderText(text);
          },
          setComponentBackNavEnabled: function(bool) {
              this.setBackNavEnabled(bool);
              this.model.setProperty("/component/backNavEnabled", bool);
              this.setComponentBackNavFunction(!bool ? null : this.getComponentBackNavFunction());
          },
          setComponentBackNavFunction: function(fn) {
              this._backNavFunction = fn;
          },
          getComponentBackNavFunction: function() {
              return this._backNavFunction;
          },

         setData: function(data) {
            this.model.setObjectData(data);
         },
         hasChanges: function() {
            return this.model.getProperty("/hasChanges");
         },

         updateUnreadSyncMessageCount: function() {
            var that = this;

            this.requestHelper.getData("SAMSyncMessages", {}, function(data) {
               that.model.setProperty("/unreadMessages", parseInt(data[0][""]));
            }, function(error) {
               //MessageBox.alert(error);
            }, true, "unreadCount");

         },

         refresh: function() {
            this.model.refresh();
         }
      };

      var globalViewModel = GlobalViewModel;

      return globalViewModel;
   });