sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageBox", "sap/m/BusyDialog"], function (Controller, MessageBox, BusyDialog) {
    "use strict";

    return Controller.extend("SAMContainer.controller.home.ComponentHolder", {
        onInit: function () {
            this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
            sap.ui.getCore().getEventBus().subscribe("core", "onLoggedOut", this.onLogout, this);

            this.oRouter.getRoute("container").attachPatternMatched(this._onRouteMatched, this);

            this.components = [];
            this.component = this.getOwnerComponent();
            this.isInitialized = false;
        },

        _onRouteMatched: function (oEvent) {
            const samUser = this.getOwnerComponent().globalViewModel.getSAMUser();
            // Check SAMScenario if udb preload is available?
            // -> Yes -> Download it from the MW and initialize
            // -> No -> Try to initiate component from static resources
            if (!this.getOwnerComponent().getManifestEntry("sap.app").SAMConfig.useLocalDevSources && samUser.getScenario().getVersion().hasComponent()) {
                this.initializeDynamically();
            } else {
                this.initializeStatically();
            }
        },

        onExit: function(oEvent) {
            sap.ui.getCore().getEventBus().unsubscribe("core", "onLoggedOut", this.onLogout, this);
        },

        onLogout: function(channel, eventId, data) {
            var hardReload = false;

            if (data) {
                hardReload = data.hardReload ? data.hardReload : hardReload;
            }

            if (this.components.length > 0) {
                this.unloadComponent();
            }

            this.oRouter.navTo("login", { query: {hardReload: hardReload} });
        },

        unloadComponent: function() {
            this.components[0].destroy();
            this.components = [];
        },

        initializeDynamically: function() {
            const that = this;
            // Download component preload from middleware and initialize it
            const samUser = this.getOwnerComponent().globalViewModel.getSAMUser();
            const component = samUser.getScenario().getVersion().component;

            const onPrerequisitesFinished = function(filePath) {
                if(that.busyDialog){
                    that.busyDialog.destroy();
                }
                clearInterval(that.myVar);
                jQuery.getScript(this.getDocumentsDirectoryForPlatform() + "/" + filePath, function(data) {
                    that._loadComponent({
                        id: "samComponent",
                        name: component.name
                    });
                });
            };

            var oClose = function(oEvent){
                that.cancelPressed = oEvent.getParameter("cancelPressed")
                    if (that.cancelPressed) {
                        var cancelSuccess = function() {
                            that.oRouter.navTo("login");
                        };
        
                    cancelFileTransfer( samUser.getScenario().version.component.name, cancelSuccess, cancelSuccess);
                }              
            };

            this.cancelPressed = false;
            this.busyDialog = new BusyDialog({
                close: oClose
            });
            this.busyDialog.addStyleClass("busyDialogText");
            
            this.busyDialog.open();
            this.myVar = setInterval(function(){
                that.busyDialog.setShowCancelButton(true);
                that.busyDialog.setText(that.component.i18n.getText("SLOW_INTERNET_MESSAGE"));
            }, 5000);

            component.download(samUser, samUser.getScenario().getVersion()).then(onPrerequisitesFinished.bind(this)).catch(this.onDynamicInitializationError.bind(this));
        },

        onDynamicInitializationError: function(oEvent) {
            var that = this;
            if(that.cancelPressed){
                if(this.busyDialog){
                   this.busyDialog.destroy();
                }
                clearInterval(this.myVar);
                MessageBox.error(this.component.i18n.getText("ERROR_DOWLOAD_PRELOAD"),{
                    onClose: function() {
                        that.oRouter.navTo("login");
                    }
                });
            }
        },

        getDocumentsDirectoryForPlatform: function () {
            var device = sap.ui.Device.os;
            var os = device.name;

            switch (os) {
                case device.OS.ANDROID:
                    return cordova.file.dataDirectory + "files";
                case device.OS.IOS:
                    return cordova.file.documentsDirectory;
                default:
                    return cordova.file.dataDirectory;
            }
        },

        initializeStatically: function() {
            const that = this;
            // Initialize from static resources
            const samMobileComponent = {
                id: "samComponent",
                name: "SAMMobile"
            };

            jQuery.sap.registerModulePath("SAMMobile", "./components/SAMMobile");

            // Check static resources are available, will throw an error if they are not
            sap.ui.require(['SAMContainer/components/SAMMobile/Component'], function(c) {
                this._loadComponent(samMobileComponent);
            }.bind(this), function(c) {
                MessageBox.error(that.component.i18n.getText("ERROR_STATIC_COMPONENT_INITIALIZATION_FAILED"),{
                    onClose: function() {
                        that.oRouter.navTo("login");
                    }
                });
            });
        },

        _loadComponent: function(component) {
            const currentVisibleComponent = this._getVisibleComponent();

            if (currentVisibleComponent && currentVisibleComponent.getId() !== component.id) {
                currentVisibleComponent.setVisible(false);
            }

            if (!this._componentExists(component.id)) {
                const toComponent = new sap.ui.core.ComponentContainer(component.id, {
                    name: component.name,
                    height: "100%",
                    manifest: true,
                    async: false
                });

                toComponent.placeAt(this.getView().getId() + "--containerComponentHolder");

                this.components.push(toComponent);
            }

            const toComponent = this._getComponentById(component.id);

            if (toComponent) {
                toComponent.setVisible(true);
            }
        },

        _componentExists: function (componentId) {
            const foundComponent = this.components.filter(function (component) {
                return component.getId() == componentId;
            });

            return foundComponent.length > 0 ? true : false;
        },

        _getComponentById: function (componentId) {
            const foundComponent = this.components.filter(function (component) {
                return component.getId() === componentId;
            });

            return foundComponent.length > 0 ? foundComponent[0] : null;
        },

        _getVisibleComponent: function () {
            const visibleComponent = this.components.filter(function (component) {
                return component.getVisible();
            });

            return visibleComponent.length > 0 ? visibleComponent[0] : null;
        }
    });

});